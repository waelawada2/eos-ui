#!/usr/bin/env bash

#
# Portable shell-based URL encoder
#
# See: https://stackoverflow.com/questions/38015239/url-encoding-a-string-in-shell-script-in-a-portable-way
#
url_encode_pipe() {
  local LANG=C; local c; while IFS= read -r c; do
    case $c in [a-zA-Z0-9.~_-]) printf "$c"; continue ;; esac
    printf "$c" | od -An -tx1 | tr ' ' % | tr -d '\n'
  done <<EOF
$(fold -w1)
EOF
  echo
}

url_encode() { printf "$*" | url_encode_pipe ;}
