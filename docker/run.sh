#!/usr/bin/env bash

APP_NAME=eos-ui

. utils.sh

#
# Configure Keycloak
#
sed -i "s;https://keycloakqa.aws.sothebys.com/auth;${KEYCLOAK_URL};" /usr/share/nginx/html/${APP_NAME}/assets/js/keycloak.json

#
# Fetch environment-specific configuration from Config Server
#
echo "Config server parameters: uri=${CONFIG_SERVER_URI}, application=${APP_NAME}, profiles=${STATIC_PROFILES_ACTIVE}, label=${CONFIG_SERVER_LABEL}"

# Insert username and password into config server URL so Busybox's wget can process it: http://user:password@hostname
CONFIG_SERVER_PASSWORD=$(url_encode ${CONFIG_SERVER_PASSWORD})
CONFIG_SERVER_URL=$(echo ${CONFIG_SERVER_URI} | sed -e "s/\(https\{0,1\}\:\/\/\)/\1${CONFIG_SERVER_USER}:${CONFIG_SERVER_PASSWORD}@/")

# Append profiles and label
CONFIG_SERVER_URL=${CONFIG_SERVER_URL}/${APP_NAME}/${STATIC_PROFILES_ACTIVE}/${CONFIG_SERVER_LABEL}

# Read config from config server
FULL_CONFIG=$(wget -O - -T 10 ${CONFIG_SERVER_URL})
STATUS=$?

if [ ! ${STATUS} -eq 0 ]; then
  echo "Failed to fetch configuration from ${CONFIG_SERVER_URI}. Exiting."
  exit ${STATUS}
fi

echo ${FULL_CONFIG}| jq '.propertySources[0].source' > /usr/share/nginx/html/${APP_NAME}/assets/config.json

#
# Start NGINX
#
nginx -g 'daemon off;'
