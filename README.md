# Property database ui

Frontend for management of events.

## Prerequisites

- Nodejs 6 or newer
- Angular Cli

## How to Build

Clone the Git repository to a local directory:

```
$ git clone http://bitbucket.org/sothebys/property-database-ui
```  

## How to run

### Run locally

Run the application using `npm start`.

### Run with Docker

Build the Docker image:
```
docker build -t eos-ui .
```

Run the Docker image:
```
# docker run -it --rm -e PROFILES_ACTIVE=dev -e KEYCLOAK_URL=https://keycloakqa.aws.sothebys.com/auth -e CONFIG_SERVER_URI=http://stb-api.sdev.aws.sothebys.com/config-server -e CONFIG_SERVER_USER=<USER> -e CONFIG_SERVER_PASSWORD=<PASSWORD> -e CONFIG_SERVER_LABEL=master -p 8080:80 eos-ui
```

## How to test

Test the application using `npm test`.

## Configure authentication

``` 
sudo nano /etc/hosts 
```
Add 10.90.22.189    stb-api.dev.aws.sothebys.com keycloakqa.aws.sothebys.com stb-api.qa.aws.sothebys.com 

Login using sothebys username and user.
