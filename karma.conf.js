// Karma configuration file, see link for more information
// https://karma-runner.github.io/0.13/config/configuration-file.html
/* this constant is declared to hold params passed as --isolate in the ng test command,
in order to run certain set of tests instead of all suite */
const argv = require('minimist')(process.argv.slice(2)),
isolate = (argv.isolate !== true) && argv.isolate;
console.log("isolate",isolate);

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular/cli'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular/cli/plugins/karma')
    ],
    client:{
      clearContext: false, // leave Jasmine Spec Runner output visible in browser
      captureConsole:true,
      args: [isolate] // arguments to be passed
    },
    coverageIstanbulReporter: {
      reports: [ 'html', 'lcovisolate' ],
      fixWebpackSourcePaths: true
    },
    angularCli: {
      environment: 'dev'
    },
    reporters: ['progress', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false
  });
};
