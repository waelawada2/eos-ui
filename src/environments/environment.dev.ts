
export const environment = {
  production: false,
  sessionTime: {
    sessionTime: 1800000,
    popupTime: 1500000,
  }
}
