import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  //clear session
  headerClick() {
    sessionStorage.removeItem('selectSort');
    sessionStorage.removeItem('indexSort');
    sessionStorage.removeItem('MainViewQuery');
    sessionStorage.removeItem('MainQueryValue');
    sessionStorage.removeItem('searchparam');
    sessionStorage.removeItem('searchData');
    sessionStorage.removeItem('sortparam');
    sessionStorage.removeItem('totalelements');
    sessionStorage.removeItem('Savedlistsortingquery');
    sessionStorage.removeItem('refreshPage');
  }
}
