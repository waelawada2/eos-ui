import { Injectable } from '@angular/core';

@Injectable()
export class HeaderService {

  constructor() { }

  headerToggle(flag:number){
    return flag?1:0;
  }

}
