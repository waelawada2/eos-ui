import { BrowserModule } from '@angular/platform-browser';
import { Component, NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { routingComponents } from './app-routing.module';
import { Http, HttpModule } from '@angular/http';
import { FileSelectDirective } from 'ng2-file-upload';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';
import { ImageUploadModule } from 'ng2-imageupload';
import { ModalModule } from 'ngx-modal';
import { Ng2CompleterModule } from 'ng2-completer';
import { TagInputModule } from 'ngx-chips';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { DropuploadComponent } from './upload/dropupload/dropupload.component';
import { DropuploadComponent as NewObjectDropUploadComponent } from './upload/dropupload/new-object-dropupload.component';
import { MergeComponent } from './Merge/Merge.component';
import { ObjectService, CRNumberPipe, ArchiveNumberPipe } from './object/object.service';
import { ObjectComponent } from './object/object.component'
import { DynamicModule } from './Merge/dynamic-module';
import { DndModule } from 'ng2-dnd';
import { ObjectModalComponent } from './modal/newObjectModal.component';
import { EditionModalComponent } from './modal/newEditionModal.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SavedSearchService } from './saved-search/saved-search.service';
import { SavedSearchComponent } from './saved-search/saved-search.component';
import { SaveSearchModalComponent } from './saved-search/saved-search-modal.component';
import { SavedListComponent } from './saved-list/saved-list.component';
import { SaveListModalComponent } from './saved-list/saved-list-modal.component';
import { SaveListShareModalComponent } from './saved-list/saved-list-share-modal.component';
import { ToastyModule } from 'ng2-toasty';
import { TosatService } from './common/toasty-service';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { ConfirmComponent } from './common/confirm-component';
import { AlertComponent } from './common/alert-component';
import { MergeTwoComponent } from './Merge/mergetwo-component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { CustomSortComponent } from './Merge/customesort.component';
import { DualListBoxModule } from './customSortBox';
import { SortingComponent } from './sorting/sorting.component';
import { DeleteObjectStatusComponent } from './modal/deleteObjectStatusModal.component';
import { DialogModule } from 'primeng/primeng';
import { AngularDraggableModule } from 'angular2-draggable';
import { EventModule } from './event/event.module';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { PresentationComponent } from './object/presentation/presentation.component';
import { EventReportComponent } from './object/event-report/event-report.component';
import { EventCardsComponent } from './object/event-report/event-card-transaction/event-card-transaction.component';
import { EventCardPublicationComponent } from './object/event-report/event-card-publication/event-card-publication.component';
import { EventCardPricingComponent } from './object/event-report/event-card-pricing/event-card-pricing.component';
import { AddObjectToEventService } from './event/shared/services';
import { AddObjectsItemComponent } from './object/add-objects-item/add-objects-item.component';
import { CurrentOwnerComponent } from './object/current-owner/current.owner.component';
import { AdminComponent } from './admin/admin.component';
import { AmendDropdownComponent } from './amend-dropdown/amend-dropdown.component';
import { EditDropdownValuesModalComponent } from './amend-dropdown/edit-dropdown-values-modal.component';
import { AddDropdownValuesModalComponent } from './amend-dropdown/add-dropdown-values-modal.component';
import { SavedListBackgroundService } from './SavedlListBackground/savedListBackground.service'
import { RoleGuardService } from './authentication/role-guard.service';
import { AdminSavedList } from './adminSavedList/admin-saved-list.component';
import { ConfigurationLoaderService } from './configuration.service';
@NgModule({
  imports: [
    AppRoutingModule,
    BootstrapModalModule,
    BrowserAnimationsModule,
    BrowserModule,
    DndModule.forRoot(),
    DualListBoxModule.forRoot(),
    DynamicModule.withComponents([ObjectComponent]),
    EventModule,
    DialogModule,
    FormsModule,
    HttpModule,
    ImageUploadModule,
    InfiniteScrollModule,
    ModalModule,
    Ng2CompleterModule,
    NguiAutoCompleteModule,
    AngularDraggableModule,
    NgxDatatableModule,
    ReactiveFormsModule,
    SimpleNotificationsModule.forRoot(),
    SlimLoadingBarModule.forRoot(),
    TagInputModule,
    ToastyModule.forRoot(),
  ],
  declarations: [
    AddObjectsItemComponent,
    AlertComponent,
    AppComponent,
    ArchiveNumberPipe,
    ConfirmComponent,
    CRNumberPipe,
    CurrentOwnerComponent,
    CustomSortComponent,
    DropuploadComponent,
    NewObjectDropUploadComponent,
    EditionModalComponent,
    EventCardPricingComponent,
    EventCardPublicationComponent,
    EventCardsComponent,
    PresentationComponent,
    EventReportComponent,
    FileSelectDirective,
    FooterComponent,
    HeaderComponent,
    MergeComponent,
    MergeTwoComponent,
    ObjectComponent,
    ObjectModalComponent,
    routingComponents,
    SavedListComponent,
    SavedSearchComponent,
    SaveListModalComponent,
    SaveListShareModalComponent,
    SaveSearchModalComponent,
    SortingComponent,
    DeleteObjectStatusComponent,
    AdminComponent,
    AmendDropdownComponent,
    EditDropdownValuesModalComponent,
    AddDropdownValuesModalComponent,
    AdminSavedList
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    AddObjectsItemComponent,
    AlertComponent,
    ConfirmComponent,
    CustomSortComponent,
    EditionModalComponent,
    MergeTwoComponent,
    ObjectModalComponent,
    DeleteObjectStatusComponent
  ],
  providers: [
    AddObjectToEventService,
    ConfigurationLoaderService,
    CustomSortComponent,
    DropuploadComponent,
    NewObjectDropUploadComponent,
    EditionModalComponent,
    FooterComponent,
    ObjectModalComponent,
    ObjectService,
    SavedListComponent,
    SavedSearchComponent,
    SavedListBackgroundService,
    SavedSearchService,
    SaveListModalComponent,
    SaveListShareModalComponent,
    SaveSearchModalComponent,
    TosatService,
    DeleteObjectStatusComponent,
    EditDropdownValuesModalComponent,
    AddDropdownValuesModalComponent,
    AdminSavedList,
    RoleGuardService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [ConfigurationLoaderService], multi: true },
  ],
})
export class AppModule { }



export function init_app(appLoadService: ConfigurationLoaderService) {
  return () => appLoadService.initializeApp();
}
