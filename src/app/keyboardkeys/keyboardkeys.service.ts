import { Injectable } from '@angular/core';

@Injectable()
export class KeyboardkeysService {

  constructor() { }
	//get Artist data based on query parameter
	getKeyboardResponse(e:any){
		//Previous record � Left arrow
		if (e.keyCode===37){
		  //e.preventDefault();
		  return 'prev';
		}
		//Next record � Right arrow
		if (e.keyCode===39){
		  //e.preventDefault();
		  return 'next';
		}
		//New � Ctrl + Alt + N
		if ((navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey) && e.altKey && e.keyCode===78){
		  //e.preventDefault();
		  return 'add';
		}
		//Save � Ctrl + Alt + S
		if ((navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey) && e.altKey && e.keyCode===83){
		  //e.preventDefault();
		  return 'addRecord';
		}
		//New Find � Ctrl + Alt + C � Only available in search mode
		if ((navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey) && e.altKey && e.keyCode===67){
		  //e.preventDefault();
		  return 'find';
		}
		//Amend Find � Ctrl + Alt + F � Available in all modes.
		if ((navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey) && e.altKey && e.keyCode===70){
		  //e.preventDefault();
		  return 'amendFind';
		}
		//Save Find � Ctrl + Alt + K � Only available in result mode
		if ((navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey) && e.altKey && e.keyCode===75){
		  //e.preventDefault();
		  return 'save';
		}
		//Add single record to list � Ctrl + Alt + R � only available in result mode.
		if ((navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey) && e.altKey && e.keyCode===82){
		  //e.preventDefault();
		  return 'addSingleRecord';
		}
		//Add all record to list � Ctrl + Alt + A � only available in result mode.
		if ((navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey) && e.altKey && e.keyCode===65){
		  //e.preventDefault();
		  return 'addAllRecord';
		}
		//Thumbnail view � Ctrl + Alt + T � only available on object screen.
		if ((navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey) && e.altKey && e.keyCode===84){
		  //e.preventDefault();
		  return 'thumbnailView';
		}
		//Home � Ctrl + Alt + H � available on all screens and all modes
		if ((navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey) && e.altKey && e.keyCode===72){
		  //e.preventDefault();
		  return 'homeView';
		}
		//Get out of edit mode without saving � Ctrl + Alt + Z ---available on all screens in edit mode.
		if ((navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey) && e.altKey && e.keyCode===90){
		  //e.preventDefault();
		  return 'editMode';
		}
		//Perform Search � Enter � available only on search mode
		if (e.keyCode === 13){
		  //e.preventDefault();
		  return 'enter';
		}
		//Main Object view � Ctrl + Alt + M � available on all screens and all modes.
		if ((navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey) && e.altKey && e.keyCode===77){
		  //e.preventDefault();
		  return 'objectView';
		}
		//Duplicate Window � Ctrl + Alt + W � available on all screens and all modes.
		if ((navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey) && e.altKey && e.keyCode===87){
		  //e.preventDefault();
		  return 'duplicateWindow';
		}
	}
}
