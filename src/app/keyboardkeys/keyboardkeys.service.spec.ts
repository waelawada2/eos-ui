import { TestBed, inject } from '@angular/core/testing';

import { KeyboardkeysService } from './keyboardkeys.service';

describe('KeyboardkeysService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KeyboardkeysService]
    });
  });

  it('should be created', inject([KeyboardkeysService], (service: KeyboardkeysService) => {
    expect(service).toBeTruthy();
  }));
});
