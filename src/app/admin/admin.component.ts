import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from '../../app/header/header.service';
import { KeyboardkeysService } from '../../app/keyboardkeys/keyboardkeys.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],
  providers: [ HeaderService, KeyboardkeysService ]
})

export class AdminComponent implements OnInit {
  public keycloak = window['keycloak'];
  
  constructor(private router: Router,
              private _keyboardkeysService: KeyboardkeysService,
              private _headerService: HeaderService) { }

  ngOnInit() {}
  
  // Header Drop-down toggle mode
	toggleFlag: number;
	toggleFunction(flag: number) {
		this.toggleFlag = this._headerService.headerToggle((flag===this.toggleFlag)?0:flag);
  }
  
  redirect(page) {
    switch(page){
      case 'home':
      this.router.navigateByUrl('/');
      break;

      case 'admin':
      this.router.navigateByUrl('/admin');
      break;

      case 'importexport':
      this.router.navigate(['/importexport']);
      break;

      case 'amend':
      this.router.navigate(['/amend-dropdown']);
      break;

      case 'lists':
      this.router.navigate(['/adminsavedList']);
      break;

      default: 
      this.router.navigateByUrl('/admin');
    }
  }
  // Keyboard shortcuts
	keyboardShortCutAdminFunction(e) {
    switch(this._keyboardkeysService.getKeyboardResponse(e)){
      case 'duplicateWindow':
      let urlP = window.location.href;
      window.open(urlP, '_blank');
      break;
    }
  }
}