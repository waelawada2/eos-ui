import { AfterViewInit, Component, HostListener, OnDestroy, AfterContentInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BlockScreenService } from './event/shared/services/block-screen/block-screen.service'
import { Subscription } from 'rxjs/Subscription';
import { ViewEncapsulation } from '@angular/core';
import { DialogService } from 'ng2-bootstrap-modal';
import { AlertComponent } from './common/alert-component';
import { environment } from '../environments/environment';
import { SavedListBackgroundService } from './SavedlListBackground/savedListBackground.service';
import { TosatService } from './common/toasty-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', './app.component.css'],
  providers: [BlockScreenService],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnDestroy, AfterContentInit, AfterViewInit {

  private blockerSubs: Subscription;
  public isLocked = false;
  private _keycloak: any = window['keycloak'];
  private _session: any;
  private _popup: any;
  private _sessionTime: any = `${environment.sessionTime.sessionTime}`;
  private _popupTime: any = `${environment.sessionTime.popupTime}`;
  public showProgressBar = false;
  public progressBarNo = 0;
  public progressBarPercentage = 0;
  public totalRecords = 0;

  subscription: Subscription;
  removeProgress: Subscription;

  constructor(
    private blockScreen: BlockScreenService,
    private _dialogService: DialogService,
    private _savedListBackgroundService: SavedListBackgroundService,
    public tosatservice: TosatService,
  ) {
    this.subscription = _savedListBackgroundService.itemAdded$.subscribe(
      event => {
        this.showProgressBar = true;
        this.progressBarNo = event.valuesadded;
        this.progressBarPercentage = event.percentage;
        this.totalRecords = event.totalRecords;
      }
    )

    this.removeProgress = _savedListBackgroundService.removeProgress$.subscribe(
      event => {
        this.showProgressBar = false;
      }
    )
  }

  public resetTimer() {

    clearTimeout(this._session);
    clearTimeout(this._popup);

    this._popup = setTimeout(() => {
      this._dialogService.addDialog(AlertComponent,
        { title: 'Session Timeout', message: "Your session will logout in 5 minutes,please click Ok to continue" }, { backdropColor: 'rgba(220,220,220,0.5)' })
        .subscribe((isConfirmed) => {
          this.resetTimer();
        });
    }, this._popupTime);

    this._session = setTimeout(() => {
      this._keycloak.logout();
    }, this._sessionTime);
  }

  callResetTimer() {
    this.resetTimer();
  }

  ngAfterViewInit() {
    this.resetTimer();
  }

  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.subscription.unsubscribe();
    this.removeProgress.unsubscribe();
    this.blockerSubs.unsubscribe();
  }

  ngAfterContentInit() {
    this.blockerSubs = this.blockScreen.blocker.skip(1).subscribe((lock) => {
      this.isLocked = lock;
    })
  }
}
