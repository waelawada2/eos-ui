import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ConfigurationLoaderService } from 'app/configuration.service'

@Injectable()
export class ArtistService {
  private _url: string;

  constructor(private _http: Http, configuration: ConfigurationLoaderService) {
    this._url = configuration.getSettings().apiUrl

  }

  //get Artist data based on query parameter
  getArtist(body: Object) {
    return this._http.get(this._url + "/artists?" + body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          document.getElementsByTagName("html")[0].classList.remove("loading");
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  getObservableArtist(query: Object) {
    return this._http.get(this._url + '/artists?' + query);
  }

  updateArtistCatalogueRaisonees(body: Object, id: string) {
    return this._http.put(this._url + `/artists/` + id + `/catalogues-raisonees/` + body['id'], body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  // get individual Artist
  getIndividualArtist(body: Object) {
    return this._http.get(this._url + "/artists/" + body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          document.getElementsByTagName("html")[0].classList.remove("loading");
          throw new Error('This request has failed ' + response.status)
        } else {
          document.getElementsByTagName("html")[0].classList.remove("loading");
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  // Add a new Artist
  addArtist(body: Object, flag: boolean) {
    return this._http.post(this._url + "/artists?validArtist=" + flag, body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          document.getElementsByTagName("html")[0].classList.remove("loading");
          throw new Error('This request has failed ' + response.status)
        } else {
          document.getElementsByTagName("html")[0].classList.remove("loading");
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  // Update a Artist
  updateArtist(artistId: string, body: Object) {
    return this._http.put(this._url + `/artists/${artistId}`, body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          document.getElementsByTagName("html")[0].classList.remove("loading");
          throw new Error('This request has failed ' + response.status)
        } else {
          document.getElementsByTagName("html")[0].classList.remove("loading");
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  // Delete a Artist
  removeArtist(id: string) {
    return this._http.delete(this._url + `/artists/${id}`)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  // add Artist CatalogueRaisonees
  addArtistCatalogueRaisonees(body: Object, id: string) {
    return this._http.post(this._url + `/artists/` + id + `/catalogues-raisonees`, body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  // remove Artist CatalogueRaisonees
  removeArtistCatalogueRaisonees(artistId: string, catalogueRaisId: string) {
    return this._http.delete(this._url + `/artists/` + artistId + `/catalogues-raisonees/` + catalogueRaisId)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  _errorHandler(_error: Response) {
    document.getElementsByTagName("html")[0].classList.remove("loading");
    return Observable.throw(_error || "server error 404");
  }
}
