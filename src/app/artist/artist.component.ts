import { Component, OnInit, ElementRef, Input, EventEmitter, ViewChild, Output, HostListener } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ArtistService } from './artist.service';
import { FooterService, FooterUser } from '../../app/footer/footer.service';
import { KeyboardkeysService } from '../../app/keyboardkeys/keyboardkeys.service';
import { PaginationService } from '../../app/pagination/pagination.service';
import { HeaderService } from '../../app/header/header.service';
import { AppService } from '../../app/app.service';
import {TosatService} from '../common/toasty-service';
import {ConfirmComponent} from '../common/confirm-component';
import { DialogService } from "ng2-bootstrap-modal";
import {ObjectService} from '../object/object.service'
import {AlertComponent} from '../common/alert-component'
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";
import 'rxjs/Rx';
import { Helper } from '../utils/helper';
import { SavedSearchComponent} from '../saved-search/saved-search.component';
import { SaveSearchModalComponent } from '../saved-search/saved-search-modal.component';
import { SavedSearchService } from '../saved-search/saved-search.service';
import { ModalModule } from 'ngx-modal';

@Component({
  //selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css'],
  providers: [AppService, ArtistService, KeyboardkeysService, PaginationService, SaveSearchModalComponent, Helper, SavedSearchComponent, HeaderService, FooterService ]
})
export class ArtistComponent implements OnInit {
  public keycloak = window['keycloak'];
  public confirmResult:boolean = null;
  promptMessage:string = '';
  public column: NodeListOf<Element> = document.getElementsByClassName('column');
  public navbar: NodeListOf<Element> = document.getElementsByClassName('navbar');
  public columnHeight: any;
  public fName = [];
  public lName = [];
  public nCountry = [];
  public nationalityAll = [];
  public categoryAll = [];
  public temp;
  public artistDetailsForm: FormGroup;
  public selected = [];
  public activeArtist;
  public queryParam;
  public errorMessage: string;
  public updatedCRArray;

  //Search, Edit, Save Mode flag
  public editModeFlag = false;
  public searchMode = false;
public headerFlag = 'search';

//Error message flag
public requiredFlag = false;
public confirmFlag = false;

  //pagination
  public current_page = 1;
  public total_pages = 0;
  public curPageValue = 1;
  public pageNo;
  public pageNumber=0;
  public pageData = [];
  public emptyPagination = '';
  public widthStyle: number = 85;
  public CRChanged : boolean = false;
  public CRupdatedindex = 0;
  private showProjects: boolean = true;

  // Gender selection toggle flag
  private genderMToggleFlag = 0;
  private genderFToggleFlag = 0;
  
  private firstNameFound = false;
  private lastNameFound = false;

  // Access to 'Save Search' Modal
@ViewChild(SaveSearchModalComponent) public saveSearchModal: SaveSearchModalComponent;
@ViewChild ('firstNameList') firstNameList: ElementRef;
@ViewChild ('lastNameList') lastNameList: ElementRef;


  //constructor method
  constructor(private dialogService:DialogService,
      private tosatservice:TosatService,
      private builder: FormBuilder,
      private _sanitizer: DomSanitizer,
      private http: Http,
      private el: ElementRef,
      private route: ActivatedRoute,
      private _appService: AppService,
    private _keyboardkeysService: KeyboardkeysService,
    private _paginationService: PaginationService,
    private _headerService:  HeaderService,
      private _footerService: FooterService,
      private _objectService:ObjectService,
      private _artistService: ArtistService,
      private router: Router,
      private _savedSearchModalComponent: SaveSearchModalComponent,
      private _savedSearchService: SavedSearchService
  ) {
    this._appService
        .getCountries()
        .subscribe( Response => {
              this.nationalityAll = Response;
              this.nCountry.splice(0, this.nCountry && this.nCountry.length);
              for (var i = 0; i < this.nationalityAll.length; i++) {
                if(this.nationalityAll[i]&&this.nationalityAll[i].countryName){
                  this.nCountry.push(this.nationalityAll[i].countryName);
                }
              }
              this.nCountry.sort()
          }, resFileError => this.errorMsg = resFileError);
      this._appService
          .getCategories()
          .subscribe(Response => {
              this.categoryAll = Response;
          }, resFileError => this.errorMsg = resFileError);
      setTimeout(() => {
        if(window.location.search){
              // Navigate to 'Saved Search' results.
              if(window.location.search && window.location.search.indexOf("savesearchshareid=")>-1) {
                let savedSearchID;
                let searchCriterias;
                let userName = this.keycloak.tokenParsed.preferred_username;
                  // Get 'Saved Search ID'.
                  if(window.location.search.split("savesearchshareid=")[1]){
                    savedSearchID = window.location.search.split("savesearchshareid=")[1];
                  }

                  if(savedSearchID) {
                    // Get 'Saved Search' query params.
                    this._savedSearchService.getSharedSavedSearchById(savedSearchID)
                    .subscribe( savedSearch => {
                      searchCriterias = savedSearch.searchCriterias;
                      let query = '';
                      for (let i in searchCriterias) {
                        query+= searchCriterias[i].paramName+"="+searchCriterias[i].paramValue+"&";
                      }
                      query = query.charAt(query.length-1)=='&'?query.substr(0, query.length-1): query;
                      // Find 'Artists' matching the 'Saved Search' criteria.
                      this.nameChange('searchMode', query);
                    }, resFileError => this.errorMessage = resFileError);
                  }
              }
              else if(window.location.search && window.location.search.indexOf("savesearch=")>-1) {
                  // Get 'Saved Search ID'.
                  if(window.location.search.split("savesearch=")[1]){
                    let saveSearchID = (window.location.search.split("savesearch=")[1]).split("&")[0];
                  }
                  // Get query params related to the selected 'Saved Search'.
                  let query = window.location.search.substr(window.location.search.indexOf("&"),window.location.search.length);
                  // Perform search based on saved search criteria.
                  this.nameChange('searchMode', query)
              }
              else {
                if(window.location.search&&window.location.search.indexOf("id=")>-1){
                  this.queryParam = window.location.search.split('id=');
                  this.queryParam = { id: this.queryParam[1] };
				          this.total_pages = 1;
                  this.nameChange('singleArtist', this.queryParam);
                }
              }
          }
      }, 500);
  }

  // country change function
  countryValChange(countryVal) {
      if(this.nationalityAll.length<1){
        this.loadCountries(countryVal);
      }
      if (countryVal && this.nationalityAll.length>0) {
          this.temp = this.nationalityAll.filter(function(el) {
              return el.countryName == countryVal;
          });
          this.selected.push(this.temp[0]);
          this.artistDetailsForm.controls['countries'].setValue("");
          this.nCountry = this.nCountry.filter(function(el) {
              return el !== countryVal;
          });
          this.nCountry.sort();
      }
  }

  //load all countries
  loadCountries(countryValue){
      this._appService
        .getCountries()
        .subscribe( Response => {
              this.nationalityAll = Response;
              this.nCountry.splice(0, this.nCountry && this.nCountry.length);
              for (var i = 0; i < this.nationalityAll.length; i++) {
                if(this.nationalityAll[i]&&this.nationalityAll[i].countryName){
                  this.nCountry.push(this.nationalityAll[i].countryName);
                }
              }
              this.nCountry.sort();
              this.countryValChange(countryValue);
          }, resFileError => this.errorMsg = resFileError);
  }

  //load categories
  loadCategories(flagCat,catValue){
    this._appService
          .getCategories()
          .subscribe(Response => {
              this.categoryAll = Response;
              this.nameChange(flagCat,catValue);
          }, resFileError => this.errorMsg = resFileError);
  }

  // remove the country function
  removeCountry(item) {
      this.nCountry.push(item.countryName);
      this.nCountry.sort();
      this.selected.splice(this.selected.indexOf(item), 1);
  }

  // by default intialization/called function
  ngOnInit() {
    this.showProjects = true;
      // Dynamically set column height to span to the height of the browser window
    //this.columnHeight = document.body.clientHeight - this.navbar[0].clientHeight - 20;
      this.artistDetailsForm = this.builder.group({
          firstName: new FormControl(""),
          lastName: new FormControl("",[Validators.required]),
          gender: new FormControl(""),
          birthYear: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
          deathYear: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
          display: new FormControl(""),
          countries: new FormControl(""),
          artistCatalogueRaisonees: this.builder.array([]),
          notes: new FormControl("")
      })
      this.updatedCRArray = JSON.parse(JSON.stringify(this.artistDetailsForm.value.artistCatalogueRaisonees));
  }
  public errorMsg;

  // Header Drop-down toggle mode
toggleFlag: number;
toggleFunction(flag: number) {
  this.toggleFlag = this._headerService.headerToggle((flag===this.toggleFlag)?0:flag);
}

  //firstname/lastname autocomplete function
  nameListFormatter = (data: any) => {
      let html = `<span>${data}</span>`;
      return this._sanitizer.bypassSecurityTrustHtml(html);
  }
  public categories;
  public selectedCategories = [];
  public nameFList:boolean = false;
  public nameLList:boolean = false;

  //CreatedBy and UpdateBy userName
  userName: FooterUser[];
  public footerName: FooterUser;
  @Output() changed = new EventEmitter<FooterUser>();

  //firstname/lastname value changed
  nameChange(flag, value: any) {
      if ((flag != "singleArtist") && value && value.length > 0) {
    value = (flag==="firstName")?("firstName="+value):value;
          value = (flag==="lastName")?("lastName="+value):value;
          this._artistService
              .getArtist(value + '&lod=MAX')
              .subscribe(Response => {
                  if (flag == "firstName") {
                      this.fName.splice(0, this.fName.length);
                      for (var i = 0; i < Response.content.length; i++) {
                          this.fName.push(Response.content[i].firstName);
                      }
                      this.nameFList = this.fName.length>10?true:false;
                      Response.content.length>0?this.firstNameFound=true:this.firstNameFound=false;
                  } else if (flag == "lastName") {
                      this.lName.splice(0, this.lName.length);
                      for (let lNameVal of Response.content) {
                          this.lName.push(lNameVal.lastName);
                      }
                      this.nameLList = this.fName.length>10?true:false;
                      Response.content.length>0?this.lastNameFound=true:this.lastNameFound=false;
                  } else {
                      if(flag == "pagination"){
                          value = parseInt(value)-1;
                          value = (this.curPageValue-1)%20;
                      }
                      else {
                          value = 0;
                      }
                      this.total_pages = Response.totalElements;
                      this.pageData = Response.content;
                      if (Response.content.length > 0) {
                          this.ngOnInit();
                          this.queryParam = { id : Response.content[value].id};
                          this.nameChange('singleArtist', this.queryParam);
                      } else{
                        this.defaultMode();
                        this.emptyPagination = 'No records found';
                        this.amendSearch();
                        this.widthStyle = 115;
                      }

                  }
              }, resFileError => {
                this.errorMsg = resFileError;
                if(flag === 'firstName') {
                  this.firstNameFound = false;
                }
                else if(flag === 'lastName') {
                  this.lastNameFound = false;
                }
              });
              document.getElementsByTagName( "html" )[0].classList.remove( "loading" );
      }

      // get individual record from BE
      if(flag == "singleArtist"){
            this.ngOnInit();
            this.searchMode = true;
            this.indexCRValue =[];
            this.editModeFlag = true;
            this._artistService
                .getIndividualArtist(value.id)
                .subscribe(Response => {
                    this.selected = [];
                    this.headerFlag = 'result';
                    this.requiredFlag = true;
                    Response.content = Response;
                    this.activeArtist = Response.content.id;
                    this.selectedCategories = [];
                    window.history.replaceState(null, null,"artist?id="+this.activeArtist);
                    this.artistDetailsForm.controls['firstName'].setValue(Response.content.firstName);
                    this.artistDetailsForm.controls['lastName'].setValue(Response.content.lastName);
                    if(Response.content&& Response.content.gender==='male'){
                    Response.content.gender = "Male";
                    }
                    if(Response.content&& Response.content.gender==='female'){
                    Response.content.gender = "Female";
                    }
                    this.artistDetailsForm.controls['gender'].setValue(Response.content.gender);
                    this.artistDetailsForm.controls['birthYear'].setValue(Response.content.birthYear);
                    this.artistDetailsForm.controls['deathYear'].setValue(Response.content.deathYear);
                    this.artistDetailsForm.controls['display'].setValue(Response.content.display);
                    this.artistDetailsForm.controls['countries'].setValue("");
                    this.artistDetailsForm.controls['notes'].setValue(Response.content.notes);
                    Response.content.artistCatalogueRaisonees.forEach(x => {
                        const control = < FormArray > this.artistDetailsForm.controls['artistCatalogueRaisonees'];
                        control.push(this.pushArtistCatalogueRaisonees(x));
                    });      
                    this.artistDetailsForm.controls.artistCatalogueRaisonees['controls'] = this.sort(this.artistDetailsForm.controls.artistCatalogueRaisonees['controls']);
                    this.updatedCRArray = JSON.parse(JSON.stringify(this.artistDetailsForm.value.artistCatalogueRaisonees));                    
                    Response.content.countries.forEach(x => {
                        this.countryValChange(x.countryName);
                    });
                    this.categories = Response.content.categories;
                    if(this.categoryAll.length < 1){
                      this.loadCategories(flag,value);
                    }
                    for (var i = 0; i < this.categoryAll.length; i++) {
                        var flags = this.categoryAll[i].id;
                        flags = this.categories.filter(function(el) { return flags == el.id; });
                        this.categoryAll[i].status = flags.length ? true : false;
                        if (flags.length) {
                            this.selectedCategories.push({
                                id: this.categoryAll[i].id,
                                description: this.categoryAll[i].description,
                                name: this.categoryAll[i].name
                            });
                        }
                    }

                    var footerDetails;
                    var createdByFName, createdByLName, updatedByFName, updatedByLName;
                    if(Response.content && Response.content.createdBy){
                      createdByFName = Response.content.createdBy.firstName?Response.content.createdBy.firstName:'';
                      createdByLName = Response.content.createdBy.lastName?Response.content.createdBy.lastName:'';
                    }
                    if(Response.content && Response.content.updatedBy){
                      updatedByFName = Response.content.updatedBy.firstName?Response.content.updatedBy.firstName:'';
                      updatedByLName = Response.content.updatedBy.lastName?Response.content.updatedBy.lastName:'';
                    }
                    this.footerName = {
                      createdDate: Response.content.createdDate?Response.content.createdDate:null,
                      createdBy: createdByFName && createdByLName? createdByFName+" "+createdByLName: null,
                      updatedDate: Response.content.updatedDate?Response.content.updatedDate:null,
                      updatedBy: updatedByFName && updatedByLName?updatedByFName+" "+updatedByLName:null
                    }
                    this.changed.emit(this.footerName);
                }, resFileError =>{
					this.errorMsg = resFileError;
					if(resFileError.status===404){
            this.defaultMode();
            this.emptyPagination = 'No record found';
            this.amendSearch();
            this.widthStyle = 115;
					}
				});
      }
  }

  
	sort(arr){
		var len = arr.length;
		for (var i = len-1; i>=0; i--){
		  for(var j = 1; j<=i; j++){
			if(arr[j-1]['controls']['sortId']._value > arr[j]['controls']['sortId']._value){
				const control = < FormArray > arr[j-1];
				arr[j-1] = arr[j];
				arr[j] = control;
			 }
		  }
		}
		return arr;
	 }

  // showing the autocomplte functionalilty based on condition country, firstname and lastname
  autocompleListFormatter = (data: any) => {
      let html = `<span>${data.name} ${data.id} </span>`;
      return this._sanitizer.bypassSecurityTrustHtml(html);
  }

  // clear the form value
  defaultMode(){
    if(this.headerFlag == "edit" && !this.confirmFlag){
      this.showConfirm('defaultMode', this.artistDetailsForm.value);
    }
    else{
        this.confirmFlag = false;
        this.artistDetailsForm.controls['artistCatalogueRaisonees'] = this.builder.array([]);
        this.artistDetailsForm.controls['firstName'].setValue("");
        this.artistDetailsForm.controls['lastName'].setValue("");
        this.artistDetailsForm.controls['gender'].setValue("");
        this.artistDetailsForm.controls['birthYear'].setValue("");
        this.artistDetailsForm.controls['deathYear'].setValue("");
        this.artistDetailsForm.controls['display'].setValue("");
        this.artistDetailsForm.controls['countries'].setValue("");
        this.editModeFlag = false;
        this.searchMode = false;
        this.headerFlag = 'search';
    this.emptyPagination = '';
        this.selected.forEach(data => {
          this.nCountry.push(data.countryName);
        })
        this.nCountry.sort();
        this.selected = [];
        this.selectedCategories= [];
        this.pageData = [];
        this.pageNo = 0;
        this.pageNumber= 0;
        this.pageData = [];
        this.current_page = 1;
        this.total_pages = 0;
        this.curPageValue = 1;
        this.activeArtist = null;
        this.requiredFlag = false;
        for (var i = 0; i < this.categoryAll.length; i++) {
          this.categoryAll[i].status = false;
        }
      window.history.replaceState(null, null,"artist");
    }
  }

  onChangeFunction(event) {
    if(event && this.activeArtist) {
      // Radio & checkbox
       if(typeof(event)!='string' && (event.target.type=='radio' || event.target.type=='checkbox')) {
        this.headerFlag = this.activeArtist?'edit':this.headerFlag;
      }
      // Nationality
      else if((typeof(event)==='string') && event!="") {
        this.headerFlag = this.activeArtist?'edit':this.headerFlag;
      }
      // Check if 'left', 'right' arrow keys
      else if(event.which!=18 && event.which!=20 && event.which!=33 && event.which!=34 && event.which!=35 && event.which!=36 && event.which!=37 && event.which!=39 && event.which!=45 && event.keyCode!=17&&event.keyCode!=27&&event.keyCode!=145&&event.keyCode!=91&&event.keyCode!=93) {
        if(event===null) {
          this.headerFlag = this.activeArtist?'edit':this.headerFlag;
        }
        // Number field
        else if (event.target.type=='number' && event.which!== 0 && !event.ctrlKey && !event.metaKey && !event.altKey
        && event.keyCode!=9 && event.which!=13 && event.keyCode!=16 && ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode==8 || event.keyCode==46 || event.type=='change')) {
          this.headerFlag = this.activeArtist?'edit':this.headerFlag;
        }
      }
    }
	}

  // initial the Artist CatalogueRaisonees
  initArtistCatalogueRaisonees(sortnumber) {
      return this.builder.group({
          author: [''],
          year: ['',Validators.pattern('^[1-9][0-9]{3}$')],
          type: [''],
          volumes: [''],
          id: [null],
          sortId: [sortnumber]
      });
  }

  // push the Artist CatalogueRaisonees
  pushArtistCatalogueRaisonees(res) {
      return this.builder.group({
          author: [(res&&res.author)?res.author:''],
          year: [(res&&res.year)?res.year:'',Validators.pattern('^[1-9][0-9]{3}$')],
          type: [(res&&res.type)?res.type:''],
          volumes: [(res&&res.volumes)?res.volumes:''],
          id: [(res&&res.id)?res.id:null],
          sortId: [(res&&res.sortId)?res.sortId:'']
      });
  }

  // add the Artist CatalogueRaisonees
  public sortCode : number;
  addArtistCatalogueRaisonees() {
      const control = < FormArray > this.artistDetailsForm.controls['artistCatalogueRaisonees'];
      var tempCRArray = control.controls;
      var tempSortArray = [];
      tempCRArray.map(function(item){
        if(item.value.sortId !== "")
          tempSortArray.push(item.value.sortId);
      });
      this.sortCode = Math.max.apply(null, tempSortArray);
      if(this.sortCode == 0 || this.sortCode == -Infinity){
        control.push(this.initArtistCatalogueRaisonees(control.length+1));      
      } else {
        control.push(this.initArtistCatalogueRaisonees(this.sortCode+1));      
      }
      var CRtimeout =  setTimeout(function(){
        var elem = document.getElementById('CRbody');
        elem.scrollTop = elem.scrollHeight - elem.clientHeight;
        clearTimeout(CRtimeout);
      }, 10)
  }

  // remove the Artist CatalogueRaisonees
  removeArtistCatalogueRaisonee(i: number, value: any) {
      if(value.id){
        const control = < FormArray > this.artistDetailsForm.controls['artistCatalogueRaisonees'];
        this._artistService
        .removeArtistCatalogueRaisonees(this.activeArtist,value.id)
        .subscribe(Response => {
          control.removeAt(i);
          this.CRChanged = true;
          this.onChangeFunctionCR();
          var sinArtist = {id: this.activeArtist};
          this.nameChange('singleArtist',sinArtist)
        }, resFileError =>{
          this.errorMsg = resFileError;
          if(this.errorMsg.status == 409){
			document.getElementsByTagName("body")[0].classList.add("positionFixed");
            this.dialogService.addDialog(AlertComponent,
             {title:'',message:'Selected artist CR already assigned with property CR. So It cannot be deleted.'})
			 .subscribe((isConfirmed)=>{document.getElementsByTagName("body")[0].classList.remove("positionFixed");});
          }
        } );
      }else{
        const control = < FormArray > this.artistDetailsForm.controls['artistCatalogueRaisonees'];
        control.removeAt(i);
      }
  }


  //displayName value based on condtion firstName and lastName value
   displayName(flag, value: any) {
    if(this.headerFlag!="search"){
      if (flag == "displayFName") {
          if( !(value || this.artistDetailsForm.value.lastName) && (!this.artistDetailsForm.value.birthYear || !this.artistDetailsForm.value.deathYear)){
            this.artistDetailsForm.controls['display'].setValue("");
          } else if( (value || this.artistDetailsForm.value.lastName) && this.artistDetailsForm.value.birthYear && this.artistDetailsForm.value.deathYear){
            this.artistDetailsForm.controls['display'].setValue((value ? value + " " : "") + (this.artistDetailsForm.value.lastName ? this.artistDetailsForm.value.lastName : "") + (this.artistDetailsForm.value.birthYear ? " (" + this.artistDetailsForm.value.birthYear + "-" : "") + (this.artistDetailsForm.value.deathYear ? this.artistDetailsForm.value.deathYear + ")" : ""));
          } else if( (value || this.artistDetailsForm.value.lastName) && this.artistDetailsForm.value.birthYear){
        	this.artistDetailsForm.controls['display'].setValue((value ? value + " " : "") + (this.artistDetailsForm.value.lastName ? this.artistDetailsForm.value.lastName + " " : "") + (this.artistDetailsForm.value.birthYear ? "( B. " + this.artistDetailsForm.value.birthYear + " ) " : ""));
          } else{
            this.artistDetailsForm.controls['display'].setValue((value? value + " " : "") + (this.artistDetailsForm.value.lastName ? this.artistDetailsForm.value.lastName + " " : ""));
          }
      } else if (flag == "displayLName") {
          if( !(value || this.artistDetailsForm.value.firstName) && (!this.artistDetailsForm.value.birthYear || !this.artistDetailsForm.value.deathYear)){
            this.artistDetailsForm.controls['display'].setValue("");
          } else if( (value || this.artistDetailsForm.value.firstName) && this.artistDetailsForm.value.birthYear && this.artistDetailsForm.value.deathYear){
            this.artistDetailsForm.controls['display'].setValue((this.artistDetailsForm.value.firstName ? this.artistDetailsForm.value.firstName + " " : "") + (value ? value + " " : "") + (this.artistDetailsForm.value.birthYear ? " (" + this.artistDetailsForm.value.birthYear + "-" : "") + (this.artistDetailsForm.value.deathYear ? this.artistDetailsForm.value.deathYear + ")" : ""));
          } else if( (value || this.artistDetailsForm.value.firstName) && this.artistDetailsForm.value.birthYear){
        	this.artistDetailsForm.controls['display'].setValue((this.artistDetailsForm.value.firstName ? this.artistDetailsForm.value.firstName + " " : "") + (value ? value + " " : "") + (this.artistDetailsForm.value.birthYear ? "( B. " + this.artistDetailsForm.value.birthYear + " ) " : ""));
          } else{
            this.artistDetailsForm.controls['display'].setValue((this.artistDetailsForm.value.firstName ? this.artistDetailsForm.value.firstName + " " : "") + (value ? value + " " : ""));
          }
      } else if ( flag == "displayBYear") {
        if((this.artistDetailsForm.value.firstName || this.artistDetailsForm.value.lastName) && this.artistDetailsForm.value.deathYear && value){
          this.artistDetailsForm.controls['display'].setValue((this.artistDetailsForm.value.firstName ? this.artistDetailsForm.value.firstName + " " : "") + (this.artistDetailsForm.value.lastName ? this.artistDetailsForm.value.lastName + " " : "") + (value ? " (" + value + "-" : "") + (this.artistDetailsForm.value.deathYear ? this.artistDetailsForm.value.deathYear + ")" : ""));
        } else if((this.artistDetailsForm.value.firstName || this.artistDetailsForm.value.lastName) && value){
        	this.artistDetailsForm.controls['display'].setValue((this.artistDetailsForm.value.firstName ? this.artistDetailsForm.value.firstName + " " : "") + (this.artistDetailsForm.value.lastName ? this.artistDetailsForm.value.lastName + " " : "") + (value ? "( B. " + value + " ) " : ""));
        } else{
          this.artistDetailsForm.controls['display'].setValue((this.artistDetailsForm.value.firstName ? this.artistDetailsForm.value.firstName + " " : "") + (this.artistDetailsForm.value.lastName ? this.artistDetailsForm.value.lastName + " " : ""));
        }
      } else if ( flag == "displayDYear") {
        if( (this.artistDetailsForm.value.firstName || this.artistDetailsForm.value.lastName) && this.artistDetailsForm.value.birthYear && value){
          this.artistDetailsForm.controls['display'].setValue((this.artistDetailsForm.value.firstName ? this.artistDetailsForm.value.firstName + " " : "") + "" + (this.artistDetailsForm.value.lastName ? this.artistDetailsForm.value.lastName : "") + "" + (this.artistDetailsForm.value.birthYear ? " (" + this.artistDetailsForm.value.birthYear + "-" : "") + "" + (value ? value + ")" : ""));
        } else if((this.artistDetailsForm.value.firstName || this.artistDetailsForm.value.lastName) && this.artistDetailsForm.value.birthYear){
        	this.artistDetailsForm.controls['display'].setValue((this.artistDetailsForm.value.firstName ? this.artistDetailsForm.value.firstName + " " : "") + (this.artistDetailsForm.value.lastName ? this.artistDetailsForm.value.lastName + " " : "") + (this.artistDetailsForm.value.birthYear ? "( B. " + this.artistDetailsForm.value.birthYear + " ) " : ""));
        } else{
          this.artistDetailsForm.controls['display'].setValue((this.artistDetailsForm.value.firstName ? this.artistDetailsForm.value.firstName + " " : "") + (this.artistDetailsForm.value.lastName ? this.artistDetailsForm.value.lastName + " " : ""));
        }
      }
    }
  }

  //reset full form data
  resetFullForm() {
    this.selected.forEach(data => {
      this.nCountry.push(data.countryName);
    })
    this.nCountry.sort();
    this.selected = [];
    this.artistDetailsForm.controls['artistCatalogueRaisonees'] = this.builder.array([]);
    this.artistDetailsForm.reset();
    this.selected = [];
    this.activeArtist = null;
    for (var i = 0; i < this.categoryAll.length; i++) {
      this.categoryAll[i].status = false;
    }
    this.selectedCategories =[];
    this.footerName = {
      createdDate: null,
      createdBy: null,
      updatedDate: null,
      updatedBy: null
    }
    this.changed.emit(this.footerName);
  }

  // Artist project category change
  categoryChange(value: any, e) {
      if (e.target.checked) {
          value.status = true;
          var cateLength = this.selectedCategories.length;
          var flags = this.selectedCategories.filter(function(el) {
              return value.id != el.id
          });
          if (flags.length === cateLength) {
              this.selectedCategories.push({
                  id: value.id,
                  description: value.description,
                  name: value.name
              });
          }
      } else {
          this.selectedCategories = this.selectedCategories.filter(function(el) {
              return value.id != el.id
          });
      }
  }

  // create the query parameter function
  toQueryString(obj) {
      var parts = [];
      for (var i in obj) {
          if (obj.hasOwnProperty(i)) {
              if(encodeURIComponent(i)==='firstName' || encodeURIComponent(i)==='lastName' || encodeURIComponent(i)==='gender' || encodeURIComponent(i)==='display' || encodeURIComponent(i)==='birthYear' || encodeURIComponent(i)==='deathYear' ){
                if(encodeURIComponent(obj[i]) != 'undefined'){
                  parts.push(encodeURIComponent(i) + "=" + ((encodeURIComponent(obj[i]) != 'undefined')? encodeURIComponent(obj[i]) : ""));
                }
              }
          }
      }
      return parts.join("&");
  }

  public queryValue;
  public searchData;

  redirectPage(){
    if(this.headerFlag == "edit" && !this.confirmFlag){
      this.showConfirm('redirect', this.artistDetailsForm.value);
    } else if(this.headerFlag==="create"){
      this.createModeExit((exitFlag)=>{
        if(exitFlag){
          this.confirmFlag = false;
          this.router.navigateByUrl('/');
        }
      })
    }else{
      this.confirmFlag = false;
      this.router.navigateByUrl('/');
    }
  }

  
  deleteConfirm(data:any) {
    this.confirmFlag = false; 
      this._objectService.getProperties('&lod=MIN&artistIds='+this.activeArtist).subscribe(Response =>{
  document.getElementsByTagName("body")[0].classList.add("positionFixed");
      if(Response.content && Response.content.length>0){
          this.dialogService.addDialog(AlertComponent,
            {title:'',message:'Objects are still assigned to this artist, please reassign objects before deleting this artist'})
      .subscribe((isConfirmed)=>{
              document.getElementsByTagName("body")[0].classList.remove("positionFixed");
            });
      } else{
          this.dialogService.addDialog(ConfirmComponent, {title:'',message:'Are you sure you want to delete this artist?',okRequired:true})
            .subscribe((isConfirmed)=>{
        document.getElementsByTagName("body")[0].classList.remove("positionFixed");
              this.confirmResult = isConfirmed;
              if(this.confirmResult){
                this.onSubmit('deleteMode',data);
              }
            });
        }
    },(resFileError) => {});
 } 
 
 showConfirm(flag: string, value: any) {
	  document.getElementsByTagName("body")[0].classList.add("positionFixed");
      let disposable = this.dialogService.addDialog(ConfirmComponent, {
              title:'Confirm', 
      message: 'There are some unsaved changes made to this record, would you like to save them?'
    })
              .subscribe((isConfirmed)=>{
			      document.getElementsByTagName('body')[0].classList.remove('positionFixed');
                  this.confirmFlag = true;
                  if(isConfirmed) {
                    this.onSubmit('updateMode', this.artistDetailsForm.value);
                    this.confirmFlag = false;
                  }
                  else {
                    this.headerFlag == "search";
                    this.CRUpdated = false;
                    this.updatedCRindex = [];
                    this.updatedCRArray = [];
                    switch(flag){
                    case 'backMode':
                        this.backMode();
                    break;
                    case 'defaultMode':
                        this.backMode();
                    break;
                    case 'addMode':
                        this.flagMode('addMode');
                        this.headerFlag = "create";
                    break;
                    case 'redirect':
                        this.router.navigateByUrl('/');
                    break;
                    case 'pageFunction':
                        this.pageFunction(value);
                    break;
                    case 'deleteMode':
                        this.deleteConfirm(this.artistDetailsForm.value);
                    break;
                    case 'lastPage':
                      if(this.total_pages>0){
                        this.curPageValue = this.total_pages;
                        this.getCurrValue(this.total_pages-1,'lastPage');
                      }
                    break;
                    case 'firstPage':
                      if(this.total_pages>0){
                        this.getCurrValue(0,'firstPage');
                      }
                    break;
                    }
                  }
              });
              setTimeout(()=>{
                  disposable.unsubscribe();
              },100000);

  }
  

  // submit form : add, find, update and delete the functionality
  public RequestCRArray = [];
  onSubmit(flag: string, value: any) {
    if (flag === 'searchMode') {
          document.getElementsByTagName( "html" )[0].classList.add( "loading" );
          this.searchData = value;
          value.countries = this.selected;
          value.categories = this.selectedCategories;
          value.firstName = (value.firstName && value.firstName.trim())?value.firstName.trim():'undefined';
          value.lastName = (value.lastName && value.lastName.trim())?value.lastName.trim():'undefined';
          value.gender =value.gender?value.gender:'undefined';
          value.birthYear =value.birthYear?value.birthYear:'undefined';
          value.deathYear = value.deathYear?value.deathYear:'undefined';
          value.display = (value.display&&value.display.trim())?value.display.trim():'undefined';
          value.birthYear = value.birthYear?value.birthYear.toString():value.birthYear;
          value.deathYear = value.deathYear?value.deathYear.toString():value.deathYear;
          delete value.notes;
          this.queryValue = this.toQueryString(value);
          this.current_page = 1;
          this.total_pages = 0;
          this.curPageValue = 1;
          value.countries.forEach(data => {
              this.queryValue = this.queryValue.concat("&countryNames="+data.countryName);
          });
          value.categories.forEach(data => {
              this.queryValue = this.queryValue.concat("&categoryNames="+data.name);
          });
          
          var flagSearch = false, enterValid = true;
          for (var key in value) {
            if (value.hasOwnProperty(key)) {
              if(value[key]!='undefined' && value[key].length){
                flagSearch = true;
                if(((key==='birthYear' ||key==='deathYear')&&value[key]&&value[key].length!=4)){
                  enterValid = false;
                }
              }
            }
          }
          if(flagSearch){
            if(enterValid){
              this.current_page = 1;
              this.total_pages = 0;
              this.curPageValue = 1;
              this.searchMode = true;
              this.editModeFlag = true;
              // Session storage of 'search' parameters to create a 'saved search'.
              sessionStorage.removeItem('searchparam');
              sessionStorage.setItem("searchparam", this.queryValue);
              this.nameChange('searchMode', this.queryValue);
            } else{
              document.getElementsByTagName( "html" )[0].classList.remove( "loading" );
              this.tosatservice.addToastError('Please enter valid input.','top-right');
            }
          }else{
            document.getElementsByTagName( "html" )[0].classList.remove( "loading" );
            this.tosatservice.addToastError('Please specify at least one criteria to perform a search.','top-right');            
          }
      } else if (flag === 'updateMode' && this.activeArtist) {
          document.getElementsByTagName( "html" )[0].classList.add( "loading" );    
          value.countries = this.selected;
          value.notes = this.artistDetailsForm.controls['notes'].value;
		      this.editModeFlag = false;
          value.categories = this.selectedCategories;
          value.id = this.activeArtist;
          delete value.id;
          var tempCataRais = value.artistCatalogueRaisonees.filter(function (cateRaisonees) {
            return cateRaisonees.id == null;
          });
		      value.artistCatalogueRaisonees.forEach((data,index) => {
            if(data.id){ 
              if(this.indexCRValue.indexOf(index)>-1){
                this.updateArtist(data)
              }
            }
          });
		      this.requiredFlag = true;
          if(this.artistDetailsForm.valid){
            if(tempCataRais.length>0){
              this._artistService
                .addArtistCatalogueRaisonees(tempCataRais, this.activeArtist)
                .subscribe(Response => {
                value.artistCatalogueRaisonees = value.artistCatalogueRaisonees.concat(Response);
                var catalogData = [];
                this.editModeFlag = true;
                value.artistCatalogueRaisonees.forEach(data => { if(data.id != null){ catalogData.push(data); } });
                this._artistService
                .updateArtist(this.activeArtist, value)
                .subscribe(Response => {
                  if(Response){ 
                  document.getElementsByTagName( "html" )[0].classList.remove( "loading" );
                  this.tosatservice.addToast('Artist successfully updated','top-right');
                  }
                  this.nameChange('singleArtist',Response)
                }, resFileError => this.errorMsg = resFileError);
                }, resFileError => {
                document.getElementsByTagName( "html" )[0].classList.remove( "loading" );  
                this.tosatservice.addToastError('There is an error while saving your record','top-right');
                this.errorMsg = resFileError;
                });
            } else{
              this._artistService
              .updateArtist(this.activeArtist, value)
              .subscribe(Response => {
                if(Response){ 
                  document.getElementsByTagName( "html" )[0].classList.remove( "loading" );                      
                  this.tosatservice.addToast('Artist successfully updated','top-right');
                }
                this.editModeFlag = true;
                this.nameChange('singleArtist',Response)
              }, resFileError => {
                document.getElementsByTagName( "html" )[0].classList.remove( "loading" );  
                this.tosatservice.addToastError('There is an error while saving your record','top-right');
                this.errorMsg = resFileError;
              });
            }
          } else {
            this.tosatservice.addToastError('There is an error while saving your record','top-right');
            document.getElementsByTagName( "html" )[0].classList.remove( "loading" );  
          }
      } else if (flag === 'saveMode') {
          document.getElementsByTagName( "html" )[0].classList.add( "loading" ); 
          value.countries = this.selected;
          value.categories = this.selectedCategories;
          this.requiredFlag = true;
          if(this.artistDetailsForm.valid){
            value.firstName = (value.firstName&&value.firstName.trim())?value.firstName.trim():'';
            value.lastName  = (value.lastName&&value.lastName.trim())?value.lastName.trim():'';
            value.display   = (value.display&&value.display.trim())?value.display.trim():'';
            this._artistService
              .addArtist(value,false)
              .subscribe(Response => {
                  this.tosatservice.addToast('Artist successfully created','top-right');
                  this.searchMode = true;
                  this.editModeFlag = true;
                  this.activeArtist = Response.id;
                  this.nameChange('singleArtist',Response);
                  document.getElementsByTagName( "html" )[0].classList.remove( "loading" );                  
              }, resFileError => {
                if(resFileError.status===409){
                  document.getElementsByTagName( "html" )[0].classList.remove( "loading" );                  
                  let disposable = this.dialogService.addDialog(ConfirmComponent, {
                    title:'Confirm',  
                    message: 'An artist already exists with the provided values. Are you sure you would like to create a new artist?'})
                    .subscribe((isConfirmed)=>{
                      if(isConfirmed){
                        document.getElementsByTagName( "html" )[0].classList.add( "loading" );          
                        this._artistService
                        .addArtist(value,true)
                        .subscribe(Response => {
                            this.tosatservice.addToast('Artist successfully created','top-right');
                            this.searchMode = true;
                            this.editModeFlag = true;
                            this.activeArtist = Response.id;
                            this.nameChange('singleArtist',Response);
                            document.getElementsByTagName( "html" )[0].classList.remove( "loading" );  
                        },resFileError => {
                          document.getElementsByTagName( "html" )[0].classList.remove( "loading" );
                          this.tosatservice.addToastError('There is an error while saving your record','top-right');
                          this.errorMsg = resFileError;
                        });
                      }
                    })
                } else{
                  document.getElementsByTagName( "html" )[0].classList.remove( "loading" );
                  this.tosatservice.addToastError('There is an error while saving your record','top-right');
                  this.errorMsg = resFileError;
                }
            });
          } else{
            document.getElementsByTagName( "html" )[0].classList.remove( "loading" );
            this.tosatservice.addToastError('There is an error while saving your record','top-right');
          }
      } else if (flag === 'deleteMode') {
          this._artistService
            .removeArtist(this.activeArtist)
            .subscribe(Response => {
                this.tosatservice.addToast('Artist successfully deleted','top-right');
                if(this.total_pages==this.curPageValue-1){
                  this.defaultMode();
                } else{
                  this.total_pages = this.total_pages-1;
                  if(this.curPageValue == 1){
              var query = this.queryValue + "&page=0";
              this.nameChange('pagination', query.toString());
                  }else{
              this.confirmFlag = false;
                  this.resetFullForm();
              this.pageObject = {
                pageNo: this.pageNo,
                pageNumber: this.pageNumber,
                curPageValue: this.curPageValue,
                pageData: this.pageData,
                  flagPage: 'prevPage'
                  };
              this.pageObject = this._paginationService.getNextPage(this.pageObject);
              this.curPageValue = this.pageObject.curPageValue;
              this.pageNumber = this.pageObject.pageNo;
              this.pageNo = this.pageObject.pageNo;
              var query = this.queryValue + "&page=" + this.pageNumber;
              this.nameChange('pagination', query.toString());
                  }
                }
            }, resFileError => this.errorMsg = resFileError);
      }    
}


	public indexCRValue = [];
	indexCRArray(index){
	  if( !(this.indexCRValue.indexOf(index)>-1)){
		  this.indexCRValue.push(index);
	  }
	}
  // change the status based on mode function
flagMode(value: any) {
    if(this.headerFlag == "edit" && !this.confirmFlag){
      this.showConfirm('addMode', this.artistDetailsForm.value);
    }
    else{
      this.confirmFlag = false; 
      if (value == 'addMode') {
          this.resetFullForm();
          this.editModeFlag = true;
          this.searchMode = false;
          this.current_page = 1;
          this.total_pages = 0;
          this.curPageValue = 1;
          this.headerFlag = 'create';
          this.requiredFlag = false;
          this.emptyPagination = '';
          this.widthStyle = 85;
          window.history.replaceState(null, null,"artist");
      } 
    }
  }

//pagination
public pageObject;
public pageEnter: boolean = false;
pageFunction(flagNo: string) {
  if(this.headerFlag == "edit" && !this.confirmFlag){
      this.showConfirm('pageFunction', flagNo);
  }
  else{
      this.confirmFlag = false; 
      this.resetFullForm();
      this.pageObject = {
        pageNo: this.pageNo,
        pageNumber: this.pageNumber,
        curPageValue: this.curPageValue,
        pageData: this.pageData,
        flagPage: (flagNo=='nextPage')?'nextPage':'prevPage'
      };
      this.pageObject = this._paginationService.getNextPage(this.pageObject);
      this.curPageValue = this.pageObject.curPageValue;
      this.pageNumber = this.pageObject.pageNo;
      this.pageNo = this.pageObject.pageNo;
      if(this.pageObject.pageFlag==="single"){
          var objectId = {id: this.pageData[this.pageObject.arryIndex].id};
          this.nameChange('singleArtist', objectId);
      } else{
        var query = this.queryValue+"&page="+this.pageNumber;
        this.nameChange('pagination', query.toString());
      }
  }
}

//jump user input page
getCurrValue(inputvalue,event) {
  this.pageEnter = true;
  if(event==="firstPage" || event==="lastPage" || event==="enter"){
    this.pageEnter = false;
    if(inputvalue >= 0 && inputvalue < this.total_pages){
      this.resetFullForm();
      this.curPageValue = inputvalue;
      this.current_page = this.curPageValue+1;
      this.pageFunction('nextPage');
    } else{
      if(inputvalue >= this.total_pages){
        this.getCurrValue(this.total_pages-1,'lastPage');
      } else {
        if(inputvalue <= -1){
          this.getCurrValue(0,'firstPage');
        }
      }
    }
  }
}

// Toggle 'Gender' selection
private toggleGenderSelection(event) {
  let eleGender = event.target;
  if(eleGender.checked) {
    // Male
    if(eleGender.value == 'Male') {
      if(this.genderMToggleFlag!=0) {
        (<HTMLInputElement>document.getElementById(eleGender.id)).checked = false;
        this.genderMToggleFlag = 0;      
      }
      else {
        (<HTMLInputElement>document.getElementById(eleGender.id)).checked = true;
        this.genderMToggleFlag = 1;
        this.genderFToggleFlag = 0;
      }
    }
    // Female
    else if(eleGender.value == 'Female') {
      if(this.genderFToggleFlag!=0) {
        (<HTMLInputElement>document.getElementById(eleGender.id)).checked = false;
        this.genderFToggleFlag = 0;
      }
      else {
        (<HTMLInputElement>document.getElementById(eleGender.id)).checked = true;
        this.genderFToggleFlag = 1;
        this.genderMToggleFlag = 0;
      }
    }
    // Reset artistDetailsForm 'gender' value if no gender is selected
    if(this.genderMToggleFlag==0 && this.genderFToggleFlag==0) {
      this.artistDetailsForm.controls['gender'].reset();
    }
  }
}

// Focus 'gender' radio control on tab
// Highlight focussed gender in 'bold'
public focusElement(event) {
  let ele = event.target;
  if(ele.type == 'radio') {
    // Male
    if(ele.value == 'Male') {
      (<HTMLInputElement>document.getElementById('genderMale').nextSibling.nextSibling).style.fontWeight = 'bold';
      (<HTMLInputElement>document.getElementById('genderFemale').nextSibling.nextSibling).style.fontWeight = 'normal';
    }
    // Female
    else if(ele.value == 'Female') {
      (<HTMLInputElement>document.getElementById('genderFemale').nextSibling.nextSibling).style.fontWeight = 'bold';
      (<HTMLInputElement>document.getElementById('genderMale').nextSibling.nextSibling).style.fontWeight = 'normal';
    }
  }
}

// Focus radio control on tab
public removeFocusOnBlur(event) {
  let ele = event.target;
  if(ele.type == 'radio') {
    if(ele.value == 'Male') {
      (<HTMLInputElement>document.getElementById('genderMale').nextSibling.nextSibling).style.fontWeight = 'normal';
    }
    else if(ele.value == 'Female') {
      (<HTMLInputElement>document.getElementById('genderFemale').nextSibling.nextSibling).style.fontWeight = 'normal';
    }
  }
}


redirectFLPage(flag:string,){
  if(this.headerFlag==="edit"){
    this.showConfirm(flag, 'FLValue');
  } else{
    if(flag==='lastPage'&& this.total_pages>0){
      this.curPageValue = this.total_pages;
      this.getCurrValue(this.total_pages-1,'lastPage');
    }
    if(flag==='firstPage'&& this.total_pages>0){
      this.getCurrValue(0,'firstPage');
    }
  }
}


	// Keyboard shortcuts
	keyboardShortCutFunction(e) {
	  this.toggleFlag=0;
	  switch(this._keyboardkeysService.getKeyboardResponse(e)){
		case 'save':
		  if(this.headerFlag==='result'||this.headerFlag==='edit'){
			  this.onSubmit( ((this.searchMode ||this.editModeFlag) && this.activeArtist!=null)?'updateMode':'saveMode',this.artistDetailsForm.value);
		  }
		break;
		case 'addRecord':
		  if(this.headerFlag==='create'){
		  	this.onSubmit( ((this.searchMode ||this.editModeFlag) && this.activeArtist!=null)?'updateMode':'saveMode',this.artistDetailsForm.value);
		  }
		break;
		case 'find':
		  if(this.headerFlag==='search'){
			  this.onSubmit('searchMode',this.artistDetailsForm.value);
		  } else if(this.headerFlag==='result'||this.headerFlag==='edit'){
			  this.defaultMode();
		  }
		break;
		case 'amendFind':
		  if(this.headerFlag==='result'||this.headerFlag==='edit' || this.headerFlag==='create'){
			  this.backMode();
		  }
		break;
		case 'add':
		  if(this.headerFlag==='search'||this.headerFlag==='result'||this.headerFlag==='edit'){
			  this.flagMode('addMode');
		  }
		break;
		case 'homeView':
		  if(this.headerFlag==='search'||this.headerFlag==='result'||this.headerFlag==='edit'){
			  this.redirectPage();
		  }
		break;
		case 'objectView':
		  if(this.headerFlag==='search'||this.headerFlag==='result'){
			  this.router.navigateByUrl('/object');
		  }else if(this.headerFlag === "edit" && !this.confirmFlag){
			  this.showConfirm('objectView', this.artistDetailsForm.value);
		  } else if(this.headerFlag==="create"){
        this.createModeExit((exitFlag)=>{
          if(exitFlag){
            this.router.navigateByUrl('/object');
          }
        })
		  }
		break;
		case 'next':
		  if((this.total_pages>0) && (this.total_pages>this.curPageValue)){
			  this.pageFunction('nextPage');
		  }
		break;
		case 'prev':
		  if((this.total_pages>0) && (this.curPageValue>1)){
			  this.pageFunction('prevPage');
		  }
		break;
		case 'editMode':
		  if(this.headerFlag==='edit'){
        this.headerFlag = 'result';
        if(this.total_pages>0){
          this.getCurrValue(this.curPageValue-1,'enter');
        }
		  }
    break;
    case 'duplicateWindow':
        let urlP = window.location.href;
        window.open(urlP, '_blank');
    break;
		case 'enter':
		  if(!this.activeArtist  && this.headerFlag === "search" ){
			  this.onSubmit('searchMode',this.artistDetailsForm.value);
		  } else if(this.pageEnter){
			  this.getCurrValue(this.curPageValue-1,'enter');
		  } else if(this.headerFlag === "create"){
			  this.onSubmit('saveMode',this.artistDetailsForm.value);
		  }
		break;
	  }
	}

//Create Mode exit
createModeExit(callback){
  var value = this.artistDetailsForm.value;
  value.countries = this.selected;
  value.categories = this.selectedCategories;
  value.birthYear = value.birthYear?value.birthYear.toString():value.birthYear;
  value.deathYear = value.deathYear?value.deathYear.toString():value.deathYear;
  var flagSearch = false;
  for (var key in value) {
    if (value.hasOwnProperty(key)) {
      if(value[key] && value[key]!='undefined' && value[key].length){
        flagSearch = true;
      }
    }
  }
  if(flagSearch){
    this.dialogService.addDialog(ConfirmComponent, {
      title:'Confirm', 
      message: 'Any changes you made on this screen will be discarded. Would you like to proceed?'})
      .subscribe((isConfirmed)=>{
        if(isConfirmed){
          this.headerFlag = 'search';
          callback(true);
        } else{
          callback(false);
        }
      });
  } else{
    this.headerFlag = 'search';
    callback(true);
  }
}
//Back button
backMode(){
  if(this.headerFlag === "edit" && !this.confirmFlag){
      this.showConfirm('backMode', this.artistDetailsForm.value);
  } else if(this.headerFlag==="create"){
	this.createModeExit((exitFlag)=>{
      if(exitFlag){
        this.backMode();
      }
    })
  } else{
      //this.confirmFlag = false; 
      this.defaultMode();
      this.selected = [];
	    this.indexCRValue = [];
      this.headerFlag = 'search';
      this.requiredFlag = false;
      this.selectedCategories = [];
      window.history.replaceState(null, null,"artist");
      this.artistDetailsForm.controls['firstName'].setValue((this.searchData &&this.searchData.firstName&&this.searchData.firstName!='undefined')?this.searchData.firstName:'');
      this.artistDetailsForm.controls['lastName'].setValue((this.searchData &&this.searchData.lastName&&this.searchData.lastName!='undefined')?this.searchData.lastName:'');
      if(this.searchData&& this.searchData.gender==='male'){
        this.searchData.gender = "Male";
      }
      if(this.searchData&& this.searchData.gender==='female'){
        this.searchData.gender = "Female";
      }
      this.artistDetailsForm.controls['gender'].setValue((this.searchData&&this.searchData.gender&&this.searchData.gender!='undefined')?this.searchData.gender:'');
      this.artistDetailsForm.controls['birthYear'].setValue((this.searchData&&this.searchData.birthYear&&this.searchData.birthYear!='undefined')?this.searchData.birthYear:null);
      this.artistDetailsForm.controls['deathYear'].setValue((this.searchData&&this.searchData.deathYear&&this.searchData.deathYear!='undefined')?this.searchData.deathYear:null);
      this.artistDetailsForm.controls['display'].setValue((this.searchData&&this.searchData.display&&this.searchData.display!='undefined')?this.searchData.display:'');
      this.artistDetailsForm.controls['countries'].setValue("");
      if(this.searchData&&this.searchData.artistCatalogueRaisonees){
        this.searchData.artistCatalogueRaisonees.forEach(x => {
            const control = < FormArray > this.artistDetailsForm.controls['artistCatalogueRaisonees'];
            control.push(this.pushArtistCatalogueRaisonees(x));
        });
      }
	  if(this.searchData && this.searchData.countries){
      this.searchData.countries.forEach(x => {
        this.countryValChange(x.countryName);
      });
	  }
      this.categories = (this.searchData && this.searchData.categories)?this.searchData.categories:[];
      for (var i = 0; i < this.categoryAll.length; i++) {
        var flags = this.categoryAll[i].id;
        flags = this.categories.filter(function(el) { return flags == el.id; });
        this.categoryAll[i].status = flags.length ? true : false;
        if (flags.length) {
            this.selectedCategories.push({
                id: this.categoryAll[i].id,
                description: this.categoryAll[i].description,
                name: this.categoryAll[i].name
            });
        }
      }
      this.footerName = { createdDate: null,createdBy: null,updatedDate: null,updatedBy: null}
      this.changed.emit(this.footerName);
  }
}
removeNationality(event){
  this.artistDetailsForm.controls['countries'].setValue("");
}
// Get query string params
getQueryStringValue(queryStr, action) {
  let queryStringParams = <any>[];
  let query: string = "";

  for (let param in queryStr){
      if (param == 'birthYear' && queryStr['birthYear']) {
          query += "&birthYear="+queryStr['birthYear'];
      }
      if (param == 'deathYear' && queryStr['deathYear']) {
          query += "&deathYear="+queryStr['deathYear'];
      }
      if (param == 'display' && queryStr['display']) {
          query += "&display="+queryStr['display'];
      }
      if (param == 'firstName' && queryStr['firstName']) {
          query += "&firstName="+queryStr['firstName'];
      }
      if (param == 'lastName' && queryStr['lastName']) {
          query += "&lastName="+queryStr['lastName'];
      }
      if (param == 'gender' && queryStr['gender']) {
          query += "&gender="+queryStr['gender'];
      }
      if (param == 'countries' && queryStr['countries']) {
          let countryNames = "";
          let tempcountryNames = this.artistDetailsForm.controls['countries'].value;
          for (let i in tempcountryNames) {
              countryNames += "countryNames="+tempcountryNames[i].value+"&";
          }
          query += "&"+countryNames;
      }
  }

  if (this.selectedCategories.length > 0) {
      let categoryNames = "";
      //let tempcategoryNames = this.artistDetailsForm.controls['countries'].value;
      for (let i in this.selectedCategories) {
          categoryNames += "categoryNames="+this.selectedCategories[i].name+"&";
      }
      query += "&"+categoryNames;
  }

  switch (action) {
      case "search":
          return query;
  }
}
@HostListener('window:input', ['$event'])
onInput(e) {
	if(!(e && (e.target && (JSON.stringify(e.target.classList).indexOf('currentValueMode')>-1)) || (e.srcElement && e.srcElement.form && JSON.stringify(e.srcElement.form.classList).indexOf('noModeChange')>-1))) {
		this.headerFlag = this.activeArtist?'edit':this.headerFlag;
    }
}
onChangeFunctionCR(){
  if(this.CRChanged){
    this.headerFlag = this.activeArtist?'edit':this.headerFlag ;
    this.CRChanged = false;
  }
}

public CRUpdated = false;
public updatedCRindex = [];


onCRUpdate(i,type,event){
  switch(type){
    case 'author':
           this.updatedCRArray[i].author = event.target.innerText;
          break;
    case 'year':
           this.updatedCRArray[i].year =  parseInt(event.target.innerText);
          break;
    case 'volumes':
           this.updatedCRArray[i].volumes =  event.target.innerText; 
          break; 
    case 'type':
           this.updatedCRArray[i].type =  event.target.innerText;  
          break;
  }
  this.CRUpdated = true;   
  if(this.updatedCRindex.indexOf(i) == -1){
    this.updatedCRindex.push(i);
  }
}

updateArtist(CR){
  if(this.artistDetailsForm.valid){
    this._artistService.updateArtistCatalogueRaisonees(CR,this.activeArtist)
    .subscribe(Response  => {
    }, resFileError => {
        this.tosatservice.addToastError('There is an error while saving your record','top-right');
        this.errorMsg = resFileError;
    });
  }
}

amendSearch(){
  this.selected = [];
  this.indexCRValue = [];
  this.headerFlag = 'search';
  this.requiredFlag = false;
  this.selectedCategories = [];
  window.history.replaceState(null, null,"artist");
  this.artistDetailsForm.controls['firstName'].setValue((this.searchData &&this.searchData.firstName&&this.searchData.firstName!='undefined')?this.searchData.firstName:'');
  this.artistDetailsForm.controls['lastName'].setValue((this.searchData &&this.searchData.lastName&&this.searchData.lastName!='undefined')?this.searchData.lastName:'');
  if(this.searchData&& this.searchData.gender==='male'){
    this.searchData.gender = "Male";
  }
  if(this.searchData&& this.searchData.gender==='female'){
    this.searchData.gender = "Female";
  }
  this.artistDetailsForm.controls['gender'].setValue((this.searchData&&this.searchData.gender&&this.searchData.gender!='undefined')?this.searchData.gender:'');
  this.artistDetailsForm.controls['birthYear'].setValue((this.searchData&&this.searchData.birthYear&&this.searchData.birthYear!='undefined')?this.searchData.birthYear:null);
  this.artistDetailsForm.controls['deathYear'].setValue((this.searchData&&this.searchData.deathYear&&this.searchData.deathYear!='undefined')?this.searchData.deathYear:null);
  this.artistDetailsForm.controls['display'].setValue((this.searchData&&this.searchData.display&&this.searchData.display!='undefined')?this.searchData.display:'');
  this.artistDetailsForm.controls['countries'].setValue("");
  if(this.searchData&&this.searchData.artistCatalogueRaisonees){
    this.searchData.artistCatalogueRaisonees.forEach(x => {
        const control = < FormArray > this.artistDetailsForm.controls['artistCatalogueRaisonees'];
        control.push(this.pushArtistCatalogueRaisonees(x));
    });
  }
if(this.searchData && this.searchData.countries){
  this.searchData.countries.forEach(x => {
    this.countryValChange(x.countryName);
  });
}
  this.categories = (this.searchData && this.searchData.categories)?this.searchData.categories:[];
  for (var i = 0; i < this.categoryAll.length; i++) {
    var flags = this.categoryAll[i].id;
    flags = this.categories.filter(function(el) { return flags == el.id; });
    this.categoryAll[i].status = flags.length ? true : false;
    if (flags.length) {
        this.selectedCategories.push({
            id: this.categoryAll[i].id,
            description: this.categoryAll[i].description,
            name: this.categoryAll[i].name
        });
    }
  }
  this.footerName = { createdDate: null,createdBy: null,updatedDate: null,updatedBy: null}
  this.changed.emit(this.footerName);
}

// EOS-838 - Reject new values in autocomplete list (Artist first/last name fields).
resetArtistNameInput(field) {
  if(field == 'firstname') {
    if(this.firstNameList.nativeElement.nextSibling) {
      this.firstNameList.nativeElement.nextSibling.style.display = 'none';
    }
    if(this.firstNameFound===false) {
      this.artistDetailsForm.controls['firstName'].setValue('');
    }
  }
  else if (field == 'lastname') {
    if(this.lastNameList.nativeElement.nextSibling) {
      this.lastNameList.nativeElement.nextSibling.style.display = 'none';
    }
    if(this.lastNameFound===false) {
      this.artistDetailsForm.controls['lastName'].setValue('');
    }
  }
}
onOpenSaveSearch(){
  document.getElementById('saveSearch-Artist').click();
  this.toggleFlag = 0;
}
	//checkbox focus event
	addOutline(event) {
		let parent = event.srcElement.parentElement;
		parent.querySelector(".fa-check").classList.add("outline");
	}
	removeOutline(event) {
		let parent = event.srcElement.parentElement;
		parent.querySelector(".fa-check").classList.remove("outline");
  }
  
  onProgressNumber(event){
    console.log(event);
  }
}