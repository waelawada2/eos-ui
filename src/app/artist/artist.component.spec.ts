import { NO_ERRORS_SCHEMA , CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ArtistComponent } from './artist.component';
import { SavedSearchComponent} from '../saved-search/saved-search.component';
import { SaveSearchModalComponent } from '../saved-search/saved-search-modal.component';

class DummyRouter {}

describe('ArtistComponent', () => {
  let component: ArtistComponent;
  let fixture: ComponentFixture<ArtistComponent>;
  const myMockWindow = <any>{
    'keycloak': {
      'tokenParsed' : 'mockedToken'
    }};
    beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [ ArtistComponent, SaveSearchModalComponent, SavedSearchComponent ],
        providers: [
          FormBuilder,
          { provide: ActivatedRoute, useClass: DummyRouter },
          { provide: Router, useClass: DummyRouter },
          { provide: Window, useValue: myMockWindow}
        ],
        imports: [ HttpModule ],
        schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ]
      })
      .compileComponents();
    }));
    beforeEach(() => {
      fixture = TestBed.createComponent(ArtistComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
    xit(' window should be injected in saved-search-modal.component ', () => {
      /*
      this test is being ignored:
      Window should be injected in the module, and should be used in constructor if any variable initalization might take place
      - this is in order to have a way to inject window into the test.
      saved-searched-modal-component.ts( line 26 )
      ( public userName = window['keycloak'].tokenParsed?window['keycloak'].tokenParsed.preferred_username:null;)
      */
      expect(component).toBeTruthy();
    });
  })
