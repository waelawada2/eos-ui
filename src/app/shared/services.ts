import { ObjectService } from './services/object/object.service'
import { EventReportService } from './services/object/event-report/event-report.service'

const GLOBAL_SHARED_SERVICES = [
  ObjectService,
  EventReportService,
]

export {
  EventReportService,
  ObjectService,
  GLOBAL_SHARED_SERVICES
}
