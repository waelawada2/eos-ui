import { Injectable, Injector } from '@angular/core';
import AbstractService from '../../../../event/shared/services/abstract.service';
import { GeneralConstants } from '../../../constants';
import * as _ from 'lodash';

@Injectable()
export class EventReportService extends AbstractService<any> {
  customUrl: any

  constructor(injector: Injector) {
    super('items', injector);
  }

  getURLToSearch(params: string, objectId: any) {
    return `items?objects=${objectId}${params}`;
  }
}
