import * as _ from 'lodash';
import { Injectable, Injector } from '@angular/core';
import { Http, Response } from '@angular/http';
import AbstractService, { ENVIRONMENT } from '../../../event/shared/services/abstract.service';
import { Property } from '../../../event/shared/models/property.interface';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/concat';

@Injectable()
export class ObjectService extends AbstractService<Property> {

  private searchParams: any;

  constructor(injector: Injector) {
    super('properties', injector, ENVIRONMENT.PROPERTY)
  }

  private assembleImageUrl(object: Property): string {
    return `${this.completeUrl}/${object.id}/images/default?${object.imagePath}`
  }

  transformElements(elements: Property[]): Property[] {
    return elements.map((obj: Property) => {
      if (obj && obj.imagePath) {
        obj.imageUrl = this.assembleImageUrl(obj)
      }
      return obj
    })
  }

  public setPagingAndGetElements(data: any): Property[] {
    let nextUrl = data.totalPages > data.number ? `${this.completeUrl}` : null;

    if (nextUrl) {
      nextUrl += `?size=${data.size}&page=${data.number + 1}`;

      const propertyNames = Object.keys(this.searchParams);
      propertyNames.forEach(property => {
        if (property !== 'size' && property !== 'page') {
          const propertyValue = _.get(this.searchParams, property);
          nextUrl += propertyValue ? `&${property}=${propertyValue}` : '';
        }
      })
    }
    this.error.next(null)
    this.links.next({ ..._.get(data, '_links'), next: { href: nextUrl } })
    this.page.next({ number: data.number, size: data.size, totalElements: data.totalElements, totalPages: data.totalPages });
    return this.transformElements(_.get(data, 'content'));
  }

  private getObjectById(id: string): Observable<Property> {
    return this.http
      .get(`${this.completeUrl}/${id}`)
      .map((response: Response) => response.json())
      .catch((response: Response) => Observable.of(null))
  }


  public getElements(searchParams: any = {}, id = null): void {
    this.searchParams = searchParams
    super.getElements(searchParams, id)

  }

  public getNextElements(requested = -1) {
    this.links.take(1).subscribe({
      next: value => {
        const size = this.page.value.size;
        const loadedElements = this.elements.value.length;
        let newSize = -1;
        if (requested > size + loadedElements) {
          newSize = requested - loadedElements + 1
        }
        this.processNextQuery(value.next.href, newSize, this.page.value.number)
      }
    })
  }


  getObjects(ids: string[]): Observable<Property> {
    if (!ids) {
      return Observable.empty();
    }
    return Observable.concat(...ids.map(id => this.getObjectById(id)));
  }
}
