import { GeneralConstants } from '../../event/shared/constants'

export default {
  ...GeneralConstants,
  ...{
    ENTITY_IMPORT_EXPORT: {
      'ARTIST': 'Artist',
      'OBJECT': 'Object',
      'SAVEDLIST': 'SavedList',
      'IMPORT': 'Import',
      'EXPORT': 'Export',
      'ESTABLISHMENT': 'Establishment',
      'TOP_RIGHT': 'top-right',
      'PROPERTY': 'property',
      'SOURCE_NAME': 'Source Name',
      'CLIENT_SOURCES': 'client-sources',
      'CSVSIZE': '50000',
      'SOURCE': 'Source',
      'NAME': 'Name',
    },
    ADD_OBJECTS_ITEM: {
      'TOP_RIGHT': 'top-right',
    },
    ENTITY_EVENT_FILTER: {
      'EVENT_FILTER': ['All', 'Pricing', 'Publication', 'Transaction'],
      'ALL': 'All',
      'DATE_FORMAT': 'dd/MMM/yyyy',
      'OBJECT': 'OBJECT',
      'INSTALLATION': 'INSTALLATION',
    },
    NOTIFICATIONS: {
      OPTIONS: {
        'timeOut': 5000,
        'showProgressBar': false,
        'pauseOnHover': false,
        'clickToClose': true
      },
      SETUP: {
        'position': ['top', 'right'],
        'timeOut': 0,
        'lastOnBottom': true,
      },
      ENTITY_IMPORT_EXPORT: {
        'ARTIST': 'Artist',
        'OBJECT': 'Object',
        'SAVEDLIST': 'SavedList',
        'IMPORT': 'Import',
        'EXPORT': 'Export',
        'ESTABLISHMENT': 'Establishment',
        'TOP_RIGHT': 'top-right',
        'PROPERTY': 'property',
      },

    },
    IMPORT_TYPES: {
      establishment: 'ESTABLISHMENT',
      source_name: 'SOURCE_NAME'
    }
  }
}
