import { Component } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";

export interface ConfirmModel {
  title:string;
  message:string;
  okRequired:boolean;
}

@Component({
  selector: 'confirm',
  template: `<div class="modal-dialog  app-dialog confirm-dialog">
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" (click)="close()" >&times;</button>
                  <h4 class="modal-title">{{title || 'Confirm'}}</h4>
                </div>
                <div class="modal-body">
                  <p>{{message || 'Are you sure?'}} 
                    <select *ngIf="dropSelected!='false'" (change)="onChangeFileType($event.target.value)" value="{{dropSelected}}">
                    <option value="complete">Complete</option>
                    <option value="incomplete">InComplete</option>
                    </select>
                  </p>
                </div>
                <div *ngIf="!okRequired" class="modal-footer">
                  <button type="button" class="btn btn-primary confirm" (click)="confirm()">Yes</button>
                  <button type="button" class="btn btn-default cancel" (click)="cancel()">No</button>
                </div>
                <div *ngIf="okRequired" class="modal-footer">
                  <button type="button" class="btn btn-primary confirm" (click)="confirm()">Yes</button>
                  <button type="button" class="btn btn-default cancel" (click)="cancel()">No</button>
                </div>
            </div>
            </div>`
})
export class ConfirmComponent extends DialogComponent<ConfirmModel, any> implements ConfirmModel {
  title: string;
  message: string;
  okRequired: boolean = false;
  dropSelected: string = "false";

  constructor(dialogService: DialogService) {
    super(dialogService);
  }
  ngOnInit(){
    if(this.title==='dropDownFalse'||(this.title==='dropDownTrue')){
      this.dropSelected = (this.title==='dropDownFalse')?"complete":"incomplete";
      this.title ="";
    }
  }
  onChangeFileType(value) {
    this.dropSelected = (value==='complete')?"complete":"incomplete";
  }
  confirm() {
    if(this.dropSelected!='false'){
      this.result = this.dropSelected;
    }else{
      this.result = true;    
    }
    this.close();
  }
  cancel() {
    this.result = false;
    this.close();
  }
}
