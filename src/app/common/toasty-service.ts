import {Component,Injectable} from '@angular/core';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';
import { Subject } from 'rxjs/Subject';
@Injectable()
export class TosatService {
    constructor(private toastyService:ToastyService, private toastyConfig: ToastyConfig) { 
        // Assign the selected theme name to the `theme` property of the instance of ToastyConfig.  
        // Possible values: default, bootstrap, material 
        this.toastyConfig.theme = 'bootstrap';
        this.toastyConfig.position="top-right";
        
    }
    
    addToast(message:string,position:any) {
        var toastOptions:ToastOptions = {
            title: '',
            msg: message,
            showClose: true,
            timeout: 15000,
            theme: 'bootstrap',
            onAdd: (toast:ToastData) => {
            },
            onRemove: function(toast:ToastData) {
            }
        };
         this.toastyService.success(toastOptions);

    }
    addToastError(message:string,position:any) {
        var toastOptions:ToastOptions = {
            title: '',
            msg: message,
            showClose: true,
            timeout: 15000,
            theme: 'bootstrap',
            onAdd: (toast:ToastData) => {
            },
            onRemove: function(toast:ToastData) {
            }
        };
         this.toastyService.error(toastOptions);

    }
   
}