import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { AppService } from '../../app/app.service';
import { ObjectService } from '../../app/object/object.service';
import { ArtistService } from '../../app/artist/artist.service';
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";
import 'rxjs/Rx';
import { CompleterService, CompleterData } from 'ng2-completer';
import { EditionModalComponent } from '../../app/modal/newEditionModal.component';
import { ConfirmComponent } from '../common/confirm-component';
import { TosatService } from '../common/toasty-service';
import { KeyboardkeysService } from '../../app/keyboardkeys/keyboardkeys.service';
import { DropuploadComponent } from '../../app/upload/dropupload/new-object-dropupload.component';
import { ConfigurationLoaderService } from 'app/configuration.service'

export interface AddObjectModal {
  modeFlag: string;
}

@Component({
  selector: 'prompt',
  templateUrl: `./newObjectModal.html`,
  providers: [AppService, ObjectService, ArtistService, KeyboardkeysService]
})
export class ObjectModalComponent extends DialogComponent<AddObjectModal, string> implements AddObjectModal, OnInit {

  private URL: string;

  modeFlag: string;
  message: string = '';

  // Select List 'OPEN/CLOSE' status
  private categoryListOpen = false;
  private subCategoryListOpen = false;
  private presizeNameListOpen = false;
  private editionNameListOpen = false;
  private flagsCountryListOpen = false;
  // Last CR of selected artists
  public lastCR = [];

  @ViewChild(DropuploadComponent)
  private dropuploadComponent: DropuploadComponent;

  apply(value) {
    this.result = this.message;
    var artistArr = [];
    this.checkedArtist.forEach(data => {
      artistArr.push(data.id);
    });
    delete value.artistData;
    delete value.archiveNumbers;
    delete value.editionSizeTotal;
    delete value.researcherNotesDate;
    value.artistIds = artistArr;
    value.archiveNumbers = this.getArchiveNumbers();
    value.editionSizeTypeName = (this.editionSizeObject && this.editionSizeObject.editionSizeType) ? this.editionSizeObject.editionSizeType : null;
    value.editionSizeTotal = (this.editionSizeObject && this.editionSizeObject.editionSizeTotal) ? this.editionSizeObject.editionSizeTotal : null;
    value.seriesTotal = (this.editionSizeObject && this.editionSizeObject.editionSizeSeries) ? this.editionSizeObject.editionSizeSeries : null;
    value.trialProofTotal = (this.editionSizeObject && this.editionSizeObject.editionSizeTrialProof) ? this.editionSizeObject.editionSizeTrialProof : null;
    value.artistProofTotal = (this.editionSizeObject && this.editionSizeObject.editionSizeArtistProof) ? this.editionSizeObject.editionSizeArtistProof : null;
    value.bonATirerTotal = (this.editionSizeObject && this.editionSizeObject.editionSizeBonATirerProof) ? this.editionSizeObject.editionSizeBonATirerProof : null;
    value.printersProofTotal = (this.editionSizeObject && this.editionSizeObject.editionSizePrintersProof) ? this.editionSizeObject.editionSizePrintersProof : null;
    value.horsCommerceProofTotal = (this.editionSizeObject && this.editionSizeObject.editionSizeHorsCommerceProof) ? this.editionSizeObject.editionSizeHorsCommerceProof : null;
    value.museumProofTotal = (this.editionSizeObject && this.editionSizeObject.editionSizeMuseumProof) ? this.editionSizeObject.editionSizeMuseumProof : null;
    value.dedicatedProofTotal = (this.editionSizeObject && this.editionSizeObject.editionSizeDedicatedProof) ? this.editionSizeObject.editionSizeDedicatedProof : null;
    value.editionOverride = (this.editionSizeObject && this.editionSizeObject.editionSizeEditionOverride) ? this.editionSizeObject.editionSizeEditionOverride : null;
    value.editionSizeNotes = (this.editionSizeObject && this.editionSizeObject.editionSizeNotes) ? this.editionSizeObject.editionSizeNotes : null;
    if (this.objectDetailsForm.controls['researcherNotesDate'].value) {
      value.researcherNotesDate = this.getDate(this.objectDetailsForm.controls['researcherNotesDate'].value, "mm-dd-yyyy");
    }
    value.propertyCatalogueRaisonees = this.updatePropertyCatalogRaisonnees('save');
    this.requiredFlag = true;

    if (value.category == "") {
      value.category = null;
    }
    if (value.subCategory == "") {
      value.subCategory = null;
    }
    if (value.presizeName == "") {
      value.presizeName = null;
    }
    if (value.editionName == "") {
      value.editionName = null;
    }
    if (value.editionSizeEditionOverride == "") {
      value.editionSizeEditionOverride = null;
    }

    if ((this.objectDetailsForm.valid || this.requiredFlag) && this.checkedArtist.length) {
      var regex = '^0*([1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9])$';
      if (value && (value && value.yearStart && !value.yearStart.toString().match(regex) ? false : true)
        && (value && value.yearEnd && !value.yearEnd.toString().match(regex) ? false : true)
        && (value && value.castYearStart && !value.castYearStart.toString().match(regex) ? false : true)
        && (value && value.castYearEnd && !value.castYearEnd.toString().match(regex) ? false : true)) {
        if (value.yearStart && !value.yearEnd) {
          this.tosatservice.addToastError('Please enter Year2', 'top-right');
        } else {
          if (value.castYearStart && !value.castYearEnd) {
            this.tosatservice.addToastError('Please enter CastYear2', 'top-right');
          } else {
            if (value.yearStart && value.yearEnd && value.yearEnd < value.yearStart) {
              this.tosatservice.addToastError('Year2 must be greater than or equal to Year1', 'top-right');
            } else {
              if (value.castYearStart && value.castYearEnd && value.castYearEnd < value.castYearStart) {
                this.tosatservice.addToastError('CastYear2 must be greater than or equal to CastYear1', 'top-right');
              } else {
                if (!value.yearStart && value.yearEnd) {
                  this.tosatservice.addToastError('Please enter Year1', 'top-right');
                } else {
                  if (!value.castYearStart && value.castYearEnd) {
                    this.tosatservice.addToastError('Please enter CastYear1', 'top-right');
                  } else {
                    this._objectService
                      .addProperty(value)
                      .subscribe(Response => {
                        if (this.dropuploadComponent._dropzone.files.length > 0) {
                          this.dropuploadComponent._dropzone.options.url = `${this.URL}/properties/${Response.id}/images`;
                          this.dropuploadComponent._dropzone.processQueue();
                        }
                        setTimeout(() => {
                          this.close();
                          this.tosatservice.addToast('A new object has successfully been created: <a target="_blank" href=object?id=' + Response.id + '>' + "'" + Response.id + "'" + '</a>', 'top-right');
                        }, 1000);
                      }, resFileError => {
                        this.errorMsg = resFileError;
                        this.tosatservice.addToastError('There is an error while saving your record', 'top-right');
                      });
                  }
                }
              }
            }
          }
        }
      } else {
        this.tosatservice.addToastError('There is an error while saving your record', 'top-right');
      }
    } else {
      this.tosatservice.addToastError('There is an error while saving your record', 'top-right');
    }
  }


  ngOnInit() {
    this.categories = [];
    this.countries = [];
    this.presizeNames = [];
    this.editionTypes = [];
    this.subcategories = [];

    // Dynamically set column height to span to the height of the browser window
    this._objectService
      .getPreSizeTypes()
      .subscribe(presizeType => {
        presizeType.forEach((presizeType, index) => {
          this.presizeNames.push({ 'name': presizeType.name, 'id': presizeType.id, "description": presizeType.description });
        });
      }, resFileError => this.errorMsg = resFileError);

    this._objectService
      .getEditionTypes()
      .subscribe(editionTypes => {
        editionTypes.forEach((editionType, index) => {
          this.editionTypes.push({ 'name': editionType.name, 'id': editionType.id, "description": editionType.description });
        });
      }, resFileError => this.errorMsg = resFileError);

    // Maps Categories to 'Category' dropdown using ng-completer component
    this.categoryService = <any>this.completerService.local(this.categories, 'name', 'name');

    // Maps Categories to 'Category' dropdown using ng-completer component
    if (this.subcategories.length > 0) {
      this.subCategoryService = <any>this.completerService.local(this.subcategories, 'name', 'name');
    } else {
      this.subCategoryService = <any>this.completerService.local(this.subcategories, 'name', 'name');
    }

    // Maps Presize Types to 'Presize Type' dropdown using ng-completer component
    this.presizeTypesService = <any>this.completerService.local(this.presizeNames, 'name', 'name');

    // Maps Edition Types to 'Presize Type' dropdown using ng-completer component
    this.editionTypeService = <any>this.completerService.local(this.editionTypes, 'name', 'name');

    this.objectDetailsForm = this.builder.group({
      artistData: new FormControl("", [Validators.required]),
      imagePath: new FormControl(""),
      title: new FormControl(""),
      originalTitle: new FormControl(""),
      signature: new FormControl(""),
      medium: new FormControl(""),
      category: new FormControl(),
      subCategory: new FormControl(),
      presizeName: new FormControl(),
      parts: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
      heightCm: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
      widthCm: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
      depthCm: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
      heightIn: new FormControl(""),
      widthIn: new FormControl(""),
      depthIn: new FormControl(""),
      sizeText: new FormControl(""),
      yearStart: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
      yearEnd: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
      castYearStart: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
      castYearEnd: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
      yearText: new FormControl(""),
      yearCirca: new FormControl(""),
      castYearCirca: new FormControl(""),
      posthumous: new FormControl(""),
      editionNumber: new FormControl(""),
      editionName: new FormControl(),
      editionSizeTotal: new FormControl(""),
      weight: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
      foundry: new FormControl(),
      certificate: new FormControl(),
      signed: new FormControl(),
      stamped: new FormControl(),
      authentication: new FormControl(),
      archiveNumbers: new FormControl(),
      sortCode: new FormControl(),
      flags: this.builder.group({
        flagAuthenticity: new FormControl(),
        flagRestitution: new FormControl(),
        flagSfs: new FormControl(),
        flagCondition: new FormControl,
        flagMedium: new FormControl(),
        flagsCountry: new FormControl()
      }),
      researcherName: new FormControl(),
      researcherNotesDate: new FormControl("", Validators.pattern('^[0-3]?[0-9] [A-Za-z]{3} (?:[0-9]{2})?[0-9]{2}$')),
      researcherNotesText: new FormControl(),
      volume: new FormControl(),
      number: new FormControl(),
    });
  }

  public keycloak = this.keycloak;
  public objectDetailsForm: FormGroup;
  public errorMsg;
  public categories = [];
  public subcategories = [];
  public presizeNames = [];
  public editionSizeTypes: any;
  public editionTypes = [];
  // archive number
  public archiveNumbers = [];
  //Catalogue Raisonnes array
  public propertyCatalogRaisonnee = [];
  //Error message flag
  public requiredFlag = false;
  // Flags Fields
  public country: string;
  public countries: Array<object> = [];
  public errorMessage: string;
  protected categoryService: CompleterData;
  protected subCategoryService: CompleterData;
  protected presizeTypesService: CompleterData;
  protected editionTypeService: CompleterData;

  // Researcher Notes
  public researchNotesDate: Date;

  // Pagination
  public propertyNumber: number = 0;

  //constructor method
  constructor(private builder: FormBuilder,
    private _sanitizer: DomSanitizer,
    private _objectService: ObjectService,
    private _artistService: ArtistService,
    private router: Router,
    private completerService: CompleterService,
    private _keyboardkeysService: KeyboardkeysService,
    private _appService: AppService,
    dialogService: DialogService,
    private tosatservice: TosatService,
    configuration: ConfigurationLoaderService,
  ) {
    super(dialogService);

    this.URL = configuration.getSettings().apiUrl;
    this._objectService
      .getCategories()
      .subscribe(categories => {
        categories.forEach((category, index) => {
          this.categories.push({ name: category.name, id: category.id, description: category.description });
        });
      }, resFileError => this.errorMsg = resFileError);

    this._objectService
      .getEditionSizeTypes()
      .subscribe(Response => {
        this.editionSizeTypes = Response;
      }, resFileError => this.errorMsg = resFileError);

    this._appService.getCountries()
      .subscribe(countries => { countries.forEach((country, index) => { this.countries.push({ 'countryName': country.countryName }); }); },
        error => { this.errorMessage = error; }
      );
  }

  //selectArtist autocomplete function
  nameListFormatter = (data: any) => {
    this.artistMap = "";
    if (data.birthYear && data.deathYear && data.nationality) {
      this.artistMap = `<span><i class='fa fa-birthday-cake' aria-hidden='true'></i> ` + data.birthYear + ` &nbsp;&nbsp;&nbsp;<i class='fa fa-bed' aria-hidden='true'> </i> ` + data.deathYear + ` &nbsp;&nbsp;&nbsp;<i class='fa fa-flag-o' aria-hidden='true'> </i> ` + data.nationality + `</span>`
    } else if (data.birthYear && data.nationality) {
      this.artistMap = `<span><i class='fa fa-birthday-cake' aria-hidden='true'></i> ` + data.birthYear + ` &nbsp;&nbsp;&nbsp;<i class='fa fa-flag-o' aria-hidden='true'> </i> ` + data.nationality + `</span>`
    } else if (data.deathYear && data.nationality) {
      this.artistMap = `<i class='fa fa-bed' aria-hidden='true'> </i> ` + data.deathYear + ` &nbsp;&nbsp;&nbsp;<i class='fa fa-flag-o' aria-hidden='true'> </i> ` + data.nationality + `</span>`
    } else if (data.birthYear && data.deathYear) {
      this.artistMap = `<span><i class='fa fa-birthday-cake' aria-hidden='true'></i> ` + data.birthYear + ` &nbsp;&nbsp;&nbsp;<i class='fa fa-bed' aria-hidden='true'> </i> ` + data.deathYear + ` </span>`
    } else if (data.birthYear) {
      this.artistMap = `<span><i class='fa fa-birthday-cake' aria-hidden='true'></i> ` + data.birthYear;
    }
    let html = `<span class="auto-title">` + data.firstName + `</span><br/>` + this.artistMap;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  }

  // Get 'Archive Numbers'
  public getArchiveNumbers() {
    let tempArchiveNo = this.objectDetailsForm.controls['archiveNumbers'].value;
    this.archiveNumbers = [];
    for (let archiveNo in tempArchiveNo) {
      if (tempArchiveNo[archiveNo].hasOwnProperty('value')) {
        this.archiveNumbers.push({ "archiveNumber": tempArchiveNo[archiveNo].value });
      }
      else {
        this.archiveNumbers.push({ "archiveNumber": tempArchiveNo[archiveNo] });
      }
    }
    return this.archiveNumbers;
  }

  public artistName = [];
  public checkedArtist = [];
  public tempArtist = [];
  public artistMap;
  public artistQuery;

  //Artist change
  artistChange(flag: string, value: any) {
    if (flag === 'add' && typeof value === 'object') {
      this.tempArtist = this.checkedArtist.filter(function (art) {
        if (value)
          return art.id == value.id;
      });
      if (this.tempArtist.length > 0) {
        this.objectDetailsForm.controls['artistData'].setValue("");
      } else if (value) {
        this.checkedArtist.push(value);
        this.objectDetailsForm.controls['artistData'].setValue("");
        this.getArtistCatalogRaisonnees(value.id);
      }
    } else if (flag === 'remove' && typeof value === 'object') {
      this.tempArtist = this.checkedArtist.filter(function (art) {
        return art.id != value.id;
      });
      this.checkedArtist = this.tempArtist;
      this.removeArtistCatalogRaisonnees(value.id);
    } else {
      if (value) {
        value = value.split(' ');
        if (value[0] && value[1]) {
          this.artistQuery = `firstName=` + value[0] + `&lastName=` + value[1] + `&lod=MAX`;
          this.refreshData(this.artistQuery);
        } else if (value[0]) {
          this.artistQuery = `name=` + value[0] + `&lod=MAX`;
          this.refreshData(this.artistQuery);
        }
      }
    }
  }

  //Get artist with Value
  refreshData(event) {
    this._artistService
      .getArtist(event)
      .subscribe(Response => {
        this.artistName.splice(0, this.artistName.length);
        Response.content.forEach(data => {
          var bd = ((data.birthYear && data.deathYear) ? "(" + data.birthYear + "-" + data.deathYear + ")" : (data.birthYear ? "(b." + data.birthYear + ")" : ''));
          this.artistName.push({
            firstName: (data.firstName ? data.firstName + ' ' : '') + data.lastName,
            display: data.display ? data.display : ((data.firstName ? data.firstName + ' ' : '') + data.lastName + bd),
            birthYear: data.birthYear,
            deathYear: data.deathYear,
            id: data.id
          });
        });
      }, resFileError => this.errorMsg = resFileError);
  }

  // Get current date
  public getToday(format) {
    let today = null;
    let now = new Date();
    let day = ("0" + now.getDate()).slice(-2);
    let month = now.getMonth();
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    let currentMonth = months[month];

    if (format.toLowerCase() == "mm-dd-yyyy") {
      today = (month + 1) + "-" + day + "-" + now.getFullYear();
    }
    else {
      today = day + " " + currentMonth + " " + now.getFullYear();
    }
    return today;
  }

  // Convert given date to formats: MM-DD-YYYY (or) YYYY-MM-DD (or) DD MMM YYYY
  public getDate(dt, format) {
    let day, month, year;
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    // If date has '-', 'T' and in the format YYYY-MM-DD (e.g. 2017-05-01T19:32:16.013Z)
    if (dt && (dt.indexOf("T") > -1) && (dt.indexOf("-") > -1)) {
      dt = dt.slice(0, dt.indexOf("T"));
      day = dt.split("-")[1];
      month = dt.split("-")[0];
      year = dt.split("-")[2];
      if (month.charAt(0) == "0") {
        month = month.slice(1, 2);
      }

      month = months[month - 1];
    }
    // If date has '-' and in the format MM-DD-YYYY (e.g. 05-01-2017)
    else if (dt && dt.indexOf("-") > -1) {
      dt = dt;
      day = dt.split("-")[1];
      month = dt.split("-")[0];
      year = dt.split("-")[2];
      if (month.charAt(0) == "0") {
        month = month.slice(1, 2);
      }
      month = months[month - 1];
    }
    // If date has ' ' (space) and in the format DD MMM YYYY (e.g. 01 May 2017)
    else if (dt && dt.indexOf(" ") > -1) {
      dt = dt;
      day = dt.split(" ")[0];
      month = dt.split(" ")[1];
      year = dt.split(" ")[2];
      if (/^[a-zA-Z()]$/.test(month.charAt(0))) {
        month = months.indexOf(month) + 1;
        month = month.length == 1 ? "0" + month : month;
        day = day.length == 1 ? "0" + day : day;
      }
      else if (!/^[a-zA-Z()]$/.test(month.charAt(0))) {
        if (month.charAt(0) == "0") {
          month = month.slice(1, 2);
        }
        month = months[month];
      }
    }

    if (format) {
      if (format.toLowerCase() == "mm-dd-yyyy") {
        return (month + "-" + day + "-" + year);
      }
      else if (format.toLowerCase() == "dd mmm yyyy") {
        return (day + " " + month + " " + year);
      }
    }
    else {
      return (day + " " + month + " " + year);
    }
  }

  // Set 'Researcher Name' and 'Date'
  public setResearcherInfo() {
    if (this.objectDetailsForm.controls['researcherNotesText'].value) {
      let researcherName = window['keycloak'].tokenParsed.name;
      this.objectDetailsForm.controls['researcherName'].setValue(researcherName);
      this.researchNotesDate = this.getToday("dd mmm yyyy");
      this.objectDetailsForm.controls['researcherNotesDate'].setValue(this.researchNotesDate);
    }
    else {
      this.objectDetailsForm.controls['researcherName'].reset();
      this.objectDetailsForm.controls['researcherNotesDate'].reset();
    }
  }

  // Conversion Cm to Inches and Inches to Cm viceversa
  CmToInViceVersaChange(flag: string, conversionFlag: string, value: any) {
    if (value) {
      var cal = (conversionFlag === 'CmToIn') ? 0.393701 : 2.54;
      if ((parseFloat(value) * cal) && conversionFlag == 'CmToIn') {
        var decimal = (parseFloat(value) * cal) % 1;
        var offset = null;
        if (decimal < 1 / 8) {
          offset = null;
        }

        if (decimal >= 1 / 8 && decimal < (1 / 8 + 1 / 4) / 2) {
          offset = "1/8";
        }

        if ((decimal >= (1 / 8 + 1 / 4) / 2 && decimal < 1 / 4) || (decimal >= 1 / 4 && decimal < (1 / 4 + 3 / 8) / 2)) {
          offset = "1/4";
        }

        if ((decimal >= (1 / 4 + 3 / 8) / 2 && decimal < 3 / 8) || (decimal >= 3 / 8 && decimal < (3 / 8 + 1 / 2) / 2)) {
          offset = "3/8";
        }

        if ((decimal >= (3 / 8 + 1 / 2) / 2 && decimal < 1 / 2) || (decimal >= 1 / 2 && decimal < (1 / 2 + 5 / 8) / 2)) {
          offset = "1/2";
        }

        if ((decimal >= (1 / 2 + 5 / 8) / 2 && decimal < 5 / 8) || (decimal >= 5 / 8 && decimal < (5 / 8 + 3 / 4) / 2)) {
          offset = "5/8";
        }

        if ((decimal >= (5 / 8 + 3 / 4) / 2 && decimal < 3 / 4) || (decimal >= 3 / 4 && decimal < (3 / 4 + 7 / 8) / 2)) {
          offset = "3/4";
        }

        if ((decimal >= (3 / 4 + 7 / 8) / 2 && decimal < 7 / 8) || (decimal >= 7 / 8 && decimal < (7 / 8 + 1) / 2)) {
          offset = "7/8";
        }

        if (decimal >= (7 / 8 + 1) / 2) {
          offset = '1';
        }

        if (offset == '1') {
          this.objectDetailsForm.controls[flag].setValue(Math.trunc((parseFloat(value) * cal)) + 1);
        } else {
          if (Math.trunc((parseFloat(value) * cal)) == 0) {
            this.objectDetailsForm.controls[flag].setValue(offset);
          } else {
            if (offset == null) {
              this.objectDetailsForm.controls[flag].setValue(Math.trunc((parseFloat(value) * cal)));
            } else {
              this.objectDetailsForm.controls[flag].setValue(Math.trunc((parseFloat(value) * cal)) + ' ' + offset);
            }
          }
        }
      } else {
        this.objectDetailsForm.controls[flag].setValue(((parseFloat(value) * cal).toFixed(2)));
      }
    } else {
      this.objectDetailsForm.controls[flag].setValue(null);
    }
  }

  // Record last CR for multiple artists in sessionStorage.
  getLastCR() {
    let lastCR = '';
    this.lastCR.forEach((artist, index) => {
      if (index === this.lastCR.length - 1) {
        lastCR += artist.CRId;
      }
      else {
        lastCR += artist.CRId + ",";
      }
    });
    sessionStorage.setItem('lastcr', lastCR);
  }

  // Get Artist Catalog Raisonnees
  public selArtist;
  public getArtistCatalogRaisonnees(data) {
    this._artistService.getIndividualArtist(data)
      .subscribe(Response => {
        if (Response.artistCatalogueRaisonees.length > 0) {
          Response.artistCatalogueRaisonees.forEach((data, index) => {
            this.propertyCatalogRaisonnee = this.propertyCatalogRaisonnee.concat(data);
            // Add artist's last CR to 'lastCR []'
            if (index === (Response.artistCatalogueRaisonees.length - 1)) {
              this.lastCR.push({ "artistId": Response.id, "CRId": data.id });
            }
            if (this.checkedArtist.length > 1) {
              this.getLastCR();
            }
            else {
              sessionStorage.removeItem('lastcr');
            }
          });
        }
      }, resFileError => this.errorMsg = resFileError);
  }

  // Remove Property Catalog Raisonnees
  public removeArtistCatalogRaisonnees(data) {
    this._artistService.getIndividualArtist(data)
      .subscribe(Response => {
        if (Response.artistCatalogueRaisonees.length > 0) {
          Response.artistCatalogueRaisonees.forEach(data => {
            this.propertyCatalogRaisonnee = this.propertyCatalogRaisonnee.filter(function (CRdata) {
              return data.id != CRdata.id;
            });
          });
        }
      }, resFileError => this.errorMsg = resFileError);
  }

  // Update Property Catalog Raisonnees
  public updatePropertyCatalogRaisonnees(flag: string) {
    var finalCRarr = [];
    this.propertyCatalogRaisonnee.forEach(data => {
      if (data.id && (flag == 'save')) {
        if (data.numberCR || data.volumeCR) {
          finalCRarr.push({ "artistCatalogueRaisoneeId": data.id, "number": data.numberCR ? data.numberCR : null, "volume": data.volumeCR ? data.volumeCR : null });
        }
      }
    })
    this.propertyCatalogRaisonnee = finalCRarr;
    return this.propertyCatalogRaisonnee;
  }

  //edition number modal
  public editionSizeObject: any;
  EditionModalPrompt() {
    this.dialogService.addDialog(EditionModalComponent,
      { editionSizeTypes: this.editionSizeTypes, editionSizeObject: this.editionSizeObject, newObject: true },
      { backdropColor: 'rgba(220,220,220,0.5)' })
      .subscribe((message) => {
        this.editionSizeObject = message;
        this.objectDetailsForm.controls['editionSizeTotal'].setValue(this.editionSizeObject && this.editionSizeObject.editionSizeEditionOverride ? this.editionSizeObject.editionSizeEditionOverride : (this.editionSizeObject && this.editionSizeObject.editionSizeDisplay ? this.editionSizeObject.editionSizeDisplay : null));

      });
  }

  public confirmResult: boolean = false;
  cancel() {
    if ((this.checkedArtist && this.checkedArtist.length) || this.objectDetailsForm.value.imagePath || this.objectDetailsForm.value.title || this.objectDetailsForm.value.originalTitle || this.objectDetailsForm.value.signature || this.objectDetailsForm.value.medium || this.objectDetailsForm.value.category || this.objectDetailsForm.value.subCategory || this.objectDetailsForm.value.presizeName || this.objectDetailsForm.value.parts || this.objectDetailsForm.value.heightCm || this.objectDetailsForm.value.widthCm || this.objectDetailsForm.value.depthCm || this.objectDetailsForm.value.heightIn || this.objectDetailsForm.value.widthIn || this.objectDetailsForm.value.depthIn || this.objectDetailsForm.value.sizeText || this.objectDetailsForm.value.yearStart || this.objectDetailsForm.value.yearEnd || this.objectDetailsForm.value.castYearStart || this.objectDetailsForm.value.castYearEnd || this.objectDetailsForm.value.yearText || this.objectDetailsForm.value.yearCirca || this.objectDetailsForm.value.castYearCirca || this.objectDetailsForm.value.posthumous || this.objectDetailsForm.value.editionNumber || this.objectDetailsForm.value.editionName || this.objectDetailsForm.value.editionSizeTotal || this.objectDetailsForm.value.weight || this.objectDetailsForm.value.foundry || this.objectDetailsForm.value.certificate || this.objectDetailsForm.value.signed || this.objectDetailsForm.value.stamped || this.objectDetailsForm.value.authentication || (this.objectDetailsForm.value.archiveNumbers && this.objectDetailsForm.value.archiveNumbers.length) || this.objectDetailsForm.value.sortCode) {
      document.getElementsByTagName("body")[0].classList.add("positionFixed");
      this.dialogService.addDialog(ConfirmComponent, {
        title: '',
        message: 'You are about to leave without saving. Would you like to save your changes?'
      })
        .subscribe((isConfirmed) => {
          document.getElementsByTagName("body")[0].classList.remove("positionFixed");
          this.confirmResult = isConfirmed;
          if (this.confirmResult) {
            this.apply(this.objectDetailsForm.value);
          } else {
            this.close();
          }
        });
    } else {
      this.close();
    }
  }
  validateSelectedItem(selectedItem, list) {
    if (selectedItem) {
      selectedItem = selectedItem.toLowerCase();
      let selectListObject;
      if (list === "category") {
        let index = this.categories.findIndex(list => list.name.toLowerCase() === selectedItem);
        if (!(index > -1)) {
          this.objectDetailsForm.controls['category'].setValue('');
        }
      }
      else if (list === "subCategory") {
        let index = this.subcategories.findIndex(list => list.name.toLowerCase() === selectedItem);
        if (!(index > -1)) {
          this.objectDetailsForm.controls['subCategory'].setValue('');
        }
      }
      else if (list === "presizeName") {
        let index = this.presizeNames.findIndex(list => list.name.toLowerCase() === selectedItem);
        if (!(index > -1)) {
          this.objectDetailsForm.controls['presizeName'].setValue('');
        }
      }
      else if (list === "editionName") {
        let index = this.editionTypes.findIndex(list => list.name.toLowerCase() === selectedItem);
        if (!(index > -1)) {
          this.objectDetailsForm.controls['editionName'].setValue('');
        }
      }
      else if (list === "flagsCountry") {
        let index = this.countries.findIndex(list => list['name'].toLowerCase() === selectedItem);
        if (!(index > -1)) {
          this.objectDetailsForm.patchValue({
            'flags': { flagsCountry: '' }
          });
        }
      } else {
        return false;
      }
    }
  }
  //Keyboard keys shortcut
  keyboardShortCutFunctionModal(e) {
    switch (this._keyboardkeysService.getKeyboardResponse(e)) {
      case 'addRecord':
        this.apply(this.objectDetailsForm.value);
        break;
      case 'enter':
        this.apply(this.objectDetailsForm.value);
        break;
    }
  }

  onPaste(event) {
    if (parseInt(event.clipboardData.getData('text/plain')) < 0 || isNaN(parseInt(event.clipboardData.getData('text/plain'))))
      return false;
    return true;
  }

  onSignaturePaste(event) {
    if (event.clipboardData.getData('text/plain').length > 500)
      return false;
    return true;
  }


  // 'Caret' (select list) click event 
  private openList(list, listName) {
    if (listName == 'category') {
      this.categoryListOpen = !this.categoryListOpen;
      if (this.categoryListOpen === true) {
        list.open();
      }
      else {
        list.close();
      }
    }
    if (listName == 'subcategory') {
      this.subCategoryListOpen = !this.subCategoryListOpen;
      if (this.subCategoryListOpen === true) {
        list.open();
      }
      else {
        list.close();
      }
    }
    if (listName == 'presizename') {
      this.presizeNameListOpen = !this.presizeNameListOpen;
      if (this.presizeNameListOpen === true) {
        list.open();
      }
      else {
        list.close();
      }
    }
    if (listName == 'editionname') {
      this.editionNameListOpen = !this.editionNameListOpen;
      if (this.editionNameListOpen === true) {
        list.open();
      }
      else {
        list.close();
      }
    }
    if (listName == 'flagscountry') {
      this.flagsCountryListOpen = !this.flagsCountryListOpen;
      if (this.flagsCountryListOpen === true) {
        list.open();
      }
      else {
        list.close();
      }
    }
  }

  // Close drop-down lists
  closeList(list, listName) {
    list.close();

    if (listName == 'category') {
      this.categoryListOpen = !this.categoryListOpen;
    }
    if (listName == 'subcategory') {
      this.subCategoryListOpen = !this.subCategoryListOpen;
    }
    if (listName == 'presizename') {
      this.presizeNameListOpen = !this.presizeNameListOpen;
    }
    if (listName == 'editionname') {
      this.editionNameListOpen = !this.editionNameListOpen;
    }
    if (listName == 'flagscountry') {
      this.flagsCountryListOpen = !this.flagsCountryListOpen;
    }
  }

  // EOS-838 - Reject new values in autocomplete list (Artist Name field).
  resetArtistNameInput() {
    this.objectDetailsForm.controls['artistData'].setValue('');
  }
  //checkbox focus event
  addOutline(event) {
    let parent = event.srcElement.parentElement;
    parent.querySelector(".fa-check").classList.add("outline");
  }
  removeOutline(event) {
    let parent = event.srcElement.parentElement;
    parent.querySelector(".fa-check").classList.remove("outline");
  }
}