import { Component, OnInit, HostListener } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';

export interface EditionModal {
  editionSizeTypes: any;
  editionSizeObject: any;
  newObject: any;
}

@Component({
  selector: 'prompt',
  template: `<div class="modal-dialog new-edition-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<h4 class="modal-title pull-left"> Edition Size</h4>
			<div class="pull-right">
				<button type="button" class="btn btn-success" [disabled]="!(
					(editionSizeSeriesFlag && editionSizeArtistProofFlag && editionSizeTrialProofFlag &&
					editionSizeBonATirerProofFlag && editionSizePrintersProofFlag && editionSizeHorsCommerceProofFlag&&
					editionSizeDedicatedProofFlag && editionSizeMuseumProofFlag) &&
					(editionSizeForm.value.editionSizeSeries >= -1 && editionSizeForm.value.editionSizeSeries <= 99000)&&
					(editionSizeForm.value.editionSizeArtistProof >= -1 && editionSizeForm.value.editionSizeArtistProof <= 99000)&&
					(editionSizeForm.value.editionSizeTrialProof >= -1 && editionSizeForm.value.editionSizeTrialProof <= 99000)&&
					(editionSizeForm.value.editionSizeBonATirerProof >= -1 && editionSizeForm.value.editionSizeBonATirerProof <= 99000)&&
					(editionSizeForm.value.editionSizePrintersProof >= -1 && editionSizeForm.value.editionSizePrintersProof <= 99000)&&
					(editionSizeForm.value.editionSizeHorsCommerceProof >= -1 && editionSizeForm.value.editionSizeHorsCommerceProof <= 99000)&&
					(editionSizeForm.value.editionSizeDedicatedProof >= -1 && editionSizeForm.value.editionSizeDedicatedProof <= 99000)&&
					(editionSizeForm.value.editionSizeMuseumProof >= -1 && editionSizeForm.value.editionSizeMuseumProof <= 99000))"  (click)="submitForm('save',editionSizeForm.value)">Save</button>
				<button type="button" class="btn btn-default" (click)="submitForm('close',editionSizeForm.value)">Cancel</button>
			</div>
		</div>
		<div class="modal-body">
			<form [formGroup]="editionSizeForm" [class]="newObject?'noModeChange':''" novalidate>
				<div class="col-sm-12">
					<span class="col-sm-5 modal-label">Type</span>
					<span class="col-sm-7">
						<select formControlName="editionSizeType" class="form-control" >
							<option *ngFor="let editionSizeType of editionSizeTypes" value={{editionSizeType.name}}>{{editionSizeType.name}}</option>
						</select>
					</span>
				</div>
				<div class="col-sm-12">
					<span class="col-sm-5 modal-label">Series</span>
					<span class="col-sm-7">
					<input type="number" [readonly]="(editionSizeForm.value.editionSizeType==='Unique')" min="-1" (paste)="onPaste($event)" (ngModelChange)="flagModeChange('editionSizeSeriesFlag',editionSizeForm.value.editionSizeSeries)" onkeydown="javascript: return (event.keyCode == 190 || event.keyCode == 69 || event.keyCode == 187 || event.keyCode == 107) ? false : true" formControlName="editionSizeSeries" class="form-control sizeForm" placeholder="Series" value="{{editionSizeForm.value.editionSizeSeries}}">
						<p *ngIf=" !(editionSizeForm.value.editionSizeSeries===0 || editionSizeForm.value.editionSizeSeries===-1) && editionSizeForm.controls['editionSizeSeries'].hasError('pattern')" class="alert alert-danger">
							Please enter a number in the range -1 to 99000. Decimals are not accepted.
						</p>
					</span>
				</div>
				<div class="col-sm-12">
					<span class="col-sm-5 modal-label">Artist Proof</span>
					<span class="col-sm-7">
						<input type="number" [readonly]="(editionSizeForm.value.editionSizeType==='Unique')" min="-1" (paste)="onPaste($event)" (ngModelChange)="flagModeChange('editionSizeArtistProofFlag',editionSizeForm.value.editionSizeArtistProof)" onkeydown="javascript: return (event.keyCode == 190 || event.keyCode == 69 || event.keyCode == 187 || event.keyCode == 107) ? false : true" formControlName="editionSizeArtistProof" class="form-control sizeArtistProof" placeholder="Artist Proof" value="{{editionSizeForm.value.editionSizeArtistProof}}">
						<p *ngIf="!(editionSizeForm.value.editionSizeArtistProof===0 || editionSizeForm.value.editionSizeArtistProof===-1) && editionSizeForm.controls['editionSizeArtistProof'].hasError('pattern')" class="alert alert-danger">
							Please enter a number in the range -1 to 99000. Decimals are not accepted.
						</p>
					</span>
				</div>
				<div class="col-sm-12">
					<span class="col-sm-5 modal-label">Trial Proof</span>
					<span class="col-sm-7">
						<input type="number" [readonly]="(editionSizeForm.value.editionSizeType==='Unique')" min="-1" (paste)="onPaste($event)" (ngModelChange)="flagModeChange('editionSizeTrialProofFlag',editionSizeForm.value.editionSizeTrialProof)" onkeydown="javascript: return (event.keyCode == 190 || event.keyCode == 69 || event.keyCode == 187 || event.keyCode == 107) ? false : true" formControlName="editionSizeTrialProof" class="form-control sizeTrialProof" placeholder="Trial Proof" value="{{editionSizeForm.value.editionSizeTrialProof}}">
						<p *ngIf="!(editionSizeForm.value.editionSizeTrialProof===0 || editionSizeForm.value.editionSizeTrialProof===-1) && editionSizeForm.controls['editionSizeTrialProof'].hasError('pattern')" class="alert alert-danger">
							Please enter a number in the range -1 to 99000. Decimals are not accepted.
						</p>
					</span>
				</div>
				<div class="col-sm-12">
					<span class="col-sm-5 modal-label">Bon &agrave; Tirer Proof</span>
					<span class="col-sm-7">
						<input type="number" [readonly]="(editionSizeForm.value.editionSizeType==='Unique')" min="-1" (paste)="onPaste($event)" (ngModelChange)="flagModeChange('editionSizeBonATirerProofFlag',editionSizeForm.value.editionSizeBonATirerProof)" onkeydown="javascript: return (event.keyCode == 190 || event.keyCode == 69 || event.keyCode == 187 || event.keyCode == 107) ? false : true" formControlName="editionSizeBonATirerProof" class="form-control sizeBonATirerProof" placeholder="Bon à Tirer Proof" value="{{editionSizeForm.value.editionSizeBonATirerProof}}">
						<p *ngIf="!(editionSizeForm.value.editionSizeBonATirerProof===0 || editionSizeForm.value.editionSizeBonATirerProof===-1) && editionSizeForm.controls['editionSizeBonATirerProof'].hasError('pattern')" class="alert alert-danger">
							Please enter a number in the range -1 to 99000. Decimals are not accepted.
						</p>
					</span>
				</div>
				<div class="col-sm-12">
					<span class="col-sm-5 modal-label">Printer's Proof</span>
					<span class="col-sm-7">
						<input type="number" [readonly]="(editionSizeForm.value.editionSizeType==='Unique')" min="-1" (paste)="onPaste($event)" (ngModelChange)="flagModeChange('editionSizePrintersProofFlag',editionSizeForm.value.editionSizePrintersProof)" onkeydown="javascript: return (event.keyCode == 190 || event.keyCode == 69 || event.keyCode == 187 || event.keyCode == 107) ? false : true" formControlName="editionSizePrintersProof" class="form-control sizePrintersProof" placeholder="Printer's Proof" value="{{editionSizeForm.value.editionSizePrintersProof}}">
						<p *ngIf="!(editionSizeForm.value.editionSizePrintersProof===0 || editionSizeForm.value.editionSizePrintersProof===-1) && editionSizeForm.controls['editionSizePrintersProof'].hasError('pattern')" class="alert alert-danger">
							Please enter a number in the range -1 to 99000. Decimals are not accepted.
						</p>
					</span>
				</div>
				<div class="col-sm-12">
					<span class="col-sm-5 modal-label">Hors Commerce Proof</span>
					<span class="col-sm-7">
						<input type="number" [readonly]="(editionSizeForm.value.editionSizeType==='Unique')" min="-1" (paste)="onPaste($event)" (ngModelChange)="flagModeChange('editionSizeHorsCommerceProofFlag',editionSizeForm.value.editionSizeHorsCommerceProof)" onkeydown="javascript: return (event.keyCode == 190 || event.keyCode == 69 || event.keyCode == 187 || event.keyCode == 107) ? false : true" formControlName="editionSizeHorsCommerceProof" class="form-control sizeHorsCommerceProof" placeholder="Hors Commerce Proof" value="{{editionSizeForm.value.editionSizeHorsCommerceProof}}">
						<p *ngIf="!(editionSizeForm.value.editionSizeHorsCommerceProof===0 || editionSizeForm.value.editionSizeHorsCommerceProof===-1) && editionSizeForm.controls['editionSizeHorsCommerceProof'].hasError('pattern')" class="alert alert-danger">
							Please enter a number in the range -1 to 99000. Decimals are not accepted.
						</p>
					</span>
				</div>
				<div class="col-sm-12">
					<span class="col-sm-5 modal-label">Dedicated Proof</span>
					<span class="col-sm-7">
						<input type="number" [readonly]="(editionSizeForm.value.editionSizeType==='Unique')" min="-1" (paste)="onPaste($event)" (ngModelChange)="flagModeChange('editionSizeDedicatedProofFlag',editionSizeForm.value.editionSizeDedicatedProof)" onkeydown="javascript: return (event.keyCode == 190 || event.keyCode == 69 || event.keyCode == 187 || event.keyCode == 107) ? false : true" formControlName="editionSizeDedicatedProof" class="form-control sizeDedicatedProof" placeholder="Dedicated Proof" value="{{editionSizeForm.value.editionSizeDedicatedProof}}">
						<p *ngIf="!(editionSizeForm.value.editionSizeDedicatedProof===0 || editionSizeForm.value.editionSizeDedicatedProof===-1) && editionSizeForm.controls['editionSizeDedicatedProof'].hasError('pattern')" class="alert alert-danger">
							Please enter a number in the range -1 to 99000. Decimals are not accepted.
						</p>
					</span>
				</div>
				<div class="col-sm-12">
					<span class="col-sm-5 modal-label">Museum Proof</span>
					<span class="col-sm-7">
						<input type="number" [readonly]="(editionSizeForm.value.editionSizeType==='Unique')" min="-1" (paste)="onPaste($event)" (ngModelChange)="flagModeChange('editionSizeMuseumProofFlag',editionSizeForm.value.editionSizeMuseumProof)" onkeydown="javascript: return (event.keyCode == 190 || event.keyCode == 69 || event.keyCode == 187 || event.keyCode == 107) ? false : true" formControlName="editionSizeMuseumProof" class="form-control sizeMuseumProof" placeholder="Museum Proof" value="{{editionSizeForm.value.editionSizeMuseumProof}}">
						<p *ngIf="!(editionSizeForm.value.editionSizeMuseumProof===0 || editionSizeForm.value.editionSizeMuseumProof===-1) && editionSizeForm.controls['editionSizeMuseumProof'].hasError('pattern')" class="alert alert-danger">
							Please enter a number in the range -1 to 99000. Decimals are not accepted.
						</p>
					</span>
				</div>
				<div class="col-sm-12 m-t-10">
					<span class="col-sm-5 modal-label">Edition Override</span>
					<span class="col-sm-7">
						<textarea rows="5" formControlName="editionSizeEditionOverride" class="form-control f-11 b-1-ddd edition-size-override" maxlength="200" (keyup)="overrideEditionDisplay()" placeholder="Edition Override"></textarea>
					</span>
				</div>
				<div class="col-sm-12">
					<span class="col-sm-5 modal-label">Total </span>
					<span class="col-sm-7">
						<input type="number" onkeydown="javascript: return (event.keyCode == 190 || event.keyCode == 69 || event.keyCode == 187) ? false : true" readonly formControlName="editionSizeTotal"
						value="{{getTotal()}}"
						class="form-control" placeholder="Total">
					</span>
				</div>
				<div class="col-sm-12">
					<span class="col-sm-5 modal-label">Display</span>
					<span class="col-sm-7">
						<textarea rows="2" formControlName="editionSizeDisplay" readonly
							value="{{getDisplayValue()}}" class="form-control">
						</textarea>
					</span>
				</div>
				<div class="col-sm-12 m-t-10">
					<span class="col-sm-5 modal-label">Edition Notes</span>
					<span class="col-sm-7">
						<textarea rows="5" formControlName="editionSizeNotes" class="form-control f-11 b-1-ddd edition-size-notes" maxlength="300" placeholder="Edition Notes"></textarea>
					</span>
				</div>
				<div class="clearfix"></div>
				<!-- Any validation -->
				<div class="col-sm-12" *ngIf="alertMsg">
					<span class="text-danger" #dvError ng-if="(test > 0 && test < 31)">Please enter values from 1 to 30</span>
				</div>
			</form>
		</div>
		<div class="modal-footer"></div>
		</div>
	</div>`
})
export class EditionModalComponent extends DialogComponent<EditionModal, string> implements EditionModal, OnInit {
  message: any;
  editionSizeTypes: any;
  editionSizeObject: any;
  newObject: any;

  public editionSizeForm: FormGroup;
  private _editionOverrideEditFlag: boolean = false;

  constructor(private _fb: FormBuilder, dialogService: DialogService) {
    super(dialogService);
  }

  ngOnInit() {
    var Notes = "";
    this.editionSizeForm = this._fb.group({
      editionSizeType: [(this.editionSizeObject && this.editionSizeObject.editionSizeType) ? this.editionSizeObject.editionSizeType : 'Unspecified', Validators.required],
      editionSizeSeries: [(this.editionSizeObject && this.editionSizeObject.editionSizeSeries) ? this.editionSizeObject.editionSizeSeries : null, [Validators.pattern('^0*([0-9]|[1-8][0-9]|9[0-9]|[1-8][0-9]{2}|9[0-8][0-9]|99[0-9]|[1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9]|[1-8][0-9]{4}|9[0-8][0-9]{3}|99000)$'), Validators.maxLength(5)]],
      editionSizeArtistProof: [(this.editionSizeObject && this.editionSizeObject.editionSizeArtistProof) ? this.editionSizeObject.editionSizeArtistProof : null, [Validators.pattern('^0*([0-9]|[1-8][0-9]|9[0-9]|[1-8][0-9]{2}|9[0-8][0-9]|99[0-9]|[1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9]|[1-8][0-9]{4}|9[0-8][0-9]{3}|99000)$'), Validators.maxLength(5)]],
      editionSizeTrialProof: [(this.editionSizeObject && this.editionSizeObject.editionSizeTrialProof) ? this.editionSizeObject.editionSizeTrialProof : null, [Validators.pattern('^0*([0-9]|[1-8][0-9]|9[0-9]|[1-8][0-9]{2}|9[0-8][0-9]|99[0-9]|[1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9]|[1-8][0-9]{4}|9[0-8][0-9]{3}|99000)$'), Validators.maxLength(5)]],
      editionSizeBonATirerProof: [(this.editionSizeObject && this.editionSizeObject.editionSizeBonATirerProof) ? this.editionSizeObject.editionSizeBonATirerProof : null, [Validators.pattern('^0*([0-9]|[1-8][0-9]|9[0-9]|[1-8][0-9]{2}|9[0-8][0-9]|99[0-9]|[1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9]|[1-8][0-9]{4}|9[0-8][0-9]{3}|99000)$'), Validators.maxLength(5)]],
      editionSizePrintersProof: [(this.editionSizeObject && this.editionSizeObject.editionSizePrintersProof) ? this.editionSizeObject.editionSizePrintersProof : null, [Validators.pattern('^0*([0-9]|[1-8][0-9]|9[0-9]|[1-8][0-9]{2}|9[0-8][0-9]|99[0-9]|[1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9]|[1-8][0-9]{4}|9[0-8][0-9]{3}|99000)$'), Validators.maxLength(5)]],
      editionSizeHorsCommerceProof: [(this.editionSizeObject && this.editionSizeObject.editionSizeHorsCommerceProof) ? this.editionSizeObject.editionSizeHorsCommerceProof : null, [Validators.pattern('^0*([0-9]|[1-8][0-9]|9[0-9]|[1-8][0-9]{2}|9[0-8][0-9]|99[0-9]|[1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9]|[1-8][0-9]{4}|9[0-8][0-9]{3}|99000)$'), Validators.maxLength(5)]],
      editionSizeDedicatedProof: [(this.editionSizeObject && this.editionSizeObject.editionSizeDedicatedProof) ? this.editionSizeObject.editionSizeDedicatedProof : null, [Validators.pattern('^0*([0-9]|[1-8][0-9]|9[0-9]|[1-8][0-9]{2}|9[0-8][0-9]|99[0-9]|[1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9]|[1-8][0-9]{4}|9[0-8][0-9]{3}|99000)$'), Validators.maxLength(5)]],
      editionSizeMuseumProof: [(this.editionSizeObject && this.editionSizeObject.editionSizeMuseumProof) ? this.editionSizeObject.editionSizeMuseumProof : null, [Validators.pattern('^0*([0-9]|[1-8][0-9]|9[0-9]|[1-8][0-9]{2}|9[0-8][0-9]|99[0-9]|[1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9]|[1-8][0-9]{4}|9[0-8][0-9]{3}|99000)$'), Validators.maxLength(5)]],
      editionSizeEditionOverride: [(this.editionSizeObject && this.editionSizeObject.editionSizeEditionOverride) ? this.editionSizeObject.editionSizeEditionOverride : null],
      editionSizeTotal: [(this.editionSizeObject && this.editionSizeObject.editionSizeTotal) ? this.editionSizeObject.editionSizeTotal : ''],
      editionSizeDisplay: [(this.editionSizeObject && this.editionSizeObject.editionSizeDisplay) ? this.editionSizeObject.editionSizeDisplay : ''],
      editionSizeNotes: [(this.editionSizeObject && this.editionSizeObject.editionSizeNotes) ? this.editionSizeObject.editionSizeNotes : null],
    });
  }

  submitForm(flag: string, data: any) {
    if (flag === 'save') {
      data.editionSizeDisplay = (
        (data.editionSizeType === 'Known' || data.editionSizeType === 'Unspecified') ? ((data.editionSizeSeries) != null ? (parseInt(data.editionSizeSeries) ? (parseInt(data.editionSizeSeries) == -1 ? 1 : parseInt(data.editionSizeSeries)) : '') : '') +
          ((data.editionSizeArtistProof) != null ? (parseInt(data.editionSizeArtistProof) > 0 ? '+' + parseInt(data.editionSizeArtistProof) + 'AP' : (parseInt(data.editionSizeArtistProof) == -1 ? '+AP' : '')) : '') +
          ((data.editionSizeTrialProof) != null ? (parseInt(data.editionSizeTrialProof) > 0 ? '+' + parseInt(data.editionSizeTrialProof) + 'TP' : (parseInt(data.editionSizeTrialProof) == -1 ? '+TP' : '')) : '') +
          ((data.editionSizeBonATirerProof) != null ? (parseInt(data.editionSizeBonATirerProof) > 0 ? '+' + parseInt(data.editionSizeBonATirerProof) + 'BAT' : (parseInt(data.editionSizeBonATirerProof) == -1 ? '+BAT' : '')) : '') +
          ((data.editionSizePrintersProof) != null ? (parseInt(data.editionSizePrintersProof) > 0 ? '+' + parseInt(data.editionSizePrintersProof) + 'PP' : (parseInt(data.editionSizePrintersProof) == -1 ? '+PP' : '')) : '') +
          ((data.editionSizeHorsCommerceProof) != null ? (parseInt(data.editionSizeHorsCommerceProof) > 0 ? '+' + parseInt(data.editionSizeHorsCommerceProof) + 'HC' : (parseInt(data.editionSizeHorsCommerceProof) == -1 ? '+HC' : '')) : '') +
          ((data.editionSizeDedicatedProof) != null ? (parseInt(data.editionSizeDedicatedProof) > 0 ? '+' + parseInt(data.editionSizeDedicatedProof) + 'DP' : (parseInt(data.editionSizeHorsCommerceProof) == -1 ? '+DP' : '')) : '') +
          ((data.editionSizeMuseumProof) != null ? (parseInt(data.editionSizeMuseumProof) > 0 ? '+' + parseInt(data.editionSizeMuseumProof) + 'MP' : (parseInt(data.editionSizeMuseumProof) == -1 ? '+MP' : '')) : '') : data.editionSizeType
      );
      data.editionSizeTotal = (parseInt(data.editionSizeSeries) ? parseInt(data.editionSizeSeries) : 0) +
        (parseInt(data.editionSizeArtistProof) ? parseInt(data.editionSizeArtistProof) : 0) +
        (parseInt(data.editionSizeTrialProof) ? parseInt(data.editionSizeTrialProof) : 0) +
        (parseInt(data.editionSizeBonATirerProof) ? parseInt(data.editionSizeBonATirerProof) : 0) +
        (parseInt(data.editionSizePrintersProof) ? parseInt(data.editionSizePrintersProof) : 0) +
        (parseInt(data.editionSizeHorsCommerceProof) ? parseInt(data.editionSizeHorsCommerceProof) : 0) +
        (parseInt(data.editionSizeDedicatedProof) ? parseInt(data.editionSizeDedicatedProof) : 0) +
        (parseInt(data.editionSizeMuseumProof) ? parseInt(data.editionSizeMuseumProof) : 0);
      this.result = data;
      this.close();
    } else if (flag === 'close') {
      if (this.editionSizeObject) {
        this.editionSizeObject.editionSizeDisplay = ((this.editionSizeObject.editionSizeType === 'Known' || this.editionSizeObject.editionSizeType === 'Unspecified') ?
          ((this.editionSizeObject.editionSizeSeries) != null ? (parseInt(this.editionSizeObject.editionSizeSeries) ? (parseInt(this.editionSizeObject.editionSizeSeries) === -1 ? 1 : parseInt(this.editionSizeObject.editionSizeSeries)) : '') : '') +
          ((this.editionSizeObject.editionSizeArtistProof) != null ? (parseInt(this.editionSizeObject.editionSizeArtistProof) > 0 ? '+' + parseInt(this.editionSizeObject.editionSizeArtistProof) + 'AP' : (parseInt(this.editionSizeObject.editionSizeArtistProof) == -1 ? '+AP' : '')) : '') +
          ((this.editionSizeObject.editionSizeTrialProof) != null ? (parseInt(this.editionSizeObject.editionSizeTrialProof) > 0 ? '+' + parseInt(this.editionSizeObject.editionSizeTrialProof) + 'TP' : (parseInt(this.editionSizeObject.editionSizeTrialProof) == -1 ? '+TP' : '')) : '') +
          ((this.editionSizeObject.editionSizeBonATirerProof) != null ? (parseInt(this.editionSizeObject.editionSizeBonATirerProof) > 0 ? '+' + parseInt(this.editionSizeObject.editionSizeBonATirerProof) + 'BAT' : (parseInt(this.editionSizeObject.editionSizeBonATirerProof) == -1 ? '+BAT' : '')) : '') +
          ((this.editionSizeObject.editionSizePrintersProof) != null ? (parseInt(this.editionSizeObject.editionSizePrintersProof) > 0 ? '+' + parseInt(this.editionSizeObject.editionSizePrintersProof) + 'PP' : (parseInt(this.editionSizeObject.editionSizePrintersProof) == -1 ? '+PP' : '')) : '') +
          ((this.editionSizeObject.editionSizeHorsCommerceProof) != null ? (parseInt(this.editionSizeObject.editionSizeHorsCommerceProof) > 0 ? '+' + parseInt(this.editionSizeObject.editionSizeHorsCommerceProof) + 'HC' : (parseInt(this.editionSizeObject.editionSizeHorsCommerceProof) == -1 ? '+HC' : '')) : '') +
          ((this.editionSizeObject.editionSizeDedicatedProof) != null ? (parseInt(this.editionSizeObject.editionSizeDedicatedProof) > 0 ? '+' + parseInt(this.editionSizeObject.editionSizeDedicatedProof) + 'DP' : (parseInt(this.editionSizeObject.editionSizeDedicatedProof) == -1 ? '+DP' : '')) : '') +
          ((this.editionSizeObject.editionSizeMuseumProof) != null ? (parseInt(this.editionSizeObject.editionSizeMuseumProof) > 0 ? '+' + parseInt(this.editionSizeObject.editionSizeMuseumProof) + 'MP' : (parseInt(this.editionSizeObject.editionSizeMuseumProof) == -1 ? '+MP' : '')) : '')
          : this.editionSizeObject.editionSizeType);
        this.result = this.editionSizeObject;
        this.close();
      } else {
        this.close();
      }
    }
  }

  onPaste(event) {
    if (parseInt(event.clipboardData.getData('text/plain')) < 0 || isNaN(parseInt(event.clipboardData.getData('text/plain'))))
      return false;
    return true;
  }

  getDisplayValue() {
    if (!(this.editionSizeForm.controls['editionSizeEditionOverride'].value &&
      this.editionSizeForm.controls['editionSizeEditionOverride'].value.replace(/\s/g, "") != '')) {
      return ((this.editionSizeForm.value.editionSizeType === 'Known' ||
        this.editionSizeForm.value.editionSizeType === 'Unspecified') ?
        (this.editionSizeForm.value.editionSizeSeries != null ?
          (this.editionSizeForm.value.editionSizeSeries >= -1 ? (this.editionSizeForm.value.editionSizeSeries === -1 ? 1 : this.editionSizeForm.value.editionSizeSeries) : '') : '') +
        (this.editionSizeForm.value.editionSizeArtistProof != null ?
          (this.editionSizeForm.value.editionSizeArtistProof > 0 ? '+' + this.editionSizeForm.value.editionSizeArtistProof + 'AP' : (this.editionSizeForm.value.editionSizeArtistProof == -1 ? '+AP' : '')) : '') +
        (this.editionSizeForm.value.editionSizeTrialProof != null ?
          (this.editionSizeForm.value.editionSizeTrialProof > 0 ? '+' + this.editionSizeForm.value.editionSizeTrialProof + 'TP' : (this.editionSizeForm.value.editionSizeTrialProof == -1 ? '+TP' : '')) : '') +
        (this.editionSizeForm.value.editionSizeBonATirerProof != null ?
          (this.editionSizeForm.value.editionSizeBonATirerProof > 0 ? '+' + this.editionSizeForm.value.editionSizeBonATirerProof + 'BAT' : (this.editionSizeForm.value.editionSizeBonATirerProof == -1 ? '+BAT' : '')) : '') +
        (this.editionSizeForm.value.editionSizePrintersProof != null ?
          (this.editionSizeForm.value.editionSizePrintersProof > 0 ? '+' + this.editionSizeForm.value.editionSizePrintersProof + 'PP' : (this.editionSizeForm.value.editionSizePrintersProof == -1 ? '+PP' : '')) : '') +
        (this.editionSizeForm.value.editionSizeHorsCommerceProof != null ?
          (this.editionSizeForm.value.editionSizeHorsCommerceProof > 0 ? '+' + this.editionSizeForm.value.editionSizeHorsCommerceProof + 'HC' : (this.editionSizeForm.value.editionSizeHorsCommerceProof == -1 ? '+HC' : '')) : '') +
        (this.editionSizeForm.value.editionSizeDedicatedProof != null ?
          (this.editionSizeForm.value.editionSizeDedicatedProof > 0 ? '+' + this.editionSizeForm.value.editionSizeDedicatedProof + 'DP' : (this.editionSizeForm.value.editionSizeDedicatedProof == -1 ? '+DP' : '')) : '') +
        (this.editionSizeForm.value.editionSizeMuseumProof != null ?
          (this.editionSizeForm.value.editionSizeMuseumProof > 0 ? '+' + this.editionSizeForm.value.editionSizeMuseumProof + 'MP' : (this.editionSizeForm.value.editionSizeMuseumProof == -1 ? '+MP' : '')) : '') :
        this.editionSizeForm.value.editionSizeType);
    }
    else {
      return this.editionSizeForm.controls['editionSizeEditionOverride'].value;
    }
  }

  getTotal() {
    return (this.editionSizeForm.value.editionSizeSeries === -1 ? 1 : (this.editionSizeForm.value.editionSizeSeries >= 0) ? this.editionSizeForm.value.editionSizeSeries : 0) +
      (this.editionSizeForm.value.editionSizeArtistProof === -1 ? 1 : (this.editionSizeForm.value.editionSizeArtistProof >= 0) ? this.editionSizeForm.value.editionSizeArtistProof : 0) +
      (this.editionSizeForm.value.editionSizeTrialProof === -1 ? 1 : (this.editionSizeForm.value.editionSizeTrialProof >= 0) ? this.editionSizeForm.value.editionSizeTrialProof : 0) +
      (this.editionSizeForm.value.editionSizeBonATirerProof === -1 ? 1 : (this.editionSizeForm.value.editionSizeBonATirerProof >= 0) ? this.editionSizeForm.value.editionSizeBonATirerProof : 0) +
      (this.editionSizeForm.value.editionSizePrintersProof === -1 ? 1 : (this.editionSizeForm.value.editionSizePrintersProof >= 0) ? this.editionSizeForm.value.editionSizePrintersProof : 0) +
      (this.editionSizeForm.value.editionSizeHorsCommerceProof === -1 ? 1 : (this.editionSizeForm.value.editionSizeHorsCommerceProof >= 0) ? this.editionSizeForm.value.editionSizeHorsCommerceProof : 0) +
      (this.editionSizeForm.value.editionSizeDedicatedProof === -1 ? 1 : (this.editionSizeForm.value.editionSizeDedicatedProof >= 0) ? this.editionSizeForm.value.editionSizeDedicatedProof : 0) +
      (this.editionSizeForm.value.editionSizeMuseumProof === -1 ? 1 : (this.editionSizeForm.value.editionSizeMuseumProof >= 0) ? this.editionSizeForm.value.editionSizeMuseumProof : 0);
  }

  overrideEditionDisplay() {
    if (this.editionSizeForm.controls['editionSizeEditionOverride'].value &&
      this.editionSizeForm.controls['editionSizeEditionOverride'].value.replace(/\s/g, "") != '') {
      this._editionOverrideEditFlag = true;
      this.editionSizeForm.controls['editionSizeDisplay'].setValue(this.editionSizeForm.controls['editionSizeEditionOverride'].value);
    }
    else {
      this._editionOverrideEditFlag = false;
      this.editionSizeForm.controls['editionSizeDisplay'].setValue(this.getDisplayValue());
    }
  }

  public editionSizeTypeFlag: boolean = true;
  public editionSizeSeriesFlag: boolean = true;
  public editionSizeArtistProofFlag: boolean = true;
  public editionSizeTrialProofFlag: boolean = true;
  public editionSizeBonATirerProofFlag: boolean = true;
  public editionSizePrintersProofFlag: boolean = true;
  public editionSizeHorsCommerceProofFlag: boolean = true;
  public editionSizeDedicatedProofFlag: boolean = true;
  public editionSizeMuseumProofFlag: boolean = true;

  @HostListener('keyup', ['$event'])
  inputChanged(e) {
    this.editionSizeSeriesFlag = (e && e.target && e.target.classList && (JSON.stringify(e.target.classList).indexOf('sizeForm') > -1) ? ((e.target.validationMessage.indexOf("Please enter a number") > -1) ? false : true) : this.editionSizeSeriesFlag);
    this.editionSizeArtistProofFlag = (e && e.target && e.target.classList && (JSON.stringify(e.target.classList).indexOf('sizeArtistProof') > -1) ? ((e.target.validationMessage.indexOf("Please enter a number") > -1) ? false : true) : this.editionSizeArtistProofFlag);
    this.editionSizeTrialProofFlag = (e && e.target && e.target.classList && (JSON.stringify(e.target.classList).indexOf('sizeTrialProof') > -1) ? ((e.target.validationMessage.indexOf("Please enter a number") > -1) ? false : true) : this.editionSizeTrialProofFlag);
    this.editionSizeBonATirerProofFlag = (e && e.target && e.target.classList && (JSON.stringify(e.target.classList).indexOf('sizeBonATirerProof') > -1) ? ((e.target.validationMessage.indexOf("Please enter a number") > -1) ? false : true) : this.editionSizeBonATirerProofFlag);
    this.editionSizePrintersProofFlag = (e && e.target && e.target.classList && (JSON.stringify(e.target.classList).indexOf('sizePrintersProof') > -1) ? ((e.target.validationMessage.indexOf("Please enter a number") > -1) ? false : true) : this.editionSizePrintersProofFlag);
    this.editionSizeHorsCommerceProofFlag = (e && e.target && e.target.classList && (JSON.stringify(e.target.classList).indexOf('sizeHorsCommerceProof') > -1) ? ((e.target.validationMessage.indexOf("Please enter a number") > -1) ? false : true) : this.editionSizeHorsCommerceProofFlag);
    this.editionSizeDedicatedProofFlag = (e && e.target && e.target.classList && (JSON.stringify(e.target.classList).indexOf('sizeDedicatedProof') > -1) ? ((e.target.validationMessage.indexOf("Please enter a number") > -1) ? false : true) : this.editionSizeDedicatedProofFlag);
    this.editionSizeMuseumProofFlag = (e && e.target && e.target.classList && (JSON.stringify(e.target.classList).indexOf('sizeMuseumProof') > -1) ? ((e.target.validationMessage.indexOf("Please enter a number") > -1) ? false : true) : this.editionSizeMuseumProofFlag);
  }
  flagModeChange(flag, value) {
    if (value >= -1) {
      this[value] = true;
    }
  }
}