import { Component } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { style } from '@angular/core/src/animation/dsl';

export interface DeleteObjectStatusModal {
  title: string;
  message: string;
}

@Component({
  selector: 'alert',
  template: `<div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" (click)="close()" >&times;</button>
                    <h4 class="modal-title">{{title || 'Confirm'}}</h4>
                    </div>
                    <div class="modal-body" *ngIf="status && status.allFailed==='false'">
                        <p>{{status.deleted}} objects deleted successfully.</p>
                        <p class="m-t-20">Failed to delete the following {{status.failedTotal}} objects:</p>
                        <ul class='failedObjectList'>
                            <li *ngFor="let obj of status.failedObjects">{{obj}}</li>
                        </ul>
                    </div>
                    <div class="modal-body" *ngIf="status && status.allFailed==='true'">
                        <p>Failed to delete the following {{status.failedTotal}} objects:</p>
                        <ul class='failedObjectList'>
                            <li *ngFor="let obj of status.failedObjects">{{obj}}</li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" (click)="close()">OK</button>
                    </div>
                </div>
             </div>`,
    styles: [
        `.failedObjectList {
            max-height: 450px;
            overflow-y: scroll;
            font-size: 11px;
            list-style: none;
            padding: 0;
            margin: 0;
            font-family: monospace;
        }
        ul.failedObjectList li {
            padding-left: 1em; 
            text-indent: -.7em;
        }
        ul.failedObjectList li::before {
            content: "• "; 
        }
        .m-t-20 {
            margin-top: 20px;
          }
        `
    ]
})
export class DeleteObjectStatusComponent extends DialogComponent<DeleteObjectStatusModal, null> implements DeleteObjectStatusModal {
  title: string;
  message;
  status;
  constructor(dialogService: DialogService) {
    super(dialogService);
  }

  ngOnInit() {
    this.status = {}; //Tracks the status of 'delete - object' operation.
    this.message = this.message.split('||'); // input received from 'showDeleteStatus()' in merge.component.ts
    this.status.allFailed = this.message[0].split(':')[1];
    this.status.failedTotal = this.message[1].split(':')[1];
    this.status.failedObjects = this.message[2].split(':')[1].split(',');
    this.status.deleted = this.message[3]?this.message[3].split(':')[1]:null;
  }
}