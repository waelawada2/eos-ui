import {Injectable} from "@angular/core";


@Injectable()
export class Helper {
    constructor() {}

    // Clear input field on clicking 'x' icon.
    clearInput(ele){
        (<HTMLInputElement>document.getElementById(ele)).value = '';
    }
    
    // Convert given date to formats: MM-DD-YYYY (or) YYYY-MM-DD (or) DD MMM YYYY
    public getDate(dt, format) {
		let day, month, year;
		let months = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];

		// If date has '-', 'T' and in the format YYYY-MM-DD (e.g. 2017-05-01T19:32:16.013Z)
		if(dt && (dt.indexOf("T") > -1) && (dt.indexOf("-") > -1)) {
			dt = dt.slice(0, dt.indexOf("T"));
			day = dt.split("-")[1];
			month = dt.split("-")[0];
			year = dt.split("-")[2];
			if(month.charAt(0)=="0") {
				month = month.slice(1,2);
			}

			month = months[month-1];
		}
		// If date has '-' and in the format MM-DD-YYYY (e.g. 05-01-2017)
		else if(dt && dt.indexOf("-") > -1) {
			dt = dt;
			day = dt.split("-")[1];
			month = dt.split("-")[0];
			year = dt.split("-")[2];
			if(month.charAt(0)=="0") {
				month = month.slice(1,2);
			}
			month = months[month-1];
		}
		// If date has ' ' (space) and in the format DD MMM YYYY (e.g. 01 May 2017)
		else if(dt && dt.indexOf(" ") > -1) {
			dt = dt;
			day = dt.split(" ")[0];
			month = dt.split(" ")[1];
			year = dt.split(" ")[2];
			if(/^[a-zA-Z()]$/.test(month.charAt(0))) {
				month = months.indexOf(month) + 1;
				month = month.length == 1 ? "0"+month: month;
				day = day.length == 1 ? "0"+day: day;
			}
			else if(!/^[a-zA-Z()]$/.test(month.charAt(0))){
				if(month.charAt(0)=="0") {
					month = month.slice(1,2);
				}
				month = months[month];
			}
		}

		if(format) {
			if(format.toLowerCase() == "mm-dd-yyyy") {
				return(month+"-"+day+"-"+year);
			}
			else if(format.toLowerCase() == "dd mmm yyyy") {
				return(day+" "+month+" "+year);
			}
		}
		else {
			return(day+" "+month+" "+year);
		}
  	}

	// Send e-mail
	sendEmail(message) {
		window.location.href = "mailto:?subject="+message.subject+"&body="+message.body;
	}
}