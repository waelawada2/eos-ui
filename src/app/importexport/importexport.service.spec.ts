import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { TestBed, inject } from '@angular/core/testing';
import { ImportexportService } from './importexport.service';
import { HttpModule } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';

describe('ImportexportService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ImportexportService],
      imports : [ HttpModule ],
      schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
    });
  });

  it('should be created', inject([ImportexportService], (service: ImportexportService) => {
    expect(service).toBeTruthy();
  }));
});
