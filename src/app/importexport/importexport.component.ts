import { Component, OnInit, ElementRef, Input, OnDestroy } from '@angular/core';
import { FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { Http, Response } from '@angular/http';
import { ActivatedRoute, Router, Params, NavigationStart, RoutesRecognized } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { ImportexportService } from './importexport.service';
import { HeaderService } from '../../app/header/header.service';
import { SlimLoadingBarService } from 'ng2-slim-loading-bar';
import { TosatService } from '../common/toasty-service';
import { ArtistService } from '../artist/artist.service';
import { ObjectService } from '../object/object.service';
import { Observable } from 'rxjs/Observable';
import { DialogService } from 'ng2-bootstrap-modal';
import { ConfirmComponent } from '../common/confirm-component';
import { AlertComponent } from '../common/alert-component';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { GeneralConstants, GeneralDictionary } from '../shared/constants';
import { NotificationsService } from 'angular2-notifications';
import { validateImportFileName } from '../event/shared/validators';
import { EVENTCONST } from '../event/shared/constants';
import { BlockScreenService } from '../event/shared/services/block-screen/block-screen.service';
import { openFileInNewTab } from '../event/shared/utils'
import { KeyboardkeysService } from '../../app/keyboardkeys/keyboardkeys.service';
import { AdminSavedListService } from '../adminSavedList/admin-saved-list.service';
import { ConfigurationLoaderService } from 'app/configuration.service'

import * as _ from 'lodash';

@Component({
  selector: 'app-importexport',
  templateUrl: './importexport.component.html',
  styleUrls: ['./importexport.component.css', './importexport.component.scss'],
  providers: [ImportexportService, HeaderService, ArtistService, KeyboardkeysService, AdminSavedListService]
})
export class ImportexportComponent implements OnInit, OnDestroy {

  public uploader: FileUploader;
  private intvl = 20000;
  public fileName;
  public errorMsg;
  public displayUploadMsg: any;
  public buttonName = GeneralConstants.ENTITY_IMPORT_EXPORT.EXPORT;

  private URL: string;
  private s3URL: string;

  public defaultEvents = [
    { name: GeneralConstants.ENTITY_IMPORT_EXPORT.ARTIST },
    { name: GeneralConstants.ENTITY_IMPORT_EXPORT.OBJECT },
    { name: GeneralConstants.ENTITY_IMPORT_EXPORT.ESTABLISHMENT },
    { name: GeneralConstants.ENTITY_IMPORT_EXPORT.SOURCE_NAME }]
  public eventArray: any = [
    ...this.defaultEvents,
    { name: GeneralConstants.ENTITY_IMPORT_EXPORT.SAVEDLIST },
  ]
  public keycloak = window['keycloak'];
  public userName = this.keycloak.tokenParsed ? this.keycloak.tokenParsed.preferred_username : null;
  public isAdmin = this.keycloak.resourceAccess["property-database-ui"].roles.includes("ADMIN") ? true : false;
  public userSaveList = [];
  public saveList;
  public csvImportExportForm: FormGroup;
  public options = GeneralConstants.NOTIFICATIONS.SETUP;
  public cancelFlag = false;
  public prevPage = 'home';
  public savedListData: Array<any>;
  public savedListData_copy: Array<any>;
  public totalpages: number = 0;
  public currentPage: number = 0;

  toggleFlag: number;

  public fileType = false;
  public fileProgress = false;
  public fileEvent: string = GeneralConstants.ENTITY_IMPORT_EXPORT.ARTIST;
  public filePollTasks;
  public exportsID;
  public XHRRequestNo = 0;

  constructor(private http: Http,
    private el: ElementRef,
    private route: ActivatedRoute,
    private _importexportService: ImportexportService,
    private _headerService: HeaderService,
    private slimLoadingBarService: SlimLoadingBarService,
    private router: Router,
    private _fb: FormBuilder,
    private _artistService: ArtistService,
    private _objectService: ObjectService,
    protected notificationSvc: NotificationsService,
    private blockScreenService: BlockScreenService,
    private tosatservice: TosatService,
    private dialogService: DialogService,
    private _keyboardkeysService: KeyboardkeysService,
    private _adminSavedListService: AdminSavedListService,
    private configuration: ConfigurationLoaderService,
  ) {
    this.URL = configuration.getSettings().apiUrl
    this.s3URL = configuration.getSettings().s3ApiUrl
    this.uploader = new FileUploader({ url: this.URL, itemAlias: 'uploadFile' })
  }

  ngOnInit() {
    // Track urlAfterRedirects
    this.router.events
      .filter(e => e instanceof RoutesRecognized)
      .pairwise()
      .subscribe((e: any[]) => {
        sessionStorage.setItem('import-export-referrer', e[0].urlAfterRedirects);
      });

    if (sessionStorage.getItem('import-export-referrer') == '/admin') {
      this.prevPage = 'admin';
    }
    else {
      this.prevPage = 'home';
    }


    this.csvImportExportForm = this._fb.group({
      savelist: new FormControl()
    });

    this.displayUploadMsg = 'none';
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
    };
  }

  ngOnDestroy() {
    sessionStorage.removeItem('import-export-referrer');
  }

  // Header Drop-down toggle mode

  toggleFunction(flag: number) {
    this.toggleFlag = this._headerService.headerToggle((flag === this.toggleFlag) ? 0 : flag);
  }



  onChangeFileType(_value) {

    if (_value === GeneralConstants.ENTITY_IMPORT_EXPORT.IMPORT.toLowerCase()) {
      this.fileType = true;
      this.buttonName = GeneralConstants.ENTITY_IMPORT_EXPORT.IMPORT
      this.eventArray = this.defaultEvents;
    } else {
      if (_value == "Delete") {
        this.fileType = false;
        this.buttonName = "Delete";
        this.eventArray = [{ name: GeneralConstants.ENTITY_IMPORT_EXPORT.SAVEDLIST }];
        this.fileEvent = GeneralConstants.ENTITY_IMPORT_EXPORT.SAVEDLIST;
        this.onChangeFileEvent(this.fileEvent);
      } else {
        this.fileType = false;
        this.buttonName = GeneralConstants.ENTITY_IMPORT_EXPORT.EXPORT;

        this.eventArray = [
          ...this.defaultEvents,
          { name: GeneralConstants.ENTITY_IMPORT_EXPORT.SAVEDLIST },
        ];
      }
    }
  }

  onChangeFileEvent(data) {
    if (data === GeneralConstants.ENTITY_IMPORT_EXPORT.SAVEDLIST) {
      this.fileEvent = data;
      if (this.buttonName == "Delete") {
        this._importexportService.getAllAdminSaveLists()
          .subscribe(response => {
            this.savedListData = response;
            this.savedListData_copy = response;
          }, resFileError => {
          })
      } else {
        this.curPage = 0;
        this.totPages = 0;
        this._importexportService
          .getSaveLists(this.userName, this.curPage)
          .subscribe(Response => {
            this.userSaveList = Response.content;
            this.totPages = Response.totalPages;
          }, resFileError => {
          });
      }
    } else {
      this.fileEvent = data;
    }
  }

  onChangeSaveList(value) {
    this.saveList = value;
  }

  upload() {
    if (this.fileType) {
      this.fileProgress = true;
      const inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#uploadFile');
      const fileCount: number = inputEl.files.length;
      const formData = new FormData();
      if (fileCount > 0) {
        this.slimLoadingBarService.start(() => {
        });
        if (inputEl.files.item(0).name.indexOf('.csv') > -1) {
          if (inputEl.files.item(0).name.toString().toLowerCase().length - 4 <= 218) {
            formData.append('file', inputEl.files.item(0));
            switch (this.fileEvent) {
              case GeneralConstants.ENTITY_IMPORT_EXPORT.ESTABLISHMENT:
                formData.append('type', GeneralConstants.IMPORT_TYPES.establishment);
                if (_.includes(_.toLower(inputEl.files.item(0).name), _.toLower(this.fileEvent))) {
                  this._importexportService
                    .getFileUploadEventURL(_.toLower(GeneralConstants.ENTITY_IMPORT_EXPORT.IMPORT), formData)
                    .subscribe(this.successUpload.bind(this), this.errorUpload.bind(this));
                } else {
                  this.showMessage(GeneralDictionary.NOTIFICATIONS.INVALID_FILENAME(this.fileEvent),
                    _.toLower(GeneralDictionary.NOTIFICATIONS.error));
                }
                break;
              case GeneralConstants.ENTITY_IMPORT_EXPORT.SOURCE_NAME:
                formData.append('type', GeneralConstants.IMPORT_TYPES.source_name);
                if (validateImportFileName(GeneralConstants.ENTITY_IMPORT_EXPORT.NAME,
                  GeneralConstants.ENTITY_IMPORT_EXPORT.NAME, inputEl.files.item(0).name)) {
                  this._importexportService
                    .getFileUploadEventURL(_.toLower(GeneralConstants.ENTITY_IMPORT_EXPORT.IMPORT), formData)
                    .subscribe(this.successUpload.bind(this), this.errorUpload.bind(this));
                } else {
                  this.showMessage(GeneralDictionary.NOTIFICATIONS.INVALID_FILENAME(this.fileEvent),
                    _.toLower(GeneralDictionary.NOTIFICATIONS.error));
                }
                break;
              case GeneralConstants.ENTITY_IMPORT_EXPORT.ARTIST:
                if (inputEl.files.item(0).name.toLowerCase().indexOf('artist') <= -1) {
                  this.tosatservice.addToastError('Error uploading file. Please ensure that the uploaded file is an Artist Screen file.', 'top-right');
                } else {
                  if (inputEl.files.item(0).name.toLowerCase().indexOf('.csv') <= -1) {
                    this.tosatservice.addToastError('Error uploading file. Please ensure that the uploaded file is .csv.', 'top-right');
                  } else {
                    this.uploadFile();
                  }
                }
                break;
              default:
                this.fileEvent = (this.fileEvent === GeneralConstants.ENTITY_IMPORT_EXPORT.OBJECT
                  || this.fileEvent === GeneralConstants.ENTITY_IMPORT_EXPORT.PROPERTY)
                  ? GeneralConstants.ENTITY_IMPORT_EXPORT.PROPERTY : GeneralConstants.ENTITY_IMPORT_EXPORT.ARTIST.toLowerCase();
                this._importexportService
                  .getFileUpload(this.fileEvent, formData)
                  .subscribe((response) => {
                    this.successUpload(response)
                    this.fileEvent = (this.fileEvent === 'property') ? 'Object' : 'Artist';
                    this.displayUploadMsg = 'block';
                    this.slimLoadingBarService.complete();
                  }, this.errorUpload.bind(this));
                break;
            }
          }
          else {
            this.uploader.progress = 0;
            this.fileProgress = false;
            this.showMessage(GeneralDictionary.NOTIFICATIONS.FILENAME_LENGTH_EXCEEDED(this.fileEvent),
              _.toLower(GeneralDictionary.NOTIFICATIONS.error));
            this.displayUploadMsg = "block";
            this.slimLoadingBarService.complete();
          }
        } else {
          this.uploader.progress = 0;
          this.fileProgress = false;
          this.showMessage(GeneralDictionary.NOTIFICATIONS.INVALID_FILE_EXTENSION(this.fileEvent),
            _.toLower(GeneralDictionary.NOTIFICATIONS.error));
          this.displayUploadMsg = "block";
          this.slimLoadingBarService.complete();
        }
      }
    } else {

      if (this.fileEvent === GeneralConstants.ENTITY_IMPORT_EXPORT.ARTIST) {
        this._importexportService
          .getCSVArtistDownload('size=2000')
          .subscribe(Response => {
            this.exportsID = Response;
            this._importexportService.getLimitStatus('properties', this.exportsID)
              .subscribe(Response => {
                if (Response.json().toLowerCase() === "limitexceeded") {
                  document.getElementsByTagName("html")[0].classList.remove("loading");
                  this.tosatservice.addToastError('There was an error exporting your list: Your list exceeds the maximum exportable range of 20,000 objects.', 'top-right');
                } else {
                  var intvl = 20000;
                  this.pollTasks(this.URL + '/artists/export/', intvl, this.exportsID);
                }
              }, resFileError => {
                this.tosatservice.addToastError('Something went wrong...! <br/> Please try again...!', 'top-right');
              });
          }, resFileError => {
            this.tosatservice.addToastError('Something went wrong...! <br/> Please try again...!', 'top-right');
          });
      } else if (this.fileEvent === 'SavedList' && this.csvImportExportForm.value && this.csvImportExportForm.value.savelist) {
        this._importexportService
          .getSaveListDownload(this.userName, this.saveList)
          .subscribe(Response => {
            this.exportsID = Response;
            this._importexportService.getLimitStatus('properties', this.exportsID)
              .subscribe(Response => {
                if (Response.json().toLowerCase() === "limitexceeded") {
                  document.getElementsByTagName("html")[0].classList.remove("loading");
                  this.tosatservice.addToastError('There was an error exporting your list: Your list exceeds the maximum exportable range of 20,000 objects.', 'top-right');
                } else {
                  var intvl = 20000;
                  this.pollTasks(this.URL + '/users/' + this.userName + '/lists/export/', intvl, this.exportsID);
                }
              }, resFileError => {
                this.tosatservice.addToastError('Something went wrong...! <br/> Please try again...!', 'top-right');
              });
          });
      } else if (this.fileEvent === GeneralConstants.ENTITY_IMPORT_EXPORT.ESTABLISHMENT) {
        this.exportAction(`${_.toLower(GeneralConstants.ENTITY_IMPORT_EXPORT.ESTABLISHMENT)}s`);
      } else if (this.fileEvent === GeneralConstants.ENTITY_IMPORT_EXPORT.SOURCE_NAME) {
        this.exportAction(GeneralConstants.ENTITY_IMPORT_EXPORT.CLIENT_SOURCES);
      }
    }
  }

  exportAction(type) {
    this._importexportService
      .getCSVDownload(type)
      .distinctUntilKeyChanged('status')
      .subscribe(response => {
        let notification;
        switch (_.get(response, 'status')) {
          case EVENTCONST.EXPORT_STATUS.progress:
            notification = this.showMessageNotificationSvc(GeneralDictionary.NOTIFICATIONS.info,
              GeneralDictionary.NOTIFICATIONS.DOWNLOADED_FILE_STARTED(`${_.lowerCase(type)}`),
              _.lowerCase(GeneralDictionary.NOTIFICATIONS.info), { timeOut: 0, clickToClose: false })
            this.blockScreenService.lockScreen();
            break;
          case EVENTCONST.EXPORT_STATUS.completed:
            this.showMessageNotificationSvc(GeneralDictionary.NOTIFICATIONS.success,
              GeneralDictionary.NOTIFICATIONS.DOWNLOADED_FILE(`${_.lowerCase(type)}`
              ),
              _.lowerCase(GeneralDictionary.NOTIFICATIONS.success))
            if (notification) {
              this.notificationSvc.remove(notification.id);
            }
            openFileInNewTab(response.fileUrl);
            this.blockScreenService.unlockScreen();
            break;
        }
      },
        error => {
          this.notificationSvc.remove();
          this.blockScreenService.unlockScreen();
          this.showMessageNotificationSvc(GeneralDictionary.NOTIFICATIONS.error, error, GeneralDictionary.NOTIFICATIONS.error)
        })
  }
  successUpload(response: Response) {
    this.showMessage(GeneralDictionary.NOTIFICATIONS.FILE_UPLOADED_SUCCESSFULLY,
      _.toLower(GeneralDictionary.NOTIFICATIONS.SUCCESS));
    document.getElementsByTagName('html')[0].classList.remove('loading');
  }

  errorUpload(resFileError) {
    document.getElementsByTagName('html')[0].classList.remove('loading');
    let err = JSON.stringify(resFileError);
    let errorType: string = null;
    let errDetails = {};
    if (err.indexOf('"Error: File does not contain the expected number of columns') > -1) {
      errorType = 'columnError';
    }
    if (err.indexOf('"Error: File contains empty headers') > -1) {
      errorType = 'emptyHeaderError';
    }
    else if (err.indexOf('Error: File contains columns which are not in the expected order') > -1) {
      let uploaded = err.split('Uploaded :')[1].split(',')[0] ? err.split('Uploaded :')[1].split(',')[0] : null;
      let expected = err.split('Expected :')[1].split('\\')[0] ? err.split('Expected :')[1].split('\\')[0] : null;
      errDetails['uploaded'] = uploaded.trim();
      errDetails['expected'] = expected.trim();
      errorType = 'orderError';
    }
    else if (err.indexOf('Error: File contains column headers which are not expected') > -1) {
      errorType = 'headerError';
      let uploaded = err.split('Uploaded :')[1].split(',')[0] ? err.split('Uploaded :')[1].split(',')[0] : null;
      let expected = err.split('Expected :')[1].split('\\')[0] ? err.split('Expected :')[1].split('\\')[0] : null;
      errDetails['uploaded'] = uploaded.trim();
      errDetails['expected'] = expected.trim();
    }

    this.uploader.progress = 0;
    this.fileProgress = false;
    if (errorType === 'emptyHeaderError') {
      this.tosatservice.addToastError('File upload failed: File contains empty headers.', 'top-right');
    } else if (errorType === 'columnError') {
      this.tosatservice.addToastError('File upload failed: File does not contain the expected number of columns.', 'top-right');
    }
    else if (errorType === 'orderError') {
      if (errDetails && errDetails['expected'] !== '' && errDetails['uploaded'] != '') {
        this.tosatservice.addToastError('File upload failed: File contains columns which are not in the expected order. Column "' + errDetails['uploaded'] + '" displayed in place of "' + errDetails['expected'] + '".', 'top-right');
      }
      else {
        this.tosatservice.addToastError('File upload failed: File contains columns which are not in the expected order.', 'top-right');
      }
    }
    else if (errorType === 'headerError') {
      if (errDetails && errDetails['expected'] !== '' && errDetails['uploaded'] != '') {
        this.tosatservice.addToastError('File upload failed: File contains column headers which are not as expected. Incorrect header "' + errDetails['uploaded'] + '" displayed in place of "' + errDetails['expected'] + '".', 'top-right');
      }
      else {
        this.tosatservice.addToastError('File upload failed: File contains column headers which are not as expected.', 'top-right');
      }
    }
    else {
      this.tosatservice.addToastError('File upload failed.', 'top-right');
    }
    this.displayUploadMsg = "block";
    this.slimLoadingBarService.complete();


  }

  successDownload(response: Response) {
    this.downloadFile(response, this.fileEvent)
    this.showMessage(GeneralDictionary.NOTIFICATIONS.DOWNLOADED_FILE(this.fileEvent),
      _.toLower(GeneralDictionary.NOTIFICATIONS.SUCCESS));
    document.getElementsByTagName('html')[0].classList.remove('loading');
  }

  errorDownload(resFileError) {
    this.showMessage(GeneralDictionary.NOTIFICATIONS.DOWNLOADED_FILE_ERROR(this.fileEvent),
      _.toLower(GeneralDictionary.NOTIFICATIONS.error));
    document.getElementsByTagName('html')[0].classList.remove('loading');
  }

  downloadFile(data: Response, type: string) {
    const BOM = '\uFEFF';
    const csvData = BOM + [data['_body']];
    const myBlob = new Blob([csvData], { type: 'text/csv' });
    const blobURL = (window.URL).createObjectURL(myBlob);
    const anchor = document.createElement('a');
    anchor.download = type + '.csv';
    anchor.href = blobURL;
    anchor.click();
  }

  protected showMessage(content, type) {
    if (type === _.toLower(GeneralDictionary.NOTIFICATIONS.SUCCESS)) {
      this.tosatservice.addToast(content, GeneralConstants.ENTITY_IMPORT_EXPORT.TOP_RIGHT);
    } else {
      this.tosatservice.addToastError(content, GeneralConstants.ENTITY_IMPORT_EXPORT.TOP_RIGHT);
    }
  }

  protected showMessageNotificationSvc(title, content, type, _overwriteOptions = {}) {
    let notification;
    const options = _.merge({}, GeneralConstants.NOTIFICATIONS.OPTIONS, _overwriteOptions);
    if (type === 'success') {
      notification = this.notificationSvc.success(title, content, options);
    } else if (type === 'info') {
      notification = this.notificationSvc.info(title, content, options);
    } else {
      notification = this.notificationSvc.error(title, content, options);
    }
    return notification;
  }

  destroyMessage(event) {
    this.notificationSvc.remove();
  }

  pollTasks(url, intvl, exportId) {
    if (intvl > 0) {
      document.getElementsByTagName('html')[0].classList.add('loading');
      this.cancelFlag = true;
      this.filePollTasks = Observable.interval(intvl)
        .switchMap(() => this.http.get(url + exportId + '/status')).map((response) => {
          const res = response.json();
          if (res.toLowerCase() === 'completed') {
            this.filePollTasks.unsubscribe();
            this.cancelFlag = false;
            document.getElementsByTagName("html")[0].classList.remove("loading");
            window.open(this.s3URL + "/" + exportId + ".csv", "_blank");
            this.tosatservice.addToast('File downloaded successfully.', 'top-right');
          } else if (res.toLowerCase() === 'error') {
            this.filePollTasks.unsubscribe();
            this.cancelFlag = false;
            document.getElementsByTagName("html")[0].classList.remove("loading");
            this.tosatservice.addToastError('Something went wrong...!<br/> Please try again...!', 'top-right');
          } else if (res.toLowerCase() === "limitexceeded") {
            this.filePollTasks.unsubscribe();
            this.cancelFlag = false;
            document.getElementsByTagName("html")[0].classList.remove("loading");
            this.tosatservice.addToastError('There was an error exporting your list: Your list exceeds the maximum exportable range of 20,000 objects.', 'top-right');
          } else if (response && response['_body'] && response['_body'].type && response['_body'].type === "error") {
            this.filePollTasks.unsubscribe();
            this.cancelFlag = false;
            document.getElementsByTagName("html")[0].classList.remove("loading");
            this.tosatservice.addToastError('Something went wrong...!<br/> Please try again...!', 'top-right');
          }
        })
        .subscribe((response) => { }, resFileError => {
          this.filePollTasks.unsubscribe();
          this.cancelFlag = false;
          document.getElementsByTagName("html")[0].classList.remove("loading");
          this.tosatservice.addToastError('Something went wrong...!<br/> Please try again and reload the page...!', 'top-right');
        })
    } else {
      this.cancelFlag = false;
      this.filePollTasks.unsubscribe();
      document.getElementsByTagName("html")[0].classList.remove("loading");
      this.tosatservice.addToastError('Export failed...!', 'top-right');
    }
  }

  //cancel export
  cancelExport() {
    document.getElementsByTagName("html")[0].classList.remove("loading");
    this.dialogService.addDialog(ConfirmComponent, {
      title: 'Cancel Export',
      message: 'Are you sure you would like to quit exporting this list?'
    }, { backdropColor: 'rgba(220,220,220,0.5)' })
      .subscribe((isConfirmed) => {
        document.getElementsByTagName("html")[0].classList.add("loading");
        if (isConfirmed) {
          this.pollTasks('url', 0, 'exportId')
        }
      });
  }


  uploadFile() {
    this.slimLoadingBarService.start(() => {
    });
    let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#uploadFile');
    let formData = new FormData
    if (inputEl.files.item(0).name.indexOf(".csv") > -1) {
      formData.append('file', inputEl.files.item(0));
      this.fileEvent = (this.fileEvent === 'Object' || this.fileEvent === 'property') ? 'property' : 'artist';
      this._importexportService
        .getFileUpload(this.fileEvent, formData)
        .subscribe(Response => {
          this.fileEvent = (this.fileEvent === 'property') ? 'Object' : 'Artist';
          this.displayUploadMsg = "block";
          this.tosatservice.addToast('File Uploaded Successfully.', 'top-right');
          this.slimLoadingBarService.complete();
        }, resFileError => {
          let err = JSON.stringify(resFileError);
          let errorType: string = null;
          let errDetails = {};

          if (err.indexOf('"Error: File does not contain the expected number of columns') > -1) {
            errorType = 'columnError';
          }
          else if (err.indexOf('Error: File contains columns which are not in the expected order') > -1) {
            let uploaded = err.split('Uploaded :')[1].split(',')[0] ? err.split('Uploaded :')[1].split(',')[0] : null;
            let expected = err.split('Expected :')[1].split('\\')[0] ? err.split('Expected :')[1].split('\\')[0] : null;
            errDetails['uploaded'] = uploaded.trim();
            errDetails['expected'] = expected.trim();
            errorType = 'orderError';
          }
          else if (err.indexOf('Error: File contains column headers which are not expected') > -1) {
            errorType = 'headerError';
            let uploaded = err.split('Uploaded :')[1].split(',')[0] ? err.split('Uploaded :')[1].split(',')[0] : null;
            let expected = err.split('Expected :')[1].split('\\')[0] ? err.split('Expected :')[1].split('\\')[0] : null;
            errDetails['uploaded'] = uploaded.trim();
            errDetails['expected'] = expected.trim();
          }
          this.fileEvent = (this.fileEvent === 'property') ? 'Object' : 'Artist';
          this.uploader.progress = 0;
          this.fileProgress = false;
          if (errorType === 'columnError') {
            this.tosatservice.addToastError('File upload failed: File does not contain the expected number of columns.', 'top-right');
          }
          else if (errorType === 'orderError') {
            if (errDetails && errDetails['expected'] !== '' && errDetails['uploaded'] != '') {
              this.tosatservice.addToastError('File upload failed: File contains columns which are not in the expected order. Column "' + errDetails['uploaded'] + '" displayed in place of "' + errDetails['expected'] + '".', 'top-right');
            }
            else {
              this.tosatservice.addToastError('File upload failed: File contains columns which are not in the expected order.', 'top-right');
            }
          }
          else if (errorType === 'headerError') {
            if (errDetails && errDetails['expected'] !== '' && errDetails['uploaded'] != '') {
              this.tosatservice.addToastError('File upload failed: File contains column headers which are not as expected. Incorrect header "' + errDetails['uploaded'] + '" displayed in place of "' + errDetails['expected'] + '".', 'top-right');
            }
            else {
              this.tosatservice.addToastError('File upload failed: File contains column headers which are not as expected.', 'top-right');
            }
          }
          else {
            this.tosatservice.addToastError('File upload failed.', 'top-right');
          }
          this.displayUploadMsg = "block";
          this.slimLoadingBarService.complete();
        });
    }
    else {
      this.uploader.progress = 0;
      this.fileProgress = false;
      this.tosatservice.addToastError('File Uploaded Failed.', 'top-right');
      this.displayUploadMsg = "block";
      this.slimLoadingBarService.complete();
    }
  }
  //keyboard shortcult
  keyboardShortCutFunction(e) {
    switch (this._keyboardkeysService.getKeyboardResponse(e)) {
      case 'objectView':
        this.router.navigate(['/object'])
        break;
      case 'duplicateWindow':
        let urlP = window.location.href;
        window.open(urlP, '_blank');
        break;
      case 'homeView':
        this.router.navigate(['/'])
        break;
    }
  }

  redirect(page) {
    switch (page) {
      case 'home':
        this.router.navigateByUrl('/');
        break;

      default:
        this.router.navigateByUrl('/admin');
    }
  }

  scrolled() {
    var elem = document.getElementById('AdminSavedListContainer');
    if (elem.scrollHeight - elem.scrollTop === elem.clientHeight) {
      if (this.currentPage <= this.totalpages) {
        this._adminSavedListService.getSavedLists(this.currentPage)
          .subscribe(response => {
            this.savedListData = this.savedListData.concat(response.content);
            this.savedListData_copy = this.savedListData_copy.concat(response.content);
            this.totalpages = response.totalPages;
            this.currentPage++;
          })
      }
    }
  }

  public curPage = 0;
  public totPages = 0;
  scrolledSavedList() {
    var elem = document.getElementById('savedListData');
    if (elem.scrollHeight - elem.scrollTop === elem.clientHeight) {
      if (this.curPage <= this.totPages) {
        this.curPage++;
        this._importexportService
          .getSaveLists(this.userName, this.curPage)
          .subscribe(Response => {
            this.userSaveList = this.userSaveList.concat(Response.content);
          }, resFileError => {
          });
      }
    }
  }

  deleteSavedList(index) {
    this._importexportService.getAdminSaveListByName(this.savedListData[index])
      .subscribe((response: Response) => {
        var recordCount = response['propertyIds'].length;
        this.dialogService.addDialog(ConfirmComponent, {
          title: '',
          message: 'Are you sure you would like to delete all ' + recordCount + ' objects in this list?'
        }, { backdropColor: 'rgba(220,220,220,0.5)' })
          .subscribe((isConfirmed) => {
            if (isConfirmed) {
              if (response['sharedUsersList'].length > 0) {
                this.dialogService.addDialog(AlertComponent,
                  { title: '', message: "Error deleting objects: There are one or more shared users on this list. Please remove shared users and try again." }, { backdropColor: 'rgba(220,220,220,0.5)' })
                  .subscribe((isConfirmed) => {
                  });
              } else {
                document.getElementsByTagName("html")[0].classList.add("loading");
                this._importexportService.deleteSavedList(response['id'])
                  .subscribe(Response => {
                    this.tosatservice.addToast('All objects within Saved List, ' + response['name'] + ' have successfully been deleted.', 'top-right');
                    var index_copy = this.savedListData_copy.findIndex(item => item == response['name']);
                    var index = this.savedListData.findIndex(item => item == response['name']);
                    if (index_copy > -1) {
                      this.savedListData_copy.splice(index_copy, 1);
                    }
                    if (index > -1) {
                      this.savedListData.splice(index, 1);
                    }
                    document.getElementsByTagName("html")[0].classList.remove("loading");
                  }, resFileError => {
                    if (resFileError.status == 409) {
                      this.tosatservice.addToastError('Failed to send an email for the Deleted Saved List', 'top-right');
                    } else {
                      this.tosatservice.addToastError('saveList Delete Failed', 'top-right');
                    }
                    document.getElementsByTagName("html")[0].classList.remove("loading");
                  })
              }
            }
          });
      })
  }

  textboxValue(event) {
    if (event.target.value) {
      this.savedListData = this.savedListData_copy.filter(item => {
        return item.toLowerCase().indexOf(event.target.value.toLowerCase()) > -1;
      })
    } else {
      document.getElementsByTagName("html")[0].classList.add("loading");
      this._importexportService.getAllAdminSaveLists()
        .subscribe(response => {
          this.savedListData = response;
          this.savedListData_copy = response;
          document.getElementsByTagName("html")[0].classList.remove("loading");
        }, resFileError => {
        })
    }
  }

}
