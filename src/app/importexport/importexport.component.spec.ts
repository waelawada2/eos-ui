import { NO_ERRORS_SCHEMA  , CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ImportexportComponent } from './importexport.component';
import { HttpModule } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';

class DummyRouter {}
class SlimLoadingBarServiceDummy {}

describe('ImportexportComponent', () => {
  let component: ImportexportComponent;
  let fixture: ComponentFixture<ImportexportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportexportComponent ],
      imports: [ HttpModule ],
      providers: [
        { provide: ActivatedRoute, useClass: DummyRouter },
        { provide: Router , useClass: DummyRouter },
        { provide: SlimLoadingBarService, useClass: SlimLoadingBarServiceDummy }
      ],
      schemas: [ NO_ERRORS_SCHEMA , CUSTOM_ELEMENTS_SCHEMA]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportexportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
