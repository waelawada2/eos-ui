import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { GeneralConstants, GeneralDictionary } from '../shared/constants';
import { processParamsObject, exportRecursiveRequest, exportStatusRequest } from '../event/shared/utils'
import { ConfigurationLoaderService } from 'app/configuration.service'

@Injectable()
export class ImportexportService {

  private _url: string
  private _eventurl: string

  constructor(private _http: Http, configuration: ConfigurationLoaderService) {
    this._url = configuration.getSettings().apiUrl
    this._eventurl = configuration.getSettings().eventApiUrl
  }

  // CSV file download Artist
  getCSVArtistDownload(query) {
    document.getElementsByTagName("html")[0].classList.add("loading");
    return this._http.post(this._url + `/artists/export?` + query, {})
      .map((response: Response) => {

        return response['_body'];
      })
      .catch(this._errorHandler);
  }

  // CSV file download Property
  getCSVPropertyDownload(query) {
    document.getElementsByTagName("html")[0].classList.add("loading");
    return this._http.post(this._url + `/properties/export?` + query, {})
      .map((response: Response) => {

        return response['_body'];
      })
      .catch(this._errorHandler);
  }
  // upload the CSV file
  getFileUpload(type: string, body: Object) {
    document.getElementsByTagName("html")[0].classList.add("loading");
    return this._http.post(this._url + "/fileUpload?type=" + type, body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          return response.json();
        } else {
          document.getElementsByTagName("html")[0].classList.remove("loading");
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  // Get Save List values
  getSaveLists(userName: string, page: number) {
    return this._http.get(this._url + `/users/` + userName + `/lists?sort=name,asc&page=` + page)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          document.getElementsByTagName("html")[0].classList.remove("loading");
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // upload the CSV file with other url
  getFileUploadEventURL(type: string, body: Object) {
    return this._http.post(`${this._eventurl}/${type}`, body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error(`${GeneralDictionary.NOTIFICATIONS.REQUEST_HAS_FAILED} ${response.status}`)
        }
      })
      .catch(this._errorHandler);
  }

  //Download CSV list
  getSaveListDownload(userName: string, entityId: string) {
    document.getElementsByTagName('html')[0].classList.add('loading');
    return this._http.post(`${this._url}/users/${userName}/lists/${entityId}/export?size=2000`, {})
      .map((response: Response) => {

        return response['_body'];
      })
      .catch(this._errorHandler);
  }

  deleteSavedList(saveListId: string) {
    return this._http.delete(this._url + '/users/lists/' + saveListId + '/delete')
      .map((response: Response) => {
        return response;
      })
      .catch(this._errorHandler);
  }

  _errorHandler(_error: Response) {
    document.getElementsByTagName('html')[0].classList.remove('loading');
    return Observable.throw(_error || GeneralDictionary.NOTIFICATIONS.SERVER_ERROR_404);
  }

  // CSV file download ESTABLISHMENT and Source Name
  getCSVDownload(type) {
    const data = new FormData();
    const statusRequestOptions = { http: this._http, baseUrl: this._eventurl }
    data.append('size', GeneralConstants.ENTITY_IMPORT_EXPORT.CSVSIZE)
    return this._http
      .post(`${this._eventurl}/${type}/export`, data)
      .map((response: Response) => {
        return JSON.parse(_.get(response, '_body'));
      })
      .flatMap(mapResponse => exportStatusRequest(_.merge({}, statusRequestOptions, { response: mapResponse }))
        .expand((expandResponse) => {
          return exportRecursiveRequest(_.merge({}, { response: expandResponse }, statusRequestOptions))
        })
      )
  }

  getAllAdminSaveLists() {
    return this._http.get(this._url + '/users/lists/admin/all')
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      });
  }

  getAdminSaveListByName(name: string) {
    let body = { "name": name };
    return this._http.get(this._url + '/users/lists/admin/name?name=' + name)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
  }

  getLimitStatus(type, exportId) {
    return this._http.get(this._url + '/' + type + '/export/' + exportId + '/status')
      .map((response: Response) => {
        return response;
      })
      .catch(this._errorHandler);
  }
}
