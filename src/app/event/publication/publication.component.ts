import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { BlockScreenService } from '../shared/services/block-screen/block-screen.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { DialogService } from 'ng2-bootstrap-modal';
import { PublicationFormComponent } from './form/publication.form.component';
import { PublicationSingleItemComponent } from './items/publication-single-item/publication-single-item.component';
import {
  AddObjectToEventService, NavigationService, PublicationItemService, PublicationService,
  ExternalClientService
} from '../shared/services';
import { Message, Publication } from '../shared/models';
import { PUBLICATION_ITEM_OBJECTS } from '../shared/mocks'
import { GeneralConstants, GeneralDictionary } from '../shared/constants';
import { getFormFormattedPublication, getAllClientsIdsFromSubEvents, appendClientDataToItems } from '../shared/utils';
import { SothebysLifeCycle, STATES, URLS } from '../sothebys-lifecycle.abstract';
import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import * as _ from 'lodash';
import * as moment from 'moment/moment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/takeWhile';
import { SORTING } from '../shared/components/navigation-header/navigation-header.sorting.interface';
import { ConfirmComponent } from './../shared/components';

const TYPE = 'PUBLICATION';
@Component({
  selector: 'app-publication',
  templateUrl: './publication.component.html',
  styleUrls: [
    '../shared/styles/panels.scss',
    '../shared/styles/forms.scss',
    './publication.component.scss',
  ],
})
export class PublicationComponent extends SothebysLifeCycle implements AfterViewInit, OnDestroy {

  protected getErrors: BehaviorSubject<any>;
  publications: Publication[];
  current: Publication;
  currentId: any;
  generalDictionary = GeneralDictionary;
  eventItems: any[] = [];
  showTable = false;
  options = GeneralConstants.NOTIFICATIONS.SETUP;
  hiddenFields = [];
  formSubscription: Subscription;
  itemsSubscription: Subscription;
  stateObservableSubscription: Subscription;
  elementsSubscription: Subscription;
  currentSearch: any = {}
  currentSearchData;
  private clientsMap = {};

  @ViewChild(PublicationFormComponent) form: PublicationFormComponent;

  constructor(
    private ps: PublicationService,
    private publicationItemsSvc: PublicationItemService,
    private addObjectToEventService: AddObjectToEventService,
    protected blockScreenService: BlockScreenService,
    protected notificationSvc: NotificationsService,
    protected navService: NavigationService,
    protected route: ActivatedRoute,
    protected dialogService: DialogService,
    private clientService: ExternalClientService,
    protected router: Router) {
    super()
    this.baseUrl = 'publication';
    this.section = TYPE;
    this.getErrors = this.ps.error;
  }

  ngAfterViewInit() {
    const form = this.form.publicationForm;
    this.formSubscription = form.valueChanges
      .distinctUntilChanged()
      .subscribe((a) => this.validData.next(form.valid));

    this.stateObservable
      .debounceTime(100)
      .take(1)
      .subscribe({
        next: this.validateForm.bind(this)
      })

    this.stateObservableSubscription = this.stateObservable
      .distinctUntilChanged()
      .subscribe({
        next: state => {
          setTimeout(() => this.updateHiddenFields(state))
        }
      })

    super.initialize();

    this.itemsSubscription = this.publicationItemsSvc
      .elements
      .distinctUntilChanged()
      .skipWhile(() => this.state === STATES.NEW || this.state === STATES.SEARCH)
      .subscribe({
        next: (items) => {
          this.eventItems = _.clone(items);

          const clientIds = getAllClientsIdsFromSubEvents(items);
          this.clientService.getClients(clientIds)
            .subscribe((client) => {
              this.clientsMap[client.entityId] = client
            }, null,
              () => {
                this.eventItems = _.clone(appendClientDataToItems(this.eventItems, this.clientsMap))
                this.checkClientLoadErrors(clientIds)
              });

          this.ableToRemove.next(items.length <= 0)
        }
      })

    this.elementsSubscription = this.ps.elements.distinctUntilChanged()
      .skipWhile(() => this.state !== STATES.EDIT)
      .subscribe({
        next: (event) => {
          this.updatePublications(event)
          if (event.length > 0 && this.currentId && event[0].id === this.currentId && this.addObjectToEventService.creating) {
            this.doItemCreation(event[0]);
          }
        }
      })
  }

  public ngOnDestroy() {
    super.ngOnDestroy();
    this.formSubscription.unsubscribe();
    this.itemsSubscription.unsubscribe();
    this.stateObservableSubscription.unsubscribe();
    this.elementsSubscription.unsubscribe();
  }

  private checkClientLoadErrors(clientIds) {
    clientIds.forEach(id => {
      if (!this.clientsMap[id]) {
        this.notificationSvc.error(GeneralDictionary.NOTIFICATIONS.error,
          GeneralDictionary.NOTIFICATIONS.ERR_NOT_FOUND_CLIENT(id),
          GeneralConstants.NOTIFICATIONS.OPTIONS
        )
      }
    })
  }

  protected clearForm(restore) {
    this.showTable = false;
    this.updatePages(0, 0);
    this.form.reset();
    this.form.emptyDate();
    this.current = undefined;
    this.eventItems = [];
    this.currentSearch = {}
    this.loadPaginated = 0
    if (restore) {
      this.form.setValue(this.currentSearchData);
    }
    this.currentSearchData = {};
    this.current = null;
    this.validateForm(this.state);
  }

  public create() {
    super.create();
    this.form.emptyDate(true);
    this.validateForm(this.state);
  }

  getSortParameters(order: SORTING = SORTING.DESCENDING) {
    return order === SORTING.DESCENDING ? GeneralConstants.SORTING.PUBLICATION.DESC : GeneralConstants.SORTING.PUBLICATION.ASC
  }

  protected validateForm(state) {
    switch (state) {
      case STATES.SEARCH:
        this.form.searchMode();
        break;
      default:
        this.form.standarMode();
        break;
    }
  }

  protected doSearchAction(id): BehaviorSubject<any> {
    this.currentId = id;
    this.currentSearch = { ...this.form.getSearchData(), size: this.pageSize };
    this.ps.getElements({ ...this.currentSearch, sort: this.getSortParameters() }, id);
    return this.ps.elements;
  }

  private doItemCreation(event) {
    if (event && this.addObjectToEventService.objectId) {
      if (this.addObjectToEventService.item) {
        this.loadItemPopup(this.addObjectToEventService.item, false)
      } else {
        this.publicationItemsSvc.create({
          type: _.toUpper(GeneralDictionary.SECTIONS.PUBLICATION),
          event: { id: event.id, type: _.toUpper(GeneralDictionary.SECTIONS.PUBLICATION) },
          objects: [{ objectId: this.addObjectToEventService.objectId }]
        }).take(1).subscribe((result) => {
          const response = JSON.parse(_.get(result, '_body'));
          if (_.has(response, 'error')) {
            return this.handleErrorAction(result);
          }
          this.addObjectToEventService.item = response;
          this.loadItemPopup(this.addObjectToEventService.item, false)
          this.getEventItems()
        })
      }
    }
  }

  protected doCreateAction(): Observable<any> {
    const data = this.form.getEditionData();
    return this.ps.create(data);
  }

  protected doEditAction(): Observable<any> {
    const data = this.form.getEditionData();
    return this.ps.amend(data, this.current.id);
  }

  protected doDeleteAction(): Observable<any> {
    return this.ps.delete(this.current.id);
  }

  protected doImportAction(file: File): Observable<any> {
    return this.ps.import(file, GeneralConstants.IMPORT_TYPES.publication);
  }

  protected doExportAction(): Observable<any> {
    return this.ps.export(this.currentSearch)
  }

  private updatePublications(p) {
    this.publications = p;
    if (this.loadPaginated !== 0) {
      this.changePage(this.loadPaginated)
      this.loadPaginated = 0;
    }
  }

  protected displayItemNumber(number: number) {
    this.form.resetInputFields()
    return this.getPublicationData(number);
  }

  private getPublicationData(number: number = 0) {
    this.eventItems = [];
    this.showTable = false;
    if (!this.publications) {
      this.updatePages(0, 0);
      return;
    }
    this.showTable = true;
    if (number >= 0 && this.publications.length > number) {
      this.current = this.publications[number];
      if (this.current) {
        this.getEventItems();
        this.form.setValue(getFormFormattedPublication(this.current))
        this.validateForm(this.state);
      }
      this.updatePages(number, this.ps.page.value.totalElements)
    } else {
      this.ps.getNextElements(number)
      this.loadPaginated = number;
    }
    return _.get(this.current, 'id');
  }

  private getEventItems() {
    this.publicationItemsSvc.getItemsByUrl(this.current._links.items.href);
  }

  public handleFormMessages(message: Message) {
    this.showMessage(message.title, message.message, message.type);
  }

  public loadMoreItems() {
    const totalElements = this.publicationItemsSvc.page.value.totalElements;
    if (totalElements > this.eventItems.length) {
      this.publicationItemsSvc.getNextElements()
    }
  }

  protected reorderResults(): BehaviorSubject<any> {
    this.ps.getElements({ ...this.currentSearch, sort: this.getSortParameters(this.sortOrder) });
    return this.ps.elements;
  }

  private removeItem(item) {

    if (item.consignmentEvent || item.purchaseEvent) {
      this.showMessage(GeneralDictionary.NOTIFICATIONS.error,
        GeneralDictionary.NOTIFICATIONS.ERR_CANT_DELETE_RELATED_ITEM, GeneralDictionary.NOTIFICATIONS.error)
      return;
    }
    const disposable = this.dialogService.addDialog(ConfirmComponent, {
      title: this.generalDictionary.NOTIFICATIONS.DELETE_CONFIRM_TITLE,
      message: this.generalDictionary.NOTIFICATIONS.DELETE_ITEM_FROM_EVENT(_.upperFirst(_.toLower(this.section)))
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        this.processItemRemoval(item)
      }
      disposable.unsubscribe()
    });
  }

  private processItemRemoval(item) {
    this.publicationItemsSvc.delete(item.id).subscribe(
      (result) => this.showMessage(GeneralDictionary.NOTIFICATIONS.success,
        GeneralDictionary.NOTIFICATIONS.deleted(`${_.capitalize(TYPE)} ${GeneralConstants.ENTITY_TYPE.ITEM}`),
        _.toLower(GeneralDictionary.NOTIFICATIONS.success)),
      this.handleErrorAction.bind(this))
  }

  private updateHiddenFields(state) {
    switch (state) {
      case STATES.EDIT:
      case STATES.NEW:
        this.hiddenFields = ['eventId', 'verifiedDropDown'];
        break;
      case STATES.SEARCH:
        this.hiddenFields = ['eventId', 'verified'];
        break;
    }
  }

  private loadItemPopup(item, isClosable: boolean) {
    const disposable = this.dialogService.addDialog(PublicationSingleItemComponent, {
      item,
      title: `${_.upperFirst(_.toLower(GeneralDictionary.SECTIONS.PUBLICATION))} ${GeneralConstants.ENTITY_TYPE.ITEM}`,
      isClosable: isClosable,
    }).subscribe((response) => {
      if (response) {
        if (response.redirect) {
          this.router.navigate([response.url])
          return
        }
        this.publicationItemsSvc.amend({ ...response, id: null }, response.id).subscribe((res) => {
          if (_.has(response, 'error')) {
            return this.handleErrorAction(res);
          }
          this.addObjectToEventService.reset();
          this.showMessage(GeneralDictionary.NOTIFICATIONS.success,
            GeneralDictionary.NOTIFICATIONS.saved(_.upperFirst(GeneralConstants.ENTITY_TYPE.ITEM)),
            _.toLower(GeneralDictionary.NOTIFICATIONS.success))
        }, this.handleErrorAction.bind(this))
      }
    })
  }
}
