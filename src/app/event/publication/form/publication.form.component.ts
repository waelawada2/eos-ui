import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ListInputFormComponent } from '../../shared/components'
import { ExhibitionService, PublicationService, PublicationTypeService } from '../../shared/services';
import { GeneralDictionary, FormsDictionary, GeneralConstants } from '../../shared/constants';
import { Message, Publication } from '../../shared/models';
import { autocompleteByValue, formatDate, setValidators } from '../../shared/utils';
import { RangeValidator, noRequiredObjectValidator } from '../../shared/validators';
import { Subscription } from 'rxjs/Subscription';
import * as _ from 'lodash';
import * as moment from 'moment/moment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-publication-form',
  templateUrl: './publication.form.component.html',
  styleUrls: ['../../shared/styles/forms.scss',
    './publication.form.component.scss'],
})
export class PublicationFormComponent implements AfterViewInit, OnInit, OnDestroy {

  @ViewChildren(ListInputFormComponent) listInputs: QueryList<ListInputFormComponent>;

  datePickerConfig = GeneralConstants.DATE_PICKER_CONFIG;
  publications: Publication[];
  publicationTypes: any[];
  publicationForm: FormGroup;
  formErrors = {};
  dictionary = FormsDictionary;
  listFormatter = autocompleteByValue;
  disabled = false;
  publicationSubscription: Subscription;
  publicationTypeSubscription: Subscription;
  formSubscription: Subscription;
  validate = false;
  verifiedValues = [
    { realValue: true, value: FormsDictionary.PUBLICATION.FIELDS.VERIFIED },
    { realValue: false, value: FormsDictionary.PUBLICATION.FIELDS.NOT_VERIFIED },
  ];
  event: Publication;

  standarValidatorsMap = {
    date: Validators.required,
    author: Validators.maxLength(100),
    edition: Validators.maxLength(20),
    isbn: Validators.maxLength(20),
    notes: Validators.maxLength(1000),
    pages: RangeValidator(0, 9000),
    publicationType: noRequiredObjectValidator(true),
    subTitle: Validators.maxLength(100),
    title: [Validators.required, Validators.maxLength(100)],
    volume: Validators.maxLength(10),
  }
  searchValidatorsMap = {
    ..._.pick(this.standarValidatorsMap, ['author', 'edition', 'isbn', 'notes', 'pages', 'volume', 'subTitle']),
    title: Validators.maxLength(100),
    publicationType: noRequiredObjectValidator(),
  }


  exhibitionData = [];
  publicationData = [];
  notAllowedIds = [];

  @Input()
  set disableForm(value: boolean) {
    this.disabled = value;
  }

  @Input()
  hideElements: any[] = [];


  @Output()
  messageEmitted: EventEmitter<Message> = new EventEmitter<Message>();

  constructor(private fb: FormBuilder,
    private exhibitionSvc: ExhibitionService,
    private publicationSvc: PublicationService,
    private publicationTypeSvc: PublicationTypeService,
    private router: Router) {
    this.publicationSubscription = this.publicationSvc.elements.subscribe({ next: (value) => this.publications = value });
    this.publicationTypeSubscription = this.publicationTypeSvc.elements.subscribe({ next: (value) => this.publicationTypes = value });
  }

  ngOnInit() {
    this.publicationForm = this.fb.group({
      author: [''],
      code: [''],
      date: [new Date((new Date()).setHours(0, 0, 0))],
      edition: [''],
      eventId: [''],
      exhibitions: [''],
      isbn: [''],
      notes: [''],
      pages: [''],
      publications: [''],
      publicationType: [''],
      subTitle: [''],
      title: [''],
      verifiedDropDown: [''],
      verified: [true],
      volume: [''],
    })

    if (this.disabled) {
      this.publicationForm.disable();
    }
  }

  ngAfterViewInit() {
    this.formSubscription = this.publicationForm.valueChanges.debounceTime(300).subscribe(this.onValueChanged.bind(this));
  }

  ngOnDestroy() {
    this.publicationSubscription.unsubscribe();
    this.publicationTypeSubscription.unsubscribe();
    this.formSubscription.unsubscribe();
  }
  standarMode() {
    setValidators(this.publicationForm, this.standarValidatorsMap);
    this.validate = true;
  }
  searchMode() {
    setValidators(this.publicationForm, this.searchValidatorsMap);
    this.validate = false;
  }
  createMode() {
    this.standarMode();
    this.emptyDate(true);
  }
  setValue(value) {
    if (value.id) {
      this.notAllowedIds[0] = value.id
    } else {
      this.notAllowedIds = []
    }
    this.event = value;
    this.publicationForm.patchValue({ ...value, publications: value.publicationEvents, exhibitions: value.exhibitionEvents });
    this.exhibitionData = value.exhibitionEvents;
    this.publicationData = value.publicationEvents;
  }

  reset() {
    this.publicationForm.reset();
    this.publicationForm.patchValue({ verified: true })
    this.exhibitionData = [];
    this.publicationData = [];
    this.notAllowedIds = [];
    this.resetInputFields();
  }

  resetInputFields() {
    this.listInputs.forEach((inputs) => inputs.reset())
  }

  onValueChanged() {
    const validationMessages = this.dictionary.PUBLICATION.VALIDATION;
    this.formErrors = _.reduce(this.publicationForm.controls, (prev, value, index) => {
      if (value.invalid && index !== 'establishment') {
        const message = _.get(validationMessages, `${_.toUpper(_.snakeCase(index))}`);
        const pairs = _.toPairs(value.errors)
        prev[index] = _.get(message, pairs[0][0])
      }
      return prev
    }, {});
  }

  changeExhibitionValues(value) {
    this.publicationForm.patchValue({ exhibitions: value })
  }

  changePublicationValues(value) {
    this.publicationForm.patchValue({ publications: value })
  }

  handleListInputMessages(value: Message) {
    this.messageEmitted.emit(value);
  }

  emptyDate(now = false) {
    const dateField = this.publicationForm.controls.date
    if (now) {
      if (_.get(dateField, 'value._isValid', false)) {
        dateField.patchValue(dateField.value)
      } else {
        dateField.patchValue(moment().format(GeneralConstants.DATE_FORMAT.formatDateUIDisplay))
      }
    } else {
      dateField.patchValue(null)
    }
  }

  getSearchData = () => {
    const currentSearchData = _.clone(this.publicationForm.value);
    return {
      ..._.pick(currentSearchData,
        ['author', 'edition', 'eventId', 'isbn', 'notes', 'pages', 'id', 'subTitle', 'title', 'volume', 'code']),
      publicationTypeId: _.get(currentSearchData, 'publicationType.id'),
      establishmentId: _.get(currentSearchData, 'establishment.establishment.id'),
      type: _.upperCase(GeneralDictionary.SECTIONS.PUBLICATION),
      date: formatDate(currentSearchData.date, true),
      verified: _.get(currentSearchData, 'verifiedDropDown.realValue'),
    }
  }

  getEditionData() {
    const copy = _.clone(this.publicationForm.value);
    const data: any = _.pick(copy, ['author', 'edition', 'isbn', 'notes', 'pages',
      'publicationType', 'id', 'subTitle', 'title', 'verified', 'volume', 'code'])
    data.establishment = _.get(copy, 'establishment.establishment');
    data.type = _.upperCase(GeneralDictionary.SECTIONS.PUBLICATION);
    data.date = formatDate(copy.date, true);
    data.exhibitionEvents = this.getExhibitionEvents(copy);
    data.publicationEvents = this.getPublicationEvents(copy);
    return data;
  }

  private getExhibitionEvents(data) {
    return _.map(data.exhibitions, (exhibition: any) => ({ id: exhibition.id, type: 'EXHIBITION' }))
  }

  private getPublicationEvents(data) {
    return _.map(data.publications, (publication: Publication) => ({ id: publication.id, type: 'PUBLICATION' }))
  }

  goToEventPage() {
    this.router.navigate([`/publication/${this.event.id}`]);
    return false;
  }

}

