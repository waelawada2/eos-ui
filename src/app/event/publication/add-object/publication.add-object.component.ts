import { Component, AfterViewInit, OnDestroy, ViewChild, forwardRef } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { DialogService } from 'ng2-bootstrap-modal';
import { GeneralDictionary, GeneralConstants } from '../../shared/constants';
import { AddObjectToEventService, PublicationItemService, PublicationService } from '../../shared/services';
import { PublicationFormComponent } from '../form/publication.form.component';
import { ConfirmComponent } from '../../shared/components';
import { getErrorStringFromServer } from '../../shared/utils';
import { Subscription } from 'rxjs/Subscription';
import * as _ from 'lodash';

@Component({
  providers: [PublicationService],
  templateUrl: './publication.add-object.component.html',
  styleUrls: [
    '../../shared/styles/tables.scss',
    '../../shared/styles/forms.scss',
    './publication.add-object.component.scss',
  ],
})
export class PublicationAddObjectComponent implements AfterViewInit, OnDestroy {
  dictionary = GeneralDictionary;
  hiddenFormFields: string[] = ['exhibition', 'publication', 'notes', 'verified'];
  private publicationSubscription: Subscription;
  private events: any[];
  private options = GeneralConstants.NOTIFICATIONS.OPTIONS;
  private objectId;
  private title = `${_.upperFirst(GeneralDictionary.SECTIONS.PUBLICATION)} ${_.upperFirst(GeneralConstants.ENTITY_TYPE.EVENT)}`;
  private emptyMessage = GeneralDictionary.RESULT_MESSAGES
    .NO_RECORDS_FOUND(`${GeneralDictionary.SECTIONS.PUBLICATION} ${GeneralConstants.ENTITY_TYPE.EVENT}s`)

  @ViewChild(PublicationFormComponent) form: PublicationFormComponent;

  constructor(
    private publicationService: PublicationService,
    private publicationItemService: PublicationItemService,
    private addObjectToEventService: AddObjectToEventService,
    protected notificationSvc: NotificationsService,
    protected dialogService: DialogService,
    private router: Router) {
  }

  ngAfterViewInit() {
    this.publicationSubscription = this.publicationService.elements.skip(1).subscribe(this.processPublications.bind(this));
    this.objectId = this.addObjectToEventService.objectId;
    setTimeout(() => this.form.reset())
    if (!this.objectId) {
      this.returnToObjects();
    }
  }

  ngOnDestroy() {
    this.publicationSubscription.unsubscribe();
  }

  processPublications(events) {
    this.events = events;
  }

  getPublications() {
    this.publicationService.getElements();
  }

  close() {
    this.returnToObjects();
  }

  returnToObjects() {
    this.addObjectToEventService.reset();
    this.router.navigate(['object'], { queryParams: { id: this.objectId } });
  }

  filter() {
    const data = this.form.getSearchData()
    this.publicationService.getElements({ ...data, sort: GeneralConstants.SORTING.PUBLICATION.DESC }, _.get(data, 'eventId'))
  }

  addToObject(event) {
    const disposable = this.dialogService.addDialog(ConfirmComponent, {
      title: GeneralDictionary.NOTIFICATIONS.ADD_OBJECT_TO_EVENT_TITLE,
      message: GeneralDictionary.NOTIFICATIONS.ADD_OBJECT_TO_EVENT(_.upperFirst(_.toLower(GeneralDictionary.SECTIONS.PUBLICATION)))
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        this.doCreation(event);
      }
      disposable.unsubscribe()
    });
  }

  doCreation(event) {
    this.publicationItemService.create({
      type: _.toUpper(GeneralDictionary.SECTIONS.PUBLICATION),
      event: _.pick(event, ['id', 'type']),
      objects: [{ objectId: this.objectId }]
    }).subscribe((result) => {
      const response = JSON.parse(_.get(result, '_body'));
      if (_.has(response, 'error')) {
        return this.handleErrorAction(result);
      }
      this.addObjectToEventService.event = event;
      this.addObjectToEventService.item = response;
      this.addObjectToEventService.item.event = event;
      this.addObjectToEventService.creating = true;
      const url = `${_.lowerCase(GeneralDictionary.SECTIONS.PUBLICATION)}/${event.id}`;
      this.router.navigate([url]);
    })
  }

  handleErrorAction(response) {
    this.notificationSvc.error(GeneralDictionary.NOTIFICATIONS.error,
      getErrorStringFromServer(response), GeneralConstants.NOTIFICATIONS.OPTIONS);
  }

  tableScrolled() {
    const totalElements = this.publicationService.page.value.totalElements;
    if (totalElements > this.events.length) {
      this.publicationService.getNextElements()
    }
  }

  addEvent() {
    const url = `${_.lowerCase(GeneralDictionary.SECTIONS.PUBLICATION)}/new`;
    this.addObjectToEventService.creating = true;
    this.router.navigate([url]);
  }
}
