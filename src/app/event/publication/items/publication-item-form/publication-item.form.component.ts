import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormsDictionary } from '../../../shared/constants';
import { ColourTypeService, PublicationItemTypeService } from '../../../shared/services'
import { autocompleListFormatter, autocompleteByValue } from '../../../shared/utils';
import { ObjectValidator } from '../../../shared/validators';
import { Subscription } from 'rxjs/Subscription';
import * as _ from 'lodash';


@Component({
  selector: 'app-publication-item-form',
  templateUrl: './publication-item.form.component.html',
  styleUrls: [
    '../../../shared/styles/forms.scss',
  ],
})
export class PublicationItemFormComponent implements OnInit, OnDestroy {
  dictionary = FormsDictionary;

  form: FormGroup;
  formErrors: string[] = [];

  itemTypes: any[];
  colourTypes: any[];
  current: any;

  itemTypesSubscription: Subscription;
  colourTypesSubscription: Subscription;

  listFormatter = autocompleListFormatter;
  listFortmatterByValue = autocompleteByValue;

  constructor(
    private fb: FormBuilder,
    private colourTypeSvc: ColourTypeService,
    private pubItemTypeSvc: PublicationItemTypeService,
  ) {
    this.itemTypesSubscription = this.pubItemTypeSvc.elements.subscribe({ next: (types) => this.itemTypes = types })
    this.colourTypesSubscription = this.colourTypeSvc.elements.subscribe({ next: (types) => this.colourTypes = types });
  }

  ngOnInit() {
    this.form = this.fb.group({
      cataloguingPage: ['', Validators.maxLength(5)],
      number: ['', [Validators.maxLength(50)]],
      plate: ['', [Validators.maxLength(50)]],
      code: [''],
      illustrationPage: ['', [Validators.maxLength(5)]],
      figure: ['', [Validators.maxLength(50)]],
      colour: ['', [Validators.maxLength(5), ObjectValidator(false)]],
      type: ['', [ObjectValidator(false)]],
    });
  }

  ngOnDestroy() {
    this.itemTypesSubscription.unsubscribe();
    this.colourTypesSubscription.unsubscribe();
  }

}
