import { OnInit, Component, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { PublicationItemFormComponent } from '../publication-item-form/publication-item.form.component';
import { ItemDetailsFormComponent } from '../../../shared/components/item-details-form/item-details.form'
import { GeneralDictionary, FormsDictionary } from '../../../shared/constants';
import { convertToAutoformatCompatibleObject } from '../../../shared/utils';
import * as _ from 'lodash';
interface Data {
  title: string,
  item: any,
  isClosable: boolean,
}

@Component({
  styleUrls: ['../../../shared/styles/modals.scss', './publication-single-item.component.scss'],
  templateUrl: './publication-single-item.component.html',
})
export class PublicationSingleItemComponent extends DialogComponent<Data, any> implements Data, OnInit, AfterViewInit {

  dictionary = FormsDictionary


  item: any;
  title: string;
  isClosable: boolean;
  form: FormGroup;
  validForm = true;

  hideDetailElements: string[] = ['itemId'];

  @ViewChild(PublicationItemFormComponent) formItem: PublicationItemFormComponent;
  @ViewChild(ItemDetailsFormComponent) formDetails: ItemDetailsFormComponent;

  constructor(public dialogService: DialogService, private fb: FormBuilder) {
    super(dialogService);
  }

  ngOnInit() {
    this.form = this.fb.group({
      main: this.formItem.form,
    })
  }

  ngAfterViewInit() {
    this.fillFormPublicationDetails();
    this.formItem.form.valueChanges.subscribe(this.checkFormsValidity.bind(this));
    this.formDetails.form.valueChanges.subscribe(this.checkFormsValidity.bind(this));
  }

  checkFormsValidity() {
    this.validForm = this.formItem.form.valid && this.formDetails.form.valid;
  }

  save() {
    this.result = this.getFormData();
    this.close();
  }

  getFormData() {
    const colour = _.get(this.formItem.form.value, 'colour.id');
    const publicationType = _.get(this.formItem.form.value, 'type.id');
    return {
      ..._.pick(this.formItem.form.value, ['cataloguingPage', 'figure', 'number', 'plate']),
      illustration: _.get(this.formItem.form.value, 'illustrationPage'),
      publicationItemType: publicationType ? { id: _.get(this.formItem.form.value, 'type.id') } : null,
      comments: _.get(this.formDetails.form.value, 'notes'),
      colourType: colour ? { id: _.get(this.formItem.form.value, 'colour.id') } : null,
      objects: this.item.objects,
      event: _.pick(this.item.event, ['type', 'id']),
      ..._.pick(this.item, ['consignmentEvent', 'cataloguingNotes', 'objects', 'id', 'type'])
    }
  }

  openItem() {
    const url = `${_.lowerCase(GeneralDictionary.SECTIONS.PUBLICATION)}/${this.item.event.id}/items/${this.item.id}`;
    this.result = { redirect: true, url }
    this.close();
  }

  private fillFormPublicationDetails() {
    this.formItem.form.patchValue({
      illustrationPage: this.item.illustration,
      ..._.pick(this.item, ['cataloguingPage', 'number', 'plate', 'figure']),
      colour: convertToAutoformatCompatibleObject(this.item.colourType),
      type: convertToAutoformatCompatibleObject(this.item.publicationItemType, 'value'),
    });

    this.formDetails.form.patchValue({
      notes: this.item.comments,
    })
  }

}
