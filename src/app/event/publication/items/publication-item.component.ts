import { AfterViewInit, Component, forwardRef, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { BlockScreenService } from '../../shared/services/block-screen/block-screen.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogService } from 'ng2-bootstrap-modal';
import { NotificationsService } from 'angular2-notifications';
import { GeneralConstants, FormsDictionary } from '../../shared/constants';
import { Publication } from '../../shared/models';
import { PublicationFormComponent } from './../form/publication.form.component';
import { AddObjectToEventService, NavigationService, PublicationItemService, PublicationService } from '../../shared/services'
import { convertToAutoformatCompatibleObject, getFormFormattedPublication } from '../../shared/utils';
import { ObjectValidator } from '../../shared/validators';
import { SothebysLifeCycle, STATES, URLS } from '../../sothebys-lifecycle.abstract';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { SORTING } from '../../shared/components/navigation-header/navigation-header.sorting.interface';
import * as _ from 'lodash';
import { PublicationItemFormComponent } from 'app/event/publication/items/publication-item-form/publication-item.form.component';

const TYPE = 'PUBLICATION';

@Component({
  selector: 'app-publication-item',
  templateUrl: './publication-item.component.html',
  styleUrls: [
    '../../shared/styles/panels.scss',
    '../../shared/styles/forms.scss',
    './publication-item.component.scss',
  ],
})
export class PublicationItemComponent extends SothebysLifeCycle implements AfterViewInit, OnInit, OnDestroy {
  dictionary = FormsDictionary;

  form: FormGroup;
  formErrors: string[] = [];
  hideElements = ['exhibition', 'edition', 'eventId', 'isbn', 'pages', 'publication', 'notes', 'verified', 'verifiedDropDown', 'volume'];
  publicationId: string;
  publicationElement: Publication;

  items: any[];
  current: any;

  options = GeneralConstants.NOTIFICATIONS.SETUP;

  hideDetailElements: string[] = ['itemId'];

  private currentSearch: any = {};
  private currentSearchData: any = {};
  private publicationSubscription: Subscription;
  private itemsSubscription: Subscription;
  private pageSubscription: Subscription;
  protected getErrors: BehaviorSubject<any>;

  @ViewChild(forwardRef(() => PublicationFormComponent)) publicationForm: PublicationFormComponent;
  @ViewChild(PublicationItemFormComponent) formPublicationDetails: PublicationItemFormComponent;

  constructor(private fb: FormBuilder,
    private ps: PublicationService,
    private pubItemSvc: PublicationItemService,
    protected navService: NavigationService,
    protected notificationSvc: NotificationsService,
    protected blockScreenService: BlockScreenService,
    private addObjectToEventService: AddObjectToEventService,
    protected dialogService: DialogService,
    protected route: ActivatedRoute,
    protected router: Router) {
    super();

    this.route.params.take(1).subscribe(this.loadFromURL.bind(this));

    this.section = TYPE;
    this.sectionType = GeneralConstants.ENTITY_TYPE.ITEM;
    this.getErrors = this.ps.error;

    this.ableToCreate = false;
  }

  ngOnInit() {
    this.form = this.fb.group({
    })
    this.ableToRemove.next(true)
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.publicationSubscription.unsubscribe();
    this.itemsSubscription.unsubscribe();
    this.pageSubscription.unsubscribe();
    this.addObjectToEventService.reset();
  }

  ngAfterViewInit() {
    this.publicationSubscription = this.ps.element.subscribe({ next: this.updateItem.bind(this) })
    this.form.valueChanges.subscribe(this.formChanged.bind(this))
    setTimeout(() => this.form.get('cataloguing').disable())
    this.route.params.take(1)
      .subscribe(params => setTimeout(() => {
        const eventId = _.get(params, 'publId');
        this.baseUrl = `publication/${eventId}/items`;
        if (params['id'] === URLS.NEW) {
          this.router.navigate([this.baseUrl, 'search']).then(super.initialize.bind(this))
        } else {
          super.initialize();
        }
      }));
    this.doDataSubscription();
  }

  doDataSubscription() {
    this.itemsSubscription = this.pubItemSvc.elements.skipWhile(() => this.state !== STATES.EDIT)
      .subscribe({ next: this.updatePublicationItems.bind(this) });
    this.pageSubscription = this.pubItemSvc.page.skipWhile(() => this.state !== STATES.DELETE && this.state !== STATES.EDIT)
      .subscribe({ next: (page) => this.updatePages(page.number, page.totalElements) })
  }

  formChanged() {
    this.validData.next(this.form.valid)
  }

  loadFromURL(params) {
    this.publicationId = params['publId'];
    this.baseUrl = `publication/${this.publicationId}/items`;
    this.getPublicationData();
  }

  getPublicationData() {
    if (this.publicationId) {
      this.ps.getElement(this.publicationId);
    } else {
      this.router.navigate(['publication', 'search']);
    }
  }

  updateItem(item) {
    this.publicationElement = item;
    if (item) {
      this.publicationForm.setValue(getFormFormattedPublication(item));
    }
  }

  getFormData() {
    const data = _.clone(this.form.value);
    const publData = _.clone(this.publicationForm.publicationForm.value);
    const colour = _.get(this.formPublicationDetails.form.value, 'colour.id');
    const publicationType = _.get(this.formPublicationDetails.form.value, 'type.id');
    return {
      consignmentEvent: _.get(this.current, 'consignmentEvent'),
      type: TYPE,
      ..._.pick(data.cataloguing, ['description', 'provenance', 'literature', 'exhibition', 'condition']),
      cataloguingNotes: _.get(data, 'cataloguing.notes'),
      ..._.pick(this.formPublicationDetails.form.value, ['cataloguingPage', 'figure', 'number', 'plate', 'code']),
      illustration: _.get(this.formPublicationDetails.form.value, 'illustrationPage'),
      event: { id: this.publicationId, type: TYPE },
      publicationItemType: publicationType ? { id: _.get(this.formPublicationDetails.form.value, 'type.id') } : null,
      comments: _.get(data, 'itemDetails.notes'),
      colourType: colour ? { id: _.get(this.formPublicationDetails.form.value, 'colour.id') } : null,
      objects: _.get(this.current, 'objects'),
    }
  }

  getSearchFormData() {
    this.currentSearchData = _.clone(this.formPublicationDetails.form.value);
    return {
      ..._.pick(this.currentSearchData, ['cataloguingPage', 'figure', 'number', 'plate', 'code']),
      type: TYPE,
      illustration: _.get(this.currentSearchData, 'illustrationPage'),
      colourTypeId: _.get(this.currentSearchData, 'colour.id'),
      publicationItemTypeId: _.get(this.currentSearchData, 'type.id'),
    }
  }

  protected clearForm(restore) {
    this.updatePages(0, 0);
    this.form.get('cataloguing').disable()
    this.formPublicationDetails.form.reset();
    this.form.get('itemDetails').reset();
    this.form.get('cataloguing').reset();
    this.currentSearch = {};
    this.loadPaginated = 0;
    if (restore) {
      this.form.patchValue(this.currentSearchData);
    }
    this.currentSearchData = {};
    this.current = null;
  }

  protected doSearchAction(id): BehaviorSubject<any> {
    if (id) {
      this.pubItemSvc.getElements({}, id)
    } else {
      this.currentSearch = { ...this.getSearchFormData(), size: this.pageSize };
      this.pubItemSvc.getItemsByUrl(this.publicationElement._links.items.href, false,
        { ...this.currentSearch, sort: this.getSortParameters() });
    }
    return this.pubItemSvc.elements;
  }

  private updatePublicationItems(items) {
    this.items = items;
    if (this.loadPaginated !== 0) {
      this.changePage(this.loadPaginated)
      this.loadPaginated = 0;
    }
    this.current = this.items[this.loadPaginated]
  }

  public create() {
    super.create();
    this.form.get('cataloguing').enable()
  }

  public search() {
    super.search();
    this.form.get('cataloguing').enable()
  }

  protected doCreateAction(): Observable<any> {
    return this.pubItemSvc.create(this.getFormData())
  }

  protected doEditAction(): Observable<any> {
    return this.pubItemSvc.amend(this.getFormData(), this.current.id);
  }

  protected doDeleteAction(): Observable<any> {
    return this.pubItemSvc.delete(this.current.id);
  }

  protected doImportAction(file: File): Observable<any> {
    return this.pubItemSvc.import(file, GeneralConstants.IMPORT_TYPES.publication_item);
  }

  protected doExportAction(): Observable<any> {
    return this.pubItemSvc.export(
      {
        ...this.currentSearch,
        type: _.get(this.publicationElement, 'type')
      },
      _.get(this.publicationElement, 'id'))
  }

  protected displayItemNumber(number: number) {
    this.form.get('cataloguing').enable()
    if (number >= 0 && this.items.length > number) {
      this.current = this.items[number];
      if (this.current) {
        this.fillFormPublicationDetails(this.current);
        this.fillFormCataloguing(this.current);
      }
      this.updatePages(number, this.pubItemSvc.page.value.totalElements)
    } else {
      this.pubItemSvc.getNextElements(number)
      this.loadPaginated = number;
    }
    return _.get(this.current, ['id'])
  }

  private fillFormPublicationDetails(item) {
    this.formPublicationDetails.form.patchValue({
      illustrationPage: item.illustration,
      ..._.pick(item, ['cataloguingPage', 'number', 'plate', 'figure', 'code']),
      colour: convertToAutoformatCompatibleObject(item.colourType),
      type: convertToAutoformatCompatibleObject(item.publicationItemType, 'value'),
    });
  }

  private fillFormCataloguing(item) {
    this.form.patchValue({
      cataloguing: {
        ..._.pick(item, ['description', 'provenance', 'literature', 'exhibition', 'condition']),
        notes: item.cataloguingNotes,
      },
      itemDetails: {
        notes: _.get(item, 'comments'),
      }
    });
  }

  protected reorderResults(): BehaviorSubject<any> {
    this.pubItemSvc.getItemsByUrl(this.publicationElement._links.items.href, false,
      { ...this.currentSearch, sort: this.getSortParameters(this.sortOrder) });
    return this.pubItemSvc.elements;
  }

  private getSortParameters(order: SORTING = SORTING.DESCENDING) {
    return order === SORTING.DESCENDING ? GeneralConstants.SORTING.PUBLICATION_ITEM.DESC : GeneralConstants.SORTING.PUBLICATION_ITEM.ASC
  }

}
