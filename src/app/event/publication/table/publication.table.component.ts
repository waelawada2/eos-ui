import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { GeneralConstants, GeneralDictionary, TableDictionary, EVENTCONST } from '../../shared/constants';
import { ContextMenuOptions } from '../../shared/components/context-menu/context-menu-options.interface'
import { Subject } from 'rxjs/Subject';
import * as _ from 'lodash';

@Component({
  selector: 'app-publication-table',
  templateUrl: './publication.table.component.html',
  styleUrls: [
    './../../shared/styles/tables.scss',
    './../../shared/styles/panels.scss',
    './publication.table.component.scss',
  ],
})
export class PublicationTableComponent {

  defaultImagePlaceholder = GeneralConstants.PLACEHOLDER;
  generalConstant = EVENTCONST.PUBLICATION_TABLE;
  generalDictionary = GeneralDictionary;
  tableDictionary = TableDictionary.PUBLICATION;
  filter = ''
  public constantsOwnership = GeneralConstants.OWNERSHIP.DEFAULT_VALUES;

  private menuOptions: ContextMenuOptions[] = [{
    text: GeneralDictionary.CONTEXT_MENU.DELETE(GeneralConstants.ENTITY_TYPE.ITEM),
    id: 'menu-delete',
    subject: new Subject<any>()
  }];

  @Input()
  set eventItems(items: any) {
    this.items = items;
  }

  @Input() noClientVersion = false
  @Input() title = TableDictionary.PUBLICATION.TABLE_TITLE

  @Output() moreItems: EventEmitter<any> = new EventEmitter();
  @Output() removeItem: EventEmitter<any> = new EventEmitter();
  @Output() showPublicationItemPopup: EventEmitter<any> = new EventEmitter();

  items: any[] = [];
  localItems: any;

  constructor(
    private router: Router,
  ) {
    this.menuOptions[0].subject.subscribe(this.removeItemHandler.bind(this));
  }

  onFilter(kw: any) {
    this.filter = kw;
  }

  itemsTableScrolled() {
    this.moreItems.emit();
  }

  editConsignment(id) {
    this.router.navigate(['consignment-purchase', id]);
  }

  addPerson(item) {
    this.router.navigate(['consignment-purchase-add-item', item.event.id, item.id, _.upperCase(GeneralDictionary.SECTIONS.CONSIGNMENT)],
      { fragment: this.constantsOwnership.SETDEFAULTVALUE });
  }

  removeItemHandler(item) {
    this.removeItem.emit(item)
  }

  showPublicationItemPopUp(item) {
    this.showPublicationItemPopup.emit(item);
  }
}
