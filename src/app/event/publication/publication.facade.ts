import { PublicationAddObjectComponent } from './add-object/publication.add-object.component';
import { PublicationComponent } from './publication.component';
import { PublicationFormComponent } from './form/publication.form.component';
import { PublicationItemComponent } from './items/publication-item.component';
import { PublicationItemFormComponent } from './items/publication-item-form/publication-item.form.component';
import { PublicationSingleItemComponent } from './items/publication-single-item/publication-single-item.component';
import { PublicationTableComponent } from './table/publication.table.component';

const PUBLICATION_COMPONENTS = [
  PublicationComponent,
  PublicationAddObjectComponent,
  PublicationFormComponent,
  PublicationItemComponent,
  PublicationItemFormComponent,
  PublicationSingleItemComponent,
  PublicationTableComponent,
];

export {
  PublicationComponent,
  PublicationAddObjectComponent,
  PublicationFormComponent,
  PublicationItemComponent,
  PublicationItemFormComponent,
  PublicationSingleItemComponent,
  PublicationTableComponent,
  PUBLICATION_COMPONENTS,
}
