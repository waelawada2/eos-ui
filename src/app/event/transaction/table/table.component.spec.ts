import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement }    from '@angular/core';
import { By }              from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA }          from '@angular/core';
import { TransactionService } from './../../shared/services';
import { KeysPipe } from './../../shared/pipes';
import { Observable } from 'rxjs/Observable';
import { TransactionComponent } from './../transaction.component';
import { TableComponent } from './table.component';
import { TRANSACTION_ITEMS } from '../../mocks/transaction-items.mock';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;
  let transactionService;
  let transactionComponent;
  let testItems = TRANSACTION_ITEMS;

  let mockedTransactionService = {
    getByUrl : () => {}
  };

  let mockedTransactionComponent = {
    showMessage: () => {}
  };


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TableComponent,
        KeysPipe,
       ],
      schemas:      [ NO_ERRORS_SCHEMA ],
      providers:    [ 
        {provide: TransactionService, useValue: mockedTransactionService },
        {provide: TransactionComponent, useValue: mockedTransactionComponent }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    transactionService = fixture.debugElement.injector.get(TransactionService);
    transactionComponent = fixture.debugElement.injector.get(TransactionComponent);
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should show items in the table', () => {
    let debugElements: DebugElement[],
      selector = '.event-transaction-table-component #eventLotTable tbody tr';

    component.items = testItems;
    fixture.detectChanges();
    debugElements = fixture.debugElement.queryAll(By.css(selector));
    expect(debugElements.length).toBe(2);
  });

  it('should call the transaction and notifications, and add items on scrolldown event', () => {
    let spyService = spyOn(transactionService, 'getByUrl')
      .and.returnValue(Observable.of(testItems));

    let spyNotify = spyOn(transactionComponent, 'showMessage');

    component.items = testItems;
    expect(component.items._embedded.items.length).toBe(2);
    
    component.lotTableScrolled();
    fixture.detectChanges();
    
    expect(spyService).toHaveBeenCalled();
    expect(spyNotify).toHaveBeenCalled();
    expect(component.items._embedded.items.length).toBe(4);
  });
});
