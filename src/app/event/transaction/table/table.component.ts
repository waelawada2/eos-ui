import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { TransactionItem } from '../../shared/models';
import { EVENTCONST, TableDictionary, GeneralConstants, GeneralDictionary } from '../../shared/constants';
import { ContextMenuOptions } from '../../shared/components/context-menu/context-menu-options.interface'
import { Subject } from 'rxjs/Subject';
import * as _ from 'lodash';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: [
    './../../shared/styles/panels.scss',
    './../../shared/styles/tables.scss',
    './table.component.scss'
  ]
})
export class TableComponent implements OnInit {

  public tableHeaders = [];
  public catalogItems = [];
  public originalItems;
  public eventInfo: object;
  public filter: string;
  public defaultImagePlaceholder: string;
  public defaultCataloguing: string;
  public cataloguingItems: Object;
  public items: any;
  public completeItems: any;
  public currencyCode;
  public currencySymbol;
  public generalDictionary = GeneralDictionary;

  private menuOptions: ContextMenuOptions[] = [{
    text: GeneralDictionary.CONTEXT_MENU.DELETE(GeneralConstants.ENTITY_TYPE.ITEM),
    id: 'menu-delete',
    subject: new Subject<any>()
  }];

  @Input() set transactionItems(_items: any) {
    // prefilter results to avoid pipes and functions in ngfor
    this.items = _.clone(_items);
    this.completeItems = _items;
  }
  @Input() set transactionCurrencies(currency) {
    if (currency) {
      this.currencyCode = currency.code;
      this.currencySymbol = currency.symbol;
    } else {
      this.currencyCode = GeneralConstants.CURRENCY_DEFAULTS.currencyCode;
      this.currencySymbol = GeneralConstants.CURRENCY_DEFAULTS.currencySymbol;
    }
  }

  @Input() title = TableDictionary.TRANSACTION.TABLE_TITLE;
  @Input() noClientVersion = false;

  @Output() showTransactionItemPopup: EventEmitter<any> = new EventEmitter();
  @Output() moreItems: EventEmitter<any> = new EventEmitter();
  @Output() removeItem: EventEmitter<any> = new EventEmitter();

  constructor(
    private router: Router,
  ) { }

  initTable() {
    this.tableHeaders = EVENTCONST.lotTableHeaders;
    this.cataloguingItems = TableDictionary.PRICING_LABELS;
    // fallback cases for objects while backend fix the object array
    this.defaultImagePlaceholder = GeneralConstants.PLACEHOLDER;
    this.defaultCataloguing = EVENTCONST.cataloguing;
  }

  isNumber(value) {
    return !isNaN(parseInt(value, 10));
  }

  lotTableScrolled() {
    this.moreItems.emit();
  }

  addPerson(item, type: string = '') {
    this.router.navigate(['consignment-purchase-add-item', item.event.id, item.id, _.toUpper(type)]);
  }

  filterItems(word: string) {
    const results = [];

    if (!word || typeof word !== 'string') {
      this.items._embedded.items = this.originalItems && this.originalItems.length ? this.originalItems : this.items._embedded.items;
      this.filter = undefined;
      return;
    }

    this.filter = word;
    this.originalItems = this.originalItems && this.originalItems.length ? this.originalItems : this.items._embedded.items;

    this.originalItems.forEach((row, index) => {
      const keys = Object.keys(row);
      keys.forEach((key) => {
        const stringified = JSON.stringify(row[key]).toLowerCase();
        if (stringified.indexOf(word.toLowerCase()) !== -1 && row !== results[results.length - 1]) {
          results.push(row);
        }
      })
    })

    return results.length ? results : this.originalItems;
  }

  ngOnInit() {
    this.initTable();
    this.menuOptions[0].subject.subscribe(this.deleteItem.bind(this));
  }

  editConsignmentPurchase(id) {
    this.router.navigate(['consignment-purchase', id]);
  }
  deleteItem(item) {
    this.removeItem.emit(item)
  }

  showTransactionItemPopUp(item) {
    this.showTransactionItemPopup.emit(item);
  }

}
