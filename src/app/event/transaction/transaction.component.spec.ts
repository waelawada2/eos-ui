import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TransactionComponent } from './transaction.component';
import { Detail } from './../shared/models';
import { NavigationService, TransactionService } from './../shared/services';
import { NotificationsService } from 'angular2-notifications';
import { EVENTTRANSACTION } from '../mocks/transaction-event.mock';
import { VENUES } from '../mocks/venues.mock';
import { CURRENCIES } from '../mocks/currencies.mock';
import { DEPARTMENTS } from '../mocks/departments.mock';

class MockTransactionService {
  getEventTransaction(): Observable<any> {
    return Observable.of(EVENTTRANSACTION);
  };

  getVenues(): Observable<any> {
    return Observable.of(VENUES);
  }

  getCurrencies(): Observable<any> {
    return Observable.of(CURRENCIES);
  }

  getDepartments(): Observable<any> {
    return Observable.of(DEPARTMENTS);
  }

  addEventTransaction(): Observable<any> {
    return Observable.of(EVENTTRANSACTION);
  }

  updateEventTransaction(): Observable<any> {
    return Observable.of(EVENTTRANSACTION);
  }

};
class MockNavigationService {
  getNavChangeEmitter(): Observable<any> {
    return Observable.of('event');
  }

  emitNavStatusEvent(): Observable<any> {
    return Observable.of('status');
  }
};

describe('TransactionComponent', () => {
  let component: TransactionComponent;
  let fixture: ComponentFixture<TransactionComponent>;

  const fb: FormBuilder = new FormBuilder();

  function createForm() {
   return fb.group({
      date: [new Date(), Validators.required],
      type: ['', Validators.required],
      title: ['', Validators.required],
      venue: ['', Validators.required],
      location: fb.group({
        countryName: [''],
        state: [''],
        cityName: [''],
        region:  ['']
      }),
      department: ['', Validators.required],
      saleNumber: ['', Validators.required],
      currency: ['', Validators.required],
      totalLots: [''],
      notes: ['']
    });
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionComponent ],
      providers: [
        {
          provide: TransactionService,
          useClass: MockTransactionService
        },
        {
          provide: NavigationService,
          useClass: MockNavigationService
        },
        NotificationsService
      ],
      imports: [ ReactiveFormsModule, FormsModule ],
      schemas : [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionComponent);
    component = fixture.componentInstance;
    component.ngAfterViewInit();
    fixture.detectChanges();
  });

  it('Form invalid when empty', () => {
    /*
    const transactionForm = fixture.componentInstance.transactionForm;
    expect(transactionForm.valid).toBe(false);
    fixture.detectChanges();
    */
  });

  it('submitting a form emits a event', () => {
    /* expect(component.transactionForm.valid).toBe(false);
    const transactionForm = fixture.componentInstance.transactionForm;

    transactionForm.controls['date'].setValue('08-08-2014 hh:mm:ss');
    transactionForm.controls['transactionType'].setValue('Auction');
    transactionForm.controls['title'].setValue('Something');
    transactionForm.controls['venue'].setValue('Some Venue');
    transactionForm.controls['department'].setValue('Some Department');
    transactionForm.controls['saleNumber'].setValue('1234');
    transactionForm.controls['currency'].setValue('USD');
    // expect(component.transactionForm.valid).toBe(true);

    const transactionDetail = component.transactionDetail;
    if ( transactionDetail ) {
      if ( transactionDetail['eventId'] ) {
        component.updateEventTransaction( transactionDetail['eventId'], transactionForm.controls );
      }
    }else {
      component.addEventTransaction( transactionForm.controls );
    }
    */
  });

});
