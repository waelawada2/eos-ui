import { Component, ViewChild, AfterViewInit, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { TransactionItemFormComponent } from '../form/transaction-item-form.component';
import { ItemDetailsFormComponent } from '../../../shared/components/item-details-form/item-details.form'
import { GeneralDictionary, FormsDictionary, GeneralConstants } from '../../../shared/constants';
import { convertToAutoformatCompatibleObject } from '../../../shared/utils';
import { TableComponent } from '../../table/table.component'
import { Subscription } from 'rxjs/Subscription';
import {
  autocompleListFormatter, autocompleteByValue, getErrorStringFromServer, getTransactionItemData,
  processParamsObject
} from '../../../shared/utils';
import * as _ from 'lodash';
interface Data {
  title: string,
  item: any,
  isClosable: boolean,
}

@Component({
  styleUrls: ['./transaction-single-item.component.scss'],
  templateUrl: './transaction-single-item.component.html',
})
export class TransactionSingleItemComponent extends DialogComponent<Data, any> implements Data, OnInit, AfterViewInit {

  dictionary = FormsDictionary

  item: any;
  title: string;
  form: FormGroup;
  itemFormHiddenFields;
  itemOptions = {};
  validateForm;
  transactionForm: FormGroup;
  formPublicationDetails: FormGroup;
  isClosable: boolean;
  validForm = true;

  @ViewChild(TransactionItemFormComponent) formItem: TransactionItemFormComponent;
  @ViewChild(ItemDetailsFormComponent) formDetails: ItemDetailsFormComponent;

  constructor(
    public dialogService: DialogService, private fb: FormBuilder
  ) {
    super(dialogService);
  }

  ngOnInit() {
    this.validateForm = false;
    this.form = this.fb.group({});
    this.itemFormHiddenFields = ['EoRDropdown'];
    this.itemOptions = {
      transactionType: this.item.transactionType,
      currency: this.item.event.currency
    };
  }

  ngAfterViewInit() {
    this.formItem.form.valueChanges.subscribe(this.checkFormsValidity.bind(this));
    this.formDetails.form.valueChanges.subscribe(this.checkFormsValidity.bind(this));
    this.fillFormDetails();
  }

  checkFormsValidity() {
    this.validForm = this.formItem.form.valid && this.formDetails.form.valid;
  }

  save() {
    this.result = this.getFormData();
    this.close();
  }

  getFormData() {
    return {
      objects: _.get(this.item.objects, 'objects'),
      type: this.item.event.type,
      ..._.pick(this.item, ['description', 'provenance', 'literature', 'exhibition', 'condition', 'cataloguingNotes',
        'comments', 'transactionType', 'consignmentEvent', 'objects', 'purchaseEvent', 'status', 'id']),
      event: _.pick(this.item.event, ['id', 'type']),
      legacyId: _.get(this.formDetails.form.value, 'itemId'),
      comments: _.get(this.formDetails.form.value, 'notes'),
      lotNumber: _.get(this.formItem.form.value, 'lotNumber'),
      status: _.get(this.formItem.form.value, 'status'),
      auction: processParamsObject(_.pick(this.formItem.form.value,
        ['estimateOnRequest', 'lowEstimate', 'highEstimate', 'term', 'hammer', 'premium'])),
      privateSale: _.pick(this.formItem.form.value, ['netBuyer', 'netVendor']),
    }
  }

  openItem() {
    const url = `${_.lowerCase(GeneralDictionary.SECTIONS.TRANSACTION)}/${this.item.event.id}/items/${this.item.id}`;
    this.result = { redirect: true, url }
    this.close();
  }

  private fillFormDetails() {
    const formValues = {
      status: convertToAutoformatCompatibleObject(this.item.status),
      ..._.pick(this.item, ['lotNumber']),
      ..._.pick(this.item.auction, ['estimateOnRequest', 'lowEstimate', 'highEstimate', 'hammer', 'premium']),
      term: convertToAutoformatCompatibleObject(_.get(this.item.auction, 'term')),
      ..._.pick(this.item.privateSale, ['netBuyer', 'netVendor']),
    };

    this.formItem.form.patchValue(formValues);

    this.formDetails.form.patchValue({
      notes: this.item.comments,
      itemId: this.item.legacyId,
    })
  }

}
