import { Component, Input, forwardRef, OnDestroy, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { BlockScreenService } from '../../shared/services/block-screen/block-screen.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogService } from 'ng2-bootstrap-modal';
import { EVENTCONST, GeneralDictionary, GeneralConstants, DetailsDictionary, FormsDictionary } from '../../shared/constants';
import { Subscription } from 'rxjs/Subscription';
import { autocompleListFormatter, autocompleteByValue, getErrorStringFromServer, processParamsObject } from '../../shared/utils';
import { DetailsComponent } from '../details/details.component';
import { NavigationService, TransactionService, TransactionItemService } from '../../shared/services';
import { NotificationsService } from 'angular2-notifications';
import { Detail, Transaction, TransactionItem } from '../../shared/models';
import { SothebysLifeCycle, STATES, URLS } from '../../../event/sothebys-lifecycle.abstract';
import { formatDate, convertToAutoformatCompatibleObject } from '../../shared/utils';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SORTING } from '../../shared/components/navigation-header/navigation-header.sorting.interface';
import * as _ from 'lodash';
import * as moment from 'moment/moment';
const TYPE = 'TRANSACTION';

@Component({
  selector: 'app-transaction-item',
  templateUrl: './transaction-item.component.html',
  styleUrls: [
    '../../shared/styles/forms.scss',
    '../../shared/styles/panels.scss',
    './transaction-item.component.scss',
  ],
})
export class TransactionItemComponent extends SothebysLifeCycle implements OnInit, OnDestroy, AfterViewInit {

  currency;
  currentSearch;
  currentSearchData: any = {};
  dictionary = FormsDictionary;
  event;
  transactionEvent;
  form: FormGroup;
  formErrors: string[] = [];
  transactionForm: FormGroup;
  formPublicationDetails: FormGroup;
  hideElements = ['notes', 'totalLots', 'eventId'];
  itemFormHiddenFields;
  items = [];
  itemId;
  itemOptions = {};
  currentItem;
  listFormatter = autocompleListFormatter;
  listFortmatterByValue = autocompleteByValue;
  navSubscription: Subscription;
  transactionServiceSubscription: Subscription;
  paramsSubs: Subscription;
  auctionValueToOmit = ['lotNumber', 'status'];
  auctionValueToOmitOnSearch = ['lotNumber', 'status', 'term', 'estimateOnRequest', 'estimateOnRequestDropdown'];
  cataloguingFields = ['description', 'provenance', 'literature', 'exhibition', 'condition'];
  transactionId: string;
  transactionType;
  stateObservableSubscription

  options = GeneralConstants.NOTIFICATIONS.SETUP;
  validate = false;

  protected getErrors: BehaviorSubject<any>;

  @ViewChild(forwardRef(() => DetailsComponent)) private eventDetails: DetailsComponent;

  constructor(
    protected blockScreenService: BlockScreenService,
    protected navService: NavigationService,
    private formBuilder: FormBuilder,
    protected route: ActivatedRoute,
    private myCustomRoute: ActivatedRoute,
    protected transactionService: TransactionService,
    protected transactionItemService: TransactionItemService,
    protected notificationSvc: NotificationsService,
    protected dialogService: DialogService,
    protected router: Router) {
    super()

    this.getErrors = transactionItemService.error;
    this.itemFormHiddenFields = ['EoRDropdown', 'eventId'];
    this.section = TYPE;
    this.sectionType = GeneralConstants.ENTITY_TYPE.ITEM;
    this.ableToCreate = false;
  }
  ngOnInit() {
    this.form = this.formBuilder.group({code: ['']});
    this.ableToRemove.next(true)
  }

  ngOnDestroy() {
    this.stateObservableSubscription.unsubscribe();
    super.ngOnDestroy();
  }

  ngAfterViewInit() {
    this.transactionForm = this.eventDetails.transactionForm;
    this.form.valueChanges.subscribe((value) => { this.validData.next(this.form.valid); });

    this.route.params
      .take(1)
      .subscribe(params => setTimeout(() => {
        const eventId = _.get(params, 'transactionId');
        this.baseUrl = `transaction/${eventId}/items`;

        if (params['id'] === URLS.NEW) {
          this.router.navigate([this.baseUrl, 'search'])
            .then(() => this.transactionService.getElements({}, eventId))
        } else {
          this.transactionService.getElements({}, eventId)
        }
      }));

    this.transactionService.elements
      .skip(1)
      .take(1)
      .subscribe((transaction) => {
        if (transaction.length === 0) {
          return;
        }
        this.transactionEvent = transaction[0];
        this.eventDetails.setDetailData(this.transactionEvent);
        this.transactionType = _.get(this.transactionEvent, 'transactionType');
        this.itemOptions = this.buildItemOptions(this.transactionEvent);
        super.initialize();
      })

    this.stateObservableSubscription = this.stateObservable.debounceTime(100).subscribe((data) => {
      this.validate = this.state === STATES.NEW
      switch (data) {
        case STATES.NEW:
        case STATES.EDIT:
          this.itemFormHiddenFields = ['EoRDropdown', 'eventId'];
          break;
        case STATES.SEARCH:
          this.itemFormHiddenFields = ['EoR', 'eventId'];
          break;
      }

    });

  }

  buildItemOptions(transactionEvent: Transaction) {
    return {
      currency: _.get(transactionEvent, 'currency'),
      date: _.get(transactionEvent, 'date'),
      transactionType: this.transactionType
    }
  }

  protected doSearchAction(id): BehaviorSubject<any> {
    if (!id) {
      const transactionLinks = _.get(this.transactionEvent, '_links.items.href');
      this.currentSearch = { ... this.getFormSearchData(), size: this.pageSize };
      this.transactionItemService.getItemsByUrl(transactionLinks, false,
        { ...this.currentSearch, sort: this.getSortParameters() });
    } else {
      this.transactionItemService.getElements({}, id);
    }

    const elements = this.transactionItemService.elements;
    elements.takeUntil(this.stateObservable.skip(1))
      .subscribe({
        next: this.updateItemTransaction.bind(this),
      });
    this.transactionItemService.page.takeUntil(this.stateObservable.skip(1))
      .subscribe({ next: (page) => this.updatePages(page.number, page.totalElements) })
    return elements;
  }

  protected doCreateAction(): Observable<any> {
    return this.transactionItemService.create(this.getFormData());
  }

  protected doEditAction(): Observable<any> {
    return this.transactionItemService.amend(this.getFormData(), this.currentItem.id);
  }

  protected doDeleteAction(): Observable<any> {
    return this.transactionItemService.delete(this.currentItem.id);
  }

  protected doImportAction(file: File): Observable<any> {
    return this.transactionItemService.import(file, GeneralConstants.IMPORT_TYPES.transaction_item);
  }

  protected doExportAction(): Observable<any> {
    return this.transactionItemService.export(
      {
        ...this.currentSearch,
        type: _.get(this.transactionEvent, 'type')
      },
      _.get(this.transactionEvent, 'id'));
  }

  protected clearForm(restore) {
    this.form.reset({ auction: { estimateOnRequest: false } });
    this.itemId = '';
    this.updatePages(0, 0);
    this.currentSearch = {};
    this.loadPaginated = 0;
    if (restore) {
      this.form.patchValue(this.currentSearchData)
    }
    this.currentSearchData = {};
    this.currentItem = null
  }
  protected displayItemNumber(number) {
    return this.getTransactionItemData(number);
  }

  updateItemTransaction(itemTransaction) {
    this.items = itemTransaction;
    if (this.loadPaginated !== 0) {
      this.changePage(this.loadPaginated)
      this.loadPaginated = 0;
    }
  }

  getTransactionItemData(index = 0) {
    if (!this.items) {
      this.updatePages(0, 0);
      return;
    }
    if (index >= 0 && this.items.length > index) {
      this.currentItem = this.items[index];
      if (this.currentItem) {
        this.updateItem(this.currentItem);
      }
      this.updatePages(index, this.transactionItemService.page.value.totalElements);
    } else {
      this.transactionItemService.getNextElements(index);
      this.loadPaginated = index;
    }
    return _.get(this.currentItem, 'id');
  }

  getFormSearchData() {
    this.currentSearchData = _.clone(this.form.value);
    let _data = this.getCommonSearchData(this.currentSearchData);
    switch (this.transactionType) {
      case GeneralConstants.ITEM_TYPES.AUCTION:
        _data = _.merge(_data, this.getAuctionSearchData(this.currentSearchData.auction));
        break;
      case GeneralConstants.ITEM_TYPES.PRIVATE_SALE:
        _data = _.merge(_data, this.getPrivateSaleSearchData(this.currentSearchData.privateSale));
        break;
    }
    return _data;
  }
  getCommonSearchData(data): any {
    return {
      type: 'TRANSACTION',
      legacyId: _.get(data, 'itemDetails.itemId'),
      comments: _.get(data, 'itemDetails.notes'),
    }
  }
  getPrivateSaleSearchData(data): any {

    const privateSearch = {};
    _.each(_.omit(data, this.auctionValueToOmitOnSearch), (value, key) => {
      privateSearch['privateSale.' + key] = value;
    })
    return {
      ...privateSearch,
      transactionStatusId: _.get(data, 'status.id'),
      lotNumber: _.get(data, 'lotNumber'),
    }
  }

  getAuctionSearchData(data): any {
    const auctionSearch: any = { auction: {} };
    _.each(_.omit(data, this.auctionValueToOmitOnSearch), (value, key) => {
      auctionSearch['auction.' + key] = value;
    })
    auctionSearch['auction.estimateOnRequest'] = _.get(data, 'estimateOnRequestDropdown.realValue');
    return {
      ...auctionSearch,
      lotNumber: _.get(data, 'lotNumber'),
      transactionStatusId: _.get(data, 'status.id'),
      auctionTermId: _.get(data, 'term.id'),
    }
  }

  getFormData() {
    const data = _.clone(this.form.value);
    return {
      code: data.code,
      consignmentEvent: _.get(this.currentItem, 'consignmentEvent'),
      purchaseEvent: _.get(this.currentItem, 'purchaseEvent'),
      type: 'TRANSACTION',
      ..._.pick(data.cataloguing, this.cataloguingFields),
      cataloguingNotes: _.get(data, 'cataloguing.notes'),
      event: { id: _.get(this.transactionEvent, 'id'), type: _.get(this.transactionEvent, 'type') },
      legacyId: _.get(data, 'itemDetails.itemId'),
      comments: _.get(data, 'itemDetails.notes'),
      transactionType: _.get(this.transactionEvent, 'transactionType'),
      ...this.privateSaleFormData(data.privateSale),
      ...this.auctionFormData(data.auction),
      objects: _.get(this.currentItem, 'objects'),
    }
  }
  auctionFormData(data) {
    if (!data) {
      return null;
    }
    return {
      auction: processParamsObject(_.omit(data, this.auctionValueToOmit)),
      lotNumber: _.get(data, 'lotNumber'),
      status: _.get(data, 'status')
    }

  }

  privateSaleFormData(data) {
    if (!data) {
      return null;
    }
    return {
      privateSale: _.omit(data, this.auctionValueToOmit),
      lotNumber: _.get(data, 'lotNumber'),
      status: _.get(data, 'status'),
    }
  }

  buildAuction(data) {
    if (!_.get(data, 'auction')) {
      return {};
    }
    const _auction = {
      ...data.auction,
      ..._.pick(data, this.auctionValueToOmit)
    }
    _auction.status = convertToAutoformatCompatibleObject(_.get(_auction, 'status'));
    _auction.term = convertToAutoformatCompatibleObject(_.get(_auction, 'term'));
    return _auction;
  }
  buidPrivateSale(data) {
    if (!_.get(data, 'privateSale')) {
      return {};
    }
    const _privateSale = {
      ...data.privateSale,
      ..._.pick(data, this.auctionValueToOmit),
    }
    _privateSale.status = convertToAutoformatCompatibleObject(_.get(_privateSale, 'status'));
    return _privateSale;
  }
  buildFormData(data) {
    return {
      code: data.code,
      itemDetails: {
        itemId: data['legacyId'],
        notes: data['comments'],
      },
      auction: this.buildAuction(data),
      privateSale: this.buidPrivateSale(data),
      cataloguing: { ..._.pick(data, this.cataloguingFields), notes: _.get(data, 'cataloguingNotes') },
    }

  }

  updateItem(data) {
    this.form.patchValue(this.buildFormData(data));
  }

  protected reorderResults(): BehaviorSubject<any> {
    this.transactionItemService.getItemsByUrl(_.get(this.transactionEvent, '_links.items.href'), false,
      { ...this.currentSearch, sort: this.getSortParameters(this.sortOrder) });
    return this.transactionItemService.elements;
  }

  private getSortParameters(order: SORTING = SORTING.DESCENDING) {
    return order === SORTING.DESCENDING ? GeneralConstants.SORTING.TRANSACTION_ITEM.DESC : GeneralConstants.SORTING.TRANSACTION_ITEM.ASC
  }

}
