import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { FormsDictionary, GeneralConstants } from '../../../shared/constants';
import { autocompleListFormatter, autocompleteByValue } from '../../../shared/utils';
import { CurrencyCustomSymbolPipe } from '../../../shared/pipes';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { AuctionTermsServices, TransactionStatusService } from '../../../shared/services';
import { CurrencyFormattedInputDirective } from '../../../shared/components';
import { RangeValidator, noRequiredObjectValidator } from '../../../shared/validators';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-transaction-item-form-component',
  templateUrl: './transaction-item-form-component.html',
  styleUrls: [
    '../../../shared/styles/forms.scss',
    './transaction-item-form-component.scss',
  ]
})
export class TransactionItemFormComponent implements OnInit, OnDestroy {

  private LOT_LENGTH = 7;

  form: FormGroup;
  currency = '';
  date = '';
  dictionary = FormsDictionary;
  disabled = false;
  formErrors = {};
  listFormatter = autocompleListFormatter;
  EoRListFormatter = autocompleteByValue;
  statusList = [];
  termList = [];
  transactionType;
  types = GeneralConstants.ITEM_TYPES;
  validationMessages = this.dictionary.TRANSACTION_ITEMS.VALIDATION;
  validate = false;
  auctionTermSubscription: Subscription;
  statusSubscription: Subscription;
  EoRValues = [
    { realValue: true, value: this.dictionary.TRANSACTION_ITEMS.FIELDS.EOR },
    { realValue: false, value: this.dictionary.TRANSACTION_ITEMS.FIELDS.NOT_EOR },
  ]

  @Input() parentForm: FormGroup;

  @Input()
  hideElements: any[] = [];

  @Input()
  set disableForm(value: boolean) {
    this.disabled = value;
  }
  @Input() set options(options) {
    this.currency = _.get(options, 'currency');
    this.transactionType = _.get(options, 'transactionType');
    this.date = _.get(options, 'date');
    this.buildForm(this.transactionType);
  };

  @Input() set validation(validate) {
    this.validate = validate;
    if (this.form) {
      this.form.updateValueAndValidity();
    }
  }

  constructor(
    private formBuilder: FormBuilder,
    private auctionTermsService: AuctionTermsServices,
    private transactionStatusService: TransactionStatusService,
  ) { }

  buildForm(transactionType) {
    let form;
    if (!transactionType) {
      return;
    }
    switch (this.transactionType) {
      case this.types.AUCTION:
        form = this.createAuctionForm()
        break;
      case this.types.PRIVATE_SALE:
        form = this.createPrivateSaleForm();
        break;
    }
    this.parentForm.addControl(_.camelCase(transactionType), form);
    this.form.valueChanges.debounceTime(300).subscribe(this.onValueChanged.bind(this));
  }


  createAuctionForm() {
    return this.form = this.formBuilder.group({
      lotNumber: ['', Validators.maxLength(this.LOT_LENGTH)],
      estimateOnRequest: [false],
      estimateOnRequestDropdown: [''],
      lowEstimate: [''],
      highEstimate: [''],
      hammer: [''],
      premium: [''],
      term: ['', [noRequiredObjectValidator(false)]],
      status: ['', [noRequiredObjectValidator(false)]],
    });
  }
  createPrivateSaleForm() {
    return this.form = this.formBuilder.group({
      lotNumber: ['', Validators.maxLength(this.LOT_LENGTH)],
      netBuyer: [''],
      netVendor: [''],
      status: ['', [noRequiredObjectValidator(false)]],
    });
  }

  ngOnInit() {
    this.auctionTermSubscription = this.auctionTermsService.elements
      .subscribe(auctionTerms => this.termList = auctionTerms);

    this.statusSubscription = this.transactionStatusService.elements
      .subscribe(transactionStatus => this.statusList = transactionStatus);
  }

  ngOnDestroy() {
    this.statusSubscription.unsubscribe();
    this.auctionTermSubscription.unsubscribe();
  }

  onValueChanged() {
    this.formErrors = _.reduce(this.form.controls, (prev, value, index) => {
      if (value.invalid) {
        const message = _.get(this.validationMessages, `${_.toUpper(_.snakeCase(index))}`);
        const pairs = _.toPairs(value.errors)
        prev[index] = _.get(message, pairs[0][0])
      }
      return prev
    }, {});
  };

  setDateGreaterThatToday(date) {
    const _date = moment(date, GeneralConstants.DATE_FORMAT.formatDate);
    const _today = moment(new Date());
    return _date.isSameOrAfter(_today);
  };
}
