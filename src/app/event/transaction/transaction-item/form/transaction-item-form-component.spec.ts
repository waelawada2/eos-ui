import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionItemFormComponent } from './transaction-item-form.component';

describe('TransactionItemFormComponent', () => {
  let component: TransactionItemFormComponent;
  let fixture: ComponentFixture<TransactionItemFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionItemFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionItemFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
