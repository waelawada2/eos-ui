import * as _ from 'lodash';
import * as moment from 'moment/moment';
import { Component, OnDestroy, OnInit, Input, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { FormsDictionary, GeneralDictionary, FormsConstants, GeneralConstants } from '../../shared/constants';
import { Currency, Detail } from '../../shared/models';
import { CurrencyService, DepartmentService } from '../../shared/services';
import { RangeValidator, noRequiredObjectValidator } from '../../shared/validators';
import { autocompleListFormatter, convertToAutoformatCompatibleObject, formatDate, setValidators } from '../../shared/utils';
import { EstablishmentFormComponent } from '../../shared/components';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import 'rxjs/add/observable/of';
import { Router } from '@angular/router';

const TYPE = GeneralConstants.EVENT_TYPES.TRANSACTION;

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: [
    './../../shared/styles/forms.scss',
    './../../shared/styles/panels.scss',
    'details.component.scss'
  ],
})

export class DetailsComponent implements AfterViewInit, OnDestroy, OnInit {

  @ViewChild(DetailsComponent) establishmentFormComponent: EstablishmentFormComponent;

  public datePickerConfig = GeneralConstants.DATE_PICKER_CONFIG;
  public departments: any[];
  public currencies: Currency[];
  public dictionary = FormsDictionary;
  public validationMessages = FormsDictionary.VALIDATION;
  public transactionDetail: Detail;
  public initialEstablishment;
  public transactionTypes = FormsConstants.transactionTypes;
  public formLabels = GeneralDictionary.FORM_LABELS;
  public formErrors = {};
  public disabled = false;
  public validate;
  public currencySubscription: Subscription;
  public departmentSubscription: Subscription;
  public departmentFieldSubscription: Subscription;
  private formSubscription: Subscription;
  private currencyFieldSubscription: Subscription;
  public transactionForm: FormGroup;
  public transactionItemsLength = 0;
  private standarValidators = {
    date: Validators.required,
    transactionType: noRequiredObjectValidator(true),
    title: Validators.required,
    department: noRequiredObjectValidator(),
    currency: noRequiredObjectValidator(true),
    totalLots: RangeValidator(0, 99000),
    notes: Validators.maxLength(1000),
  }
  private searchValidators = {
    transactionType: noRequiredObjectValidator(),
    department: noRequiredObjectValidator(),
    currency: noRequiredObjectValidator(),
  }
  public toFormatter: BehaviorSubject<Currency[]> = new BehaviorSubject([]);
  public listFormatter = autocompleListFormatter;


  @Input() set disableForm(disable) {
    this.disabled = disable;
  }
  @Input()
  hideElements: any[] = [];

  constructor(
    private fb: FormBuilder,
    private currencyService: CurrencyService,
    private departmentService: DepartmentService,
    private router: Router,
  ) { }

  ngOnInit() {

    this.transactionForm = this.fb.group({
      code: [''],
      date: [new Date((new Date()).setHours(0, 0, 0))],
      transactionType: [''],
      title: [''],
      department: [''],
      saleNumber: [''],
      currency: [''],
      totalLots: [''],
      notes: [''],
      eventId: ['']
    });
    if (this.disabled) {
      this.transactionForm.disable();
    }
  }

  searchMode() {
    setValidators(this.transactionForm, this.searchValidators);
    this.validate = false;
  }

  standarMode() {
    setValidators(this.transactionForm, this.standarValidators);
    this.validate = true;
  }
  createMode() {
    this.standarMode();
    this.emptyDate(true);
  }

  ngAfterViewInit() {
    this.currencySubscription = this.currencyService.elements
      .debounceTime(100)
      .subscribe({ next: (currencies) => this.currencies = currencies });

    this.departmentSubscription = this.departmentService.elements
      .debounceTime(100)
      .subscribe({ next: (departments) => this.departments = departments });
    this.formSubscription = this.transactionForm.valueChanges.debounceTime(100).subscribe(this.onValueChanged.bind(this));
    this.currencyFieldSubscription = this.transactionForm.get('currency').valueChanges.debounceTime(400)
      .subscribe(this.getDataToFilter.bind(this))
    this.departmentFieldSubscription = this.transactionForm.get('department').valueChanges.debounceTime(400)
      .subscribe(this.getDataToFilterDepartment.bind(this))
  }

  ngOnDestroy() {
    this.departmentSubscription.unsubscribe();
    this.departmentFieldSubscription.unsubscribe();
    this.currencySubscription.unsubscribe();
    this.currencyFieldSubscription.unsubscribe();
    this.formSubscription.unsubscribe();
  }

  onValueChanged(data?: any, second?) {
    this.formErrors = _.reduce(this.transactionForm.controls, (prev, value, index) => {
      if (value.invalid && index !== 'establishment') {
        const message = _.get(this.validationMessages, `${_.toUpper(_.snakeCase(index))}`);
        const pairs = _.toPairs(value.errors)
        prev[index] = _.get(message, pairs[0][0])
      }
      return prev
    }, {});
  }
  setDetailData(data) {
    if (data) {
      this.transactionDetail = data;
      const eventData = _.clone(data);
      const transactionType = _.filter(this.transactionTypes, ['id', eventData.transactionType]);
      const date = formatDate(eventData.date);
      const name = 'name';
      eventData.currency = convertToAutoformatCompatibleObject(eventData.currency, 'code');
      eventData.department = convertToAutoformatCompatibleObject(eventData.department, name)
      eventData.establishment = { establishment: convertToAutoformatCompatibleObject(eventData.establishment, name) };
      this.transactionForm.patchValue({
        date: date,
        transactionType: convertToAutoformatCompatibleObject(transactionType[0], name),
        ..._.pick(eventData, ['title', 'department', 'establishment', 'currency', 'totalLots', 'notes', 'eventId', 'code'])
      });
    }
  }

  reset() {
    this.transactionForm.reset();
    this.transactionDetail = null;
  }

  emptyDate(now = false) {
    const dateField = this.transactionForm.controls.date;
    const dateVal = now
      ? _.get(dateField, 'value._isValid', false)
        ? dateField.value
        : moment().format(GeneralConstants.DATE_FORMAT.formatDateUIDisplay)
      : null
    dateField.patchValue(dateVal)
  }

  eventScroll(event) {
    const totalElements = this.currencyService.page.value.totalElements;
    if (totalElements > this.currencies.length) {
      this.currencyService.getNextElements();
    }
  }

  observableSource = (): Observable<any[]> => {
    return this.currencyService.elements;
  }

  listFormatterResult(data) {
    return `${data.name} (${data.code})`;
  }

  getDataToFilter(data) {
    // Search data when the input have values for filter;
    const search = data && typeof data === 'string' ? { codeOrName: data } : null;
    this.currencyService.getElements(search);
  }

  getFormData(): any {
    const data = _.clone(this.transactionForm.value)
    data.establishment = _.get(data, 'establishment.establishment');
    data.date = formatDate(data.date, true);
    data.transactionType = _.get(data.transactionType, 'id');
    data.type = TYPE
    return data;
  }

  getSearchFormData(): any {
    const currentSearchData = this.getFormData();
    return {
      ..._.omit(currentSearchData, ['establishment', 'currency', 'department']),
      establishmentId: _.get(currentSearchData, 'establishment.id'),
      currencyId: _.get(currentSearchData, 'currency.id'),
      departmentId: _.get(currentSearchData, 'department.id'),
    };
  }

  observableDepartment = (): Observable<any[]> => {
    return this.departmentService.elements;
  }

  eventScrollDepartment(event) {
    const totalElements = this.departmentService.page.value.totalElements;
    if (totalElements > this.departments.length) {
      this.departmentService.getNextElements();
    }
  }

  getDataToFilterDepartment(data) {
    // Search data when the input have values for filter;
    const search = data && typeof data === 'string' ? { name: data } : null;
    this.departmentService.getElements(search);
  }

  goToEventPage() {
    this.router.navigate([`/transaction/${this.transactionDetail.id}`]);
    return false;
  }
}

