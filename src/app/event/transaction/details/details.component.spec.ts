import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { DetailsComponent } from './details.component';
// tslint:disable-next-line:max-line-length
import { FormsModule, ReactiveFormsModule, FormBuilder, FormControl, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Ng2CompleterModule } from 'ng2-completer';

describe('DetailsComponent', () => {
  let component: DetailsComponent;
  let fixture: ComponentFixture<DetailsComponent>;

  const fb: FormBuilder = new FormBuilder();

  function getParentForm() {
    return fb.group({
      date: [new Date(), Validators.required],
      transactionType: ['', Validators.required],
      title: ['', Validators.required],
      venue: ['', Validators.required],
      location: fb.group({
        countryName: [''],
        state: [''],
        cityName: [''],
        region: ['']
      }),
      department: ['', Validators.required],
      saleNumber: ['', Validators.required],
      currency: ['', Validators.required],
      totalLots: [''],
      notes: ['']
    });
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DetailsComponent],
      providers: [FormBuilder],
      imports: [FormsModule, ReactiveFormsModule, Ng2CompleterModule],
      schemas: [NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsComponent);
    component = fixture.componentInstance;
    // component.parent = getParentForm();
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('Form invalid when empty', () => {
    /*
    const detailForm = component.parent = getParentForm();
    expect(detailForm.valid).toBe(false);
    */
  });

  it('Set Details Form values', () => {
    /*
    const detailForm = component.parent = getParentForm();
    detailForm.controls['date'].setValue('08-08-2014 hh:mm:ss');
    detailForm.controls['transactionType'].setValue('Auction');
    detailForm.controls['title'].setValue('Something');
    detailForm.controls['venue'].setValue('Some Venue');
    detailForm.controls['department'].setValue('Some Department');
    detailForm.controls['saleNumber'].setValue('1234');
    detailForm.controls['currency'].setValue('USD');
    detailForm.controls['totalLots'].setValue('1234');
    detailForm.controls['notes'].setValue('Some Notes');
    */
  });

  it('Check it form Values changed', () => {
    // const detailForm = component.parent = getParentForm();
  });

  it('Set Venues location', () => {
    /*
    const detailForm = component.parent = getParentForm();
    const location = detailForm.controls['location'];
    location['controls']['countryName'].setValue('Some country');
    location['controls']['state'].setValue('Some state');
    location['controls']['cityName'].setValue('Some city');
    location['controls']['region'].setValue('Some region');
    */
  });
});
