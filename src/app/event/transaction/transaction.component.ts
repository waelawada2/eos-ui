import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { BlockScreenService } from './../shared/services/block-screen/block-screen.service';
import { BUTTON_STATUS } from './../shared/components/navigation-header/navigation-header.button-status.interface';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmComponent } from './../shared/components';
import { DialogService } from 'ng2-bootstrap-modal';
import { DetailsComponent } from './details/details.component';
import {
  NavigationService, TransactionService, TransactionItemService, AddObjectToEventService,
  ExternalClientService
} from './../shared/services';
import { NotificationsService } from 'angular2-notifications';
import { GeneralConstants, GeneralDictionary } from '../shared/constants';
import { Detail, Transaction, TransactionItem } from '../shared/models';
import { SothebysLifeCycle, STATES } from '../sothebys-lifecycle.abstract';
import { formatDate, appendClientDataToItems, getAllClientsIdsFromSubEvents } from '../shared/utils';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SORTING } from '../shared/components/navigation-header/navigation-header.sorting.interface';
import { TransactionSingleItemComponent } from './transaction-item/transaction-single-item/transaction-single-item.component';
import { TransactionItemFormComponent } from '../transaction/transaction-item/form/transaction-item-form.component';
import 'rxjs/add/operator/catch';
import * as _ from 'lodash';
const TYPE = GeneralConstants.EVENT_TYPES.TRANSACTION;

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: [
    '../shared/styles/panels.scss',
    './transaction.component.scss',
  ]
})
export class TransactionComponent extends SothebysLifeCycle implements OnDestroy, AfterViewInit {
  public currentId;
  public elementSubscription: Subscription;
  public enabledForm = true;
  public event = '';
  private eventId: string;
  public formLabels = GeneralDictionary.FORM_LABELS;
  public navSubscription;
  public subscription: any;
  public transactions;
  public transactionEvent: Transaction;
  public transactionItems: TransactionItem[];
  public transactionDetail: Detail;
  public transactionPageNumber = 0;
  public transactionPageTotal = 0;
  public options = GeneralConstants.NOTIFICATIONS.SETUP
  public showTable = false;
  public query: string;
  private currentSearch: any = {}
  private currentSearchData;
  public stateObservableSubscription: Subscription;
  private itemsSubscription: Subscription;
  protected getErrors: BehaviorSubject<any>;
  public hiddenFormFields = ['eventId'];
  private clientsMap = {};
  private currentNumber = -1;

  @ViewChild(DetailsComponent) private transactionFormComponent: DetailsComponent;

  constructor(
    protected blockScreenService: BlockScreenService,
    protected navService: NavigationService,
    protected route: ActivatedRoute,
    protected transactionService: TransactionService,
    protected transactionItemService: TransactionItemService,
    protected dialogService: DialogService,
    protected notificationSvc: NotificationsService,
    private clientService: ExternalClientService,
    private addObjectToEventService: AddObjectToEventService,
    protected router: Router,
  ) {
    super();
    this.baseUrl = 'transaction';
    this.section = TYPE;
    this.getErrors = transactionService.error;
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    if (this.itemsSubscription) {
      this.itemsSubscription.unsubscribe();
    }
    this.elementSubscription.unsubscribe();
    this.transactionItems = null;
  }

  ngAfterViewInit() {
    const form = this.transactionFormComponent.transactionForm;
    this.validData.next(false);
    form.valueChanges
      .distinctUntilChanged()
      .subscribe(() => this.validData.next(form.valid));
    this.stateObservable
      .debounceTime(100)
      .take(1)
      .subscribe({
        next: this.validateForm.bind(this)
      })

    super.initialize();

    this.itemsSubscription = this.transactionItemService.elements
      .skip(1)
      .distinctUntilChanged()
      .skipWhile(() => this.state === STATES.NEW || this.state === STATES.SEARCH)
      .debounceTime(100)
      .map((items) => {
        const clientIds = getAllClientsIdsFromSubEvents(items)
        this.clientService.getClients(clientIds)
        return items
      })
      .subscribe({
        next: items => {
          this.transactionItems = _.clone(items);
          const clientIds = getAllClientsIdsFromSubEvents(items);
          this.clientService.getClients(clientIds)
            .subscribe(client => this.clientsMap[client.entityId] = client,
              null,
              () => {
                this.transactionItems = _.clone(appendClientDataToItems(items, this.clientsMap))
                this.checkClientLoadErrors(clientIds)
              });

          this.ableToRemove.next(items.length <= 0)
        }
      });

    this.elementSubscription = this.transactionService.elements
      .distinctUntilChanged()
      .skipWhile(() => this.state !== STATES.EDIT)
      .subscribe({
        next: (event) => {
          this.updateTransaction(event);
          if (event.length > 0 && this.currentId && event[0].id === this.currentId && this.addObjectToEventService.creating) {
            this.doItemCreation(event[0]);
          }
        }
      });
  }

  validateForm(state) {
    switch (state) {
      case STATES.SEARCH:
        this.transactionFormComponent.searchMode();
        break;
      default:
        this.transactionFormComponent.standarMode();
        break;
    }
  }

  private checkClientLoadErrors(clientIds) {
    clientIds.forEach(id => {
      if (!this.clientsMap[id]) {
        this.notificationSvc.error(GeneralDictionary.NOTIFICATIONS.error,
          GeneralDictionary.NOTIFICATIONS.ERR_NOT_FOUND_CLIENT(id),
          GeneralConstants.NOTIFICATIONS.OPTIONS
        )
      }
    })
  }

  protected clearForm(restore = false) {
    this.transactionDetail = null;
    this.transactionItems = null;
    this.transactions = null;
    this.updatePages(0, 0);
    this.transactionFormComponent.reset();
    this.transactionFormComponent.emptyDate();
    this.showTable = false;
    this.loadPaginated = 0;
    if (restore) {
      this.transactionFormComponent.setDetailData(this.currentSearchData);
    }
    this.currentSearch = {};
    this.validateForm(this.state);
  }

  protected doSearchAction(id): BehaviorSubject<any> {
    this.currentId = id;
    this.currentSearch = { ... this.transactionFormComponent.getSearchFormData(), id, size: this.pageSize };
    this.transactionService.getElements({ ...this.currentSearch, sort: this.getSortParameters() }, id);
    return this.transactionService.elements;
  };

  protected doCreateAction(): Observable<any> {
    return this.transactionService.create(this.transactionFormComponent.getFormData());
  }

  protected doEditAction(): Observable<any> {
    return this.transactionService.amend(this.transactionFormComponent.getFormData(), this.transactionDetail.id);
  }

  protected doDeleteAction(): Observable<any> {
    return this.transactionService.delete(this.transactionDetail.id);
  }

  protected doImportAction(file: File): Observable<any> {
    return this.transactionService.import(file, GeneralConstants.IMPORT_TYPES.transaction);
  }

  protected doExportAction(): Observable<any> {
    return this.transactionService.export(this.currentSearch);
  }

  public create() {
    super.create();
    this.validateForm(this.state);
    this.transactionFormComponent.emptyDate(true);
  }

  updateTransaction(data) {
    this.transactions = data;
    if (this.loadPaginated !== 0) {
      this.changePage(this.loadPaginated)
      this.loadPaginated = 0;
    } else {
      this.displayItemNumber(0);
    }
  }

  displayItemNumber(number) {
    if (number !== this.currentNumber) {
      this.currentNumber = number;
      return this.getTransactionData(number);
    }
  }

  getTransactionData(index = 0) {
    this.transactionItems = [];
    this.showTable = false;
    if (!this.transactions) {
      this.updatePages(0, 0);
      return;
    }
    this.showTable = true;
    if (index >= 0 && this.transactions.length > index) {
      this.transactionDetail = this.transactions[index];
      if (this.transactionDetail) {
        this.transactionFormComponent.setDetailData(this.transactionDetail);
        this.validateForm(this.state);
        this.getTransactionItems();
      }
      this.updatePages(index, this.transactionService.page.value.totalElements);
    } else {
      this.transactionService.getNextElements(index);
      this.loadPaginated = index;
    }
    return _.get(this.transactionDetail, 'id');
  }

  getTransactionItems() {
    this.transactionItemService.getItemsByUrl(_.get(this.transactionDetail, '_links.items.href'), false, { size: this.pageSize });
  }

  loadMoreItems() {
    const totalElements = this.transactionItemService.page.value.totalElements;
    if (totalElements > this.transactionItems.length) {
      this.transactionItemService.getNextElements()
    }
  }

  protected reorderResults(): BehaviorSubject<any> {
    this.transactionService.getElements({ ...this.currentSearch, sort: this.getSortParameters(this.sortOrder) });
    return this.transactionService.elements;
  }

  private getSortParameters(order: SORTING = SORTING.DESCENDING) {
    return order === SORTING.DESCENDING ? GeneralConstants.SORTING.TRANSACTION.DESC : GeneralConstants.SORTING.TRANSACTION.ASC
  }

  private removeItem(item) {
    if (item.consignmentEvent || item.purchaseEvent) {
      this.showMessage(GeneralDictionary.NOTIFICATIONS.error,
        GeneralDictionary.NOTIFICATIONS.ERR_CANT_DELETE_RELATED_ITEM,
        _.lowerCase(GeneralDictionary.NOTIFICATIONS.error))
      return;
    }
    const disposable = this.dialogService.addDialog(ConfirmComponent, {
      title: GeneralDictionary.NOTIFICATIONS.DELETE_CONFIRM_TITLE,
      message: GeneralDictionary.NOTIFICATIONS.DELETE_ITEM_FROM_EVENT(_.upperFirst(GeneralDictionary.SECTIONS.TRANSACTION))
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        this.processItemRemoval(item)
      }
      disposable.unsubscribe()
    });
  }

  private processItemRemoval(item) {
    this.transactionItemService.delete(item.id).subscribe(
      (result) => this.showMessage(GeneralDictionary.NOTIFICATIONS.success,
        GeneralDictionary.NOTIFICATIONS
          .deleted(`${_.upperFirst(GeneralDictionary.SECTIONS.TRANSACTION)} ${GeneralConstants.ENTITY_TYPE.ITEM}`),
        _.lowerCase(GeneralDictionary.NOTIFICATIONS.success)),
      this.handleErrorAction.bind(this))
  }

  private loadItemPopup(item, isClosable: boolean) {
    this.dialogService.addDialog(TransactionSingleItemComponent, {
      item,
      title: `${_.upperFirst(_.toLower(GeneralDictionary.SECTIONS.TRANSACTION))} ${GeneralConstants.ENTITY_TYPE.ITEM}`,
      isClosable,
    }).take(1).subscribe(
      response => {
        if (response) {
          if (response.redirect) {
            this.router.navigate([response.url])
            return
          }
          this.transactionItemService.amend({ ...response, id: null }, response.id)
            .subscribe(
              res => {
                this.addObjectToEventService.reset();
                this.showMessage(GeneralDictionary.NOTIFICATIONS.success,
                  GeneralDictionary.NOTIFICATIONS.saved(_.upperFirst(GeneralConstants.ENTITY_TYPE.ITEM)),
                  _.toLower(GeneralDictionary.NOTIFICATIONS.success))
              },
              this.handleErrorAction.bind(this)
            )
        }
      },
      this.handleErrorAction.bind(this)
    )
  }

  private doItemCreation(event) {
    if (event && this.addObjectToEventService.objectId && this.addObjectToEventService.item) {
      this.loadItemPopup(this.addObjectToEventService.item, false)
    } else {
      this.addObjectToEventService.event = event;
      this.transactionItemService.create(this.dataToSet(event))
        .take(1).subscribe((result) => {
          const response = JSON.parse(_.get(result, '_body'));
          if (_.has(response, 'error')) {
            return this.handleErrorAction(result);
          }
          this.addObjectToEventService.item = response;
          this.loadItemPopup(this.addObjectToEventService.item, false)
          this.getTransactionItems()
        })
    }
  }

  dataToSet(event) {
    const resultData = {
      type: _.toUpper(GeneralDictionary.SECTIONS.TRANSACTION),
      event: { id: event.id, type: _.toUpper(GeneralDictionary.SECTIONS.TRANSACTION) },
      objects: [{ objectId: this.addObjectToEventService.objectId }],
      transactionType: _.get(event, 'transactionType'),
      auction: {},
      privateSale: {}
    }
    _.get(event, 'transactionType') === GeneralConstants.ITEM_TYPES.AUCTION
      ? resultData.auction = { estimateOnRequest: false }
      : resultData.privateSale = { netBuyer: null, netVendor: null };

    return resultData;
  }

}
