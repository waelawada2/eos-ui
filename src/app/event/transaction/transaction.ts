import { DetailsComponent } from './details/details.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { TableComponent } from './table/table.component';
import { TransactionComponent } from './transaction.component';
import { TransactionItemComponent } from './transaction-item/transaction-item.component';
import { TransactionItemFormComponent } from './transaction-item/form/transaction-item-form.component';
import { TransactionAddObjectComponent } from './add-object/transaction.add-object.component';
import { TransactionSingleItemComponent } from './transaction-item/transaction-single-item/transaction-single-item.component';


const TRANSACTION_COMPONENTS = [
  DetailsComponent,
  StatisticsComponent,
  TableComponent,
  TransactionAddObjectComponent,
  TransactionComponent,
  TransactionItemComponent,
  TransactionItemFormComponent,
  TransactionSingleItemComponent
];

export {
  DetailsComponent,
  StatisticsComponent,
  TableComponent,
  TransactionAddObjectComponent,
  TransactionComponent,
  TransactionItemComponent,
  TransactionItemFormComponent,
  TransactionSingleItemComponent,
  TRANSACTION_COMPONENTS,
}
