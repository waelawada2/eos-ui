import { Component, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { DialogService } from 'ng2-bootstrap-modal';
import { GeneralDictionary, GeneralConstants } from '../../shared/constants';
import { AddObjectToEventService, TransactionItemService, TransactionService } from '../../shared/services';
import { DetailsComponent } from './../details/details.component'
import { FormGroup } from '@angular/forms';
import { ConfirmComponent } from '../../shared/components';
import { getErrorStringFromServer } from '../../shared/utils';
import { Subscription } from 'rxjs/Subscription';
import * as _ from 'lodash';

@Component({
  providers: [TransactionService],
  templateUrl: './transaction.add-object.component.html',
  styleUrls: ['../../shared/styles/tables.scss', '../../shared/styles/forms.scss',
    './transaction.add-object.component.scss',
  ],
})
export class TransactionAddObjectComponent implements AfterViewInit, OnDestroy {
  dictionary = GeneralDictionary;
  transactionForm: FormGroup;
  private transactionSubscription: Subscription;
  transactionItemSuscribe: Subscription;
  private events: any[];
  private options = GeneralConstants.NOTIFICATIONS.OPTIONS;
  private objectId;
  private title = `${_.upperFirst(GeneralDictionary.SECTIONS.TRANSACTION)} ${_.upperFirst(GeneralConstants.ENTITY_TYPE.EVENT)}`;
  private emptyMessage = GeneralDictionary.RESULT_MESSAGES
    .NO_RECORDS_FOUND(`${GeneralDictionary.SECTIONS.TRANSACTION} ${GeneralConstants.ENTITY_TYPE.EVENT}s`)

  @ViewChild(DetailsComponent) transactionFormComponent: DetailsComponent;

  constructor(
    private transactionService: TransactionService,
    private transactionItemService: TransactionItemService,
    private addObjectToEventService: AddObjectToEventService,
    protected notificationSvc: NotificationsService,
    protected dialogService: DialogService,
    private router: Router
  ) {
  }

  ngAfterViewInit() {
    this.transactionForm = this.transactionFormComponent.transactionForm;
    this.transactionSubscription = this.transactionService.elements.skip(1).subscribe(this.processTransaction.bind(this));
    this.objectId = this.addObjectToEventService.objectId;
    this.transactionFormComponent.reset();
    if (!this.objectId) {
      this.close();
    }
  }

  ngOnDestroy() {
    this.transactionSubscription.unsubscribe();
    if (this.transactionItemSuscribe) {
      this.transactionItemSuscribe.unsubscribe();
    }
  }

  processTransaction(events) {
    this.events = events;
  }

  close() {
    this.router.navigate(['object'], { queryParams: { id: this.objectId } });
  }

  filter() {
    const data = this.transactionFormComponent.getSearchFormData();
    this.transactionService.getElements({ ...data, sort: GeneralConstants.SORTING.TRANSACTION.DESC }, _.get(data, 'eventId'))
  }

  addToObject(event) {
    this.dialogService.addDialog(ConfirmComponent, {
      title: GeneralDictionary.NOTIFICATIONS.ADD_OBJECT_TO_EVENT_TITLE,
      message: GeneralDictionary.NOTIFICATIONS.ADD_OBJECT_TO_EVENT(_.upperFirst(_.toLower(GeneralDictionary.SECTIONS.TRANSACTION)))
    }).take(1).subscribe((isConfirmed) => {
      if (isConfirmed) {
        this.doCreation(event);
      }
    });
  }

  doCreation(event) {
    this.transactionItemSuscribe = this.transactionItemService.create(this.dataToSet(event))
      .subscribe((result) => {
        const response = JSON.parse(_.get(result, '_body'));
        if (_.has(response, 'error')) {
          return this.handleErrorAction(result);
        }
        this.addObjectToEventService.event = event;
        this.addObjectToEventService.item = response;
        const url = `${_.lowerCase(GeneralDictionary.SECTIONS.TRANSACTION)}/${event.id}`;
        this.addObjectToEventService.creating = true;
        this.router.navigate([url]);
      })
  }

  handleErrorAction(response) {
    this.notificationSvc.error(GeneralDictionary.NOTIFICATIONS.error,
      getErrorStringFromServer(response), GeneralConstants.NOTIFICATIONS.OPTIONS);
  }


  dataToSet(event) {
    const resultData = {
      type: _.toUpper(GeneralDictionary.SECTIONS.TRANSACTION),
      event: _.pick(event, ['id', 'type']),
      objects: [{ objectId: this.objectId }],
      transactionType: _.get(event, 'transactionType'),
      auction: {},
      privateSale: {}
    }
    _.get(event, 'transactionType') === GeneralConstants.ITEM_TYPES.AUCTION
      ? resultData.auction = { estimateOnRequest: false }
      : resultData.privateSale = { netBuyer: null, netVendor: null };

    return resultData;
  }

  addEvent() {
    const url = `${_.lowerCase(GeneralDictionary.SECTIONS.TRANSACTION)}/new`;
    this.addObjectToEventService.creating = true;
    this.router.navigate([url]);
  }

  tableScrolled() {
    const totalElements = this.transactionService.page.value.totalElements;
    if (totalElements > this.events.length) {
      this.transactionService.getNextElements()
    }
  }

}
