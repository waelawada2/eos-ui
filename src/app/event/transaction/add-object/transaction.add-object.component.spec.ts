import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionAddObjectComponent } from './transaction.add-object.component';

describe('AddObjectComponent', () => {
  let component: TransactionAddObjectComponent;
  let fixture: ComponentFixture<TransactionAddObjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TransactionAddObjectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionAddObjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
