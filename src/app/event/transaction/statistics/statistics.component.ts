import { Component, Input } from '@angular/core';
import { Statistics } from '../../shared/models';
import { StatisticsCons } from '../../shared/constants';
import { GeneralConstants } from '../../shared/constants';



@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent {
  public defaultStatistics = new Statistics();
  private disabled: Boolean = true;
  private transactionType: String;
  public statistics;
  private labels;
  private currencyCode;
  private currencySymbol;
  switchLabels(label = StatisticsCons.AUCTION) {
    switch (label) {
      case StatisticsCons.PRIVATE_SALE:
        this.labels.totalLow = StatisticsCons.privateSaleLabels.totalLow;
        this.labels.totalHigh = StatisticsCons.privateSaleLabels.totalHigh;
        break;
      case StatisticsCons.AUCTION:
        this.labels.totalLow = StatisticsCons.defaults.totalLow;
        this.labels.totalHigh = StatisticsCons.defaults.totalhigh;
        break;
    };
  }
  @Input() set transactionDetail(transactionDetail) {
    if (transactionDetail) {
      this.statistics = transactionDetail.statistics;
      this.switchLabels(transactionDetail.transactionType);
      this.currencyCode = transactionDetail.currency.code;
      this.currencySymbol = transactionDetail.currency.symbol;

    } else {
      this.statistics = this.defaultStatistics;
      this.currencyCode = GeneralConstants.CURRENCY_DEFAULTS.currencyCode;
      this.currencySymbol = GeneralConstants.CURRENCY_DEFAULTS.currencySymbol;
      this.switchLabels();
    }
  };
  constructor() {
    this.labels = StatisticsCons.labels;
  }
}
