export class Statistics {
    confirmedLotsOfMoreThan10Mil:number = 0;
    targetedLotsOfMoreThan10Mil: number = 0;
    confirmedLotsBetween1And10Mil: number = 0;
    targetedLotsBetween1And10Mil: number = 0;
    confirmedLotsLessThan1Mil: number = 0;
    targetedLotsLessThan1Mil: number = 0;
    confirmedLotsTotal: number = 0;
    targetedLotsTotal: number = 0;
    confirmedLotsTotalLow: number = 0;
    targetedLotsTotalLow: number = 0;
    confirmedLotsTotalHigh: number = 0;
    targetedLotsTotalHigh:number = 0;
}
