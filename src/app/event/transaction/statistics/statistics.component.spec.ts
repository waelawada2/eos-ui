import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { StatisticsComponent } from './statistics.component';
import { EVENTTRANSACTION } from '../../mocks/transaction-event.mock';
import { Statistics } from './statistics';
import { StatisticsCons } from '../../shared/constants';

describe('StatisticsComponent', () => {
  let component: StatisticsComponent;
  let fixture: ComponentFixture<StatisticsComponent>;
  const statisticData: any = EVENTTRANSACTION._embedded.events[0];
  const statisticDataPrivateSale: any = EVENTTRANSACTION._embedded.events[1];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StatisticsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticsComponent);
    component = fixture.componentInstance;
    component.transactionDetail = undefined;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should have a default value', () => {
    fixture.detectChanges();
    expect(component.statistics).toEqual(new Statistics());
  });

  it('should set value when data is ready', () => {
    component.transactionDetail = statisticData;
    fixture.detectChanges();
    expect(component.statistics).toEqual(statisticData.statistics);
  });

    it(' function switchlabels must be called', function () {
      spyOn(component, "switchLabels");
      component.transactionDetail = statisticData;
      fixture.detectChanges();
      expect(component.switchLabels).toHaveBeenCalled();
    })

    it(' function switchlabels must be called with params PRIVATE_SALE', fakeAsync(function () {
      spyOn(component, "switchLabels");
      component.transactionDetail = statisticDataPrivateSale;
      fixture.detectChanges();
      tick();
      expect(component.switchLabels).toHaveBeenCalledWith(StatisticsCons.PRIVATE_SALE);
    }))
});
