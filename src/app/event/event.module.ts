import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2CompleterModule } from 'ng2-completer';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { HttpModule } from '@angular/http';
import { EventRoutingModule } from './event-routing.module';
import { PUBLICATION_COMPONENTS } from './publication/publication.facade';
import { PublicationSingleItemComponent } from './publication/items/publication-single-item/publication-single-item.component';
import { PricingSingleItemComponent } from './pricing/pricing-item/pricing-single-item/pricing-single-item.component';
import { TRANSACTION_COMPONENTS } from './transaction/transaction';
import { CONSIGNMENT_PURCHASE_COMPONENTS } from './consignment-purchase/consignment-purchase.facade';
import { PRICING_COMPONENTS } from './pricing/pricing.facade';
import { CLIENT_COMPONENTS } from './client/client.facade';
import { AlertComponent, ClientDialogComponent, ConfirmComponent, SHARED_COMPONENTS, UploadDialogComponent } from './shared/components';
import { SHARED_PIPES } from './shared/pipes';
import { SHARED_SERVICES } from './shared/services';
import { GLOBAL_SHARED_SERVICES } from '../shared/services';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { ContextMenuDirective, ContextMenuService } from './shared/components/context-menu/context-menu.facade';
import { SelectModule } from 'ng2-select-compat';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { TransactionSingleItemComponent } from './transaction/transaction-item/transaction-single-item/transaction-single-item.component';
import { SHARED_DIRECTIVES } from './shared/directives';

@NgModule({
  imports: [
    BootstrapModalModule,
    BrowserModule,
    BsDatepickerModule.forRoot(),
    CommonModule,
    EventRoutingModule,
    FormsModule,
    HttpModule,
    InfiniteScrollModule,
    Ng2CompleterModule,
    NguiAutoCompleteModule,
    ReactiveFormsModule,
    SelectModule,
    SimpleNotificationsModule.forRoot(),
  ],
  declarations: [
    CLIENT_COMPONENTS,
    CONSIGNMENT_PURCHASE_COMPONENTS,
    ContextMenuDirective,
    PRICING_COMPONENTS,
    PUBLICATION_COMPONENTS,
    SHARED_COMPONENTS,
    SHARED_DIRECTIVES,
    SHARED_PIPES,
    TRANSACTION_COMPONENTS,
  ],
  entryComponents: [
    AlertComponent,
    ClientDialogComponent,
    ConfirmComponent,
    PricingSingleItemComponent,
    PublicationSingleItemComponent,
    TransactionSingleItemComponent,
    UploadDialogComponent,
  ],
  providers: [
    ContextMenuService,
    DecimalPipe,
    GLOBAL_SHARED_SERVICES,
    SHARED_SERVICES,
  ],
  exports: [
    SHARED_COMPONENTS
  ],
})
export class EventModule { }
