/*tslint:disable*/
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TransactionComponent, TransactionItemComponent, TransactionAddObjectComponent } from './transaction/transaction';
import { ConsignmentPurchaseComponent } from './consignment-purchase/consignment-purchase.component';
import { PublicationComponent, PublicationAddObjectComponent, PublicationItemComponent } from './publication/publication.facade';
import { PricingComponent, PricingItemComponent } from './pricing/pricing.facade';
import { ConsignmentPurchaseAddItemComponent } from './consignment-purchase/consignment-purchase-add-item/consignment-purchase-add-item.facade';
import { PricingAddObjectComponent } from '../event/pricing/add-object/pricing.add-object.component';

import { ClientComponent } from './client/client.facade';

// Router
const routes: Routes = [
  { path: 'client', redirectTo:'client/search'},
  { path: 'client/:id', component: ClientComponent},
  { path: 'transaction', redirectTo: 'transaction/search' },
  { path: 'transaction/:id', component: TransactionComponent },
  { path: 'transaction/:transactionId/items', redirectTo: 'transaction/:transactionId/items/search' },
  { path: 'transaction/:transactionId/items/:id', component: TransactionItemComponent },
  { path: 'transaction-add-object', component: TransactionAddObjectComponent },
  { path: 'consignment-purchase', redirectTo: 'consignent-purchase/search' },
  { path: 'consignment-purchase/:id', component: ConsignmentPurchaseComponent },
  { path: 'consignment-purchase-add-item/:eventId/:itemId/:subtype', component: ConsignmentPurchaseAddItemComponent },
  { path: 'publication', redirectTo: 'publication/search' },
  { path: 'publication/:id', component: PublicationComponent },
  { path: 'publication/:publId/items', redirectTo: 'publication/:publId/items/search' },
  { path: 'publication/:publId/items/:id', component: PublicationItemComponent },
  { path: 'publication-add-object', component: PublicationAddObjectComponent },
  { path: 'pricing', redirectTo: 'pricing/search' },
  { path: 'pricing/:id', component: PricingComponent },
  { path: 'pricing/:pricId/items', redirectTo: 'pricing/:pricId/items/search' },
  { path: 'pricing/:pricId/items/:id', component: PricingItemComponent },
  { path: 'pricing-add-object', component: PricingAddObjectComponent },
]

// angular
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class EventRoutingModule { }
