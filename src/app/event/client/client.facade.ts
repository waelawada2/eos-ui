import { ClientComponent } from './client.component';
import { ClientCurrentComponent } from './collections/client-current.component'

const CLIENT_COMPONENTS = [
  ClientComponent,
  ClientCurrentComponent,
];

export {
  ClientComponent,
  ClientCurrentComponent,
  CLIENT_COMPONENTS
}
