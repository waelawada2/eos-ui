import { AfterViewInit, OnDestroy } from '@angular/core';
import { Property, getHint } from '../../shared/models/property.interface';
import { Subscription } from 'rxjs/Subscription';
import { GeneralDictionary } from '../../shared/constants';
import AbstractService, { Paging } from '../../shared/services/abstract.service'
import * as _ from 'lodash';

export default abstract class ClientCollectionAbstract implements AfterViewInit, OnDestroy {

  protected THUMBS_SIZE = 18;
  protected MAX_BEFORE_SHOW_ALL = 3;
  protected objectServiceSubscription: Subscription;
  protected pageSubscription: Subscription;

  protected theArtist: any;
  protected theClient: any;
  protected title: string;
  protected abstract dynamicTitle;
  protected currents = [];
  protected objects = [];
  protected hasPrevs = true;
  protected hasNexts = true;
  protected page = 0;
  protected loading = true;
  protected showAllDisplayed = true;
  protected pagingInfo: Paging = {
    number: 0,
    size: 0,
    totalElements: 0,
    totalPages: 0,
  };

  protected objectService: AbstractService<Property>;

  ngAfterViewInit() {
    this.objectServiceSubscription = this.objectService.elements.skip(1).subscribe(this.processChangedElements.bind(this))
    this.pageSubscription = this.objectService.page.skip(1).subscribe(this.processPageChange.bind(this))
  }

  ngOnDestroy() {
    this.objectServiceSubscription.unsubscribe();
    this.pageSubscription.unsubscribe();
  }

  loadData() {
    if (this.theClient) {
      this.page = 0;
      const currentsQuery = {
        currentOwnerIds: _.get(this.theClient, 'id', null),
        artistIds: _.get(this.theArtist, 'id', null),
        size: this.THUMBS_SIZE,
        page: this.page
      };
      this.loading = true;
      this.objectService.getElements(currentsQuery)
      this.currents = []
      this.objects = [];
    }
  }

  processChangedElements(items) {
    if (items && items.toString() !== '') {
      this.currents = items.map(prop => ({
        ..._.pick(prop, ['id', 'imageUrl']),
        title: `${prop.title || ''} (${prop.originalTitle || ''})`,
        hint: getHint(prop),
        route: '../../object',
      }))
    }

    this.updateDisplayedObjects();
  }

  updateDisplayedObjects() {
    this.loading = false;
    this.objects = this.currents.slice(this.page * this.THUMBS_SIZE, (this.page + 1) * this.THUMBS_SIZE)
    this.hasPrevs = this.page > 0
    this.showAllDisplayed = (this.currents.length > 0)
  }

  navCurrents(dir) {
    if (dir > 0) {
      if (this.pagingInfo.number < this.pagingInfo.totalPages) {
        this.page++;
        if (this.MAX_BEFORE_SHOW_ALL <= this.page) {
          this.showAll();
          return;
        }
        if (this.page * this.THUMBS_SIZE >= this.currents.length) {
          this.loading = true;
          this.objects = [];
          this.objectService.getNextElements()
        } else {
          this.updateDisplayedObjects();
        }
      }
    } else {
      if (this.page > 0) {
        this.page--;
        this.updateDisplayedObjects();
      }
    }
  }

  processPageChange(page) {
    this.pagingInfo = page;
    this.updateTitle(page.totalElements)
    this.hasNexts = this.page < page.totalPages - 1;
  }

  protected abstract showAll();

  protected abstract updateTitle(page);

}

