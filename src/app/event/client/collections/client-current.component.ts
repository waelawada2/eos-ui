
import { AfterViewInit, Component, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ExternalClient } from '../../shared/models';
import { ObjectService } from '../../../shared/services';
import { GeneralDictionary } from '../../shared/constants';
import ClientAbstractCollection from './client-collection.abstract'
import * as _ from 'lodash';

@Component({
  selector: 'app-client-current-collection',
  providers: [ObjectService],
  templateUrl: './client-current.component.html',
})
export class ClientCurrentComponent extends ClientAbstractCollection implements AfterViewInit, OnDestroy {

  @Input() set client(client: any) {
    this.theClient = client;
    this.loadData()
  }
  @Input() set artist(artist: any) {
    this.theArtist = artist;
    this.loadData();
  }

  protected dynamicTitle = GeneralDictionary.DYNAMIC_TITLE;

  constructor(
    protected objectService: ObjectService,
    private router: Router,
  ) {
    super()
    this.title = GeneralDictionary.DYNAMIC_TITLE.currCollection.prefix;
  }

  ngAfterViewInit() {
    super.ngAfterViewInit()
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  showAll() {
    this.router.navigate([`/thumbnail`], { queryParams: { clientId: this.theClient.id } });
  }

  updateTitle(total) {
    const { prefix, empty, elementNumber } = GeneralDictionary.DYNAMIC_TITLE.currCollection;
    if (total > 0) {
      this.title = `${prefix} ${elementNumber(total)}`;
    } else {
      this.title = `${prefix} ${empty}`;
    }
  }
}
