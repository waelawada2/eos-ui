import { Component, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { Router, RouterLink, ActivatedRoute } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { DialogService } from 'ng2-bootstrap-modal';
import { GeneralConstants, GeneralDictionary, FormsDictionary } from '../shared/constants';
import { NavigationService, ItemsService, ClientStatusService, ExternalClientService } from '../shared/services';
import { ObjectService } from '../../shared/services';
import { ConfirmComponent, ClientFormComponent, ArtistFormComponent } from '../shared/components';
import { ExternalClientFormComponent } from '../shared/components';
import { ExternalClient } from '../shared/models';
import { Property } from '../shared/models/property.interface';
import { CLIENTSOURCETYPE, CLIENTSTATUS, CLIENTSOURCE, AFFILIATEDTYPE } from '../shared/mocks';
import { SothebysLifeCycle, STATES } from '../sothebys-lifecycle.abstract';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SORTING } from '../shared/components/navigation-header/navigation-header.sorting.interface'
import { Thumbnail } from '../shared/components/two-lane-thumbs/thumbnail.interface';
import { randomlyAddFields } from '../shared/mocks';

import * as _ from 'lodash';
import * as moment from 'moment/moment';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: [
    '../shared/styles/panels.scss',
    './client.component.scss',
  ]
})
export class ClientComponent extends SothebysLifeCycle implements AfterViewInit, OnDestroy {
  client: ExternalClient;
  private currentSearch: any = {};

  private pasts: Thumbnail[] = [];
  private recents: Thumbnail[] = [];

  clientSubscription: Subscription;
  options = GeneralConstants.NOTIFICATIONS.SETUP;
  currentsQuery: any = {};
  currentsSubscription: Subscription;
  eventServiceSubscription: Subscription;
  formSubscription: Subscription;
  stateSubscription: Subscription;
  currentSearchData;
  resultEvents: any = {};
  valueItems = [];
  clients: ExternalClient[];
  current: ExternalClient;
  showTwoLaneThumbs = false;
  pageChangeSubscription: Subscription;
  titleRecentEvents = GeneralDictionary.SECTIONS.RECENT_EVENTS;

  private selectedArtist = null;

  protected getErrors: BehaviorSubject<any>;
  private dictionary = GeneralDictionary;
  private dynamicTitle = GeneralDictionary.DYNAMIC_TITLE;

  @ViewChild(ExternalClientFormComponent) externalClientForm: ExternalClientFormComponent;
  @ViewChild(ArtistFormComponent) artistForm: ArtistFormComponent;

  constructor(
    private clientService: ExternalClientService,
    private clientStatus: ClientStatusService,
    private is: ItemsService,
    protected notificationSvc: NotificationsService,
    protected navService: NavigationService,
    protected route: ActivatedRoute,
    protected dialogService: DialogService,
    protected router: Router
  ) {
    super()
    this.baseUrl = 'client';
    this.sectionType = GeneralConstants.ENTITY_TYPE.CLIENT;
    this.getErrors = new BehaviorSubject(null);
    this.sortOrder = SORTING.ASCENDING;
  }

  ngAfterViewInit() {
    const form = this.externalClientForm.form;
    this.stateSubscription = this.stateObservable
      .filter(state => state === STATES.NEW)
      .take(1)
      .subscribe({
        next: state => {
          this.externalClientForm.standarMode();
        }
      })
    this.formSubscription = form.valueChanges
      .debounceTime(400)
      .subscribe((a) => this.validData.next(form.valid))
    super.initialize()

    this.clientSubscription = this.clientService.elements
      .distinctUntilChanged()
      .skipWhile(() => this.state !== STATES.EDIT)
      .subscribe({ next: this.updateClient.bind(this) })

    this.pageChangeSubscription = this.navService.getPageChangeEmitter()
      .subscribe((value) => {
        this.artistForm.clearForm();
      })
  }

  public ngOnDestroy() {
    super.ngOnDestroy()
    this.formSubscription.unsubscribe()
    this.stateObservable.unsubscribe()
    this.clientSubscription.unsubscribe()
    this.pageChangeSubscription.unsubscribe();
  }

  protected clearForm(restore) {
    this.updatePages(0, 0)
    this.externalClientForm.reset()
    this.current = undefined;
    this.loadPaginated = 0;
    if (restore) {
      this.externalClientForm.restoreData(this.currentSearchData);
    }
    this.currentSearch = {}
    this.currentSearchData = {}
    this.validateForm()
    this.showTwoLaneThumbs = false;
  }

  public finishCreateState(client) {
    const transformedClient = {
      id: _.get(client, 'entityId'),
      ...client,
    }
    super.finishCreateState(transformedClient);
  }

  protected doSearchAction(id): BehaviorSubject<any> {
    const data = this.externalClientForm.form.getRawValue();
    this.currentSearch = { ... this.externalClientForm.getFormSearchData(), size: this.pageSize };
    this.currentSearchData = data;
    this.clientService.getElements({ ...this.currentSearch, id, sort: this.getSortParameters(this.sortOrder) }, id);
    const elements = this.clientService.elements;
    if (elements) {
      this.showTwoLaneThumbs = true;
    }
    return elements;
  }

  protected doCreateAction(): Observable<any> {
    return this.clientService.create(this.externalClientForm.getFormData());
  }

  protected doEditAction(): Observable<any> {
    const data = this.externalClientForm.getFormData();
    return this.clientService.amend(data, _.get(this.externalClientForm.client, 'id'))
  }

  protected doDeleteAction(): Observable<any> {
    return null
  }

  protected doImportAction(file: File): Observable<any> {
    return null
  }

  protected doExportAction(): Observable<any> {
    return null
  }

  protected displayItemNumber(number: number) {
    return this.getClientData(number);
  }


  protected reorderResults(): BehaviorSubject<any> {
    this.clientService.getElements({ ...this.currentSearch, sort: this.getSortParameters(this.sortOrder) });
    return this.clientService.elements;
  }

  create() {
    super.create();
    this.validateForm();
  }

  private getSortParameters(order: SORTING = SORTING.DESCENDING) {
    return order === SORTING.DESCENDING ? GeneralConstants.SORTING.CLIENT.DESC : GeneralConstants.SORTING.CLIENT.ASC;
  }

  private validateForm() {
    switch (this.state) {
      case STATES.SEARCH:
        this.externalClientForm.searchMode();
        break;
      default:
        this.externalClientForm.standarMode();
        break
    }
  }

  public navPasts(dir) {
    // TODO Refactor this to its own component
  }

  public navRecents(dir) {
    // TODO Refactor this to its own component
  }

  private updateClient(clients: ExternalClient[]) {
    this.clients = clients;
    if (this.loadPaginated !== 0) {
      this.changePage(this.loadPaginated)
      this.loadPaginated = 0
    }
  }

  private getClientData(number: number = 0) {
    if (!this.clients) {
      this.updatePages(0, 0);
      return;
    }
    if (number >= 0 && this.clients.length > number) {

      this.current = this.clients[number];
      if (this.current) {
        this.validateForm()
        this.externalClientForm.updateDataForm(this.current);
      }
    } else {
      this.loadPaginated = number;
      this.clientService.getNextElements(number);
    }
    this.updatePages(number, this.clientService.page.value.totalElements)
    return _.get(this.current, 'id');
  }

  private addFilterByArtist(artist) {
    this.selectedArtist = artist;
  }

}
