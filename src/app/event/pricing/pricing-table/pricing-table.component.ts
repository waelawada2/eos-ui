import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EVENTCONST, TableDictionary, GeneralDictionary, GeneralConstants } from '../../shared/constants';
import { TableObjectComponent } from '../../shared/components';
import { ContextMenuOptions } from '../../shared/components/context-menu/context-menu-options.interface'
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-pricing-table',
  templateUrl: './pricing-table.component.html',
  styleUrls: [
    '../../shared/styles/panels.scss',
    '../../shared/styles/tables.scss',
    './pricing-table.component.scss',
  ]
})
export class PricingTableComponent implements OnInit {
  pricingItems: object[];
  pricingItemsResponse: any;
  pricingTableHeaders: string[] = EVENTCONST.pricingHeaders;
  defaultImagePlaceholder: string = GeneralConstants.PLACEHOLDER;
  defaultCataloguing: string = EVENTCONST.cataloguing;
  cataloguingItems: object = TableDictionary.PRICING_LABELS;
  filter: string;
  currencyCode = GeneralConstants.CURRENCY_DEFAULTS.currencyCode;
  currencySymbol = GeneralConstants.CURRENCY_DEFAULTS.currencySymbol;
  labels = GeneralDictionary.FORM_LABELS
  objectTableOptions: any = { header: false, embedded: true }

  private menuOptions: ContextMenuOptions[] = [{
    text: GeneralDictionary.CONTEXT_MENU.DELETE(GeneralConstants.ENTITY_TYPE.ITEM),
    id: 'menu-delete',
    subject: new Subject<any>()
  }];

  @Input() set setItems(_items: any) {
    this.pricingItemsResponse = _items;
    this.pricingItems = _items && _items._embedded ? _items._embedded.items : _items
  }

  @Output() showPricingItemPopup: EventEmitter<any> = new EventEmitter();
  @Output() moreItems: EventEmitter<any> = new EventEmitter();
  @Output() removeItem: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.pricingTableHeaders = EVENTCONST.pricingHeaders;
    this.cataloguingItems = TableDictionary.PRICING_LABELS;


    // fallback cases for objects while backend fix the object array
    this.defaultImagePlaceholder = GeneralConstants.PLACEHOLDER;
    this.defaultCataloguing = EVENTCONST.cataloguing;

    this.currencyCode = GeneralConstants.CURRENCY_DEFAULTS.currencyCode;
    this.currencySymbol = GeneralConstants.CURRENCY_DEFAULTS.currencySymbol;
    this.menuOptions[0].subject.subscribe(this.deleteItem.bind(this));
  }

  onFilter(word) {
    if (!this.pricingItemsResponse || !this.pricingItemsResponse._embedded) {
      return;
    }

    this.pricingItems = this.pricingItemsResponse._embedded.items;
    this.filter = word;
  }

  lotTableScrolled() {
    this.moreItems.emit();
  }

  deleteItem(item) {
    this.removeItem.emit(item)
  }

  showPricingItemPopUp(item) {
    this.showPricingItemPopup.emit(item);
  }

}
