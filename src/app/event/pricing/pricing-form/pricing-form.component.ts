import * as _ from 'lodash';
import * as moment from 'moment/moment';
import { Component, OnInit, Input, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormsDictionary, FormsConstants, GeneralConstants, GeneralDictionary } from '../../shared/constants';
import { ClientFormComponent, AffiliatedClientComponent } from '../../shared/components';
import { noRequiredObjectValidator } from '../../shared/validators';
import { autocompleListFormatter, convertToAutoformatCompatibleObject, setValidators } from '../../shared/utils';
import { ValuationOriginService, ValuationTypeService } from '../../shared/services';
import { Subscription } from 'rxjs/Subscription';
import { PricingEvent } from '../../shared/models';
import { formatDate, processParamsObject } from '../../shared/utils';

@Component({
  selector: 'pricing-form',
  templateUrl: './pricing-form.component.html',
  styleUrls: [
    '../../shared/styles/panels.scss',
    '../../shared/styles/forms.scss',
    './pricing-form.component.scss',
  ]
})
export class PricingFormComponent implements OnInit, OnDestroy, AfterViewInit {

  public datePickerConfig = GeneralConstants.DATE_PICKER_CONFIG;
  public pricingEventForm: FormGroup;
  public valuationFormErrors = {};
  public dictionary = FormsDictionary;
  public labels = GeneralDictionary.FORM_LABELS;
  public listFormatter = autocompleListFormatter;
  public valuationForm: FormGroup;
  public pricingTypes = [];
  public pricingOrigins = [];
  public pricingEvent: PricingEvent;
  private typesObs: any;
  private originsObs: any;
  private formObs: any;
  public formRequired: any = {};
  private disabled = false;
  private searchValidatorMap = {
    valuationOrigin: noRequiredObjectValidator(),
    valuationType: noRequiredObjectValidator(),
  };
  private standarValidatorMap = {
    valuationOrigin: noRequiredObjectValidator(true),
    valuationDate: Validators.required,
    valuationId: Validators.maxLength(20),
    valuationProposal: Validators.maxLength(100),
    valuationType: noRequiredObjectValidator(true),
    valuationNotes: Validators.maxLength(1000),
  }
  public validate = false;

  valuationOriginSubscription: Subscription;
  valuationTypeSubscription: Subscription;

  @ViewChild(ClientFormComponent) clientForm: ClientFormComponent;
  @ViewChild(AffiliatedClientComponent) affiliatedForm: AffiliatedClientComponent;

  @Input()
  hideElements: any[] = [];

  @Input()
  hideAffiliatedClient = false;

  @Input()
  hideClient = false;

  @Input()
  hideSubtitles = false;

  @Input()
  set disableForm(value: boolean) {
    this.disabled = value;
  }

  constructor(
    private fb: FormBuilder,
    private valuationOriginService: ValuationOriginService,
    private valuationTypeService: ValuationTypeService,
  ) {
  }

  ngOnInit() {
    this.valuationForm = this.fb.group({
      code: [''],
      valuationOrigin: [''],
      valuationDate: [''],
      valuationId: [''],
      valuationProposal: [''],
      valuationType: [''],
      valuationNotes: [''],
    });
    this.pricingEventForm = this.fb.group({
      valuation: this.valuationForm,
      eventId: ['']
    });
    if (this.disabled) {
      this.pricingEventForm.disable();
      this.valuationForm.disable()
    }
  }

  ngAfterViewInit() {
    const numberOfEvents = 1000,
      params = { size: numberOfEvents }

    this.formObs = this.valuationForm.valueChanges
      .distinctUntilChanged()
      .debounceTime(100)
      .subscribe(data => this.onValueChanged(data));

    this.valuationOriginSubscription = this.valuationOriginService.elements
      .debounceTime(100)
      .subscribe({
        next: origins => this.pricingOrigins = origins
      })
    this.valuationTypeSubscription = this.valuationTypeService.elements
      .debounceTime(100)
      .subscribe({
        next: types => this.pricingTypes = types
      })
    this.valuationOriginService.getElements(params)
    this.valuationTypeService.getElements(params)
  }

  ngOnDestroy() {
    this.formObs.unsubscribe();
    this.valuationOriginSubscription.unsubscribe();
    this.valuationTypeSubscription.unsubscribe();
  }
  standarMode() {
    setValidators(this.valuationForm, this.standarValidatorMap);
    this.validate = true;
    this.affiliatedForm.validateClientFields();
  }
  searchMode() {
    setValidators(this.valuationForm, this.searchValidatorMap);
    this.validate = false;
    this.affiliatedForm.validateClientFields();
  }
  createMode() {
    this.standarMode();
    this.emptyDate(true);
    this.clientForm.setDefaultValues();
  }

  updateForm(data) {
    this.valuationForm.patchValue(data.valuation);
    if (!this.hideClient) {
      this.clientForm.setFormInfo(data);
    }
    if (!this.hideAffiliatedClient) {
      this.affiliatedForm.setFormInfo(data);
    }
  }

  onValueChanged(data?: any) {
    this.valuationFormErrors = _.reduce(this.valuationForm.controls, (prev, value, index) => {
      if (value.invalid) {
        const message = _.get(this.dictionary.VALUATION, `${_.toUpper(_.snakeCase(index))}`);
        const pairs = _.toPairs(value.errors);
        prev[index] = _.get(message, pairs[0][0]);
      }
      return prev
    }, {});
  }

  serializeDropdownData(oldObject, idLabel, label) {
    const newObject: object = {
      id: oldObject[idLabel],
      text: oldObject[label]
    };

    return newObject;
  }

  reset() {
    this.pricingEventForm.reset();
  }

  emptyDate(now = false) {
    const dateField = this.valuationForm.controls.valuationDate,
      dateVal = now
        ? _.get(dateField, 'value._isValid', false)
          ? dateField.value : moment().format(GeneralConstants.DATE_FORMAT.formatDateUIDisplay) : null

    dateField.patchValue(dateVal)
  }

  getSearchData() {
    const value = this.pricingEventForm.value
    const valuation = _.get(value, 'valuation')
    return {
      type: GeneralConstants.EVENT_TYPES.PRICING,
      code: valuation.code,
      date: formatDate(valuation.valuationDate, true),
      eventId: _.get(value, 'eventId'),
      valuationOriginId: _.get(valuation, 'valuationOrigin.id'),
      valuationTypeId: _.get(valuation, 'valuationType.id'),
      proposal: _.get(valuation, 'valuationProposal'),
      valuationId: _.get(valuation, 'valuationId'),
      notes: _.get(valuation, 'valuationNotes'),
      mainClientId: _.get(value, 'client.clientId'),
      clientSourceId: _.get(value, 'client.clientSource.id'),
      clientSourceTypeId: _.get(value, 'client.clientSourceType.id'),
      clientStatusId: _.get(value, 'client.clientStatus.id'),
      affiliatedClientId: _.get(value, 'affiliatedClient.clientId'),
      affiliatedSourceId: _.get(value, 'affiliatedClient.clientSource.id'),
      affiliatedSourceTypeId: _.get(value, 'affiliatedClient.clientSourceType.id'),
      affiliatedTypeId: _.get(value, 'affiliatedClient.affiliatedType.id'),
      valuation
    }
  }

  getEditionData() {
    const value = this.pricingEventForm.value;
    const valuation = _.get(value, 'valuation')
    return {
      ..._.pick(valuation, 'valuationId', 'valuationOrigin', 'valuationType', 'code'),
      type: GeneralConstants.EVENT_TYPES.PRICING,
      date: formatDate(valuation.valuationDate, true),
      notes: valuation.valuationNotes,
      proposal: valuation.valuationProposal,
      mainClient: processParamsObject({
        clientId: _.get(value, 'client.clientId'),
        clientSource: { id: _.get(value, 'client.clientSource.id') },
        clientSourceType: { id: _.get(value, 'client.clientSourceType.id') },
        clientStatus: { id: _.get(value, 'client.clientStatus.id') },
      }),
      affiliatedClient: this.getAffiliatedClient(),
    }
  }

  private getAffiliatedClient() {
    const value = this.pricingEventForm.value;
    if (_.get(value, 'affiliatedClient.clientId')) {
      return processParamsObject({
        clientId: _.get(value, 'affiliatedClient.clientId'),
        clientSource: { id: _.get(value, 'affiliatedClient.clientSource.id') },
        clientSourceType: { id: _.get(value, 'affiliatedClient.clientSourceType.id') },
        affiliatedType: { id: _.get(value, 'affiliatedClient.affiliatedType.id') },
      })
    }
    return null
  }
}
