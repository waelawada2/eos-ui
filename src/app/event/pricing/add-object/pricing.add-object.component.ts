import { Component, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { DialogService } from 'ng2-bootstrap-modal';
import { GeneralDictionary, GeneralConstants } from '../../shared/constants';
import { AddObjectToEventService, PricingItemService, PricingService } from '../../shared/services';
import { PricingFormComponent } from '../pricing-form/pricing-form.component';
import { ConfirmComponent } from '../../shared/components';
import { getErrorStringFromServer } from '../../shared/utils';
import { Subscription } from 'rxjs/Subscription';
import * as _ from 'lodash';

@Component({
  providers: [PricingService],
  templateUrl: './pricing.add-object.component.html',
  styleUrls: [
    '../../shared/styles/tables.scss',
    '../../shared/styles/forms.scss',
    './pricing.add-object.component.scss',
  ],
})
export class PricingAddObjectComponent implements AfterViewInit, OnDestroy {
  dictionary = GeneralDictionary;
  private pricingSubscription: Subscription;
  private events: any[];
  private options = GeneralConstants.NOTIFICATIONS.OPTIONS;
  private objectId;
  private title = `${_.upperFirst(GeneralDictionary.SECTIONS.PRICING)} ${_.upperFirst(GeneralConstants.ENTITY_TYPE.EVENT)}`;
  private emptyMessage = GeneralDictionary.RESULT_MESSAGES
    .NO_RECORDS_FOUND(`${GeneralDictionary.SECTIONS.PRICING} ${GeneralConstants.ENTITY_TYPE.EVENT}s`)


  private hideElements = ['valuationNotes'];
  @ViewChild(PricingFormComponent) form: PricingFormComponent;

  constructor(
    private pricingService: PricingService,
    private pricingItemService: PricingItemService,
    private addObjectToEventService: AddObjectToEventService,
    protected notificationSvc: NotificationsService,
    protected dialogService: DialogService,
    private router: Router) {
  }

  ngAfterViewInit() {
    this.pricingSubscription = this.pricingService.elements.skip(1).subscribe(this.processPricings.bind(this));
    this.objectId = this.addObjectToEventService.objectId;

    if (!this.objectId) {
      this.returnToObjects();
    }
  }

  ngOnDestroy() {
    this.pricingSubscription.unsubscribe();
  }

  processPricings(events) {
    this.events = events;
  }

  getPricings() {
    this.pricingService.getElements();
  }

  close() {
    this.returnToObjects();
  }

  returnToObjects() {
    this.router.navigate(['object'], { queryParams: { id: this.objectId } });
  }

  filter() {
    const data = this.form.getSearchData();
    this.pricingService.getElements({ ...data, sort: GeneralConstants.SORTING.PRICING.DESC }, _.get(data, 'eventId'))
  }

  addToObject(event) {
    this.dialogService.addDialog(ConfirmComponent, {
      title: GeneralDictionary.NOTIFICATIONS.ADD_OBJECT_TO_EVENT_TITLE,
      message: GeneralDictionary.NOTIFICATIONS.ADD_OBJECT_TO_EVENT(_.upperFirst(_.toLower(GeneralDictionary.SECTIONS.PRICING)))
    })
      .take(1)
      .subscribe((isConfirmed) => {
        if (isConfirmed) {
          this.doCreation(event);
        }
      });
  }

  doCreation(event) {
    this.pricingItemService.create({
      type: _.toUpper(GeneralDictionary.SECTIONS.PRICING),
      event: _.pick(event, ['id', 'type']),
      objects: [{ objectId: this.objectId }]
    }).subscribe((result) => {
      const response = JSON.parse(_.get(result, '_body'));
      if (_.has(response, 'error')) {
        return this.handleErrorAction(result);
      }
      this.addObjectToEventService.event = event;
      this.addObjectToEventService.item = response;
      const url = `${_.lowerCase(GeneralDictionary.SECTIONS.PRICING)}/${event.id}`;
      this.addObjectToEventService.creating = true;
      this.router.navigate([url]);
    })
  }

  handleErrorAction(response) {
    this.notificationSvc.error(GeneralDictionary.NOTIFICATIONS.error,
      getErrorStringFromServer(response), GeneralConstants.NOTIFICATIONS.OPTIONS);
  }

  tableScrolled() {
    const totalElements = this.pricingService.page.value.totalElements;
    if (totalElements > this.events.length) {
      this.pricingService.getNextElements()
    }
  }

  addEvent() {
    const url = `${_.lowerCase(GeneralDictionary.SECTIONS.PRICING)}/new`;
    this.addObjectToEventService.creating = true;
    this.router.navigate([url]);
  }
}
