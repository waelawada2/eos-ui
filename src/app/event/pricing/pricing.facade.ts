import { PricingAddObjectComponent, } from './add-object/pricing.add-object.component';
import { PricingComponent } from './pricing.component';
import { PricingFormComponent } from './pricing-form/pricing-form.component';
import { PricingItemComponent } from './pricing-item/pricing-item.component';
import { PricingItemFormComponent } from './pricing-item/pricing-item-form/pricing-item.form.component';
import { PricingSingleItemComponent } from './pricing-item/pricing-single-item/pricing-single-item.component';
import { PricingTableComponent } from './pricing-table/pricing-table.component';

const PRICING_COMPONENTS = [
  PricingAddObjectComponent,
  PricingComponent,
  PricingFormComponent,
  PricingItemComponent,
  PricingItemFormComponent,
  PricingSingleItemComponent,
  PricingTableComponent,

];

export {
  PricingAddObjectComponent,
  PricingComponent,
  PricingFormComponent,
  PricingItemComponent,
  PricingItemFormComponent,
  PricingSingleItemComponent,
  PricingTableComponent,
  PRICING_COMPONENTS
}
