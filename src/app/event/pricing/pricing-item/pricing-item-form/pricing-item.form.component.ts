import { AfterViewInit, Component, OnInit, OnDestroy, Input, ViewChildren, QueryList, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AuctionTransactionService, PrivateSaleTransactionService, ExhibitionService } from '../../../shared/services';
import { GeneralDictionary, GeneralConstants, FormsDictionary } from '../../../shared/constants';
import {
  autocompleListFormatter, autocompleteByValue,
  convertToAutoformatCompatibleObject, formatDate
} from '../../../shared/utils';
import { ObjectValidator, noRequiredObjectValidator } from '../../../shared/validators';
import { Currency, PricingItem, convertPricingToForm } from '../../../shared/models';
import { createName, randomlyAddFields, PRICING_ITEMS } from '../../../shared/mocks';
import { CurrencyEstimateComponent } from '../../../shared/components';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';
import * as moment from 'moment/moment';
import { ItemDetailsFormComponent } from '../../../shared/components/item-details-form/item-details.form';
const TYPE = 'PRICING';

@Component({
  selector: 'app-pricing-item-form',
  templateUrl: './pricing-item.form.component.html',
  styleUrls: [
    '../../../shared/styles/forms.scss',
    '../../../shared/styles/panels.scss',
    './pricing-item.form.component.scss',
  ],
})
export class PricingItemFormComponent implements AfterViewInit, OnInit, OnDestroy {
  auctionCurrencyFieldSubscription = [];
  componentIds: any = ['auction', 'ps', 'insurance', 'fmv'];
  componentOptions: any = {};
  dictionary = FormsDictionary;
  emptyCurrencySymbol = { code: 'USD', symbol: ' ' };
  fieldsInvalidity = {};
  fieldsValue = {};
  formErrors: any = [];
  form: FormGroup;
  formItemValues: FormGroup;
  formItemPipelines: FormGroup;
  formRequired: any = {};
  generalDictionary = GeneralDictionary;
  items: any[];
  labels = GeneralDictionary.FORM_LABELS;
  listFormatter = autocompleListFormatter;
  listFortmatterByValue = autocompleteByValue;
  options = GeneralConstants.NOTIFICATIONS.SETUP;
  pipeAuctions: any[];
  pipePrivateSales: any[];
  pipeExhibitions: any[];
  requiredMode: boolean;
  transactions: any[];
  types = GeneralConstants.ITEM_TYPES;

  auctionTransactionSubscription: Subscription;
  formValuesPipelinesSubscription: Subscription;
  formPipelineAuctionSubscription: Subscription;
  formPipelinePSSubscription: Subscription;
  formPipelineExhibitionSubscription: Subscription;
  exhibitionSubscription: Subscription;
  privateSaleTransactionSubscription: Subscription;

  @ViewChildren('currencyEstimate') currencyEstimateComponents: QueryList<CurrencyEstimateComponent>;
  @ViewChild(ItemDetailsFormComponent) itemDet: ItemDetailsFormComponent;

  constructor(
    private fb: FormBuilder,
    private auctionTransactionSvc: AuctionTransactionService,
    private privateSaleTransactionSvc: PrivateSaleTransactionService,
    private exhibitionsSvc: ExhibitionService,
  ) {
  }

  ngOnInit() {
    this.setForms();

    this.componentIds.forEach((id) => {
      this.componentOptions[id] = { id, label: this.labels[id] }
    })

    this.componentOptions[this.componentIds[0]].estimateHigh = true;
  }

  ngOnDestroy() {
    this.exhibitionSubscription.unsubscribe();
    this.auctionTransactionSubscription.unsubscribe();
    this.privateSaleTransactionSubscription.unsubscribe();
    this.formValuesPipelinesSubscription.unsubscribe();
    this.formPipelineAuctionSubscription.unsubscribe();
    this.formPipelinePSSubscription.unsubscribe();
    this.formPipelineExhibitionSubscription.unsubscribe();
    _.map(this.auctionCurrencyFieldSubscription, subs => subs.unsubscribe());
  }

  ngAfterViewInit() {
    // setup component options
    this.currencyEstimateComponents.forEach((currencyEstimateComponent: CurrencyEstimateComponent) => {
      this.formItemValues.addControl(currencyEstimateComponent.id, currencyEstimateComponent.form)
    })

    this.formPipelineAuctionSubscription = this.formItemPipelines.get('pipelineAuction').valueChanges
      .distinctUntilChanged()
      .debounceTime(400)
      .subscribe(value => this.filterPipelineSearch(value, this.auctionTransactionSvc, 'pipelineAuctionTarget'))

    this.formPipelinePSSubscription = this.formItemPipelines.get('pipelinePrivateSale').valueChanges
      .distinctUntilChanged()
      .debounceTime(400)
      .subscribe(value => this.filterPipelineSearch(value, this.privateSaleTransactionSvc, 'pipelinePrivateSaleTarget'))

    this.formPipelineExhibitionSubscription = this.formItemPipelines.get('pipelineExhibition').valueChanges
      .distinctUntilChanged()
      .skip(1)
      .subscribe(value => this.checkboxUpdate('pipelineExhibitionTarget', value))

    this.formValuesPipelinesSubscription = this.formItemValues.valueChanges
      .distinctUntilChanged()
      .subscribe(this.currencyPipelinesUpdate.bind(this))

    this.exhibitionSubscription = this.exhibitionsSvc.elements.subscribe({
      next: (exhibitions) => {
        this.pipeExhibitions = exhibitions.map((exhibition) => createName(randomlyAddFields(exhibition)))
      }
    });

    this.auctionTransactionSubscription = this.auctionTransactionSvc.elements.subscribe({
      next: (transactions) => {
        this.pipeAuctions = _.map(transactions, transaction => createName(transaction))
      }
    });

    this.privateSaleTransactionSubscription = this.privateSaleTransactionSvc.elements.subscribe({
      next: (transactions) => {
        this.pipePrivateSales = _.map(transactions, transaction => createName(transaction))
      }
    });

    this.exhibitionsSvc.getElements()
    this.currencyFieldsSubscriptions()
  }

  private checkboxUpdate(fieldName, value) {
    const field = this.formItemPipelines.get(fieldName),
      checkboxValue = _.isObject(value)

    if (checkboxValue) {
      field.enable()
    } else {
      field.disable()
    }

    field.patchValue(checkboxValue)
  }

  private currencyFieldsSubscriptions() {
    const currencyAuction = this.formItemValues.controls[this.componentIds[0]],
      currencyPs = this.formItemValues.controls[this.componentIds[1]],
      currenciesArray = [
        { field: currencyAuction, id: this.componentIds[0] },
        { field: currencyPs, id: this.componentIds[1] }
      ]

    currenciesArray.forEach((currencyGroup) => {
      currencyGroup.field.valueChanges.filter(data => _.get(data, 'currency.id'))
        .distinctUntilChanged()
        .debounceTime(400)
        .subscribe((data) => {
          const date = moment(new Date()).add(1, 'days'),
            params = {
              currencyId: data.currency.id,
              eventDateAfter: formatDate(date, true),
            }

          const transactionType = currencyGroup.id === this.componentIds[0] ?
            this.types.AUCTION : this.types.PRIVATE_SALE

          this.filterPipelineEvent(params, transactionType)
        })
    })
  }

  setValidation(required) {
    this.requiredMode = required
    this.currencyEstimateComponents.forEach((currencyEstimateComponent: CurrencyEstimateComponent) => {
      currencyEstimateComponent.checkValidity(required)
    })
  }

  filterPipelineEvent(params, transactionType) {
    const _transactionType = _.camelCase(_.toLower(transactionType)),
      serviceString = `${_transactionType}TransactionSvc`,
      filterString = `pipe${_.upperFirst(_transactionType)}s`,
      input = this.formItemPipelines.get(`pipeline${_.upperFirst(_transactionType)}`),
      newCurrencyId = _.clone(params).currencyId

    if (!params || !transactionType) {
      return
    }

    this[serviceString].getElements(params);
    this[serviceString].elements
      .skip(1)
      .take(1)
      .distinctUntilChanged()
      .subscribe(filteredtransactions => {
        const currentCurrencyId = _.get(input.value, 'currency.id'),
          pipelineValue = newCurrencyId === currentCurrencyId ? input.value : undefined

        input.reset(pipelineValue)
      })
  }

  currencyPipelinesUpdate(form) {
    const auctionCurrency = form[this.componentIds[0]].currency,
      psCurrency = form[this.componentIds[1]].currency,
      auctionPipe = this.formItemPipelines.get('pipelineAuction'),
      psPipe = this.formItemPipelines.get('pipelinePrivateSale'),
      auctionCheck = this.formItemPipelines.get('pipelineAuctionTarget'),
      psCheck = this.formItemPipelines.get('pipelinePrivateSaleTarget')

    if (auctionCurrency) {
      auctionPipe.enable()
    } else {
      auctionPipe.reset()
      auctionCheck.reset()
      auctionPipe.disable()
      auctionCheck.disable()
    }

    if (psCurrency) {
      psPipe.enable()
    } else {
      psPipe.reset()
      psCheck.reset()
      psPipe.disable()
      psCheck.disable()
    }
  }

  getFormData() {
    const formValues = this.formItemValues.value;
    const formPipelines = this.formItemPipelines.value;
    return {
      code: formValues.code,
      auctionCurrency: _.pick(formValues[this.componentIds[0]].currency, 'id'),
      privateSaleCurrency: _.pick(formValues[this.componentIds[1]].currency, 'id'),
      insuranceCurrency: _.pick(formValues[this.componentIds[2]].currency, 'id'),
      fairMarketValueCurrency: _.pick(formValues[this.componentIds[3]].currency, 'id'),
      auctionLowValue: this.getEstimateValue(formValues[this.componentIds[0]].estimate),
      auctionHighValue: this.getEstimateValue(formValues[this.componentIds[0]].estimateHigh, true),
      privateSaleNetSeller: this.getEstimateValue(formValues[this.componentIds[1]].estimate),
      insurancePrice: this.getEstimateValue(formValues[this.componentIds[2]].estimate),
      fairMarketValuePrice: this.getEstimateValue(formValues[this.componentIds[3]].estimate),
      auctionPlusOne: !!formValues.auctionPlusOne,
      pipelineAuctionTarget: !!formPipelines.pipelineAuctionTarget,
      pipelinePrivateSaleTarget: !!formPipelines.pipelinePrivateSaleTarget,
      pipelineExhibitionTarget: !!formPipelines.pipelineExhibitionTarget,
      pipelineAuction: _.pick(formPipelines.pipelineAuction, ['id', 'type']),
      pipelinePrivateSale: _.pick(formPipelines.pipelinePrivateSale, ['id', 'type']),
      pipelineExhibition: _.pick(formPipelines.pipelineExhibition, ['id', 'type'])
    }
  }

  getEstimateValue(estimateValue, high?) {
    const label = high ? 'highValue' : 'lowValue'
    return _.get(estimateValue, label, estimateValue)
  }

  getSearchFormData() {
    const data = this.getFormData();

    const searchData = {
      type: TYPE,
      auctionCurrencyId: _.get(data.auctionCurrency, 'id'),
      privateSaleCurrencyId: _.get(data.privateSaleCurrency, 'id'),
      insuranceCurrencyId: _.get(data.insuranceCurrency, 'id'),
      fairMarketValueCurrencyId: _.get(data.fairMarketValueCurrency, 'id'),
      pipelineAuctionId: _.get(data.pipelineAuction, 'id'),
      pipelinePrivateSaleId: _.get(data.pipelinePrivateSale, 'id'),
      pipeExhibitionId: _.get(data.pipelineExhibition, 'id'),
      ..._.pick(data, ['auctionLowValue', 'auctionHighValue', 'privateSaleNetSeller',
        'insurancePrice', 'fairMarketValuePrice']),
    };

    return searchData;
  }

  filterPipelineSearch(value, service, target?) {
    if (typeof value === 'string') {
      service.getElements({
        title: value,
        eventDateAfter: formatDate(moment(new Date()).add(1, 'days'), true)})
    }

    this.checkboxUpdate(target, value)
  }

  public disablePipelines() {
    this.formItemPipelines.get('pipelineAuction').disable()
    this.formItemPipelines.get('pipelinePrivateSale').disable()
  }

  public clearForm() {
    this.requiredMode = false;
    this.formErrors = {}
    this.formItemValues.reset()
    this.formItemPipelines.reset()
    this.disablePipelines()
  }

  public fillFormItem(item) {
    this.formItemValues.patchValue({
      auctionPlusOne: item.auctionPlusOne,
      code: item.code,
    })

    this.formItemValues.controls[this.componentIds[0]].patchValue({
      currency: convertToAutoformatCompatibleObject(item.auctionCurrency, 'name'),
      estimate: item.auctionLowValue,
      estimateHigh: item.auctionHighValue,
    })

    this.formItemValues.controls[this.componentIds[1]].patchValue({
      currency: convertToAutoformatCompatibleObject(item.privateSaleCurrency, 'name'),
      estimate: item.privateSaleNetSeller,
    })

    this.formItemValues.controls[this.componentIds[2]].patchValue({
      currency: convertToAutoformatCompatibleObject(item.insuranceCurrency, 'name'),
      estimate: item.insurancePrice,
    })

    this.formItemValues.controls[this.componentIds[3]].patchValue({
      currency: convertToAutoformatCompatibleObject(item.fairMarketValueCurrency, 'name'),
      estimate: item.fairMarketValuePrice,
    })

    this.formItemPipelines.patchValue({
      ..._.pick(item, ['pipelineAuctionTarget', 'pipelineExhibitionTarget', 'pipelinePrivateSaleTarget']),
      pipelineAuction: convertToAutoformatCompatibleObject(createName(item.pipelineAuction), 'name'),
      pipelinePrivateSale: convertToAutoformatCompatibleObject(createName(item.pipelinePrivateSale), 'name'),
      pipelineExhibition: convertToAutoformatCompatibleObject(createName(randomlyAddFields(item.pipelineExhibition)), 'name'),
    })
  }

  setForms() {
    // form structure
    this.formItemValues = this.fb.group({
      auctionPlusOne: [''],
      code: [''],
    })

    this.formItemPipelines = this.fb.group({
      pipelineAuction: ['', noRequiredObjectValidator()],
      pipelineAuctionTarget: [''],
      pipelinePrivateSale: ['', noRequiredObjectValidator()],
      pipelinePrivateSaleTarget: [''],
      pipelineExhibition: ['', noRequiredObjectValidator()],
      pipelineExhibitionTarget: [''],
    })

    this.form = this.fb.group({
      itemValue: this.formItemValues,
      itemPipe: this.formItemPipelines,
    })
  }

  switch(form, ctrl) {
    const elm: any = this[form].get(ctrl);
    if (elm.disabled) {
      return
    }

    elm.patchValue(!elm.value);
  }

  auctionTransactionScroll(event) {
    const totalElements = this.auctionTransactionSvc.page.value.totalElements;
    if (totalElements > this.pipeAuctions.length) {
      this.auctionTransactionSvc.getNextElements();
    }
  }

  privateSaleTransactionScroll(event) {
    const totalElements = this.privateSaleTransactionSvc.page.value.totalElements;
    if (totalElements > this.pipePrivateSales.length) {
      this.privateSaleTransactionSvc.getNextElements();
    }
  }

  exhibitionScroll(event) {
    const totalElements = this.exhibitionsSvc.page.value.totalElements;
    if (totalElements > this.pipeExhibitions.length) {
      this.exhibitionsSvc.getNextElements();
    }
  }

  observableTransactionAuction = (): Observable<any[]> => {
    return this.auctionTransactionSvc.elements
  }

  observableTransactionPrivateSale = (): Observable<any[]> => {
    return this.privateSaleTransactionSvc.elements
  }

  observableExhibition = (): Observable<any[]> => {
    return this.exhibitionsSvc.elements
  }
}
