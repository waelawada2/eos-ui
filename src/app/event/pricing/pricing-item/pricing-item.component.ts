import { AfterViewInit, Component, forwardRef, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { BlockScreenService } from '../../shared/services/block-screen/block-screen.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GeneralDictionary, GeneralConstants, FormsDictionary } from '../../shared/constants';
import { Subscription } from 'rxjs/Subscription';
import { Currency, PricingItem, convertPricingToForm } from '../../shared/models';
import { PricingFormComponent } from '../pricing-form/pricing-form.component';
import { PricingItemFormComponent } from './pricing-item-form/pricing-item.form.component';
import { PricingService, PricingItemService, NavigationService } from '../../shared/services';
import { ItemDetailsFormComponent, ItemCataloguingFormComponent } from '../../shared/components';
import { NotificationsService } from 'angular2-notifications';
import { SothebysLifeCycle, STATES, URLS } from '../../sothebys-lifecycle.abstract'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/race';
import { SORTING } from '../../shared/components/navigation-header/navigation-header.sorting.interface'
import * as _ from 'lodash';
import * as moment from 'moment/moment';
import { DialogService } from 'ng2-bootstrap-modal';

@Component({
  selector: 'app-pricing-item',
  templateUrl: './pricing-item.component.html',
  styleUrls: [
    '../../shared/styles/forms.scss',
    '../../shared/styles/panels.scss',
    './pricing-item.component.scss',
  ],
})
export class PricingItemComponent extends SothebysLifeCycle implements AfterViewInit, OnInit, OnDestroy {

  dictionary = FormsDictionary;
  generalDictionary = GeneralDictionary
  options = GeneralConstants.NOTIFICATIONS.SETUP;
  items: any[];
  current: any;
  form: FormGroup;
  pricingElement: any;
  requiredMode = false;
  pricingSubscription: Subscription;
  formSubscription: Subscription;

  private currentSearch: any = {};
  private currentSearchData: any = {};

  private formEventHiddenFields = ['eventId', 'valuationNotes'];

  protected getErrors: BehaviorSubject<any>;

  @ViewChild(PricingItemFormComponent) formItem: PricingItemFormComponent;
  @ViewChild(ItemDetailsFormComponent) itemDet: ItemDetailsFormComponent;
  @ViewChild(ItemCataloguingFormComponent) itemCat: ItemCataloguingFormComponent;
  @ViewChild(PricingFormComponent) eventForm: PricingFormComponent;

  constructor(
    private fb: FormBuilder,
    private pricingService: PricingService,
    private priItemSvc: PricingItemService,
    protected blockScreenService: BlockScreenService,
    protected route: ActivatedRoute,
    protected router: Router,
    protected dialogService: DialogService,
    protected navService: NavigationService,
    protected notificationSvc: NotificationsService,
  ) {
    super();
    this.route.params.take(1).subscribe(this.loadFromURL.bind(this));
    this.section = GeneralConstants.EVENT_TYPES.PRICING;
    this.sectionType = GeneralConstants.ENTITY_TYPE.ITEM;
    this.getErrors = this.priItemSvc.error;
    this.ableToCreate = false;
  }

  ngOnInit() {
    this.setForms();
    this.ableToRemove.next(true)
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.pricingSubscription.unsubscribe();
    this.formSubscription.unsubscribe();
  }

  ngAfterViewInit() {
    this.form.addControl('itemValue', this.formItem.formItemValues)
    this.form.addControl('itemPipe', this.formItem.formItemPipelines)

    this.stateObservable
      .debounceTime(100)
      .take(1)
      .subscribe({
        next: this.validateForm.bind(this)
      })

    this.formSubscription = this.form.valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .subscribe(this.formChanged.bind(this))

    setTimeout(() => this.form.get('cataloguing').disable());

    this.route.params.take(1)
      .subscribe(params => setTimeout(() => {
        const eventId = _.get(params, 'pricId');
        this.baseUrl = `pricing/${eventId}/items`;
        if (params['id'] === URLS.NEW) {
          this.router.navigate([this.baseUrl, 'search']).then(super.initialize.bind(this))
        } else {
          super.initialize();
        }
      }));

    this.pricingSubscription = this.pricingService.element.subscribe({ next: this.updateEvent.bind(this) })
  }

  validateForm(state) {
    this.requiredMode = state !== STATES.SEARCH
    this.formItem.setValidation(this.requiredMode)
  }

  loadFromURL(params) {
    const pricingId = params['pricId'];
    this.baseUrl = `pricing/${pricingId}/items`;
    this.getPricingEventData(pricingId);
  }

  formChanged(form) {
    this.validData.next(this.form.valid)
  }

  getPricingEventData(id) {
    if (id) {
      this.pricingService.getElement(id);
    } else {
      this.router.navigate(['pricing', 'search']);
    }
  }

  updateEvent(event) {
    this.pricingElement = event;
    if (event) {
      this.eventForm.updateForm(convertPricingToForm(event));
    }
  }

  public create() {
    super.create();
    this.formItem.setValidation(true)
    this.form.get('cataloguing').enable()
    this.formItem.disablePipelines()
  }

  public search() {
    super.search();
    this.formItem.setValidation(false)
    this.form.get('cataloguing').enable()
  }

  protected clearForm(restore) {
    this.updatePages(0, 0);
    this.form.reset()
    this.form.get('cataloguing').disable()
    this.form.updateValueAndValidity({ onlySelf: true })
    this.formItem.clearForm()
    this.currentSearch = {}
    this.loadPaginated = 0;
    if (restore) {
      this.form.patchValue(this.currentSearchData);
    }
    this.current = null;
  }

  protected doSearchAction(id): BehaviorSubject<any> {
    if (id) {
      this.priItemSvc.getElements({}, id)
    } else {
      this.currentSearch = {
        ...this.formItem.getSearchFormData(),
        legacyId: _.get(this.form.value, 'itemDetails.itemId'),
        size: this.pageSize
      }
      this.currentSearchData = this.form.value;
      this.priItemSvc.getItemsByUrl(_.get(this.pricingElement, '_links.items.href'), false,
        { ...this.currentSearch, sort: this.getSortParameters() })
    }

    this.priItemSvc.elements.takeWhile(() => this.state === STATES.EDIT)
      .subscribe({ next: this.updatePricingItems.bind(this) })
    this.priItemSvc.page.takeWhile(() => this.state === STATES.DELETE || this.state === STATES.EDIT)
      .subscribe({ next: (page) => this.updatePages(page.number, page.totalElements) })

    return this.priItemSvc.elements
  }

  protected doCreateAction(): Observable<any> {
    return this.priItemSvc.create(this.getFormData())
  }

  protected doEditAction(): Observable<any> {
    return this.priItemSvc.amend(this.getFormData(), this.current.id)
  }

  protected doDeleteAction(): Observable<any> {
    return this.priItemSvc.delete(this.current.id)
  }

  protected doImportAction(file: File): Observable<any> {
    return this.priItemSvc.import(file, GeneralConstants.IMPORT_TYPES.pricing_item);
  }

  protected doExportAction(): Observable<any> {
    return this.priItemSvc.export(
      {
        ...this.currentSearch,
        type: _.get(this.pricingElement, 'type')
      },
      _.get(this.pricingElement, 'id'))
  }

  protected displayItemNumber(number: number) {
    this.form.get('cataloguing').enable()
    if (number >= 0 && this.items.length > number) {
      this.current = this.items[number]
      if (this.current) {
        this.formItem.fillFormItem(this.current)
        this.fillFormCataloguing(this.current)
      }
      this.updatePages(number, this.priItemSvc.page.value.totalElements)
    } else {
      this.priItemSvc.getNextElements(number);
      this.loadPaginated = number;
    }
    return _.get(this.current, ['id'])
  }

  private updatePricingItems(items) {
    this.items = items
    if (this.loadPaginated !== 0) {
      this.changePage(this.loadPaginated)
      this.loadPaginated = 0;
    }
  }

  getFormData() {
    return {
      type: GeneralConstants.EVENT_TYPES.PRICING,
      event: _.pick(this.pricingElement, ['id', 'type']),
      ...this.formItem.getFormData(),
      cataloguingNotes: _.get(this.itemCat.form.value, 'notes'),
      comments: _.get(this.itemDet.form.value, 'notes'),
      legacyId: _.get(this.itemDet.form.value, 'itemId'),
      objects: _.get(this.current, 'objects', []),
      ...this.itemCat.form.value,
      ...this.itemDet.form.value,
    };
  }

  private fillFormCataloguing(item) {
    this.form.patchValue({
      cataloguing: {
        ..._.pick(item, ['description', 'provenance', 'literature', 'exhibition', 'condition']),
        notes: item.cataloguingNotes,
      },
      itemDetails: {
        itemId: item.legacyId,
        notes: item.comments
      }
    })
  }

  setForms() {
    this.form = this.fb.group({
    })
  }

  protected reorderResults(): BehaviorSubject<any> {
    this.priItemSvc.getItemsByUrl(_.get(this.pricingElement, '_links.items.href'), false,
      { ...this.currentSearch, sort: this.getSortParameters(this.sortOrder) })
    return this.priItemSvc.elements;
  }

  private getSortParameters(order: SORTING = SORTING.DESCENDING) {
    return order === SORTING.DESCENDING ? GeneralConstants.SORTING.PRICING_ITEM.DESC : GeneralConstants.SORTING.PRICING_ITEM.ASC
  }

  goToEventPage() {
    if (this.pricingElement) {
      this.router.navigate([`/pricing/${this.pricingElement.id}`]);
    } else {
      this.router.navigate([`/pricing/search`]);
    }
    return false;
  }
}
