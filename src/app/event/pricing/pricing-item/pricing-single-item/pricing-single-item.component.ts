import { OnInit, Component, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { PricingItemFormComponent } from '../pricing-item-form/pricing-item.form.component';
import { ItemDetailsFormComponent } from '../../../shared/components/item-details-form/item-details.form'
import { GeneralDictionary, FormsDictionary } from '../../../shared/constants';
import { removeScrollBar } from '../../../shared/utils';
import * as _ from 'lodash';

interface Data {
  title: string,
  item: any,
  isClosable: boolean,
}

@Component({
  styleUrls: [
    '../../../shared/styles/panels.scss',
    './pricing-single-item.component.scss',
  ],
  templateUrl: './pricing-single-item.component.html',
})
export class PricingSingleItemComponent extends DialogComponent<Data, any> implements Data, OnInit, AfterViewInit {

  dictionary = FormsDictionary

  item: any;
  title: string;
  form: FormGroup;
  isClosable: boolean;

  hideDetailElements: string[] = ['itemId'];

  @ViewChild(PricingItemFormComponent) formItem: PricingItemFormComponent;
  @ViewChild(ItemDetailsFormComponent) itemDet: ItemDetailsFormComponent;

  constructor(public dialogService: DialogService, private fb: FormBuilder) {
    super(dialogService);
    removeScrollBar()
  }

  ngOnInit() {
    this.form = this.fb.group({
    })
  }

  ngAfterViewInit() {
    this.form.addControl('formItem', this.formItem.form)
    setTimeout(() => {
      this.formItem.setValidation(true)
      this.formItem.fillFormItem(this.item);
      this.itemDet.form.get('notes').patchValue(this.item.comments);
    })
  }

  save() {
    this.result = this.getFormData();
    this.close();
  }

  close() {
    removeScrollBar(false)
    super.close()
  }

  getFormData() {
    const model: any = {
      ...this.formItem.getFormData(),
      ...this.itemDet.form.value,
      ...{ comments: this.itemDet.form.get('notes').value },
      ..._.pick(this.item, ['cataloguingNotes', 'objects', 'event', 'id', 'type']),
    };

    return model
  }

  openItem() {
    const url = `${_.lowerCase(GeneralDictionary.SECTIONS.PRICING)}/${this.item.event.id}/items/${this.item.id}`;
    this.result = { redirect: true, url }
    this.close();
  }

}
