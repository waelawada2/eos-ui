import { Component, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { BlockScreenService } from './../shared/services/block-screen/block-screen.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { DialogService } from 'ng2-bootstrap-modal';
import { GeneralConstants, GeneralDictionary } from '../shared/constants';
import { NavigationService, PricingService, PricingItemService, AddObjectToEventService } from './../shared/services';
import { PricingFormComponent } from './pricing-form/pricing-form.component';
import { PricingSingleItemComponent } from './pricing-item/pricing-single-item/pricing-single-item.component';
import { ConfirmComponent } from './../shared/components';
import { PricingEvent, convertPricingToForm, PricingItem } from '../shared/models';
import { PRICING_ITEMS, createName, randomlyAddFields } from '../shared/mocks';
import { SothebysLifeCycle, STATES } from '../sothebys-lifecycle.abstract';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SORTING } from '../shared/components/navigation-header/navigation-header.sorting.interface'

import * as _ from 'lodash';
import * as moment from 'moment/moment';

const TYPE = 'PRICING';
@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: [
    '../shared/styles/panels.scss',
    './pricing.component.scss',
  ]
})
export class PricingComponent extends SothebysLifeCycle implements AfterViewInit, OnDestroy {
  currentId;
  pricings: PricingEvent[];
  current: PricingEvent;
  generalDictionary = GeneralDictionary;
  generalConst = GeneralConstants;
  eventItems: any[] = [];
  private currentSearch: any = {}
  showTable = false;
  options = GeneralConstants.NOTIFICATIONS.SETUP;
  formSubscription: Subscription;
  itemsSubscription: Subscription;
  stateSubscription: Subscription;
  elementSubscription: Subscription;
  currentSearchData;
  hiddenFields = ['eventId'];

  protected getErrors: BehaviorSubject<any>;

  @ViewChild(PricingFormComponent) form: PricingFormComponent;

  constructor(
    private pricingService: PricingService,
    private addObjectToEventService: AddObjectToEventService,
    private pricingItemSvc: PricingItemService,
    protected blockScreenService: BlockScreenService,
    protected notificationSvc: NotificationsService,
    protected navService: NavigationService,
    protected route: ActivatedRoute,
    protected dialogService: DialogService,
    protected router: Router
  ) {
    super()
    this.baseUrl = 'pricing';
    this.section = TYPE
    this.getErrors = this.pricingService.error;
  }

  ngAfterViewInit() {
    const form = this.form.pricingEventForm
    this.formSubscription = form.valueChanges
      .debounceTime(400)
      .subscribe(() => this.validData.next(form.valid))

    this.stateSubscription = this.stateObservable
      .debounceTime(100)
      .distinctUntilChanged()
      .subscribe(() => this.validData.next(form.valid))

    this.stateObservable
      .debounceTime(100)
      .take(1)
      .subscribe({
        next: this.validateForm.bind(this)
      })

    this.itemsSubscription = this.pricingItemSvc
      .elements.skipWhile(() => this.state === STATES.NEW || this.state === STATES.SEARCH)
      .subscribe({
        next: (items) => {
          this.eventItems = _.clone(items);
          this.ableToRemove.next(items.length <= 0)
        }
      })

    super.initialize();
    this.elementSubscription = this.pricingService.elements
      .distinctUntilChanged()
      .skipWhile(() => this.state !== STATES.EDIT)
      .subscribe({
        next: (event) => {
          this.updatePricing(event)
          if (event.length > 0 && this.currentId && event[0].id === this.currentId && this.addObjectToEventService.creating) {
            this.doItemCreation(event[0]);
          }
        }
      })
  }

  public ngOnDestroy() {
    super.ngOnDestroy();
    this.formSubscription.unsubscribe();
    this.itemsSubscription.unsubscribe();
    this.stateObservable.unsubscribe();
    this.elementSubscription.unsubscribe();
  }

  protected clearForm(restore) {
    this.showTable = false;
    this.updatePages(0, 0);
    this.form.reset();
    this.form.emptyDate();
    this.current = undefined;
    this.eventItems = [];
    this.loadPaginated = 0;
    if (restore) {
      this.form.updateForm({
        ..._.omit(this.currentSearchData, 'client'),
        mainClient: this.currentSearchData.client,
      });
    }
    this.currentSearch = {}
    this.currentSearchData = {}
    this.validateForm(this.state);
  }

  create() {
    super.create();
    this.validateForm(this.state);
  }

  protected doSearchAction(id): BehaviorSubject<any> {
    this.currentId = id;
    this.currentSearchData = this.form.pricingEventForm.getRawValue();
    this.currentSearch = { ...this.form.getSearchData(), valuation: null, size: this.pageSize };
    this.pricingService.getElements({ ...this.currentSearch, sort: this.getSortParameters() }, id);
    return this.pricingService.elements;
  }

  private doItemCreation(event) {
    if (event && this.addObjectToEventService.objectId) {
      if (this.addObjectToEventService.item) {
        this.loadItemPopup(this.addObjectToEventService.item, false)
      } else {
        this.addObjectToEventService.event = event;
        this.pricingItemSvc.create({
          type: _.toUpper(GeneralDictionary.SECTIONS.PRICING),
          event: { id: event.id, type: _.toUpper(GeneralDictionary.SECTIONS.PRICING) },
          objects: [{ objectId: this.addObjectToEventService.objectId }]
        }).take(1).subscribe((result) => {
          const response = JSON.parse(_.get(result, '_body'));
          if (_.has(response, 'error')) {
            return this.handleErrorAction(result);
          }
          this.addObjectToEventService.item = response;
          this.loadItemPopup(this.addObjectToEventService.item, false)
          this.getEventItems()
        })
      }
    }
  }

  protected doCreateAction(): Observable<any> {
    return this.pricingService.create(this.form.getEditionData());
  }

  protected doEditAction(): Observable<any> {
    return this.pricingService.amend(this.form.getEditionData(), this.current.id);
  }

  protected doDeleteAction(): Observable<any> {
    return this.pricingService.delete(this.current.id);
  }

  protected doImportAction(file: File): Observable<any> {
    return this.pricingService.import(file, GeneralConstants.IMPORT_TYPES.pricing);
  }

  protected doExportAction(): Observable<any> {
    return this.pricingService.export(this.currentSearch)
  }

  private updatePricing(p) {
    this.pricings = p;
    if (this.loadPaginated !== 0) {
      this.changePage(this.loadPaginated)
      this.loadPaginated = 0
    }
  }

  protected displayItemNumber(number: number) {
    return this.getPricingData(number);
  }

  private getPricingData(number: number = 0) {
    this.eventItems = [];
    this.showTable = false;
    if (!this.pricings) {
      this.updatePages(0, 0);
      return;
    }
    this.showTable = true;
    if (number >= 0 && this.pricings.length > number) {
      this.current = this.pricings[number];
      if (this.current) {
        this.getEventItems();
        this.form.updateForm(convertPricingToForm(this.current));
        this.validateForm(this.state);
      }
    } else {
      this.loadPaginated = number;
      this.pricingService.getNextElements(number);
    }
    this.updatePages(number, this.pricingService.page.value.totalElements)
    return _.get(this.current, 'id');
  }

  private getEventItems() {
    const linkItems = this.current._links.items
    if (!linkItems) {
      return;
    }
    this.pricingItemSvc.getItemsByUrl(linkItems.href, false, { size: this.pageSize });
    const elements = this.pricingItemSvc.elements;

    elements.takeWhile(() => this.state === STATES.EDIT || this.state === STATES.DELETE)
      .subscribe({
        next: (items) => {
          this.eventItems = items;
          this.ableToRemove.next(this.eventItems.length <= 0)
          this.eventItems.forEach((item) => {
            item.objects = item.objects
            item.pipelineExhibition = !_.isEmpty(item.pipelineExhibition) ?
              randomlyAddFields(item.pipelineExhibition) : item.pipelineExhibition
          })
        }
      })
  }

  public moreItems() {
    const totalElements = this.pricingItemSvc.page.value.totalElements;
    if (totalElements > this.eventItems.length) {
      this.pricingItemSvc.getNextElements()
    }
  }

  protected reorderResults(): BehaviorSubject<any> {
    this.pricingService.getElements({ ...this.currentSearch, sort: this.getSortParameters(this.sortOrder) });
    return this.pricingService.elements;
  }

  private getSortParameters(order: SORTING = SORTING.DESCENDING) {
    return order === SORTING.DESCENDING ? GeneralConstants.SORTING.PRICING.DESC : GeneralConstants.SORTING.PRICING.ASC
  }

  private removeItem(item) {
    if (item.consignmentEvent || item.purchaseEvent) {
      this.showMessage(GeneralDictionary.NOTIFICATIONS.error,
        GeneralDictionary.NOTIFICATIONS.ERR_CANT_DELETE_RELATED_ITEM, GeneralDictionary.NOTIFICATIONS.error)
      return;
    }
    const disposable = this.dialogService.addDialog(ConfirmComponent, {
      title: this.generalDictionary.NOTIFICATIONS.DELETE_CONFIRM_TITLE,
      message: this.generalDictionary.NOTIFICATIONS.DELETE_ITEM_FROM_EVENT(_.upperFirst(_.toLower(this.section)))
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        this.processItemRemoval(item)
      }
      disposable.unsubscribe()
    });
  }

  private processItemRemoval(item) {
    this.pricingItemSvc.delete(item.id).subscribe(
      (result) => this.showMessage(GeneralDictionary.NOTIFICATIONS.success,
        GeneralDictionary.NOTIFICATIONS.deleted(`${_.capitalize(TYPE)} ${GeneralConstants.ENTITY_TYPE.ITEM}`),
        _.toLower(GeneralDictionary.NOTIFICATIONS.success)),
      this.handleErrorAction.bind(this))
  }

  validateForm(state) {
    switch (state) {
      case STATES.SEARCH:
        this.form.searchMode()
        break;
      case STATES.NEW:
        this.form.createMode();
        break;
      default:
        this.form.standarMode()
        break;
    }
  }

  private loadItemPopup(item, isClosable: boolean) {
    this.dialogService.addDialog(PricingSingleItemComponent, {
      item,
      title: `${_.upperFirst(_.toLower(GeneralDictionary.SECTIONS.PRICING))} ${GeneralConstants.ENTITY_TYPE.ITEM}`,
      isClosable: isClosable,
    }).take(1)
      .filter((response) => {
        if (response && response.redirect) {
          this.router.navigate([response.url])
          return false;
        }
        return true;
      })
      .subscribe((response) => {
        this.pricingItemSvc.amend({ ...response, id: null }, response.id).subscribe((res) => {
          if (_.has(response, 'error')) {
            return this.handleErrorAction(res);
          }
          this.addObjectToEventService.reset();
          this.showMessage(GeneralDictionary.NOTIFICATIONS.success,
            GeneralDictionary.NOTIFICATIONS.saved(_.upperFirst(GeneralConstants.ENTITY_TYPE.ITEM)),
            _.toLower(GeneralDictionary.NOTIFICATIONS.success))
        })
      })
  }
}
