import { OnDestroy } from '@angular/core';
import { Router, RouterLink, ActivatedRoute } from '@angular/router';
import { DialogService } from 'ng2-bootstrap-modal';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { NotificationsService } from 'angular2-notifications';
import { ConfirmComponent, UploadDialogComponent, AlertComponent } from './shared/components';
import { NavigationService } from './shared/services';
import { GeneralConstants, GeneralDictionary, EVENTCONST } from './shared/constants';
import {
  getErrorStringFromServer, saveCsvFile,
  formatStringWithStartCase, formatStringWithStartCaseKeepingCharacter, openFileInNewTab
} from './shared/utils';

import { Subscription } from 'rxjs/Subscription';
import { BUTTON_STATUS } from './shared/components/navigation-header/navigation-header.button-status.interface';
import { SORTING } from './shared/components/navigation-header/navigation-header.sorting.interface';
import { } from './shared/utils';
import { BlockScreenService } from './shared/services/block-screen/block-screen.service';


import * as _ from 'lodash';
import AbstractService from 'app/event/shared/services/abstract.service';
import { take } from 'rxjs/operator/take';

export const URLS = {
  NEW: 'new',
  SEARCH: 'search',
  NOT_FOUND: 'not-found',
}

export abstract class SothebysLifeCycle implements OnDestroy {

  protected pageNumber = 0;
  protected pageTotal = 0;
  protected totalElements = 0;
  protected pageSize = 20;
  protected state: STATES = STATES.SEARCH;
  protected navService: NavigationService;
  protected route: ActivatedRoute;
  protected router: Router;
  protected baseUrl: string;
  protected searchAction: Observable<any>;
  protected notificationSvc: NotificationsService;
  protected sectionTitle = '';
  protected section = '';
  protected sectionType: string = GeneralConstants.ENTITY_TYPE.EVENT;
  protected validData: BehaviorSubject<boolean> = new BehaviorSubject(true);
  protected ableToRemove: BehaviorSubject<boolean> = new BehaviorSubject(false);
  protected dialogService: DialogService;
  private navServiceSubs: Subscription;
  private pageChangeSubs: Subscription;
  private sortChangeSubs: Subscription;
  protected stateObservable: BehaviorSubject<STATES> = new BehaviorSubject(STATES.SEARCH);
  private searchLength = 0;
  protected loadPaginated = 0;
  protected ableToCreate = true;
  protected blockScreenService: BlockScreenService;

  protected abstract getErrors: BehaviorSubject<any>;
  private getErrorsSubscription: Subscription;

  protected sortOrder: SORTING = SORTING.DESCENDING;

  public constructor() {
  }

  public initialize() {
    this.navServiceSubs = this.navService.getNavChangeEmitter()
      .subscribe(this.selectedNavItem.bind(this));

    this.pageChangeSubs = this.navService.getPageChangeEmitter()
      .subscribe(this.changePage.bind(this))

    this.sortChangeSubs = this.navService.getSortingChangeEmitter()
      .subscribe(this.sortingChanged.bind(this))


    this.route.params.take(1).subscribe(params => setTimeout(() => {
      switch (params['id']) {
        case URLS.NEW:
          this.goToCreate(false);
          break;
        case URLS.SEARCH:
          this.goToSearch(false);
          break;
        case URLS.NOT_FOUND:
          this.goToSearch();
          break;
        default:
          this.goToEdit(params['id'], false);
          break;
      }
      this.setTitle();
    }));

    this.getErrorsSubscription = this.getErrors.skip(1).subscribe((error) => {
      if (error) {
        this.showMessage(GeneralDictionary.NOTIFICATIONS.error, getErrorStringFromServer(error),
          GeneralConstants.NOTIFICATIONS.TYPES.error)
      }
    })
  }

  public ngOnDestroy() {
    this.navServiceSubs.unsubscribe();
    this.pageChangeSubs.unsubscribe();
    this.sortChangeSubs.unsubscribe();
    this.getErrorsSubscription.unsubscribe();
    this.state = null
  }

  public clear() {
    switch (this.state) {
      case STATES.NEW:
      case STATES.EDIT:
        this.goToSearch()
        break;
      case STATES.SEARCH:
        this.clearForm();
        break;
    }
  }

  public create() {
    switch (this.state) {
      case STATES.SEARCH:
        this.goToCreate();
        break;
      case STATES.EDIT:
      case STATES.NEW:
      default:
        break;
    }
  }

  public delete() {
    switch (this.state) {
      case STATES.EDIT:
        this.goToDelete();
        break;
      case STATES.NEW:
      case STATES.SEARCH:
      default:
        break;
    }
  }

  public search() {
    switch (this.state) {
      case STATES.SEARCH:
        this.goToEdit();
        break;
      case STATES.EDIT:
        this.clear();
        break;
      case STATES.NEW:
        this.goToSearch(true);
        break;
      default:
        break;
    }
  }

  public save() {
    switch (this.state) {
      case STATES.EDIT:
        this.doEditAction().take(1).subscribe(this.afterEditResponse.bind(this), this.handleErrorAction.bind(this));
        break;
      case STATES.NEW:
        this.doCreateAction().take(1).subscribe(this.afterCreateResponse.bind(this), this.handleErrorAction.bind(this));
        break;
      case STATES.SEARCH:
      default:
        break;
    }
  }

  private changeState(state: STATES) {
    this.state = state;
    this.stateObservable.next(state);
  }

  public import() {
    this.dialogService.addDialog(UploadDialogComponent, {
      title: GeneralDictionary.NOTIFICATIONS.UPLOAD_TITLE,
      message: GeneralDictionary.NOTIFICATIONS.UPLOAD_SELECT,
      type: this.getSectionName(),
      entityType: _.toLower(this.sectionType),
    }).take(1).subscribe((file: File) => {
      if (file) {
        this.showMessage(GeneralDictionary.NOTIFICATIONS.info, GeneralDictionary.NOTIFICATIONS.UPLOADING_FILE,
          _.lowerCase(GeneralDictionary.NOTIFICATIONS.info))
        this.doImportAction(file).take(1).subscribe(() => {
          this.showMessage(GeneralDictionary.NOTIFICATIONS.success, GeneralDictionary.NOTIFICATIONS.FILE_UPLOADED,
            _.lowerCase(GeneralDictionary.NOTIFICATIONS.success))
        }, error => {
          this.showMessage(GeneralDictionary.NOTIFICATIONS.error, getErrorStringFromServer(error),
            _.lowerCase(GeneralDictionary.NOTIFICATIONS.error))
        });
      }
    });
  }

  protected getSectionName(): string | string[] {
    return _.toLower(this.section);
  }

  public export() {
    let notification;
    this.doExportAction()
      .distinctUntilKeyChanged('status')
      .subscribe(response => {
        switch (_.get(response, 'status')) {
          case EVENTCONST.EXPORT_STATUS.progress:
            notification = this.showMessage(GeneralDictionary.NOTIFICATIONS.info,
              GeneralDictionary.NOTIFICATIONS.DOWNLOADED_FILE_STARTED(`${_.lowerCase(this.section)} ${_.lowerCase(this.sectionType)}s`),
              _.lowerCase(GeneralDictionary.NOTIFICATIONS.info), { timeOut: 0, clickToClose: false })
            this.blockScreenService.lockScreen();
            break;
          case EVENTCONST.EXPORT_STATUS.completed:
            this.showMessage(GeneralDictionary.NOTIFICATIONS.success,
              GeneralDictionary
                .NOTIFICATIONS
                .DOWNLOADED_FILE(`${_.lowerCase(this.section)} ${_.lowerCase(this.sectionType)}s`, _.get(response, 'fileUrl')
                ),
              _.lowerCase(GeneralDictionary.NOTIFICATIONS.success))

            if (notification) {
              this.notificationSvc.remove(notification.id);
            }
            openFileInNewTab(response.fileUrl);
            this.blockScreenService.unlockScreen();
            break;
        }
      },
        error => {
          this.notificationSvc.remove();
          this.blockScreenService.unlockScreen();
          this.showMessage(GeneralDictionary.NOTIFICATIONS.error, error, GeneralDictionary.NOTIFICATIONS.error)
        })
  }

  private goToSearch(redirect = true, restore = false) {
    if (redirect) {
      this.router.navigate([this.baseUrl, URLS.SEARCH]);
    }
    this.changeState(STATES.SEARCH);
    this.clearForm(restore);
    this.navService.enterSearchMode(this.ableToCreate);
    this.validData
      .takeWhile(() => this.state === STATES.SEARCH)
      .distinctUntilChanged()
      .debounceTime(100)
      .subscribe({
        next: valid => {
          this.updateValidForm(valid, STATES.SEARCH)
        }
      })
  }

  private restore() {
    this.navService.enterSearchMode();
    this.changeState(STATES.SEARCH);
    this.goToSearch(true, true);
  }
  private goToCreate(redirect = true) {
    this.changeState(STATES.NEW);
    if (redirect) {
      this.router.navigate([this.baseUrl, URLS.NEW]);
    }
    this.navService.enterCreateMode();
    this.validData
      .takeWhile(() => this.state === STATES.NEW)
      .subscribe({ next: (v) => this.updateValidForm(v, STATES.NEW) })
  }

  private goToEdit(id = null, redirect = true) {
    this.changeState(STATES.EDIT);
    this.validData
      .takeWhile(() => this.state === STATES.EDIT)
      .subscribe({ next: (response) => this.updateValidForm(response, STATES.EDIT, !!id) })
    this.ableToRemove
      .takeWhile(() => this.state === STATES.EDIT)
      .subscribe({ next: this.toggleRemoveButton.bind(this) })
    this.doSearchAction(id)
      .skip(1)
      .take(1)
      .subscribe({ next: (response) => this.afterSearchResponse(response, redirect) })
  }

  private goToDelete() {
    this.changeState(STATES.DELETE);
    const section = this.section
    this.dialogService.addDialog(ConfirmComponent, {
      title: GeneralDictionary.NOTIFICATIONS.DELETE_CONFIRM_TITLE,
      message: GeneralDictionary.NOTIFICATIONS.DELETE_CONFIRM(_.lowerCase(`${this.getSectionName()} ${this.sectionType}`)),
    })
      .take(1)
      .subscribe((isConfirmed) => {
        if (isConfirmed) {
          this.doDeleteAction().take(1)
            .subscribe(this.afterDeleteResponse.bind(this), (data) => {
              this.changeState(STATES.EDIT);
              return this.handleErrorAction(data);
            })
        } else {
          this.changeState(STATES.EDIT);
        }
      });
    this.ableToRemove.takeWhile(() => this.state === STATES.EDIT).subscribe({ next: this.toggleRemoveButton.bind(this) })
    this.validData.takeWhile(() => this.state === STATES.EDIT)
      .subscribe({ next: (response) => this.updateValidForm(response, STATES.EDIT) })
  }

  private updateValidForm(valid, state, hasId: boolean = false) {
    switch (state) {
      case STATES.EDIT:
        this.ableToRemove.take(1).subscribe(value => this.navService.enterEditMode(valid, value, hasId))
        break;
      case STATES.NEW:
        this.navService.enterCreateMode(valid);
        break;
      case STATES.SEARCH:
        this.navService.enterCustomMode({ search: !valid ? BUTTON_STATUS.DISABLED : BUTTON_STATUS.SHOWN });
        break;

    }
  }

  private toggleRemoveButton(ableToRemove) {
    this.navService.enterCustomMode({ remove: ableToRemove ? BUTTON_STATUS.SHOWN : BUTTON_STATUS.DISABLED })
  }

  setTitle() {
    const section = GeneralDictionary.SECTIONS;
    const titleName = formatStringWithStartCaseKeepingCharacter(`${this.section} ${this.sectionType}`, '/');
    switch (this.state) {
      case STATES.EDIT:
        this.sectionTitle = section.TITLES.EDIT(titleName);
        break;
      case STATES.NEW:
        this.sectionTitle = section.TITLES.NEW(titleName);
        break;
      case STATES.SEARCH:
      default:
        this.sectionTitle = section.TITLES.SEARCH(`${titleName}s`);
        break;
    }
  }

  selectedNavItem(event: string) {
    switch (event) {
      case 'save':
        this.save();
        break;
      case 'search':
        this.search();
        break;
      case 'new':
        this.create()
        break;
      case 'clearSearch':
        this.clear();
        break;
      case 'restore':
        this.restore();
        break;
      case 'remove':
        this.delete();
        break;
      case 'import':
        this.import();
        break;
      case 'export':
        this.export();
        break;
      default:
        break;
    }
    this.setTitle()
  }

  private afterSearchResponse(d, redirect = true): void {
    if (!d) {
      this.goToSearch();
      return;
    }
    if (d.length === 0) {
      this.searchLength = 0;
      if (this.ableToCreate) {
        return this.promptCreation();
      } else {
        const sectionName = this.getSectionName();
        const flattedSectionName = typeof sectionName === 'string' ? sectionName : sectionName.join(' or ');
        const sectionType = `${this.sectionType}s`;

        const title = GeneralDictionary.NOTIFICATIONS.NOT_FOUND_TITLE;
        const message = GeneralDictionary.NOTIFICATIONS.NOT_FOUND_MESSAGE(_.toLower(`${flattedSectionName} ${sectionType}`));

        return this.showAlert(title, message);
      }
    }
    this.searchLength = d.length;
    const first: any = _.first(d);
    if (first && first.id || first && first.entityId) {
      if (redirect) {
        const id = first.id ? first.id : first.entityId;
        this.router.navigate([this.baseUrl, id]);
      }
      this.displayItemNumber(0);
    } else if (redirect) {
      this.router.navigate([this.baseUrl, URLS.NOT_FOUND]);
    }

    this.navService.toggleSortArrows(d.length > 1)
  }

  private afterEditResponse(d): void {
    const response = _.get(d, '_body');

    if (_.has(response, 'error')) {
      return this.handleErrorAction(d);
    }
    const title = formatStringWithStartCase(`${this.section} ${this.sectionType}`);
    this.showMessage(GeneralDictionary.NOTIFICATIONS.success,
      GeneralDictionary.NOTIFICATIONS.updated(title),
      _.lowerCase(GeneralDictionary.NOTIFICATIONS.success));
    this.navService.emitUpwardsPageChange(0);
  }

  private afterCreateResponse(data): void {
    const response = JSON.parse(_.get(data, '_body'));
    if (_.has(response, 'error')) {
      return this.handleErrorAction(data);
    }
    this.finishCreateState(response)
  }

  public finishCreateState(response) {
    const title = formatStringWithStartCase(`${this.section} ${this.sectionType}`);
    this.showMessage(GeneralDictionary.NOTIFICATIONS.success,
      GeneralDictionary.NOTIFICATIONS.saved(title),
      _.lowerCase(GeneralDictionary.NOTIFICATIONS.success));
    this.goToEdit(_.get(response, 'id'))
  }

  private afterDeleteResponse(data): void {
    this.changeState(STATES.EDIT);
    if (this.searchLength > 1) {
      this.searchLength--;
      const id = this.displayItemNumber(0);
      this.router.navigate([this.baseUrl, id]);
    } else {
      this.goToSearch();
    }

    const title = formatStringWithStartCase(`${this.section} ${this.sectionType}`);
    this.showMessage(GeneralDictionary.NOTIFICATIONS.success,
      GeneralDictionary.NOTIFICATIONS.removed(title),
      _.lowerCase(GeneralDictionary.NOTIFICATIONS.success));
  }

  private promptCreation() {
    const sectionName = this.getSectionName();
    this.dialogService.addDialog(ConfirmComponent, {
      title: GeneralDictionary.NOTIFICATIONS.CREATE_QUESTION_TITLE,
      message: GeneralDictionary.NOTIFICATIONS.CREATE_QUESTION(_.toLower(typeof sectionName === 'string' ? sectionName : sectionName[0])),
    }).take(1).subscribe((isConfirmed) => {
      if (isConfirmed) {
        this.goToCreate()
      } else {
        this.goToSearch()
      }
      this.setTitle()
    });
  }

  private showAlert(title, message) {
    const sectionName = this.getSectionName();
    const sectionType = this.sectionType;
    this.dialogService.addDialog(AlertComponent, {
      title: title,
      message: message,
    }).take(1).subscribe(() => {
      this.goToSearch()
    });
  }

  protected showMessage(title, content, type, _overwriteOptions = {}) {
    let notification;
    const options = _.merge(_.clone(GeneralConstants.NOTIFICATIONS.OPTIONS), _overwriteOptions);
    if (type === 'success') {
      notification = this.notificationSvc.success(title, content, options);
    } else if (type === 'tableInfo' || type === 'info') {
      notification = this.notificationSvc.info(title, content, options);
    } else {
      notification = this.notificationSvc.error(title, content, options);
    }
    return notification;
  }

  protected handleErrorAction(data) {
    this.showMessage(GeneralDictionary.NOTIFICATIONS.error, getErrorStringFromServer(data),
      _.lowerCase(GeneralDictionary.NOTIFICATIONS.error));
  }

  protected changePage(page: string | number) {
    const id = this.displayItemNumber(+page);
    this.router.navigate([this.baseUrl, id]);
    this.updatePages(+page, this.pageTotal)
  }

  protected updatePages(number, total) {
    this.pageNumber = number
    this.pageTotal = total;
  }

  protected sortingChanged(value: SORTING) {
    this.sortOrder = value;
    if (this.state === STATES.EDIT) {
      const reorderObs = this.reorderResults()
      if (reorderObs) {
        reorderObs.skip(1)
          .take(1)
          .subscribe({ next: this.afterSearchResponse.bind(this) })
      }
    }
  }

  protected removeObject(item, object, service: AbstractService<any>) {
    if (item.objects.length === 1) {
      this.showMessage(
        GeneralDictionary.NOTIFICATIONS.error,
        GeneralDictionary.NOTIFICATIONS.REMOVE_LAST_OBJECT_ERROR_MESSAGE,
        'error'
      )
      return;
    }

    this.dialogService.addDialog(ConfirmComponent, {
      title: GeneralDictionary.NOTIFICATIONS.REMOVE_CONFIRM_TITLE,
      message: GeneralDictionary.NOTIFICATIONS.REMOVE_OBJECT_FROM_ITEM,
    })
      .take(1)
      .subscribe((isConfirmed) => {
        if (isConfirmed) {
          const modifiedItem: any = _.clone(item)
          modifiedItem.id = null
          modifiedItem.objects = item.objects.filter(obj => obj.objectId !== object.id)

          service.amend(modifiedItem, item.id).subscribe((res) => {
            if (res.status === 200) {
              item.objects = JSON.parse(res._body).objects;
              this.showMessage(
                GeneralDictionary.NOTIFICATIONS.success,
                GeneralDictionary.NOTIFICATIONS.REMOVE_OBJECT_SUCCESS_MESSAGE,
                'success'
              )
            }
          })
        }
      });
  }
  protected abstract clearForm(restore?);
  protected abstract doSearchAction(id): BehaviorSubject<any>;
  protected abstract doCreateAction(): Observable<any>;
  protected abstract doEditAction(): Observable<any>;
  protected abstract doDeleteAction(): Observable<any>;
  protected abstract doExportAction(): Observable<any>;
  protected abstract doImportAction(file: File): Observable<any>;
  protected abstract displayItemNumber(number: number);
  protected abstract reorderResults(): BehaviorSubject<any>;

}

export class STATES {
  static SEARCH = 'search';
  static NEW = 'new';
  static EDIT = 'edit';
  static DELETE = 'delete';
}
