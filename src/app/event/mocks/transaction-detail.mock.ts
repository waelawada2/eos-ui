
export const TRANSACTIONDETAILS = {
  eventId: 'TR1',
  date: '08/20/20017',
  type: 'Auctions',
  title: 'Event',
  venue: {
    venueId: '1',
    name: 'Establishment1',
    location: {
      countryName: 'Colombia',
      state: 'Antioquia',
      cityName: 'Medellin',
      region: ''
    }
  },
  department: 'CAT/SAP',
  saleNumber: 1,
  currency: 'USD',
  totalLots: 1,
  notes: 'Event transaction'
}


