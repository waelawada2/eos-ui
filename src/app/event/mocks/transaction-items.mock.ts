export const TRANSACTION_ITEMS = {
  "_embedded": {
    "items": [
      {
        "itemId": "sample-item-id-mongobee-0",
        "type": "TRANSACTION",
        "event": null,
        "objects": [
          {
            "objectId": "sample-object-id-mongobee-0",
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/sample-object-id-mongobee-0"
              }
            }
          },
          {
            "objectId": "sample-object-id-mongobee-1",
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/sample-object-id-mongobee-1"
              }
            }
          },
          {
            "objectId": "sample-object-id-mongobee-2",
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/sample-object-id-mongobee-2"
              }
            }
          }
        ],
        "legacyId": "abcd123-mongobee",
        "comments": "Test Comments mongobee",
        "description": "Test description mongobee",
        "provenance": "Test provenance mongobee",
        "exhibition": "Test exhibition mongobee",
        "literature": "Test Literature mongobee",
        "condition": "Test Condition mongobee",
        "cataloguingNotes": "Test Notes mongobee",
        "transactionType": "AUCTION",
        "lotNumber": "1",
        "status": "SOLD",
        "consignmentEvent": null,
        "purchaseEvent": null,
        "auction": {
          "estimateOnRequest": true,
          "lowEstimate": 1000,
          "highEstimate": 2000,
          "term": "GUARANTEE",
          "hammer": 3000,
          "premium": 5000
        },
        "privateSale": null,
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/items/sample-item-id-mongobee-0"
          }
        }
      },
      {
        "itemId": "sample-item-id-mongobee-1",
        "type": "TRANSACTION",
        "event": null,
        "objects": [
          {
            "objectId": "sample-object-id-mongobee-0",
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/sample-object-id-mongobee-0"
              }
            }
          },
          {
            "objectId": "sample-object-id-mongobee-1",
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/sample-object-id-mongobee-1"
              }
            }
          },
          {
            "objectId": "sample-object-id-mongobee-2",
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/sample-object-id-mongobee-2"
              }
            }
          }
        ],
        "legacyId": "abcd123-mongobee",
        "comments": "Test Comments mongobee",
        "description": "Test description mongobee",
        "provenance": "Test provenance mongobee",
        "exhibition": "Test exhibition mongobee",
        "literature": "Test Literature mongobee",
        "condition": "Test Condition mongobee",
        "cataloguingNotes": "Test Notes mongobee",
        "transactionType": "AUCTION",
        "lotNumber": "1",
        "status": "SOLD",
        "consignmentEvent": null,
        "purchaseEvent": null,
        "auction": {
          "estimateOnRequest": true,
          "lowEstimate": 1000,
          "highEstimate": 2000,
          "term": "GUARANTEE",
          "hammer": 3000,
          "premium": 5000
        },
        "privateSale": null,
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/items/sample-item-id-mongobee-1"
          }
        }
      },
      {
        "itemId": "sample-item-id-mongobee-2",
        "type": "TRANSACTION",
        "event": null,
        "objects": [
          {
            "objectId": "sample-object-id-mongobee-0",
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/sample-object-id-mongobee-0"
              }
            }
          },
          {
            "objectId": "sample-object-id-mongobee-1",
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/sample-object-id-mongobee-1"
              }
            }
          },
          {
            "objectId": "sample-object-id-mongobee-2",
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/sample-object-id-mongobee-2"
              }
            }
          }
        ],
        "legacyId": "abcd123-mongobee",
        "comments": "Test Comments mongobee",
        "description": "Test description mongobee",
        "provenance": "Test provenance mongobee",
        "exhibition": "Test exhibition mongobee",
        "literature": "Test Literature mongobee",
        "condition": "Test Condition mongobee",
        "cataloguingNotes": "Test Notes mongobee",
        "transactionType": "AUCTION",
        "lotNumber": "1",
        "status": "SOLD",
        "consignmentEvent": null,
        "purchaseEvent": null,
        "auction": {
          "estimateOnRequest": true,
          "lowEstimate": 1000,
          "highEstimate": 2000,
          "term": "GUARANTEE",
          "hammer": 3000,
          "premium": 5000
        },
        "privateSale": null,
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/items/sample-item-id-mongobee-2"
          }
        }
      },
      {
        "itemId": "IT-2X0Y54KW7NJ",
        "type": "TRANSACTION",
        "event": {
          "type": "TRANSACTION",
          "eventId": "ET-541Z5YL4ZKX",
          "title": "Transaction event 3",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505943997749,
          "saleNumber": null,
          "totalLots": 0,
          "transactionType": null,
          "statistics": null,
          "currency": null,
          "venue": null,
          "department": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/ET-541Z5YL4ZKX"
            }
          }
        },
        "objects": [
          {
            "objectId": null,
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/null"
              }
            }
          }
        ],
        "legacyId": "abc123 1",
        "comments": "Test comments 1",
        "description": "Test description",
        "provenance": " Test provenance",
        "exhibition": "Test exhibition",
        "literature": " Test literature",
        "condition": "Test condition",
        "cataloguingNotes": "Test notes",
        "transactionType": "AUCTION",
        "lotNumber": "3",
        "status": "SOLD",
        "consignmentEvent": {
          "type": "CONSIGNMENT",
          "eventId": "86951ZMWVXP",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944206836,
          "consignerName": null,
          "consignerId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMWVXP"
            }
          }
        },
        "purchaseEvent": null,
        "auction": {
          "estimateOnRequest": true,
          "lowEstimate": 1000,
          "highEstimate": 2000,
          "term": "GUARANTEE",
          "hammer": 3000,
          "premium": 4000
        },
        "privateSale": null,
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/items/IT-2X0Y54KW7NJ"
          }
        }
      },
      {
        "itemId": "IT-KN26XMQKYML",
        "type": "TRANSACTION",
        "event": {
          "type": "TRANSACTION",
          "eventId": "ET-541Z5YL4ZKX",
          "title": "Transaction event 3",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505943997749,
          "saleNumber": null,
          "totalLots": 0,
          "transactionType": null,
          "statistics": null,
          "currency": null,
          "venue": null,
          "department": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/ET-541Z5YL4ZKX"
            }
          }
        },
        "objects": [
          {
            "objectId": null,
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/null"
              }
            }
          }
        ],
        "legacyId": "abc123 1",
        "comments": "Test comments 1",
        "description": "Test description",
        "provenance": " Test provenance",
        "exhibition": "Test exhibition",
        "literature": " Test literature",
        "condition": "Test condition",
        "cataloguingNotes": "Test notes",
        "transactionType": "AUCTION",
        "lotNumber": "3",
        "status": "SOLD",
        "consignmentEvent": {
          "type": "CONSIGNMENT",
          "eventId": "86951ZMWVXP",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944206836,
          "consignerName": null,
          "consignerId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMWVXP"
            }
          }
        },
        "purchaseEvent": {
          "type": "PURCHASE",
          "eventId": "86951ZMY2J6",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944300057,
          "purchaserName": null,
          "purchaserId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMY2J6"
            }
          }
        },
        "auction": {
          "estimateOnRequest": true,
          "lowEstimate": 1000,
          "highEstimate": 2000,
          "term": "GUARANTEE",
          "hammer": 3000,
          "premium": 4000
        },
        "privateSale": null,
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/items/IT-KN26XMQKYML"
          }
        }
      },
      {
        "itemId": "IT-LNYWX4P6Z9Q",
        "type": "TRANSACTION",
        "event": {
          "type": "TRANSACTION",
          "eventId": "ET-541Z5YL4ZKX",
          "title": "Transaction event 3",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505943997749,
          "saleNumber": null,
          "totalLots": 0,
          "transactionType": null,
          "statistics": null,
          "currency": null,
          "venue": null,
          "department": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/ET-541Z5YL4ZKX"
            }
          }
        },
        "objects": [
          {
            "objectId": null,
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/null"
              }
            }
          }
        ],
        "legacyId": "abc123 1",
        "comments": "Test comments 1",
        "description": "Test description",
        "provenance": " Test provenance",
        "exhibition": "Test exhibition",
        "literature": " Test literature",
        "condition": "Test condition",
        "cataloguingNotes": "Test notes",
        "transactionType": "AUCTION",
        "lotNumber": "3",
        "status": "SOLD",
        "consignmentEvent": {
          "type": "CONSIGNMENT",
          "eventId": "86951ZMWVXP",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944206836,
          "consignerName": null,
          "consignerId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMWVXP"
            }
          }
        },
        "purchaseEvent": {
          "type": "PURCHASE",
          "eventId": "86951ZMY2J6",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944300057,
          "purchaserName": null,
          "purchaserId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMY2J6"
            }
          }
        },
        "auction": {
          "estimateOnRequest": true,
          "lowEstimate": 1000,
          "highEstimate": 2000,
          "term": "GUARANTEE",
          "hammer": 3000,
          "premium": 4000
        },
        "privateSale": null,
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/items/IT-LNYWX4P6Z9Q"
          }
        }
      },
      {
        "itemId": "IT-VN0VJL4V2VX",
        "type": "TRANSACTION",
        "event": {
          "type": "TRANSACTION",
          "eventId": "ET-541Z5YL4ZKX",
          "title": "Transaction event 3",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505943997749,
          "saleNumber": null,
          "totalLots": 0,
          "transactionType": null,
          "statistics": null,
          "currency": null,
          "venue": null,
          "department": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/ET-541Z5YL4ZKX"
            }
          }
        },
        "objects": [
          {
            "objectId": null,
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/null"
              }
            }
          }
        ],
        "legacyId": "abc123 1",
        "comments": "Test comments 1",
        "description": "Test description",
        "provenance": " Test provenance",
        "exhibition": "Test exhibition",
        "literature": " Test literature",
        "condition": "Test condition",
        "cataloguingNotes": "Test notes",
        "transactionType": "AUCTION",
        "lotNumber": "3",
        "status": "SOLD",
        "consignmentEvent": {
          "type": "CONSIGNMENT",
          "eventId": "86951ZMWVXP",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944206836,
          "consignerName": null,
          "consignerId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMWVXP"
            }
          }
        },
        "purchaseEvent": {
          "type": "PURCHASE",
          "eventId": "86951ZMY2J6",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944300057,
          "purchaserName": null,
          "purchaserId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMY2J6"
            }
          }
        },
        "auction": {
          "estimateOnRequest": true,
          "lowEstimate": 1000,
          "highEstimate": 2000,
          "term": "GUARANTEE",
          "hammer": 3000,
          "premium": 4000
        },
        "privateSale": null,
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/items/IT-VN0VJL4V2VX"
          }
        }
      },
      {
        "itemId": "IT-75NQ1ZYN5Z6",
        "type": "TRANSACTION",
        "event": {
          "type": "TRANSACTION",
          "eventId": "ET-541Z5YL4ZKX",
          "title": "Transaction event 3",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505943997749,
          "saleNumber": null,
          "totalLots": 0,
          "transactionType": null,
          "statistics": null,
          "currency": null,
          "venue": null,
          "department": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/ET-541Z5YL4ZKX"
            }
          }
        },
        "objects": [
          {
            "objectId": null,
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/null"
              }
            }
          }
        ],
        "legacyId": "abc123 1",
        "comments": "Test comments 1",
        "description": "Test description",
        "provenance": " Test provenance",
        "exhibition": "Test exhibition",
        "literature": " Test literature",
        "condition": "Test condition",
        "cataloguingNotes": "Test notes",
        "transactionType": "AUCTION",
        "lotNumber": "3",
        "status": "SOLD",
        "consignmentEvent": {
          "type": "CONSIGNMENT",
          "eventId": "86951ZMWVXP",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944206836,
          "consignerName": null,
          "consignerId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMWVXP"
            }
          }
        },
        "purchaseEvent": {
          "type": "PURCHASE",
          "eventId": "86951ZMY2J6",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944300057,
          "purchaserName": null,
          "purchaserId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMY2J6"
            }
          }
        },
        "auction": {
          "estimateOnRequest": true,
          "lowEstimate": 1000,
          "highEstimate": 2000,
          "term": "GUARANTEE",
          "hammer": 3000,
          "premium": 4000
        },
        "privateSale": null,
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/items/IT-75NQ1ZYN5Z6"
          }
        }
      },
      {
        "itemId": "IT-KN26J981Z80",
        "type": "TRANSACTION",
        "event": {
          "type": "TRANSACTION",
          "eventId": "ET-541Z5YL4ZKX",
          "title": "Transaction event 3",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505943997749,
          "saleNumber": null,
          "totalLots": 0,
          "transactionType": null,
          "statistics": null,
          "currency": null,
          "venue": null,
          "department": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/ET-541Z5YL4ZKX"
            }
          }
        },
        "objects": [
          {
            "objectId": null,
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/null"
              }
            }
          }
        ],
        "legacyId": "abc123 1",
        "comments": "Test comments 1",
        "description": "Test description",
        "provenance": " Test provenance",
        "exhibition": "Test exhibition",
        "literature": " Test literature",
        "condition": "Test condition",
        "cataloguingNotes": "Test notes",
        "transactionType": "AUCTION",
        "lotNumber": "3",
        "status": "SOLD",
        "consignmentEvent": {
          "type": "CONSIGNMENT",
          "eventId": "86951ZMWVXP",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944206836,
          "consignerName": null,
          "consignerId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMWVXP"
            }
          }
        },
        "purchaseEvent": {
          "type": "PURCHASE",
          "eventId": "86951ZMY2J6",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944300057,
          "purchaserName": null,
          "purchaserId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMY2J6"
            }
          }
        },
        "auction": {
          "estimateOnRequest": true,
          "lowEstimate": 1000,
          "highEstimate": 2000,
          "term": "GUARANTEE",
          "hammer": 3000,
          "premium": 4000
        },
        "privateSale": null,
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/items/IT-KN26J981Z80"
          }
        }
      },
      {
        "itemId": "IT-WVPZ8L5J0MW",
        "type": "TRANSACTION",
        "event": {
          "type": "TRANSACTION",
          "eventId": "ET-541Z5YL4ZKX",
          "title": "Transaction event 3",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505943997749,
          "saleNumber": null,
          "totalLots": 0,
          "transactionType": null,
          "statistics": null,
          "currency": null,
          "venue": null,
          "department": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/ET-541Z5YL4ZKX"
            }
          }
        },
        "objects": [
          {
            "objectId": null,
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/null"
              }
            }
          }
        ],
        "legacyId": "abc123 1",
        "comments": "Test comments 1",
        "description": "Test description",
        "provenance": " Test provenance",
        "exhibition": "Test exhibition",
        "literature": " Test literature",
        "condition": "Test condition",
        "cataloguingNotes": "Test notes",
        "transactionType": "AUCTION",
        "lotNumber": "3",
        "status": "SOLD",
        "consignmentEvent": {
          "type": "CONSIGNMENT",
          "eventId": "86951ZMWVXP",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944206836,
          "consignerName": null,
          "consignerId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMWVXP"
            }
          }
        },
        "purchaseEvent": {
          "type": "PURCHASE",
          "eventId": "86951ZMY2J6",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944300057,
          "purchaserName": null,
          "purchaserId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMY2J6"
            }
          }
        },
        "auction": {
          "estimateOnRequest": true,
          "lowEstimate": 1000,
          "highEstimate": 2000,
          "term": "GUARANTEE",
          "hammer": 3000,
          "premium": 4000
        },
        "privateSale": null,
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/items/IT-WVPZ8L5J0MW"
          }
        }
      },
      {
        "itemId": "IT-1XW2NKQV5N1",
        "type": "TRANSACTION",
        "event": {
          "type": "TRANSACTION",
          "eventId": "ET-541Z5YL4ZKX",
          "title": "Transaction event 3",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505943997749,
          "saleNumber": null,
          "totalLots": 0,
          "transactionType": null,
          "statistics": null,
          "currency": null,
          "venue": null,
          "department": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/ET-541Z5YL4ZKX"
            }
          }
        },
        "objects": [
          {
            "objectId": null,
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/null"
              }
            }
          }
        ],
        "legacyId": "abc123 1",
        "comments": "Test comments 1",
        "description": "Test description",
        "provenance": " Test provenance",
        "exhibition": "Test exhibition",
        "literature": " Test literature",
        "condition": "Test condition",
        "cataloguingNotes": "Test notes",
        "transactionType": "AUCTION",
        "lotNumber": "3",
        "status": "SOLD",
        "consignmentEvent": {
          "type": "CONSIGNMENT",
          "eventId": "86951ZMWVXP",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944206836,
          "consignerName": null,
          "consignerId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMWVXP"
            }
          }
        },
        "purchaseEvent": {
          "type": "PURCHASE",
          "eventId": "86951ZMY2J6",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944300057,
          "purchaserName": null,
          "purchaserId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMY2J6"
            }
          }
        },
        "auction": {
          "estimateOnRequest": true,
          "lowEstimate": 1000,
          "highEstimate": 2000,
          "term": "GUARANTEE",
          "hammer": 3000,
          "premium": 4000
        },
        "privateSale": null,
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/items/IT-1XW2NKQV5N1"
          }
        }
      },
      {
        "itemId": "IT-QVQ8J9PW2Q8",
        "type": "TRANSACTION",
        "event": {
          "type": "TRANSACTION",
          "eventId": "ET-541Z5YL4ZKX",
          "title": "Transaction event 3",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505943997749,
          "saleNumber": null,
          "totalLots": 0,
          "transactionType": null,
          "statistics": null,
          "currency": null,
          "venue": null,
          "department": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/ET-541Z5YL4ZKX"
            }
          }
        },
        "objects": [
          {
            "objectId": null,
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/null"
              }
            }
          }
        ],
        "legacyId": "abc123 1",
        "comments": "Test comments 1",
        "description": "Test description",
        "provenance": " Test provenance",
        "exhibition": "Test exhibition",
        "literature": " Test literature",
        "condition": "Test condition",
        "cataloguingNotes": "Test notes",
        "transactionType": "AUCTION",
        "lotNumber": "3",
        "status": "SOLD",
        "consignmentEvent": {
          "type": "CONSIGNMENT",
          "eventId": "86951ZMWVXP",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944206836,
          "consignerName": null,
          "consignerId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMWVXP"
            }
          }
        },
        "purchaseEvent": {
          "type": "PURCHASE",
          "eventId": "86951ZMY2J6",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944300057,
          "purchaserName": null,
          "purchaserId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMY2J6"
            }
          }
        },
        "auction": {
          "estimateOnRequest": true,
          "lowEstimate": 1000,
          "highEstimate": 2000,
          "term": "GUARANTEE",
          "hammer": 3000,
          "premium": 4000
        },
        "privateSale": null,
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/items/IT-QVQ8J9PW2Q8"
          }
        }
      },
      {
        "itemId": "IT-1XW2NKQV9P8",
        "type": "TRANSACTION",
        "event": {
          "type": "TRANSACTION",
          "eventId": "ET-541Z5YL4ZKX",
          "title": "Transaction event 3",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505943997749,
          "saleNumber": null,
          "totalLots": 0,
          "transactionType": null,
          "statistics": null,
          "currency": null,
          "venue": null,
          "department": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/ET-541Z5YL4ZKX"
            }
          }
        },
        "objects": [
          {
            "objectId": null,
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/null"
              }
            }
          }
        ],
        "legacyId": "abc123 1",
        "comments": "Test comments 1",
        "description": "Test description",
        "provenance": " Test provenance",
        "exhibition": "Test exhibition",
        "literature": " Test literature",
        "condition": "Test condition",
        "cataloguingNotes": "Test notes",
        "transactionType": "AUCTION",
        "lotNumber": "3",
        "status": "SOLD",
        "consignmentEvent": {
          "type": "CONSIGNMENT",
          "eventId": "86951ZMWVXP",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944206836,
          "consignerName": null,
          "consignerId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMWVXP"
            }
          }
        },
        "purchaseEvent": {
          "type": "PURCHASE",
          "eventId": "86951ZMY2J6",
          "title": "DHFKJDFH",
          "date": "17-08-2017 12:24:00",
          "notes": "Notes about Transaction Event 3",
          "lastModified": 1505944300057,
          "purchaserName": null,
          "purchaserId": null,
          "sourceId": null,
          "clientId": null,
          "mainId": null,
          "kcmId": null,
          "designation": null,
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/events/86951ZMY2J6"
            }
          }
        },
        "auction": {
          "estimateOnRequest": true,
          "lowEstimate": 1000,
          "highEstimate": 2000,
          "term": "GUARANTEE",
          "hammer": 3000,
          "premium": 4000
        },
        "privateSale": null,
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/items/IT-1XW2NKQV9P8"
          }
        }
      }
    ]
  },
  "_links": {
    "self": {
      "href": "http://localhost:9980/event-service/v1/items"
    }
  },
  "page": {
    "size": 20,
    "totalElements": 19,
    "totalPages": 1,
    "number": 0
  },
}
