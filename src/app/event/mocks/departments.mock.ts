export const DEPARTMENTS = {
  '_embedded': {
    'departments': [
      {
        'departmentId': 'department1',
        'name': 'Department 1',
        '_links': {
          'self': {
            'href': 'http://localhost:9980/event-service/v1/departments/department1'
          }
        }
      },
      {
        'departmentId': 'department2',
        'name': 'Department 2',
        '_links': {
          'self': {
            'href': 'http://localhost:9980/event-service/v1/departments/department2'
          }
        }
      }
    ]
  },
  '_links': {
    'self': {
      'href': 'http://localhost:9980/event-service/v1/departments?page=0&size=20'
    }
  },
  'page': {
    'size': 20,
    'totalElements': 2,
    'totalPages': 1,
    'number': 0
  }
}
