export const CURRENCIES = {
  '_embedded': {
    'currencies': [
      {
        'currencyId': 'COP',
        'name': 'Colombian Peso',
        '_links': {
          'self': {
            'href': 'http://localhost:9980/event-service/v1/currencies/COP'
          }
        }
      },
      {
        'currencyId': 'USD',
        'name': 'American Dollar',
        '_links': {
          'self': {
            'href': 'http://localhost:9980/event-service/v1/currencies/USD'
          }
        }
      }
    ]
  },
  '_links': {
    'self': {
      'href': 'http://localhost:9980/event-service/v1/currencies?page=0&size=20'
    }
  },
  'page': {
    'size': 20,
    'totalElements': 2,
    'totalPages': 1,
    'number': 0
  }
}
