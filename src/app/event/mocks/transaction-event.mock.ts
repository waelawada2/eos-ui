export const EVENTTRANSACTION = {
  "_embedded": {
    "events": [
      {
        "eventId": "ET-2X0PV41PWQ0",
        "type": "TRANSACTION",
        "title": "Transaction event 3",
        "date": "17-08-2017 12:24:00",
        "notes": "Notes about Transaction Event 3",
        "lastModified": 1504744753791,
        "saleNumber": "12345",
        "totalLots": 2,
        "transactionType": "AUCTION",
        "statistics": {
          "confirmedLotsOfMoreThan10Mil": 0,
          "targetedLotsOfMoreThan10Mil": 0,
          "confirmedLotsBetween1And10Mil": 0,
          "targetedLotsBetween1And10Mil": 0,
          "confirmedLotsLessThan1Mil": 0,
          "targetedLotsLessThan1Mil": 0,
          "confirmedLotsTotal": 0,
          "targetedLotsTotal": 0,
          "confirmedLotsTotalLow": 0,
          "targetedLotsTotalLow": 0,
          "confirmedLotsTotalHigh": 0,
          "targetedLotsTotalHigh": 0
        },
        "currency": {
          "currencyId": "COP",
          "name": "Colombian Peso",
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/currencies/COP"
            }
          }
        },
        "venue": {
          "venueId": "sample-venue-id-mongobee0",
          "name": "Venue 0",
          "cityCode": "CLO",
          "cityName": "Cali",
          "countryCode": "CO",
          "countryName": "Colombia",
          "state": "FL",
          "region": "South",
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/venues/sample-venue-id-mongobee0"
            }
          }
        },
        "department": {
          "departmentId": "department1",
          "name": "Department 1",
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/departments/department1"
            }
          }
        },
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/events/ET-2X0PV41PWQ0"
          },
          "items": {
            "href": "http://localhost:9980/event-service/v1/events/ET-2X0PV41PWQ0/items"
          }
        }
      },
      {
        "eventId": "ET-Y61VXYM6V4W",
        "type": "TRANSACTION",
        "title": "Transaction event 3",
        "date": "17-08-2017 12:24:00",
        "notes": "private sale note",
        "lastModified": 1504903913064,
        "saleNumber": "12345",
        "totalLots": 2,
        "transactionType": "PRIVATE_SALE",
        "statistics": {
          "confirmedLotsOfMoreThan10Mil": 0,
          "targetedLotsOfMoreThan10Mil": 0,
          "confirmedLotsBetween1And10Mil": 0,
          "targetedLotsBetween1And10Mil": 0,
          "confirmedLotsLessThan1Mil": 0,
          "targetedLotsLessThan1Mil": 0,
          "confirmedLotsTotal": 0,
          "targetedLotsTotal": 0,
          "confirmedLotsTotalLow": 0,
          "targetedLotsTotalLow": 0,
          "confirmedLotsTotalHigh": 0,
          "targetedLotsTotalHigh": 0
        },
        "currency": {
          "currencyId": "COP",
          "name": "Colombian Peso",
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/currencies/COP"
            }
          }
        },
        "venue": {
          "venueId": "sample-venue-id-mongobee0",
          "name": "Venue 0",
          "cityCode": "CLO",
          "cityName": "Cali",
          "countryCode": "CO",
          "countryName": "Colombia",
          "state": "FL",
          "region": "South",
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/venues/sample-venue-id-mongobee0"
            }
          }
        },
        "department": {
          "departmentId": "department1",
          "name": "Department 1",
          "_links": {
            "self": {
              "href": "http://localhost:9980/event-service/v1/departments/department1"
            }
          }
        },
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/events/ET-Y61VXYM6V4W"
          },
          "items": {
            "href": "http://localhost:9980/event-service/v1/events/ET-Y61VXYM6V4W/items"
          }
        }
      }
    ]
  },
  "_links": {
    "self": {
      "href": "http://localhost:9980/event-service/v1/events?page=0&size=20"
    }
  },
  "page": {
    "size": 20,
    "totalElements": 2,
    "totalPages": 1,
    "number": 0
  }
}