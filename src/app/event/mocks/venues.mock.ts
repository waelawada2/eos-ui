export const VENUES = {
  '_embedded': {
    'venues': [
      {
        'venueId': 'sample-venue-id-mongobee0',
        'name': 'Venue 0',
        'cityCode': 'CLO',
        'cityName': 'Cali',
        'countryCode': 'CO',
        'countryName': 'Colombia',
        'state': 'FL',
        'region': 'South',
        '_links': {
          'self': {
            'href': 'http://localhost:9980/event-service/v1/venues/sample-venue-id-mongobee0'
          }
        }
      },
      {
        'venueId': 'sample-venue-id-mongobee1',
        'name': 'Venue 1',
        'cityCode': 'CLO',
        'cityName': 'Cali',
        'countryCode': 'CO',
        'countryName': 'Colombia',
        'state': 'FL',
        'region': 'South',
        '_links': {
          'self': {
            'href': 'http://localhost:9980/event-service/v1/venues/sample-venue-id-mongobee1'
          }
        }
      }
    ]
  },
  '_links': {
    'self': {
      'href': 'http://localhost:9980/event-service/v1/venues?page=0&size=20'
    }
  },
  'page': {
    'size': 20,
    'totalElements': 10,
    'totalPages': 1,
    'number': 0
  }
}
