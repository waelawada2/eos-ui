import { AddEventsToItemsService } from './services/consignment-purchase/add-events-to-items.service';
import { AddObjectToEventService } from './services/add-event-to-object/add-object-to-event.service';
import { AffiliatedTypeService } from './services/client/affiliated-type.service';
import { AuctionTermsServices } from '../shared/services/auction-terms/auction-terms.service';
import { AuctionTransactionService } from './services/transaction/auction-transaction.service';
import { ClientSegmentService } from './services/client/client-segment.service';
import { ClientSourceService } from './services/client/client-source.service';
import { ClientSourceTypeService } from './services/client/client-source-type.service';
import { ClientStatusService } from './services/client/client-status.service';
import { ClientTypeService } from './services/client/client-type.service';
import { ColourTypeService } from './services/colour-type/colour-type.service';
import { ConsignmentPurchaseService } from './services/consignment-purchase/consignment-purchase.service';
import { CurrencyService } from './services/currency/currency.service';
import { DepartmentService } from './services/department/department.service';
import { EstablishmentService } from './services/establishment/establishment.service';
import { EstimateService } from './services/estimate/estimate.service';
import { EventReportService } from '../../shared/services/object/event-report/event-report.service';
import { ExhibitionService } from './services/exhibition/exhibition.service';
import { ExternalClientService } from './services/client/external-client.service';
import { ExternalClientStatusService } from './services/client/external-client-status.service';
import { ExternalClientSuffixService } from './services/client/external-client-suffix.service';
import { ExternalClientTitleService } from './services/client/external-client-title.service';
import { GendersService } from './services/genders/genders.service';
import { ItemsService } from './services/items.service';
import { KeyClientManagerService } from './services/client//kcm.service';
import { NavigationService } from './services/navigation/navigation.service';
import { PricingItemService } from './services/pricing/pricing-item.service';
import { PricingService } from './services/pricing/pricing.service';
import { PrivateSaleTransactionService } from './services/transaction/private-sale-transaction.service';
import { PropertiesByEventsService } from './services/property/propertiesByEvent.service';
import { PublicationItemService } from './services/publication/publication-item.service';
import { PublicationItemTypeService } from './services/publication/publication-item-type.service';
import { PublicationService } from './services/publication/publication.service';
import { PublicationTypeService } from './services/publication/publication-type.service';
import { TransactionItemService } from './services/transaction/transaction-item.service'
import { TransactionService } from './services/transaction/transaction.service';
import { TransactionStatusService } from './services/transaction/transaction-status.service'
import { ValuationOriginService } from './services/valuation/valuation-origin.service';
import { ValuationTypeService } from './services/valuation/valuation-type.service';

const SHARED_SERVICES = [
  AddEventsToItemsService,
  AddObjectToEventService,
  AffiliatedTypeService,
  AuctionTermsServices,
  AuctionTransactionService,
  ClientSegmentService,
  ClientSourceService,
  ClientSourceTypeService,
  ClientStatusService,
  ClientTypeService,
  ColourTypeService,
  ConsignmentPurchaseService,
  CurrencyService,
  DepartmentService,
  EstablishmentService,
  EstimateService,
  EventReportService,
  ExhibitionService,
  ExternalClientService,
  ExternalClientStatusService,
  ExternalClientSuffixService,
  ExternalClientTitleService,
  GendersService,
  ItemsService,
  KeyClientManagerService,
  NavigationService,
  PricingItemService,
  PricingService,
  PrivateSaleTransactionService,
  PropertiesByEventsService,
  PublicationItemService,
  PublicationItemTypeService,
  PublicationService,
  PublicationTypeService,
  TransactionItemService,
  TransactionService,
  TransactionStatusService,
  ValuationOriginService,
  ValuationTypeService
];

export {
  AddEventsToItemsService,
  AddObjectToEventService,
  AffiliatedTypeService,
  AuctionTermsServices,
  AuctionTransactionService,
  ClientSegmentService,
  ClientSourceService,
  ClientSourceTypeService,
  ClientStatusService,
  ClientTypeService,
  ColourTypeService,
  ConsignmentPurchaseService,
  CurrencyService,
  DepartmentService,
  EstablishmentService,
  EstimateService,
  EventReportService,
  ExhibitionService,
  ExternalClientService,
  ExternalClientStatusService,
  ExternalClientSuffixService,
  ExternalClientTitleService,
  GendersService,
  ItemsService,
  KeyClientManagerService,
  NavigationService,
  PricingItemService,
  PricingService,
  PrivateSaleTransactionService,
  PropertiesByEventsService,
  PublicationItemService,
  PublicationItemTypeService,
  PublicationService,
  PublicationTypeService,
  TransactionItemService,
  TransactionService,
  TransactionStatusService,
  ValuationOriginService,
  ValuationTypeService,
  SHARED_SERVICES,
}
