import * as _ from 'lodash';
import * as moment from 'moment/moment';
import { Observable } from 'rxjs/Observable';
import { GeneralDictionary, GeneralConstants, EVENTCONST } from '../shared/constants';
import { Publication } from './models'

export const autocompleListFormatter = (data: any, paramName: string = 'name'): string => `<span>${_.get(data, paramName)}</span>`;
export const customAutocompleListFormatter = (propertyName) => (_data): string => `<span>${_.get(_data, propertyName)}</span>`;
export const autocompleteByValue = (data: any): string => autocompleListFormatter(data, 'value');
/* This function resolves the ngui/autocomplete bug of patchValue on a form
https://github.com/ng2-ui/auto-complete/issues/130 for this implementation
*/
export const convertToAutoformatCompatibleObject = (object, value = 'name'): any => {
  if (!object) {
    return null;
  }

  const obj = _.clone(object);
  obj.toString = (function () {
    return _.get(this, value)
  })
  return obj;
}
export const formatDate = (date, toServer?) => {
  const serverFormat = GeneralConstants.DATE_FORMAT.formatDateTime;
  const uiFormat = GeneralConstants.DATE_FORMAT.formatDateUIDisplay;
  const input = toServer ? uiFormat : serverFormat;
  const output = !toServer ? uiFormat : serverFormat;
  const _date = moment(date, input);
  return !_date.isValid() ? '' : _date.format(output);
}
export const onMouseOverImage = (eventItem) => {
  const itemImage = EVENTCONST.itemImage;
  itemImage.showImage = true;
  itemImage.xPos = eventItem.clientX;
  itemImage.yPos = eventItem.clientY;
  itemImage.newImage = eventItem.path[0].currentSrc;
}

export const onMouseOutImage = (eventItem) => {
  const itemImage = EVENTCONST.itemImage;
  itemImage.showImage = false;
}

export const getErrorStringFromServer = (data: any, notFoundText?: string): string => {
  const msg = JSON.parse(_.get(data, '_body'));
  if (data.status === 404 && notFoundText) {
    return notFoundText;
  }
  if (data.status === 500) {
    return GeneralDictionary.ERRORS.ERROR_500;
  } else if (_.has(msg, ['0', 'logref'])) {
    return _.get(msg, ['0', 'logref'])
  } else if (_.has(msg, ['0', 'message'])) {
    return _.get(msg, ['0', 'message'])
  } else if (_.has(msg, ['message'])) {
    return _.get(msg, ['message'])
  }
  return _.reduce(msg.messages, (res: string, m: any, index: number) => res + `${m.field} ${m.message}
    ${index < msg.messages.length - 1 ? ' , ' : ''}`, '');

}

export const getFormFormattedPublication = (publication: Publication) => {
  const formPublication: any = _.pick(publication, 'author', 'date', 'edition', 'eventId', 'exhibitionEvents', 'id', 'isbn',
    'notes', 'pages', 'subTitle', 'title', 'publicationEvents', 'publicationType', 'verified', 'volume', 'code');
  formPublication.establishment = {
    establishment: convertToAutoformatCompatibleObject(publication.establishment, 'name'),
  }
  return formPublication
}

const pickedDataFromItemsAuction = ['hammer', 'highEstimate', 'lowEstimate', 'premium'];

export const cleanTransactionItems = (_items: any[]) => {
  _.each(_items, item => {
    item.auction = _.pick(item.auction, pickedDataFromItemsAuction);
  })
  return _items;
}

export const getTransactionItemData = (item) => {
  const auction = _.pick(item.auction, ['estimateOnRequest', 'lowEstimate', 'highEstimate', 'term', 'hammer', 'premium'])
  return {
    objects: _.get(item, 'objects'),
    type: item.event.type,
    ..._.pick(item, ['description', 'provenance', 'literature', 'exhibition', 'condition', 'cataloguingNotes',
      'comments', 'legacyId', 'lotNumber', 'transactionType', 'consignmentEvent', 'objects', 'purchaseEvent', 'status']),
    event: _.pick(item.event, ['id', 'type']),
    auction: _.pick(item.auction, ['estimateOnRequest', 'lowEstimate', 'highEstimate', 'term', 'hammer', 'premium']),
    privateSale: _.pick(item.privateSale, ['netBuyer', 'netVendor']),
  }
}

export const saveCsvFile = (stream: string, name: string) => {
  const data = new Blob([stream], { type: 'text/csv' });
  const textFile = window.URL.createObjectURL(data);

  const link = document.createElement('a');
  link.innerHTML = '';
  link.href = textFile;
  link.download = name;
  link.style.display = 'none';

  const body = document.getElementsByTagName('body')[0]
  body.appendChild(link)

  link.click()

  body.removeChild(link)
}

export const formatStringWithStartCaseKeepingCharacter = (text: string, character: string): string =>
  _.reduce(text.split(character), (res, val, index) => index > 0
    ? `${res}${character}${formatStringWithStartCase(val)}`
    : formatStringWithStartCase(val)
    , '')


export const formatStringWithStartCase = (text: string): string => _.startCase(_.toLower(text))

function eachObject(array, searchValue) {
  let index = -1;
  const length = array.length;
  const result = [];
  let resultIndex = 0;
  while (++index < length) {
    const item = array[index];
    if (cleanEachItem(item, searchValue)) {
      result[resultIndex] = item;
      resultIndex++;
    }
  }
  return result;
}

function cleanEachItem(item, searchValue) {
  let index = -1;
  const keys = Object.keys(item);
  const length = keys.length;
  while (++index < length) {
    const key = keys[index];
    const itemValue = item[key];
    const constructor = itemValue ? itemValue.constructor : '';
    if (!itemValue || constructor === Boolean) {
      continue
    };
    switch (constructor) {
      case Object:
        if (cleanEachItem(itemValue, searchValue)) {
          return true;
        }
        break;
      case Array:
        const innerResult = eachObject(itemValue, searchValue);
        if (innerResult.length !== 0) {
          return true;
        }
        break;
      default:
        if (String(itemValue).toLowerCase().indexOf(searchValue.toLowerCase()) !== -1) {
          return true;
        }
        break;
    }
  }
  return false;
}

export const searchByAnyValue = (items, searchstr) => eachObject(items, searchstr)
export const setValidators = (form, validators) => {
  _.forEach(form.controls, (control, key) => {
    control.clearValidators();
    form.get(key).setValidators(_.get(validators, key));
    control.updateValueAndValidity();
  })
}

export const updateValueAndValidity = form => _.forEach(form.controls, control => { control.updateValueAndValidity() })
export const getAge = (date: string): number => date
  ? Math.floor(moment(new Date()).diff(moment(date, GeneralConstants.DATE_FORMAT.formatDateTime), 'years', true))
  : null
export const getAgeDate = (age: number = 0) => moment().subtract(age, 'years').format(GeneralConstants.DATE_FORMAT.formatDateTime);
export const processParamsObject = (item): any => _.omitBy(item, (value) => _.isEmpty(value) && !_.isNumber(value) && !_.isBoolean(value))
export const cleanLinks: any = (obj) => {
  if (!obj) {
    return null;
  }
  return _.omit(obj, '_links')
};
export const removeScrollBar = (remove = true): any => remove
  ? document.body.style.overflow = 'hidden'
  : document.body.removeAttribute('style')

export const transformToSlicedArray = (arr: any[], size: number) => {
  let pointer = 0;
  const transformed: any = [];
  while (pointer < arr.length) {
    const last = size + pointer > arr.length ? arr.length : size + pointer;
    transformed.push(arr.slice(pointer, last))
    pointer += size;
  }
  return transformed
}

export const getAllClientsIdsFromSubEvents = (items: any[]) => {
  const clientIds = [];

  items.forEach(item => {
    if (item.consignmentEvent) {
      if (item.consignmentEvent.mainClient) {
        clientIds.push(item.consignmentEvent.mainClient.clientId);
      }
      if (item.consignmentEvent.affiliatedClient) {
        clientIds.push(item.consignmentEvent.affiliatedClient.clientId);
      }
    }

    if (item.purchaseEvent) {
      if (item.purchaseEvent.mainClient) {
        clientIds.push(item.purchaseEvent.mainClient.clientId);
      }
      if (item.purchaseEvent.affiliatedClient) {
        clientIds.push(item.purchaseEvent.affiliatedClient.clientId);
      }
    }
  });

  return _.uniq(clientIds);
}

export const appendClientDataToItems = (items: any, clientMap: any) => {
  items.forEach(item => {

    if (item.consignmentEvent) {
      const mainClientIdConsignment = _.get(item, 'consignmentEvent.mainClient.clientId');
      if (mainClientIdConsignment) {
        item.consignmentEvent.mainClient.clientData = clientMap[mainClientIdConsignment];
      }

      const affiliatedClientIdConsignment = _.get(item, 'consignmentEvent.affiliatedClient.clientId');
      if (affiliatedClientIdConsignment) {
        item.consignmentEvent.affiliatedClient.clientData = clientMap[affiliatedClientIdConsignment];
      }

    }

    if (item.purchaseEvent) {
      const mainClientIdPurchase = _.get(item, 'purchaseEvent.mainClient.clientId');
      if (mainClientIdPurchase) {
        item.purchaseEvent.mainClient.clientData = clientMap[mainClientIdPurchase];
      }

      const affiliatedClientIdPurchase = _.get(item, 'purchaseEvent.affiliatedClient.clientId');
      if (affiliatedClientIdPurchase) {
        item.purchaseEvent.affiliatedClient.clientData = clientMap[affiliatedClientIdPurchase];
      }

    }
  });

  return items;
}

// reusable functions for export (events,event items, source names, establishments)
const repeatExportRequesDelay = 2000;

export const exportStatusRequest = (params) => {
  const response = params.response;
  const baseUrl = params.baseUrl;
  const http = params.http;
  let obs = Observable.of(response).take(1);
  switch (response.status) {
    case EVENTCONST.EXPORT_STATUS.progress:
      obs = http.get(baseUrl + '/export/status/' + _.get(response, 'fileId'))
        .map((responseValue: Response) => {
          return JSON.parse(_.get(responseValue, '_body'));
        }).switchMap(status => {
          return Observable.of(status);
        })
      break;
  }
  return obs;
}

export const exportRecursiveRequest = function (params) {
  const expandResponse = params.response;
  const http = params.http;
  const baseUrl = params.baseUrl;
  return exportShouldRepeatRequest(expandResponse) ?
    Observable.of(expandResponse).delay(repeatExportRequesDelay).flatMap(repeatResponse =>
      exportStatusRequest({ response: repeatResponse, http, baseUrl }))
    : Observable.empty()
}

const exportShouldRepeatRequest = (response) => {
  return response.status === EVENTCONST.EXPORT_STATUS.completed ? null : response;
}
export const openFileInNewTab = (fileUrl) => {
  window.open(fileUrl, '_blank');
}
