import { AffiliatedClient, Client } from './../models';

export interface ConsignmentPurchase {
  code: string,
  eventId: string
  type: string,
  date: string,
  mainClient: Client,
  affiliatedClient: AffiliatedClient,
  notes: string
}


export interface ConsignmentPurchaseToItems {
  eventId: string
  type: string,
  date: string,
  mainClient: Client
}
