import * as _ from 'lodash';

export interface Property {
  archiveNumbers: number[],
  artistProofTotal: number,
  artists: Artist[],
  authentication: string,
  bonATirerTotal: number,
  castYearCirca: string,
  castYearStart: string,
  castYearEnd: string,
  certificate: string,
  yearCirca: string,
  depthCm: number,
  editionName: string,
  editionNumber: number,
  foundry: string,
  heightCm: number,
  horsCommerceProofTotal: number,
  id: string,
  imagePath: string,
  medium: string,
  originalTitle: string,
  parentCategory: string,
  parts: string,
  posthumous: string,
  presizeName: string,
  presizeType: string,
  printersProofTotal: number,
  propertyCatalogueRaisonees: any[],
  reminderDate: string,
  reminderEmployee: string,
  reminderNotesText: string,
  researcherName: string,
  researcherNotesDate: string
  researcherNotesText: string,
  seriesTotal: number,
  signature: string,
  signed: string,
  sortCode: string,
  stamped: string,
  subCategory: string
  title: string,
  trialProofTotal: number,
  uniqueEdition: string,
  uploadReference: string,
  weight: string,
  widthCm: number,
  yearStart: number,
  yearEnd: number,
  yearText: string,
  unknownEditionSizeType: string,
  editionSizeNotes: string,
  sizeText: string,
  editionSizeTypeName: number,
  editionSizeTotal: number,
  cancellationProofTotal: number,
  colorTrialProofTotal: number,
  dedicatedProofTotal: number,
  museumProofTotal: number,
  progressiveProofTotal: number,
  rightToProduceProofTotal: number,
  workingProofTotal: number,
  editionOverride: string,
  flags: Flag,
  tags: Tag,
  createdDate: string,
  updatedDate: string,
  createdBy: User,
  updatedBy: User,
  objectCodes: string[],
  // custom fields
  imageUrl?: string
}

export function getHint(prop: Property) {
  let hint: string = _.map(prop.artists, 'display').join(' ')
  if (prop.title || prop.originalTitle) {
    if (prop.title) {
      hint += `\r\n${prop.title} `;
    }
    hint += `${prop.originalTitle ? '(' + prop.originalTitle + ')' : ''}`
  }
  if (prop.medium) {
    hint += `\r\n${prop.medium || ''}`
  }
  if (prop.heightCm || prop.widthCm || prop.depthCm) {
    hint += `\r\n${prop.presizeType ? _.upperFirst(_.get(prop, 'presizeType.name')) + ': ' : ''}${
      _.filter([prop.heightCm, prop.widthCm, prop.depthCm]).join(' x ')} cm.`
  }
  if (prop.yearText) {
    hint += `\r\n${prop.yearText}`
  } else if (prop.yearStart || prop.yearEnd || prop.castYearStart || prop.castYearEnd) {
    hint += `\r\n${_.uniq(_.filter([prop.yearStart, prop.yearEnd])).join('-')}`
    if (prop.castYearStart || prop.castYearEnd) {
      hint += `${prop.yearStart || prop.yearEnd ? ', ' : ''}Cast: ${_.uniq(_.filter([prop.castYearStart, prop.castYearEnd])).join('-')}`
    }
  }
  if (prop.propertyCatalogueRaisonees && prop.propertyCatalogueRaisonees.length) {
    hint += '\r\n' + _.map(prop.propertyCatalogueRaisonees, (x, i) =>
      `CR${i + 1}:${x.artistCatalogueRaisoneeAuthor}:${x.volume}:${x.number}`).join(' ')
  }
  if (prop.archiveNumbers && prop.archiveNumbers.length) {
    hint += '\r\n' + _.map(prop.archiveNumbers, (x, i) => `AR${i + 1}: ${_.get(x, 'archiveNumber')}`).join('; ')
  }
  if (prop.sortCode) {
    hint += `\r\nS:${prop.sortCode}`
  }
  return hint
}

export interface Artist {
  approved: boolean,
  artistCatalogueRaisonees: any[],
  birthYear: number,
  categories: any[],
  countries: Country[],
  createdBy: User,
  createdDate: string,
  deathYear: string,
  display: string,
  firstName: string,
  gender: string,
  id: string,
  lastName: string,
  ulanId: string,
  updatedBy: string,
  updatedDate: string
}

export interface Country {
  id: number,
  countryName: string
}

export interface User {
  firstName: string,
  userName: string,
  lastName: string,
  email: string
}

export interface Flag {
  flagAuthenticity: boolean,
  flagCondition: boolean,
  flagMedium: boolean,
  flagRestitution: boolean,
  flagSfs: boolean,
  flagsCountry: Country
}

export interface Tag {
  imageText: string,
  imageFigure: string,
  expertiseLocation: string,
  imageSubjects: any[],
  imageGenders: any[],
  imagePositions: any[],
  imageAnimals: any[],
  imageMainColours: any[],
  expertiseSitters: any[],
  otherTags: any[],
  autoOrientation: string,
  autoImage: boolean,
  autoScale: string,
  autoVolume: string
}
