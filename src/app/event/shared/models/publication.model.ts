import { Establishment, Exhibition } from './../models';

export interface Publication {
  id: string,
  code: string,
  type: string,
  publicationType: string,
  date: Date,
  title: string,
  subTitle: string,
  author: string,
  establishment: Establishment,
  exhibitionEvents: Exhibition[],
  publicationEvents: Publication[],
  volume: string,
  edition: string,
  pages: number,
  verified: boolean,
  isbn: string,
  notes: string,
  image: string,
  _links: any,
}
