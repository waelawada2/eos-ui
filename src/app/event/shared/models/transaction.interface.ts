import { Establishment, Department, Currency, Statistics } from '../models';

export interface Transaction {
  id: string,
  type: string,
  transactionType: TransactionType,
  title: string,
  establishment: Establishment,
  currency: Currency,
  department: Department,
  saleNumber: number,
  statistics: Statistics,
  date: string,
  totalLots: number,
  notes: string,
  _link: any,
}

export interface TransactionType {
  id: string;
  name: String;
}