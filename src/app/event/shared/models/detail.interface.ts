export interface Detail {
  id: string,
  date: Date,
  type: string,
  title: string,
  establishment: Establishment,
  department: string,
  saleNumber: number,
  currency: string,
  totalLots: number,
  notes: string
}
export interface Establishment {
  id: string,
  name: string,
  cityCode: string,
  cityName: string,
  countryCode: string,
  countryName: string,
  state: string,
  region: string,
}

