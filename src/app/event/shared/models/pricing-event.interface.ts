export interface PricingEvent {
  client: Client,
  affiliatedClient: Client,
  date : string,
  id: string,
  pricingType: string,
  valuationId: string,
  origin : string,
  proposal : string,
  notes :string,
  type: string,
  _links: any,
}

export interface Client {
  id: string,
  type?: number,
  sourceType?: string,
  sourceName?: any,
  status?: number,
  display?: string,
}
