export interface Client {
  clientSource: ClientSource,
  clientSourceType: ClientSourceType,
  clientId: number,
  clientStatus: ClientStatus,
  display: string
}

export interface AffiliatedClient {
  clientId: number,
  clientSource: ClientSource,
  clientSourceType: ClientSourceType,
  affiliatedType: AffiliatedType
}

export interface ClientSource {
  entityId: string,
  name: string
}

export interface ClientSourceType {
  entityId: string,
  name: string
}

export interface ClientStatus {
  entityId?: string,
  name?: string
}

export interface AffiliatedType {
  id: string,
  name: string
}

export interface ExternalClient {
  id?: string,
  entityId?: string,
  name?: string,
  city?: string,
  country?: string,
  state?: string,
  region?: string
  segment?: Segment,
  level?: number;
  individual?: Individual,
  status?: ClientStatus,
  mainClient?: ExternalClient,
  type?: ClientType,
  kcm?: KeyClientManager,
  fromClientSystem?: boolean,
  age?: number,
  _links?: any,
}

export interface ClientType {
  id?: string,
  name?: string,
}

interface Individual {
  firstName?: string,
  middleName?: string,
  lastName?: string,
  gender?: any,
  birthDate?: string,
  deceased?: boolean,
}

export interface KeyClientManager {
  entityId?: string,
  name?: string,
}

export interface Segment {
  name?: string,
  entityId?: string,
}

export interface ClientSuffix {
  name?: string,
}

export interface ClientTitle {
  name: string,
  gender: string
}
