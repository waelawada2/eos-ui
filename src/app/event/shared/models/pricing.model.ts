import { convertToAutoformatCompatibleObject, formatDate } from '../utils';
import { GeneralConstants } from '../constants';
import * as _ from 'lodash';
import * as moment from 'moment/moment';

export interface PricingEvent {
  affiliatedClient: Client,
  client: Client,
  code: string,
  date: string,
  id: string,
  pricingType: string,
  valuationId: string,
  valuationOrigin: any,
  valuationType: any,
  origin: string,
  proposal: string,
  notes: string,
  type: string,
  _links: any,
}

export interface Client {
  id: string,
  type?: number,
  sourceType?: string,
  sourceName?: any,
  status?: number,
  display?: string,
}

export interface PricingItem {
  'itemId': string,
  'type': string,
  'event': {},
  'objects': {},
  'cataloguingNotes': string,
  'values': {},
  'pipeline': {},
}

export function convertPricingToForm(event: PricingEvent) {
  const { id, code, valuationId, valuationOrigin, valuationType, date, notes, proposal, ...pricingModel } = event;
  return {
    valuation: {
      id,
      code,
      valuationOrigin: convertToAutoformatCompatibleObject(valuationOrigin, 'name'),
      valuationType: convertToAutoformatCompatibleObject(valuationType, 'name'),
      valuationDate: formatDate(date),
      valuationNotes: notes,
      valuationProposal: proposal,
      valuationId,
    },
    ...pricingModel,
    affiliatedClient: _.get(pricingModel, 'affiliatedClient', {}),
  };
}
