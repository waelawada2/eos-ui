export class TransactionItem {
  code: string;
  status: string;
  lot?: string;
  image?: ItemImage;
  cataloguing?: ItemCatalog;
  prices?: TransactionPrice[];
  consigners?: TransactionPerson[];
  buyers?: TransactionPerson[];
  auction: Auction;
  consignmentEvent?: any;
  purchaseEvent?: any;
}

interface Auction {
  estimateOnRequest?: boolean,
  hammer: number;
  highEstimate: number;
  lowEstimate: number;
  premium?: string;
  term: Term;
  unsoldHammer: string;
}

interface Term {
  id: string;
  name: string;
}

interface ItemCatalog {
  description: string;
  owner: string;
  title: string;
  category: string;
  dimentions: string,
  created: string;
}

interface TransactionPerson {
  fullname: string;
  type: string;
  id: string;
}

interface TransactionPrice {
  amount: number;
  type: string;
}

interface ItemImage {
  url: string;
  alt: string;
}
