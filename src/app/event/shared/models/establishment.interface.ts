export interface Establishment {
  venueId: string,
  name: string,
  cityCode: string,
  cityName: string,
  countryCode: string,
  countryName: string,
  state: string,
  region: string,
}
