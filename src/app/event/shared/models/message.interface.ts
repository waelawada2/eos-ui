export interface Message {
  title: string,
  message: string,
  type: MessageType,
}


export enum MessageType {
  SUCCESS,
  ERROR,
}
