import { Directive, Input, forwardRef, OnInit, HostListener, Renderer, ElementRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CurrencyCustomSymbolPipe } from '../../pipes';
import { GeneralConstants } from '../../constants';
import * as _ from 'lodash';

@Directive({
  selector: '[currencyFormatted]',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CurrencyFormattedInputDirective),
      multi: true,
    },
  ]
})
export class CurrencyFormattedInputDirective implements ControlValueAccessor, OnInit {

  private value: any = '';
  private currency: any = '';
  private currencyCustomSymbolPipe: CurrencyCustomSymbolPipe = new CurrencyCustomSymbolPipe('en-US');
  private nativeElement: any;

  @Input() options
  /*
    {
      code @String
      symbol @String
      decimals @Boolean
    }
  */
  @HostListener('input', ['$event']) onChange = this.change.bind(this);
  @HostListener('focus', ['$event']) onFocus = this.focus.bind(this);
  @HostListener('blur', ['$event']) onBlur = this.blur.bind(this);

  constructor(private renderer: Renderer, private element: ElementRef) {
  }


  ngOnInit() {
    this.nativeElement = this.element.nativeElement;
  }

  public writeValue(obj: any) {
    this._value = (obj);
    if (obj) {
      this.renderer.setElementProperty(this.nativeElement, 'value', this.currency);
    } else {
      this.renderer.setElementProperty(this.nativeElement, 'value', obj);
    }
  }
  public registerOnChange(fn: any) {
    this.propagateChange = fn;
  }
  public registerOnTouched() { }
  private transform(value) {
    return this.currencyCustomSymbolPipe.transform(
      value,
      this.options.code,
      this.options.symbol,
      this.options.decimal,
      this.options.hideCurrency
    );
  }
  set _value(value) {
    const _value = value === '' ? NaN : +value;
    if (_.isNumber(_value) && !isNaN(_value)) {
      this.value = _value;
      this.currency = this.transform(_value);
    } else {
      this.value = '';
      this.currency = '';
    }

  }
  private change(event) {
    this._value = event.target.value;
    this.propagateChange(this.value);
  }
  private blur(event) {
    if (!this.value) {
      this.renderer.setElementProperty(this.nativeElement, 'value', this.value);
      return;
    };
    this.renderer.setElementProperty(this.nativeElement, 'value', this.currency);
  }
  private focus() {
    if (!this.value) { return; }
    this.renderer.setElementProperty(this.nativeElement, 'value', this.value);
  }
  private propagateChange = (_: any) => { };
}
