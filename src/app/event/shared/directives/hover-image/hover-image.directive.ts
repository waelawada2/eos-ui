import { Directive, ElementRef, HostListener, Renderer } from '@angular/core';

@Directive({
  selector: '[appHoverImage]'
})
export class HoverImageDirective {

  private element: ElementRef;
  private renderer: Renderer;
  private bigImage: ElementRef;

  constructor(el: ElementRef, renderer: Renderer) {
    this.element = el;
    this.renderer = renderer;
  }

  @HostListener('mouseover') onMouseOver() {
    const { src, alt } = this.element.nativeElement
    this.bigImage = this.renderer.createElement(document.body, 'img');
    this.renderer.setElementAttribute(this.bigImage, 'src', src)
    this.renderer.setElementAttribute(this.bigImage, 'alt', alt)
    this.renderer.setElementClass(this.bigImage, 'hovered-image', true);
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.renderer.detachView([this.bigImage])
  }

}
