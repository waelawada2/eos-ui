import { Injectable, Injector } from '@angular/core';
import AbstractService from '../abstract.service';

@Injectable()
export class EstimateService extends AbstractService<any> {

  constructor(injector: Injector) {
    super('estimate-values', injector);
    this.getElements();
  }
}
