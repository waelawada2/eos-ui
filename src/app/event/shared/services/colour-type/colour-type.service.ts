import { Injectable, Injector } from '@angular/core';
import AbstractService from '../abstract.service';

@Injectable()
export class ColourTypeService extends AbstractService<any> {

  constructor(injector: Injector) {
    super('colour-types', injector);
    this.getElements();
  }

}
