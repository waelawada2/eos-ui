import { Injectable, Injector } from '@angular/core';
import { GeneralConstants } from '../../constants';
import AbstractService, { ENVIRONMENT } from '../abstract.service';

@Injectable()
export class GendersService extends AbstractService<any> {

  constructor(injector: Injector) {
    super(GeneralConstants.ENDPOINTS.CLIENT.GENDERS, injector, ENVIRONMENT.CLIENT);
    this.getElements();
  }

}

