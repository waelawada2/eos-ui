import { Injectable, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { BUTTON_STATUS } from '../../components/navigation-header/navigation-header.button-status.interface';
import { SORTING } from '../../components/navigation-header/navigation-header.sorting.interface';

@Injectable()
export class NavigationService {

  navEvent: EventEmitter<string> = new EventEmitter();
  pageEvent: EventEmitter<string> = new EventEmitter();
  sortEvent: EventEmitter<number> = new EventEmitter();
  upwardsPageEvent: EventEmitter<number> = new EventEmitter();

  status: BehaviorSubject<any> = new BehaviorSubject({
    clear: BUTTON_STATUS.SHOWN,
    export: BUTTON_STATUS.HIDDEN,
    import: BUTTON_STATUS.SHOWN,
    new: BUTTON_STATUS.SHOWN,
    remove: BUTTON_STATUS.SHOWN,
    restore: BUTTON_STATUS.HIDDEN,
    save: BUTTON_STATUS.SHOWN,
    search: BUTTON_STATUS.SHOWN,
    sort: BUTTON_STATUS.HIDDEN,
  });

  constructor() { }

  emitNavChangeEvent(string) {
    this.navEvent.emit(string);
  }

  getNavChangeEmitter() {
    return this.navEvent;
  }

  emitSortingChangeEvent(value: SORTING) {
    this.sortEvent.emit(value)
  }

  getSortingChangeEmitter() {
    return this.sortEvent;
  }

  emitPageChangeEvent(string) {
    this.pageEvent.emit(string)
  }

  getPageChangeEmitter() {
    return this.pageEvent;
  }

  emitUpwardsPageChange(value: number) {
    this.upwardsPageEvent.emit(value)
  }

  enterEditMode(valid = false, canRemove = true, hasId = false) {
    const mode = {
      clear: BUTTON_STATUS.HIDDEN,
      export: hasId ? BUTTON_STATUS.HIDDEN : BUTTON_STATUS.SHOWN,
      import: BUTTON_STATUS.HIDDEN,
      new: BUTTON_STATUS.HIDDEN,
      remove: canRemove ? BUTTON_STATUS.SHOWN : BUTTON_STATUS.DISABLED,
      restore: !hasId ? BUTTON_STATUS.SHOWN : BUTTON_STATUS.HIDDEN,
      save: valid ? BUTTON_STATUS.SHOWN : BUTTON_STATUS.DISABLED,
      search: BUTTON_STATUS.SHOWN,
      sort: this.status.value.sort,
    }
    this.status.next(mode)
  }

  enterCreateMode(valid = false) {
    const mode = {
      clear: BUTTON_STATUS.HIDDEN,
      export: BUTTON_STATUS.HIDDEN,
      import: BUTTON_STATUS.HIDDEN,
      new: BUTTON_STATUS.HIDDEN,
      remove: BUTTON_STATUS.HIDDEN,
      restore: BUTTON_STATUS.HIDDEN,
      save: valid ? BUTTON_STATUS.SHOWN : BUTTON_STATUS.DISABLED,
      search: BUTTON_STATUS.SHOWN,
      sort: BUTTON_STATUS.HIDDEN,
    }
    this.status.next(mode)
  }

  enterSearchMode(ableToCreate = true) {
    const mode = {
      clear: BUTTON_STATUS.SHOWN,
      export: BUTTON_STATUS.HIDDEN,
      import: BUTTON_STATUS.SHOWN,
      new: ableToCreate ? BUTTON_STATUS.SHOWN : BUTTON_STATUS.HIDDEN,
      remove: BUTTON_STATUS.HIDDEN,
      restore: BUTTON_STATUS.HIDDEN,
      save: BUTTON_STATUS.HIDDEN,
      search: BUTTON_STATUS.SHOWN,
      sort: BUTTON_STATUS.HIDDEN,
    }
    this.status.next(mode);
  }

  enterCustomMode(mode) {
    this.status.take(1).subscribe({ next: response => this.status.next({ ...response, ...mode }) })
  }

  toggleSortArrows(show = false) {
    const state = show ? BUTTON_STATUS.SHOWN : BUTTON_STATUS.HIDDEN
    this.status.next({ ...this.status.value, sort: state })
  }

}
