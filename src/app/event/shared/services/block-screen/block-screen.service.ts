
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class BlockScreenService {

  public blocker: BehaviorSubject<boolean> = new BehaviorSubject(false);
  constructor() { }

  lockScreen() {
    this.blocker.next(true);
  }
  unlockScreen() {
    this.blocker.next(false);
  }

}
