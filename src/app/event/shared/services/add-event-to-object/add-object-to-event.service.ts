import { Injectable } from '@angular/core';

@Injectable()
export class AddObjectToEventService {

  public objectId;
  public event;
  public item;
  public creating = false;

  constructor() {

  }

  reset() {
    this.objectId = null;
    this.event = null;
    this.item = null;
    this.creating = false;
  }

}
