import { TestBed, inject } from '@angular/core/testing';

import { AuctionTermsServices } from './auction-terms.service';

describe('AuctionTermsServices', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuctionTermsServices]
    });
  });

  it('should be created', inject([AuctionTermsServices], (service: AuctionTermsServices) => {
    expect(service).toBeTruthy();
  }));
});
