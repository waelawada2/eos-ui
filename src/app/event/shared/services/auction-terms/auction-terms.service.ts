import { AuctionTerm } from '../../models';
import { Injectable, Injector } from '@angular/core';
import AbstractService from '../abstract.service';

@Injectable()
export class AuctionTermsServices extends AbstractService<AuctionTerm> {

  constructor(injector: Injector) {
    super('auction-terms', injector);
    this.getElements();
  }

}
