import { TestBed, inject } from '@angular/core/testing';

import { AddEventsToItemsService } from './add-events-to-items.service';

describe('AddEventsToItemsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddEventsToItemsService]
    });
  });

  it('should be created', inject([AddEventsToItemsService], (service: AddEventsToItemsService) => {
    expect(service).toBeTruthy();
  }));
});
