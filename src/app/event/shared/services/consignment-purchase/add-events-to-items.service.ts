import { Injectable, Injector } from '@angular/core';
import { ConsignmentPurchaseToItems } from '../../models';
import AbstractService from '../abstract.service';
import * as _ from 'lodash';


@Injectable()
export class AddEventsToItemsService extends AbstractService<ConsignmentPurchaseToItems> {

  item: any = {};
  formData: any = {};
  statusPage = '';

  constructor(injector: Injector) {
    super('items', injector);
  }

  reset() {
    this.item = {};
    this.formData = {};
    this.statusPage = '';
  }

}
