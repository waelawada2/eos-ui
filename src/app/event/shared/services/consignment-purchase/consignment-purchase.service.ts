import { Injectable, Injector } from '@angular/core';
import { ConsignmentPurchase } from '../../models';
import AbstractService from '../abstract.service';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash'

@Injectable()
export class ConsignmentPurchaseService extends AbstractService<ConsignmentPurchase> {

  constructor(injector: Injector) {
    super('events', injector);
  }

  public export(params: any = {}): Observable<any> {
    if (!_.has(params, 'type')) {
      params.type = ['CONSIGNMENT', 'PURCHASE'];
    }

    return super.export(params);
  }

}
