import { TestBed, inject } from '@angular/core/testing';

import { ConsignmentPurchaseService } from './consignment-purchase.service';

describe('ConsignmentPurchaseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConsignmentPurchaseService]
    });
  });

  it('should be created', inject([ConsignmentPurchaseService], (service: ConsignmentPurchaseService) => {
    expect(service).toBeTruthy();
  }));
});
