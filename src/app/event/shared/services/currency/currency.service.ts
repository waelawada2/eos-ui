import { Injectable, Injector } from '@angular/core';
import { Currency } from '../../models';
import AbstractService from '../abstract.service';

@Injectable()
export class CurrencyService extends AbstractService<any> {

  constructor(injector: Injector) {
    super('currencies', injector);
    this.getElements();
  }

}

