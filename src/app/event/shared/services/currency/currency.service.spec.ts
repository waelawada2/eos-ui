import { TestBed, inject } from '@angular/core/testing';
import { HttpModule } from '@angular/http';

import { CurrencyService } from './currency.service';

import { CURRENCIES } from './../../../mocks/currencies.mock';

describe('TransactionService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CurrencyService],
       imports: [ HttpModule ],
    });
  });

  it('should be created', inject([CurrencyService],
    (service: CurrencyService) => {
      expect(service).toBeTruthy();
  }));

  describe('should return the list of available currencies', () => {

  });

});
