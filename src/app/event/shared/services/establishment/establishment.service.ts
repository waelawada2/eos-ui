import { Injectable, Injector } from '@angular/core';
import AbstractService from '../abstract.service';

@Injectable()
export class EstablishmentService extends AbstractService<any> {

  constructor(injector: Injector) {
    super('establishments', injector);
    this.getElements();
  }

}
