import { Injectable, Injector } from '@angular/core';
import { Response } from '@angular/http';
import AbstractService from '../abstract.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/reduce';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import * as _ from 'lodash';

@Injectable()
export class PropertiesByEventsService extends AbstractService<any> {
  public resultFinal = [];

  constructor(injector: Injector) {
    super('events', injector);
  }

  public getElements(search, id: string = null) {
    const resultFinal: Array<any> = [];
    this.http
      .get(this.completeUrl, { params: search })
      .map((response: Response) => response.json())
      .concatMap((eventsResponse) => this
        .getItemsByEvents(eventsResponse)
        .reduce((acc, curr) => acc.concat(curr))
        .concatMap((itemResult) => {
          const items = _.get(itemResult, '_embedded.items', []);
          return Observable.from(items)
            .concatMap((item) =>
              this.getObjectsByItem(item)
                .reduce((acc, curr) => acc.concat(curr))
                .concatMap((objects) =>
                  Observable.of(objects)
                    .map((response: Response) => response.toString() !== '' ? response.json() : []))
            )
        })
      )
      .reduce((acc, actual) => {
        if (actual.toString() !== '') {
          acc.push(actual)
        }
        return acc;
      }, [])
      .subscribe((data) => {
        // TODO add pagination
        this.elements.next(data);
      }, (error) => {
        // TODO pending handling errors
        this.elements.next(null)
        this.error.next(error)
        this.page.next({
          number: 0,
          size: 0,
          totalElements: 0,
          totalPages: 0,
        })
      });
  }

  getItemsByEvents(data): Observable<any> {
    const itemsSearch = _.get(data, '_embedded.events')
    return itemsSearch
      .map((theEvent) => {
        return this.http
          .get(`${this.completeUrl}/${theEvent.id}/items`)
          .map((response: Response) => response.json())
      });
  }


  getObjectsByItem(data): Observable<any> {
    const objects = _.get(data, 'objects', []);
    if (objects && objects != null) {
      return objects
        .map((object) => {
          const url = `${this.environment.apiUrl}/properties/${object.objectId}`;
          return this.http.get(url)
        })
    } else {
      return Observable.of([])
    }
  }

}
