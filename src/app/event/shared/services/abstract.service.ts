import * as _ from 'lodash';
import { Injector } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { EVENTCONST } from '../../shared/constants';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { processParamsObject, exportRecursiveRequest, exportStatusRequest } from '../utils'
import { ConfigurationLoaderService } from 'app/configuration.service'

export enum ENVIRONMENT {
  CLIENT,
  EVENT,
  PROPERTY
}

export default abstract class AbstractService<T> {

  private headers: Headers;
  private baseUrl = '';
  protected completeUrl: string;
  protected http: Http;
  protected idName = 'id';

  protected currentSubscription: Subscription;
  protected environment: any;

  public elements: BehaviorSubject<T[]> = new BehaviorSubject([]);
  public element: BehaviorSubject<T> = new BehaviorSubject(null);
  public error: BehaviorSubject<any> = new BehaviorSubject(null);
  public page: BehaviorSubject<Paging> = new BehaviorSubject({
    number: 0,
    size: 0,
    totalElements: 0,
    totalPages: 0,
  });
  public links: BehaviorSubject<Links> = new BehaviorSubject(null)

  constructor(protected url: string, protected injector: Injector,
    protected useEnvironment: ENVIRONMENT = ENVIRONMENT.EVENT) {

    const configService = injector.get(ConfigurationLoaderService)
    this.http = injector.get(Http);
    this.environment = configService.getSettings();
    this.setHeaders();
    switch (useEnvironment) {
      case ENVIRONMENT.CLIENT:
        this.baseUrl = `${this.environment.clientApiUrl}/`;
        break;
      case ENVIRONMENT.PROPERTY:
        this.baseUrl = `${this.environment.apiUrl}/`;
        break;
      case ENVIRONMENT.EVENT:
      default:
        this.baseUrl = `${this.environment.eventApiUrl}/`;
        break;
    }
    this.completeUrl = this.baseUrl + this.url;
  }

  private setHeaders(): void {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.headers.append('Authorization', 'Bearer 111');
  }

  public getElements(searchParams: any = {}, id = null, optionalParams: any = {}): void {
    if (this.currentSubscription) {
      this.currentSubscription.unsubscribe();
    }
    const search = processParamsObject(searchParams);
    if (id) {
      this.currentSubscription = this.getHttpRequestById(id, optionalParams)
        .catch((response: Response) => Observable.throw(response))
        .subscribe((data) => {
          this.page.next({
            number: 0,
            size: 1,
            totalElements: 0,
            totalPages: 1,
          })
          this.elements.next(this.transformElements([data]))
          this.error.next(null)
          this.links.next(_.get(data, '_links'))
        }, (error) => {
          this.elements.next(null)
          this.error.next(error)
          this.page.next({
            number: 0,
            size: 0,
            totalElements: 0,
            totalPages: 0,
          })
        });
    } else {
      this.currentSubscription = this.http
        .get(this.completeUrl, { params: search })
        .map((response: Response) => response.json())
        .catch((response: Response) => Observable.throw(response))
        .subscribe((data) => {
          this.elements.next(this.setPagingAndGetElements(data))
        }, (error) => {
          this.elements.next(null)
          this.error.next(error)
          this.page.next({
            number: 0,
            size: 0,
            totalElements: 0,
            totalPages: 0,
          })
        });
    }
  }

  public getNextElements(requested = -1) {
    this.links.take(1).subscribe({
      next: value => {
        const size = this.page.value.size;
        const loadedElements = this.elements.value.length;
        let newSize = -1;
        if (requested > size + loadedElements) {
          newSize = requested - loadedElements + 1
        }

        if (value.next) {
          this.processNextQuery(value.next.href, newSize, this.page.value.number)
        }
      }
    })
  }

  protected processSinglePage(url) {
    this.http
      .get(url)
      .map((response: Response) => response.json())
      .catch((response: Response) => Observable.throw(response))
      .subscribe((data) => {
        (<Observable<T[]>>this.elements).take(1).subscribe({
          next: result => {
            this.elements.next(result.concat(this.setPagingAndGetElements(data)))
          }
        })
        this.error.next(null);
      }, (error) => {
        this.error.next(error)
      });
  }

  protected processNextQuery(url, size, actualPage) {
    const sizeReg = /size=\d*/;
    const pageReg = /page=\d*/
    const result = sizeReg.exec(url)
    const value = +result[0].substring(5, result[0].length)
    const nextRequestedElements = value * (actualPage + 2);
    if (size === -1 || nextRequestedElements >= size) {
      return this.processSinglePage(url);
    }

    const lastPage = Math.ceil(size / value);
    const urlArray = [];
    for (let i = actualPage + 1; i <= lastPage; i++) {
      urlArray.push(url.replace(pageReg, `page=${i}`))
    }
    let links = null;
    Observable.from(urlArray)
      .concatMap((elemUrl) => this.http.get(elemUrl)
        .map((response: Response) => response.json()))
      .map((element) => {
        links = _.get(element, '_links')
        return element
      })
      .reduce((acc, elem) => acc.concat(_.get(elem, ['_embedded', _.camelCase(this.url)])), [])
      .subscribe((elements) => {
        (<Observable<T[]>>this.elements).take(1).subscribe({
          next: (oldElements) => {
            const newElements = oldElements.concat(elements)
            this.elements.next(this.transformElements(newElements))
            this.error.next(null)
            this.page.next({ ...this.page.value, number: lastPage + 1 })
            this.links.next(links)
          }
        })
      })
  }

  public setPagingAndGetElements(data: any): T[] {
    this.error.next(null)
    this.links.next(_.get(data, '_links'))
    this.page.next(_.get(data, 'page'));
    return this.transformElements(_.get(data, ['_embedded', _.camelCase(this.url)]));
  }

  public transformElement(element: any): T {
    return <T>element;
  }

  public transformElements(elements: any[]): T[] {
    return _.map(elements, element => this.transformElement(element));
  }

  public getElement(itemId: string) {
    (<Observable<T[]>>this.elements).take(1).subscribe((v) => {
      const found = _.find(v, el => _.get(el, this.idName) === itemId)
      if (!found) {
        this.getHttpRequestById(itemId)
          .subscribe((data) => this.element.next(this.transformElement(data)), (error) => {
            this.element.next(null)
            this.error.next(error)
          });
      } else {
        this.element.next(found);
      }
    });
  }

  protected getHttpRequestById(id: string, optionalParams: any = {}): Observable<any> {
    return this.http
      .get(`${this.completeUrl}/${id}`, { params: optionalParams })
      .map((response: Response) => response.json())
      .catch((response: Response) => Observable.throw(response))
  }

  public getItemsByUrl(url, completeUrl = false, params: any = {}) {
    const newUrl = completeUrl ? this.baseUrl + url : url;
    this.http
      .get(newUrl, { params: processParamsObject(params) })
      .map((response: Response) => response.json())
      .catch((response: Response) => null)
      .subscribe((data) => {
        this.elements.next(this.setPagingAndGetElements(data));
      });
  }

  public amend(item: T, id: string, patch = false): Observable<any> {
    const procItem = processParamsObject(item);
    return this.http.put(`${this.completeUrl}/${id}`, procItem)
      .map((response: Response) => {
        if (response.status === 500) {
          return this._errorHandler(response);
        }
        this.updateLocalItem(JSON.parse(_.get(response, '_body')), id);
        return response;
      }).catch(this._errorHandler.bind(this));
  }

  public create(item: T): Observable<any> {
    const procItem = processParamsObject(item);
    this.elements.next([])
    return this.handleResponse(this.http.post(this.completeUrl, procItem));
  }

  private handleResponse(obs: Observable<Response>) {
    return obs.map((response: Response) => {
      if (response.status === 500 || response.status === 400) {
        return this._errorHandler(response);
      }
      return response;
    }).catch(this._errorHandler.bind(this))
  }

  public delete(itemId: string) {
    return this.http
      .delete(`${this.completeUrl}/${itemId}`)
      .map((response: Response) => {
        const _response = response.json() || {};
        if (response.status === 500) {
          return this._errorHandler(response);
        }
        if (response.status === 409) {
          return this._errorHandler(response);
        }
        if (response.status === 204) {
          _response.status = response.status;
          this.removeFromLocalData(itemId);
        }
        return _response;
      }).catch(this._errorHandler.bind(this));
  }

  public import(file: File, fileType: string): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    formData.append('type', fileType);

    const headers = new Headers();
    headers.append('Accept', 'application/json');

    return this.http.post(this.getImportUrl(), formData, new RequestOptions({ headers }))
      .catch(error => Observable.throw(error))
  }

  private getImportUrl() {
    return this.baseUrl + 'import';
  }

  private getExportUrl(url) {
    let _url = this.completeUrl;
    if (url) {
      _url = this.baseUrl + 'events/' + url + '/' + this.url;
    }
    return _url + '/export/';
  }

  public export(parameters: any = {}, url = ''): Observable<any> {
    const formData = new FormData();
    const statusRequestOptions = { http: this.http, baseUrl: this.baseUrl }
    _.map(_.omit(processParamsObject(parameters), 'size'), (value: any, key) => {

      if ((typeof value) === 'object') {
        _.map(value, (_value) => {
          formData.append(String(key), String(_value))
        })
      } else {
        formData.append(String(key), String(value))
      }
    })
    formData.append('size', String(this.page.value.totalElements));
    return this.http
      .post(this.getExportUrl(url), formData)
      .map((response: Response) => {
        return JSON.parse(_.get(response, '_body'));
      })
      .flatMap(mapResponse => exportStatusRequest(_.merge({}, statusRequestOptions, { response: mapResponse }))
        .expand((expandResponse) => {
          return exportRecursiveRequest(_.merge({}, { response: expandResponse }, statusRequestOptions))
        })
      )
  }

  private removeParameterFromUrl(url, paramToErase) {
    if (url) {
      const regex = new RegExp('[?&]' + paramToErase + '=([^&]*)', 'g')
      return url.replace(regex, '')
    }
    return url;
  }

  /* Removes from local data and updates page object **/
  private removeFromLocalData(itemId: string) {
    (<Observable<T[]>>this.elements).take(1).subscribe((currentElements: T[]) => {
      const newArr = _.remove(currentElements, el => _.get(el, this.idName) === itemId);
      if (newArr.length > 0) {
        this.elements.next(currentElements);
        this.page.take(1).subscribe(p => this.page.next({ ...p, totalElements: p.totalElements - 1 }))
      }
    });
  }

  protected updateLocalItem(item: any, id: string) {
    (<Observable<T[]>>this.elements).take(1).subscribe((currentElements: T[]) => {
      const index = _.findIndex(currentElements, (element) => _.get(element, this.idName) === id)
      if (index !== -1) {
        const newElems = _.clone(currentElements);
        newElems.splice(index, 1);
        newElems.unshift(this.transformElement(item))
        this.elements.next(newElems);
      }
    });
  }

  protected _errorHandler(_error: Response) {
    return Observable.throw(_error);
  }

}

export interface Paging {
  number: number,
  size: number,
  totalElements: number,
  totalPages: number,
}


interface Links {
  first?: Link,
  last?: Link,
  next?: Link,
  prev?: Link,
  self?: Link,
}


interface Link {
  href: string,
}
