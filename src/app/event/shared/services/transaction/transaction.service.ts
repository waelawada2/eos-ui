import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Transaction } from '../../models';
import AbstractService from '../abstract.service';

@Injectable()
export class TransactionService extends AbstractService<Transaction> {

  constructor(injector: Injector) {
    super('events', injector);
  }

  public getElements(searchParams: any = {}, id = null) {
    super.getElements({ ...searchParams, type: 'TRANSACTION' }, id);
  }

  public export(params: any = {}): Observable<any> {
    return super.export({ ...params, type: 'TRANSACTION' })
  }

}
