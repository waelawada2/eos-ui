
import { Injectable, Injector } from '@angular/core';
import AbstractService from '../abstract.service';

@Injectable()
export class TransactionStatusService extends AbstractService<any> {

  constructor(injector: Injector) {
    super('transaction-statuses', injector);
    this.getElements();
  };

}
