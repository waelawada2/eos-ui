import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TransactionService } from './transaction.service';
import AbstractService from '../abstract.service';

import * as _ from 'lodash';

@Injectable()
export class AuctionTransactionService extends TransactionService {

  constructor(injector: Injector) {
    super(injector);
  }
  public getElements(searchParams: any = {}, id = null) {
    super.getElements({ ...searchParams, type: 'TRANSACTION', transactionType: 'AUCTION' }, id);
  }

  public export(params: any = {}): Observable<any> {
    return super.export({ ...params, type: 'TRANSACTION', transactionType: 'AUCTION' })
  }
}
