import { TestBed, inject } from '@angular/core/testing';

import { TransactionStatusService } from './transaction-status.service';

describe('TransactionStatusService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TransactionStatusService]
    });
  });

  it('should be created', inject([TransactionStatusService], (service: TransactionStatusService) => {
    expect(service).toBeTruthy();
  }));
});
