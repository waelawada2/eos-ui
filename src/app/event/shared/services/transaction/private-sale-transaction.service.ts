import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { TransactionService } from './transaction.service';
import AbstractService from '../abstract.service';

@Injectable()
export class PrivateSaleTransactionService extends TransactionService {

  constructor(injector: Injector) {
    super(injector);
  }

  public getElements(searchParams: any = {}, id = null) {
    super.getElements({ ...searchParams, type: 'TRANSACTION', transactionType: 'PRIVATE_SALE' }, id);
  }

  public export(params: any = {}): Observable<any> {
    return super.export({ ...params, type: 'TRANSACTION', transactionType: 'PRIVATE_SALE' })
  }

}
