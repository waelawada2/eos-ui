import { Injectable, Injector } from '@angular/core';
import AbstractService from '../abstract.service';
import { TransactionItem } from '../../models';

@Injectable()
export class TransactionItemService extends AbstractService<any> {
  constructor(injector: Injector) {
    super('items', injector);
  };
}
