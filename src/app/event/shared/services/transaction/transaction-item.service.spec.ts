import { TestBed, inject } from '@angular/core/testing';

import { TransactionItemService } from './transaction-item.service';

describe('TransactionItemService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TransactionItemService]
    });
  });

  it('should be created', inject([TransactionItemService], (service: TransactionItemService) => {
    expect(service).toBeTruthy();
  }));
});
