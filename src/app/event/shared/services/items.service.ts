import { Injectable, Injector } from '@angular/core';
import { Http } from '@angular/http';
import AbstractService from './abstract.service';

@Injectable()
export class ItemsService extends AbstractService<any> {

  constructor(injector: Injector) {
    super('items', injector);
  }

}
