import { Injectable, Injector } from '@angular/core';
import { Department } from '../../models';
import AbstractService from '../abstract.service';

@Injectable()
export class DepartmentService extends AbstractService<Department> {

  constructor(injector: Injector) {
    super('departments', injector);
    this.getElements();
  }

}

