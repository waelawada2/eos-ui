import { Injectable, Injector } from '@angular/core';
import { GeneralConstants } from '../../constants';
import AbstractService from '../abstract.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PricingService extends AbstractService<any> {
  constructor(injector: Injector) {
    super('events', injector);
  }

  public getElements(searchParams: any = {}, id: string = null) {
    super.getElements({ ...searchParams, type: 'PRICING' }, id)
  }

  public export(params: any = {}): Observable<any> {
    return super.export({ ...params, type: 'PRICING' })
  }
}
