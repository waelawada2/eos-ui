import { Injectable, Injector } from '@angular/core';
import AbstractService from '../abstract.service';

@Injectable()
export class PricingItemService extends AbstractService<any> {

  constructor(injector: Injector) {
    super('items', injector);
  }
}
