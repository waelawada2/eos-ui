import { TestBed, inject } from '@angular/core/testing';

import { ClientSourceTypeService } from './client-source-type.service';

describe('ClientSourceTypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClientSourceTypeService]
    });
  });

  it('should be created', inject([ClientSourceTypeService], (service: ClientSourceTypeService) => {
    expect(service).toBeTruthy();
  }));
});
