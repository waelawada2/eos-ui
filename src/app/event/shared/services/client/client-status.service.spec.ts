import { TestBed, inject } from '@angular/core/testing';

import { ClientStatusService } from './client-status.service';

describe('ClientStatusService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClientStatusService]
    });
  });

  it('should be created', inject([ClientStatusService], (service: ClientStatusService) => {
    expect(service).toBeTruthy();
  }));
});
