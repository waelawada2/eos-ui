import { Injectable, Injector } from '@angular/core';
import { GeneralConstants } from '../../constants';
import { Segment } from '../../models';
import AbstractService, { ENVIRONMENT } from '../abstract.service';

@Injectable()
export class ClientSegmentService extends AbstractService<Segment> {

  constructor(injector: Injector) {
    super(GeneralConstants.ENDPOINTS.CLIENT.CLIENT_SEGMENT, injector, ENVIRONMENT.CLIENT);
    this.getElements();
  }

}
