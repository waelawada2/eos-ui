import { Injectable, Injector } from '@angular/core';
import { Response } from '@angular/http';
import { GeneralConstants } from '../../constants';
import { ExternalClient } from '../../models';
import { processParamsObject, cleanLinks } from '../../utils';
import { CLIENTSTATUS, EXTERNAL_CLIENT, KCM } from '../../mocks';
import AbstractService, { ENVIRONMENT } from '../abstract.service';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import * as _ from 'lodash';

@Injectable()
export class ExternalClientService extends AbstractService<ExternalClient> {

  constructor(injector: Injector) {
    super(GeneralConstants.ENDPOINTS.CLIENT.CLIENT, injector, ENVIRONMENT.CLIENT);
  }

  public transformElement(client: any): ExternalClient {
    return {
      id: _.get(client, 'entityId'),
      ..._.pick(client, ['city', 'country', 'entityId', 'level', 'name', 'region', 'state', 'fromClientSystem']),
      individual: {
        ..._.omit(_.get(client, 'individual'), 'gender'),
        gender: cleanLinks(_.get(client, 'individual.gender'))
      },
      status: cleanLinks(_.get(client, 'clientStatus')),
      kcm: cleanLinks(_.get(client, 'keyClientManager')),
      type: cleanLinks(_.get(client, 'clientType')),
      segment: cleanLinks(_.get(client, 'marketingSegment')),
      mainClient: _.get(client, 'mainClient')
    }
  }

  getClients(ids: string[]): Observable<any> {
    if (!ids) {
      return Observable.empty();
    }
    return Observable.concat(...ids.map(id => this.getClientRequest(id)));
  }

  getClientRequest(id: string) {
    return this.http
      .get(`${this.completeUrl}/${id}`)
      .map((response: Response) => response.json())
      .catch((response: Response) => Observable.empty());
  }

}
