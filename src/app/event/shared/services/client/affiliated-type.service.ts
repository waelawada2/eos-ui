import { Injectable, Injector } from '@angular/core';
import { GeneralConstants } from '../../constants';
import { AffiliatedType } from '../../models';
import AbstractService from '../abstract.service';

@Injectable()
export class AffiliatedTypeService extends AbstractService<AffiliatedType> {

  constructor(injector: Injector) {
    super(GeneralConstants.ENDPOINTS.CLIENT.AFFILIATED_TYPE, injector);
    this.getElements();
  }

}
