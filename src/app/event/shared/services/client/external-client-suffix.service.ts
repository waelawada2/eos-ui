import { Injectable, Injector } from '@angular/core';
import { GeneralConstants } from '../../constants';
import { ClientSuffix } from '../../models';
import AbstractService, { ENVIRONMENT } from '../abstract.service';

@Injectable()
export class ExternalClientSuffixService extends AbstractService<ClientSuffix> {

  constructor(injector: Injector) {
    super(GeneralConstants.ENDPOINTS.CLIENT.CLIENT_SUFFIX, injector, ENVIRONMENT.CLIENT);
    this.getElements();
  }
}
