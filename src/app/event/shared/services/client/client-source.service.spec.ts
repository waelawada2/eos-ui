import { TestBed, inject } from '@angular/core/testing';

import { ClientSourceService } from './client-source.service';

describe('ClientSourceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClientSourceService]
    });
  });

  it('should be created', inject([ClientSourceService], (service: ClientSourceService) => {
    expect(service).toBeTruthy();
  }));
});
