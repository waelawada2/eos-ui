import { TestBed, inject } from '@angular/core/testing';

import { AffiliatedTypeService } from './affiliated-type.service';

describe('AffiliatedTypeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AffiliatedTypeService]
    });
  });

  it('should be created', inject([AffiliatedTypeService], (service: AffiliatedTypeService) => {
    expect(service).toBeTruthy();
  }));
});
