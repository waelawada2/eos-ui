import { Injectable, Injector } from '@angular/core';
import { GeneralConstants } from '../../constants';
import { ClientStatus } from '../../models';
import AbstractService from '../abstract.service';

@Injectable()
export class ClientStatusService extends AbstractService<ClientStatus> {

  constructor(injector: Injector) {
    super(GeneralConstants.ENDPOINTS.CLIENT.CLIENT_STATUS, injector);
    this.getElements();
  }

}
