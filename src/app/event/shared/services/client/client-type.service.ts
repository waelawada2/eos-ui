import { Injectable, Injector } from '@angular/core';
import { GeneralConstants } from '../../constants';
import { ClientType } from '../../models';
import AbstractService, { ENVIRONMENT } from '../abstract.service';

@Injectable()
export class ClientTypeService extends AbstractService<ClientType> {

  constructor(injector: Injector) {
    super(GeneralConstants.ENDPOINTS.CLIENT.CLIENT_TYPE, injector, ENVIRONMENT.CLIENT);
    this.getElements();
  }

}
