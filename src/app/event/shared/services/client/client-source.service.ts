import { Injectable, Injector } from '@angular/core';
import { GeneralConstants } from '../../constants';
import { ClientSource } from '../../models';
import AbstractService from '../abstract.service';

@Injectable()
export class ClientSourceService extends AbstractService<ClientSource> {

  constructor(injector: Injector) {
    super(GeneralConstants.ENDPOINTS.CLIENT.CLIENT_SOURCE, injector);
    this.getElements();
  }

}
