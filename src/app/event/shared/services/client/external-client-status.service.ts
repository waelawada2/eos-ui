import { Injectable, Injector } from '@angular/core';
import { GeneralConstants } from '../../constants';
import { ClientStatus } from '../../models';
import AbstractService, { ENVIRONMENT } from '../abstract.service';

@Injectable()
export class ExternalClientStatusService extends AbstractService<ClientStatus> {

  constructor(injector: Injector) {
    super(GeneralConstants.ENDPOINTS.CLIENT.CLIENT_STATUS, injector, ENVIRONMENT.CLIENT);
    this.getElements();
  }

}
