import { Injectable, Injector } from '@angular/core';
import { GeneralConstants } from '../../constants';
import { KeyClientManager } from '../../models';
import AbstractService, { ENVIRONMENT } from '../abstract.service';

@Injectable()
export class KeyClientManagerService extends AbstractService<KeyClientManager> {

  constructor(injector: Injector) {
    super(GeneralConstants.ENDPOINTS.CLIENT.KCM, injector, ENVIRONMENT.CLIENT);
    this.getElements();
  }

}
