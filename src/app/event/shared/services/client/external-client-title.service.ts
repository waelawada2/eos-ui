import { Injectable, Injector } from '@angular/core';
import { GeneralConstants } from '../../constants';
import { ClientTitle } from '../../models';
import AbstractService, { ENVIRONMENT } from '../abstract.service';

@Injectable()
export class ExternalClientTitleService extends AbstractService<ClientTitle> {

  constructor(injector: Injector) {
    super(GeneralConstants.ENDPOINTS.CLIENT.CLIENT_TITLE, injector, ENVIRONMENT.CLIENT);
    this.getElements();
  }
}
