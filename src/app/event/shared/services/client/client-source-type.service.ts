import { Injectable, Injector } from '@angular/core';
import { GeneralConstants } from '../../constants';
import { ClientSourceType } from '../../models';
import AbstractService from '../abstract.service';

@Injectable()
export class ClientSourceTypeService extends AbstractService<ClientSourceType> {

  constructor(injector: Injector) {
    super(GeneralConstants.ENDPOINTS.CLIENT.CLIENT_SOURCE_TYPE, injector);
    this.getElements();
  }

}
