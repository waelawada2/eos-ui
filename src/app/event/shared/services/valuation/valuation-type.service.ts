import { Injectable, Injector } from '@angular/core';
import AbstractService from '../abstract.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ValuationTypeService extends AbstractService<any> {

  constructor(injector: Injector) {
    super('valuation-types', injector)
  }

  public getElements(searchParams: any = {}, id: string = null) {
    super.getElements(searchParams, id)
  }

  public export(params: any = {}): Observable<any> {
    return super.export({ ...params })
  }

}
