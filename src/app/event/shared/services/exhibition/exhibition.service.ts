import { Injectable, Injector } from '@angular/core';
import { Exhibition } from '../../models';
import AbstractService from '../abstract.service';

@Injectable()
export class ExhibitionService extends AbstractService<Exhibition> {

  constructor(injector: Injector) {
    super('events', injector);
  }

  public getElements(searchParams: any = {}, id: string = null) {
    searchParams.date = undefined
    super.getElements({ ...searchParams, type: 'EXHIBITION' }, id)
  }

  public transformElement(element: Exhibition): Exhibition {
    return element;
  }

}
