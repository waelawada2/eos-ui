import { Injectable, Injector } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Publication } from '../../models';
import AbstractService from '../abstract.service';
import { convertToAutoformatCompatibleObject, formatDate } from './../../utils';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

@Injectable()
export class PublicationService extends AbstractService<Publication> {

  constructor(injector: Injector) {
    super('events', injector);
  }

  public transformElement(element: Publication): Publication {
    const p: any = _.pick(element, ['author', 'edition', 'id', 'isbn', 'notes', 'code',
      'pages', 'publicationEvents', 'subTitle', 'title', 'type', 'verified', 'volume', '_links'])
    p.date = formatDate(element.date);
    p.publicationType = convertToAutoformatCompatibleObject(_.get(element, 'publicationType'), 'value');
    p.establishment = convertToAutoformatCompatibleObject(_.get(element, 'establishment'), 'name');
    p.exhibitionEvents = element.exhibitionEvents ? element.exhibitionEvents.filter(item => !!item) : []
    return p;
  }

  public export(params: any = {}): Observable<any> {
    return super.export({ ...params, type: 'PUBLICATION' })
  }

}
