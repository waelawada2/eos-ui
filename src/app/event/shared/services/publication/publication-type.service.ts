import { Injectable, Injector } from '@angular/core';
import AbstractService from '../abstract.service';

@Injectable()
export class PublicationTypeService extends AbstractService<any> {

  constructor(injector: Injector) {
    super('publication-types', injector);
    this.getElements();
  }

}
