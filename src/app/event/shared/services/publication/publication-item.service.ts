import { Injectable, Injector } from '@angular/core';
import AbstractService from '../abstract.service';
import * as _ from 'lodash';

@Injectable()
export class PublicationItemService extends AbstractService<any> {

  constructor(injector: Injector) {
    super('items', injector);
  }

}
