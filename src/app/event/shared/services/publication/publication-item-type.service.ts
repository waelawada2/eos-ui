import { Injectable, Injector } from '@angular/core';
import AbstractService from '../abstract.service';

@Injectable()
export class PublicationItemTypeService extends AbstractService<any> {

  constructor(injector: Injector) {
    super('publication-item-types', injector);
    this.getElements();
  }

}
