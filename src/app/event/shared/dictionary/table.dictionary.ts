export class TableDictionary {
  public static PRICING_LABELS = {
    'highEstimate': 'High',
    'lowEstimate': 'Low',
    'W': 'Withdrawn',
    'BI': 'BI',
    'hammer': 'Hammer',
    'premium': 'Premium',
    'unsoldHammer': 'Unsold Hammer',
    'netBuyer': 'Net Buyer',
    'netVendor': 'Net Vendor'
  }

  public static PUBLICATION = {
    CATALOGUING: 'Cataloguing',
    CLIENT: 'Client',
    FIGURE: 'Figure',
    ILLUSTRATION: 'Illustration',
    IMAGE: 'Image',
    NUMBER: 'Number',
    PAGE: 'Page',
    PLATE: 'Plate',
    TABLE_TITLE: 'Publication Items',
    DETAILS: 'Details',
    OBJECT: 'Object'
  }

  public static EXTERNAL_CLIENT = {
    KCM: 'Client KCM',
    ID: 'ID',
    MAIN_CLIENT_ID: 'Main Client ID',
    MAIN_CLIENT_PARTY: 'Main Client Party Name',
    PARTY: 'Party Name',
  }

  public static TRANSACTION = {
    TABLE_TITLE: 'Confirmed Lots',
  }
}
