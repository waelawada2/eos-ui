export default {
  VALIDATION: {
    TRANSACTION_TYPE: {
      required: 'Type is required.'
    },
    TITLE: {
      required: 'Title is required.'
    },
    VENUE: {
      required: 'Venue is required.'
    },
    DEPARTMENT: {
      required: 'Department is required.'
    },
    SALE_NUMBER: {
      required: 'sale Number is required.'
    },
    CURRENCY: {
      required: 'Currency is required.'
    },
    TOTAL_LOTS: {
      maxlength: 'Must be less than 99000'
    },
    NOTES: {
      maxlength: 'Notes cannot be more than 200'
    },
    INVALID_FORM_MESSAGE: 'Please fill in all required fields before submitting'
  },
  TRANSACTION: {
    UPDATE: {
      label: 'Success',
      message: 'Event transaction updated successfully',
    },
    SAVE: {
      title: 'Success',
      message: 'Event transaction saved successfully',
    },
    SEARCH: {
      noresult: {
        label: 'no results',
        message: 'no match found',
      }
    },
    DELETE: {
      label: 'Delete',
      message: 'Event was deleted sucessfully',
    },
  }
}
