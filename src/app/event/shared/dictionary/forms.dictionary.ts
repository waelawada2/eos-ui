export default {
  CLIENT: {
    LABELS: {
      AFFILIATED_TYPE: 'Affiliation Type',
      AFFILIATED_CLIENT_ID: 'Client ID',
      CLIENT_ID: 'ID',
      CLIENT_KCM: 'Client KCM',
      CLIENT_NAME: 'Client Name',
      CLIENT_NUMBER: 'Client Number',
      CLIENT_SOURCE_TYPE: 'Source Type',
      CLIENT_SOURCE: 'Source Name',
      CLIENT_STATUS: 'Status',
      CLIENT: 'Client',
      DISPLAY: 'Designation Text',
      KCM: 'KCM',
      MAIN_CLIENT: 'Main Client',
      MAIN_CLIENT_ID: 'Main Client ID',
      CLIENT_NOT_EDITABLE: '* This Client cannot be modified on EOS. Please refer to the Client System',
    },
    MESSAGES: {
      EMPTY_CLIENT_SOURCE_NAMES: 'No Source Name Found',
      LOADING_CLIENT_SOURCE_NAMES: 'Getting Source Name List..',
    },
    VALIDATION: {
      AFFILIATED_TYPE: {
        required: 'Affiliated type required',
        notValidObject: 'No Type Found',
      },
      CLIENT_ID: {
        required: 'Client Id required',
        maxlength: 'Client Id cannot be more than 10',
        pattern: 'Client Id must contain only numbers'
      },
      CLIENT_SOURCE_TYPE: {
        required: ' Source type required',
        notValidObject: 'No Source type Found',
      },
      CLIENT_SOURCE: {
        required: 'Source name required',
        notValidObject: 'No Source Name Found',
      },
      CLIENT_STATUS: {
        required: 'Client status required',
        notValidObject: 'No Status Found',
      },
    }
  },
  CONSIGNMENT_PURCHASE: {
    OWNERSHIP_TITLE: 'Ownership',
    AFFILIATED_TITLE: 'Affiliation',
    LABELS: {
      CONSIGMENT_PURCHASE: 'Consignment/Purchase',
      EVENT_ID: 'Event Id',
      NOTES: 'Notes',
    },
    MESSAGES: {
      SELECT_EVENT: ' Select an Event',
    },
    TABLE_TITLE: 'Currently Owned',
    TABLE_HEADERS: {
      CATALOGUING: 'Cataloguing',
      IMAGE: 'Image',
      VALUES: 'Values'
    },
    TITLE: 'Client Details',
  },
  ESTABLISHMENT: {
    LABELS: {
      CITY: 'City',
      COUNTRY: 'Country',
      ESTABLISHMENT: 'Establishment',
      REGION: ' Region',
      STATE: 'State',
    },
    MESSAGES: {
      LOADING_ESTABLISHMENT: 'Getting Establishment List..',
      EMPTY_ESTABLISHMENT: 'No Establishment Found',
    },
    VALIDATION: {
      required: 'Establishment is required.',
      notValidObject: 'No Establishment Found',
    },
  },
  EXTERNAL_CLIENT: {
    LABELS: {
      AGE: 'Age',
      CITY: 'City',
      CLIENT: 'Client',
      CLIENT_LOCATION: 'Client Location',
      CLIENT_ORIGIN: 'Client Origin',
      CLIENT_SEGMENT: 'Client Segment',
      CLIENT_TYPE: 'Client Type',
      COUNTRY: 'Country',
      DECEASED: 'Deceased',
      GENDER: 'Gender',
      KCM: 'KCM',
      LEVEL: 'Level',
      MAIN_CLIENT_ID: 'Main Client ID',
      MAIN_CLIENT_PARTY_NAME: 'Main Client Party Name',
      MAIN_CLIENT_TYPE: 'Main Client Type',
      MIDDLE_NAME: 'Middle Name',
      NAME: 'First Name',
      NUMBER: 'Client Number',
      ORIGIN: 'Origin',
      PARTY: 'Party Name',
      REGION: 'Region',
      SEGMENT: 'Segment',
      STATE: 'State',
      STATUS: 'Status',
      SUFFIX: 'Suffix',
      SURNAME: 'Last Name',
      TITLE: 'Title',
      TYPE: 'Type',
    },
    MESSAGES: {
      EMPTY_KCMS: 'No KCMs available',
      EMPTY_ORIGINS: 'No Origins Available',
      EMPTY_RESPONSE: 'No Clients Available',
      EMPTY_SEGMENTS: 'No Segments Available',
      EMPTY_STATUS: 'No Statuses Available',
      EMPTY_SUFFIXES: 'No Suffix Available',
      EMPTY_TITLES: 'no Title Available',
      EMPTY_TYPES: 'No Types available',
      LOADING_KCMS: 'Getting KCMs List',
      LOADING_ORIGINS: 'No Origins Available',
      LOADING_SEGMENTS: 'Loading Segment List',
      LOADING_STATUS: 'Loading Status List',
      LOADING_SUFFIXES: 'Getting Suffix List',
      LOADING_TITLES: 'Getting Titles List',
      LOADING_TYPES: 'Getting Type List',
    },
    VALUES: {
      DECEASED: {
        YES: 'Yes',
        NO: 'No',
      },
      GENDER: {
        FEMALE: 'Female',
        MALE: 'Male',
      }
    },
    VALIDATION: {
      PARTY: {
        required: 'Party Name is required',
      },
      DECEASED: {
        notValidObject: 'No Deceased Found',
      },
      GENDER: {
        notValidObject: 'No Gender Found',
      },
      KCM: {
        notValidObject: 'No KCM Found',
      },
      SEGMENT: {
        notValidObject: 'No Segment Found',
      },
      STATUS: {
        notValidObject: 'No Status Found',
      },
      LEVEL: {
        required: 'Level is required',
        range: 'Level should be 0 to 10',
      },
      TYPE: {
        notValidObject: 'No Type Found ',
        required: 'Type is required',
      },
      ORIGIN: {
        notValidObject: 'No Origin Found ',
      },
      MAIN_CLIENT_TYPE: {
        notValidObject: 'No Type Found ',
      },
      SURNAME: {
        required: 'Last Name is required',
        maxlength: `Last Name can't be more than 35 characters long`,
      },
      NAME: {
        maxlength: `First Name can't be more than 35 characters long`,
      },
      MIDDLE_NAME: {
        maxlength: `Middle Name can't be more than 35 characters long`,
      },
      CITY: {
        maxlength: `City can't be more than 30 characters long`,
      },
      STATE: {
        maxlength: `State can't be more than 150 characters long`,
      },
      REGION: {
        maxlength: `Region can't be more than 150 characters long`,
      },
      COUNTRY: {
        maxlength: `Country can't be more than 150 characters long`,
      },
    },
  },
  LABELS: {
    AUTHOR: 'Author',
    COMMENTS: 'Notes',
    DATE: 'Date',
    CONDITION: 'Condition',
    DESCRIPTION: ' Description',
    EVENT_ID: 'Event ID',
    EXHIBITION: 'Exhibition',
    ITEM_ID: 'Old Item ID',
    LITERATURE: 'Literature',
    NOTES: 'OD Notes',
    PROVENANCE: 'Provenance',
    SUB_TITLE: 'Subtitle',
    TITLE: 'Title',
    TYPE: 'Type',
  },
  LIST_INPUT: {
    ADD_BUTTON: 'Add element',
    ELEMENT_REPEATED: 'The selected element is already on the list',
    EMPTY_FIELD: 'No id was entered',
    ERROR: 'Error',
    NO_ELEMENT_FOUND: 'No element was found with the given value',
    SELF_ELEMENT: 'Cannot relate the same element',
  },
  MESSAGES: {
    EMPTY_PUBLICATION_TYPES: 'No Publication Type Found...',
    EMPTY_PUBLICATION_VERIFIED_FIELD: 'No valid Verified value selected',
    EMPTY_TRANSACTION_TYPES: 'No Transaction Type Found...',
    ERROR: 'Error',
    LOADING_PUBLICATION_TYPES: 'Getting Publication Type list...',
    LOADING_TRANSACTION_TYPES: 'Getting Transaction Type list...',
    NO_ELEMENTS_FOUND: 'No events found.',
    REMOVE_EVENT_DETAIL: 'Event was deleted successfully',
    SUCCESS_EVENT_DETAIL: 'Event saved successfully',
    SUCCESS: 'Success',
    UPDATE_EVENT_DETAIL: 'Event updated successfully',
  },
  PUBLICATION: {
    LABELS: {
      EDITION: 'Edi. No.',
      EXHIBITIONS: 'Associated Exhibition',
      ISBN: 'ISBN',
      PAGES: 'Pages',
      PUBLICATIONS: 'Associated Publications',
      VERIFIED_STATUS: 'Verification Status',
      VERIFIED: 'Verified',
      VOLUME: 'Volume',
      NOTES: 'notes',
      NOTES_ITEMS: 'notes_items',
    },
    FIELDS: {
      VERIFIED: 'Verified',
      NOT_VERIFIED: 'Not verified',
    },
    VALIDATION: {
      DATE: {
        required: 'Date is required'
      },
      AUTHOR: {
        maxlength: `Author can't be more than 100 characters long`,
      },
      EDITION: {
        maxlength: `Edition can't be more than 20 characters long`,
      },
      ISBN: {
        maxlength: `ISBN can't be more than 20 characters long`,
      },
      NOTES: {
        maxlength: `Notes can't be more than 1000 characters long`,
      },
      PAGES: {
        range: `Pages should be between 0 and 9000`,
      },
      PUBLICATION_TYPE: {
        required: `Publication Type is required`,
        notValidObject: 'No Publication type Found',
      },
      SUB_TITLE: {
        maxlength: `Notes can't be more than 100 characters long`,
      },
      TITLE: {
        maxlength: `Notes can't be more than 100 characters long`,
        required: `Title is required`,
      },
      VOLUME: {
        maxlength: `Notes can't be more than 10 characters long`,
      },
    }
  },
  PUBLICATION_ITEMS: {
    FURTHER_DETAILS_TITLE: 'Further Details',
    ITEM_CATALOGUING: 'Item Cataloguing',
    ITEM_DETAILS_TITLE: 'Item Details',
    LABELS: {
      CATALOGUING_PAGE: 'Cat. Page',
      COLOUR: ' Colour',
      EVENT_ID: 'Event ID',
      FIGURE: 'Figure',
      ILLUSTRATION_PAGE: 'Ill. Page',
      NUMBER: 'Number',
      PLATE: 'Plate',
    },
    MAIN_TITLE: 'Event Details',
    MESSAGES: {
      EMPTY_COLOURS: 'No Color Found',
      EMPTY_ITEM_TYPES: 'No Item Type Found',
      LOADING_DATA: 'Getting  List..',
      PUBLICATION_NOT_FOUND: 'No publication was found with the given Id',
    },
    OBJECTS: 'Objects',
    PUBLICATION_DETAILS_TITLE: 'Publication Details',
    REFERENCE_DETAILS_TITLE: 'Reference Details',
    VALIDATION: {
      CATALOGUING_PAGE: {
        maxlength: 'Cataloguing page cannot be more than 5 characters',
      },
      COLOUR: {
        maxlength: 'Colour cannot be more than 5 characters',
      },
      ILLUSTRATION_PAGE: {
        maxlength: 'Illustration page cannot be more than 5 characters',
      },
      PLATE: {
        maxlength: 'Plate cannot be more than 5 characters',
      },
      NUMBER: {
        maxlength: 'Number cannot be more than 5 characters',
      },
    },
  },
  PRICING_ITEMS: {
    FURTHER_DETAILS_TITLE: 'Further Details',
    ITEM_CATALOGUING: 'Item Cataloguing',
    ITEM_DETAILS_TITLE: 'Item Details',
    MAIN_TITLE: 'Event Details',
    OBJECTS: 'Objects',
  },
  TRANSACTION_ITEMS: {
    FIELDS: {
      EOR: 'Yes',
      NOT_EOR: 'No',
    },
    FURTHER_DETAILS_TITLE: 'Further Details',
    ITEM_CATALOGUING: 'Item Cataloguing',
    ITEM_DETAILS_TITLE: 'Item Details',
    LABELS: {
      LOT: 'Lot',
      EOR: 'EOR',
      LOW: 'Low',
      HIGH: 'High',
      TERM: 'Term',
      STATUS: 'Status',
      HAMMER: 'Hammer',
      PREMIUM: 'Premium',
      NET_BUYER: 'Net Buyer',
      NET_VENDOR: 'Net Vendor',
    },
    LOT_DETAILS_TITLE: 'Lot Details',
    MAIN_TITLE: 'Main Event',
    MESSAGES: {
      LOADING_TERM: 'Getting Term List..',
      EMPTY_TERM: 'No Term Found',
      LOADING_STATUS: 'Getting Status List..',
      EMPTY_STATUS: 'No Status Found',
    },
    TRANSACTION_DETAILS_TITLE: ' Details',
    VALIDATION: {
      LOT_NUMBER: {
        maxlength: `Lot number can't be more than 7 characters`,
      },
      STATUS: {
        notValidObject: 'Status not found'
      },
      TERM: {
        notValidObject: 'Term not found'
      }
    }
  },
  VALIDATION: {
    TRANSACTION_TYPE: {
      required: 'Transaction Type is required.',
      notValidObject: 'No Transaction type Found',
    },
    DATE: {
      required: 'Date is required'
    },
    TITLE: {
      required: 'Title is required.'
    },
    CURRENCY: {
      required: 'Currency is required.',
      notValidObject: 'No Currency Found',
    },
    DEPARTMENT: {
      required: 'Department is required.',
      notValidObject: 'No Department Found',
    },
    ITEM_ID: {
      maxlength: 'Item ID cannot be more than 20 characters long',
    },
    TOTAL_LOTS: {
      maxlength: 'Must be less than 99000'
    },
    NOTES: {
      maxlength: 'Notes cannot be more than 1000'
    },
    NOTES_ITEMS: {
      maxlength: 'Notes cannot be more than 200'
    },
    INVALID_FORM_MESSAGE: 'Please fill in all required fields before submitting',
    EVENT_TYPE: {
      required: 'Event required',
      notValidObject: 'No Event type Found',
    },
    GENERIC_FIELD: {
      required: 'This field is required',
      maxLength: 'This field excedes the max limit of characters',
      minLength: 'This field requires a minimum of characters',
      notValidObject: 'Select a valid value'
    },
    SALE_NUMBER: {
      required: 'Sale number is required.',
      range: `Sale Number should be between 0 and 99000`,
    },
  },
  VALUATION: {
    VALUATION_DATE: {
      required: 'Date is required',
    },
    VALUATION_ID: {
      maxlength: 'Valuation Id cannot be more than 20'
    },
    VALUATION_ORIGIN: {
      required: 'Origin is required',
      notValidObject: 'No Origin Found',
    },
    VALUATION_NOTES: {
      maxlength: 'Note cannot be more than 1000'
    },
    VALUATION_PROPOSAL: {
      maxlength: 'Proposal cannot be more than 100'
    },
    VALUATION_TYPE: {
      required: 'Type is required',
      notValidObject: 'No Valuation type Found',
    },

  },
  CURRENCY_ESTIMATE: {
    CURRENCY: {
      required: 'Currency is required',
    },
    estimate: {
      maxlength: 'High Estimate can \'t be more than 9 digits long',
      min: 'High Estimate value should be higher than 0',
    },
    estimateHigh: {
      maxlength: 'High Estimate can \'t be more than 9 digits long',
      min: 'High Estimate value should be higher than 0',
    }
  },
  NAVIGATION: {
    eventPage: 'Go to Event Page',
  },
}
