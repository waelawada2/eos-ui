export class StatisticsCons {
  public static PRIVATE_SALE = 'PRIVATE_SALE';
  public static AUCTION = 'AUCTION';
  public static defaults = {
    'totalLow': 'Total Low',
    'totalhigh': 'Total High'
  }
  public static privateSaleLabels = {
    'totalLow': 'Total Low',
    'totalHigh': 'Total High',
  }
  public static labels = {
    'lots>10': 'Lots > 10m',
    'lots<1<10': '1m < Lots < 10m',
    'lots<1': 'Lots < 1m',
    'lotsTotal': 'Lots Total',
    'totalLow': StatisticsCons.defaults.totalLow,
    'totalHigh': StatisticsCons.defaults.totalhigh
  }

}
