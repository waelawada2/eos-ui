
import { AbstractControl, ValidatorFn } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { GeneralConstants } from './constants';
import * as _ from 'lodash'

export const RangeValidator = (min: number, max: number): ValidatorFn => {
  return (control: AbstractControl): { [key: string]: any } => {
    const range = control.value >= min && control.value <= max;
    return !range ? { 'range': { value: control.value } } : null;
  };
}
export const ObjectValidator = (required: boolean = true): ValidatorFn => {
  return (control: AbstractControl): { [key: string]: any } => {
    const isValidObject = typeof control.value === 'object';
    return !isValidObject && required ? { 'notValidObject': true } : null;
  };
}

export const noRequiredObjectValidator = (required: boolean = false): ValidatorFn => {
  return (control: AbstractControl): { [key: string]: any } => {
    const value = control.value;
    const isValidObject = typeof value === 'object' && value;
    const isEmptyStr = !value || value === '';
    const errorObject = { 'notValidObject': true };
    const errorRequired = { 'required': true };
    let error = null;
    if (required) {
      if (isEmptyStr) {
        error = errorRequired;
      } else {
        if (!isValidObject) {
          error = errorObject;
        }
      }
    } else {
      if (!isEmptyStr && !isValidObject) {
        error = errorObject;
      }

    }
    return error;
  };
}
export const validateImportFileName = (entityType: string, type: string | string[], name: string): boolean => {
  let validName = true;
  if (typeof type === 'string') {
    validName = _.includes(_.toLower(name), _.toLower(type))
  } else {
    validName = _.some(type, (value) => _.includes(_.toLower(name), _.toLower(value)))
  }
  const validType = _.includes(_.toLower(name), _.toLower(entityType)) && _.endsWith(name, '.csv')

  return validType && validName;
}
