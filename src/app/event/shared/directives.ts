import { HoverImageDirective } from './directives/hover-image/hover-image.directive';

const SHARED_DIRECTIVES = [
  HoverImageDirective,
]

export {
  HoverImageDirective,
  SHARED_DIRECTIVES,
}
