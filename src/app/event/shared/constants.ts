// Export Constants
export { default as GeneralConstants } from './constants/general.constants';
export { default as FormsConstants } from './constants/forms.constants';
export { default as EVENTCONST } from './constants/events.constants';

// Export Dictionary
export { default as FormsDictionary } from './dictionary/forms.dictionary';
export { StatisticsCons } from './dictionary/statistics.dictionary';
export { default as GeneralDictionary } from './dictionary/general.dictionary';
export { TableDictionary } from './dictionary/table.dictionary';
export { default as DetailsDictionary } from './dictionary/details.dictionary';
