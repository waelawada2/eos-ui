export { PUBLICATIONS, PUBLICATION_TYPES, PUBLICATION_ITEMS, PUBLICATION_ITEM_OBJECTS } from './mocks/publication.mock';

export { EVENTCONSIGNMENTPURCHASE, TRANSACTION_ITEMS } from './mocks/consignment-purchase.mock';

export { CLIENTSOURCETYPE, CLIENTSTATUS, CLIENTSOURCE, AFFILIATEDTYPE, EXTERNAL_CLIENT, KCM } from './mocks/client.mock';

export { PRICING_EVENT, PRICING_ITEMS } from './mocks/pricing.mock';

export { transformData, randomlyAddFields, createName } from './mocks/autogenerate.mock';
