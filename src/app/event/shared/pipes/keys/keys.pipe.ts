import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'keys', pure: false })
export class KeysPipe implements PipeTransform {
  transform(value: any, args: any[] = null): any {
    return Object.keys(value)
  }
}

@Pipe({ name: 'CRNumberCheck' })
export class CRNumberPipe implements PipeTransform {
  transform(CRdata) {
    return CRdata.filter(CR => CR.number);
  }
}

@Pipe({ name: 'archiveNumberCheck' })
export class ArchiveNumberPipe implements PipeTransform {
  transform(archiveNumberData) {
    return archiveNumberData.filter(archive => archive.archiveNumber);
  }
}
