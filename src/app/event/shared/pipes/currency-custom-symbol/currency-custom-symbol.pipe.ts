import { Pipe, PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { GeneralConstants } from '../../constants';

@Pipe({
  name: 'currencyCustomSymbol'
})
export class CurrencyCustomSymbolPipe extends CurrencyPipe implements PipeTransform {

  transform(value: any, currencyCode: any, currencySymbol: any, fractions: any, hideCurrency = false): any {
    const CURRENCY_CONSTANTS = GeneralConstants.CURRENCY_DEFAULTS;
    if (typeof value !== 'number') { return value };
    let substringIndex = 1;
    const currencyFormattedValue = super.transform(value, (currencyCode || CURRENCY_CONSTANTS.currencyCode), true,
      !fractions ? CURRENCY_CONSTANTS.noFractions : CURRENCY_CONSTANTS.twofractions);
    if (currencyCode !== '' && currencyFormattedValue.indexOf(currencyCode) !== -1) {
      substringIndex = currencyCode.length;
    }
    let currencyValue = currencyCode || CURRENCY_CONSTANTS.currencyCode
    if (hideCurrency) {
      currencyValue = '';
    }
    return currencyValue + ' ' + currencyFormattedValue.substring(substringIndex);
  }

}
