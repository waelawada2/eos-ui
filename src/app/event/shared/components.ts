import EstablishmentFormComponent from './components/establishment.form/establishment.component'
import { AffiliatedClientComponent } from './components/affiliated-client/affiliated-client.component';
import { AlertComponent } from './components/dialogs/alert.component';
import { ClientFormComponent } from './components/client-form/client-form.component';
import { ClientComponent } from './components/client/client.component';
import { ClientDialogComponent } from './components/client/client.dialog.component';
import { ConfirmComponent } from './components/dialogs/dialog.component';
import { ContextMenuComponent } from './components/context-menu/context-menu.component';
import { CurrencyFormattedInputDirective } from '../shared/directives/currency-formatted-input/currency-formatted-input.directive';
import { CurrencyEstimateComponent } from './components/currency-estimate/currency-estimate.component';
import { ExternalClientFormComponent } from './components/external-client-form/external-client-form.component';
import { ItemCataloguingFormComponent } from './components/item-cataloguing-form/item-cataloguing.form';
import { ItemDetailsFormComponent } from './components/item-details-form/item-details.form';
import { ListInputFormComponent } from './components/list-input-form/list-input.component';
import { NavigationHeaderComponent } from './components/navigation-header/navigation-header.component';
import { ObjectComponent } from './components/table-object/object/object.component';
import { TableFilterComponent } from './components/table-filter/table-filter.component';
import { TableObjectComponent } from './components/table-object/table-object.component';
import { TwoLaneThumbsComponent } from './components/two-lane-thumbs/two-lane-thumbs.component';
import { ArtistFormComponent } from './components/artist-form/artist.component'
import { UploadDialogComponent } from './components/dialogs/upload-dialog.component';

const SHARED_COMPONENTS = [
  AffiliatedClientComponent,
  AlertComponent,
  ArtistFormComponent,
  ClientComponent,
  ClientDialogComponent,
  ClientFormComponent,
  ConfirmComponent,
  ContextMenuComponent,
  CurrencyEstimateComponent,
  CurrencyFormattedInputDirective,
  EstablishmentFormComponent,
  ExternalClientFormComponent,
  ItemCataloguingFormComponent,
  ItemDetailsFormComponent,
  ListInputFormComponent,
  NavigationHeaderComponent,
  ObjectComponent,
  UploadDialogComponent,
  TableFilterComponent,
  TableObjectComponent,
  TwoLaneThumbsComponent,
];

export {
  AffiliatedClientComponent,
  ArtistFormComponent,
  AlertComponent,
  ClientComponent,
  ClientDialogComponent,
  ClientFormComponent,
  ConfirmComponent,
  ContextMenuComponent,
  CurrencyEstimateComponent,
  CurrencyFormattedInputDirective,
  EstablishmentFormComponent,
  ExternalClientFormComponent,
  ItemCataloguingFormComponent,
  ItemDetailsFormComponent,
  ListInputFormComponent,
  NavigationHeaderComponent,
  ObjectComponent,
  UploadDialogComponent,
  TableFilterComponent,
  TableObjectComponent,
  TwoLaneThumbsComponent,
  SHARED_COMPONENTS,
}
