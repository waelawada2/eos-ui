import { CurrencyCustomSymbolPipe } from './pipes/currency-custom-symbol/currency-custom-symbol.pipe';
import { KeysPipe, CRNumberPipe, ArchiveNumberPipe } from './pipes/keys/keys.pipe';

const SHARED_PIPES = [
  CurrencyCustomSymbolPipe,
  KeysPipe,
  CRNumberPipe,
  ArchiveNumberPipe,
]

export {
  CurrencyCustomSymbolPipe,
  KeysPipe,
  SHARED_PIPES,
  CRNumberPipe,
  ArchiveNumberPipe,
}
