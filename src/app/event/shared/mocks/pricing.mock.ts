export const PRICING_EVENT = {
  '_embedded': {
    'events': [
      {
        'date': '17-08-2017 12:24:00',
        'eventId': 'EP-2X0PV41PWQ0',
        'notes': 'Notes about Transaction Event 3',
        'origin': 'GENERAL',
        'pricingType': 'REACTIVE',
        'proposal': '1231232',
        'type': 'PRICING',
        'valuationId': '1232132',
        'client': {
          'id': 'CL-1231312',
          'sourceType': 'EMPLOYEE',
          'sourceName': 1,
          'status': 1,
          'display': '232132',
        },
        'affiliatedClient': {
          'sourceType': 'EMPLOYEE',
          'sourceName': 2,
          'type': 2,
          'id': '321321'
        },
        '_links': {
          'self': {
            'href': 'http://localhost:9980/event-service/v1/events/EP-2X0PV41PWQ0'
          },
          'items': {
            'href': 'http://localhost:9980/event-service/v1/events/EP-2X0PV41PWQ0/items'
          }
        }
      }
    ]
  },
  '_links': {
    'self': {
      'href': 'http://localhost:9980/event-service/v1/events?page=0&size=20'
    }
  },
  'page': {
    'size': 20,
    'totalElements': 2,
    'totalPages': 1,
    'number': 0
  }
}

export const PRICING_ITEMS = {
  '_embedded': {
    'items': [
      {
        'itemId': 'IT-M875Q41Y62Z',
        'type': 'PRICING',
        'event': {
          'date': '17-08-2017 12:24:00',
          'eventId': 'EP-2X0PV41PWQ0',
          'notes': 'Notes about Transaction Event 3',
          'origin': '',
          'pricingType': '',
          'proposal': '',
          'type': 'PRICING',
          'valudationId': '',
          '_links': {
            'self': {
              'href': 'http://localhost:9980/event-service/v1/events/ET-869LKM5292K'
            }
          }
        },
        'objects': [
          {
              'objectId': 'OO-000HNKB3',
              '_links': {
                  'self': {
                      'href': 'http://localhost:9990/property-service/v1/properties/OO-000HNKB3'
                  }
              }
          },
          {
              'objectId': 'OO-00004BXC',
              '_links': {
                  'self': {
                      'href': 'http://localhost:9990/property-service/v1/properties/OO-00004BXC'
                  }
              }
          },
          {
              'objectId': 'OO-0000GNXC',
              '_links': {
                  'self': {
                      'href': 'http://localhost:9990/property-service/v1/properties/OO-0000GNXC'
                  }
              }
          },
          {
              'objectId': 'OO-0000YZLD',
              '_links': {
                  'self': {
                      'href': 'http://localhost:9990/property-service/v1/properties/OO-0000YZLD'
                  }
              }
          }
        ],
        'cataloguingNotes': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dapibus risus ante, vitae volutpat nunc aliquet eget. Sed orci tortor, porta vel venenatis ac, rhoncus quis purus. Nunc sit amet lorem tortor. Proin ut ligula purus. Integer massa nunc, vulputate ut sem nec, luctus aliquet libero. Praesent molestie felis et orci venenatis, eget gravida nisl lacinia. Morbi augue tortor, egestas ac pretium a, rhoncus id mi. Maecenas lacinia a erat eu pharetra. Suspendisse potenti. Morbi dictum velit nibh, quis pulvinar lorem ultricies eu. Curabitur tortor quam, sodales id convallis gravida, molestie vitae orci. Fusce interdum accumsan accumsan. Ut eget placerat dolor. Praesent placerat sem eu magna fermentum dapibus. Donec tempus egestas massa ac euismod. Phasellus eu risus luctus nunc mattis gravida in id quam.',
        'values': {
          'auction': {
            'lowEstimate': 5000000,
            'highEstimate': 20000000,
          },
          'ps': 10000000,
          'insurance': 10000000,
          'fmv': 10000000
        },
        'pipeline': {
          'auction': 'Sotheby\'s New York Evening Sale 15 June 2018',
          'ps': '',
          'exhibition': '',
        },
      },
      {
        'itemId': 'IT-M875Q41Y62Z',
        'type': 'PRICING',
        'event': {
          'date': '17-08-2017 12:24:00',
          'eventId': 'EP-2X0PV41PWQ0',
          'notes': 'Notes about Transaction Event 3',
          'origin': '',
          'pricingType': '',
          'proposal': '',
          'type': 'PRICING',
          'valudationId': '',
          '_links': {
            'self': {
              'href': 'http://localhost:9980/event-service/v1/events/ET-869LKM5292K'
            }
          }
        },
        'objects': [
          {
            'objectId': null,
            '_links': {
              'self': {
                'href': 'http://localhost:9990/property-service/v1/properties/null'
              }
            }
          },
          {
            'objectId': null,
            '_links': {
              'self': {
                'href': 'http://localhost:9990/property-service/v1/properties/null'
              }
            }
          }
        ],
        'cataloguingNotes': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dapibus risus ante, vitae volutpat nunc aliquet eget. Sed orci tortor, porta vel venenatis ac, rhoncus quis purus. Nunc sit amet lorem tortor. Proin ut ligula purus. Integer massa nunc, vulputate ut sem nec, luctus aliquet libero. Praesent molestie felis et orci venenatis, eget gravida nisl lacinia. Morbi augue tortor, egestas ac pretium a, rhoncus id mi. Maecenas lacinia a erat eu pharetra. Suspendisse potenti. Morbi dictum velit nibh, quis pulvinar lorem ultricies eu. Curabitur tortor quam, sodales id convallis gravida, molestie vitae orci. Fusce interdum accumsan accumsan. Ut eget placerat dolor. Praesent placerat sem eu magna fermentum dapibus. Donec tempus egestas massa ac euismod. Phasellus eu risus luctus nunc mattis gravida in id quam.',
        'values': {
          'auction': {
            'lowEstimate': 100000000,
            'highEstimate': 20000000,
          },
          'ps': 10000000,
          'insurance': 10000000,
          'fmv': 10000000
        },
        'pipeline': {
          'auction': '',
          'ps': 'Sotheby\'s New York Evening Sale 15 June 2018',
          'exhibition': '',
        },
      },
      {
        'itemId': 'IT-M875Q41Y62Z',
        'type': 'PRICING',
        'event': {
          'date': '17-08-2017 12:24:00',
          'eventId': 'EP-2X0PV41PWQ0',
          'notes': 'Notes about Transaction Event 3',
          'origin': '',
          'pricingType': '',
          'proposal': '',
          'type': 'PRICING',
          'valudationId': '',
          '_links': {
            'self': {
              'href': 'http://localhost:9980/event-service/v1/events/ET-869LKM5292K'
            }
          }
        },
        'objects': [
          {
            'objectId': null,
            '_links': {
              'self': {
                'href': 'http://localhost:9990/property-service/v1/properties/null'
              }
            }
          },
          {
            'objectId': null,
            '_links': {
              'self': {
                'href': 'http://localhost:9990/property-service/v1/properties/null'
              }
            }
          }
        ],
        'cataloguingNotes': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dapibus risus ante, vitae volutpat nunc aliquet eget. Sed orci tortor, porta vel venenatis ac, rhoncus quis purus. Nunc sit amet lorem tortor. Proin ut ligula purus. Integer massa nunc, vulputate ut sem nec, luctus aliquet libero. Praesent molestie felis et orci venenatis, eget gravida nisl lacinia. Morbi augue tortor, egestas ac pretium a, rhoncus id mi. Maecenas lacinia a erat eu pharetra. Suspendisse potenti. Morbi dictum velit nibh, quis pulvinar lorem ultricies eu. Curabitur tortor quam, sodales id convallis gravida, molestie vitae orci. Fusce interdum accumsan accumsan. Ut eget placerat dolor. Praesent placerat sem eu magna fermentum dapibus. Donec tempus egestas massa ac euismod. Phasellus eu risus luctus nunc mattis gravida in id quam.',
        'values': {
          'auction': {
            'lowEstimate': 100000000,
            'highEstimate': 20000000,
          },
          'ps': 10000000,
          'insurance': 10000000,
          'fmv': 10000000
        },
        'pipeline': {
          'auction': '',
          'ps': '',
          'exhibition': '',
        },
      },
      {
        'itemId': 'IT-M875Q41Y62Z',
        'type': 'PRICING',
        'event': {
          'date': '17-08-2017 12:24:00',
          'eventId': 'EP-2X0PV41PWQ0',
          'notes': 'Notes about Transaction Event 3',
          'origin': '',
          'pricingType': '',
          'proposal': '',
          'type': 'PRICING',
          'valudationId': '',
          '_links': {
            'self': {
              'href': 'http://localhost:9980/event-service/v1/events/ET-869LKM5292K'
            }
          }
        },
        'objects': [
          {
            'objectId': null,
            '_links': {
              'self': {
                'href': 'http://localhost:9990/property-service/v1/properties/null'
              }
            }
          }
        ],
        'cataloguingNotes': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dapibus risus ante, vitae volutpat nunc aliquet eget. Sed orci tortor, porta vel venenatis ac, rhoncus quis purus. Nunc sit amet lorem tortor. Proin ut ligula purus. Integer massa nunc, vulputate ut sem nec, luctus aliquet libero. Praesent molestie felis et orci venenatis, eget gravida nisl lacinia. Morbi augue tortor, egestas ac pretium a, rhoncus id mi. Maecenas lacinia a erat eu pharetra. Suspendisse potenti. Morbi dictum velit nibh, quis pulvinar lorem ultricies eu. Curabitur tortor quam, sodales id convallis gravida, molestie vitae orci. Fusce interdum accumsan accumsan. Ut eget placerat dolor. Praesent placerat sem eu magna fermentum dapibus. Donec tempus egestas massa ac euismod. Phasellus eu risus luctus nunc mattis gravida in id quam.',
        'values': {
          'auction': {
            'lowEstimate': 100000000,
            'highEstimate': 20000000,
          },
          'ps': 10000000,
          'insurance': 10000000,
          'fmv': 10000000
        },
        'pipeline': {
          'auction': '',
          'ps': '',
          'exhibition': '',
        },
      },
      {
        'itemId': 'IT-M875Q41Y62Z',
        'type': 'PRICING',
        'event': {
          'date': '17-08-2017 12:24:00',
          'eventId': 'EP-2X0PV41PWQ0',
          'notes': 'Notes about Transaction Event 3',
          'origin': '',
          'pricingType': '',
          'proposal': '',
          'type': 'PRICING',
          'valudationId': '',
          '_links': {
            'self': {
              'href': 'http://localhost:9980/event-service/v1/events/ET-869LKM5292K'
            }
          }
        },
        'objects': [
          {
            'objectId': null,
            '_links': {
              'self': {
                'href': 'http://localhost:9990/property-service/v1/properties/null'
              }
            }
          },
          {
            'objectId': null,
            '_links': {
              'self': {
                'href': 'http://localhost:9990/property-service/v1/properties/null'
              }
            }
          }
        ],
        'cataloguingNotes': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dapibus risus ante, vitae volutpat nunc aliquet eget. Sed orci tortor, porta vel venenatis ac, rhoncus quis purus. Nunc sit amet lorem tortor. Proin ut ligula purus. Integer massa nunc, vulputate ut sem nec, luctus aliquet libero. Praesent molestie felis et orci venenatis, eget gravida nisl lacinia. Morbi augue tortor, egestas ac pretium a, rhoncus id mi. Maecenas lacinia a erat eu pharetra. Suspendisse potenti. Morbi dictum velit nibh, quis pulvinar lorem ultricies eu. Curabitur tortor quam, sodales id convallis gravida, molestie vitae orci. Fusce interdum accumsan accumsan. Ut eget placerat dolor. Praesent placerat sem eu magna fermentum dapibus. Donec tempus egestas massa ac euismod. Phasellus eu risus luctus nunc mattis gravida in id quam.',
        'values': {
          'auction': {
            'lowEstimate': 100000000,
            'highEstimate': 20000000,
          },
          'ps': 10000000,
          'insurance': 10000000,
          'fmv': 10000000
        },
        'pipeline': {
          'auction': '',
          'ps': '',
          'exhibition': '',
        },
      },
      {
        'itemId': 'IT-M875Q41Y62Z',
        'type': 'PRICING',
        'event': {
          'date': '17-08-2017 12:24:00',
          'eventId': 'EP-2X0PV41PWQ0',
          'notes': 'Notes about Transaction Event 3',
          'origin': '',
          'pricingType': '',
          'proposal': '',
          'type': 'PRICING',
          'valudationId': '',
          '_links': {
            'self': {
              'href': 'http://localhost:9980/event-service/v1/events/ET-869LKM5292K'
            }
          }
        },
        'objects': [
          {
            'objectId': null,
            '_links': {
              'self': {
                'href': 'http://localhost:9990/property-service/v1/properties/null'
              }
            }
          },
          {
            'objectId': null,
            '_links': {
              'self': {
                'href': 'http://localhost:9990/property-service/v1/properties/null'
              }
            }
          }
        ],
        'cataloguingNotes': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dapibus risus ante, vitae volutpat nunc aliquet eget. Sed orci tortor, porta vel venenatis ac, rhoncus quis purus. Nunc sit amet lorem tortor. Proin ut ligula purus. Integer massa nunc, vulputate ut sem nec, luctus aliquet libero. Praesent molestie felis et orci venenatis, eget gravida nisl lacinia. Morbi augue tortor, egestas ac pretium a, rhoncus id mi. Maecenas lacinia a erat eu pharetra. Suspendisse potenti. Morbi dictum velit nibh, quis pulvinar lorem ultricies eu. Curabitur tortor quam, sodales id convallis gravida, molestie vitae orci. Fusce interdum accumsan accumsan. Ut eget placerat dolor. Praesent placerat sem eu magna fermentum dapibus. Donec tempus egestas massa ac euismod. Phasellus eu risus luctus nunc mattis gravida in id quam.',
        'values': {
          'auction': {
            'lowEstimate': 100000000,
            'highEstimate': 20000000,
          },
          'ps': 10000000,
          'insurance': 10000000,
          'fmv': 10000000
        },
        'pipeline': {
          'auction': '',
          'ps': '',
          'exhibition': '',
        },
      },
    ]
  },
  '_links': {
    'first': {
      'href': 'http://localhost:9980/event-service/v1/items?page=0&size=6'
    },
    'self': {
      'href': 'http://localhost:9980/event-service/v1/items?size=6'
    },
    'next': {
      'href': 'http://localhost:9980/event-service/v1/items?page=1&size=6'
    },
    'last': {
      'href': 'http://localhost:9980/event-service/v1/items?page=1&size=6'
    }
  },
  'page': {
    'size': 6,
    'totalElements': 9,
    'totalPages': 2,
    'number': 0
  }
}
