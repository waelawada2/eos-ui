export const EVENTCONSIGNMENTPURCHASE = {
  "_embedded": {
    "events": [
      {
        "eventId": "sample-consignment-id-mongobee",
        "type": "CONSIGNMENT",
        "date": "2017-09-29T15:45:46.342+0000",
        "notes": "Notes for the consignmente event",
        "mainClient": {
          "mainClientId": "0123456789",
          "clientSource": {
            "clientSourceId": "client-source-1",
            "name": "Edouard Benveniste",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-sources/client-source-1"
              }
            }
          },
          "clientSourceType": {
            "clientSourceTypeId": "source-type1-employee",
            "name": "Employee",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-source-types/source-type1-employee"
              }
            }
          },
          "clientStatus": {
            "clientStatusId": "S1",
            "name": "Active",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-statuses/S1"
              }
            }
          },
          "display": null
        },
        "affiliatedClient": {
          "affiliatedClientId": "9876543210",
          "clientSource": {
            "clientSourceId": "client-source-1",
            "name": "Edouard Benveniste",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-sources/client-source-1"
              }
            }
          },
          "clientSourceType": {
            "clientSourceTypeId": "source-type1-employee",
            "name": "Employee",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-source-types/source-type1-employee"
              }
            }
          },
          "affiliatedType": {
            "affiliatedTypeId": "AT1",
            "name": "Courtesy",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/affiliated-types/AT1"
              }
            }
          }
        },
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/events/sample-consignment-id-mongobee"
          },
          "items": {
            "href": "http://localhost:9980/event-service/v1/events/sample-consignment-id-mongobee/items"
          }
        }
      },
      {
        "eventId": "sample-purchase-id-mongobee",
        "type": "PURCHASE",
        "date": "2017-09-29T15:45:46.434+0000",
        "notes": "Notes for the purchase event",
        "mainClient": {
          "mainClientId": "0123456789",
          "clientSource": {
            "clientSourceId": "client-source-1",
            "name": "Edouard Benveniste",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-sources/client-source-1"
              }
            }
          },
          "clientSourceType": {
            "clientSourceTypeId": "source-type1-employee",
            "name": "Employee",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-source-types/source-type1-employee"
              }
            }
          },
          "clientStatus": {
            "clientStatusId": "S1",
            "name": "Active",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-statuses/S1"
              }
            }
          },
          "display": null
        },
        "affiliatedClient": {
          "affiliatedClientId": "9876543210",
          "clientSource": {
            "clientSourceId": "client-source-1",
            "name": "Edouard Benveniste",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-sources/client-source-1"
              }
            }
          },
          "clientSourceType": {
            "clientSourceTypeId": "source-type1-employee",
            "name": "Employee",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-source-types/source-type1-employee"
              }
            }
          },
          "affiliatedType": {
            "affiliatedTypeId": "AT1",
            "name": "Courtesy",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/affiliated-types/AT1"
              }
            }
          }
        },
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/events/sample-purchase-id-mongobee"
          },
          "items": {
            "href": "http://localhost:9980/event-service/v1/events/sample-purchase-id-mongobee/items"
          }
        }
      },
      {
        "eventId": "SP-541Z6NK2M5N",
        "type": "PURCHASE",
        "date": "2017-08-09T00:00:00.000+0000",
        "notes": "Purchase Event notes should not be more than 200 characters.",
        "mainClient": {
          "mainClientId": "0123456789",
          "clientSource": {
            "clientSourceId": "client-source-2",
            "name": "newspaper BBC News",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-sources/client-source-2"
              }
            }
          },
          "clientSourceType": {
            "clientSourceTypeId": "source-type1-employee",
            "name": "Employee",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-source-types/source-type1-employee"
              }
            }
          },
          "clientStatus": {
            "clientStatusId": "S1",
            "name": "Active",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-statuses/S1"
              }
            }
          },
          "display": "display Notes."
        },
        "affiliatedClient": {
          "affiliatedClientId": "0123456789",
          "clientSource": {
            "clientSourceId": "client-source-2",
            "name": "newspaper BBC News",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-sources/client-source-2"
              }
            }
          },
          "clientSourceType": {
            "clientSourceTypeId": "source-type2-media",
            "name": "Media",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-source-types/source-type2-media"
              }
            }
          },
          "affiliatedType": {
            "affiliatedTypeId": "AT1",
            "name": "Courtesy",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/affiliated-types/AT1"
              }
            }
          }
        },
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/events/SP-541Z6NK2M5N"
          },
          "items": {
            "href": "http://localhost:9980/event-service/v1/events/SP-541Z6NK2M5N/items"
          }
        }
      },
      {
        "eventId": "SC-PXK19Z8Z0LY",
        "type": "CONSIGNMENT",
        "date": "2017-08-09T00:00:00.000+0000",
        "notes": "Purchase Event notes should not be more than 200 characters.",
        "mainClient": {
          "mainClientId": "0123456789",
          "clientSource": {
            "clientSourceId": "client-source-2",
            "name": "newspaper BBC News",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-sources/client-source-2"
              }
            }
          },
          "clientSourceType": {
            "clientSourceTypeId": "source-type1-employee",
            "name": "Employee",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-source-types/source-type1-employee"
              }
            }
          },
          "clientStatus": {
            "clientStatusId": "S1",
            "name": "Active",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-statuses/S1"
              }
            }
          },
          "display": "display Notes."
        },
        "affiliatedClient": {
          "affiliatedClientId": "0123456789",
          "clientSource": {
            "clientSourceId": "client-source-2",
            "name": "newspaper BBC News",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-sources/client-source-2"
              }
            }
          },
          "clientSourceType": {
            "clientSourceTypeId": "source-type2-media",
            "name": "Media",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/client-source-types/source-type2-media"
              }
            }
          },
          "affiliatedType": {
            "affiliatedTypeId": "AT1",
            "name": "Courtesy",
            "_links": {
              "self": {
                "href": "http://localhost:9980/event-service/v1/affiliated-types/AT1"
              }
            }
          }
        },
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/events/SC-PXK19Z8Z0LY"
          },
          "items": {
            "href": "http://localhost:9980/event-service/v1/events/SC-PXK19Z8Z0LY/items"
          }
        }
      }
    ]
  },
  "_links": {
    "self": {
      "href": "http://localhost:9980/event-service/v1/events?type=CONSIGNMENT&type=PURCHASE"
    }
  },
  "page": {
    "size": 20,
    "totalElements": 4,
    "totalPages": 1,
    "number": 0
  }
}


export const TRANSACTION_ITEMS = {
  "_embedded": {
    "items": [
      {
        "itemId": "sample-item-id-mongobee-0",
        "type": "TRANSACTION",
        "event": null,
        "objects": [
          {
            "objectId": "sample-object-id-mongobee-0",
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/sample-object-id-mongobee-0"
              }
            }
          },
          {
            "objectId": "sample-object-id-mongobee-1",
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/sample-object-id-mongobee-1"
              }
            }
          },
          {
            "objectId": "sample-object-id-mongobee-2",
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/sample-object-id-mongobee-2"
              }
            }
          }
        ],
        "legacyId": "abcd123-mongobee",
        "comments": "Test Comments mongobee",
        "description": "Test description mongobee",
        "provenance": "Test provenance mongobee",
        "exhibition": "Test exhibition mongobee",
        "literature": "Test Literature mongobee",
        "condition": "Test Condition mongobee",
        "cataloguingNotes": "Test Notes mongobee",
        "transactionType": "AUCTION",
        "lotNumber": "1",
        "status": "SOLD",
        "consignmentEvent": null,
        "purchaseEvent": null,
        "auction": {
          "estimateOnRequest": true,
          "lowEstimate": 1000,
          "highEstimate": 2000,
          "term": "GUARANTEE",
          "hammer": 3000,
          "premium": 5000
        },
        "privateSale": null,
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/items/sample-item-id-mongobee-0"
          }
        }
      },
      {
        "itemId": "sample-item-id-mongobee-1",
        "type": "TRANSACTION",
        "event": null,
        "objects": [
          {
            "objectId": "sample-object-id-mongobee-0",
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/sample-object-id-mongobee-0"
              }
            }
          },
          {
            "objectId": "sample-object-id-mongobee-1",
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/sample-object-id-mongobee-1"
              }
            }
          },
          {
            "objectId": "sample-object-id-mongobee-2",
            "_links": {
              "self": {
                "href": "http://localhost:9990/property-service/v1/properties/sample-object-id-mongobee-2"
              }
            }
          }
        ],
        "legacyId": "abcd123-mongobee",
        "comments": "Test Comments mongobee",
        "description": "Test description mongobee",
        "provenance": "Test provenance mongobee",
        "exhibition": "Test exhibition mongobee",
        "literature": "Test Literature mongobee",
        "condition": "Test Condition mongobee",
        "cataloguingNotes": "Test Notes mongobee",
        "transactionType": "AUCTION",
        "lotNumber": "1",
        "status": "SOLD",
        "consignmentEvent": null,
        "purchaseEvent": null,
        "auction": {
          "estimateOnRequest": true,
          "lowEstimate": 1000,
          "highEstimate": 2000,
          "term": "GUARANTEE",
          "hammer": 3000,
          "premium": 5000
        },
        "privateSale": null,
        "_links": {
          "self": {
            "href": "http://localhost:9980/event-service/v1/items/sample-item-id-mongobee-1"
          }
        }
      },
    ]
  },
  "_links": {
    "self": {
      "href": "http://localhost:9980/event-service/v1/items"
    }
  },
  "page": {
    "size": 20,
    "totalElements": 2,
    "totalPages": 1,
    "number": 0
  },
}
