import * as _ from 'lodash';

function transformData(data) {
  data.establishmentName = _.get(data, 'establishment.name');
  data.city = _.get(data, 'establishment.cityName')
  return data
}

function createName(data) {
  if (!data) {
    return data
  }
  data.name = `
  ${_.get(data, 'establishment.name')}
  ${_.get(data, 'establishment.cityName')}
  ${_.get(data, 'title')}
  ${_.get(data, 'date')}
`
  return data
}

function randomlyAddFields(value) {
  if (!value || !value.id) {
    return value
  }
  if (!value.establishment) {
    value.establishment = generateEstablishment();
  }
  value.date = createRandomDate();
  value.date1 = createRandomDate();
  value.date2 = createRandomDate();
  value.title = createRandomTitle();
  return value;
}

function generateEstablishment() {
  const rand = Math.ceil(Math.random() * 100);

  return {
    code: `code-${rand}`,
    name: `establishment-${rand}`,
    cityName: createRandomCity(),
    id: rand,
  }
}

function createRandomCity() {
  const cities = ['Bogota', 'Cali', 'Medellin'];
  const rand = Math.floor(Math.random() * cities.length);
  return cities[rand];
}

function createRandomDate() {
  const start = new Date(new Date().setFullYear(new Date().getFullYear() - 1))
  const end = new Date();
  const newDate = new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  return newDate.toISOString().slice(0, 10)
}

function createRandomTitle() {
  const rand = Math.ceil(Math.random() * 100);
  return `Event-${rand}`
}

export { transformData, randomlyAddFields, createName };
