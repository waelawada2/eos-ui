export const PUBLICATIONS = {
  '_embedded': {
    'publications': [
      {
        'type': 'PUBLICATION',
        'eventId': 'XL470545K4Q',
        'title': 'Publication event 1',
        'date': '17-08-2017 12:24:00',
        'notes': 'Notes about Publication Event 3',
        'lastModified': 1505166379683,
        'publicationType': 'PRIVATE_SALE',
        'subTitle': null,
        'author': 'J.R.R. Tolkien',
        'publisher': {
          'venueId': 'sample-venue-id-mongobee0',
          'name': 'Venue 0',
          'cityCode': 'CLO',
          'cityName': 'Cali',
          'countryCode': 'CO',
          'countryName': 'Colombia',
          'state': 'FL',
          'region': 'South',
          '_links': {
            'self': {
              'href': 'http://localhost:9980/event-service/v1/venues/sample-venue-id-mongobee0'
            }
          }
        },
        'volume': null,
        'edition': null,
        'pages': 250,
        'isbn': null,
        'exhibition': null,
        'publications': null,
        '_links': {
          'self': {
            'href': 'http://localhost:9980/event-service/v1/events/XL470545K4Q'
          },
          'items': {
            'href': 'http://localhost:9980/event-service/v1/events/XL470545K4Q/items'
          }
        }
      },
      {
        'type': 'PUBLICATION',
        'eventId': 'BL470545K4Q',
        'title': 'Publication event 1',
        'date': '17-08-2017 12:24:00',
        'notes': 'Notes about Publication Event 3',
        'lastModified': 1505166379683,
        'publicationType': 'PRIVATE_SALE',
        'subTitle': null,
        'author': 'J.R.R. Tolkien',
        'publisher': {
          'venueId': 'sample-venue-id-mongobee0',
          'name': 'Venue 0',
          'cityCode': 'CLO',
          'cityName': 'Cali',
          'countryCode': 'CO',
          'countryName': 'Colombia',
          'state': 'FL',
          'region': 'South',
          '_links': {
            'self': {
              'href': 'http://localhost:9980/event-service/v1/venues/sample-venue-id-mongobee0'
            }
          }
        },
        'volume': null,
        'edition': null,
        'pages': 250,
        'isbn': null,
        'exhibition': null,
        'publications': null,
        '_links': {
          'self': {
            'href': 'http://localhost:9980/event-service/v1/events/XL470545K4Q'
          },
          'items': {
            'href': 'http://localhost:9980/event-service/v1/events/XL470545K4Q/items'
          }
        }
      },
      {
        'type': 'PUBLICATION',
        'eventId': 'AL470545K4Q',
        'title': 'Publication event 1',
        'date': '17-08-2017 12:24:00',
        'notes': 'Notes about Publication Event 3',
        'lastModified': 1505166379683,
        'publicationType': 'PRIVATE_SALE',
        'subTitle': null,
        'author': 'J.R.R. Tolkien',
        'publisher': {
          'venueId': 'sample-venue-id-mongobee0',
          'name': 'Venue 0',
          'cityCode': 'CLO',
          'cityName': 'Cali',
          'countryCode': 'CO',
          'countryName': 'Colombia',
          'state': 'FL',
          'region': 'South',
          '_links': {
            'self': {
              'href': 'http://localhost:9980/event-service/v1/venues/sample-venue-id-mongobee0'
            }
          }
        },
        'volume': null,
        'edition': null,
        'pages': 250,
        'isbn': null,
        'exhibition': null,
        'publications': null,
        '_links': {
          'self': {
            'href': 'http://localhost:9980/event-service/v1/events/XL470545K4Q'
          },
          'items': {
            'href': 'http://localhost:9980/event-service/v1/events/XL470545K4Q/items'
          }
        }
      },
    ]
  },
  '_links': {
    'self': {
      'href': 'http://localhost:9980/event-service/v1/currencies?page=0&size=20'
    }
  },
  'page': {
    'size': 20,
    'totalElements': 2,
    'totalPages': 3,
    'number': 0
  }
}


export const PUBLICATION_TYPES = [
  { publicationTypeId: '59c5590e987b9c41cab03ee3', value: 'Catalogue Raisonne' },
  { publicationTypeId: '2', value: 'Monograph Book' },
  { publicationTypeId: '3', value: 'Thematic Book' },
  { publicationTypeId: '4', value: 'Exhibition Catalogue' },
  { publicationTypeId: '5', value: 'Foundation Website' },
  { publicationTypeId: '6', value: 'Digital Catalogue Raisonne' },
  { publicationTypeId: '7', value: 'Newspaper' },
  { publicationTypeId: '8', value: 'Magazine' },
  { publicationTypeId: '9', value: 'Online Article' },
  { publicationTypeId: '10', value: 'Other' },
];

export const PUBLICATION_ITEMS = {
  '_embedded': {
      'items': [
          {
              'itemId': '59c43761e111224af57c578e',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 0',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43761e111224af57c578e'
                  }
              }
          },
          {
              'itemId': '59c43761e111224af57c578f',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 1',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43761e111224af57c578f'
                  }
              }
          },
          {
              'itemId': '59c43761e111224af57c5790',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 2',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43761e111224af57c5790'
                  }
              }
          },
          {
              'itemId': '59c43761e111224af57c5791',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 3',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43761e111224af57c5791'
                  }
              }
          },
          {
              'itemId': '59c43761e111224af57c5792',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 4',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43761e111224af57c5792'
                  }
              }
          },
          {
              'itemId': '59c43761e111224af57c5793',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 5',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43761e111224af57c5793'
                  }
              }
          },
          {
              'itemId': '59c43761e111224af57c5794',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 6',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43761e111224af57c5794'
                  }
              }
          },
          {
              'itemId': '59c43761e111224af57c5795',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 7',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43761e111224af57c5795'
                  }
              }
          },
          {
              'itemId': '59c43762e111224af57c5796',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 8',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43762e111224af57c5796'
                  }
              }
          },
          {
              'itemId': '59c43762e111224af57c5797',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 9',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43762e111224af57c5797'
                  }
              }
          },
          {
              'itemId': '59c43762e111224af57c5798',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 10',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43762e111224af57c5798'
                  }
              }
          },
          {
              'itemId': '59c43762e111224af57c5799',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 11',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43762e111224af57c5799'
                  }
              }
          },
          {
              'itemId': '59c43762e111224af57c579a',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 12',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43762e111224af57c579a'
                  }
              }
          },
          {
              'itemId': '59c43762e111224af57c579b',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 13',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43762e111224af57c579b'
                  }
              }
          },
          {
              'itemId': '59c43762e111224af57c579c',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 14',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43762e111224af57c579c'
                  }
              }
          },
          {
              'itemId': '59c43762e111224af57c579d',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 15',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43762e111224af57c579d'
                  }
              }
          },
          {
              'itemId': '59c43762e111224af57c579e',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 16',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43762e111224af57c579e'
                  }
              }
          },
          {
              'itemId': '59c43762e111224af57c579f',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 17',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43762e111224af57c579f'
                  }
              }
          },
          {
              'itemId': '59c43762e111224af57c57a0',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 18',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43762e111224af57c57a0'
                  }
              }
          },
          {
              'itemId': '59c43762e111224af57c57a1',
              'type': 'PUBLICATION',
              'event': {
                  'type': 'PUBLICATION',
                  'eventId': '59c430a4a97d8e39fa30762f',
                  'title': 'Pub Event Title 0',
                  'date': '21-09-2017 09:21:32',
                  'notes': 'Notes',
                  'lastModified': null,
                  '_links': {
                      'self': {
                          'href': 'http://localhost:9980/event-service/v1/events/59c430a4a97d8e39fa30762f'
                      }
                  }
              },
              'objects': null,
              'legacyId': null,
              'comments': null,
              'description': 'Publication Item Description for 19',
              'provenance': null,
              'exhibition': null,
              'literature': null,
              'condition': null,
              'cataloguingNotes': 'Notes',
              'publicationItemType': 'Object',
              'cataloguingPage': '12',
              'number': 1,
              'figure': 2,
              'plate': 10,
              'page': 23,
              'colour': 'COLOUR',
              'illustration': 3,
              '_links': {
                  'self': {
                      'href': 'http://localhost:9980/event-service/v1/items/59c43762e111224af57c57a1'
                  }
              }
          }
      ]
  },
  '_links': {
      'self': {
          'href': 'http://localhost:9980/event-service/v1/items'
      }
  },
  'page': {
      'size': 20,
      'totalElements': 20,
      'totalPages': 1,
      'number': 0
  }
}

export const PUBLICATION_ITEM_OBJECTS = [
  { imageUrl: 'imageUrl', cataloguing: 'Colour Illustration' },
  { imageUrl: 'imageUrl', cataloguing: 'B/W Image' }
]
