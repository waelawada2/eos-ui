export const CLIENTSOURCETYPE = {
  "_embedded": {
    "clientSourceTypes": [{
      "clientSourceTypeId": "source-type1-employee",
      "name": "Employee",
      "_links": {
        "self": {
          "href": "http://localhost:9980/event-service/v1/client-source-types/source-type1-employee"
        }
      }
    }, {
      "clientSourceTypeId": "source-type2-media",
      "name": "Media",
      "_links": {
        "self": {
          "href": "http://localhost:9980/event-service/v1/client-source-types/source-type2-media"
        }
      }
    }]
  },
  "_links": {
    "self": {
      "href": "http://localhost:9980/event-service/v1/client-source-types"
    }
  },
  "page": {
    "size": 20,
    "totalElements": 2,
    "totalPages": 1,
    "number": 0
  }
}


export const CLIENTSTATUS = {
  "_embedded": {
    "clientstatuses": [{
      "id": "S1",
      "name": "Active",
      "_links": {
        "self": {
          "href": "http://localhost:9980/event-service/v1/client-statuses/S1"
        }
      }
    }, {
      "id": "S2",
      "name": "Confidential",
      "_links": {
        "self": {
          "href": "http://localhost:9980/event-service/v1/client-statuses/S2"
        }
      }
    }, {
      "id": "S3",
      "name": "Ignore",
      "_links": {
        "self": {
          "href": "http://localhost:9980/event-service/v1/client-statuses/S3"
        }
      }
    }]
  },
  "_links": {
    "self": {
      "href": "http://localhost:9980/event-service/v1/client-statuses"
    }
  },
  "page": {
    "size": 20,
    "totalElements": 3,
    "totalPages": 1,
    "number": 0
  }
}

export const CLIENTSOURCE = {
  "_embedded": {
    "clientSources": [{
      "clientSourceId": "client-source-1",
      "name": "Edouard Benveniste",
      "_links": {
        "self": {
          "href": "http://localhost:9980/event-service/v1/client-sources/client-source-1"
        }
      }
    }, {
      "clientSourceId": "client-source-2",
      "name": "newspaper BBC News",
      "_links": {
        "self": {
          "href": "http://localhost:9980/event-service/v1/client-sources/client-source-2"
        }
      }
    }]
  },
  "_links": {
    "self": {
      "href": "http://localhost:9980/event-service/v1/client-sources"
    }
  },
  "page": {
    "size": 20,
    "totalElements": 2,
    "totalPages": 1,
    "number": 0
  }
}

export const AFFILIATEDTYPE = {
  "_embedded": {
    "affiliatedtypes": [{
      "affiliatedTypeId": "AT1",
      "name": "Courtesy",
      "_links": {
        "self": {
          "href": "http://localhost:9980/event-service/v1/affiliated-types/AT1"
        }
      }
    }, {
      "affiliatedTypeId": "AT2",
      "name": "Loan",
      "_links": {
        "self": {
          "href": "http://localhost:9980/event-service/v1/affiliated-types/AT2"
        }
      }
    }, {
      "affiliatedTypeId": "AT3",
      "name": "Partial Promise",
      "_links": {
        "self": {
          "href": "http://localhost:9980/event-service/v1/affiliated-types/AT3"
        }
      }
    }, {
      "affiliatedTypeId": "AT4",
      "name": "Promise",
      "_links": {
        "self": {
          "href": "http://localhost:9980/event-service/v1/affiliated-types/AT4"
        }
      }
    }, {
      "affiliatedTypeId": "AT5",
      "name": "Agent",
      "_links": {
        "self": {
          "href": "http://localhost:9980/event-service/v1/affiliated-types/AT5"
        }
      }
    }, {
      "affiliatedTypeId": "AT6",
      "name": "Gifted",
      "_links": {
        "self": {
          "href": "http://localhost:9980/event-service/v1/affiliated-types/AT6"
        }
      }
    }, {
      "affiliatedTypeId": "AT7",
      "name": "Acquired",
      "_links": {
        "self": {
          "href": "http://localhost:9980/event-service/v1/affiliated-types/AT7"
        }
      }
    }]
  },
  "_links": {
    "self": {
      "href": "http://localhost:9980/event-service/v1/affiliated-types"
    }
  },
  "page": {
    "size": 20,
    "totalElements": 7,
    "totalPages": 1,
    "number": 0
  }
}


export const EXTERNAL_CLIENT = {
  "_embedded": {
    "clients": [
      {
        "entityId": "NT-J3KL4R",
        "name": "Mr Test",
        "city": "new york",
        "state": "NY",
        "country": "usa",
        "region": "usa",
        "segment": 44,
        "level": 123,
        "individual": {
          "firstName": "John",
          "lastName": "Smith",
          "gender": "Male",
          "birthDate": "2011-12-18",
          "deceased": true
        },
        "_links": {
          "self": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "client": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "keyClientManager": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/keyClientManager"
          },
          "clientAssociations": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientAssociations"
          },
          "clientStatus": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientStatus"
          },
          "mainClient": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/mainClient"
          },
          "clientType": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientType"
          }
        }
      },
      {
        "entityId": "NT-J3KL4R",
        "name": "Mr Test",
        "city": "new york",
        "state": "NY",
        "country": "usa",
        "region": "usa",
        "segment": 44,
        "level": 123,
        "individual": {
          "firstName": "John",
          "lastName": "Smith",
          "gender": "Male",
          "birthDate": "2011-12-18",
          "deceased": true
        },
        "_links": {
          "self": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "client": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "keyClientManager": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/keyClientManager"
          },
          "clientAssociations": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientAssociations"
          },
          "clientStatus": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientStatus"
          },
          "mainClient": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/mainClient"
          },
          "clientType": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientType"
          }
        }
      },
      {
        "entityId": "NT-J3KL4R",
        "name": "Mr Test",
        "city": "new york",
        "state": "NY",
        "country": "usa",
        "region": "usa",
        "segment": 44,
        "level": 123,
        "individual": {
          "firstName": "John",
          "lastName": "Smith",
          "gender": "Male",
          "birthDate": "2011-12-18",
          "deceased": true
        },
        "_links": {
          "self": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "client": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "keyClientManager": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/keyClientManager"
          },
          "clientAssociations": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientAssociations"
          },
          "clientStatus": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientStatus"
          },
          "mainClient": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/mainClient"
          },
          "clientType": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientType"
          }
        }
      },
      {
        "entityId": "NT-J3KL4R",
        "name": "Mr Test",
        "city": "new york",
        "state": "NY",
        "country": "usa",
        "region": "usa",
        "segment": 44,
        "level": 123,
        "individual": {
          "firstName": "John",
          "lastName": "Smith",
          "gender": "Male",
          "birthDate": "2011-12-18",
          "deceased": true
        },
        "_links": {
          "self": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "client": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "keyClientManager": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/keyClientManager"
          },
          "clientAssociations": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientAssociations"
          },
          "clientStatus": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientStatus"
          },
          "mainClient": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/mainClient"
          },
          "clientType": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientType"
          }
        }
      },
      {
        "entityId": "NT-J3KL4R",
        "name": "Mr Test",
        "city": "new york",
        "state": "NY",
        "country": "usa",
        "region": "usa",
        "segment": 44,
        "level": 123,
        "individual": {
          "firstName": "John",
          "lastName": "Smith",
          "gender": "Male",
          "birthDate": "2011-12-18",
          "deceased": true
        },
        "_links": {
          "self": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "client": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "keyClientManager": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/keyClientManager"
          },
          "clientAssociations": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientAssociations"
          },
          "clientStatus": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientStatus"
          },
          "mainClient": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/mainClient"
          },
          "clientType": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientType"
          }
        }
      },
      {
        "entityId": "NT-J3KL4R",
        "name": "Mr Test",
        "city": "new york",
        "state": "NY",
        "country": "usa",
        "region": "usa",
        "segment": 44,
        "level": 123,
        "individual": {
          "firstName": "John",
          "lastName": "Smith",
          "gender": "Male",
          "birthDate": "2011-12-18",
          "deceased": true
        },
        "_links": {
          "self": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "client": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "keyClientManager": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/keyClientManager"
          },
          "clientAssociations": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientAssociations"
          },
          "clientStatus": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientStatus"
          },
          "mainClient": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/mainClient"
          },
          "clientType": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientType"
          }
        }
      },
      {
        "entityId": "NT-J3KL4R",
        "name": "Mr Test",
        "city": "new york",
        "state": "NY",
        "country": "usa",
        "region": "usa",
        "segment": 44,
        "level": 123,
        "individual": {
          "firstName": "John",
          "lastName": "Smith",
          "gender": "Male",
          "birthDate": "2011-12-18",
          "deceased": true
        },
        "_links": {
          "self": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "client": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "keyClientManager": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/keyClientManager"
          },
          "clientAssociations": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientAssociations"
          },
          "clientStatus": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientStatus"
          },
          "mainClient": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/mainClient"
          },
          "clientType": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientType"
          }
        }
      },
      {
        "entityId": "NT-J3KL4R",
        "name": "AAAA AAAA AAAA AAAA AAAA AAAA BBBBB",
        "city": "AAAAAAAAA",
        "state": "AAAAAAAAA",
        "country": "AAAAAAAAA",
        "region": "AAAAAAAAABBBBBBBBBBBBB",
        "segment": 12345678901234567890123456,
        "level": 123,
        "individual": {
          "firstName": "John",
          "lastName": "Smith",
          "gender": "Male",
          "birthDate": "2011-12-18",
          "deceased": true
        },
        "_links": {
          "self": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "client": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "keyClientManager": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/keyClientManager"
          },
          "clientAssociations": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientAssociations"
          },
          "clientStatus": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientStatus"
          },
          "mainClient": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/mainClient"
          },
          "clientType": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientType"
          }
        }
      },
      {
        "entityId": "NT-J3KL4R",
        "name": "Mr Test",
        "city": "new york",
        "state": "NY",
        "country": "usa",
        "region": "usa",
        "segment": 44,
        "level": 123,
        "individual": {
          "firstName": "John",
          "lastName": "Smith",
          "gender": "Male",
          "birthDate": "1982-12-06",
          "deceased": true
        },
        "_links": {
          "self": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "client": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "keyClientManager": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/keyClientManager"
          },
          "clientAssociations": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientAssociations"
          },
          "clientStatus": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientStatus"
          },
          "mainClient": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/mainClient"
          },
          "clientType": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientType"
          }
        }
      },
      {
        "entityId": "NT-J3KL4R",
        "name": "Mr Test",
        "city": "new york",
        "state": "NY",
        "country": "usa",
        "region": "usa",
        "segment": 44,
        "level": 123,
        "individual": {
          "firstName": "John",
          "lastName": "Smith",
          "gender": "Male",
          "birthDate": "2011-12-18",
          "deceased": false
        },
        "_links": {
          "self": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "client": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "keyClientManager": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/keyClientManager"
          },
          "clientAssociations": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientAssociations"
          },
          "clientStatus": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientStatus"
          },
          "mainClient": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/mainClient"
          },
          "clientType": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientType"
          }
        }
      },
      {
        "entityId": "NT-J3KL4R",
        "name": "Mr Test",
        "city": "new york",
        "state": "NY",
        "country": "usa",
        "region": "usa",
        "segment": 44,
        "level": 123,
        "individual": {
          "firstName": "John",
          "lastName": "Smith",
          "gender": "Male",
          "birthDate": "2011-12-18",
          "deceased": true
        },
        "_links": {
          "self": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "client": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R"
          },
          "keyClientManager": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/keyClientManager"
          },
          "clientAssociations": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientAssociations"
          },
          "clientStatus": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientStatus"
          },
          "mainClient": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/mainClient"
          },
          "clientType": {
            "href": "http://localhost:8080/client-service/v1/clients/NT-J3KL4R/clientType"
          }
        }
      },

    ]
  },
  "_links": {
    "self": {
      "href": "http://localhost:8080/client-service/v1/clients{?page,size,sort}",
      "templated": true
    },
    "profile": {
      "href": "http://localhost:8080/client-service/v1/profile/clients"
    },
    "search": {
      "href": "http://localhost:8080/client-service/v1/clients/search"
    }
  },
  "page": {
    "size": 20,
    "totalElements": 1,
    "totalPages": 1,
    "number": 0
  }
}


export const KCM = {
  _embedded: {
    kcms: [{
      "firstName": "AAAA AAAA",
      "lastName": "AAAAA AAAABBBBBBBBBB",
      "_links": {
        "self": {
          "href": "http://localhost:8080/client-service/v1/key-client-managers/7"
        },
        "keyClientManager": {
          "href": "http://localhost:8080/client-service/v1/key-client-managers/7"
        }
      }
    },
    ]
  }
}
