import { Component, Input, OnDestroy, OnInit, AfterViewInit, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ArtistService } from '../../../../artist/artist.service';
import { Subscription } from 'rxjs/Subscription';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import * as _ from 'lodash';

@Component({
  selector: 'app-artist-form',
  templateUrl: './artist.component.html',
  providers: [ArtistService],
  styleUrls: [
    '../../styles/forms.scss',
    './artist.component.scss',
  ],
})
export class ArtistFormComponent implements OnDestroy, OnInit, AfterViewInit {

  public artistMap;
  public errorMsg;
  private artistForm: FormGroup;
  private selectedValue;
  private artistSubscription: Subscription;
  private emptyValue = true;

  @Output() artistSelected: EventEmitter<any> = new EventEmitter();

  constructor(private artistService: ArtistService, private formBuilder: FormBuilder, private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.artistForm = this.formBuilder.group({
      artistData: ['']
    });
  }

  ngAfterViewInit() {
    this.artistSubscription = this.artistForm.get('artistData').valueChanges.subscribe((artist) => {
      if (typeof artist === 'object') {
        this.selectedValue = artist;
        this.emptyValue = false;
      } else {
        this.emptyValue = (artist.length === 0);
        this.selectedValue = null
      }
      this.artistSelected.emit(this.selectedValue);
    })
  }

  ngOnDestroy() {
    this.artistSubscription.unsubscribe();
  }

  nameListFormatter = (data: any) => {
    this.artistMap = '';
    const birthHtml = `<i class='fa fa-birthday-cake' aria-hidden='true'></i> ${data.birthYear}`;
    const deathHtml = `<i class='fa fa-bed' aria-hidden='true'> </i> ${data.deathYear}`;
    const nationalityHtml = `<i class='fa fa-flag-o' aria-hidden='true'> </i> ${data.nationality}`;

    if (data.birthYear && data.deathYear && data.nationality) {
      this.artistMap = `<span>${birthHtml} &nbsp;&nbsp;&nbsp; ${deathHtml} &nbsp;&nbsp;&nbsp; ${nationalityHtml} </span>`
    } else if (data.birthYear && data.nationality) {
      this.artistMap = `<span>${birthHtml} &nbsp;&nbsp;&nbsp; ${nationalityHtml} </span>`
    } else if (data.deathYear && data.nationality) {
      this.artistMap = `<span>${deathHtml} &nbsp;&nbsp;&nbsp; ${nationalityHtml} </span>`
    } else if (data.birthYear && data.deathYear) {
      this.artistMap = `<span> ${birthHtml} &nbsp;&nbsp;&nbsp; ${deathHtml} </span>`
    } else if (data.birthYear) {
      this.artistMap = `<span> ${birthHtml}`
    }
    const html = `<span class="auto-title">` + data.firstName + `</span><br/>` + this.artistMap;
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }

  observableSource = (value: any): Observable<any[]> => {
    const query = this.getQueryFromInput(value);

    if (query !== 'undefined' && query !== '') {
      return this.artistService.getObservableArtist(query).map((response: Response) => {

        const values = response.json().content;
        return this.formatArtistResponse(values);

      }, resFileError => this.errorMsg = resFileError)
    } else {
      return Observable.of([]);
    }
  }


  getQueryFromInput = (value: any): String => {
    let artistQuery = '';

    if (value) {
      value = value.split(' ');
      if (value[0] && value[1]) {
        artistQuery = `firstName=` + value[0] + `&lastName=` + value[1] + `&lod=MAX`;
      } else if (value[0]) {
        artistQuery = `name=` + value[0] + `&lod=MAX`;
      }
    }

    return artistQuery;
  }

  formatArtistResponse = (response: any): any[] => {
    const artistName = [];

    response.forEach(data => {
      let countryNames = '';
      for (let i = 0; i <= 2; i++) {
        if (data.countries[i] && data.countries[i].countryName && i === 0) {
          countryNames = data.countries[i].countryName;
        } else if (data.countries[i] && data.countries[i].countryName) {
          countryNames = countryNames + ', ' + data.countries[i].countryName;
        }
      }
      artistName.push({
        firstName: data.firstName + ' ' + data.lastName,
        birthYear: data.birthYear,
        deathYear: data.deathYear,
        nationality: countryNames,
        id: data.id
      });
    })

    return artistName;
  }

  resetSelectedValue = (): void => {
    this.artistForm.get('artistData').setValue('');
  }

  valueChanged = (artist: any): void => {
    this.selectedValue = artist;
    this.artistSelected.emit(artist);
  }

  clearForm = (): void => {
    this.artistForm.get('artistData').setValue('');
    this.selectedValue = null;
  }

}

