import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';

import { FormsDictionary } from '../../constants';


@Component({
  selector: 'app-item-cataloguing-form',
  templateUrl: './item-cataloguing.form.html',
  styleUrls: ['../../styles/forms.scss'],
})
export class ItemCataloguingFormComponent implements OnInit {

  dictionary = FormsDictionary;
  form: FormGroup;
  formErrors: any = {};

  @Input()
  parentForm: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      description: ['', Validators.maxLength(10000)],
      provenance: ['', Validators.maxLength(10000)],
      exhibition: ['', Validators.maxLength(10000)],
      literature: ['', Validators.maxLength(10000)],
      condition: ['', Validators.maxLength(10000)],
      notes: ['', Validators.maxLength(10000)],
    });

    this.parentForm.addControl('cataloguing', this.form)
  }

}
