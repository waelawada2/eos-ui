import { async, ComponentFixture, ComponentFixtureAutoDetect, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ItemCataloguingFormComponent } from './item-cataloguing.form'
import { FormsModule, ReactiveFormsModule, FormControl, FormGroup } from '@angular/forms';

describe('ItemCataloguingForm', () => {
  let component: ItemCataloguingFormComponent;
  let fixture: ComponentFixture<ItemCataloguingFormComponent>;
  const str2000 = Array(2002).join('a');
  const str200 = Array(2002).join('a');

  const fb: FormBuilder = new FormBuilder();


  function getParentForm() {
    return fb.group({
    });
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ItemCataloguingFormComponent],
      schemas : [ CUSTOM_ELEMENTS_SCHEMA ],
      imports: [ FormsModule, ReactiveFormsModule ],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemCataloguingFormComponent);
    component = fixture.componentInstance;
    component.parentForm = getParentForm();
    component.ngOnInit();
  })

  it('Should Form be valid when empty', () => {
    const form = component.form;
    expect(form.valid).toBe(true);
    fixture.detectChanges();
  });

  it('Should form be invalid when description is longer than 2000 characters', () => {
    const form = component.form;
    form.patchValue({ description: str2000 });
    expect(form.valid).toBe(false);
    fixture.detectChanges();
  });

  it('Should form be invalid when provenance is longer than 2000 characters', () => {
    const form = component.form;
    form.patchValue({ provenance: str2000, description: '' });
    expect(form.valid).toBe(false);
    fixture.detectChanges();
  });

  it('Should form be invalid when literature is longer than 2000 characters', () => {
    const form = component.form;
    form.patchValue({ literature: str2000, provenance: '' });
    expect(form.valid).toBe(false);
    fixture.detectChanges();
  });

  it('Should form be invalid when notes is longer than 200 characters', () => {
    const form = component.form;
    form.patchValue({ provenance: '', notes: str200 });
    expect(form.valid).toBe(false);
    fixture.detectChanges();
  });

});
