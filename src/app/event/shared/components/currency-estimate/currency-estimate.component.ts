import { AfterViewInit, Component, OnInit, Input, Output, OnDestroy, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DecimalPipe } from '@angular/common';
import { GeneralDictionary, FormsDictionary, GeneralConstants } from './../../constants';
import 'rxjs/add/operator/debounceTime';
import {
  autocompleListFormatter, convertToAutoformatCompatibleObject,
  updateValueAndValidity, setValidators
} from './../../utils';
import { noRequiredObjectValidator } from '../../validators';
import { CurrencyService, EstimateService } from '../../services';
import { Currency } from '../../models';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import * as _ from 'lodash';

@Component({
  providers: [CurrencyService, EstimateService],
  selector: 'app-currency-estimate',
  templateUrl: './currency-estimate.component.html',
  styleUrls: [
    './../../styles/forms.scss',
    './currency-estimate.component.scss'
  ],
})
export class CurrencyEstimateComponent implements AfterViewInit, OnInit, OnDestroy {
  dictionary = FormsDictionary;
  estimateSubscription: Subscription;
  auctionCurrencyFieldSubscription: Subscription;
  estimateLowfieldsubscription: Subscription;
  estimateHighFieldSubscription: Subscription;
  currencies: any[];
  currencySubscription: Subscription;
  defaultDisplayOptions = { estimateHigh: false, label: '' }
  emptySymbol = { code: '', symbol: '', hideCurrency: true }
  estimate: any;
  estimateHigh: any;
  estimates: any[] = [];
  form: FormGroup;
  formSubscription: Subscription;
  formErrors: any = [];
  formRequired: any = {};
  formLabel: string;
  displayOptions: any = {};
  id: string = 'currencyEstimate-' + _.random(0, 100);
  labels = GeneralDictionary.FORM_LABELS;
  required: any;
  pricingConstants = GeneralConstants.PRICING_EVENT

  @Input() set validation(validate) {
    this.required = validate
  }

  @Input() set setOptions(_displayOptions) {
    this.displayOptions = _displayOptions || this.defaultDisplayOptions
  }


  constructor(
    private fb: FormBuilder,
    private currencySvc: CurrencyService,
    private estimateSvc: EstimateService,
    private decimalPipe: DecimalPipe,
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      currency: [''],
      estimate: ['', [Validators.maxLength(9), Validators.min(0)]],
      estimateHigh: ['', [Validators.maxLength(9), Validators.min(0)]],
    })
  }

  ngAfterViewInit() {
    const estimateHighField = this.form.controls.estimateHigh,
      lowEstimatesField = this.form.controls.estimate,
      lowObs = this.filterAuctionObservables(lowEstimatesField.valueChanges, estimateHighField, 'highValue'),
      highObs = this.filterAuctionObservables(estimateHighField.valueChanges, lowEstimatesField, 'lowValue'),
      estimateFieldsArray = [lowObs, highObs]

    if (_.get(this.displayOptions, 'estimateHigh')) {
      this.subscribeEstimates(estimateFieldsArray)
    }

    this.id = this.displayOptions.id || this.id

    this.currencySubscription = this.currencySvc.elements
      .subscribe(searchCurrency => this.currencies = searchCurrency);

    // Searche in Estimate, Also Lower And High
    this.estimateLowfieldsubscription = this.form.get('estimate').valueChanges
      .subscribe(this.estimateValueSearch.bind(this, this.pricingConstants.LOWVALUE));
    this.estimateHighFieldSubscription = this.form.get('estimateHigh').valueChanges
      .subscribe(this.estimateValueSearch.bind(this, this.pricingConstants.HIGHVALUE));

    this.estimateSvc.elements
      .skip(1)
      .take(1)
      .subscribe((estimates) => {
        this.estimateSubscription = this.estimateSvc.elements
          .subscribe(searchEstimate => this.estimates = this.addDecimalValues(searchEstimate))
      });

    this.formSubscription = this.form.valueChanges
      .debounceTime(400)
      .distinctUntilChanged()
      .subscribe(this.formChanged.bind(this))

    this.auctionCurrencyFieldSubscription = this.form.get('currency').valueChanges
      .filter(data => !!data || data === '')
      .distinctUntilChanged()
      .debounceTime(400)
      .subscribe((currency) => {
        this.getDataToFilter(currency);
      })
  }

  ngOnDestroy() {
    if (this.currencySubscription) {
      this.currencySubscription.unsubscribe()
    }

    if (this.estimateSubscription) {
      this.estimateSubscription.unsubscribe();
    }

    if (this.estimateLowfieldsubscription) {
      this.estimateLowfieldsubscription.unsubscribe();
    }

    if (this.estimateHighFieldSubscription) {
      this.estimateHighFieldSubscription.unsubscribe();
    }

    if (this.formSubscription) {
      this.formSubscription.unsubscribe();
    }
  }

  formChanged(form) {
    this.estimate = form.estimate
    this.estimateHigh = _.get(this.displayOptions, 'estimateHigh') ? form.estimateHigh : null
    this.checkValidity(this.required)
  }

  checkValidity(required) {
    const currencyField = this.form.controls.currency,
      validator = required && (this.estimate || this.estimateHigh) ? Validators.required : []

    this.required = required

    currencyField.clearValidators()
    currencyField.setValidators(validator)
    currencyField.updateValueAndValidity({ onlySelf: !required })
    this.form.updateValueAndValidity()

    this.formErrors = _.reduce(this.form.controls, (prev, value, index) => {
      if (value.invalid) {
        const message = _.get(this.dictionary.CURRENCY_ESTIMATE, `${_.toUpper(_.snakeCase(index))}`);
        const pairs = _.toPairs(value.errors);
        prev[index] = _.get(message, pairs[0][0])
      }
      return prev
    }, {});
  }

  private subscribeEstimates(raceArray = []) {
    const auctionEstimates = Observable
      .race(raceArray)
      .take(1)
      .subscribe((val: any) => {
        const dropdownValue = val.val || {}
        val.input.patchValue(dropdownValue[val.attr])
        this.subscribeEstimates(raceArray)
      })
  }

  private filterAuctionObservables(AuctionObs: Observable<any>, field: any, attr: string) {
    return AuctionObs
      .filter(val => _.has(val, 'id'))
      .map((val) => ({ val: val, input: field, attr: attr }))
  }

  getDataToFilter(data) {
    if (!_.get(data, 'id')) {
      // Search data when the input have values for filter;
      const search = data && typeof data === 'string' ? { codeOrName: data } : null;
      this.currencySvc.getElements(search);
    }
  }

  addDecimalValues(estimates: any) {
    return estimates.map((estimate) => ({
      ...estimate,
      nameHigh: this.decimalPipe.transform(estimate.highValue),
      nameLow: this.decimalPipe.transform(estimate.lowValue)
    }))
  }

  observableSource = (): Observable<any[]> => {
    return this.currencySvc.elements;
  }

  observableEstimate = (): Observable<any[]> => {
    return this.estimateSvc.elements
      .map((estimates) => this.addDecimalValues(estimates));
  }

  estimateValueSearch(filter, data) {
    // Search data when the input have values for filter;
    const search = data && typeof data === 'number' ? { [filter]: data } : null;
    this.estimateSvc.getElements(search);
  }

  currencyScroll(event) {
    const totalElements = this.currencySvc.page.value.totalElements;
    if (this.currencies && totalElements > this.currencies.length) {
      this.currencySvc.getNextElements();
    }
  }

  estimatesScroll(event) {
    const totalElements = this.estimateSvc.page.value.totalElements;
    if (this.estimates && totalElements > this.estimates.length) {
      this.estimateSvc.getNextElements();
    }
  }


  listFormatterCurrency(data) {
    return `${data.name} (${data.code})`;
  }

  listFomatterEstimate(data) {
    return `${data.nameLow}`
  }

  listFomatterEstimateHigh(data) {
    return `${data.nameHigh}`
  }
}
