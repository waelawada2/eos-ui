import { Component, EventEmitter, Inject, forwardRef, Input, OnInit, OnDestroy, Output } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { GeneralDictionary } from '../../constants';
import { NavigationService } from './../../services';
import { BUTTON_STATUS } from './navigation-header.button-status.interface';
import { SORTING } from './navigation-header.sorting.interface'
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-navigation-header',
  templateUrl: './navigation-header.component.html',
  styleUrls: ['./navigation-header.component.scss']
})
export class NavigationHeaderComponent implements OnInit, OnDestroy {

  dictionary = GeneralDictionary.NAVBAR;
  buttonStatusValues = BUTTON_STATUS
  expandedMenu = false;

  @Input() totalPages: number

  public currentPage = 0;
  public status: any;
  public statusSubscription: Subscription;
  public descendingEnabled: boolean;
  public ascendingEnabled: boolean;
  private form: FormGroup;
  private pageSubscription: Subscription;
  private upwardsPageSubscription: Subscription;

  constructor(
    @Inject(forwardRef(() => NavigationService)) private navService: NavigationService,
    private router: Router,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.statusSubscription = this.navService.status.subscribe({ next: this.updateStatus.bind(this) })
    this.sortDescending();
    this.form = this.fb.group({
      page: 1,
    })
    this.pageSubscription = this.form.get('page').valueChanges
      .debounceTime(400)
      .filter(value => value !== '')
      .subscribe(this.handlePageChange.bind(this));

    this.upwardsPageSubscription = this.navService.upwardsPageEvent.subscribe((value) => {
      this.currentPage = value;
      this.form.patchValue({ page: this.currentPage + 1 })
    })
  }

  ngOnDestroy() {
    this.pageSubscription.unsubscribe();
    this.statusSubscription.unsubscribe();
    this.upwardsPageSubscription.unsubscribe();
  }

  selectedNavItem(event: string) {
    this.navService.emitNavChangeEvent(event);
  }

  prevPage() {
    if (this.currentPage > 0) {
      this.form.patchValue({ page: this.currentPage })
      this.currentPage--;
    }
  }

  nextPage() {
    if (this.currentPage < this.totalPages - 1) {
      this.currentPage++;
      this.form.patchValue({ page: this.currentPage + 1 })
    }
  }

  updateStatus(value) {
    this.status = value;
  }

  displayHiddenMenu() {
    this.expandedMenu = !this.expandedMenu
  }

  hideMenu() {
    this.expandedMenu = false
  }

  goToHome() {
    this.router.navigate(['/']);
  }

  sortAscending() {
    if (this.ascendingEnabled) {
      this.navService.emitSortingChangeEvent(SORTING.ASCENDING)
    }
    this.descendingEnabled = true;
    this.ascendingEnabled = false;
  }

  sortDescending() {
    if (this.descendingEnabled) {
      this.navService.emitSortingChangeEvent(SORTING.DESCENDING)
    }
    this.descendingEnabled = false;
    this.ascendingEnabled = true;
  }

  handlePageChange(value) {
    if (value > 0 && value <= this.totalPages) {
      this.currentPage = value - 1;
      this.navService.emitPageChangeEvent(this.currentPage)
    } else {
      this.form.patchValue({ page: this.currentPage + 1 })
    }
  }

}

