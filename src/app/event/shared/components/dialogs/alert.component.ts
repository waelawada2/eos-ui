import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { GeneralDictionary } from '../../constants';

export interface ConfirmModel {
  title: string;
  message: string;
}
@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class AlertComponent extends DialogComponent<ConfirmModel, any> implements ConfirmModel {
  title: string;
  message: string;
  dictionary = GeneralDictionary
  constructor(dialogService: DialogService) {
    super(dialogService);
  }

  confirm() {
    this.close();
  }

}
