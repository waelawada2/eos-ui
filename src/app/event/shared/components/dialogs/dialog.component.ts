import { Component } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { GeneralDictionary } from '../../constants';
export interface ConfirmModel {
  title: string;
  message: string;
}
@Component({
  selector: 'app-confirm',
  styleUrls: [
    './dialog.component.scss',
  ],
  templateUrl: './dialog.component.html',
})

export class ConfirmComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel {
  title: string;
  message: string;
  dictionary = GeneralDictionary
  constructor(dialogService: DialogService) {
    super(dialogService);
  }

  confirm() {
    this.result = true;
    this.close();
  }
}
