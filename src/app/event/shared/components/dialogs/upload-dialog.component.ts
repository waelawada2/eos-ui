import { Component } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { GeneralDictionary } from '../../constants';
import { Observable } from 'rxjs/Observable';
import { validateImportFileName } from '../../validators';
import * as _ from 'lodash';

export interface UploadModel {
  title: string;
  message: string;
  entityType: string;
  type: string | string[];
}

@Component({
  selector: 'app-upload-dialog',
  styleUrls: [
    './dialog.component.scss',
  ],
  templateUrl: './upload-dialog.component.html',
})
export class UploadDialogComponent extends DialogComponent<UploadModel, File> implements UploadModel {
  title: string;
  message: string;
  entityType: string;
  type: string | string[];
  dictionary = GeneralDictionary;
  invalidName: string;
  canUpload = false;
  private file: any

  constructor(dialogService: DialogService) {
    super(dialogService);
  }

  confirm() {
    if (this.canUpload) {
      this.result = this.file;
      this.close()
    }
  }

  fileChanged(event) {
    this.file = event.target.files[0];
    this.invalidName = '';
    this.canUpload = true;
    if (this.file && !validateImportFileName(this.entityType, this.type, this.file.name)) {
      const message = this.getInvalidMessage()
      this.invalidName = GeneralDictionary.DIALOG.INVALID_FILENAME(this.entityType, message);
      this.canUpload = false
    }
  }

  getInvalidMessage(): string {
    if (typeof this.type === 'string') {
      return this.type
    } else {
      return <string>_.reduce(this.type, (result, value) => `${result}" ${GeneralDictionary.DIALOG.AND_OR} "${value}`)
    }
  }

}
