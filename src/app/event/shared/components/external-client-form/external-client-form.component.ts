import { AccordionModule } from 'ngx-bootstrap';
import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GeneralDictionary, FormsDictionary, GeneralConstants, FormsConstants } from './../../constants'
import { autocompleListFormatter, getAgeDate, setValidators, convertToAutoformatCompatibleObject } from './../../utils';
import { ClientType, ClientStatus, KeyClientManager, Segment, ClientSuffix, ClientTitle } from '../../models';
import { noRequiredObjectValidator, RangeValidator } from '../../validators';
import {
  ClientSegmentService, ClientStatusService, ClientTypeService, ExternalClientStatusService, CurrencyService,
  KeyClientManagerService, ExternalClientTitleService, GendersService, ExternalClientSuffixService,
} from '../../services';
import { DialogService } from 'ng2-bootstrap-modal';
import { ClientDialogComponent } from '../client/client.dialog.component';
import { getAge } from '../../utils';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { combineLatest } from 'rxjs/observable/combineLatest';
import * as _ from 'lodash';
import * as moment from 'moment/moment';

export class ClientCons {
  public static mainClientFields = ['mainClientID', 'mainClientPartyName', 'mainClientType']
  public static nameRelatedFields = ['title', 'name', 'middleName', 'surname', 'suffix'];
  public static nonEditableFields = ['age', 'number', 'status', 'origin', ...ClientCons.mainClientFields];
  public static individualRelated = ['gender', 'deceased'];
  public static defaultStatus = 'incomplete member'
  public static fieldsGroup = {
    client: 'client',
    clientType: 'clientType',
    clientOrigin: 'clientOrigin',
    clientSegment: 'clientSegment',
    clientLocation: 'clientLocation',
  }
}

@Component({
  selector: 'app-external-client-form',
  templateUrl: './external-client-form.component.html',
  styleUrls: [
    '../../../shared/styles/forms.scss',
    './external-client.form.component.scss',
  ]
})
export class ExternalClientFormComponent implements OnInit, OnDestroy, AfterViewInit {

  public allowChangeMainClient = false;
  private deceasedValues: any[] = FormsConstants.CLIENT_DECEASED;
  private clientConst = ClientCons;
  private defaultStatus = this.clientConst.defaultStatus;
  private dictionary = FormsDictionary;
  public client;
  private clientSubscription: Subscription;
  private generalDictionary = GeneralDictionary;
  public form: FormGroup;
  public fieldsGroup = this.clientConst.fieldsGroup;
  private listFormatter = autocompleListFormatter;
  public mainClient;
  private mainClientFields = this.clientConst.mainClientFields;
  private nameMaxLength = 35;
  private nameRelatedFields = this.clientConst.nameRelatedFields;
  private locationMaxLength = 150;
  private cityMaxLength = 30;
  private nonEditableFields = this.clientConst.nonEditableFields;
  private types: ClientType[] = [];
  private kcms: KeyClientManager[] = [];
  private statuses: ClientStatus[] = [];
  private segments: Segment[] = [];
  private suffixes: ClientSuffix[] = [];
  private titles: ClientTitle[] = [];
  private notEditable = false;
  private originValues: any[] = FormsConstants.CLIENT_ORIGIN;
  private gendersValues;
  public formErrors: any = {};
  private individualRelated = ClientCons.individualRelated;
  private typeSubscription: Subscription;
  private genderSubscription: Subscription;
  @Input()
  public hideElements = ['filter'];
  @Input()
  set disabled(disabled) {
    this.isDisabled = disabled;
    if (this.form) {
      this.verifyEnableForm();
    }
  }
  @Input() public mode = 'wide';
  private isDisabled = false;
  private kcmsSubscription: Subscription;
  private kcmSubscriptionField: Subscription;
  private partyName;
  private segmentSubscription: Subscription;
  private suffixSubscription: Subscription;
  private titleSubscription: Subscription;
  private searchValidators = {
    city: Validators.maxLength(this.cityMaxLength),
    country: Validators.maxLength(this.locationMaxLength),
    deceased: noRequiredObjectValidator(),
    firstName: [Validators.maxLength(this.nameMaxLength)],
    gender: noRequiredObjectValidator(),
    kcm: noRequiredObjectValidator(),
    level: RangeValidator(0, 10),
    mainClientType: noRequiredObjectValidator(),
    middleName: [Validators.maxLength(this.nameMaxLength)],
    name: Validators.maxLength(this.nameMaxLength),
    origin: noRequiredObjectValidator(),
    region: Validators.maxLength(this.locationMaxLength),
    segment: noRequiredObjectValidator(),
    state: Validators.maxLength(this.locationMaxLength),
    status: noRequiredObjectValidator(),
    surname: [Validators.maxLength(this.nameMaxLength)],
    type: noRequiredObjectValidator(),
  }
  private standarValidators = {
    ..._.assign(_.clone(this.searchValidators), {
      type: noRequiredObjectValidator(true),
      level: [Validators.required, RangeValidator(0, 10)]
    })
  }
  private individualValidators = {
    ..._.assign(_.clone(this.standarValidators), { surname: _.union(this.standarValidators.surname, [Validators.required]) }),
  }
  private companiesValidators = {
    ..._.assign(_.clone(this.standarValidators), { party: Validators.required }),
  }
  public isIndividual = false;
  public isCompany = false;
  private validate = false;
  public validationMessages = FormsDictionary.EXTERNAL_CLIENT.VALIDATION;
  private formSubscription: Subscription;
  private typeSubs: Subscription;
  private partySubs: Subscription;
  private suffixFieldSubs: Subscription;
  private titleFieldSubs: Subscription;
  private preselectedStatus;
  private updating = false;
  private formGroupsToClear = [];
  private individualSubscriptions = {};

  @Output()
  private submit: EventEmitter<any> = new EventEmitter<any>()

  constructor(
    private fb: FormBuilder,
    private clientTypeService: ClientTypeService,
    private kcmService: KeyClientManagerService,
    private clientStatusService: ExternalClientStatusService,
    private clientSegmentService: ClientSegmentService,
    private genderService: GendersService,
    private suffixService: ExternalClientSuffixService,
    private titleService: ExternalClientTitleService,
    private dialogService: DialogService,
  ) {
    this.subscribeToIntialData();
  }

  subscribeToIntialData() {
    this.typeSubscription = this.clientTypeService.elements.subscribe((types) => {
      this.types = types;
    })

    this.kcmsSubscription = this.kcmService.elements.subscribe(kcms => {
      this.kcms = kcms
    });

    this.clientSubscription = this.clientStatusService.elements.subscribe(statuses => {
      this.statuses = statuses;
      if (this.validate && !this.notEditable) {
        this.setStatus();
      }
    })

    this.segmentSubscription = this.clientSegmentService.elements.subscribe(segments => {
      this.segments = segments;
    })

    this.genderSubscription = this.genderService.elements.subscribe((genders) => {
      this.gendersValues = genders;
    })

    this.suffixSubscription = this.suffixService.element.subscribe((suffixes: any) => {
      this.suffixes = suffixes;
    })

    this.titleSubscription = this.titleService.element.subscribe((titles: any) => {
      this.titles = titles;
    })
  }

  ngOnInit() {
    this.form = this.fb.group({
      age: [''],
      city: [''],
      country: [''],
      deceased: [''],
      gender: [null],
      kcm: [''],
      level: [''],
      mainClientID: [''],
      mainClientPartyName: [''],
      mainClientType: [''],
      middleName: [''],
      name: [''],
      number: [''],
      origin: [''],
      party: [''],
      region: [''],
      segment: [null],
      state: [''],
      status: [''],
      suffix: [''],
      surname: [''],
      title: [''],
      type: [''],
    });
    if (this.compactMode()) {
      this.groupFields();
    }
  }

  groupFields() {
    this.individualSubscriptions = {};
    _.each({
      client: ['party', 'number', 'kcm'],
      clientType: ['type'],
      clientLocation: ['city', 'region', 'state', 'country'],
      clientSegment: ['segment'],
      clientOrigin: ['origin']
    }, (value, key) => {
      this.individualSubscriptions[String(key)] = {
        type: key,
        visible: false,
        ...this.getSubscriptionsFor(key, value)
      }
    })
  }
  clearGroup(key) {
    const sectionFields = _.get(this.individualSubscriptions, key);
    _.each(_.get(sectionFields, 'fields'), field => field.reset());
    sectionFields.visible = false;
  }
  getSubscriptionsFor(label, fields) {
    return {
      fields: this.returnFields(this.form, fields),
      subscription: combineLatest(this.getFormFieldValueChanges(this.form, fields))
        .subscribe((value) => {
          const individualElement = _.get(this.individualSubscriptions, label);
          individualElement.visible = (value.join('') !== '')
        })
    }
  }

  getFormFieldValueChanges(form, elements): any {
    return this.returnFields(form, elements, (elm) => elm.valueChanges);
  }

  returnFields(form, elements, iterator = null) {
    return _.map(elements, (element) => {
      const formElement = form.get(element);
      return iterator ? iterator(formElement) : formElement;
    });
  }

  standarModeSubscriptions() {
    this.partySubs = combineLatest(this.getFormFieldValueChanges(this.form, this.nameRelatedFields))
      .filter(() => !this.isDisabled)
      .distinctUntilChanged()
      .debounceTime(200)
      .filter(() => this.notEditablefilter())
      .filter(() => this.isIndividual)
      .subscribe(name => {
        _.map(name, (data: any) => {
          let _data = data ? data : '';
          if (typeof _data === 'object') {
            _data = _.get(data, 'name').trim();
          }
          _data.trim();
        });

        this.partyName = String(_.compact(name).join(' ').trim())
        this.form.get('party').patchValue(this.partyName);
      });

    this.typeSubs = this.form.get('type').valueChanges
      .filter(() => !this.isDisabled)
      .debounceTime(100)
      .filter(() => this.notEditablefilter())
      .filter((value) => {
        const validate = this.validate;
        const isValidObject = typeof value === 'object' && value;
        this.isIndividual = this.isCompany = false;
        if (validate) {
          setValidators(this.form, this.getValidators())
        };
        return validate && isValidObject;
      })
      .subscribe(this.disableFormByType.bind(this))
  }


  ngAfterViewInit() {
    if (this.mode === 'wide') {
      this.standarModeSubscriptions();
    }
    this.form.valueChanges
      .filter(() => !this.isDisabled)
      .filter(() => !this.notEditable)
      .distinctUntilChanged()
      .debounceTime(200)
      .subscribe(this.onValueChanged.bind(this));
    this.kcmSubscriptionField = this.form.get('kcm').valueChanges.subscribe(this.kcmSearch.bind(this))
    this.suffixFieldSubs = this.form.get('suffix').valueChanges.subscribe(search => this.onDropdownSearch(this.suffixService, search))
    this.titleFieldSubs = this.form.get('title').valueChanges.subscribe(search => this.onDropdownSearch(this.titleService, search))
    setTimeout(() => {
      this.searchMode()
      this.verifyEnableForm();
    });

  }

  verifyEnableForm() {
    if (this.isDisabled) {
      this.form.disable()
    } else {
      this.form.enable()
    }
  }

  disableFormByType(type) {
    switch (_.get(type, 'name')) {
      case this.generalDictionary.EXTERNAL_CLIENT.TYPES.INDIVIDUAL:
        this.form.get('party').disable();
        this.returnFields(this.form, this.individualRelated, (field) => field.enable());
        this.isIndividual = true;
        this.disableNameFields(false);
        break;
      default:
        this.form.get('party').enable();
        if (!this.client) {
          this.form.get('party').reset();
        }
        this.returnFields(this.form, this.individualRelated, (field) => field.disable());
        this.isCompany = true;
        this.disableNameFields(true);
        break;
    }
    setValidators(this.form, this.getValidators());

  }

  ngOnDestroy() {
    this.typeSubscription.unsubscribe();
    this.kcmsSubscription.unsubscribe();
    this.clientSubscription.unsubscribe();
    this.segmentSubscription.unsubscribe();
    this.suffixSubscription.unsubscribe();
    this.titleSubscription.unsubscribe();
  }

  notEditablefilter() {
    return !this.notEditable && this.validate;
  }

  disableNameFields(disable = false) {
    const form = this.form;
    this.disableFormField(this.returnFields(this.form, this.nameRelatedFields), disable);
  }

  disableFormField(controls, disable, reset = true) {
    controls
      .map((control) => {
        if (disable) {
          control.disable();
          if (reset) {
            control.reset();
          }
        } else {
          control.enable();
        }
      })
  }

  getValidators() {
    let validators: any = this.standarValidators;

    if (this.isIndividual) {
      validators = this.individualValidators;
    }
    if (this.isCompany) {
      validators = this.companiesValidators;
    }
    return validators;
  }

  kcmObservable = (): Observable<any[]> => {
    return this.kcmService.elements;
  }

  standarMode() {
    if (this.notEditable) {
      this.form.clearValidators();
    } else {
      this.validate = true;
      setValidators(this.form, this.getValidators());
      this.disableNonEditable();
      this.setStatus();
      this.setOrigin();
    }
  }

  searchMode() {
    setValidators(this.form, this.searchValidators);
    this.validate = false;
    this.notEditable = false;
    this.enableForm();
  }

  setStatus() {
    if (this.statuses) {
      this.preselectedStatus = _.find(this.statuses,
        (status) => this.defaultStatus.toLowerCase() === _.get(status, 'name', '').toLowerCase())
      this.form.get('status').patchValue(convertToAutoformatCompatibleObject(this.preselectedStatus));
    }
  }

  setOrigin() {
    this.form.get('origin').patchValue(convertToAutoformatCompatibleObject(this.originValues[0]));
  }

  disableNonEditable(reset = true) {
    const form = this.form;
    this.disableFormField(this.returnFields(form, this.nonEditableFields), true, reset);
  }

  send() {
    this.submit.emit(this.getFormSearchData())
  }

  reset() {
    this.form.reset();
    this.client = null;
    this.mainClient = null;
    this.updating = false;
    this.notEditable = false;
    this.form.patchValue({
      suffix: '',
      title: '',
    })
    this.verifyEnableForm();
  }

  getFromClientSystemParam(value) {
    const origin = _.get(value, 'origin.name')
    if (origin && origin.length > 0) {
      return origin !== 'EOS';
    }
    return null
  }

  getFormSearchData() {
    const value = _.clone(this.form.getRawValue());
    return {
      ..._.pick(value, ['city', 'country', 'level', 'region', 'state']),
      entityId: _.get(value, 'number'),
      'individual.firstName': _.get(value, 'name'),
      'individual.lastName': _.get(value, 'surname'),
      'individual.middleName': _.get(value, 'middleName'),
      'individual.deceased': _.get(value, 'deceased.value'),
      'individual.suffix': this.getSuffix(value),
      'individual.title': this.getTitle(value),
      'individual.gender.name': _.get(value, 'gender.name'),
      'keyClientManager.entityId': _.get(value, 'kcm.entityId'),
      name: _.get(value, 'party'),
      'marketingSegment.name': _.get(value, 'segment.name'),
      'clientStatus.name': _.get(value, 'status.name'),
      'clientType.name': _.get(value, 'type.name'),
      'mainClient.name': this.form.get('mainClientPartyName').value,
      'fromClientSystem': this.getFromClientSystemParam(value),
      agedate: value.age ? getAgeDate(value.age) : null,
      'mainClient.entityId': this.form.get('mainClientID').value,
    }
  }

  getFormData() {
    const value = _.clone(this.form.getRawValue());
    return {
      ..._.pick(value, ['city', 'country', 'level', 'region', 'state', 'number']),
      individual: {
        firstName: _.get(value, 'name'),
        lastName: _.get(value, 'surname'),
        middleName: _.get(value, 'middleName'),
        deceased: _.get(value, 'deceased.value'),
        gender: _.get(value, 'gender', null),
        suffix: this.getSuffix(value),
        title: this.getTitle(value),
      },
      name: this.isIndividual ? this.partyName : _.get(value, 'party'),
      keyClientManager: _.get(value, 'kcm'),
      marketingSegment: _.get(value, 'segment'),
      clientType: _.get(value, 'type'),
      clientStatus: this.preselectedStatus,
      fromClientSystem: this.getFromClientSystemParam(value),
      mainClient: this.mainClient,
    }
  }

  getSuffix(value) {
    const suffix = _.get(value, 'suffix')
    return typeof suffix === 'string' ? suffix : _.get(suffix, 'name', '')
  }

  getTitle(value) {
    const title = _.get(value, 'title')
    return typeof title === 'string' ? title : _.get(title, 'name', '')
  }

  compactMode() {
    return this.mode === 'compact';
  }

  onValueChanged(data?: any, second?) {
    this.formErrors = _.reduce(this.form.controls, (prev, value, index) => {
      if (value.invalid) {
        const message = _.get(this.validationMessages, `${_.toUpper(_.snakeCase(index))}`);
        const pairs = _.toPairs(value.errors)
        prev[index] = _.get(message, pairs[0][0]);
      }
      return prev
    }, {});
  }

  onDropdownSearch(service, value) {
    if (typeof value !== 'string') {
      return;
    }
    service.getElements({ name: value });
  }

  restoreData(data) {
    this.updating = true;
    this.form.patchValue(data);
  }

  eventScroll(event) {
    const totalElements = this.kcmService.page.value.totalElements;
    if (totalElements > this.kcms.length) {
      this.kcmService.getNextElements();
    }
  }

  titleScroll(event) {
    const totalElements = this.titleService.page.value.totalElements;
    if (totalElements > this.kcms.length) {
      this.titleService.getNextElements();
    }
  }

  suffixScroll(event) {
    const totalElements = this.suffixService.page.value.totalElements;
    if (totalElements > this.kcms.length) {
      this.suffixService.getNextElements();
    }
  }

  kcmSearch(kcm) {
    const search = kcm && typeof kcm === 'string' ? { 'name': kcm } : null;
    this.kcmService.getElements(search);
  }

  updateDataForm(data) {
    this.client = data;
    this.updating = true;
    this.notEditable = data.fromClientSystem;
    if (data.fromClientSystem || this.isDisabled) {
      this.form.disable();
    } else {
      this.form.enable();
      this.form.get('type').disable();
      this.disableNonEditable(false);
    }
    this.form.patchValue({
      age: getAge(_.get(data, 'individual.birthDate')),
      city: _.get(data, 'city'),
      country: _.get(data, 'country'),
      deceased: convertToAutoformatCompatibleObject(_.find(this.deceasedValues, { value: _.get(data, 'individual.deceased') })),
      gender: convertToAutoformatCompatibleObject(_.get(data, 'individual.gender')),
      kcm: convertToAutoformatCompatibleObject(_.get(data, 'kcm')),
      level: _.get(data, 'level'),
      middleName: _.get(data, 'individual.middleName'),
      name: _.get(data, 'individual.firstName'),
      number: _.get(data, 'id'),
      origin: convertToAutoformatCompatibleObject(_.find(this.originValues, { 'value': this.notEditable })),
      party: _.get(data, 'name'),
      region: _.get(data, 'region'),
      segment: convertToAutoformatCompatibleObject(_.get(data, 'segment')),
      state: _.get(data, 'state'),
      status: convertToAutoformatCompatibleObject(_.get(data, 'status')),
      suffix: _.get(data, 'individual.suffix'),
      surname: _.get(data, 'individual.lastName'),
      title: _.get(data, 'individual.title'),
      type: convertToAutoformatCompatibleObject(_.get(data, 'type')),
    });
    this.setMainClientValues(_.get(data, 'mainClient'));
  }
  enableForm() {
    this.returnFields(this.form, this.mainClientFields, control => control.disable());
  }

  observableSuffix = (): Observable<any[]> => {
    return this.suffixService.elements;
  }

  observableTitle = (): Observable<any[]> => {
    return this.titleService.elements;
  }

  setMainClientValues(data) {
    this.mainClient = data;
    this.form.patchValue({
      mainClientPartyName: _.get(data, 'name'),
      mainClientType: _.get(data, 'clientType.name') || _.get(data, 'type.name'),
      mainClientID: _.get(data, 'entityId')
    });
  }

  removeMainClient() {
    this.returnFields(this.form, this.mainClientFields, control => control.reset());
  }

  addClient() {
    this.dialogService.addDialog(ClientDialogComponent).take(1).subscribe((response) => {
      if (response) {
        this.setMainClientValues(response);
      }
    })
  }
}

