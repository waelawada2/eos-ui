import { OnInit, AfterViewInit, Component, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ObjectService } from '../../../../shared/services';
import { EVENTCONST } from './../../constants';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import * as _ from 'lodash';
import { Property } from 'app/event/shared/models';
import { GeneralDictionary } from 'app/shared/constants'
import { GeneralConstants, GeneralDictionary as EventDictionary } from 'app/event/shared/constants'
import { ContextMenuOptions } from 'app/event/shared/components/context-menu/context-menu-options.interface'
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-table-object',
  templateUrl: './table-object.component.html',
  styleUrls: ['./../../styles/tables.scss', './table-object.component.scss']
})
export class TableObjectComponent implements OnInit, AfterViewInit, OnDestroy {
  dictionary = GeneralDictionary.EVENT_REPORT.OBJECTS;
  objectSubscription: Subscription;
  objects: Property[] = []
  defaultImagePlaceholder = GeneralConstants.PLACEHOLDER;
  defaultCataloguing = EVENTCONST.cataloguing
  defaultDescription = EVENTCONST.imageDescription
  options: any = EVENTCONST.objectTableOptions


  private menuOptions: ContextMenuOptions[] = [{
    text: EventDictionary.CONTEXT_MENU.REMOVE(`${GeneralConstants.ENTITY_TYPE.OBJECT} from ${GeneralConstants.ENTITY_TYPE.ITEM}`),
    id: 'menu-delete',
    subject: new Subject<Property>()
  }];

  @Input()
  showTitles = true;

  @Input()
  enableContextMenu = false;

  @Input() set objectIds(objectIds: Array<any>) {
    if (objectIds) {
      const ids = objectIds.map(object => object.objectId)

      const properties: Property[] = []
      this._service.getObjects(ids)
        .subscribe(property => properties.push(property), null, () => this.objects = this._service.transformElements(properties));
    }
  }
  @Input() set tableOptions(options: any) {
    this.options = options || this.options
  }

  @Output() removeObject: EventEmitter<Property> = new EventEmitter();

  constructor(
    private _service: ObjectService,
  ) {

  }

  ngOnInit() {
    if (this.enableContextMenu) {
      this.menuOptions[0].subject.subscribe(this.removeObjectEmitter.bind(this));
    }
  }

  ngAfterViewInit() {
    this.objectSubscription = this._service.elements
      .subscribe((objects: any) => this.objects = objects)
  }

  ngOnDestroy() {
    this.objectSubscription.unsubscribe();
  }

  removeObjectEmitter(object) {
    this.removeObject.emit(object)
  }
}
