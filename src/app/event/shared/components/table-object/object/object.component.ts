import { Component, Input } from '@angular/core'
import { Property } from 'app/event/shared/models';
import { GeneralConstants } from 'app/event/shared/constants';
import { PlatformLocation } from '@angular/common';

@Component({
  selector: 'app-object',
  templateUrl: './object.component.html',
  styleUrls: ['./object.component.scss']
})
export class ObjectComponent {
  defaultImagePlaceholder: string = GeneralConstants.PLACEHOLDER;
  context: string;
  isClickable = true;

  @Input() object: Property;
  @Input()
  set clickable(value) {
    this.isClickable = value;
  }

  @Input()
  bigimage = true;

  constructor(platformLocation: PlatformLocation) {
    this.context = platformLocation.getBaseHrefFromDOM();
  }
}
