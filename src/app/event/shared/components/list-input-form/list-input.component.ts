import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FormsDictionary } from '../../constants';
import AbstractService from '../../services/abstract.service';
import { Message, MessageType } from '../../models';
import { Subscription } from 'rxjs/Subscription';
import { transformData, randomlyAddFields } from '../../mocks';
import * as _ from 'lodash';

@Component({
  selector: 'app-list-input-form',
  templateUrl: './list-input.component.html',
  styleUrls: ['../../styles/forms.scss', './list-input.component.scss'],
})
export class ListInputFormComponent implements OnInit, OnDestroy {

  listInputForm: FormGroup;
  dictionary = FormsDictionary.LIST_INPUT;
  value: string;
  multipleValues: any[] = [];
  ids: any[] = [];
  data: any[] = [];
  fieldSubscription: Subscription;

  @Input()
  notAllowedIds = [];

  @Input()
  set selected(value: any[]) {
    const mappedValue = _.map(value, (item) => transformData(randomlyAddFields(item)))
    this.data = this.sortData(mappedValue)
  }

  @Input()
  label: string;

  @Input()
  externalData: AbstractService<any>;

  @Output()
  messageEmitted: EventEmitter<Message> = new EventEmitter<Message>();

  @Output()
  valuesEmitter: EventEmitter<any[]> = new EventEmitter<any[]>();

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    const initialValue = '';
    this.controlUpdated(initialValue)
    this.listInputForm = this.fb.group({
      inputControl: [initialValue],
    })
    this.fieldSubscription = this.listInputForm.get('inputControl').valueChanges.subscribe(this.controlUpdated.bind(this));
  }

  ngOnDestroy() {
    this.fieldSubscription.unsubscribe();
  }

  addElement() {
    if (!this.value || this.value === '') {
      return this.messageEmitted.emit({ title: this.dictionary.ERROR, type: MessageType.ERROR, message: this.dictionary.EMPTY_FIELD });
    }

    if (this.notAllowedIds.indexOf(this.value) !== -1) {
      return this.messageEmitted.emit({ title: this.dictionary.ERROR, message: this.dictionary.SELF_ELEMENT, type: MessageType.ERROR });
    }

    if (this.value.indexOf(',') === -1) {
      if (!_.find(this.data, (element) => element.id === this.value)) {
        this.externalData.getElement(this.value);
        this.externalData.element.skip(1).take(1).subscribe(this.handleResponse.bind(this), this.errorHandler.bind(this))
      } else {
        this.messageEmitted.emit({
          title: this.dictionary.ERROR, message: `${this.dictionary.ELEMENT_REPEATED}: ${this.value}`,
          type: MessageType.ERROR
        });
      }
    } else {
      this.ids = _.map(_.split(this.value, ','), _.trim)
      const errors = _.reduce(this.ids, (res, id) => {
        if (_.find(this.data, (element) => element.id === id)) {
          this.messageEmitted.emit({
            title: this.dictionary.ERROR, message: `${this.dictionary.ELEMENT_REPEATED}: ${id}`,
            type: MessageType.ERROR
          });
          return true
        }

        if (this.notAllowedIds.indexOf(id) !== -1) {
          this.messageEmitted.emit({ title: this.dictionary.ERROR, message: this.dictionary.SELF_ELEMENT, type: MessageType.ERROR });
          return true
        }
        return res;
      }, false)
      if (!errors) {
        this.multipleValues = [];
        this.externalData.element.skip(1).take(this.ids.length)
          .subscribe(this.handleMultipleResponses.bind(this), this.errorHandler.bind(this))
        _.each(this.ids, (id) => this.externalData.getElement(id))
      }
    }
  }

  reset() {
    this.listInputForm.reset();
  }

  controlUpdated(value) {
    this.value = value;
  }

  handleMultipleResponses(data) {
    if (data) {
      data = randomlyAddFields(data);
      data = transformData(data)
      this.multipleValues.push(data);
      if (this.ids.length === this.multipleValues.length) {
        const newData = this.data.concat(this.multipleValues)
        this.reset();
        this.data = this.sortData(newData);
        this.valuesEmitter.emit(this.data);
        this.multipleValues = [];
        this.ids = [];
      }
    } else {
      this.messageEmitted.emit({ title: this.dictionary.ERROR, message: this.dictionary.NO_ELEMENT_FOUND, type: MessageType.ERROR })
    }
  }

  handleResponse(data) {
    if (data) {
      data = transformData(randomlyAddFields(data));
      const newData = _.clone(this.data)
      newData.push(data);
      this.reset();
      this.data = this.sortData(newData);
      this.valuesEmitter.emit(this.data)
    } else {
      this.messageEmitted.emit({ title: this.dictionary.ERROR, message: this.dictionary.NO_ELEMENT_FOUND, type: MessageType.ERROR })
    }
  }

  sortData(data) {
    return _.sortBy(data, ['establishmentName', 'city'])
  }

  errorHandler(error) {
    this.messageEmitted.emit({ title: this.dictionary.ERROR, message: error, type: MessageType.ERROR })
  }
}

interface IListInputData {
  id: string,
  name: string,
}

