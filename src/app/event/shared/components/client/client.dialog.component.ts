import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { GeneralDictionary } from '../../constants';
import { ExternalClient } from '../../models';

@Component({
  templateUrl: './client.dialog.component.html',
  styleUrls: [
    './client.dialog.component.scss',
  ],
})
export class ClientDialogComponent extends DialogComponent<void, ExternalClient> {
  dictionary = GeneralDictionary

  constructor(dialogService: DialogService) {
    super(dialogService);
  }

  setSelectedClient(client: ExternalClient) {
    this.result = client;
    this.close();
  }

}
