import { Component, EventEmitter, Output, OnDestroy } from '@angular/core';
import { ExternalClientService } from '../../services';
import { FormsDictionary, GeneralDictionary, GeneralConstants, TableDictionary } from '../../constants'
import { ExternalClient } from '../../models'
import { getAge } from '../../utils';
import { Subscription } from 'rxjs/Subscription';
@Component({
  providers: [ExternalClientService],
  selector: 'app-client-component',
  styleUrls: [
    '../../styles/forms.scss',
    '../../styles/tables.scss',
    './client.component.scss',
  ],
  templateUrl: './client.component.html',
})
export class ClientComponent implements OnDestroy {
  private generalDictionary = GeneralDictionary;
  private events = `${GeneralConstants.ENTITY_TYPE.EVENT}s`;
  private dictionary = TableDictionary;
  private formsDictionary = FormsDictionary;
  private getAge = getAge

  private clients: ExternalClient[] = [];
  private clientsSubscription: Subscription;

  private loading = false;

  @Output()
  public selectedClient: EventEmitter<ExternalClient> = new EventEmitter<ExternalClient>();

  constructor(
    private clientService: ExternalClientService,
  ) {

    this.clientsSubscription = clientService.elements.subscribe((clients) => {
      this.loading = false;
      this.clients = clients
    });
  }

  ngOnDestroy() {
    this.clientsSubscription.unsubscribe()
  }

  submitForm(value) {
    this.clients = [];
    this.loading = true;
    this.clientService.getElements(value);
  }

  addClient(client: ExternalClient) {
    this.selectedClient.emit(client);
  }

  loadMore() {
    const totalElements = this.clientService.page.value.totalElements;
    if (totalElements > this.clients.length) {
      this.loading = true;
      this.clientService.getNextElements()
    }
  }

}
