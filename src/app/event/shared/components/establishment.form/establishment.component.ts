import { Component, Input, OnDestroy, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { EstablishmentService } from '../../services';
import { FormsDictionary } from '../../constants';
import { autocompleListFormatter } from '../../utils';
import { noRequiredObjectValidator } from '../../../shared/validators';
import { Observable } from 'rxjs/Observable';

import * as _ from 'lodash';

@Component({
  selector: 'app-establishment-form',
  templateUrl: './establishment.component.html',
  styleUrls: ['../../styles/forms.scss'],
})
export default class EstablishmentFormComponent implements OnDestroy, OnInit, AfterViewInit {

  public establishmentForm: FormGroup;
  public establishmentData: any[] = [];
  public selectedEstablishment: any;
  public establishmentError = {};
  dictionary = FormsDictionary;
  listFormatter = autocompleListFormatter;
  establishment: AbstractControl;
  disabled = false;
  validate = false;
  requiredEstablishment = false;

  establishmentSubscription: Subscription;
  establishmentFormSubscription: Subscription;
  establishmentFieldSubscription: Subscription;

  @Input()
  parentForm: FormGroup;
  @Input() set validation(validate) {
    if (this.establishment) {
      this.requiredMode(validate);
    }
    this.validate = validate;
  };

  @Input() set required(_required) {
    this.requiredEstablishment = _required;
  };

  @Input()
  set disableForm(value: boolean) {
    this.disabled = value;
  }

  constructor(private establismentService: EstablishmentService, private fb: FormBuilder) {
  }

  ngOnInit() {
    this.establishmentForm = this.fb.group({
      establishment: [''],
      countryName: [''],
      state: [''],
      cityName: [''],
      region: [''],
    });
    this.establishment = this.establishmentForm.get('establishment');
    this.parentForm.addControl('establishment', this.establishmentForm)
    if (this.disabled) {
      this.establishmentForm.disable();
    }
    this.establishmentFormSubscription = this.establishment.valueChanges.subscribe(this.updateFormInfo.bind(this))
    this.establishmentFieldSubscription = this.establishmentForm.get('establishment').valueChanges.debounceTime(100)
      .subscribe(this.updateFormInfo.bind(this))
  }
  ngAfterViewInit() {
    this.establishmentSubscription = this.establismentService.elements.debounceTime(100)
      .subscribe({ next: (establishments) => this.establishmentData = establishments });
  }

  ngOnDestroy() {
    this.establishmentSubscription.unsubscribe();
    this.establishmentFormSubscription.unsubscribe();
    this.establishmentFieldSubscription.unsubscribe();
  }

  requiredMode(required) {
    this.establishment.clearValidators();
    this.establishment.setValidators(noRequiredObjectValidator(required));
    this.establishment.updateValueAndValidity();
  }

  updateFormInfo(data) {
    // Search data when the input have values for filter;
    const search = data && typeof data === 'string' ? { name: data } : null;

    const errors = this.establishmentForm.get('establishment').errors;
    this.establishmentError = _.reduce(this.establishmentForm.controls, (prev, value, index) => {
      if (value.invalid) {
        const message = _.get(this.dictionary, `${_.toUpper(_.snakeCase(index))}.VALIDATION`);
        const pairs = _.toPairs(value.errors);
        prev[index] = _.get(message, pairs[0][0]);
      }
      return prev;
    }, {});
    this.selectedEstablishment = data;
    if (typeof data === 'string') {
      this.establishmentForm.patchValue({ state: '', cityName: '', region: '', countryName: '' })
    } else {
      this.establishmentForm.patchValue(_.pick(data, ['state', 'cityName', 'region', 'countryName']));
    }

    this.establismentService.getElements(search);

  }

  eventScroll(event) {
    const totalElements = this.establismentService.page.value.totalElements;
    if (totalElements > this.establishmentData.length) {
      this.establismentService.getNextElements();
    }
  }

  observableSource = (): Observable<any[]> => {
    return this.establismentService.elements;
  }
}
