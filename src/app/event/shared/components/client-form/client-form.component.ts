import { AfterViewInit, Component, OnInit, Input, OnDestroy } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { DialogService } from 'ng2-bootstrap-modal'
import { NotificationsService } from 'angular2-notifications'
import { FormsDictionary, GeneralConstants, GeneralDictionary } from './../../constants'
import { autocompleListFormatter, convertToAutoformatCompatibleObject, updateValueAndValidity, setValidators } from './../../utils'
import { ClientDialogComponent } from './../client/client.dialog.component'
import { noRequiredObjectValidator, ObjectValidator } from '../../validators'
import { ClientSourceService, ClientSourceTypeService, ClientStatusService, ExternalClientService } from '../../services'
import { ClientSource, ClientStatus, ClientSourceType, ExternalClient } from '../../models'
import { Subscription } from 'rxjs/Subscription'
import { Observable } from 'rxjs/Observable'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import 'rxjs/add/observable/of'
import 'rxjs/add/operator/debounceTime'
import * as _ from 'lodash'

@Component({
  providers: [ExternalClientService],
  selector: 'app-client-form',
  templateUrl: './client-form.component.html',
  styleUrls: [
    './../../styles/forms.scss',
    './client-form.component.scss'
  ],
})
export class ClientFormComponent implements AfterViewInit, OnInit, OnDestroy {

  public dictionary = FormsDictionary.CLIENT;
  public clientForm: FormGroup;
  public formEventError = {};
  public listFormatter = autocompleListFormatter;
  public sourceTypes = [];
  public sourceNames = [];
  public statusList = [];
  public validateClient = false;
  public requiredClient = false;
  public defaultValue = GeneralConstants;
  public setValuesEnabled;
  public constantsOwnership = GeneralConstants.OWNERSHIP.DEFAULT_VALUES;
  private clientId = null;
  public standarValidatorMap = {
    clientSourceType: noRequiredObjectValidator(true),
    clientSource: noRequiredObjectValidator(true),
    clientStatus: noRequiredObjectValidator(true),
    clientId: [Validators.maxLength(10), Validators.required],
  }
  public searchValidatorMap = {
    clientSourceType: noRequiredObjectValidator(),
    clientSource: noRequiredObjectValidator(),
    clientStatus: noRequiredObjectValidator(),
  }

  clientSourceSubscription: Subscription;
  clientSourceTypeSubscription: Subscription;
  clientStatusSubscription: Subscription;
  formSubscription: Subscription;
  clientIdChangesSubscription: Subscription;
  clientDataErrorSubscription: Subscription;

  @Input()
  parentForm: FormGroup;

  @Input() set validate(_validate) {
    this.validateClient = _validate;
    if (this.clientForm) {
      this.requiredMode(_validate);
    }
  }

  @Input() set required(_required) {
    this.requiredClient = _required;
  }

  @Input()
  private disabled = false;

  constructor(
    private fb: FormBuilder,
    private clientSourceService: ClientSourceService,
    private clientSourceTypeService: ClientSourceTypeService,
    private clientStatusService: ClientStatusService,
    private dialogService: DialogService,
    private externalClientService: ExternalClientService,
    private route: ActivatedRoute,
    protected notificationSvc: NotificationsService,
  ) {
    this.setValuesEnabled = this.route.snapshot.fragment;
  }

  ngOnInit() {
    this.createFormGroup();
    this.parentForm.addControl('client', this.clientForm);
    this.requiredMode(false);
  }

  ngAfterViewInit() {
    this.subscribeToFormChanges();
    this.clientSourceSubscription = (<Observable<ClientSource[]>>this.clientSourceService.elements)
      .debounceTime(100)
      .subscribe({
        next: (data) => {
          this.sourceNames = data;
          if (this.setValuesEnabled === this.constantsOwnership.DEFAULTVALUE) {
            this.setDefaultValuesPublicationConsignment();
          }
        }
      });

    this.clientSourceTypeSubscription = (<Observable<ClientSourceType[]>>this.clientSourceTypeService.elements)
      .debounceTime(100)
      .subscribe({ next: (data) => this.sourceTypes = data });

    this.clientStatusSubscription = (<Observable<ClientStatus[]>>this.clientStatusService.elements)
      .debounceTime(100)
      .subscribe({
        next: (data) => this.statusList = data
      });

    this.subscribeToClientDataError();
  }

  requiredMode(_required) {
    setValidators(this.clientForm, _required ? this.standarValidatorMap : this.searchValidatorMap);
  }

  subscribeToFormChanges() {
    this.formSubscription = this.clientForm.valueChanges.debounceTime(200).subscribe(data => this.updateStatusForm(data));
    this.clientSourceSubscription = this.clientForm.get('clientSource').valueChanges.debounceTime(400)
      .subscribe(this.clientSourceSearch.bind(this))
  }

  ngOnDestroy() {
    this.clientSourceSubscription.unsubscribe();
    this.clientSourceTypeSubscription.unsubscribe();
    this.clientStatusSubscription.unsubscribe();
    this.formSubscription.unsubscribe();
    this.clientSourceSubscription.unsubscribe();
    this.clientDataErrorSubscription.unsubscribe();
  }

  clientSourceSearch(data) {
    // Search data when the input have values for filter;
    const search = data && typeof data === 'string' ? { name: data } : null;
    this.clientSourceService.getElements(search);
  }

  createFormGroup() {
    this.clientForm = this.fb.group({
      clientName: [''],
      clientKcm: [''],
      clientSourceType: ['', [ObjectValidator()]],
      clientSource: ['', [ObjectValidator()]],
      clientStatus: ['', [ObjectValidator()]],
      clientId: ['', Validators.maxLength(10)],
      display: [''],
      mainClient: [''],
      mainClientId: [''],
    });
    if (this.disabled) {
      this.clientForm.disable()
    }
  }


  updateStatusForm(data) {
    this.formEventError = _.reduce(this.clientForm.controls, (prev, value, index) => {
      if (value.invalid) {
        const message = _.get(this.dictionary.VALIDATION, `${_.toUpper(_.snakeCase(index))}`);
        const pairs = _.toPairs(value.errors);
        prev[index] = _.get(message, pairs[0][0]);
      }
      return prev
    }, {});
  }

  setFormInfo(data) {
    const mainClientId = _.get(data, 'mainClient.clientId')
    this.clientForm.patchValue({
      clientSourceType: convertToAutoformatCompatibleObject(_.get(data, 'mainClient.clientSourceType'), 'name'),
      clientSource: convertToAutoformatCompatibleObject(_.get(data, 'mainClient.clientSource'), 'name'),
      clientStatus: convertToAutoformatCompatibleObject(_.get(data, 'mainClient.clientStatus'), 'name'),
      clientId: _.get(data, 'mainClient.clientId'),
      display: _.get(data, 'mainClient.display')
    });
    this.getClient(mainClientId)
  }

  getClient(mainClientId) {
    this.clientId = mainClientId
    if (mainClientId) {
      this.externalClientService.getElements({}, mainClientId);
      (<Observable<ExternalClient[]>>this.externalClientService.elements)
        .skip(1)
        .take(1)
        .do(() => this.resetClient())
        .filter((elements) => !!elements)
        .concatMap((elements: ExternalClient[]) => elements)
        .take(1)
        .subscribe((element: ExternalClient) => {
          this.setClient(element);
        })
    }
  }

  resetClient(id = null) {
    this.clientForm.patchValue({
      clientId: id,
      clientName: '',
      mainClient: '',
      mainClientId: '',
      clientKcm: '',
    })
  }

  subscribeToClientDataError() {
    this.clientDataErrorSubscription = this.externalClientService.error
      .distinctUntilChanged()
      .skip(1)
      .filter((error) => !!error)
      .do(() => this.resetClient(this.clientId))
      .subscribe((error) => {
        this.notificationSvc.error(GeneralDictionary.NOTIFICATIONS.error,
          GeneralDictionary.NOTIFICATIONS.ERR_NOT_FOUND_CLIENT(this.clientId),
          GeneralConstants.NOTIFICATIONS.OPTIONS)
      })
  }

  eventScroll(event) {
    const totalElements = this.clientSourceService.page.value.totalElements;
    if (totalElements > this.sourceNames.length) {
      this.clientSourceService.getNextElements();
    }
  }

  observableSource = (): Observable<ClientSource[]> => {
    return <Observable<ClientSource[]>>this.clientSourceService.elements;
  }

  setClient(client) {
    this.clientForm.patchValue({
      clientId: _.get(client, 'entityId'),
      clientName: _.get(client, 'name'),
      mainClient: _.get(client, 'mainClient.name'),
      mainClientId: _.get(client, 'mainClient.entityId'),
      clientKcm: _.get(client, 'kcm.name'),
    })
  }

  addClient() {
    this.dialogService.addDialog(ClientDialogComponent).take(1)
      .filter((response) => !!response)
      .subscribe(this.setClient.bind(this));
  }

  setDefaultValues() {
    const sourceTypeValue = { name: this.defaultValue.OWNERSHIP.EMPLOYEE, id: _.upperCase(this.defaultValue.OWNERSHIP.EMPLOYEE) };
    const clientStatusValue = { name: this.defaultValue.OWNERSHIP.ACTIVE, id: _.upperCase(this.defaultValue.OWNERSHIP.ACTIVE) };
    this.clientForm.patchValue({
      clientSourceType: convertToAutoformatCompatibleObject(sourceTypeValue, 'name'),
      clientStatus: convertToAutoformatCompatibleObject(clientStatusValue, 'name'),
    })
  }

  setDefaultValuesPublicationConsignment() {
    const sourceTypeValue = { name: this.defaultValue.OWNERSHIP.MEDIA, id: _.upperCase(this.defaultValue.OWNERSHIP.MEDIA) };
    const clientStatusValue = { name: this.defaultValue.OWNERSHIP.ACTIVE, id: _.upperCase(this.defaultValue.OWNERSHIP.ACTIVE) };
    const clientSourceValue = this.sourceNames.find(value => _.toLower(value.name) === _.toLower(this.defaultValue.OWNERSHIP.PUBLICATION));
    this.clientForm.patchValue({
      clientSourceType: convertToAutoformatCompatibleObject(sourceTypeValue, 'name'),
      clientStatus: convertToAutoformatCompatibleObject(clientStatusValue, 'name'),
      clientSource: convertToAutoformatCompatibleObject(clientSourceValue, 'name'),
    })
    this.clientForm.get('clientSourceType').disable();
    this.clientForm.get('clientSource').disable();
    this.setValuesEnabled = undefined;
  }

}
