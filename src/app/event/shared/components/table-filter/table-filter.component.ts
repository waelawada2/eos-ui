import { EVENTCONST } from './../../constants';
import { NotificationsService } from 'angular2-notifications';

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'table-filter',
  templateUrl: './table-filter.component.html',
  styleUrls: ['./table-filter.component.scss']
})
export class TableFilterComponent implements OnInit {

  public items;
  public originalItems;
  public filter;

  @Input() set itemsSetter(items) {
    if (items) {
      this.items = items;
    }
  }

  @Output() onFilter = new EventEmitter<string>();

  constructor(
    private _service: NotificationsService,
  ) { }

  ngOnInit() {
  }

  filterItems(word: string) {
    const results = [];
    let itemsArray = [];

    if (!this.items) {
      return;
    }

    if (!word || typeof word !== 'string') {
      this.items._embedded.items = _.size(this.originalItems) ? this.originalItems : this.items._embedded.items;
      this.filter = undefined;
      this.onFilter.emit(undefined)
      this.showMessage(
        EVENTCONST.filterEvent.title,
        EVENTCONST.filterEvent.bodyNoResult,
        EVENTCONST.filterEvent.type
      );
      return;
    }

    this.filter = word;
    this.originalItems = _.size(this.originalItems) ? this.originalItems : this.items._embedded.items

    itemsArray = this.originalItems;

    itemsArray.forEach((row, index) => {

      const stringified = JSON.stringify(row).toLowerCase();
      if (stringified.indexOf(word.toLowerCase()) !== -1 && row !== results[results.length - 1]) {
        results.push(row);
      }
    })


    this.showMessage(
      EVENTCONST.filterEvent.title,
      EVENTCONST.filterEvent.body(results.length),
      EVENTCONST.filterEvent.type
    );

    this.items._embedded.items = results.length ? results : this.originalItems;
    this.onFilter.emit(word);

    return this.items._embedded.items;
  }

  showMessage(title, content, type) {
    const options = {
      timeOut: 1000,
      showProgressBar: true,
      pauseOnHover: false,
      clickToClose: true
    };

    this._service.info(title, content, options);
  }
}
