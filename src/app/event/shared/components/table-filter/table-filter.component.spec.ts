import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { TableFilterComponent } from './table-filter.component';
import { TRANSACTION_ITEMS } from '../../../mocks/transaction-items.mock';
import { NotificationsService } from 'angular2-notifications';

fdescribe('TableFilterComponent', () => {
  let component: TableFilterComponent;
  let fixture: ComponentFixture<TableFilterComponent>;
  const testItems: any = TRANSACTION_ITEMS;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableFilterComponent ],
      providers: [
        NotificationsService,
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should filter items and notify the result', () => {
    const spyNotify = spyOn(component, 'showMessage');

    component.items = testItems;
    component.filterItems('2000');
    fixture.detectChanges();
    expect(component.items._embedded.items.length).toBe(1);
    expect(spyNotify).toHaveBeenCalled();
    component.filterItems(''); // reset filter
  });
});
