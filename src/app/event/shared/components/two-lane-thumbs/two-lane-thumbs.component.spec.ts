import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoLaneThumbsComponent } from './two-lane-thumbs.component';

describe('TwoLaneThumbsComponent', () => {
  let component: TwoLaneThumbsComponent;
  let fixture: ComponentFixture<TwoLaneThumbsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwoLaneThumbsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoLaneThumbsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
