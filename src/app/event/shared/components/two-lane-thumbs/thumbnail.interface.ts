export interface Thumbnail {
  id: string
  title: string
  hint: string
  // custom fields
  imageUrl?: string
  route?: string
}
