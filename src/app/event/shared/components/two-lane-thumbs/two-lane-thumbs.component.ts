import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { GeneralConstants } from '../../constants';
import { Thumbnail } from './thumbnail.interface';
import { Navigation } from 'selenium-webdriver';
import { transformToSlicedArray } from '../../utils';
import { GeneralDictionary } from '../../../shared/constants';

@Component({
  selector: 'app-two-lane-thumbs',
  templateUrl: './two-lane-thumbs.component.html',
  styleUrls: ['./two-lane-thumbs.component.scss']
})
export class TwoLaneThumbsComponent implements OnInit {
  private placeholder = GeneralConstants.PLACEHOLDER;
  private allThumbs: any[] = [];
  private thumbs: Thumbnail[] = [];
  private position = 0;
  private MAX_PER_LINE = 9;
  private transformed = [];
  private messageNotFound: String = GeneralDictionary.RESULT_MESSAGES.PROPERTIES_FILTERED_BY_CLIENT_ARTIST_NOT_FOUND;
  private showAllText: String = GeneralDictionary.FORM_LABELS.showAll;

  @Input() title: string
  @Input() set setItems(thumbs: Thumbnail[]) {
    this.allThumbs = transformToSlicedArray(thumbs, this.MAX_PER_LINE * 2)
    this.thumbs = this.allThumbs[0]
  }
  @Input() showAllDisplayed = true;
  @Input() name: string
  @Input() hasPrev = false;
  @Input() hasNext = true;
  @Output() navigation: EventEmitter<any> = new EventEmitter();
  @Output() showAllEvent: EventEmitter<any> = new EventEmitter();
  @Input() loading = false;

  constructor() { }

  ngOnInit() { }

  prevItems() {
    if (this.position > 0) {
      this.position--
      this.thumbs = this.allThumbs[this.position];
    }
    this.navigation.emit(-1)
  }

  nextItems() {
    if (this.position + 1 <= this.allThumbs.length) {
      this.position++
      this.thumbs = this.allThumbs[this.position]
    }
    this.navigation.emit(1)
  }

  showAll() {
    this.showAllEvent.emit(null)
  }
}
