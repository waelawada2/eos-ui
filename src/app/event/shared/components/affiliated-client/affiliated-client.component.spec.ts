import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffiliatedClientComponent } from './affiliated-client.component';

describe('AffiliatedClientComponent', () => {
  let component: AffiliatedClientComponent;
  let fixture: ComponentFixture<AffiliatedClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffiliatedClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffiliatedClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
