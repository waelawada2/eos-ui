import { AfterViewInit, Component, OnInit, Input, OnDestroy } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { DialogService } from 'ng2-bootstrap-modal'
import { NotificationsService } from 'angular2-notifications'
import { FormsDictionary, GeneralConstants, GeneralDictionary } from './../../constants'
import { autocompleListFormatter, convertToAutoformatCompatibleObject, updateValueAndValidity, setValidators } from './../../utils'
import { noRequiredObjectValidator } from '../../validators'
import { ClientSourceService, ClientSourceTypeService, ExternalClientService, AffiliatedTypeService } from '../../services'
import { AffiliatedType, ClientSource, ClientSourceType, ExternalClient } from '../../models'
import { ClientDialogComponent } from '../client/client.dialog.component'
import { Subscription } from 'rxjs/Subscription'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of'
import * as _ from 'lodash'

@Component({
  providers: [ExternalClientService],
  selector: 'app-affiliated-client',
  templateUrl: './affiliated-client.component.html',
  styleUrls: [
    './../../styles/forms.scss',
    './affiliated-client.component.scss',
  ],
})
export class AffiliatedClientComponent implements AfterViewInit, OnDestroy, OnInit {
  public dictionary = FormsDictionary.CLIENT;
  public affiliatedClient: FormGroup;
  public formEventError = {};
  public listFormatter = autocompleListFormatter;
  public sourceTypes = [];
  public sourceNames = [];
  public typeList = [];
  public disable = true;
  public validateAffiliated = false;

  affiliatedTypeSubscription: Subscription;
  clientSourceTypeSubscription: Subscription
  clientSourceSubscription: Subscription;
  clientIdSubscription: Subscription;
  standarValidatorMap = {
    clientSourceType: noRequiredObjectValidator(true),
    clientSource: noRequiredObjectValidator(true),
    affiliatedType: noRequiredObjectValidator(true),
    clientId: [Validators.maxLength(10), Validators.required]
  }

  searchValidatorMap = {
    clientSourceType: noRequiredObjectValidator(),
    clientSource: noRequiredObjectValidator(),
    affiliatedType: noRequiredObjectValidator(),
  }

  private updating = false;
  private standarValidatorsSet = false
  private clientId = null
  subscription: Subscription
  affiliatedClientSubscription: Subscription
  formSubscription: Subscription
  clientErrorSubscription: Subscription

  @Input()
  parentForm: FormGroup;

  @Input()
  private disabled = false;

  @Input() set validate(_validate) {
    this.validateAffiliated = _validate;
  }

  constructor(
    private fb: FormBuilder,
    private clientSourceService: ClientSourceService,
    private clientSourceTypeService: ClientSourceTypeService,
    private affiliatedTypeService: AffiliatedTypeService,
    private dialogService: DialogService,
    private externalClientService: ExternalClientService,
    protected notificationSvc: NotificationsService,
  ) {
  }

  ngOnInit() {
    this.createFormGroup();
  }

  ngAfterViewInit() {
    this.subscribeToForm();

    this.clientSourceSubscription = (<Observable<ClientSource[]>>this.clientSourceService.elements)
      .debounceTime(100)
      .subscribe({ next: (data) => this.sourceNames = data });

    this.clientSourceTypeSubscription = (<Observable<ClientSourceType[]>>this.clientSourceTypeService.elements)
      .debounceTime(100)
      .subscribe({ next: (data) => this.sourceTypes = data });

    this.affiliatedTypeSubscription = (<Observable<AffiliatedType[]>>this.affiliatedTypeService.elements)
      .debounceTime(100)
      .subscribe({ next: (data) => this.typeList = data });

    this.subscribeToFormOptionalData();
    this.parentForm.addControl('affiliatedClient', this.affiliatedClient);

    this.subscribeToClientErrors();
  }

  subscribeToFormOptionalData() {
    this.formSubscription = this.affiliatedClient
      .valueChanges
      .filter(() => this.validateAffiliated)
      .filter(() => !this.updating)
      .map((data) => {
        this.updating = true;
        return data
      })
      .debounceTime(200)
      .map(this.validateAffiliatedForms.bind(this))
      .debounceTime(200)
      .subscribe(() => this.updating = false)
  }
  validateClientFields() {
    this.validateAffiliatedForms(this.affiliatedClient.value);
  }

  validateAffiliatedForms({ clientSourceType, clientSource, clientId, affiliatedType }) {
    const hasEnteredData = (!!clientSourceType || !!clientSource || !!clientId || !!affiliatedType)
    if (!this.standarValidatorsSet && hasEnteredData) {
      setValidators(this.affiliatedClient, this.standarValidatorMap);
      this.standarValidatorsSet = true
    } else if (this.standarValidatorsSet && !hasEnteredData) {
      setValidators(this.affiliatedClient, [])
      this.standarValidatorsSet = false
    }
  }

  subscribeToForm() {
    this.subscription = this.affiliatedClient.valueChanges.debounceTime(100)
      .subscribe(this.updateAffiliatedStatusForm.bind(this));
    this.affiliatedClientSubscription = this.affiliatedClient.get('clientSource').valueChanges.debounceTime(400)
      .subscribe(this.clientSourceSearch.bind(this))
  }

  ngOnDestroy() {
    this.clientSourceSubscription.unsubscribe();
    this.clientSourceTypeSubscription.unsubscribe();
    this.affiliatedTypeSubscription.unsubscribe();
    this.formSubscription.unsubscribe();
    this.subscription.unsubscribe();
    this.clientErrorSubscription.unsubscribe();
  }

  resetFields() {
    Object.keys(this.searchValidatorMap).forEach(key => {
      const field = this.affiliatedClient.get(key);
      field.reset();
    })
  }

  createFormGroup() {
    this.affiliatedClient = this.fb.group({
      affiliatedType: [''],
      clientId: [''],
      clientKcm: [''],
      clientName: [''],
      clientSource: [''],
      clientSourceType: [''],
    });
    if (this.disabled) {
      this.affiliatedClient.disable();
    }
  }

  updateAffiliatedStatusForm() {
    this.formEventError = _.reduce(this.affiliatedClient.controls, (prev, value, index) => {
      if (value.invalid) {
        const message = _.get(this.dictionary.VALIDATION, `${_.toUpper(_.snakeCase(index))}`);
        const pairs = _.toPairs(value.errors);
        prev[index] = _.get(message, pairs[0][0]);
      }
      return prev
    }, {});
  }

  setFormInfo(data) {
    this.clientId = _.get(data, 'affiliatedClient.clientId');
    this.affiliatedClient.reset()

    if (this.clientId) {
      this.externalClientService.getElements({}, this.clientId);
      (<Observable<ExternalClient[]>>this.externalClientService.elements)
        .skip(1)
        .take(1)
        .do(() => this.resetClient())
        .filter((elements) => !!elements)
        .concatMap((elements: ExternalClient[]) => elements)
        .take(1)
        .subscribe(this.setAffiliatedClient.bind(this),
          null,
          () => this.patchMetadaValues(data))
    }

    this.validate = true;
  }

  subscribeToClientErrors() {
    this.clientErrorSubscription = this.externalClientService.error
      .skip(1)
      .filter((error) => !!error)
      .do(() => this.resetClient(this.clientId))
      .subscribe((error) => {
        this.notificationSvc.error(GeneralDictionary.NOTIFICATIONS.error,
          GeneralDictionary.NOTIFICATIONS.ERR_NOT_FOUND_CLIENT(this.clientId),
          GeneralConstants.NOTIFICATIONS.OPTIONS)
      })
  }

  patchMetadaValues(data) {
    this.affiliatedClient.patchValue({
      clientSourceType: convertToAutoformatCompatibleObject(_.get(data, 'affiliatedClient.clientSourceType'), 'name'),
      clientSource: convertToAutoformatCompatibleObject(_.get(data, 'affiliatedClient.clientSource'), 'name'),
      affiliatedType: convertToAutoformatCompatibleObject(_.get(data, 'affiliatedClient.affiliatedType'), 'name'),
    });
  }

  eventScroll(event) {
    const totalElements = this.clientSourceService.page.value.totalElements;
    if (totalElements > this.sourceNames.length) {
      this.clientSourceService.getNextElements();
    }
  }

  observableSource = (): Observable<ClientSource[]> => {
    return <Observable<ClientSource[]>>this.clientSourceService.elements;
  }

  clientSourceSearch(data) {
    // Search data when the input have values for filter;
    const search = data && typeof data === 'string' ? { name: data } : null;
    this.clientSourceService.getElements(search);
  }

  setAffiliatedClient(value) {
    this.affiliatedClient.patchValue({
      clientId: _.get(value, 'id'),
      clientKcm: convertToAutoformatCompatibleObject(_.get(value, 'kcm')),
      clientName: _.get(value, 'name')
    })
  }

  resetClient(id = null) {
    this.setAffiliatedClient({ id, kcm: null, clientName: null })
  }

  addClient() {
    this.dialogService.addDialog(ClientDialogComponent).take(1)
      .filter(response => !!response)
      .subscribe(this.setAffiliatedClient.bind(this))
  }

}
