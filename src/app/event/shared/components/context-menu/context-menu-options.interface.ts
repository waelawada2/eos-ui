import { Subject } from 'rxjs/Subject';

export interface ContextMenuOptions {
  id: string,
  text: string,
  subject: Subject<any>,
}
