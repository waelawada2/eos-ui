import { Component, Input, HostListener } from '@angular/core';
import { ContextMenuService } from './context-menu.service';
import { ContextMenuOptions } from './context-menu-options.interface'

@Component({
  styleUrls: ['./context-menu.component.scss'],
  templateUrl: './context-menu.component.html',
  selector: 'app-context-menu',
})
export class ContextMenuComponent {

  private x = 0;
  private y = 0;
  private open = false;
  private entries: ContextMenuOptions[];
  private item: any;

  public constructor(private menuService: ContextMenuService) {
    menuService.display.subscribe(this.handleDisplay.bind(this))
  }

  @HostListener('document:click')
  clickout() {
    this.close()
  }

  handleDisplay(result: { event: MouseEvent, obj: ContextMenuOptions[], item: any }) {
    this.x = result.event.pageX;
    this.y = result.event.pageY;
    this.entries = result.obj;
    this.open = true;
    this.item = result.item;
  }

  close() {
    this.open = false;
  }

  entryClicked(entry: ContextMenuOptions, event: MouseEvent) {
    entry.subject.next(this.item)
    event.preventDefault();
  }

}
