import { Directive, Input, HostListener } from '@angular/core';
import { ContextMenuService } from './context-menu.service'
import { ContextMenuOptions } from './context-menu-options.interface'
@Directive({
  selector: '[appContextMenu]',
})
export class ContextMenuDirective {

  @Input('appContextMenu') links: { options: ContextMenuOptions[], item: any };

  constructor(private menuService: ContextMenuService) {
  }

  @HostListener('contextmenu', ['$event'])
  rightClicked(event: MouseEvent) {
    this.menuService.open(event, this.links)
    event.preventDefault();
  }

}
