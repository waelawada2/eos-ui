import { ContextMenuDirective } from './context-menu.directive';
import { ContextMenuComponent } from './context-menu.component';
import { ContextMenuService } from './context-menu.service';

const CONTEXT_MENU = [
  ContextMenuComponent,
  ContextMenuDirective,
  ContextMenuService,
]
export {
  ContextMenuComponent,
  ContextMenuDirective,
  ContextMenuService,
  CONTEXT_MENU,
}
