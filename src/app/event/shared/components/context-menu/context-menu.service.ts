import { Injectable } from '@angular/core';
import { ContextMenuOptions } from './context-menu-options.interface';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ContextMenuService {

  public display: Subject<{ event: MouseEvent, obj: ContextMenuOptions[], item: any }> =
    new Subject<{ event: MouseEvent, obj: ContextMenuOptions[], item: any }>();
  private options: ContextMenuOptions[] = []

  public setOptions(options: ContextMenuOptions[]) {
    this.options = options;
  }

  public open(event: MouseEvent, obj: { options: ContextMenuOptions[], item: any }) {
    this.display.next({ event, obj: obj.options, item: obj.item });
  }

}
