import { async, ComponentFixture, ComponentFixtureAutoDetect, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ItemDetailsFormComponent } from './item-details.form'
import { FormsModule, ReactiveFormsModule, FormControl, FormGroup } from '@angular/forms';

describe('ItemDetailsForm', () => {
  let component: ItemDetailsFormComponent;
  let fixture: ComponentFixture<ItemDetailsFormComponent>;
  const str20 = Array(22).join('a');
  const str200 = Array(2002).join('a');

  const fb: FormBuilder = new FormBuilder();

  function getParentForm() {
    return fb.group({
    });
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ItemDetailsFormComponent],
      schemas : [ CUSTOM_ELEMENTS_SCHEMA ],
      imports: [ FormsModule, ReactiveFormsModule ],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemDetailsFormComponent);
    component = fixture.componentInstance;
    component.parentForm = getParentForm();
    component.ngOnInit();
  })

  it('Should Form be valid when empty', () => {
    const form = component.form;
    expect(form.valid).toBe(true);
    fixture.detectChanges();
  });

  it('Should form be invalid when item Id is longer than 20 characters', () => {
    const form = component.form;
    form.patchValue({ itemId: str20 });
    expect(form.valid).toBe(false);
    fixture.detectChanges();
  });

  it('Should form be invalid when notes is longer than 200 characters', () => {
    const form = component.form;
    form.patchValue({ itemId: '', notes: str200 });
    expect(form.valid).toBe(false);
    fixture.detectChanges();
  });

});
