import { Component, Input, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormsDictionary } from '../../constants';
import * as _ from 'lodash';

@Component({
  selector: 'app-item-details-form',
  templateUrl: './item-details.form.html',
  styleUrls: ['../../styles/forms.scss'],
})
export class ItemDetailsFormComponent implements OnInit, AfterViewInit {

  dictionary = FormsDictionary;
  dictionatyPublication = FormsDictionary.PUBLICATION.LABELS;
  form: FormGroup;
  formErrors: any = {};

  @Input()
  parentForm: FormGroup;

  @Input()
  hideElements: any[] = [];

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      itemId: ['', Validators.maxLength(20)],
      notes: ['', Validators.maxLength(200)],
    })
  }

  ngAfterViewInit() {
    this.form.valueChanges.subscribe(this.updateErrors.bind(this));
    this.setParentToForm(this.parentForm);

  }

  setParentToForm(parent) {
    if (this.form && parent) {
      parent.addControl('itemDetails', this.form)
    }
  }

  updateErrors() {
    const notes = this.form.get('notes');
    const itemId = this.form.get('itemId');
    this.formErrors = _.reduce(this.form.controls, (prev, value, index) => {
      if (value.dirty && value.invalid) {
        const message = _.get(this.dictionary.VALIDATION, `${_.toUpper(_.snakeCase(index === this.dictionatyPublication.NOTES ?
          this.dictionatyPublication.NOTES_ITEMS : index))}`);
        const pairs = _.toPairs(value.errors)
        prev[index] = _.get(message, pairs[0][0])
      }
      return prev
    }, {});
  }
}
