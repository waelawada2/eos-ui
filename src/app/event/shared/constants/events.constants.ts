export default {
  lotTableHeaders: [
    'Lot',
    'Objects',
    'Values',
    'Clients',
    'Purchaser'
  ],
  imageDescription: 'Untitled Unknown Artist',
  cataloguing: `Andy Warhol (1928-1987)
      MAO
      signed on the reverse
      acrylic and silkscreen ink on canvas
      1972-1972
      1AP of 3 + 2AP`,
  objectTableOptions: {
    header: true,
    embedded: false,
  },
  itemsToShow: 4,
  scrollEvent: {
    title: 'Scroll in progress',
    body: (items: number, total: number) => {
      return `showing ${items} of ${total}`;
    },
    type: 'tableInfo'
  },
  filterEvent: {
    title: 'Filtering Items',
    bodyNoResult: 'no results were found',
    body: (items: number) => {
      return `${items} results were found`;
    },
    type: 'tableInfo'
  },
  /* used temporary while we integrate with services*/
  pricingHeaders: [
    'object',
    'pricing',
    'pipeline',
  ],
  pricingIdLabel: [
    ['valuationOrigin', 'valuationOriginId'],
    ['valuationType', 'valuationTypeId'],
  ],
  pricingAttrExceptions: [
    'valuationId',
    'valuationType',
    'valuationOrigin',
  ],
  consignmentPurchaseEventList: [
    { id: 'PURCHASE', name: 'Purchase' },
    { id: 'CONSIGNMENT', name: 'Consignment' }
  ],
  PUBLICATION_TABLE: {
    IN: ' in '
  },
  NOTIFICATION: {
    notificationsOptions: {
      timeOut: 5000,
      showProgressBar: true,
      pauseOnHover: false,
      clickToClose: true
    },
  },
  NAVIGATION: {
    buttonStatus: {
      save: true,
      search: false,
      new: false,
      clearSearch: false,
    },
  },
  EXPORT_STATUS: {
    progress: 'IN_PROGRESS',
    completed: 'COMPLETED'
  },
  itemImage: {
    showImage: false,
    xPos: '',
    yPos: '',
    newImage: ''
  },
}
