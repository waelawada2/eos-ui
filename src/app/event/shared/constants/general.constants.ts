export default {
  DATE_PICKER_CONFIG: {
    containerClass: 'theme-default datepicker',
    showWeekNumbers: false,
    dateInputFormat: 'DD/MMM/YYYY',
  },
  ENDPOINTS: {
    BASE_URL: 'http://localhost:9980/event-service/v1/',
    CLIENT: {
      AFFILIATED_TYPE: 'affiliated-types',
      CLIENT: 'clients',
      CLIENT_SEGMENT: 'marketing-segments',
      CLIENT_SOURCE: 'client-sources',
      CLIENT_SOURCE_TYPE: 'client-source-types',
      CLIENT_STATUS: 'client-statuses',
      CLIENT_SUFFIX: 'suffixes',
      CLIENT_TITLE: 'titles',
      CLIENT_TYPE: 'client-types',
      GENDERS: 'genders',
      KCM: 'key-client-managers',
    }
  },
  DATE_FORMAT: {
    formatDate: 'DDMMYYYY',
    formatDateTime: 'YYYY-MM-DD',
    formatDateUIDisplay: 'DD-MMM-YYYY',
    formatDatePresentationView: 'dd MMMM yyyy'
  },
  ENTITY_TYPE: {
    ITEM: 'Item',
    EVENT: 'Event',
    OBJECT: 'Object',
    CLIENT: 'Client'
  },
  EVENT_TYPES: {
    PRICING: 'PRICING',
    PUBLICATION: 'PUBLICATION',
    TRANSACTION: 'TRANSACTION',
  },
  PLACEHOLDER: 'https://image.ibb.co/c0h3MH/New_Project.png',
  ITEM_TYPES: {
    AUCTION: 'AUCTION',
    PRIVATE_SALE: 'PRIVATE_SALE'
  },
  CURRENCY_DEFAULTS: {
    'currencyCode': 'USD',
    'currencySymbol': '$',
    'noFractions': '1.0',
    'twofractions': '1.2-2'
  },
  NOTIFICATIONS: {
    OPTIONS: {
      'timeOut': 5000,
      'showProgressBar': false,
      'pauseOnHover': false,
      'clickToClose': true
    },
    SETUP: {
      'position': ['top', 'right'],
      'timeOut': 0,
      'lastOnBottom': true,
    },
    TYPES: {
      error: 'error',
      success: 'success',
    }
  },
  NAVIGATION: {
    BUTTONS: {
      'save': true,
      'search': false,
      'new': false,
      'clearSearch': true,
    },
  },
  ITEM_IMAGE: {
    'showImage': false,
    'xPos': '',
    'yPos': '',
    'newImage': ''
  },
  SORTING: {
    CLIENT: {
      ASC: [
        'individual.lastName,asc',
        'individual.firstName,asc',
        'name,asc',
      ],
      DESC: [
        'individual.lastName,desc',
        'individual.firstName,desc',
        'name,desc',
      ]
    },
    PRICING: {
      ASC: [
        'date,asc',
        'mainClient.clientSource,asc',
        '_metadata.created,asc',
      ],
      DESC: [
        'date,desc',
        'mainClient.clientSource,asc',
        '_metadata.created,asc',
      ]
    },
    PRICING_ITEM: {
      ASC: [
        'eventDate,asc',
        'event.mainClient.clientSource.name,asc',
        '_metadata.created,asc',
      ],
      DESC: [
        'eventDate,desc',
        'event.mainClient.clientSource.name,asc',
        '_metadata.created,asc',
      ],
    },
    PUBLICATION: {
      ASC: [
        'date,asc',
        'establishment,asc',
        '_metadata.created,asc'
      ],
      DESC: [
        'date,desc',
        'establishment,asc',
        '_metadata.created,asc',
      ],
    },
    PUBLICATION_ITEM: {
      ASC: [
        'eventDate,desc',
        'illustration,asc',
        '_cataloguingPage,asc',
        '_metadata.created,asc',
      ],
      DESC: [
        'eventDate,asc',
        'illustration,asc',
        '_cataloguingPage,asc',
        '_metadata.created,asc',
      ],
    },
    TRANSACTION: {
      ASC: [
        'date,asc',
        'establishment,asc',
        '_metadata.created,asc',
      ],
      DESC: [
        'date,desc',
        'establishment,asc',
        '_metadata.created,asc',
      ]
    },
    TRANSACTION_ITEM: {
      ASC: [
        'eventDate,asc',
        'lotNumber,asc',
        '_metadata.created,asc',
      ],
      DESC: [
        'eventDate,desc',
        'lotNumber,asc',
        '_metadata.created,asc',
      ]
    },
  },
  IMPORT_TYPES: {
    consignment_purchase: 'CONSIGNMENT_PURCHASE_EVENT',
    transaction: 'TRANSACTION_EVENT',
    pricing: 'PRICING_EVENT',
    publication: 'PUBLICATION_EVENT',
    pricing_item: 'PRICING_ITEM',
    publication_item: 'PUBLICATION_ITEM',
    transaction_item: 'TRANSACTION_ITEM'
  },
  PRICING_EVENT: {
    LOWVALUE: 'lowValue',
    HIGHVALUE: 'highValue',
  },
  OWNERSHIP: {
    EMPLOYEE: 'Employee',
    MEDIA: 'Media',
    ACTIVE: 'Active',
    PUBLICATION: 'Publication',
    DEFAULT_VALUES: {
      DEFAULTVALUE: 'defaultValue',
      SETDEFAULTVALUE: 'setDefaultValue'
    }
  }
}
