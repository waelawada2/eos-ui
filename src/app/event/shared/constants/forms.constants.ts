
export default {
  CLIENT_DECEASED: [{
    value: true,
    name: 'YES'
  }, {
    value: false,
    name: 'NO',
  }],
  CLIENT_ORIGIN: [{
    name: 'EOS',
    value: false,
  }, {
    name: 'Client System',
    value: true
  }],
  transactionTypes: [
    { id: 'AUCTION', name: 'Auction' },
    { id: 'PRIVATE_SALE', name: 'Private Sale' }
  ],
  pricingTypes: [
    { id: 'VT01', text: 'Reactive' },
    { id: 'VT02', text: 'Proactive' },
    { id: 'VT03', text: 'Retroactive' },
  ],
  pricingOrigin: [
    { id: 'V01', text: 'General' },
    { id: 'V02', text: 'Valuation' },
    { id: 'V03', text: 'External' },
  ],
}
