import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsignmentPurchaseComponent } from './consignment-purchase.component';

describe('ConsignmentPurchaseComponent', () => {
  let component: ConsignmentPurchaseComponent;
  let fixture: ComponentFixture<ConsignmentPurchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsignmentPurchaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsignmentPurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
