import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsignmentPurchaseTableComponent } from './consignment-purchase-table.component';

describe('ConsignmentPurchaseTableComponent', () => {
  let component: ConsignmentPurchaseTableComponent;
  let fixture: ComponentFixture<ConsignmentPurchaseTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsignmentPurchaseTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsignmentPurchaseTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
