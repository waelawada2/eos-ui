import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { ItemsService } from './../../shared/services';
import { EVENTCONST, GeneralConstants, GeneralDictionary, TableDictionary, FormsDictionary } from '../../shared/constants';
import { onMouseOutImage, onMouseOverImage, cleanTransactionItems } from './../../shared/utils';
import { ContextMenuOptions } from '../../shared/components/context-menu/context-menu-options.interface'
import { Subject } from 'rxjs/Subject';
import * as _ from 'lodash';

@Component({
  selector: 'app-consignment-purchase-table',
  templateUrl: './consignment-purchase-table.component.html',
  styleUrls: [
    './../../shared/styles/tables.scss',
    './../../shared/styles/panels.scss',
    './consignment-purchase-table.component.scss'
  ],
})
export class ConsignmentPurchaseTableComponent implements OnInit {
  public items: any;
  public filter: string;
  public defaultImagePlaceholder: string = GeneralConstants.PLACEHOLDER;
  public defaultCataloguing: string = EVENTCONST.cataloguing;
  public cataloguingItems: Object = TableDictionary.PRICING_LABELS;
  public tableLabels: Object = FormsDictionary.CONSIGNMENT_PURCHASE;
  private tableDictionary = TableDictionary.PUBLICATION;
  public onMouseOutImage = onMouseOutImage;
  public onMouseOverImage = onMouseOverImage;
  public dictionary = FormsDictionary;

  private type: string = null;

  private menuOptions: ContextMenuOptions[] = [{
    text: GeneralDictionary.CONTEXT_MENU.REMOVE(GeneralConstants.ENTITY_TYPE.ITEM),
    id: 'menu-delete',
    subject: new Subject<any>()
  }];

  @Input()
  set eventItems(items: any) {
    this.items = items;
    if (this.items.length > 0) {
      this.type = this.items[0].type
    }
  }

  @Output() moreItems: EventEmitter<any> = new EventEmitter();
  @Output() removeItem: EventEmitter<any> = new EventEmitter();

  constructor(
    protected itemsService: ItemsService
  ) { }

  ngOnInit() {
    this.menuOptions[0].subject.subscribe(this.deleteItem.bind(this));
  }

  lotTableScrolled() {
    this.moreItems.emit();
  }

  onFilter(word) {
    this.filter = word;
  }

  isNumber(value) {
    return !isNaN(parseInt(value, 10));
  }

  deleteItem(item) {
    this.removeItem.emit(item)
  }

}
