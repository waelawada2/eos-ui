import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsignmentPurchaseAddItemFormComponent } from './consignment-purchase-add-item-form.component';

describe('ConsignmentPurchaseAddItemFormComponent', () => {
  let component: ConsignmentPurchaseAddItemFormComponent;
  let fixture: ComponentFixture<ConsignmentPurchaseAddItemFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsignmentPurchaseAddItemFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsignmentPurchaseAddItemFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
