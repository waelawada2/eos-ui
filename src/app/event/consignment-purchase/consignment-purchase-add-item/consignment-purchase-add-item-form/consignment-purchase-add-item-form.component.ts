import { Component, Input, forwardRef, OnInit, ViewChild } from '@angular/core';
import { ClientFormComponent } from './../../../shared/components'
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { FormsDictionary, EVENTCONST } from './../../../shared/constants';
import { autocompleListFormatter, convertToAutoformatCompatibleObject } from './../../../shared/utils';

import * as _ from 'lodash';

@Component({
  selector: 'app-consignment-purchase-add-item-form',
  templateUrl: './consignment-purchase-add-item-form.component.html',
  styleUrls: ['./consignment-purchase-add-item-form.component.scss']
})
export class ConsignmentPurchaseAddItemFormComponent implements OnInit {
  consignmentPurchaseAddItemForm: FormGroup;
  public dictionary = FormsDictionary.CONSIGNMENT_PURCHASE;
  public eventList = EVENTCONST.consignmentPurchaseEventList;
  public formEventError = {};
  public clientTypeMain = true;
  public validationMessages = FormsDictionary.VALIDATION;
  public listFormatter = autocompleListFormatter;

  @ViewChild(forwardRef(() => ClientFormComponent)) clientForm: ClientFormComponent;

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.consignmentPurchaseAddItemForm = this.fb.group({
      eventType: ['', Validators.required],
      eventId: ['']
    });
  }

  getFormData() {
    this.consignmentPurchaseAddItemForm.get('eventType').enable();
    const form = (_.clone(this.consignmentPurchaseAddItemForm.value));
    this.consignmentPurchaseAddItemForm.get('eventType').disable();
    return form;
  }

  setFormData(subtype) {
    if (subtype) {
      const type = convertToAutoformatCompatibleObject(_.get(_.filter(this.eventList, ['id', subtype]), [0]), 'name');
      this.consignmentPurchaseAddItemForm.patchValue({
        eventType: type
      });
      this.consignmentPurchaseAddItemForm.get('eventType').disable();
    }

  }

  reset() {
    this.consignmentPurchaseAddItemForm.reset();
  }


  onValueChanged(data?: any) {
    this.formEventError = _.reduce(this.consignmentPurchaseAddItemForm.controls, (prev, value, index) => {
      if (value.dirty && value.invalid) {
        const message = _.get(this.validationMessages, `${_.toUpper(_.snakeCase(index))}`);
        const pairs = _.toPairs(value.errors);
        if (pairs[0]) {
          prev[index] = _.get(message, pairs[0][0]);
        }
      }
      return prev
    }, {});
  }

}
