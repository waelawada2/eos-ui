import { ConsignmentPurchaseAddItemTableComponent } from './consignment-purchase-add-item-table/consignment-purchase-add-item-table.component';
import { ConsignmentPurchaseAddItemFormComponent } from './consignment-purchase-add-item-form/consignment-purchase-add-item-form.component'
import { ConsignmentPurchaseAddItemComponent } from './consignment-purchase-add-item.component';

const CONSIGNMENT_PURCHASE_ADD_ITEMS_COMPONENTS = [
  ConsignmentPurchaseAddItemFormComponent,
  ConsignmentPurchaseAddItemComponent,
  ConsignmentPurchaseAddItemTableComponent,
];

export {
  ConsignmentPurchaseAddItemComponent,
  ConsignmentPurchaseAddItemFormComponent,
  ConsignmentPurchaseAddItemTableComponent,
  CONSIGNMENT_PURCHASE_ADD_ITEMS_COMPONENTS,
}
