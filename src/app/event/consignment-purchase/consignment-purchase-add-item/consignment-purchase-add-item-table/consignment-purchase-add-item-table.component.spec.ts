import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsignmentPurchaseAddItemTableComponent } from './consignment-purchase-add-item-table.component';

describe('ConsignmentPurchaseAddItemTableComponent', () => {
  let component: ConsignmentPurchaseAddItemTableComponent;
  let fixture: ComponentFixture<ConsignmentPurchaseAddItemTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsignmentPurchaseAddItemTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsignmentPurchaseAddItemTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
