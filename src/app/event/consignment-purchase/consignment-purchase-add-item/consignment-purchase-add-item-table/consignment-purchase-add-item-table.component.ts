import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormsDictionary } from '../../../shared/constants';
import { cleanTransactionItems } from './../../../shared/utils';

@Component({
  selector: 'app-consignment-purchase-add-item-table',
  templateUrl: './consignment-purchase-add-item-table.component.html',
  styleUrls: [
    './../../../shared/styles/tables.scss',
    './../../../shared/styles/panels.scss',
    './consignment-purchase-add-item-table.component.scss'
  ]
})
export class ConsignmentPurchaseAddItemTableComponent {
  items: any = [];
  dictionary = FormsDictionary;

  @Input()
  set eventItems(items: any){
    this.items = cleanTransactionItems(items);
  }

  @Input() subEventType;

  @Output() event: EventEmitter<any> = new EventEmitter<any>();

  constructor(
  ) { }

  linkEventToItem( eventSelected ){
     this.event.emit(eventSelected);
  }

}

