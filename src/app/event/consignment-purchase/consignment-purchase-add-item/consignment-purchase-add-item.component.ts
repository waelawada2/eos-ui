import { AfterViewInit, Component, forwardRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router, RouterLink, ActivatedRoute } from '@angular/router';
import { AddEventsToItemsService, ItemsService } from './../../shared/services';
import { ConsignmentPurchaseAddItemFormComponent } from './consignment-purchase-add-item-form/consignment-purchase-add-item-form.component';
import { GeneralDictionary } from './../../shared/constants';
import { FormsDictionary, EVENTCONST, GeneralConstants } from './../../shared/constants';
import { Observable } from 'rxjs/Observable';
import { DialogService } from 'ng2-bootstrap-modal';
import { ConfirmComponent } from './../../shared/components';
import { Subscription } from 'rxjs/Subscription';
import { getTransactionItemData } from '../../shared/utils';
import { NotificationsService } from 'angular2-notifications';
import * as _ from 'lodash';

@Component({
  selector: 'app-consignment-purchase-add-item',
  host: { '(window:keydown)': 'hotkeys($event)' },
  templateUrl: './consignment-purchase-add-item.component.html',
  styleUrls: [
    './consignment-purchase-add-item.component.scss'
  ],
  providers: [
    ItemsService,
  ]
})
export class ConsignmentPurchaseAddItemComponent implements AfterViewInit, OnDestroy, OnInit {
  dictionary = GeneralDictionary.NAVBAR;
  notificationDictionary = GeneralDictionary.NOTIFICATIONS;
  subEventType = '';
  eventList = EVENTCONST.consignmentPurchaseEventList;
  eventId = '';
  itemId = '';
  eventItems: any[] = [];
  item: any = {};
  eventType = '';
  subscription: Subscription;
  subscriptionItem: Subscription;
  subscriptionRoute: any;
  buttonlabels = GeneralDictionary.BUTTONS;
  options = GeneralConstants.NOTIFICATIONS.SETUP;
  public constantsOwnership = GeneralConstants.OWNERSHIP.DEFAULT_VALUES;
  public setValuesEnabled;

  @ViewChild(forwardRef(() => ConsignmentPurchaseAddItemFormComponent)) detailForm: ConsignmentPurchaseAddItemFormComponent;

  constructor(
    protected eventsService: AddEventsToItemsService,
    protected itemsService: ItemsService,
    private route: ActivatedRoute,
    private router: Router,
    private dialogService: DialogService,
    private notificationSvc: NotificationsService,
  ) { }

  ngOnInit() {
    this.subscriptionRoute = this.route.params.subscribe(params => {
      this.eventId = params['eventId'];
      this.itemId = params['itemId'];
      this.subEventType = params['subtype'];
      this.eventType = this.subEventType === 'CONSIGNMENT' ? 'consignmentEvent' : 'purchaseEvent';
      this.setValuesEnabled = this.route.snapshot.fragment;
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.detailForm.setFormData((this.subEventType))
      if (this.itemId) {
        this.subscriptionItem = this.itemsService.element
          .subscribe((item) => this.item = item)
        this.itemsService.getElement(this.itemId);
      }
    });
  }

  ngOnDestroy() {
    this.subscriptionRoute.unsubscribe();
    this.subscriptionItem.unsubscribe()
  }

  hotkeys(event) {
    if (event.keyCode == 70 && event.ctrlKey && event.altKey) {
      this.goToFilter();
    }
  }

  goToFilter() {
    const searchParams = this.getFormatedToSearch(_.clone(this.detailForm.getFormData()));
    this.getEventItems(searchParams);
  }

  goToAddEvent() {
    this.eventsService.formData = this.formatedToCreate();
    this.eventsService.item = this.item;
    this.eventsService.statusPage = 'PopUp';
    this.subscriptionItem.unsubscribe();
    if (this.setValuesEnabled === this.constantsOwnership.SETDEFAULTVALUE) {
      this.setValuesEnabled = this.constantsOwnership.DEFAULTVALUE
      this.router.navigate(['consignment-purchase', 'new'], { fragment: this.setValuesEnabled });
    } else {
      this.router.navigate(['consignment-purchase', 'new']);
    }
  }

  formatedToCreate() {
    const data = _.clone(this.detailForm.getFormData());
    const formatedData = {
      ..._.pick(data, ['eventType']),
      mainClient: _.omitBy(_.pick(data['client'], 'clientId', 'clientSource', 'clientSourceType', 'clientStatus', 'display'), _.isEmpty)
    };
    return _.omitBy(formatedData, (value) => _.isEmpty(value) && !_.isNumber(value) && !_.isBoolean(value));
  }

  addEventToItem(eventSelected) {
    let formatedData;
    if (this.eventType === 'TRANSACTION') {
      formatedData = getTransactionItemData(this.item);
      formatedData[this.eventType] = { id: eventSelected.id, type: eventSelected.type };
    } else {
      formatedData = {...this.item, id: null};
      formatedData[this.eventType] = eventSelected;
    }
    const disposable = this.dialogService.addDialog(ConfirmComponent, {
      title: this.notificationDictionary.ADD_EVENT_CONFIRM_TITLE,
      message: this.notificationDictionary.ADD_EVENT_CONFIRM,
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        this.doEditAction(formatedData).subscribe((response) => {
          this.returnToInitialSection();
        })
      }
      disposable.unsubscribe()
    });
  }

  getFormatedToSearch(data: any) {
    const searchValue = {};
    if (data) {
      searchValue['subEventType'] = data.eventType.id;
      searchValue['clientSourceId'] = data.client.clientSource ? data.client.clientSource.id : '';
      searchValue['clientSourceTypeId'] = data.client.clientSourceType ? data.client.clientSourceType.id : '';
      searchValue['clientStatusId'] = data.client.clientStatus ? data.client.clientStatus.id : '';
      searchValue['mainClientId'] = data.client.clientId;
      searchValue['eventId'] = data.eventId;
    }
    return _.omitBy(searchValue, (value) => _.isEmpty(value) && !_.isNumber(value) && !_.isBoolean(value));
  }

  close() {
    this.subscriptionItem.unsubscribe();
    this.returnToInitialSection();
  }

  returnToInitialSection() {
    switch (_.toLower(this.item.type)) {
      case _.toLower(GeneralDictionary.SECTIONS.PUBLICATION):
      case _.toLower(GeneralDictionary.SECTIONS.TRANSACTION):
        this.router.navigate([_.toLower(this.item.type), this.eventId]);
        break;
      default:
        break;
    }
  }

  getEventItems(searchParams) {
    const itemUrl = 'events/' + this.eventId + '/items';
    const options = GeneralConstants.NOTIFICATIONS.OPTIONS;
    if (itemUrl) {
      this.eventsService.getItemsByUrl(itemUrl, true, searchParams);
      this.eventsService.elements
        .skip(1)
        .take(1)
        .subscribe((items) => {
          this.eventItems = this.getEvents(items);
          if (this.eventItems.length == 0) {
            this.notificationSvc.error(GeneralDictionary.NOTIFICATIONS.error, GeneralDictionary.NOTIFICATIONS.notFound, options);
          }
        })
    }
  }

  getEvents(items) {
    return _.values(_.reduce(items, (result, item: any) => {
      const eventObj: any = _.get(item, this.eventType)
      if (eventObj) {
        _.set(result, eventObj.id, eventObj)
      }
      return result;
    }, {}));
  }

  doEditAction(data): Observable<any> {
    return this.itemsService.amend(data, this.itemId);
  }

  destroyMessage(event) {
    this.notificationSvc.remove();
  }
}
