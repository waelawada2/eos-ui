import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsignmentPurchaseAddItemComponent } from './consignment-purchase-add-item.component';

describe('ConsignmentPurchaseAddItemComponent', () => {
  let component: ConsignmentPurchaseAddItemComponent;
  let fixture: ComponentFixture<ConsignmentPurchaseAddItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsignmentPurchaseAddItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsignmentPurchaseAddItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
