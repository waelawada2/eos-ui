import { ConsignmentPurchaseFormComponent } from './consignment-purchase-form/consignment-purchase-form.component';
import { ConsignmentPurchaseTableComponent } from './consignment-purchase-table/consignment-purchase-table.component';
import { ConsignmentPurchaseComponent } from './consignment-purchase.component';
import { CONSIGNMENT_PURCHASE_ADD_ITEMS_COMPONENTS } from './consignment-purchase-add-item/consignment-purchase-add-item.facade';

const CONSIGNMENT_PURCHASE_COMPONENTS = [
  ConsignmentPurchaseFormComponent,
  ConsignmentPurchaseTableComponent,
  ConsignmentPurchaseComponent,
  CONSIGNMENT_PURCHASE_ADD_ITEMS_COMPONENTS
];

export {
  ConsignmentPurchaseFormComponent,
  ConsignmentPurchaseTableComponent,
  ConsignmentPurchaseComponent,
  CONSIGNMENT_PURCHASE_COMPONENTS,
  CONSIGNMENT_PURCHASE_ADD_ITEMS_COMPONENTS,
}
