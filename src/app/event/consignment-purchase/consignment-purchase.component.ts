import { AfterViewInit, Component, forwardRef, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BlockScreenService } from './../shared/services/block-screen/block-screen.service';
import { DialogService } from 'ng2-bootstrap-modal';
import { AddEventsToItemsService, ConsignmentPurchaseService, NavigationService, ItemsService } from './../shared/services';
import { ConfirmComponent } from './../shared/components';
import { NotificationsService } from 'angular2-notifications';
import { ConsignmentPurchaseFormComponent } from './consignment-purchase-form/consignment-purchase-form.component';
import { AffiliatedClient, Client, ConsignmentPurchase } from '../shared/models';
import { GeneralConstants, GeneralDictionary } from '../shared/constants';
import { Subscription } from 'rxjs/Subscription';
import { getTransactionItemData } from '../shared/utils';
import { SothebysLifeCycle, STATES, URLS } from '../sothebys-lifecycle.abstract';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { BUTTON_STATUS } from './../shared/components/navigation-header/navigation-header.button-status.interface';
import { formatStringWithStartCase } from '../shared/utils';
import * as _ from 'lodash';
@Component({
  selector: 'app-consignment-purchase',
  templateUrl: './consignment-purchase.component.html',
  styleUrls: [
    '../shared/styles/panels.scss',
    './consignment-purchase.component.scss',
  ],
})
export class ConsignmentPurchaseComponent extends SothebysLifeCycle implements AfterViewInit,
  OnDestroy {
  events: ConsignmentPurchase[];
  current: ConsignmentPurchase;
  generalDictionary = GeneralDictionary;
  eventItems: any[] = [];
  showTable = false;
  options = GeneralConstants.NOTIFICATIONS.SETUP;
  popPupEvent: any;
  popPupItem: any;
  PopPupState: any;
  currentSearch: any = {}
  currentSearchData: any = {}
  itemServiceSubscription: Subscription;
  validate = false;

  formSubscription: Subscription;

  protected getErrors: BehaviorSubject<any>;

  @ViewChild(forwardRef(() => ConsignmentPurchaseFormComponent)) eventDetailForm: ConsignmentPurchaseFormComponent;

  constructor(
    protected blockScreenService: BlockScreenService,
    protected cps: ConsignmentPurchaseService,
    protected createEvent: AddEventsToItemsService,
    protected dialogService: DialogService,
    protected itemsService: ItemsService,
    protected navService: NavigationService,
    protected notificationSvc: NotificationsService,
    protected route: ActivatedRoute,
    protected router: Router, ) {
    super();
    this.baseUrl = 'consignment-purchase';
    this.section = GeneralDictionary.SECTIONS.CONSIGNMENT_PURCHASE;
    this.getErrors = this.cps.error
    this.ableToCreate = false;
  }
  ngAfterViewInit() {
    this.route.params.take(1).subscribe(params => setTimeout(() => {
      if (params['id'] === URLS.NEW && this.createEvent.statusPage !== 'PopUp') {
        this.router.navigate(['consignment-purchase', 'search']).then(this.initialize.bind(this))
      } else {
        this.initialize();
      }
    }));
    const form = this.eventDetailForm.consignmentPurchaseForm;
    this.formSubscription = form.valueChanges.debounceTime(400).subscribe((data) => {
      this.validData.next(form.valid);
    });
  }

  initialize() {
    this.popPupEvent = this.createEvent.formData;
    this.popPupItem = this.createEvent.item;
    this.PopPupState = this.createEvent.statusPage;
    if (this.PopPupState === 'PopUp') {
      this.section = this.popPupEvent.eventType.name;
      setTimeout(() => {
        this.eventDetailForm.setFormData(this.popPupEvent, 'PopUp')
        this.validateForm(STATES.NEW);
      });
    } else {
      this.validateForm(STATES.SEARCH);
    }
    super.initialize()
  }
  ngOnDestroy() {
    super.ngOnDestroy()
    this.formSubscription.unsubscribe()
    if (this.itemServiceSubscription) {
      this.itemServiceSubscription.unsubscribe();
    }
  }
  validateForm(state) {
    switch (state) {
      case STATES.SEARCH:
        this.eventDetailForm.searchMode();
        break;
      default:
        this.eventDetailForm.standarMode();
        break;
    }
  }
  protected clearForm(restore) {
    this.showTable = false;
    this.updatePages(0, 0);
    this.eventDetailForm.reset();
    this.current = undefined;
    this.eventItems = [];
    this.currentSearch = {};
    this.loadPaginated = 0;
    if (restore) {
      this.eventDetailForm.setFormData(this.getFormatedData(this.currentSearchData));
    }
    this.currentSearchData = {};
    this.validateForm(this.state);
  }

  protected doSearchAction(id): BehaviorSubject<any> {
    this.currentSearch = !id ? {
      ... this.getFormatedToSearch(_.clone(this.eventDetailForm.getFormData())),
      id,
      size: this.pageSize
    } : null;
    this.currentSearchData = this.eventDetailForm.getFormData();
    this.navService.enterCustomMode({ new: BUTTON_STATUS.HIDDEN });
    this.cps.getElements(this.currentSearch, id);
    const elements = this.cps.elements;
    elements.takeUntil(this.stateObservable.skip(1)).subscribe({
      next: (formElements) => { this.updateEvent(formElements) }
    })
    this.cps.page.takeWhile(() => this.state === STATES.EDIT).subscribe({
      next: (dataPage) => {
        this.updatePages(dataPage.number, dataPage.totalElements)
      }
    })
    this.validate = false;
    return elements;
  }

  protected doCreateAction(): Observable<any> {
    const data = this.getFormatedData(_.clone(this.eventDetailForm.getFormData()));
    return this.cps.create(data);
  }

  protected doEditAction(): Observable<any> {
    const data: ConsignmentPurchase = this.getFormatedData(_.clone(this.eventDetailForm.getFormData()));
    return this.cps.amend(data, this.current['id']);
  }

  protected doDeleteAction(): Observable<any> {
    return this.cps.delete(this.current['id']);
  }

  protected doImportAction(file: File): Observable<any> {
    return this.cps.import(file, GeneralConstants.IMPORT_TYPES.consignment_purchase);
  }

  protected doExportAction(): Observable<any> {
    return this.cps.export(this.currentSearch);
  }

  private updateEvent(events) {
    this.events = events;
    if (this.loadPaginated !== 0) {
      this.changePage(this.loadPaginated)
      this.loadPaginated = 0
    }
  }

  protected displayItemNumber(number: number) {
    return this.getEventData(number);
  }

  private getEventData(number: number = 0) {
    if (!this.events) {
      this.updatePages(0, 0);
      return;
    }
    this.eventItems = [];
    this.showTable = true;
    if (number >= 0 && this.events.length > number) {
      this.current = this.events[number];
      this.eventDetailForm.clientForm.getClient(this.current.mainClient.clientId);
      const titleName = formatStringWithStartCase(`${this.current.type} ${this.sectionType}`);
      this.sectionTitle = this.generalDictionary.SECTIONS.TITLES.EDIT(titleName);
      if (this.current) {
        this.getEventItems();
        this.eventDetailForm.setFormData(this.current);
        this.validateForm(this.state);
      }
    } else {
      this.loadPaginated = number;
      this.cps.getNextElements(number)
    }
    return _.get(this.current, 'id');
  }

  private getEventItems() {
    const itemUrl = _.get(this.current, '_links.items.href');
    if (itemUrl) {
      this.itemsService.getItemsByUrl(itemUrl, false, { size: this.pageSize });
      this.itemServiceSubscription = this.itemsService.elements.skip(1).subscribe((items) => {
        this.eventItems = items || [];
        this.ableToRemove.next((!items || items.length <= 0))
      })
    }
  }

  private getFormatedToSearch(data: any) {
    return {
      type: _.get(data, 'eventType.id', ['CONSIGNMENT', 'PURCHASE']),
      clientSourceId: _.get(data, 'client.clientSource.id'),
      clientSourceTypeId: _.get(data, 'client.clientSourceType.id'),
      clientStatusId: _.get(data, 'client.clientStatus.id'),
      clientDisplay: _.get(data, 'client.display'),
      mainClientId: _.get(data, 'client.clientId'),
      affiliatedClientId: _.get(data, 'affiliatedClient.clientId'),
      affiliatedSourceId: _.get(data, 'affiliatedClient.clientSource.id'),
      affiliatedTypeId: _.get(data, 'affiliatedClient.clientType.id'),
    }
  }

  private getFormatedData(data): any {
    const formatedData: any = _.pick(<{}>data, ['notes', 'date', 'code']);
    formatedData['mainClient'] = this.formatedClientData(data.client);
    formatedData['affiliatedClient'] = this.formatedAffiliatedClientData(data.affiliatedClient);
    if (this.current) {
      formatedData['type'] = this.current['type']
    } else {
      formatedData['type'] = (this.PopPupState === 'PopUp' ? _.get(this.popPupEvent, 'eventType.id') : _.get(data, 'eventType.id'));
    }
    return _.omitBy(formatedData, (value) => _.isEmpty(value) && !_.isNumber(value) && !_.isBoolean(value));
  }

  protected formatedClientData(data): any {
    const formatedData: any = _.pick(<{}>data, ['display', 'clientSource', 'clientSourceType', 'clientStatus', 'clientId',]);
    return _.omitBy(formatedData, (value) => _.isEmpty(value) && !_.isNumber(value) && !_.isBoolean(value));
  }

  public finishCreateState(item) {
    const id = _.get(item, 'id');
    if (this.PopPupState === 'PopUp') {
      const formatedData = this.popPupItem.type === 'TRANSACTION'
        ? getTransactionItemData(this.popPupItem)
        : {
          ...this.popPupItem,
          id: null
        }
      const eventType = this.popPupEvent.eventType.id === 'CONSIGNMENT' ? 'consignmentEvent' : 'purchaseEvent';
      formatedData[eventType] = { id: id, type: this.popPupEvent.eventType.id }
      this.doEditItemAction(formatedData).subscribe((response) => {
        switch (_.toLower(this.popPupItem.event.type)) {
          case _.toLower(GeneralDictionary.SECTIONS.PUBLICATION):
          case _.toLower(GeneralDictionary.SECTIONS.TRANSACTION):
            this.redirectToEventPage();
            this.createEvent.reset();
            break;
          default:
            break;
        }
      })
    } else {
      super.finishCreateState(id);
    }
  }

  private redirectToEventPage(type = null, id = null) {
    if (type && id) {
      this.router.navigate([_.toLower(type), id]);
    } else {
      this.router.navigate([_.toLower(this.popPupItem.event.type), this.popPupItem.event.id]);
    }
  }

  protected getSectionName(): string | string[] {
    const data = this.eventDetailForm.getFormData()
    return ['consignment', 'purchase']
  }

  formatedAffiliatedClientData(data): any {
    const formatedData: any = _.pick(data, ['clientSource', 'clientSourceType', 'affiliatedType', 'clientId']);
    return _.omitBy(formatedData, (value) => _.isEmpty(value) && !_.isNumber(value) && !_.isBoolean(value));
  }

  doEditItemAction(data): Observable<any> {
    return this.itemsService.amend(data, this.popPupItem.id);
  }

  destroyMessage(event) { }

  public moreItems() {
    const totalElements = this.itemsService.page.value.totalElements;
    if (totalElements > this.eventItems.length) {
      this.itemsService.getNextElements()
    }
  }

  protected reorderResults(): BehaviorSubject<any> {
    return null;
  }

  private removeItem(item) {
    const type = this.eventDetailForm.consignmentPurchaseForm.get('eventType').value;
    const disposable = this.dialogService.addDialog(ConfirmComponent, {
      title: this.generalDictionary.NOTIFICATIONS.REMOVE_CONFIRM_TITLE,
      message: this.generalDictionary.NOTIFICATIONS.DELETE_ITEM_FROM_EVENT(type.toString()),
    }).subscribe((isConfirmed) => {
      if (isConfirmed) {
        if (type.toString().toLowerCase() === GeneralDictionary.SECTIONS.CONSIGNMENT.toLowerCase()) {
          item.consignmentEvent = null
        } else {
          item.purchaseEvent = null
        }
        const modifiedItem: any = _.clone(item)
        modifiedItem.id = null
        this.itemsService.amend(modifiedItem, item.id).subscribe((res) => {
          this.redirectToEventPage(item.event.type, item.event.id);
        })
      } else {
        this.redirectToEventPage(item.event.type, item.event.id);
      }
      disposable.unsubscribe()
    });
  }

}
