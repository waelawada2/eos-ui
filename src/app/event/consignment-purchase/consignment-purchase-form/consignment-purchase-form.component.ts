import { AfterViewInit, Component, forwardRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ClientFormComponent, AffiliatedClientComponent } from './../../shared/components';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { FormsDictionary, EVENTCONST } from './../../shared/constants';
import { noRequiredObjectValidator } from '../../shared/validators';
import { autocompleListFormatter, convertToAutoformatCompatibleObject } from './../../shared/utils';
import { Subscription } from 'rxjs/Subscription'
import { Observable } from 'rxjs/Observable'

import * as _ from 'lodash';

@Component({
  selector: 'app-consignment-purchase-form',
  templateUrl: './consignment-purchase-form.component.html',
  styleUrls: [
    '../../shared/styles/panels.scss',
    './consignment-purchase-form.component.scss',
  ],
})
export class ConsignmentPurchaseFormComponent implements AfterViewInit, OnInit, OnDestroy {
  consignmentPurchaseForm: FormGroup;
  public dictionary = FormsDictionary.CONSIGNMENT_PURCHASE;
  public eventList = EVENTCONST.consignmentPurchaseEventList;
  public formEventError = {};
  public clientTypeMain = true;
  public validationMessages = FormsDictionary.VALIDATION;
  public listFormatter = autocompleListFormatter;
  public formValue = {};
  public validate = false;
  public requiredType = true;
  evenTypeField;

  subscription: Subscription;

  hideElements = ['notes'];

  @ViewChild(forwardRef(() => ClientFormComponent)) clientForm: ClientFormComponent;
  @ViewChild(forwardRef(() => AffiliatedClientComponent)) affiliatedClient: AffiliatedClientComponent;

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm();
  }

  ngAfterViewInit() {
    this.subscribeToFormChanges()
  }

  subscribeToFormChanges() {
    this.subscription = this.consignmentPurchaseForm
      .valueChanges
      .debounceTime(400)
      .filter(() => !this.validate)
      .subscribe(this.onValueChanged.bind(this));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  createForm() {
    this.consignmentPurchaseForm = this.fb.group({
      code: [''],
      eventType: [''],
      notes: ['']
    });
    this.evenTypeField = this.consignmentPurchaseForm.get('eventType');
  }

  getFormData() {
    return this.consignmentPurchaseForm.getRawValue();
  }

  standarMode() {
    this.validate = true;
    this.evenTypeField.clearValidators();
    this.evenTypeField.setValidators(noRequiredObjectValidator(true));
    this.consignmentPurchaseForm.updateValueAndValidity();
    this.evenTypeField.disable();
    this.requiredType = true;
  }

  searchMode() {
    this.validate = false;
    this.evenTypeField.clearValidators();
    this.evenTypeField.setValidators(noRequiredObjectValidator());
    this.consignmentPurchaseForm.updateValueAndValidity();
    this.evenTypeField.enable();
    this.requiredType = false;
  }

  setFormData(data, state: any = '') {
    if (data) {
      this.reset();
      let type = convertToAutoformatCompatibleObject(_.filter(this.eventList, ['id', data.type])[0], 'name');
      if (state === 'PopUp' && state) {
        type = data.eventType;
      }
      this.consignmentPurchaseForm.patchValue({
        eventType: type,
        notes: data.notes,
        code: data.code,
      });

      if (data.mainClient) {
        this.clientForm.setFormInfo(data);
      }
      if (data.affiliatedClient) {
        this.affiliatedClient.setFormInfo(data);
      }

      if (state === 'PopUp') {
        this.setRequiredFields(false);
      }
    }
  }

  onValueChanged(data?: any) {
    this.formEventError = _.reduce(this.consignmentPurchaseForm.controls, (prev, value, index) => {
      if (value.dirty && value.invalid) {
        const message = _.get(this.validationMessages, `${_.toUpper(_.snakeCase(index))}`);
        const pairs = _.toPairs(value.errors);
        if (pairs[0]) {
          prev[index] = _.get(message, pairs[0][0]);
        }
      }
      return prev
    }, {});
  }

  reset() {
    this.consignmentPurchaseForm.reset();
  }

  setRequiredFields(required: boolean = true) {
    this.clientForm.requiredMode(required);
  }
}
