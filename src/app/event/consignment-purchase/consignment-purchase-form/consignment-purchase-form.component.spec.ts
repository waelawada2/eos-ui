import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsignmentPurchaseFormComponent } from './consignment-purchase-form.component';

describe('ConsignmentPurchaseFormComponent', () => {
  let component: ConsignmentPurchaseFormComponent;
  let fixture: ComponentFixture<ConsignmentPurchaseFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsignmentPurchaseFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsignmentPurchaseFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
