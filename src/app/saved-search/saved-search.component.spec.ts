import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { FormsModule, FormBuilder } from '@angular/forms';

import { SavedSearchComponent } from './saved-search.component';

describe('SavedSearchComponent', () => {
  let component: SavedSearchComponent;
  let fixture: ComponentFixture<SavedSearchComponent>;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedSearchComponent ],
      imports: [ HttpModule, FormsModule ],
      providers: [FormBuilder],
      schemas : [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));
  
  beforeEach(() => {
    fixture = TestBed.createComponent(SavedSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  xit(' window should be injected in saved-search-modal.component ', () => {
    /*
    this test is being ignored:
    Window should be injected in the module, and should be used in constructor if any variable initalization might take place
    - this is in order to have a way to inject window into the test.
    saved-searched-modal-component.ts( line 26 )
    ( public userName = window['keycloak'].tokenParsed?window['keycloak'].tokenParsed.preferred_username:null;)
    */
    expect(component).toBeTruthy();
  }); 
});