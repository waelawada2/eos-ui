import { Component, ViewChild, ElementRef, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response } from '@angular/http';
import { ConfigurationLoaderService } from 'app/configuration.service'
import { Helper } from '../utils/helper';
import { DatatableComponent } from '@swimlane/ngx-datatable/src/components/datatable.component';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { TosatService } from '../common/toasty-service';
import { DialogService } from "ng2-bootstrap-modal";
import { ConfirmComponent } from '../common/confirm-component';
import { SaveSearchModalComponent } from '../saved-search/saved-search-modal.component';
import { ModalModule } from 'ngx-modal';
import { SavedSearchService } from '../saved-search/saved-search.service';

class PagedData<T> {
  data: T[];
}

@Component({
  selector: 'app-saved-search',
  templateUrl: './saved-search.component.html',
  styleUrls: ['./saved-search.component.css'],
  providers: [Helper, SaveSearchModalComponent, SavedSearchService, DialogService]
})
export class SavedSearchComponent implements OnInit {
  readonly headerHeight = 30;
  readonly rowHeight = 40;
  readonly pageLimit = 10;
  public userName = window['keycloak'].tokenParsed && window['keycloak'].tokenParsed.preferred_username ? window['keycloak'].tokenParsed.preferred_username : null;
  private _url: string;
  public errorMessage: string;
  public rows = []; // Array of saved search list per User.
  public temp = []; // Filter array that keeps track of results matching the search text.
  public isLoading: boolean;
  public message = {}; // e-mail message object
  public deleteFlag = false; // Set to 'true' when a saved search item is deleted.
  public savedSearchName = '';
  public savedSearchNotes = '';
  public savedSearchID;
  public rows_copy = []; // Maintains an unedited reference to the original rows (saved search items)
  public page = 0;
  public totalPages = 0;
  public filterFlag = false;
  public confirmResult: boolean = null;
  public sortOrder = 'asc';
  public sortColumn = 'name';
  public filterSavedSearchName;

  @ViewChild(DatatableComponent) savedSearchList: DatatableComponent;
  // Access to 'Save Search' Modal
  @ViewChild(SaveSearchModalComponent) public saveSearchModal: SaveSearchModalComponent;

  constructor(private router: Router,
    private _http: Http,
    public helper: Helper,
    private tosatservice: TosatService,
    private _dialogService: DialogService,
    public _savedSearchService: SavedSearchService,
    private _savedSearchModalComponent: SaveSearchModalComponent,
    private el: ElementRef,
    configuration: ConfigurationLoaderService,
  ) {
    this._url = configuration.getSettings().apiUrl
    setTimeout(() => {
      this.userName = window['keycloak'].tokenParsed && window['keycloak'].tokenParsed.preferred_username ? window['keycloak'].tokenParsed.preferred_username : null;
    }, 500);
  }

  ngOnInit() {
    this.rows = [];
    this.onScroll(0);
  }

  // Scroll
  onScroll(offsetY: number) {
    // total height of all rows in the viewport
    const viewHeight = this.el.nativeElement.querySelector('ngx-datatable').getBoundingClientRect().height - this.headerHeight;

    // check if we scrolled to the end of the viewport
    if (!this.isLoading && offsetY + viewHeight >= this.rows.length * this.rowHeight) {
      // total number of results to load
      let limit = this.pageLimit;

      // check if we haven't fetched any results yet
      if (this.rows.length === 0) {
        // calculate the number of rows that fit within viewport
        const pageSize = Math.ceil(viewHeight / this.rowHeight);
        // change the limit to pageSize such that we fill the first page entirely
        // (otherwise, we won't be able to scroll past it)
        limit = Math.max(pageSize, this.pageLimit);
      }
      if (this.page == 0 || (this.page < this.totalPages)) {
        if (this.filterFlag != true) {
          this.loadPage(limit);
        }
      }
    }
  }

  // Sort
  onSort(event) {
    let limit = this.pageLimit;
    this.isLoading = true;
    this.page = 0;
    if (this.sortOrder == 'asc') {
      this.sortOrder = 'desc';
    }
    else {
      this.sortOrder = 'asc';
    }

    if (event.column.prop.toLowerCase() === 'name') {
      this.sortColumn = 'name';
    }
    else if (event.column.prop.toLowerCase() === 'date') {
      this.sortColumn = 'date'
    }

    if (this.filterFlag != true) {
      this.loadPage(limit);
    }
    else if (this.filterSavedSearchName) {
      this.getSavedSearchByName(this.filterSavedSearchName);
    }
  }

  // Display 'Saved Search' list on the Homepage
  private loadPage(limit: number) {
    this.isLoading = true;
    this.loadSavedSearch(limit);
  }

  public loadSavedSearch(limit) {
    this._savedSearchService.getSavedSearch(this.rows.length, limit, this.userName, this.page, this.sortColumn, this.sortOrder).subscribe(results => {
      let data = [];
      data = results.content;
      for (let row in data) {
        data[row]['date'] = this.helper.getDate(data[row]['date'], 'DD MMM YYYY');
      }
      if (this.deleteFlag || this.page == 0) {
        this.deleteFlag = false;
        this.temp = [];
        this.rows_copy = [];
      }
      this.temp.push(...data); // Store saved search items in the filter array.
      this.rows_copy.push(...data); // Store saved search items in row_copy - which maintains a copy of the original list of the saved search items.
      this.rows = [];
      // rows[] which is displayed on the datatable with saved search items
      this.rows_copy.forEach(item => {
        this.rows.push(item);
      });
      this.isLoading = false;
      this.page++; // Increment page counter.
      if (results.first === true) {
        this.totalPages = results.totalPages;
      }
    });
  }


  // Navigate to Properties/Artists results for the selected 'Saved Search' item.
  // 'search' - Object that carries information about a search like - id, searchCriterias, name, notes.
  navigateToSearchResults(search) {
    document.getElementsByTagName('html')[0].classList.add('loading');
    let query = '';
    query = this.createQuery(search.searchCriterias);
    let url = '?savesearch=' + search.id + '&' + query;
    // Properties search results.
    if (search.searchType) {
      if (search.searchType.toLowerCase() == "properties") {
        this.router.navigateByUrl('/object' + url);
        document.getElementsByTagName('html')[0].classList.remove('loading');
      }
      // Artists search results.
      else {
        this.router.navigateByUrl('/artist' + url);
        document.getElementsByTagName('html')[0].classList.remove('loading');
      }
    }
    else {
      document.getElementsByTagName('html')[0].classList.remove('loading');
    }
  }

  // Create a query string for Params listed in 'searchCriterias' object.
  createQuery(searchCriterias) {
    let query = '';
    for (let i in searchCriterias) {
      query += searchCriterias[i].paramName + "=" + searchCriterias[i].paramValue + "&";
    }
    return query;
  }

  // List results matching the filter text.
  updateFilter(event) {
    this.filterFlag = true;
    if (event.target.value.length == 0) {
      this.clearFilter();
    }
    else {
      this.getSavedSearchByName(event.target.value.toLowerCase());
    }
  }

  // Clear input field on clicking 'x' icon.
  clearInput(ele) {
    (<HTMLInputElement>document.getElementById(ele)).value = '';
  }

  // Clear filter and show all 'Saved Search' items on the table.
  clearFilter() {
    const val = '';
    const temp = this.temp.filter(function (d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });
    // When item is deleted from the filtered list
    if (this.deleteFlag) {
      this.page = 0;
      this.loadSavedSearch(this.pageLimit);
    }
    else {
      this.temp = [];
      this.rows = [];
      this.rows_copy.forEach(item => {
        this.temp.push(item);
        this.rows.push(item);
      });
    }
    this.filterFlag = false;
    this.filterSavedSearchName = null;
  }

  // Create e-mail.
  public createMessage(savedSearch) {
    let url = window.location.href.split('?')[0];
    url = url.charAt(url.length - 1) === '/' ? url.substr(0, url.length - 1) : url;
    if (savedSearch.searchType) {
      if (savedSearch.searchType.toLowerCase() == "properties") {
        url = url + '/object?savesearchshareid=' + savedSearch.id;
      }
      else {
        url = url + '/artist?savesearchshareid=' + savedSearch.id;
      }
    }
    else {
      this.tosatservice.addToast("Could not determine 'Search Type'. Failed to generate message.", 'top-right');
      return false;
    }
    this.message = {};
    this.message['subject'] = savedSearch.name;
    this.message['body'] = url;
    this.helper.sendEmail(this.message);
  }

  // Delete Saved Search
  public deleteSavedSearch(row, rows) {
    this.deleteConfirm(row, rows);
  }

  // Delete - Confirm Dialog
  deleteConfirm(row, rows) {
    document.getElementsByTagName("body")[0].classList.add("positionFixed");
    this._dialogService.addDialog(ConfirmComponent, {
      title: 'Delete Saved Search',
      message: 'Are you sure you want to delete the saved search - ' + row.name + '?'
    })
      .subscribe((isConfirmed) => {
        document.getElementsByTagName("body")[0].classList.remove("positionFixed");
        this.confirmResult = isConfirmed;
        if (this.confirmResult) {
          this._savedSearchService.deleteSavedSearch(this.userName, row.id)
            .subscribe(deletedSavedSearch => {
              this.deleteFlag = true;
              this.temp = this.rows.filter(r => r !== row);
              this.rows = this.rows.filter(r => r !== row);
              this.rows_copy = this.rows_copy.filter(r => r !== row);
            }, resFileError => this.errorMessage = resFileError);
        }
      });
  }

  // Populate 'Save Search' modal form fields.
  // Bind 'Saved Search' name, notes from DTO to input fields on 'Saved Search' modal.
  populateSaveSearchModal(row) {
    this.savedSearchName = row.name;
    this.savedSearchNotes = row.notes;
    this.savedSearchID = row.id;
    this._savedSearchModalComponent.saveSearchForm.controls['name'].setValue(this.savedSearchName);
    this._savedSearchModalComponent.saveSearchForm.controls['notes'].setValue(this.savedSearchNotes);
  }

  // Get 'Saved Search' item by ID.
  public updateSavedSearchRowByID(savedSearchID) {
    let index = this.rows.findIndex(savedSearch => savedSearchID == savedSearch.id);
    this._savedSearchService.getSavedSearchById(this.userName, savedSearchID)
      .subscribe(savedSearch => {
        this.rows[index].name = savedSearch.name;
        this.rows[index].notes = savedSearch.notes;
      }, resFileError => this.errorMessage = resFileError);
  }

  // Get 'Saved Search' by name.
  public getSavedSearchByName(savedSearchName) {
    this.filterSavedSearchName = savedSearchName;
    if (savedSearchName != '') {
      this._savedSearchService.getSavedSearchByName(this.userName, savedSearchName, this.userName, this.sortColumn, this.sortOrder)
        .subscribe(
          filteredSavedSearch => {
            const val = savedSearchName;
            let data = [];
            data = filteredSavedSearch.content;
            for (let row in data) {
              data[row]['date'] = this.helper.getDate(data[row]['date'], 'DD MMM YYYY');
            }
            this.temp = [];
            this.rows = [];
            this.temp.push(...data); // Store saved search items in the filter array.
            this.temp.forEach(item => {
              this.rows.push(item);
            });
          },
          error => {
            this.errorMessage = error;
          }
        );
    }
  }
}