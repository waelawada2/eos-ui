import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ConfigurationLoaderService } from 'app/configuration.service'

@Injectable()
export class SavedSearchService {

  private _url: string

  constructor(private _http: Http, configuration: ConfigurationLoaderService) {
    this._url = configuration.getSettings().apiUrl
  }

  // GET 'Saved Search' for logged-in user.
  public getSavedSearch(offset: number, limit: number, userName, page, sortColumn, sortOrder): Observable<any> {
    let url = '';
    url = this._url + `/users/` + userName + `/searches?page=` + page + `&sort=` + sortColumn + `,` + sortOrder;
    return this._http.get(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Could not retrieve "Saved Search" list.' + response.status)
        }
        else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Create Saved Search
  createSavedSearch(userName, searchParam: Object) {
    return this._http.post(this._url + '/users/' + userName + '/searches?', searchParam)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Request to "Create Saved Search" failed - ' + response.status)
        }
        else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Delete Saved Search
  public deleteSavedSearch(userName, savedSearchID) {
    return this._http.delete(this._url + `/users/` + userName + `/searches/${savedSearchID}`)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Deleting Saved Search ID: ' + savedSearchID + ' failed.' + response.status)
        }
        else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Update Saved Search
  public updateSavedSearch(userName, savedSearchID, searchParam: Object) {
    return this._http.put(this._url + `/users/` + userName + `/searches/${savedSearchID}`, searchParam)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Request to "Update Saved Search" failed - ' + response.status)
        }
        else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Get saved search by ID.
  public getSavedSearchById(userName, savedSearchID) {
    return this._http.get(this._url + `/users/` + userName + `/searches/${savedSearchID}`)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Access shared saved search
  public getSharedSavedSearchById(savedSearchID) {
    return this._http.get(this._url + `/users/searches/${savedSearchID}`)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Get saved search by name.
  getSavedSearchByName(userName, savedSearchName, savedSearchUserName, sortColumn, sortOrder) {
    return this._http.get(this._url + `/users/` + userName + `/searches?name=` + savedSearchName + `&user-name=` + savedSearchUserName + `&size=2000&sort=` + sortColumn + `,` + sortOrder)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Error Handler
  _errorHandler(_error: Response) {
    return Observable.throw(_error || "Server Error - 404.");
  }
}