﻿import { Component, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { TosatService } from '../common/toasty-service';
import { ModalModule } from 'ngx-modal';
import { SavedSearchService } from '../saved-search/saved-search.service';
import { DialogService } from "ng2-bootstrap-modal";
import { AlertComponent } from '../common/alert-component';
import { KeyboardkeysService } from '../../app/keyboardkeys/keyboardkeys.service';

@Component({
	selector: 'app-save-search-modal',
    templateUrl: './saved-search-modal.component.html', 
    providers: [ SavedSearchService, KeyboardkeysService ],
})
export class SaveSearchModalComponent {
	@ViewChild('saveSearchModal') saveSearchModal: ModalModule;
	// Get name, notes, savedsearchID from 'SaveSearchComponent' for individual saved search items listed on the homepage.
	@Input() savedSearchName: string ;
	@Input() savedSearchNotes: string;
	@Input() savedSearchID: number;
	@Output() public updatedSavedSearch:EventEmitter<any>  = new EventEmitter();
	@Output() public openSaveSearchPopup:EventEmitter<any>  = new EventEmitter();
	public saveSearchForm: FormGroup;
	public saveSearchMode;
	public saveSearchParam = '';
    	public searchType = ''; // 'artists' or 'properties'
	public errorMessage: string;
	public userName = window['keycloak'].tokenParsed?window['keycloak'].tokenParsed.preferred_username:null;

	constructor(private builder: FormBuilder,
				private tosatservice:TosatService,
				public _savedSearchService: SavedSearchService,
				private _keyboardkeysService: KeyboardkeysService,
			    private dialogService: DialogService) {
		// 'Save Search' modal form.
		this.saveSearchForm = this.builder.group({
			name: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(80)]),
			notes: new FormControl('', Validators.maxLength(200)),
		});
	}

	// Create Saved Search
	createSavedSearch(searchParam) {
		this._savedSearchService.createSavedSearch(this.userName, searchParam)
		.subscribe(
			savedSearch => {
				document.getElementsByTagName('html')[0].classList.remove('loading');
				this.saveSearchForm.reset();
				sessionStorage.removeItem('sortparam');
				let query = '', url;
				for (let i in savedSearch.searchCriterias) {
					query+= savedSearch.searchCriterias[i].paramName+"="+savedSearch.searchCriterias[i].paramValue+"&";
				}
				query = query.charAt(query.length-1)=='&'?query.substr(0, query.length-1): query;
				if(savedSearch.searchType.toLowerCase() == "properties") {
					url = encodeURI('object?savesearch='+savedSearch.id+'&'+query);
				}
				else {
					url = encodeURI('artist?savesearch='+savedSearch.id+'&'+query);
				}
				this.tosatservice.addToast('Your search criteria has successfully been saved - <a href='+url+'>'+"'"+savedSearch.name+"'"+'</a>.','top-right');
			},
			error => {
				document.getElementsByTagName('html')[0].classList.remove('loading');
				this.errorMessage = error;
				if(JSON.parse(this.errorMessage["_body"]).details.toLowerCase().trim().indexOf("exists") > -1){
					this.dialogService.addDialog(AlertComponent,
						{
							title:'Unique Saved Search', 
							message:"This name already exists"},{ backdropColor: 'rgba(220,220,220,0.5)' })
					.subscribe((isConfirmed)=>{ 
						   this.openSaveSearchPopup.emit();
					});
				}
			}
		);
	}

	// Update Saved Search
	updateSavedSearch(searchParam) {
		this._savedSearchService.updateSavedSearch(this.userName, this.savedSearchID, searchParam)
		.subscribe(
			savedSearch => {
				document.getElementsByTagName('html')[0].classList.remove('loading');
				if(this.saveSearchMode=='EditSaveSearch') {
					this.updatedSavedSearch.emit(savedSearch.id);
				}
			},
			error => {
				document.getElementsByTagName('html')[0].classList.remove('loading');
				this.errorMessage = error;
			}
		);
	}

	// Save Search
	public saveSearch(searchParam) { 
		// 'Edit Saved Search' on homepage - saved search lists.
		if((window.location.pathname.indexOf("object")==-1) && (window.location.pathname.indexOf("thumbnail")==-1) && (window.location.pathname.indexOf("artist")==-1)) {
			document.getElementsByTagName('html')[0].classList.add('loading');
			this.updateSavedSearch(searchParam);
		}
		// 'Save Search' - Artists/Objects search results page.
		else if(searchParam.name != '') {
			this.saveSearchParam = (window.location.pathname.indexOf("artist")>-1)?sessionStorage.getItem("searchparam"):sessionStorage.getItem("MainViewQuery");
			if(this.saveSearchParam && this.saveSearchParam != '&') {
				let searchCriteriaList = [];
				let searchCriteria = this.saveSearchParam.split("&");
				for (let i in searchCriteria) {
					let srchCritearia = searchCriteria[i].split("=");
					if(srchCritearia[0] && srchCritearia[1]) {
						searchCriteriaList.push({
							"paramName": srchCritearia[0],
							"paramValue": srchCritearia[1]
						});
					}
				}
				if(sessionStorage.getItem('sortparam')) {
					searchCriteriaList.push({
						"paramName": 'sort',
						"paramValue": sessionStorage.getItem('sortparam')
					});
				}
				searchParam.searchCriterias = searchCriteriaList;
				// Set Search Type
				this.searchType='';
				if(window.location.pathname.indexOf("object") > 0 || window.location.pathname.indexOf("thumbnail") > 0) {
					this.searchType = "properties";
				}
				else {
					this.searchType = "artists";
				}
				searchParam.searchType = this.searchType;
				if (searchParam.searchCriterias.length > 0) {
					document.getElementsByTagName('html')[0].classList.add('loading');
					this.createSavedSearch(searchParam);
				}
			}
		}
	  }
	
	  // Reset 'Save Search' Form.
	  resetSaveSearchForm(searchParam) {
		this.saveSearchForm.reset();		
	  }

	// Keyboard shortcuts
	keyboardShortCutFunct(e) {
		switch(this._keyboardkeysService.getKeyboardResponse(e)){
			case 'addRecord':
				var button = document.getElementById('savesarchList');
				if(button)
				button.click();
			break;
		}
	}
}