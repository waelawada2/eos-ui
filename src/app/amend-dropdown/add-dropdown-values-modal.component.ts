import { Component, ViewChild, Input, Output, OnInit, EventEmitter, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ModalModule } from 'ngx-modal';
import { CompleterService, CompleterData } from 'ng2-completer';
import { DialogService } from "ng2-bootstrap-modal";
import { ConfirmComponent } from '../common/confirm-component';
import { TosatService } from '../common/toasty-service';
import { AmendDropdownService } from './amend-dropdown.service';
import { AlertComponent } from '../common/alert-component';


@Component({
	selector: 'app-add-dropdown-values-modal',
	templateUrl: 'add-dropdown-values-modal.component.html',
	styleUrls: ['amend-dropdown.component.css'],
    providers: [ AmendDropdownService ]
})
export class AddDropdownValuesModalComponent implements OnInit, AfterViewInit {
	@ViewChild('addDropdownValuesModal') addDropdownValuesModal: ModalModule;
	// Get table name, dropdown value from 'AmendDropdownComponent' for selected dropdown values.
    @Input() tableName: string;
    @Input() tableValue: string;
	@Output() public updateTable:EventEmitter<any>  = new EventEmitter();
	public addDropdownValuesForm: FormGroup;
	public userName = window['keycloak'].tokenParsed?window['keycloak'].tokenParsed.preferred_username:null;
    public newValue:any = null;
	public disableSave : boolean = true;
	
    constructor(private builder: FormBuilder,
				private completerService: CompleterService,
				private _dialogService: DialogService,
				private tosatservice:TosatService,
				public _amendDropdownService: AmendDropdownService) {
		this.addDropdownValuesForm = this.builder.group({
			dropdownValue: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]),
			dropdownCountrySitterValueLname: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]),			
			dropdownCountrySitterValue: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)])
		});
	}

	ngOnInit() {
		this.newValue = '';
		this.addDropdownValuesForm.reset();
	}

	ngAfterViewInit() {
		this.addDropdownValuesForm.reset();
	}
    
    getNewValue() {
		if(this.tableValue==='country') {
			this.newValue = this.addDropdownValuesForm.controls['dropdownCountrySitterValue'].value;
		}
		else {
			this.newValue = this.addDropdownValuesForm.controls['dropdownValue'].value;
		}
    }

	setDisable(event){
		if(this.tableValue==='country') {
		   if(this.addDropdownValuesForm.controls['dropdownCountrySitterValue'].value.trim()){
			   this.disableSave = false;
			} else {
			   this.disableSave = true;	
			}
		}
		else {
			if(this.tableValue==='sitter') {
				if(this.addDropdownValuesForm.controls['dropdownCountrySitterValueLname'].value && this.addDropdownValuesForm.controls['dropdownCountrySitterValue'].value && this.addDropdownValuesForm.controls['dropdownCountrySitterValueLname'].value.trim() && this.addDropdownValuesForm.controls['dropdownCountrySitterValue'].value.trim()){
					this.disableSave = false;
				 } else {
					this.disableSave = true;	
				 }
			} else{
				if(this.addDropdownValuesForm.controls['dropdownValue'].value.trim()){
					this.disableSave = false;
				 } else {
					this.disableSave = true;	
				 }
			}
		}
	   
	}
	
	closePopup(){
		var button = document.getElementById('closeAddDropDown');
		button.click();
	}
	addDropdownValue() {
		this.getNewValue();
		var dropdown = {
			name: '',
			firstName:'',
			lastName:'',
            description: ''
		}
		var dropFlag = false;
		if(this.tableValue == 'sitter'){
			dropdown = {
				name: '',
				firstName:this.addDropdownValuesForm.controls['dropdownCountrySitterValue'].value?this.addDropdownValuesForm.controls['dropdownCountrySitterValue'].value.trim():null,
				lastName:this.addDropdownValuesForm.controls['dropdownCountrySitterValueLname'].value?this.addDropdownValuesForm.controls['dropdownCountrySitterValueLname'].value.trim():null,				
				description: ''
			}
			if(dropdown.firstName&&dropdown.lastName){
				dropFlag = true;
			}
		} else {
			dropdown = {
				name: this.newValue?this.newValue.trim():null,
				description: this.newValue?this.newValue.trim():null,
				lastName:'',
				firstName:''
			}
			if(dropdown.name){
				dropFlag = true;
			}
		}
		this.disableSave = true;
		if(dropFlag){
			switch(this.tableValue){
				case 'presizetext':
				this._amendDropdownService.addTablePresizeType(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully added value - '+this.newValue+' to '+this.tableName+'.','top-right');
						this.closePopup();
						this.updateTable.emit(response);
					},
					error => {
						this.displayErrorDetails(this.newValue, 'Pre-size Types', error);                
					}
				);
				break;
				case 'category':
				this._amendDropdownService.addTableCategories(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully added value - '+this.newValue+' to '+this.tableName+'.','top-right');
						this.closePopup();
						this.updateTable.emit(response);
					},
					error => {
						this.displayErrorDetails(this.newValue, 'Categories', error);
					}
				);
				break;
				case 'editionsizetype':
				this._amendDropdownService.addTableEditionSizeType(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully added value - '+this.newValue+' to '+this.tableName+'.','top-right');
						this.closePopup();
						this.updateTable.emit(response);
					},
					error => {
						this.displayErrorDetails(this.newValue, 'Edition Size Types', error);
					}
				);
				break;
				case 'maincolour':
				this._amendDropdownService.addTableMainColour(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully added value - '+this.newValue+' to '+this.tableName+'.','top-right');
						this.closePopup();
						this.updateTable.emit(response);
					},
					error => {
						this.displayErrorDetails(this.newValue, 'Main Color', error);
					}
				);
				break;
				case 'country':
				delete dropdown.name;
				dropdown['countryName'] = this.newValue;
				this._amendDropdownService.addTableCountry(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully added value - '+this.newValue+' to '+this.tableName+'.','top-right');
						this.closePopup();
						this.updateTable.emit(response);
					},
					error => {
						this.displayErrorDetails(this.newValue, 'Countries', error);
					}
				);
				break;
				case 'sitter':
				this._amendDropdownService.addTableSitter(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully added value - '+dropdown.firstName+' '+dropdown.lastName+' to '+this.tableName+'.','top-right');
						this.closePopup();
						this.updateTable.emit(response);
					},
					error => {
						this.displayErrorDetails(dropdown.firstName+' '+dropdown.lastName, 'Sitter', error);
					}
				);
				break;
				case 'animal':
				this._amendDropdownService.addTableAnimal(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully added value - '+this.newValue+' to '+this.tableName+'.','top-right');
						this.closePopup();
						this.updateTable.emit(response);
					},
					error => {
						this.displayErrorDetails(this.newValue, 'Animal', error);
					}
				);
				break;
				case 'position':
				this._amendDropdownService.addTablePosition(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully added value - '+this.newValue+' to '+this.tableName+'.','top-right');
						this.closePopup();
						this.updateTable.emit(response);
					},
					error => {
						this.displayErrorDetails(this.newValue, 'Position', error);
					}
				);
				break;
				case 'subject':
				this._amendDropdownService.addTableSubject(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully added value - '+this.newValue+' to '+this.tableName+'.','top-right');
						this.closePopup();
						this.updateTable.emit(response);
					},
					error => {
						this.displayErrorDetails(this.newValue, 'Subject', error);
					}
				);
				break;
			}
		} else{
			this.tosatservice.addToastError('Please enter valid input...!!!','top-right');			
		}
	}

	displayErrorDetails(value, table, error) {
		let err = JSON.parse(error['_body']).details?JSON.parse(error['_body']).details: null;
		if(err.indexOf('ConstraintViolationException')>-1 || err.indexOf('exists')>-1) {
			this.tosatservice.addToastError('Value - '+value+' already exists in table - '+table+'.','top-right');
		}
		else {
			this.tosatservice.addToastError('Failed to add '+value+'.','top-right');
		}
	}

	getValue(){
		return true;
	}
}