import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmendDropdownComponent } from './amend-dropdown.component';

describe('AmendDropdownComponent', () => {
  let component: AmendDropdownComponent;
  let fixture: ComponentFixture<AmendDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmendDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmendDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
