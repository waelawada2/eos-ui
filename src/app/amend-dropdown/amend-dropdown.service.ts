import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ConfigurationLoaderService } from 'app/configuration.service';

@Injectable()
export class AmendDropdownService {

  private _url: string;
  headers: Headers;
  options: RequestOptions;

  constructor(private _http: Http, configuration: ConfigurationLoaderService) {
    this.headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'q=0.8;application/json;q=0.9'
    });
    this._url = configuration.getSettings().apiUrl
    this.options = new RequestOptions({ "headers": this.headers });
  }

  public getTablePresizeType(): Observable<any> {
    let url = '';
    url = this._url + `/presize-types`;
    return this._http.get(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error("Could not fetch 'Presize Types'." + response.status)
        }
        else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  public getTableCategories(): Observable<any> {
    let url = '';
    url = this._url + `/categories`;
    return this._http.get(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error("Could not fetch 'Categories'." + response.status)
        }
        else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  public getTableEditionSizeType(): Observable<any> {
    let url = '';
    url = this._url + `/edition-size-types`;
    return this._http.get(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error("Could not fetch 'Categories'." + response.status)
        }
        else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  public getTableMainColour(): Observable<any> {
    let url = '';
    url = this._url + `/imageMainColours`;
    return this._http.get(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error("Could not fetch 'Categories'." + response.status)
        }
        else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  public getTableCountry(): Observable<any> {
    let url = '';
    url = this._url + `/countries`;
    return this._http.get(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error("Could not fetch 'Categories'." + response.status)
        }
        else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  public getTableSitter(): Observable<any> {
    let url = '';
    url = this._url + `/sitters`;
    return this._http.get(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error("Could not fetch 'Categories'." + response.status)
        }
        else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  public getTableAnimal(): Observable<any> {
    let url = '';
    url = this._url + `/imageAnimals`;
    return this._http.get(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error("Could not fetch 'Animals'." + response.status)
        }
        else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  public getTablePosition(): Observable<any> {
    let url = '';
    url = this._url + `/imagePositions`;
    return this._http.get(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error("Could not fetch 'Categories'." + response.status)
        }
        else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  public getTableSubject(): Observable<any> {
    let url = '';
    url = this._url + `/imageSubjects`;
    return this._http.get(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error("Could not fetch 'Categories'." + response.status)
        }
        else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Update Dropdown Value - Pre Size Types
  public updateTablePresizeType(dropdown) {
    let url = this._url + `/presize-types/${dropdown.id}`;
    let dropdownName = {
      "name": dropdown.name,
      "description": dropdown.name
    }
    if (url) {
      return this._http.put(url, dropdownName)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to update --Presize Type-- dropdown value failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Update Dropdown Value - Categories Types
  public updateTableCategories(dropdown) {
    let url = this._url + `/categories/${dropdown.id}`;
    let dropdownName = {
      "name": dropdown.name,
      "description": dropdown.name
    }
    if (url) {
      return this._http.put(url, dropdownName)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to update --Categories-- dropdown value failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Update Dropdown Value - Edition Size Type
  public updateTableEditionSizeType(dropdown) {
    let url = this._url + `/edition-size-types/${dropdown.id}`;
    let dropdownName = {
      "name": dropdown.name,
      "description": dropdown.name
    }
    if (url) {
      return this._http.put(url, dropdownName)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to update --Edition Size Type-- dropdown value failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Update Dropdown Value - Main Colour
  public updateTableMainColour(dropdown) {
    let url = this._url + `/imageMainColours/${dropdown.id}`;
    let dropdownName = {
      "name": dropdown.name,
      "description": dropdown.name
    }
    if (url) {
      return this._http.put(url, dropdownName)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to update --Main Colour-- dropdown value failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Update Dropdown Value - Country
  public updateTableCountry(dropdown) {
    let url = this._url + `/countries/${dropdown.id}`;
    let dropdownName = {
      "countryName": dropdown.name,
      "description": dropdown.name
    }
    if (url) {
      return this._http.put(url, dropdownName)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to update --Country-- dropdown value failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Update Dropdown Value - Sitter
  public updateTableSitter(dropdown) {
    let url = this._url + `/sitters`;
    let id = `${dropdown.id}`;
    let dropdownName = {
      "firstName": dropdown.fname,
      "lastName": dropdown.lname,
      "description": dropdown.name,
      "id": id
    }
    if (url) {
      return this._http.put(url, dropdownName)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to update --Sitter-- dropdown value failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Update Dropdown Value - Animal
  public updateTableAnimal(dropdown) {
    let url = this._url + `/imageAnimals`;
    let dropdownName = {
      "name": dropdown.name,
      "description": dropdown.name,
      "id": `${dropdown.id}`
    }
    if (url) {
      return this._http.put(url, dropdownName)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to update --Animal-- dropdown value failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Update Dropdown Value - Position
  public updateTablePosition(dropdown) {
    let url = this._url + `/imagePositions/${dropdown.id}`;
    let dropdownName = {
      "name": dropdown.name,
      "description": dropdown.name
    }
    if (url) {
      return this._http.put(url, dropdownName)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to update --Position-- dropdown value failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Update Dropdown Value - Subject
  public updateTableSubject(dropdown) {
    let url = this._url + `/imageSubjects/${dropdown.id}`;
    let dropdownName = {
      "name": dropdown.name,
      "description": dropdown.name
    }
    if (url) {
      return this._http.put(url, dropdownName)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to update --Subject-- dropdown value failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Delete Dropdown Value - Presize Type.
  public deleteDropdownValuePresizeType(dropdownValueID) {
    let url = this._url + `/presize-types/${dropdownValueID}`;
    return this._http.delete(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Unable to delete Presize Type - ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Delete Dropdown Value - Categories.
  public deleteDropdownValueCategories(dropdownValueID) {
    let url = this._url + `/categories/${dropdownValueID}`;
    return this._http.delete(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Unable to delete Categories - ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Delete Dropdown Value - Edition Size Type.
  public deleteDropdownValueEditionSizeType(dropdownValueID) {
    let url = this._url + `/edition-size-types/${dropdownValueID}`;
    return this._http.delete(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Unable to delete Edition Size Type - ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Delete Dropdown Value - Main Colour.
  public deleteDropdownValueMainColour(dropdownValueID) {
    let url = this._url + `/imageMainColours/${dropdownValueID}`;
    return this._http.delete(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Unable to delete Main Colour - ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Delete Dropdown Value - Country.
  public deleteDropdownValueCountry(dropdownValueID) {
    let url = this._url + `/countries/${dropdownValueID}`;
    return this._http.delete(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Unable to delete Countries - ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Delete Dropdown Value - Sitter.
  public deleteDropdownValueSitter(dropdownValueID) {
    let url = this._url + `/sitters/${dropdownValueID}`;
    return this._http.delete(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Unable to delete Sitter - ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Delete Dropdown Value - Animal.
  public deleteDropdownValueAnimal(dropdownValueID) {
    let url = this._url + `/imageAnimals/${dropdownValueID}`;
    return this._http.delete(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Unable to delete Animal - ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }


  // Delete Dropdown Value - Position.
  public deleteDropdownValuePosition(dropdownValueID) {
    let url = this._url + `/imagePositions/${dropdownValueID}`;
    return this._http.delete(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Unable to delete Position - ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }


  // Delete Dropdown Value - Subject.
  public deleteDropdownValueSubject(dropdownValueID) {
    let url = this._url + `/imageSubjects/${dropdownValueID}`;
    return this._http.delete(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Unable to delete Subject - ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }


  // Add Dropdown Value - Pre Size Types
  public addTablePresizeType(value) {
    let url = this._url + `/presize-types`;
    if (url) {
      return this._http.post(url, value)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to add value to Pre-size Type failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Add Dropdown Value - Categories Types
  public addTableCategories(value) {
    let url = this._url + `/categories`;
    if (url) {
      return this._http.post(url, value)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to add value to Category failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Add Dropdown Value - Edition Size Type
  public addTableEditionSizeType(value) {
    let url = this._url + `/edition-size-types`;
    if (url) {
      return this._http.post(url, value)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to add value to Edition Size Type failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Add Dropdown Value - Main Colour
  public addTableMainColour(value) {
    let url = this._url + `/imageMainColours`;
    if (url) {
      return this._http.post(url, value)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to add value to Main Colour failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Add Dropdown Value - Country
  public addTableCountry(value) {
    let url = this._url + `/countries`;

    if (url) {
      return this._http.post(url, value)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to add value to Country failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Add Dropdown Value - Sitter
  public addTableSitter(value) {
    let url = this._url + `/sitters`;
    if (url) {
      return this._http.post(url, value)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to add value to Sitter failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Add Dropdown Value - Animal
  public addTableAnimal(value) {
    let url = this._url + `/imageAnimals`;
    if (url) {
      return this._http.post(url, value)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to add value to Animal failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Add Dropdown Value - Position
  public addTablePosition(value) {
    let url = this._url + `/imagePositions`;
    if (url) {
      return this._http.post(url, value)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to add value to Position failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Add Dropdown Value - Subject
  public addTableSubject(value) {
    let url = this._url + `/imageSubjects`;
    if (url) {
      return this._http.post(url, value)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to add value to Subject failed -' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  public mergevalues(fromId, toId, table) {
    let body = {};
    let url = this._url + '/' + table + '/' + fromId + '?mergeFromId=' + toId;
    return this._http.put(url, body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Request to add value to Subject failed -' + response.status)
        }
        else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  _errorHandler(_error: Response) {
    return Observable.throw(_error || "Server Error - 404.");
  }
}