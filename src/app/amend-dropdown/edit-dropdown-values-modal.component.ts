import { Component, ViewChild, Input, Output, OnInit, EventEmitter, ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ModalModule } from 'ngx-modal';
import { CompleterService, CompleterData } from 'ng2-completer';
import { DialogService } from "ng2-bootstrap-modal";
import { TosatService } from '../common/toasty-service';
import { AmendDropdownService } from './amend-dropdown.service';
import { AlertComponent } from '../common/alert-component';
import { ConfirmComponent } from '../common/confirm-component';

@Component({
	selector: 'app-edit-dropdown-values-modal',
	templateUrl: 'edit-dropdown-values-modal.component.html',
	styleUrls: ['amend-dropdown.component.css'],
    providers: [ AmendDropdownService ]
})
export class EditDropdownValuesModalComponent implements OnInit {
	@ViewChild('editDropdownValuesModal') editDropdownValuesModal: ModalModule;
	@ViewChild('cancel') cancel: ElementRef;
	// Get table name, dropdown value from 'AmendDropdownComponent' for selected dropdown values.
	@Input() tableName: string;
	@Input() dropdownValue: string;
	@Input() dropdownValueLastname: string;
	@Input() originalValue: string;
	@Input() dropdownValueID: number;
	@Input() tableData : any;
	@Output() public updatedDropdownValue:EventEmitter<any> = new EventEmitter();
	@Output() public valuesMerged: EventEmitter<any> = new EventEmitter();
	@Output() public openEditModal: EventEmitter<any> = new EventEmitter();
	public editDropdownValuesForm: FormGroup;
	public errorMessage: string;
	public userName = window['keycloak'].tokenParsed?window['keycloak'].tokenParsed.preferred_username:null;
	public errorMsg;
	public updatedDropdown = {};
	
    constructor(private builder: FormBuilder,
				private completerService: CompleterService,
				private _dialogService: DialogService,
				private tosatservice:TosatService,
				public _amendDropdownService: AmendDropdownService) {}

	ngOnInit() {
		this.editDropdownValuesForm = this.builder.group({
			dropdownValue: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(30)]),
			dropdownCountrySitterValue: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)]),
			dropdownCountrySitterValueLname: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(100)])			
		});
		this.editDropdownValuesForm.reset();
	}
	closePopup(){
		this.cancel.nativeElement.click();
	}
	updateDropdownValue() {
		var dropdown = {
			id:0,
			fname:'',
			lname:'',
			name:''
		};
		var dropFlag = false;
		if(this.tableName == 'sitter'){
			dropdown = {
				id: this.dropdownValueID,
				fname: this.dropdownValue?this.dropdownValue.trim():null,
				lname:this.dropdownValueLastname?this.dropdownValueLastname.trim():null,
				name:'',
			}
			if(dropdown.fname&&dropdown.lname){
				dropFlag = true;
			}
		} else {
			 dropdown = {
				id: this.dropdownValueID,
				name: this.dropdownValue.trim(),
				fname:'',
				lname:''
			}
			if(dropdown.name){
				dropFlag = true;
			}
		}
		this.updatedDropdown = dropdown;
		if(dropFlag){
			this.closePopup();
			switch(this.tableName){
				case 'presizetext':
				this._amendDropdownService.updateTablePresizeType(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully updated value to '+dropdown.name+'.','top-right');
						this.updatedDropdownValue.emit(response);
					},
					error => {
						this.displayErrorDetails("update", this.dropdownValue, error);
					}
				);
				break;
				case 'category':
				this._amendDropdownService.updateTableCategories(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully updated value to '+dropdown.name+'.','top-right');
						this.updatedDropdownValue.emit(response);
					},
					error => {
						this.displayErrorDetails("update", this.dropdownValue, error);
					}
				);
				break;
				case 'editionsizetype':
				this._amendDropdownService.updateTableEditionSizeType(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully updated value to '+dropdown.name+'.','top-right');
						this.updatedDropdownValue.emit(response);
					},
					error => {
						this.displayErrorDetails("update", this.dropdownValue, error);
					}
				);
				break;
				case 'maincolour':
				this._amendDropdownService.updateTableMainColour(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully updated value to '+dropdown.name+'.','top-right');
						this.updatedDropdownValue.emit(response);
					},
					error => {
						this.displayErrorDetails("update", this.dropdownValue, error);
					}
				);
				break;
				case 'country':
				this._amendDropdownService.updateTableCountry(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully updated value to '+dropdown.name+'.','top-right');
						this.updatedDropdownValue.emit(response);
					},
					error => {
						this.displayErrorDetails("update", this.dropdownValue, error);
					}
				);
				break;
				case 'sitter':
				this._amendDropdownService.updateTableSitter(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully updated value to '+dropdown.fname+dropdown.lname+'.','top-right');
						this.updatedDropdownValue.emit(response);
					},
					error => {
						this.displayErrorDetails("update", dropdown.fname+' '+dropdown.lname, error);
					}
				);
				break;
				case 'animal':
				this._amendDropdownService.updateTableAnimal(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully updated value to '+dropdown.name+'.','top-right');
						this.updatedDropdownValue.emit(response);
					},
					error => {
						this.displayErrorDetails("update", this.dropdownValue, error);
					}
				);
				break;
				case 'position':
				this._amendDropdownService.updateTablePosition(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully updated value to '+dropdown.name+'.','top-right');
						this.updatedDropdownValue.emit(response);
					},
					error => {
						this.displayErrorDetails("update", this.dropdownValue, error);
					}
				);
				break;
				case 'subject':
				this._amendDropdownService.updateTableSubject(dropdown)
				.subscribe(
					response => {
						this.tosatservice.addToast('Successfully updated value to '+dropdown.name+'.','top-right');
						this.updatedDropdownValue.emit(response);
					},
					error => {
						this.displayErrorDetails("update", this.dropdownValue, error);
					}
				);
				break;
			}
		} else{
			this.tosatservice.addToastError('Please enter valid input...!!!','top-right');			
		}
	}

	public deleteDropdownValue() {
		this._dialogService.addDialog(ConfirmComponent, {
			title: 'Delete',
			message: "Are you sure you would like to delete this value?"})
			.subscribe((isConfirmed) => {
			  if(isConfirmed){
				this.cancel.nativeElement.click();
				switch(this.tableName){
					case 'presizetext':
					this._amendDropdownService.deleteDropdownValuePresizeType(this.dropdownValueID,)
					.subscribe(
						response => {
							this.valuesMerged.emit();
							this.tosatservice.addToast('Successfully deleted value - '+this.dropdownValue+'.','top-right');
						},
						error => {
							this.displayErrorDetails("delete", this.dropdownValue, error);
						}
					);
					break;
					case 'category':
					this._amendDropdownService.deleteDropdownValueCategories(this.dropdownValueID,)
					.subscribe(
						response => {
							this.valuesMerged.emit();
							this.tosatservice.addToast('Successfully deleted value - '+this.dropdownValue+'.','top-right');
						},
						error => {
							this.displayErrorDetails("delete", this.dropdownValue, error);
						}
					);
					break;
					case 'editionsizetype':
					this._amendDropdownService.deleteDropdownValueEditionSizeType(this.dropdownValueID)
					.subscribe(
						response => {
							this.valuesMerged.emit();
							this.tosatservice.addToast('Successfully deleted value - '+this.dropdownValue+'.','top-right');
						},
						error => {
							this.displayErrorDetails("delete", this.dropdownValue, error);
						}
					);
					break;
					case 'maincolour':
					this._amendDropdownService.deleteDropdownValueMainColour(this.dropdownValueID)
					.subscribe(
						response => {
							this.valuesMerged.emit();
							this.tosatservice.addToast('Successfully deleted value - '+this.dropdownValue+'.','top-right');
						},
						error => {
							this.displayErrorDetails("delete", this.dropdownValue, error);
						}
					);
					break;
					case 'country':
					this._amendDropdownService.deleteDropdownValueCountry(this.dropdownValueID)
					.subscribe(
						response => {
							this.valuesMerged.emit();
							this.tosatservice.addToast('Successfully deleted value - '+this.dropdownValue+'.','top-right');
						},
						error => {
							this.displayErrorDetails("delete", this.dropdownValue, error);
						}
					);
					break;
					case 'sitter':
					this._amendDropdownService.deleteDropdownValueSitter(this.dropdownValueID,)
					.subscribe(
						response => {
							this.valuesMerged.emit();
							this.tosatservice.addToast('Successfully deleted value - '+this.dropdownValue+'.','top-right');
						},
						error => {
							this.displayErrorDetails("delete", this.dropdownValue, error);
						}
					);
					break;
					case 'animal':
					this._amendDropdownService.deleteDropdownValueAnimal(this.dropdownValueID,)
					.subscribe(
						response => {
							this.valuesMerged.emit();
							this.tosatservice.addToast('Successfully deleted value - '+this.dropdownValue+'.','top-right');
						},
						error => {
							this.displayErrorDetails("delete", this.dropdownValue, error);
						}
					);
					break;
					case 'position':
					this._amendDropdownService.deleteDropdownValuePosition(this.dropdownValueID,)
					.subscribe(
						response => {
							this.valuesMerged.emit();
							this.tosatservice.addToast('Successfully deleted value - '+this.dropdownValue+'.','top-right');
						},
						error => {
							this.displayErrorDetails("delete", this.dropdownValue, error);
						}
					);
					break;
					case 'subject':
					this._amendDropdownService.deleteDropdownValueSubject(this.dropdownValueID,)
					.subscribe(
						response => {
							this.valuesMerged.emit();
							this.tosatservice.addToast('Successfully deleted value - '+this.dropdownValue+'.','top-right');
						},
						error => {
							this.displayErrorDetails("delete", this.dropdownValue, error);
						}
					);
					break;
					}
			  }        
		  });
	}

	displayErrorDetails(action, name, error) {
		let isDuplicate = false;
		// Update
		if(action === 'update') {
			if(JSON.parse(error['_body']).details.indexOf('ConstraintViolationException')>-1) {
				isDuplicate = true;
			}
			if(error.status===409 || isDuplicate) {
				let errDetail = 'This value -- '+name+' -- already exists. Would you like to merge with the existing value?'
				this._dialogService.addDialog(ConfirmComponent,
					{title:'Already Exists', message: errDetail},{ backdropColor: 'rgba(220,220,220,0.5)' })
					.subscribe((isConfirmed)=>{
						if(isConfirmed) {							
							var targetId = this.tableData[this.tableData.findIndex( element => (element.name?(element.name.trim()):(element.countryName?element.countryName.trim():element.displayName.trim())) == name)].id;
							var that = this;
							this.Mergevalues(targetId,this.updatedDropdown['id'],()=>{
								this.valuesMerged.emit();
							});
						}
						else {
							this.openEditModal.emit();
						}
					});
			}
			else {
				this.tosatservice.addToastError('Failed to update to '+name+'.','top-right');			
			}
		}
		// Delete
		else if (action === 'delete') {
			if(error.status===409 || error.status===404) {
				this._dialogService.addDialog(AlertComponent,
					{title:'Delete', message: "This value '"+name+"' is currently assigned to one or more objects. Please assign a different value to these objects and try again."},{ backdropColor: 'rgba(220,220,220,0.5)' })
					.subscribe((isConfirmed)=>{});
			}
			else {
				this._dialogService.addDialog(AlertComponent,
					{title:'Delete', message: "Failed to delete - "+name},{ backdropColor: 'rgba(220,220,220,0.5)' })
					.subscribe((isConfirmed)=>{});
			}
		}
	}

	Mergevalues(targetId,sourceId,callback){

		switch(this.tableName){

			case 'presizetext':
			this._amendDropdownService.mergevalues(targetId,sourceId,'presize-types')
			.subscribe(Response => {
				  this.tosatservice.addToast('Merge successful','top-right');
			      callback();			
			},resFileError => {
				   this.tosatservice.addToastError('There is an Error in Merge please Try again','top-right');
			});
			break;

			case 'category':
			this._amendDropdownService.mergevalues(targetId,sourceId,'categories')
			.subscribe(Response => {
				  this.tosatservice.addToast('Merge successful','top-right');
			      callback();			
			},resFileError => {
				   this.tosatservice.addToastError('There is an Error in Merge please Try again','top-right');
			});
			break;

			case 'editionsizetype':
			this._amendDropdownService.mergevalues(targetId,sourceId,'edition-size-types')
			.subscribe(Response => {
				  this.tosatservice.addToast('Merge successful','top-right');
			      callback();			
			},resFileError => {
				   this.tosatservice.addToastError('There is an Error in Merge please Try again','top-right');
			});		
			break;

			case 'maincolour':
			this._amendDropdownService.mergevalues(targetId,sourceId,'imageMainColours')
			.subscribe(Response => {
				  this.tosatservice.addToast('Merge successful','top-right');
			      callback();			
			},resFileError => {
				   this.tosatservice.addToastError('There is an Error in Merge please Try again','top-right');
			});				
			break;

			case 'country':
			this._amendDropdownService.mergevalues(targetId,sourceId,'countries')
			.subscribe(Response => {
				  this.tosatservice.addToast('Merge successful','top-right');
			      callback();			
			},resFileError => {
				   this.tosatservice.addToastError('There is an Error in Merge please Try again','top-right');
			});			
			break;

			case 'sitter':
			this._amendDropdownService.mergevalues(targetId,sourceId,'sitters')
			.subscribe(Response => {
				  this.tosatservice.addToast('Merge successful','top-right');
			      callback();			
			},resFileError => {
				   this.tosatservice.addToastError('There is an Error in Merge please Try again','top-right');
			});			
			break;

			case 'animal':
			this._amendDropdownService.mergevalues(targetId,sourceId,'imageAnimals')
			.subscribe(Response => {
				  this.tosatservice.addToast('Merge successful','top-right');
			      callback();			
			},resFileError => {
				   this.tosatservice.addToastError('There is an Error in Merge please Try again','top-right');
			});			
			break;		

			case 'position':
			this._amendDropdownService.mergevalues(targetId,sourceId,'imagePositions')
			.subscribe(Response => {
				  this.tosatservice.addToast('Merge successful','top-right');
			      callback();			
			},resFileError => {
				   this.tosatservice.addToastError('There is an Error in Merge please Try again','top-right');
			});			
			break;

			case 'subject':
			this._amendDropdownService.mergevalues(targetId,sourceId,'imageSubjects')
			.subscribe(Response => {
				  this.tosatservice.addToast('Merge successful','top-right');
			      callback();			
			},resFileError => {
				   this.tosatservice.addToastError('There is an Error in Merge please Try again','top-right');
			});			
			break;
		}

	}
}