import { Component, ViewChild, ElementRef, OnInit, Input, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response } from '@angular/http';
import { DatatableComponent, ColumnMode } from '@swimlane/ngx-datatable';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { CompleterCmp, CompleterService, CompleterData, CompleterItem } from 'ng2-completer';
import { TosatService } from '../common/toasty-service';
import { DialogService } from "ng2-bootstrap-modal";
import { ConfirmComponent } from '../common/confirm-component';
import { AlertComponent } from '../common/alert-component';
import { ModalModule } from 'ngx-modal';
import { Helper } from '../utils/helper';
import { HeaderService } from '../../app/header/header.service';
import { AmendDropdownService } from './amend-dropdown.service';
import { EditDropdownValuesModalComponent } from './edit-dropdown-values-modal.component'
import { AddDropdownValuesModalComponent } from './add-dropdown-values-modal.component';
import { KeyboardkeysService } from '../../app/keyboardkeys/keyboardkeys.service';
import { ConfigurationLoaderService } from '../configuration.service';

class PagedData<T> {
  data: T[];
}

@Component({
  selector: 'app-amend-dropdown',
  templateUrl: './amend-dropdown.component.html',
  styleUrls: ['./amend-dropdown.component.css'],
  providers: [DialogService, Helper, AmendDropdownService, EditDropdownValuesModalComponent, AddDropdownValuesModalComponent, HeaderService, KeyboardkeysService]
})
export class AmendDropdownComponent implements OnInit, AfterViewInit {
  readonly headerHeight = 30;
  readonly rowHeight = 40;
  readonly pageLimit = 10;
  public keycloak = window['keycloak'];
  public userName = this.keycloak.tokenParsed && window['keycloak'].tokenParsed.preferred_username ? window['keycloak'].tokenParsed.preferred_username : null;
  private _url: string;
  public rows = []; // Array of dropdown values.
  public temp = []; // Filter array that keeps track of results matching the search text.
  public rows_copy = []; // Maintains an unedited reference to the original rows (dropdown values)
  public confirmResult: boolean = null;
  public selectedtableData: any;
  @ViewChild('table') table: CompleterCmp;
  @ViewChild(DatatableComponent) tblDropdownValues: DatatableComponent;
  @ViewChild(EditDropdownValuesModalComponent) public editDropdownValuesModal: EditDropdownValuesModalComponent;
  @ViewChild(AddDropdownValuesModalComponent) public addDropdownValuesModal: AddDropdownValuesModalComponent;
  protected tablesList: CompleterData;
  public selectedTable = '';
  public editDropdownValue = '';
  public editDropdownValueLname = '';
  public editDropdownValueID: number;
  private tables = [
    { name: 'Pre-size Text', value: 'presizetext' },
    { name: 'Category', value: 'category' },
    { name: 'Edition Size Type', value: 'editionsizetype' },
    { name: 'Main Colour', value: 'maincolour' },
    { name: 'Country', value: 'country' },
    { name: 'Sitter', value: 'sitter' },
    { name: 'Animal', value: 'animal' },
    { name: 'Position', value: 'position' },
    { name: 'Subject', value: 'subject' },
  ];
  private tablesListOpen = false;
  public tableName = '';
  public displayTableData: boolean = true;

  constructor(private router: Router,
    private _http: Http,
    private _headerService: HeaderService,
    private completerService: CompleterService,
    private tosatservice: TosatService,
    private _dialogService: DialogService,
    private _editDropdownValuesModalComponent: EditDropdownValuesModalComponent,
    private el: ElementRef,
    public helper: Helper,
    private _keyboardkeysService: KeyboardkeysService,
    public _amendDropdownService: AmendDropdownService,
    configuration: ConfigurationLoaderService,
  ) {
    this._url = configuration.getSettings().apiUrl
  }

  ngOnInit() {
    this.tablesList = <any>this.completerService.local(this.tables, 'name', 'name');
    this.rows = [];
  }

  ngAfterViewInit() {
    this.tblDropdownValues.columnMode = ColumnMode.force;
  }

  // Header Drop-down toggle mode
  toggleFlag: number;
  toggleFunction(flag: number) {
    this.toggleFlag = this._headerService.headerToggle((flag === this.toggleFlag) ? 0 : flag);
  }

  redirect(page) {
    switch (page) {
      case 'home':
        this.router.navigateByUrl('/');
        break;

      default:
        this.router.navigateByUrl('/admin');
    }
  }

  updateFilter(event) {
    if (event.target.value == "") {
      this.clearFilter();
    } else {
      this.displayTableData = true;
      const val = event.target.value.toLowerCase();

      // filter
      if (this.selectedTable === 'country') {
        const temp = this.temp.filter(function (d) {
          return d.countryName.toLowerCase().startsWith(val) || !val;
        });
        // update the rows
        this.rows = temp;
      }
      else {
        if (this.selectedTable === 'sitter') {
          const temp = this.temp.filter(function (d) {
            return d.displayName.toLowerCase().startsWith(val) || !val;
          });
          // update the rows
          this.rows = temp;
        } else {
          const temp = this.temp.filter(function (d) {
            return d.name.toLowerCase().startsWith(val) || !val;
          });
          // update the rows
          this.rows = temp;
        }

      }
    }
  }

  clearFilter() {
    if (this.selectedTable == 'animal' || this.selectedTable == 'sitter') {
      this.displayTableData = false;
    } else {
      this.displayTableData = true;
    }
    const val = '';
    if (this.selectedTable === 'country') {
      const temp = this.temp.filter(function (d) {
        return d.countryName.toLowerCase().indexOf(val) !== -1 || !val;
      });
    }
    else {
      if (this.selectedTable === 'sitter') {
        console.log(this.temp);
        const temp = this.temp.filter(function (d) {
          return d.displayName.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.rows = temp;
      } else {
        const temp = this.temp.filter(function (d) {
          return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.rows = temp;
      }
    }
    this.temp = [];
    this.rows = [];
    this.rows_copy.forEach(item => {
      this.temp.push(item);
      this.rows.push(item);
    });
  }

  // Populate 'Edit Dropdown Value' modal form fields.
  populateEditDropdownValuesModal(row) {
    if (this.selectedTable === 'country') {
      this.editDropdownValue = row.countryName;
    }
    else {
      if (this.selectedTable === 'sitter') {
        this.editDropdownValue = row.firstName;
        this.editDropdownValueLname = row.lastName
      } else {
        this.editDropdownValue = row.name;
      }
    }
    this.editDropdownValueID = row.id;
    this.selectedtableData = this.rows_copy;
  }

  // Dynamic update of dropdown values as and when the values are edited.
  updateDropdownValueRowByID(row) {
    let index = this.rows.findIndex(value => row.id == value.id);
    this.rows[index].name = row.name;
    this.loadTable();
  }

  displayDropdownValues(selected) {
    if (selected && selected.originalObject.value) {
      this.selectedTable = selected.originalObject.value;
      this.tableName = selected.originalObject.name;
      this.loadTable();
    }
  }

  loadDropdownValues(limit, dropdownValues) {

    if (this.selectedTable == 'animal' || this.selectedTable == 'sitter') {
      this.displayTableData = false;
    } else {
      this.displayTableData = true;
    }

    let data = [];
    this.rows = [];
    this.rows_copy = [];
    this.temp = [];
    data = dropdownValues;
    data.forEach(item => {
      if (item) {
        this.temp.push(item);
        this.rows_copy.push(item);
      }
    })
    this.rows = [];
    this.rows_copy.forEach(item => {
      this.rows.push(item);
    });

    this.temp.sort(function (a, b) {
      if ((a.name ? a.name : (a.countryName ? a.countryName : (a.displayName ? a.displayName : ''))).toUpperCase() < (b.name ? b.name : (b.countryName ? b.countryName : (b.displayName ? b.displayName : ''))).toUpperCase())
        return 1;
      if ((a.name ? a.name : (a.countryName ? a.countryName : (a.displayName ? a.displayName : ''))).toUpperCase() > (b.name ? b.name : (b.countryName ? b.countryName : (b.displayName ? b.displayName : ''))).toUpperCase())
        return -1;
      return 0;
    })


    this.rows_copy.sort(function (a, b) {
      if ((a.name ? a.name : (a.countryName ? a.countryName : (a.displayName ? a.displayName : ''))).toUpperCase() < (b.name ? b.name : (b.countryName ? b.countryName : (b.displayName ? b.displayName : ''))).toUpperCase())
        return 1;
      if ((a.name ? a.name : (a.countryName ? a.countryName : (a.displayName ? a.displayName : ''))).toUpperCase() > (b.name ? b.name : (b.countryName ? b.countryName : (b.displayName ? b.displayName : ''))).toUpperCase())
        return -1;
      return 0;
    })

    this.rows.sort(function (a, b) {
      if ((a.name ? a.name : (a.countryName ? a.countryName : (a.displayName ? a.displayName : ''))).toUpperCase() < (b.name ? b.name : (b.countryName ? b.countryName : (b.displayName ? b.displayName : ''))).toUpperCase())
        return 1;
      if ((a.name ? a.name : (a.countryName ? a.countryName : (a.displayName ? a.displayName : ''))).toUpperCase() > (b.name ? b.name : (b.countryName ? b.countryName : (b.displayName ? b.displayName : ''))).toUpperCase())
        return -1;
      return 0;
    })
  }

  // System should not accept values other than the ones in the drop-down list.
  validateSelected(table) {
    if (table) {
      table = table.toLowerCase();
      let selectListObject;
      let index = this.tables.findIndex(list => list.name.toLowerCase() === table);
      if (!(index > -1)) {
        this.table.value = '';
      }
    }
    else {
      return false;
    }
  }

  // 'Caret' (select list) click event 
  private openList(list, listName) {
    if (listName == 'table') {
      this.tablesListOpen = !this.tablesListOpen;
      if (this.tablesListOpen === true) {
        list.open();
        this.tablesList.search('');
      }
      else {
        list.close();
      }
    }
  }

  refreshTable(row) {
    this.temp = this.rows.filter(r => r !== row);
    this.rows = this.rows.filter(r => r !== row);
    this.rows_copy = this.rows_copy.filter(r => r !== row);
  }

  loadTable() {
    if (this.selectedTable != '') {
      this.rows = [];
      switch (this.selectedTable) {
        case 'presizetext':
          this._amendDropdownService.getTablePresizeType()
            .subscribe(
              response => {
                this.loadDropdownValues(this.pageLimit, response);
              },
              error => {
                this.tosatservice.addToastError('Failed to load Pre-size Types.', 'top-right');
              }
            );
          break;
        case 'category':
          this._amendDropdownService.getTableCategories()
            .subscribe(
              response => {
                this.loadDropdownValues(this.pageLimit, response);
              },
              error => {
                this.tosatservice.addToastError('Failed to load Categories.', 'top-right');
              }
            );
          break;
        case 'editionsizetype':
          this._amendDropdownService.getTableEditionSizeType()
            .subscribe(
              response => {
                this.loadDropdownValues(this.pageLimit, response);
              },
              error => {
                this.tosatservice.addToastError('Failed to load Edition Size Types.', 'top-right');
              }
            );
          break;
        case 'maincolour':
          this._amendDropdownService.getTableMainColour()
            .subscribe(
              response => {
                this.loadDropdownValues(this.pageLimit, response);
              },
              error => {
                this.tosatservice.addToastError('Failed to load Main Colors.', 'top-right');
              }
            );
          break;
        case 'country':
          this._amendDropdownService.getTableCountry()
            .subscribe(
              response => {
                this.loadDropdownValues(this.pageLimit, response);
              },
              error => {
                this.tosatservice.addToastError('Failed to load Countries.', 'top-right');
              }
            );
          break;
        case 'sitter':
          this._amendDropdownService.getTableSitter()
            .subscribe(
              response => {
                this.loadDropdownValues(this.pageLimit, response);
              },
              error => {
                this.tosatservice.addToastError('Failed to load Sitters.', 'top-right');
              }
            );
          break;
        case 'animal':
          this._amendDropdownService.getTableAnimal()
            .subscribe(
              response => {
                this.loadDropdownValues(this.pageLimit, response);
              },
              error => {
                this.tosatservice.addToastError('Failed to load Animals.', 'top-right');
              }
            );
          break;
        case 'position':
          this._amendDropdownService.getTablePosition()
            .subscribe(
              response => {
                this.loadDropdownValues(this.pageLimit, response);
              },
              error => {
                this.tosatservice.addToastError('Failed to load Positions.', 'top-right');
              }
            );
          break;
        case 'subject':
          this._amendDropdownService.getTableSubject()
            .subscribe(
              response => {
                this.loadDropdownValues(this.pageLimit, response);
              },
              error => {
                this.tosatservice.addToastError('Failed to load Subjects.', 'top-right');
              }
            );
          break;
      }
    }
  }

  alertSelectTable() {
    this._dialogService.addDialog(AlertComponent,
      { title: 'Select Table', message: "Please select a table." }, { backdropColor: 'rgba(220,220,220,0.5)' })
      .subscribe((isConfirmed) => { });
  }
  // Keyboard shortcuts
  keyboardShortCutamendDropDownFunction(e) {
    switch (this._keyboardkeysService.getKeyboardResponse(e)) {
      case 'duplicateWindow':
        let urlP = window.location.href;
        window.open(urlP, '_blank');
        break;
    }
  }
}