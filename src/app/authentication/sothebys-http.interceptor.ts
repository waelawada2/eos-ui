import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/fromPromise';
import * as _ from 'lodash';

export const AUTH_HEADER = 'Authorization';
export const TOKEN_PREFIX = 'Bearer';
const acceptedUrls = ['/property-service/', '/event-service/', '/eos-client-service/'];

const checkTokenRequired = (url: string) => {
  if (!url) {
    return false
  }
  return _.filter(acceptedUrls, value => url.toLowerCase().indexOf(value.toLowerCase()) > 0
  ).length > 0;
}

const refreshToken = (keycloak) => {
  return Observable.fromPromise(new Promise((res, rej) => {
    // Convert to Promise/A+ standard
    const result = keycloak.updateToken(10)
    result.success((refreshed) => res(refreshed))
    result.error((e) => rej(e))
  })).catch((error) => {
    return Observable.throw(error);
  }).map((refreshed: boolean) => {
    // Get refreshed data and act accordingly
    if (refreshed) {
      console.info('Token refreshed.');
    } else {
      console.error('Failed to refresh token.')
      keycloak.logout();
    }
    return refreshed;
  });
}

const getToken = () => {
  const keycloak: any = window['keycloak'];
  const resetButton = document.getElementById('reset-timer-button')
  if (keycloak.token && !keycloak.isTokenExpired()) {
    return Observable.of(keycloak.token)
  } else {
    const response = refreshToken(keycloak).map((result) => result ? keycloak.token : null);
    if (response) {
      if (resetButton) {
        resetButton.click()
      }
      return response;
    } else {
      return null;
    }

  }
}

export const addInterceptorForXhrRequest = () => {

  const open = XMLHttpRequest.prototype.open;

  XMLHttpRequest.prototype.open = function (method, url, async) {
    if (checkTokenRequired(url)) {
      this._tokenRequired = true;
    }
    open.apply(this, arguments);
  }

  const send = XMLHttpRequest.prototype.send;

  XMLHttpRequest.prototype.send = function (method) {
    const request = this
    const resetButton = document.getElementById('reset-timer-button')
    const args = arguments
    if (this._tokenRequired) {
      const newToken = getToken()
      newToken.take(1).subscribe((token) => {
        request.setRequestHeader(AUTH_HEADER, `${TOKEN_PREFIX} ${token}`);
        if (resetButton) {
          resetButton.click()
        }
        send.apply(request, args);
      }, error => {
        window['keycloak'].logout()
      })
    } else {
      if (resetButton) {
        resetButton.click()
      }
      send.apply(this, arguments)
    }
  }

}
