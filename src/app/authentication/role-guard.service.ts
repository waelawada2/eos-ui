import { Injectable } from '@angular/core';
import { 
  Router,
  CanActivate,
  ActivatedRouteSnapshot
} from '@angular/router';
@Injectable()
export class RoleGuardService implements CanActivate {
  constructor(public router: Router) {}
  canActivate(route: ActivatedRouteSnapshot): boolean {
    let keycloak = window['keycloak'];
    const isAdmin = keycloak.resourceAccess["property-database-ui"].roles.includes("ADMIN") ? true : false;
    
    if (!isAdmin) {
      this.router.navigate(['']);
      return false;
    }
    return true;
  }
}