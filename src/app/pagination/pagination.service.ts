import { Injectable } from '@angular/core';

@Injectable()
export class PaginationService {

  public resObject:object;
  constructor() { }

  getNextPage(Response){
    Response.pageNo = (Response.curPageValue-(Response.flagPage==='nextPage'?0:2))/20;
    Response.pageNo = parseInt(Response.pageNo);
    if(Response.flagPage==='nextPage'){
      Response.curPageValue = parseInt(Response.curPageValue) + 1;
    } else{
      Response.curPageValue = parseInt(Response.curPageValue) - 1;
    }
    Response.curPageValue = (Response.curPageValue < 1)?1:Response.curPageValue;
    if( (Response.pageNo === Response.pageNumber)&& Response.pageData.length>0){
      Response.pageNumber = Response.pageNo;
       if(Response.pageData.length<20) {
        var arrIndex = (Response.curPageValue-1) % 20;
       }
       else {
        var arrIndex = (Response.curPageValue-(Response.flagPage==='nextPage'?1:1)) % Response.pageData.length;
       }
       this.resObject = {
          pageNo: Response.pageNo,
          curPageValue: Response.curPageValue,
          pageNumber: Response.pageNumber,
          pageFlag: 'single',
          arryIndex: arrIndex
        };
        return this.resObject;
    } else{
      Response.pageNumber = Response.pageNo;
      this.resObject = {
        pageNo: Response.pageNo,
        curPageValue: Response.curPageValue,
        pageNumber: Response.pageNumber,
        pageFlag: 'pagination'
      };
      return this.resObject;
    }
  }

}
