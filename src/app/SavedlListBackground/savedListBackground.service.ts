import { Injectable,EventEmitter } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Injectable()

export class SavedListBackgroundService{

    public savedListData : object;

    private itemAddedtoList = new BehaviorSubject<any>(0);
    private RemoveProgressbar = new BehaviorSubject<any>(false);

    public itemAdded$ = this.itemAddedtoList.asObservable();
    public removeProgress$ = this.RemoveProgressbar.asObservable();
    

    constructor() {
        
    }

    public add(item) {
         this.itemAddedtoList.next(item);
    }

    public removeDialog(){
        this.RemoveProgressbar.next(true);
    }


}