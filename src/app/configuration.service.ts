import { Inject, Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ConfigurationLoaderService {

  private configuration = {};

  constructor(private http: Http) {

  }

  initializeApp() {
    return this.http
      .get('./assets/config.json')
      .catch((error: Response) => {
        console.error(`The configuration Endpoint could not be reached: ${error.statusText}`)
        return Observable.from([{ error: 'error' }]);
      })
      .map((response: Response) => response.json())
      .do((config) => {
        this.configuration = config;
      }).toPromise()
  }

  getSettings(): any {
    return this.configuration;
  }
}
