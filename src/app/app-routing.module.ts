import { NgModule } from "@angular/core";
import { RouterModule, Routes, CanActivate } from "@angular/router";
import { HomeComponent } from "./home.component";
import { ObjectComponent } from './object/object.component';
import { ArtistComponent } from './artist/artist.component';
import { ImportexportComponent } from './importexport/importexport.component';
import {MergeComponent} from './Merge/Merge.component';
import { AdminComponent } from './admin/admin.component';
import { AmendDropdownComponent } from './amend-dropdown/amend-dropdown.component';
import { RoleGuardService as RoleGuard } from './authentication/role-guard.service';
import { AdminSavedList } from './adminSavedList/admin-saved-list.component';
// Router
const routes: Routes = [
	{ path: '', component: HomeComponent },
	{ path: 'object', component: ObjectComponent},
	{ path: 'artist', component: ArtistComponent },
	{ path: 'importexport', component: ImportexportComponent},
	{ path: 'thumbnail', component: MergeComponent},
	{ path: 'admin', component: AdminComponent},
	{ path: 'amend-dropdown', component: AmendDropdownComponent, canActivate: [RoleGuard]},
	{ path: 'adminsavedList',component: AdminSavedList,canActivate:[RoleGuard]}
]

//angular
@NgModule({
	imports: [
		RouterModule.forRoot(routes)
	],
	exports: [
		RouterModule
	]
})

export class AppRoutingModule{

}

export const routingComponents = [
									HomeComponent,
									ObjectComponent,
									ArtistComponent,
									ImportexportComponent,
									AdminComponent
								]
