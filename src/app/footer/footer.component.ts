import { Component, OnInit, Input } from '@angular/core';
import { FooterUser } from '../../app/footer/footer.service';

@Component({
  selector: 'apps-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

   @Input() userName: FooterUser;

   constructor() {
   }

   ngOnInit() {
     console.log(this.userName);
   }
}
