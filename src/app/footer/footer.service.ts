import { Injectable } from '@angular/core';

export class FooterUser {
  createdDate: string;
  createdBy: string;
  updatedDate: string;
  updatedBy: string;
}

@Injectable()
export class FooterService {

  constructor() { }

}
