import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { HeaderService } from './header/header.service';
import { SavedSearchComponent } from './saved-search/saved-search.component';
import { SavedListComponent } from './saved-list/saved-list.component';
import { GeneralDictionary } from './shared/constants'
import { KeyboardkeysService } from './keyboardkeys/keyboardkeys.service';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['home.component.scss'],
  providers: [HeaderService, KeyboardkeysService]
})
export class HomeComponent implements OnInit {
  public keycloak = window['keycloak'];
  generalDictionary = GeneralDictionary.HOME;
  public isAdmin = this.keycloak.resourceAccess["property-database-ui"].roles.includes("ADMIN") ? true : false;

  // Header Drop-down toggle mode
  toggleFlag: number;
  constructor(private router: Router, private _headerService: HeaderService, private _keyboardkeysService: KeyboardkeysService) {
    sessionStorage.removeItem('selectSort');
    sessionStorage.removeItem('indexSort');
    sessionStorage.removeItem('MainViewQuery');
    sessionStorage.removeItem('MainQueryValue');
    sessionStorage.removeItem('searchparam');
    sessionStorage.removeItem('searchData');
    sessionStorage.removeItem('sortparam');
    sessionStorage.removeItem('totalelements');
    sessionStorage.removeItem('Savedlistsortingquery');
    sessionStorage.removeItem('refreshPage');
  }

  ngOnInit() {
    // Display the selected object from thumbnail view in a new tab.
    if (sessionStorage.getItem('viewobjectid')) {
      let id = sessionStorage.getItem('viewobjectid');
      this.router.navigate(['/object'], { queryParams: { id: id } });
    }
  }


  toggleFunction(flag: number) {
    this.toggleFlag = this._headerService.headerToggle((flag === this.toggleFlag) ? 0 : flag);
  }

  //redirect to 'Object' module
  onObject() {
    this.router.navigate(['/object'])
  }
  //redirect to 'Artist' module
  onArtist() {
    this.router.navigate(['/artist'])
  }
  //redirect to 'Import/Export' module
  onImportExport() {
    this.router.navigate(['/importexport'])
  }
  // redirect to 'Admin' module
  onAdmin() {
    this.router.navigate(['/admin'])
  }
  keyboardShortCutFunction(e) {
    switch (this._keyboardkeysService.getKeyboardResponse(e)) {
      case 'objectView':
        this.onObject();
        break;
      case 'duplicateWindow':
        let urlP = window.location.href;
        window.open(urlP, '_blank');
        break;
    }
  }
  onPublication() {
    this.router.navigate(['/publication/search'])
  }

  onTransaction() {
    this.router.navigate(['/transaction/search'])
  }

  onConsignmentPurchase() {
    this.router.navigateByUrl('/consignment-purchase/search')
  }

  onPricing() {
    this.router.navigateByUrl('/pricing/search')
  }
}
