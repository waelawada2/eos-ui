import { Component, OnInit, ViewChild, AfterViewInit, ContentChild, AfterContentInit } from '@angular/core';
import { Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ObjectService } from '../../object/object.service';
import Dropzone from 'dropzone';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ConfirmComponent } from '../../common/confirm-component';
import { DialogService } from 'ng2-bootstrap-modal';
import { AlertComponent } from '../../common/alert-component';
import { ConfigurationLoaderService } from 'app/configuration.service'

@Component({
  selector: 'app-new-object-dropupload',
  templateUrl: './new-object-dropupload.component.html',
  styleUrls: ['./dropupload.component.css']
})
export class DropuploadComponent implements OnInit {

  private URL: string;
  public _dropzone: Dropzone;
  public fullPath: string;
  public userId: any;
  public extenalID: any;
  @Input() externalId: any;
  @Output() FullPathChange: EventEmitter<any> = new EventEmitter();
  public rem: any;
  @ViewChild('fetch')
  private fetch: ElementRef;
  @ViewChild('previewImageButton')
  private previewImageButton: ElementRef;
  @ViewChild('previewImg')
  private previewImg: ElementRef;
  public srcFlag = true;
  public imagePresent = false;
  public defaultFlag = false;

  constructor(
    private _eleRef: ElementRef,
    private activatedRoute: ActivatedRoute,
    private objectService: ObjectService,
    private dialogService: DialogService,
    private http: Http,
    configuration: ConfigurationLoaderService,
  ) {
    this.URL = configuration.getSettings().apiUrl;
  }

  fileDropped(): boolean {
    if (this._dropzone && this._dropzone.files.length > 0) {
      this.imagePresent = true;
      document.getElementById('newObjectDeleteImageButton').style.display = "block";
    }
    return false;
  }

  delete() {
    if (this._dropzone) {
      this.previewImageButton.nativeElement.style.display = "none";
      this.deleteImage();
      document.getElementById('property-image').style.display = "none"
    }
  }

  public upload(id) {
    this._dropzone.options.url = `${this.URL}/properties/${id}/images`;
    this._dropzone.processQueue();
  }

  deleteImage() {
    if (this.imagePresent) {
      this._dropzone.removeAllFiles();
      this.imagePresent = false;
      this._dropzone.options.maxFiles = 0;
      document.getElementById('newObjectDeleteImageButton').style.display = "none";
    }
  }


  initDropzone() {
    this._dropzone = new Dropzone('div#new_object_dropzone', {
      url: "images/put",
      thumbnailHeight: null,
      thumbnailWidth: null,
      autoProcessQueue: false,
      addRemoveLinks: false,
      uploadMultiple: false,
      parallelUploads: 1,
      hiddenInputContainer: '#new-object-dropzone-drop-area',
      dictDefaultMessage: '',
      maxFiles: 1,
      acceptedFiles: 'image/*',
      clickable: '#new-object-dropzone-drop-area',
      previewsContainer: '#new-object-dropzone-drop-area',
      previewTemplate: `
            <div  id="previewImage"  class="dz-preview dz-file-preview" style="position:absolute;top:0;width:100%;height:100%;line-height:500px;">
            <div  class="dz-details" style="position:absolute;top:0;width:100%;height:100%;">
                <img  data-dz-thumbnail style="max-width:480px; max-height: 480px;" />
            </div>
            </div>
    `
    });
    (<HTMLInputElement>document.getElementsByClassName('dz-hidden-input')[0]).disabled = true;

    this.resetDropzoneStyle();

    this._dropzone.on("success", (file) => { });

    this._dropzone.on("queuecomplete", (file, res) => { });

    this._dropzone.on("maxfilesexceeded", (file) => { });

    this._dropzone.on("error", (file, message) => { });

    this._dropzone.on("addedfile", (file) => {
      if (this._dropzone.files.length > 1) {
        let newFile = this._dropzone.files[1];
        this._dropzone.removeFile(file);
        let disposable = this.dialogService.addDialog(ConfirmComponent, {
          title: 'Confirm',
          message: 'Are you sure you would like to replace this image?'
        })
          .subscribe((isConfirmed) => {
            document.getElementsByTagName("body")[0].classList.remove("positionFixed");
            if (isConfirmed) {
              if (this._dropzone.files[0]) {
                this._dropzone.removeFile(this._dropzone.files[0]);
              }
              this._dropzone.addFile(newFile);
            }
          });
        setTimeout(() => {
          disposable.unsubscribe();
        }, 100000);
      }
    });
  }

  ngOnInit() {
    this.initDropzone();

    // Border style - orange on 'dragover'
    this.previewImg.nativeElement.addEventListener("dragover", (ev: DragEvent) => {
      this.highlightDropzone();
    });

    // Border style - #ccc on 'drop'
    this.previewImg.nativeElement.addEventListener("drop", (ev: DragEvent) => {
      this.resetDropzoneStyle();
    });

    // Border style - #ccc on 'dragleave'
    this.previewImg.nativeElement.addEventListener("dragleave", (ev: DragEvent) => {
      this.resetDropzoneStyle();
    });
  }

  highlightDropzone() {
    this.previewImg.nativeElement.style.border = '1px dashed darkorange';
    this.previewImg.nativeElement.style.color = 'darkorange';
  }

  resetDropzoneStyle() {
    this.previewImg.nativeElement.style.border = '1px solid #ccc';
    this.previewImg.nativeElement.style.color = '#ccc';
  }
}