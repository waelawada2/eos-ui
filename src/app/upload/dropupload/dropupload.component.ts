import { Component, OnInit, ViewChild, AfterViewInit, ContentChild, AfterContentInit } from '@angular/core';
import { Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ObjectService } from "../../object/object.service";
import * as Dropzone from 'dropzone';
import { ConfigurationLoaderService } from 'app/configuration.service'
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { ConfirmComponent } from '../../common/confirm-component';
import { DialogService } from "ng2-bootstrap-modal";
import { AlertComponent } from '../../common/alert-component';

@Component({
  selector: 'app-dropupload',
  templateUrl: './dropupload.component.html',
  styleUrls: ['./dropupload.component.css']
})
export class DropuploadComponent implements OnInit {
  private URL: string;
  private _dropzone: Dropzone;
  public fullPath: string;
  public userId: any;
  public extenalID: any;
  @Input() externalId: any;
  @Output() FullPathChange: EventEmitter<any> = new EventEmitter();
  public rem: any;
  @ViewChild('fetch')
  private fetch: ElementRef;
  @ViewChild('previewImageButton')
  private previewImageButton: ElementRef;
  @ViewChild('previewImg')
  private previewImg: ElementRef;
  public srcFlag = true;
  public imagePresent = false;
  public defaultFlag = false;


  constructor(private _eleRef: ElementRef,
    private activatedRoute: ActivatedRoute,
    private objectService: ObjectService,
    private dialogService: DialogService,
    private http: Http,
    configuration: ConfigurationLoaderService,
  ) {
    this.URL = configuration.getSettings().apiUrl;
  }


  errorHandler() {
    this.srcFlag = false;
  }
  fileDropped(): boolean {
    if (this._dropzone && this._dropzone.options.maxFiles == 1) {
      if (this._dropzone.files.length >= 1) {
        this._dropzone.options.maxFiles = 1;
        this.upload();
        return true;
      }
      else {

        return false;
      }
    }
    return false;
  }
  over() {
    if (this.defaultFlag) {
      document.getElementById('property-image').style.display = "none";
    } else {
      document.getElementById('property-image').style.display = "block";
    }
  }
  propertyOver() {
    document.getElementById('property-image').style.display = "block"
  }
  propertyOut() {
    document.getElementById('property-image').style.display = "none"
  }
  out() {
    document.getElementById('property-image').style.display = "none"
  }
  reset() {
    this.fetch.nativeElement.style.display = "none";
    document.getElementById('deletePreview').style.display = "none";
    this.previewImageButton.nativeElement.style.display = "none";
    if (document.getElementById('previewImage')) {
      document.getElementById('previewImage').style.display = "none";
    }
  }
  delete() {
    if (this._dropzone) {
      this.previewImageButton.nativeElement.style.display = "none";
      //this._dropzone.removeAllFiles();
      this.imagePresent = true;
      this.deleteImage();
      this.imagePresent = false;
      document.getElementById('property-image').style.display = "none"

    }
  }
  upload() {
    this._dropzone.options.url = this.URL + "/properties/" + this.externalId + "/images";
    this._dropzone.processQueue();
  }

  deleteImage() {
    if (this.imagePresent) {
      this.showConfirm('delete', 'Are you sure would you like to delete this image?');
    }

  }
  showConfirm(flag, message) {
    document.getElementsByTagName("body")[0].classList.add("positionFixed");
    let disposable = this.dialogService.addDialog(ConfirmComponent, {
      title: 'Confirm',
      message: message
    })
      .subscribe((isConfirmed) => {
        document.getElementsByTagName("body")[0].classList.remove("positionFixed");
        if (isConfirmed) {
          if (flag == 'delete') {

            this.imagePresent = false;
            document.getElementById('uploadtext').style.display = "block";
            this._dropzone.removeAllFiles();
            if (document.getElementById('previewImage')) {
              document.getElementById('previewImage').style.display = "block";
            }
            this.fullPath = this.URL + "/properties/" + this.extenalID + "/images/default";
            this.objectService.removeImage(this.fullPath).subscribe(Response => {
              this._dropzone.options.maxFiles = 1;
              var urlValue = window.location.href.indexOf('object');
              (<HTMLInputElement>document.getElementsByClassName('dz-hidden-input')[0]).disabled = false;
              var newURL = window.location.href.substring(0, urlValue);
              this.fullPath = newURL + "assets/images/default.png";
              this.defaultFlag = true;
              if (document.getElementById('previewImage')) {
                document.getElementById('previewImage').style.display = "none";
              }
              document.getElementById('deleteimagebutton').style.display = "none";
              document.getElementById('deletebutton').style.display = "none";

            })
            this.fetch.nativeElement.style.display = "none";
          }
          else if (flag == 'dragDrop') {
            this.fileDropped();
          }
        }
        else {
          if (flag == 'dragDrop') {
            this._dropzone.options.maxFiles = 0;
            return false;
          }
        }
      });
    setTimeout(() => {
      disposable.unsubscribe();
    }, 100000);
  }

  maxFiles() {
    this._dropzone.options.maxFiles = 1;
    if (document.getElementById('previewImage')) {
      document.getElementById('previewImage').style.display = "block";
    }
  }
  showDiv(extenalID) {
    if (document.getElementById('previewImage')) {
      document.getElementById('previewImage').style.display = "none";
      this.previewImageButton.nativeElement.style.display = "none";
    }

    this.objectService.getProperty(extenalID).subscribe((response) => {
      var d = new Date();
      var n = d.getTime();
      if (response.imagePath && response.imagePath.length > 0) {
        this.defaultFlag = false;
        this.fullPath = this.URL + "/properties/" + extenalID + "/images/default?" + n;
        this.FullPathChange.emit(this.fullPath);
        document.getElementById('fetch').classList.add('gray-background');
        this._dropzone.options.maxFiles = 1;
        this.imagePresent = true;
        document.getElementById('deleteimagebutton').style.display = "block";
        (<HTMLInputElement>document.getElementsByClassName('dz-hidden-input')[0]).disabled = true;
      } else {
        this.defaultFlag = true;
        var urlValue = window.location.href.indexOf('object');
        var newURL = window.location.href.substring(0, urlValue);
        this.fullPath = newURL + "assets/images/default.png";
        document.getElementById('fetch').classList.remove('gray-background');
        this.srcFlag = true;
        this.imagePresent = true;
        this._dropzone.options.maxFiles = 1;
        (<HTMLInputElement>document.getElementsByClassName('dz-hidden-input')[0]).disabled = false;
      }
      if (this.fullPath.substr(this.fullPath.length - 7, this.fullPath.length) !== 'default') {
        document.getElementById('uploadtext').style.display = "none";
      } else {
        document.getElementById('uploadtext').style.display = "block";
      }
      if ((this.fullPath.substr(this.fullPath.length - 11, this.fullPath.length)) === 'default.png')
        document.getElementById('uploadtext').style.display = "block";

    }, (error) => { });
    this.extenalID = extenalID;
    this.srcFlag = true;
    this.fetch.nativeElement.style.display = "block";
  }

  initDropzone() {

    this._dropzone = new Dropzone('div#my_dropzone', {
      url: "images/put",
      autoProcessQueue: false,
      addRemoveLinks: false,
      uploadMultiple: false,
      parallelUploads: 1,
      thumbnailWidth: 380,
      thumbnailHeight: 168,
      hiddenInputContainer: '#dropzone-drop-area',
      dictDefaultMessage: '',
      maxFiles: 1,
      acceptedFiles: 'image/*',
      clickable: '#dropzone-drop-area',
      previewsContainer: '#dropzone-drop-area',
      previewTemplate: `
    <div  id="previewImage"  class="dz-preview dz-file-preview" style="position:absolute;top:0;width:100%;height:170px;">
     
      <div  class="dz-details" style="position:absolute;top:0;width:100%;height:172px;">
      <img  data-dz-thumbnail style="max-width:100%;max-height:170px;height:168px;display:`+ (this.defaultFlag ? 'block' : 'none') + `" />
      </div>
    </div>
`
    });
    (<HTMLInputElement>document.getElementsByClassName('dz-hidden-input')[0]).disabled = true;
    this._dropzone.on("success", (file) => {
      document.getElementById('deleteimagebutton').style.display = "none";
      (<HTMLInputElement>document.getElementsByClassName('dz-hidden-input')[0]).disabled = true;
      document.getElementById('deletebutton').style.display = "block";
      document.getElementById('previewImage').addEventListener('mouseover', this.propertyOver);
      document.getElementById('previewImage').addEventListener('mouseout', this.propertyOut);
      document.getElementById('uploadtext').style.display = "none";
      if (document.getElementById('previewImage').getElementsByTagName('img')[0].src) {
        if (document.getElementById('property-image').getElementsByTagName('img')[0] == undefined) {
          document.getElementById('property-image').appendChild(document.createElement("IMG"));
        }
        //document.getElementById('property-image').getElementsByTagName('img')[0].src = document.getElementById('previewImage').getElementsByTagName('img')[0].src;
      }
      this._dropzone.removeAllFiles();
      this.showDiv(this.extenalID);
    });
    this._dropzone.on("queuecomplete", (file, res) => {
      if (this._dropzone.files[0] && this._dropzone.files[0].status != Dropzone.SUCCESS) {
      } else {

      }
    });
    this._dropzone.on("maxfilesexceeded", (file) => {
      this._dropzone.removeFile(file);
    });

    this._dropzone.on("error", (file, message) => {
    });
    this._dropzone.on("addedfile", (file) => {
      if (this.imagePresent) {
        this.srcFlag = false;
        if (this.fullPath.substr(this.fullPath.length - 7, this.fullPath.length) !== 'default') {
          if (!(this.fullPath.indexOf('assets') > -1)) {
            this.srcFlag = true;
            this.dialogService.addDialog(AlertComponent,
              { title: 'Image Already Exists', message: 'Image upload failed! Please delete existing image before attempting to upload a new one.' }, { backdropColor: 'rgba(220,220,220,0.5)' })
              .subscribe((isConfirmed) => {
              });
            this._dropzone.removeFile(file);
            this._dropzone.on("complete", () => { });
            return false;
          }
        }
      }
    });
    this.resetDropzoneStyle();
  }
  ngOnInit() {
    this.initDropzone();

    // Border style - orange on 'dragover'
    this.previewImg.nativeElement.addEventListener("dragover", (ev: DragEvent) => {
      this.highlightDropzone();
    });

    // Border style - #ccc on 'drop'
    this.previewImg.nativeElement.addEventListener("drop", (ev: DragEvent) => {
      this.resetDropzoneStyle();
    });

    // Border style - #ccc on 'dragleave'
    this.previewImg.nativeElement.addEventListener("dragleave", (ev: DragEvent) => {
      this.resetDropzoneStyle();
    });
  }

  highlightDropzone() {
    this.previewImg.nativeElement.style.border = '1px dashed darkorange';
    this.previewImg.nativeElement.style.color = 'darkorange';
  }

  resetDropzoneStyle() {
    this.previewImg.nativeElement.style.border = '1px solid #ccc';
    this.previewImg.nativeElement.style.color = '#ccc';
  }

}
