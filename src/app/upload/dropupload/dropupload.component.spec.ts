import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router} from '@angular/router';
import { ObjectService } from '../../object/object.service';
import { HttpModule } from '@angular/http';

import { DropuploadComponent } from './dropupload.component';

class DummyRouter {};

describe('DropuploadComponent', () => {
  let component: DropuploadComponent;
  let fixture: ComponentFixture<DropuploadComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropuploadComponent ],
      imports: [ HttpModule ],
      providers: [
        HttpModule,
        { provide: ActivatedRoute, useClass: DummyRouter },
        { provide: Router, useClass: DummyRouter },
        { provide: ObjectService, useClass: DummyRouter },
      ],
      schemas : [CUSTOM_ELEMENTS_SCHEMA]
    })
    .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(DropuploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  xit(' Dropzone should be implemented in testable way ', () => {
    /*
      DropZone should be implemented in another way in order to be tested
    */
    expect(component).toBeTruthy();
  });
});
