﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ConfigurationLoaderService } from 'app/configuration.service'

@Injectable()
export class SavedListService {
  private _url: string;
  headers: Headers;
  options: RequestOptions;

  constructor(private _http: Http, configuration: ConfigurationLoaderService) {
    this.headers = new Headers({
      'Content-Type': 'application/json',
      'Accept': 'q=0.8;application/json;q=0.9'
    });
    this.options = new RequestOptions({ "headers": this.headers });
    this._url = configuration.getSettings().apiUrl
  }

  // GET 'Saved List' for logged-in user.
  public getSavedList(offset: number, limit: number, userName, page, sortColumn, sortOrder): Observable<any> {
    let url = '';
    url = this._url + `/users/` + userName + `/lists?page=` + page + `&sort=` + sortColumn + `,` + sortOrder;
    return this._http.get(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error("Could not retrieve 'Saved List'." + response.status)
        }
        else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Get current saved list.
  getExistingList(userName) {
    return this._http.get(this._url + `/users/` + userName + `/lists?sort=name,asc`)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Get saved list by name.
  getSavedListByName(userName, savedListName, savedListUserName, sortColumn, sortOrder) {
    return this._http.get(this._url + `/users/` + userName + `/lists?name=` + savedListName + `&user-name=` + savedListUserName + `&size=2000&sort=` + sortColumn + `,` + sortOrder)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Create Saved List
  createSavedList(userName, saveListParam: Object) {
    return this._http.post(this._url + '/users/' + userName + '/lists?', saveListParam)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Request to "Create Saved List" failed - ' + response.status)
        }
        else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Delete Saved List
  public deleteSavedList(userName, savedListId) {
    return this._http.delete(this._url + `/users/` + userName + `/lists/${savedListId}`)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Deleting Saved List ID: ' + savedListId + ' failed.' + response.status)
        }
        else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Update Saved List
  public updateSavedList(userName, savedListId, saveListParam: Object) {
    let url = this._url + `/users/` + userName + `/lists/${savedListId}`;
    if (url) {
      return this._http.put(url, saveListParam)
        .map((response: Response) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to "Update Saved List" failed - ' + response.status)
          }
          else {
            return response.json();
          }
        })
        .catch(this._errorHandler);
    }
  }

  public updateSavedListAddProperty(userName, savedListId, saveListParam: any, searchParams?: {}, ignoredIds?: string[]) {
    let url = this._url + `/users/` + userName + `/lists/${savedListId}/properties`;

    if (searchParams) {
      url += '?updateAll=true'
      saveListParam = {searchCriteria: searchParams}
    }

    if (ignoredIds && ignoredIds.length) {
      saveListParam.ignoredIds = ignoredIds
    }

    if (url) {
      return this._http.post(url, saveListParam)
        .map((response: any) => {
          if (response.status < 200 || response.status >= 300) {
            throw new Error('Request to "Update Saved List" failed - ' + response.status)
          } else {
            return response._body ? JSON.parse(response._body) : response
          }
        })
        .catch(this._errorHandler);
    }
  }

  // Get saved list item by ID.
  public getSavedListById(userName, savedListId) {
    return this._http.get(this._url + `/users/` + userName + `/lists/${savedListId}`)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Get Properties by Saved List 'id', 'username'.
  public getPropertiesBySavedListId(userName, savedListId, page, size, lod, sort) {
    let query;
    if (sort) {
      query = sort + `&page=${page}&size=` + size + `&lod=` + lod;
    }
    else {
      query = `page=${page}&size=` + size + `&lod=` + lod;
    }
    return this._http.get(this._url + `/users/` + userName + `/lists/${savedListId}/properties?` + query)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Update Shared Saved List.
  public updateSharedSavedList(userName, savedListId, saveListShareDetails) {
    return this._http.put(this._url + `/users/` + userName + `/lists/${savedListId}/shareusers`, saveListShareDetails)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Delete Shared User.
  public deleteSharedUser(userName, savedListId, deletedUser) {
    this.options['body'] = JSON.stringify(deletedUser);
    return this._http.delete(this._url + `/users/` + userName + `/lists/${savedListId}/shareusers`, this.options)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  //Remove from the list
  public removeFromList(userName, savedListId, propertyId) {
    return this._http.delete(this._url + '/users/' + userName + '/lists/' + savedListId + '/properties/' + propertyId)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  _errorHandler(_error: Response) {
    return Observable.throw(_error || "Server Error - 404.");
  }
}