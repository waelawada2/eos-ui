import { Component, ViewChild, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ModalModule } from 'ngx-modal';
import { CompleterService, CompleterData } from 'ng2-completer';
import { Helper } from '../utils/helper';
import { AppService } from '../app.service';
import { SavedListService } from './saved-list.service';
import { DialogService } from 'ng2-bootstrap-modal';
import { AlertComponent } from '../common/alert-component';
import { KeyboardkeysService } from '../../app/keyboardkeys/keyboardkeys.service';
import { ConfigurationLoaderService } from 'app/configuration.service'

@Component({
  selector: 'app-save-list-share-modal',
  templateUrl: 'saved-list-share-modal.component.html',
  styleUrls: ['saved-list.component.css'],
  providers: [SavedListService, KeyboardkeysService]
})
export class SaveListShareModalComponent implements OnInit {
  @ViewChild('saveListShareModal') saveListShareModal: ModalModule;
  // Get the 'Owner' for a saved listed item.
  @Input() savedListName: string;
  @Input() savedListUser: string;
  @Output() public updateSharedIcon: EventEmitter<any> = new EventEmitter();
  @Output() public openSharedSavedList: EventEmitter<any> = new EventEmitter();
  private _url: string
  public sharedUserList = [];
  public existingSharedUserList = [];
  public newSharedUserList = [];
  public saveListShareForm: FormGroup;
  public saveListShareParam = '';
  public errorMessage: string;
  public userName = window['keycloak'].tokenParsed ? window['keycloak'].tokenParsed.preferred_username : null;
  public fullName = window['keycloak'].tokenParsed ? window['keycloak'].tokenParsed.name : null;
  protected usersListService: CompleterData;
  protected userNameListService: CompleterData;
  public userList = [];
  public message = {}; // e-mail message object
  public selectedSavedList = null;
  public users;
  public action;
  public ownerChanged;
  public newOwner;

  constructor(private builder: FormBuilder,
    private completerService: CompleterService,
    public helper: Helper,
    private _appService: AppService,
    public dialogService: DialogService,
    private _keyboardkeysService: KeyboardkeysService,
    public _savedListService: SavedListService,
    configuration: ConfigurationLoaderService) {
    this._url = configuration.getSettings().apiUrl
    this.sharedUserList = [];
    this.existingSharedUserList = [];
    this.newSharedUserList = [];
  }

  ngOnInit() {
    // Reset 'action'.
    this.action = '';
    // 'Save List' modal form.
    this.saveListShareForm = this.builder.group({
      owner: new FormControl(),
      users: new FormControl(),
      status: new FormControl(),
    });

    // Get application users.
    this._appService.getUsers()
      .subscribe(
        data => {
          this.users = data.content;
        },
        error => {
          this.errorMessage = error;
        }
      );

    this.newSharedUserList = [];
    this.existingSharedUserList = [];
  }

  // Create Saved Search
  createSavedList(saveListShareParam) {
    this._savedListService.createSavedList(this.userName, saveListShareParam)
      .subscribe(
        savedList => {
          this.saveListShareForm.reset();
        },
        error => {
          this.errorMessage = error;
        }
      );
  }

  // Update shared 'Saved List'
  updateSharedSavedList(saveListShareParam) {
    document.getElementsByTagName('html')[0].classList.add('loading');
    let saveListShareDetails = {};
    saveListShareDetails['external-id'] = this.selectedSavedList.id;
    saveListShareDetails['user-name'] = this.saveListShareForm.controls['owner'].value;
    saveListShareDetails['newUsers'] = [];
    // Check if 'owner' has changed
    if (this.newOwner) {
      let deleteSharedUser = { userName: this.newOwner };
      this.deleteUser(deleteSharedUser);
    }
    for (let user in this.newSharedUserList) {
      // Check if shared user == owner
      // If so, prevent PUSH - owner name should not be part of shared users list
      if (this.newSharedUserList[user].userName != this.newOwner) {
        saveListShareDetails['newUsers'].push(this.newSharedUserList[user].userName);
      }
    }
    setTimeout(() => {
      this._savedListService.updateSharedSavedList(this.userName, this.selectedSavedList.id, saveListShareDetails['newUsers'])
        .subscribe(
          sharedSavedList => {
            sharedSavedList['action'] = "EditSavedList";
            this.updateSharedIcon.emit(sharedSavedList);
            if (this.ownerChanged || (this.selectedSavedList.openSaveList || !this.selectedSavedList.openSaveList)) {
              this._savedListService.getSavedListById(this.userName, this.selectedSavedList.id)
                .subscribe(
                  savedList => {
                    let saveListObj = {
                      "name": savedList.name,
                      "notes": savedList.notes,
                      "openSaveList": this.saveListShareForm.controls['status'].value === "Open" ? true : false,
                      "sharedUsersList": savedList.sharedUsersList,
                      "user": this.ownerChanged ? this.newOwner : savedList.user,
                    };
                    this.updateSavedList(saveListObj);
                    if (this.ownerChanged) {
                      this.action = 'ChangeOwner';
                      this.selectedSavedList['action'] = this.action;
                      this.updateSharedIcon.emit(this.selectedSavedList);
                    }
                  },
                  error => {
                    this.errorMessage = error;
                  });
            }
          },
          error => {
            document.getElementsByTagName('html')[0].classList.remove('loading');
            this.errorMessage = error;
            if (JSON.parse(this.errorMessage["_body"]).details.toLowerCase().trim().indexOf("exists") > -1) {
              this.dialogService.addDialog(AlertComponent,
                {
                  title: 'unique Saved List',
                  message: "This name already exists"
                }, { backdropColor: 'rgba(220,220,220,0.5)' })
                .subscribe((isConfirmed) => {
                  this.openSharedSavedList.emit();
                });
            }
          }
        );
    }, 500);
  }

  // Update Saved List
  updateSavedList(saveListObj) {
    this._savedListService.updateSavedList(this.userName, this.selectedSavedList.id, saveListObj)
      .subscribe(
        savedList => {
          document.getElementsByTagName('html')[0].classList.remove('loading');
        },
        error => {
          this.errorMessage = error;
        }
      );
  }

  // Populate share save list modal pop-up.
  public populateSaveListShare(savedList) {
    document.getElementsByTagName('html')[0].classList.add('loading');
    this.sharedUserList = [];
    this.existingSharedUserList = [];
    this.newSharedUserList = [];
    this.selectedSavedList = savedList;
    this.saveListShareForm.controls.owner.setValue(this.selectedSavedList.user);

    this._savedListService.getSavedListById(this.userName, savedList.id)
      .subscribe(result => {
        if (result.openSaveList === true) {
          this.saveListShareForm.controls['status'].setValue("Open");
        } else {
          this.saveListShareForm.controls['status'].setValue("Close");
        }
        document.getElementsByTagName('html')[0].classList.remove('loading');
      }, error => {
        document.getElementsByTagName('html')[0].classList.remove('loading');
      });
    this.userList = [];
    this.saveListShareForm.controls['status'].setValue("Open");


    // Get 'Users List' from 'Users' endpoint.
    this.users.forEach((user, index) => {
      if (this.users[index].userName != this.userName && this.users[index].userName != this.selectedSavedList.user) {
        this.userList.push({ 'name': this.users[index].firstName + " " + this.users[index].lastName, 'email': this.users[index].email, 'userName': this.users[index].userName })
      }
    });

    // Maps 'Users List' to 'usersList' dropdown in 'Save List Share' modal using ng-completer component
    this.usersListService = <any>this.completerService.local(this.userList, 'name', 'name');
    this.userNameListService = <any>this.completerService.local(this.userList, 'userName', 'userName');

    if (this.selectedSavedList.sharedUsersList.length > 0) {
      for (let user in this.selectedSavedList.sharedUsersList) {
        this.sharedUserList.push({
          "userName": this.selectedSavedList.sharedUsersList[user],
        });
        this.existingSharedUserList.push({
          "userName": this.selectedSavedList.sharedUsersList[user],
        });
      }
    }
  }

  // Reset 'Save List' Form
  resetSaveListShareForm() {
    this.saveListShareForm.controls['users'].reset();
    this.sharedUserList = [];
  }

  // Create 'shared users' list
  public createSharedUserList(userObj) {
    if (userObj) {
      for (let user in this.selectedSavedList) {
        if (!this.selectedSavedList.sharedUsersList.includes(userObj.originalObject.userName)) {
          if (!this.sharedUserList.some(user => user.name == userObj.originalObject.name)) {
            this.sharedUserList.push({
              "name": userObj.originalObject.name,
              "email": userObj.originalObject.email,
              "userName": userObj.originalObject.userName,
              "new": true,
            });
            this.newSharedUserList.push({
              "userName": userObj.originalObject.userName,
            });
          }
        }
      }
    }
  }

  public deleteUser(user) {
    let deletedUser;
    this._savedListService.deleteSharedUser(this.userName, this.selectedSavedList.id, [user.userName])
      .subscribe(
        sharedSavedList => {
          this.selectedSavedList['action'] = this.action;
          this.updateSharedIcon.emit(this.selectedSavedList);
          deletedUser = this.sharedUserList.findIndex(name => name == user);
          this.sharedUserList.splice(deletedUser, 1);
          deletedUser = this.newSharedUserList.findIndex(name => name == user);
          this.newSharedUserList.splice(deletedUser, 1);
          deletedUser = this.existingSharedUserList.findIndex(name => name == user);
          this.existingSharedUserList.splice(deletedUser, 1);
        },
        error => {
          this.errorMessage = error;
        }
      );
  }

  // Remove new user from 'newSharedUserList[]'
  public removeUser(user) {
    let deletedUser;
    deletedUser = this.sharedUserList.findIndex(name => name == user);
    this.sharedUserList.splice(deletedUser, 1);
    deletedUser = this.newSharedUserList.findIndex(name => name == user);
    this.newSharedUserList.splice(deletedUser, 1);
  }


  // Validate user entered value for select lists.
  // System should not accept values other than the values in the drop-down list.
  validateSelectedOwner(selectedOwner) {
    if (selectedOwner) {
      selectedOwner = selectedOwner.toLowerCase();
      let selectListObject;
      let index = this.userList.findIndex(list => list.userName.toLowerCase() === selectedOwner);
      if (!(index > -1)) {
        this.saveListShareForm.controls['owner'].setValue(this.selectedSavedList.user);
      }
      if (selectedOwner != this.selectedSavedList.user) {
        this.ownerChanged = true;
        this.newOwner = selectedOwner;
      }
      else {
        this.ownerChanged = false;
        this.newOwner = null;
      }
    }
  }

  validateSelectedUser(selectedUser) {
    if (selectedUser) {
      selectedUser = selectedUser.toLowerCase();
      let selectListObject;
      let index = this.userList.findIndex(list => list.name.toLowerCase() === selectedUser);
      if (!(index > -1)) {
        this.saveListShareForm.controls['users'].setValue('');
      }
    }
  }
  // Keyboard shortcuts
  keyboardShortCutFunc(e) {
    switch (this._keyboardkeysService.getKeyboardResponse(e)) {
      case 'addRecord':
        var button = document.getElementById('shareSavedList');
        if (button)
          button.click();
        break;
    }
  }
}