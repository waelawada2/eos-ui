import { Component, ViewChild, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ModalModule } from 'ngx-modal';
import { CompleterService, CompleterData } from 'ng2-completer';
import { DialogService } from 'ng2-bootstrap-modal';
import { ConfirmComponent } from '../common/confirm-component';
import { TosatService } from '../common/toasty-service';
import { ObjectService } from '../object/object.service';
import { SavedListService } from './saved-list.service';
import { AlertComponent } from '../common/alert-component';
import { SavedListBackgroundService } from '../SavedlListBackground/savedListBackground.service';
import { KeyboardkeysService } from '../../app/keyboardkeys/keyboardkeys.service';



@Component({
  selector: 'app-save-list-modal',
  templateUrl: 'saved-list-modal.component.html',
  styleUrls: ['saved-list.component.css'],
    providers: [ SavedListService, ObjectService, KeyboardkeysService ]
})
export class SaveListModalComponent implements OnInit {
  @ViewChild('saveListModal') saveListModal: ModalModule;
  // Get name, notes, savedlistID from 'SaveListComponent' for individual saved list items listed on the homepage.
  @Input() savedListName: string;
  @Input() savedListNotes: string;
  @Input() savedListID: number;
  @Input() savedListUser: string;
  @Output() public updatedSavedList: EventEmitter<any>  = new EventEmitter();
  @Output() progressnumber: EventEmitter<any> = new EventEmitter();
  @Output() removeDialog: EventEmitter<any> = new EventEmitter();
  @Output() openSavedListpopup: EventEmitter<any> = new EventEmitter();
  public saveListForm: FormGroup;
  public saveListParam = '';
  public errorMessage: string;
  public userName = window['keycloak'].tokenParsed ? window['keycloak'].tokenParsed.preferred_username : null;
  protected existingListService: CompleterData;
  public currentSavedList = [];
  public radioAddToList; // Tracks 'checked' status of radio buttons on 'Save List' modal.
  public saveListMode;
  // Values for 'saveListAddType' - 'single', 'all', 'selected'.
  public saveListAddType;
  public currentListId;
  public errorMsg;
  // 'Add All Objects'
  public saveListRequest;
  public selectedPropertyIds = [];
  public updatedSavedListID;
  public responseTotalElements;
  public savedListIndex = 0;
  public pageSize = 20;
  public valuesadded = 0;
  public percentage = 0;
  public progressObject = {};
  public selectednew = false;
  public confirmResult = null;
  public listName;
  public unChecked = 0; // Items unchecked on thumbnail view
  public duplicateCount = 0;
  private arrayAttributesToSearch = ['archiveNumbers', 'artistIds', 'numbers',
    'artistCatalogueRaisoneeIds', 'currentOwnerIds', ]

    constructor(
        private builder: FormBuilder,
        private completerService: CompleterService,
        private dialogService: DialogService,
        private tosatservice: TosatService,
        private _objectService: ObjectService,
        public _savedListService: SavedListService,
        private _keyboardkeysService: KeyboardkeysService,
        private _savedListBackgroundService: SavedListBackgroundService) {
    // 'Save List' modal form.
    this.saveListForm = this.builder.group({
      radioAddToList: new FormControl(),
      selectExistingList: new FormControl(),
      name: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(80)]),
      notes: new FormControl('', Validators.maxLength(200)),
    });
  }

  ngOnInit() {
    this.saveListForm.reset();
    this.savedListIndex = 0;
    this.valuesadded = 0;
    sessionStorage.setItem('typeSelected', 'false');
  }

  // Get existing 'Saved Lists' by user.
  public getExistingSavedLists() {
    this._savedListService.getExistingList(this.userName)
      .subscribe(
        data => {
          this.currentSavedList = [];
          let savedList = data.content;
          savedList.forEach((savedList, index) => {this.currentSavedList.push({'name': savedList.name, 'id': savedList.id,})});
          // Maps existing 'Saved List' to 'selectExistingList' dropdown on 'Save List' modal using ng-completer component
          this.existingListService = <any>this.completerService.local(this.currentSavedList, 'name', 'name');
        },
        error => {
            this.errorMessage = error;
        }
      );
  }

  // Get 'Saved List' by name.
  public getSavedListByName(event) {
    const savedListName = event.target.value.toLowerCase();
    if (savedListName !== '') {
      this._savedListService.getSavedListByName(this.userName, savedListName, this.userName, 'name', 'asc')
      .subscribe(
        data => {
          this.currentSavedList = [];
          let savedList = data.content;
          savedList.forEach((savedList, index) => {this.currentSavedList.push({'name': savedList.name, 'id': savedList.id,})});
          // Maps existing 'Saved List' to 'selectExistingList' dropdown on 'Save List' modal using ng-completer component
          this.existingListService = <any>this.completerService.local(this.currentSavedList, 'name', 'name');
        },
        error => {
          this.errorMessage = error;
        }
      );
    }
  }


  // Create Saved List
  createSavedList(saveListParam, callback) {
    this._savedListService.createSavedList(this.userName, saveListParam)
    .subscribe(
      savedList => {
        this.listName = savedList.name;
        this.currentListId = savedList.id;
        this.saveListForm.reset();
        if ((this.saveListAddType == 'single' || this.saveListAddType == 'selected') && !this.selectednew) {
          this._savedListBackgroundService.removeDialog();
        }
        callback();
      },
      error => {
        const errorBody = error._body ? JSON.parse(error._body) : {}
        this.errorMessage = error;
        if (errorBody.details && JSON.parse(this.errorMessage['_body']).details.toLowerCase().trim().indexOf('exists') > -1) {
          this.dialogService.addDialog(AlertComponent,
            {
              title: 'Unique Saved List',
              message: 'This name already exists'}, { backdropColor: 'rgba(220,220,220,0.5)' })
          .subscribe((isConfirmed) => {
                this.selectedPropertyIds = [];
                this.openSavedListpopup.emit(this.saveListAddType);
          });
        }
        document.getElementsByTagName('html')[0].classList.remove('loading');
        this._savedListBackgroundService.removeDialog();
      }
    );
  }

  // Update Saved List
  updateSavedList(saveListObj) {
    let saveListId;
    if (window.location.pathname.indexOf('object') >= 0 || window.location.pathname.indexOf('thumbnail') >= 0 ) {
      saveListId = this.currentListId;
    } else {
      saveListId = this.savedListID;
      saveListObj.propertyIds = [];
    }
    this._savedListService.updateSavedList(this.userName, saveListId, saveListObj)
    .subscribe(
      savedList => {
        this.updatedSavedListID = savedList.id;
        savedList['action'] = 'EditSavedList';
        this.updatedSavedList.emit(savedList);
        document.getElementsByTagName('html')[0].classList.remove('loading');
      },
      error => {
        this.errorMessage = error;
        document.getElementsByTagName('html')[0].classList.remove('loading');
      }
    );
  }


  // Get ALL property IDs - to add to saved list.
  public getAllPropertyIds(saveListObj) {
    // Create/Update saved list from an existing saved list
    if (sessionStorage.getItem('navigatedtosavedlistid') && (sessionStorage.getItem('savelistview') == 'true')) {
      const query = false;
      this.addAllObjectsToList(query, saveListObj);
    } else {
      const searchParam = sessionStorage.getItem('MainViewQuery');
      const query = searchParam ? searchParam + '&page=0&size=1&lod=MIN' : '&page=0&size=1&lod=MIN';
      this.addAllObjectsToList(query, saveListObj);
    }
  }


  // -- ADD ALL OBJECTS TO LIST --
  // Get Properties, Adds all objects to saved list.
  public addAllObjectsToList(query, saveListObj) {
    const searchParam = sessionStorage.getItem('MainViewQuery') ? sessionStorage.getItem('MainViewQuery') : '';
    let propertiesToBeRemoved;
    let listSize;
    // Check if objects were unchecked after '--Select All--' option was selected
    if (window.location.pathname.indexOf('thumbnail') > 0 && this.saveListAddType == 'selected') {
      if ((sessionStorage.getItem('objectsUnchecked')) && (sessionStorage.getItem('objectsUnchecked').split(',').length >=1)) {
        propertiesToBeRemoved = sessionStorage.getItem('objectsUnchecked').split(',');
        propertiesToBeRemoved.splice(propertiesToBeRemoved.length - 1, 1);
      }
    }
    listSize = sessionStorage.getItem('totalelements') ? sessionStorage.getItem('totalelements') : 0;
    if (propertiesToBeRemoved && propertiesToBeRemoved.length > 0) {
      listSize = listSize > 5000 && propertiesToBeRemoved.length < listSize ? listSize - propertiesToBeRemoved.length : listSize;
    }
    if (listSize > 5000) {
      document.getElementsByTagName('html')[0].classList.remove('loading');
      this.dialogService.addDialog(ConfirmComponent, {
        title: 'Large Sized List',
        message: 'This request involves a large number of records and can significantly slow down the system.' + 
        ' Are you sure you would like to continue?'
      }).subscribe((isConfirmed) => {
        this.confirmResult = isConfirmed;
        if (this.confirmResult) {
          this.addToList(query, searchParam, saveListObj);
        }
      });
    } else {
      this.addToList(query, searchParam, saveListObj);
    }
  }

  addToList(query, searchParam, saveListObj) {
    // Create 'new' list.
    if (this.radioAddToList == 'CreateNewList') {
      this.createSavedList(saveListObj, () => {
          document.getElementsByTagName('html')[0].classList.add('loading');
        if (this.currentListId) {
          if (query) {
            searchParam = searchParam == '' ? '&lod=MIN' : searchParam;
          }

          document.getElementsByTagName('html')[0].classList.remove('loading');
          this.updateSavedListAddProperty(0, searchParam);
        } else {
          document.getElementsByTagName('html')[0].classList.remove('loading');
        }
      });
    } else { // Add to 'existing' list.
      // Add objects from one saved list to another saved list.
      if (!query) {
        this.updateSavedListAddProperty(0, false);
      } else {
        // Add objects to an existing list through object search.
        searchParam = searchParam == '' ? '&lod=MIN' : searchParam;
        this.updateSavedListAddProperty(0, searchParam);
      }
    }
  }

  // Update Saved List - add one property at a time
  updateSavedListAddProperty(page, searchParam) {
    // If properties were added through object search..
    if (searchParam) {
      let searchStr = '';
      const query = searchStr + '&page=' + page + '&size=' + this.pageSize + '&lod=MIN';

      if (searchParam.indexOf('lod=MIN') === -1) {
        searchStr = searchParam;
      }

      this._objectService
      .getProperties(query)
        .subscribe(Response => {
          let removeIds: any = sessionStorage.getItem('objectsUnchecked')
          const searchQuery = decodeURIComponent(searchParam).split('&')
              .filter(pair => pair.length)
              .reduce((pairObj: any, pair: string) => {
                const pairArray = pair.split('=')

                if (this.arrayAttributesToSearch.indexOf(pairArray[0]) > -1) {
                  pairObj[pairArray[0]] = pairObj[pairArray[0]] || []
                  pairObj[pairArray[0]].push(pairArray[1])
                } else {
                  pairObj[pairArray[0]] = pairArray[1]
                }
                return pairObj}, {})

          delete searchQuery.sort

          if (searchQuery)
          this.responseTotalElements = Response.totalElements;

          if (removeIds) {
            removeIds = removeIds.split(',').filter((id) => id.length)
          }

          this._savedListService.updateSavedListAddProperty(this.userName, this.currentListId, [], searchQuery, removeIds)
            .subscribe(
              this.successSaveListUpdate.bind(this),
              this.errorHandler.bind(this),
              () => this.resetView())
          sessionStorage.setItem('navigatedtosavedlistid', this.currentListId)
      }, resFileError => {
        this.updateSavedListAddProperty(page, searchParam);
        this.errorMsg = resFileError;
      });
    } else {
       // If properties were added from an existing saved list..
      const query = searchParam + '&page=' + page + '&size=' + this.pageSize + '&lod=MIN';
      this._savedListService.getPropertiesBySavedListId(this.userName, sessionStorage.getItem('navigatedtosavedlistid'), page, this.pageSize, 'min', null)
      .subscribe(
        Response => {
          this.responseTotalElements = Response.totalElements;
          this.selectedPropertyIds = Response.content
            .map((property => property.id))
          this.addSelectedObjectsToExistingSavedList({})
      }, resFileError => {
        this.updateSavedListAddProperty(page, searchParam);
        this.errorMsg = resFileError;
      });
    }
  }

  // Save List
  public saveList() {
    const isObjectView = window.location.pathname.indexOf('object') >= 0,
      isThumbnailView = window.location.pathname.indexOf('thumbnail') >= 0

    // -- duplicate Counter, values added and selected new set 0 and false
    this.duplicateCount = 0;
    this.valuesadded = 0;
    this.selectednew = false;

    // -- if typeselected set saveListAddType = selected
    if (sessionStorage.getItem('typeSelected') === 'true') {
      this.saveListAddType = 'selected';
      sessionStorage.setItem('typeSelected', 'false');
    }

    // Add to Existing List. -- if view is not object and is not thumbnail and radio add To List is create New or add to Existing
    if ((!isObjectView && !isThumbnailView && this.radioAddToList !== 'CreateNewList') || this.radioAddToList === 'AddToExistingList') {
      const propertyIds = [];
      // 'Add to existing list' on Object/Thumbnails page.
      if (this.radioAddToList === 'AddToExistingList') { // -- Add loading class and load the save list
        document.getElementsByTagName('html')[0].classList.add('loading');
        this._savedListService.getSavedListById(this.userName, this.currentListId)
        .subscribe(
          result => {
            if (result.openSaveList || (this.userName === result.user)) {  // -- if openSaved or user is current
              if (this.saveListForm.controls['selectExistingList'].value) { // -- if checkbox for existing list is true
                  let propertyId;
                  if (this.saveListAddType === 'single') { // if saveListAddType is single
                    if (isObjectView) { // if view is object then fill property id with the one on the url
                        propertyId = (window.location.search.split('id=')[1]);
                    } else if (isThumbnailView) { // if view is thumbnail and there is addToSaveList
                      if (sessionStorage.getItem('addtosavelistobjects') && sessionStorage.getItem('addtosavelistobjects').length > 0) {
                        propertyId = sessionStorage.getItem('addtosavelistobjects').split(','); // then add then to propertyId as an array
                        if (propertyId.length > 1) { // if property Id length replace comas with empty string ????
                            propertyId = propertyId.replace(',', '');
                        }
                      } else { // if there is not session storage remove loading class
                        document.getElementsByTagName('html')[0].classList.remove('loading');
                      }
                    }
                  }

                  if (this.currentListId) { // if current list id
                      let saveListObj = {};

                    if (this.saveListAddType === 'all' || this.saveListAddType === 'selected') {
                        // if saveListAddType is all or selected
                        // Add 'selected' objects to an existing saved list.
                        if (isThumbnailView && this.saveListAddType === 'selected') { // -- if view is thumbnail and saveListAddType is selected
                            this.selectedPropertyIds = [];
                            // -- if sessionStorage has items to be saved in the list fill select propertyIds transformed in array
                            if (sessionStorage.getItem('addtosavelistobjects') && sessionStorage.getItem('addtosavelistobjects').length > 0) {
                                this.selectedPropertyIds = sessionStorage.getItem('addtosavelistobjects').split(',');
                                this.selectedPropertyIds = this.selectedPropertyIds.filter(data => { // -- filter data from empty
                                  if (data.length) {
                                      return data;
                                  }
                                })
                            } else { // -- if there is not any object to save in the list in the session storage then remove
                                document.getElementsByTagName('html')[0].classList.remove('loading');
                            }
                            // Add 'selected' objects to an existing saved list.
                            // -- If there are properties id and session storage thumbnailsselect all is not true
                            if (this.selectedPropertyIds.length > 0 && !(sessionStorage.getItem('thumbnailselectall') === 'true')) {
                              this.valuesadded = 0; // -- reset as 0
                              if (this.selectedPropertyIds.length > 5000) {
                                // -- if properties array higher than 5000 then show warning dialog
                                this.dialogService.addDialog(ConfirmComponent, {
                                  title: 'Large Sized List',
                                  message: 'This request involves a large number of records and can significantly slow down the system.' +
                                    'Are you sure you would like to continue?',
                                }).subscribe((isConfirmed) => {
                                    this.confirmResult = isConfirmed;

                                    if (this.confirmResult) {
                                      // if confirmed then start iterating over each object id
                                      this.addSelectedObjectsToExistingSavedList(saveListObj)
                                    }
                                });
                              } else { // -- if properties are less than 5000
                                // -- then do the same above from line 622; iterate over each object
                                this.addSelectedObjectsToExistingSavedList(saveListObj)
                              }
                            } else if (sessionStorage.getItem('thumbnailselectall') === 'true') {
                              // If '--SELECT ALL-- -- if session storage has thumbnailsselectall true then get all properties
                                this.getAllPropertyIds(saveListObj);
                            }
                        } else if (this.saveListAddType === 'all' || sessionStorage.getItem('thumbnailselectall') === 'true') {
                          // -- if not ask if the selectalltype is all or the session thumbnailsselectall is true
                          this.getAllPropertyIds(saveListObj);
                        }
                    } else {  // When only 1 object is added to an existing saved list.
                      // -- if saveListAddType is not all or selected
                      saveListObj = {};
                      this.selectedPropertyIds = [propertyId]
                      this. percentage = 100;
                      this.progressObject['percentage'] = this.percentage;
                      this.progressObject['valuesadded'] = this.valuesadded;
                      this.progressObject['totalRecords'] = 1;
                      this.progressObject['name'] = this.listName;
                      this._savedListBackgroundService.add(this.progressObject);
                      this.addSelectedObjectsToExistingSavedList(saveListObj, 0);
                    }
                  } else { // if there is not current list id remove loading
                    document.getElementsByTagName('html')[0].classList.remove('loading');
                  }
                } else { // -- if selectExistingList is not true then remove loading class
                  document.getElementsByTagName('html')[0].classList.remove('loading');
                }
              } else { // -- if the flag openSaveList is not true and user is not current
                  // -- then add positionFixed class and remove loading class
                  document.getElementsByTagName('body')[0].classList.add('positionFixed'); // show dialog
                  document.getElementsByTagName('html')[0].classList.remove('loading');
                  this.dialogService.addDialog(AlertComponent, // show dialog
                      {title: 'Warning...!', message: `This list's status is Closed.
                        Please reach out to "${result.user}" to amend list.`},
                      { backdropColor: 'rgba(220,220,220,0.5)' })
                      .subscribe((isConfirmed) => { // remove positionFixed class
                      document.getElementsByTagName('body')[0].classList.remove('positionFixed');
                  });
              }
            },  error => { // -- on error remove loading class
                document.getElementsByTagName('html')[0].classList.remove('loading');
            });
      } else { // 'Update List' on Homepage. -- if radioAddToList value is not addToExistingList
        if (this.savedListID) { // if savelist id then load it
          this._savedListService.getSavedListById(this.userName, this.savedListID)
            .subscribe(
              data => { // update object and send it to the endpoint
                const saveListObj = {
                  'name': this.saveListForm.value.name,
                  'notes': this.saveListForm.value.notes,
                  'openSaveList': true,
                  'sharedUsersList': data.sharedUsersList,
                  'user': this.userName,
                }

                this.updateSavedList(saveListObj);
              },
              error => {
                this.errorMessage = error;
              }
            );
        } else { // -- if there is not savelist id then remove loading class
          document.getElementsByTagName('html')[0].classList.remove('loading');
        }
      }
    } else {
      // -- if view is object or thumbnail or radioaddlist is different from createNewList or equals to AddToExistingList
      // 'Save List' - Objects/Thumbnails page.
      // When 'Create New List' option is selected on 'save list' modal.
      if (this.saveListForm.valid) { // -- if savelist form is valid save the form data in savelistparam
        const saveListParam = this.saveListForm.value;
        let propertyId;
        if (this.saveListAddType === 'single') { // -- if savelistaddtype is single
          if (isObjectView) { // -- if the view is object save the id from url
            propertyId = (window.location.search.split('id=')[1]);
          } else if (isThumbnailView) { // if the view is thumbnail
            propertyId = window.location.search.split('propertyIds=')[1]; // take the string and split by comas in an array
            if (propertyId.length > 1) {
              propertyId = propertyId.replace(',', '');
            }
          }
        }

        let saveListObj: any = {
          'name': saveListParam.name,
          'notes': saveListParam.notes,
          'openSaveList': true,
          'user': this.userName,
        }

        if (this.saveListAddType === 'all' || this.saveListAddType === 'selected') {
          // -- if savelist form is valid and savelistAddType is all or selected set empty array
          saveListObj['propertyIds'] = [];

          if ((this.saveListAddType === 'all' && (isObjectView || isThumbnailView)) ||
              (isThumbnailView && sessionStorage.getItem('thumbnailselectall') === 'true')) {
            // Create new list - Add all objects to saved list.
            // -- if (savelistAddTpe is all and view is object or thumbnail)
            // -- or if (view is thumbnail and thumbnailSelectAll is true from session storage)
            this.getAllPropertyIds(saveListObj); // -- fill the array with properties
          } else {
            this.selectedPropertyIds = []; // -- if not this selected properties id array set empty
            if (sessionStorage.getItem('addtosavelistobjects') && sessionStorage.getItem('addtosavelistobjects').length > 0) {
              // -- if addToSavelist exists and is not empty
              this.selectedPropertyIds = sessionStorage.getItem('addtosavelistobjects').split(',');
              this.selectedPropertyIds = this.selectedPropertyIds.filter(data => { // -- ignore empty strings
                if (data.length) {
                  return data;
                }
              })
              if (this.selectedPropertyIds.length > 5000) { // -- if selected properties id > 5000 show confirmation
                this.dialogService.addDialog(ConfirmComponent, {
                  title: 'Large Sized List',
                  message: 'This request involves a large number of records and can significantly slow down the system.' +
                    ' Are you sure you would like to continue?',
                }).subscribe((isConfirmed) => {
                  this.confirmResult = isConfirmed;
                  if (this.confirmResult) {
                    this.createSavedListWithSelectedObjs(saveListObj);
                  }
                });
              } else if (this.selectedPropertyIds.length > 0) { // -- if not > 5000 but > 0 then create the saveList
                this.createSavedListWithSelectedObjs(saveListObj);
              }
            }
          }
        } else { // -- if saveListAddType is not all or selected then add class loading, crate progress object and create save list
          document.getElementsByTagName('html')[0].classList.add('loading');
          this.valuesadded = 1;
          this. percentage = 100;
          this.progressObject['percentage'] = this.percentage;
          this.progressObject['valuesadded'] = this.valuesadded;
          this.progressObject['totalRecords'] = 1;
          this.progressObject['name'] = this.listName;
                this._savedListBackgroundService.add(this.progressObject);
          this.createSavedList(saveListObj, () => {
            document.getElementsByTagName('html')[0].classList.remove('loading');
            if (this.currentListId) {
              saveListObj = {}; // -- empty saveListObj and fill with single option property id
              this.selectedPropertyIds = [propertyId];
              this.addSelectedObjectsToExistingSavedList(saveListObj, 0);
            }
          });
        }
      } else {
        document.getElementsByTagName('html')[0].classList.remove('loading');
      }
    }
  }

  // Add 'selected objects to existing saved list.
  addSelectedObjectsToExistingSavedList(saveListObj, index?) {
  const saveListId = this.currentListId; // -- send current list Id to add property with the current user, savelist id and array of ids
  saveListObj.propertyIds = this.selectedPropertyIds.filter((id) => id.length)
  this._savedListService.updateSavedListAddProperty(this.userName, saveListId, saveListObj.propertyIds)
    .subscribe(
      this.successSaveListUpdate.bind(this),
      this.errorHandler.bind(this),
      () => this.resetView());
    }

  // Reset 'Save List' Form
  resetSaveListForm(saveListParam) {
    if (window.location.pathname.indexOf('object') === -1 && window.location.pathname.indexOf('thumbnail') === -1) {
      this.saveListForm.controls['name'].setValue(saveListParam.name);
      this.saveListForm.controls['notes'].setValue(saveListParam.notes);
    } else {
      this.saveListForm.reset();
    }
  }

  // Create 'new' saved list with selected objects
  createSavedListWithSelectedObjs(saveListObj) {
    if (this.selectedPropertyIds[0] !== '') { // -- if first property is not empty
        // then flag selectednew, prepare percetange and progress object
      this.selectednew = true;
      this.percentage = this.valuesadded / this.selectedPropertyIds.length;
      this.percentage = Math.floor(this.percentage * 100);
      this.progressObject['percentage'] = this.percentage;
      this.progressObject['valuesadded'] = this.valuesadded;
      this.progressObject['totalRecords'] = this.selectedPropertyIds.length;
      this.progressObject['name'] = this.listName;
          this._savedListBackgroundService.add(this.progressObject); // add to behavior subject collection
      this.createSavedList(saveListObj, () => { // -- send savelist obj with callback handler
        this.addSelectedObjectsToExistingSavedList(saveListObj)
      });
    }
  }


  // Progress Bar - Add All
  showProgressAddAll(propertiesToBeRemoved) {
  if (propertiesToBeRemoved) {
    // percentage = valuesdded / (total - toRemove)
    this.percentage = this.valuesadded / (this.responseTotalElements - propertiesToBeRemoved.length);
    // progressObject.totalRecords = (total - toRemove)
    this.progressObject['totalRecords'] = this.responseTotalElements - propertiesToBeRemoved.length;
  } else {
    // percetage = valuesAdded / total
    this.percentage = this.valuesadded / this.responseTotalElements;
    // progressObject.totalRecords = total
    this.progressObject['totalRecords'] = this.responseTotalElements;
  }

  // percentage = largest integer less of percentage * 100
  this.percentage = Math.floor(this.percentage * 100);
  // prepare progress object with percentage, values added and list name
  this.progressObject['percentage'] = this.percentage;
  this.progressObject['valuesadded'] = this.valuesadded;
  this.progressObject['name'] = this.listName;
  this._savedListBackgroundService.add(this.progressObject);

  }

  // Progress Bar - Add Selected
  showProgressAddSelected() {
    if (this.selectedPropertyIds.length > 0) {
    this.percentage = this.valuesadded / this.selectedPropertyIds.length;
    this.percentage = Math.floor(this.percentage * 100);
    this.progressObject['percentage'] = this.percentage;
    this.progressObject['valuesadded'] = this.valuesadded;
    this.progressObject['totalRecords'] = this.selectedPropertyIds.length;
    this.progressObject['name'] = this.listName;
      this._savedListBackgroundService.add(this.progressObject);
    }
  }

  radioButtonClicked(type) {
    if (type === 'new') {
      this.saveListForm.controls['selectExistingList'].setValue('');
      this.currentListId = null;
      this.listName = null;
    } else {
      this.saveListForm.controls['selectExistingList'].setValue('');
    }
  }

  displayDuplicateStatus(totalObjs) {
    this._savedListBackgroundService.removeDialog(); // remove dialog
    const msg = this.valuesadded + '/' + totalObjs + ' object(s) added to the saved list. ' +
      this.duplicateCount + ' object(s) are duplicates that already exist in the list.';
    this.dialogService.addDialog(AlertComponent,
      {title: 'Status', message: msg}, {backdropColor: 'rgba(220,220,220,0.5)'})
      .subscribe((isConfirmed) => {});
  }

  // Keyboard shortcuts
  keyboardShortCutFun(e) {
    switch (this._keyboardkeysService.getKeyboardResponse(e)) {
      case 'addRecord':
        const button = document.getElementById('updateSavedList');

        if (button) {
          button.click();
        }
        break;
    }
  }

  resetView() {
    const items: any = document.getElementsByName('checkThumb');

    this._savedListBackgroundService.removeDialog();
    this.saveListForm.reset();
    sessionStorage.removeItem('thumbnailselectall');
    sessionStorage.removeItem('objectsUnchecked');
    sessionStorage.removeItem('addtosavelistobjects');

    document.getElementsByTagName('html')[0].classList.remove('loading');

    for (let i = 0; i < items.length; i++) { // -- loop into all the objects checked
      if (items[i].type === 'checkbox') {// -- reset all the checbox to false by default
        items[i].checked = false;
      }
    }
  }

  successSaveListUpdate(response: any = {}) {
    const message = `${response.totalInserted}/${response.totalFound} object(s) have been added to your saved list,
        <a href="object?savelist=${this.currentListId}">'${this.listName}'</a>.`

    this.tosatservice.addToast(message, 'top-right');
  }

  errorHandler(error) {
    const message = error._body ? JSON.parse(error._body).details : error.message || error.error || error

    this.tosatservice.addToastError(message, 'top right')
  }
}
