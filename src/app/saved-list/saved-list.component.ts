import { Component, ViewChild, ElementRef, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response } from '@angular/http';
import { Helper } from '../utils/helper';
import { DatatableComponent } from '@swimlane/ngx-datatable/src/components/datatable.component';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { TosatService } from '../common/toasty-service';
import { DialogService } from "ng2-bootstrap-modal";
import { ConfirmComponent } from '../common/confirm-component';
import { AppService } from '../app.service';
import { SaveListModalComponent } from './saved-list-modal.component';
import { SavedListService } from './saved-list.service';
import { SaveListShareModalComponent } from './saved-list-share-modal.component';
import { ModalModule } from 'ngx-modal';
import { ObjectService } from '../object/object.service';
import { SavedListBackgroundService } from '../SavedlListBackground/savedListBackground.service';
import { Subscription } from 'rxjs/Subscription';
import { ConfigurationLoaderService } from 'app/configuration.service'


class PagedData<T> {
  data: T[];
}

@Component({
  selector: 'app-saved-list',
  templateUrl: 'saved-list.component.html',
  styleUrls: ['saved-list.component.css'],
  providers: [Helper, AppService, SaveListModalComponent, SavedListService, SaveListShareModalComponent, ObjectService, DialogService]
})
export class SavedListComponent implements OnInit {
  readonly headerHeight = 30;
  readonly rowHeight = 40;
  readonly pageLimit = 10;
  public userName = window['keycloak'].tokenParsed && window['keycloak'].tokenParsed.preferred_username ? window['keycloak'].tokenParsed.preferred_username : null;
  private _url: string;
  public errorMessage: string;
  public rows = []; // Array of saved lists per User.
  public temp = []; // Filter array that keeps track of results matching the search text.
  public isLoading: boolean;
  public deleteFlag = false; // Set to 'true' when a saved list item is deleted.
  public filterFlag = false;
  public savedListName = '';
  public savedListNotes = '';
  public savedListID;
  public savedListIDs = [];
  public rows_copy = []; // Maintains an unedited reference to the original rows (saved list items)
  public page = 0;
  public totalPages = 0;
  public savedListUser;
  public confirmResult: boolean = null;
  public sortOrder = 'asc';
  public sortColumn = 'name';
  public filterSavedListName;
  public checkSavedListName: string = null;
  public showProgressonsavedList: boolean = false;
  public progressBarNo = 0;
  public progressBarPercentage = 0;
  public totalRecords = 0;
  public savedListProgressCompleted: boolean = false;
  public scrollIcon: boolean = false;

  subscription: Subscription;
  removeProgress: Subscription;

  @ViewChild(DatatableComponent) savedList: DatatableComponent;
  // Access to 'Save List' Modal
  @ViewChild(SaveListModalComponent) public saveListModal: SaveListModalComponent;
  // Access to 'Share Save List' Modal
  @ViewChild(SaveListShareModalComponent) public saveListShareModal: SaveListShareModalComponent;
  constructor(private router: Router,
    private _http: Http,
    public helper: Helper,
    private tosatservice: TosatService,
    private _dialogService: DialogService,
    private _savedListModalComponent: SaveListModalComponent,
    private _saveListShareModalComponent: SaveListShareModalComponent,
    private el: ElementRef,
    public _savedListService: SavedListService,
    private _savedListBackgroundService: SavedListBackgroundService,
    configuration: ConfigurationLoaderService,
  ) {
    this._url = configuration.getSettings().apiUrl
    setTimeout(() => {
      this.userName = window['keycloak'].tokenParsed && window['keycloak'].tokenParsed.preferred_username ? window['keycloak'].tokenParsed.preferred_username : null;
    }, 500);
    this.subscription = _savedListBackgroundService.itemAdded$.subscribe(
      event => {
        this.checkSavedListName = event.name;
        this.savedListProgressCompleted = false;
        this.showProgressonsavedList = true;
        this.progressBarNo = event.valuesadded;
        this.progressBarPercentage = event.percentage;
        this.totalRecords = event.totalRecords;
      });

    this.removeProgress = _savedListBackgroundService.removeProgress$.subscribe(
      event => {
        if (event) {
          this.savedListProgressCompleted = true;
          this.showProgressonsavedList = false;
        }
      });

  }

  ngOnInit() {
    this.rows = [];
    this.onScroll(0);
  }

  // Scroll
  onScroll(offsetY: number) {
    // total height of all rows in the viewport
    const viewHeight = this.el.nativeElement.querySelector('ngx-datatable').getBoundingClientRect().height - this.headerHeight;

    // check if we scrolled to the end of the viewport
    if (!this.isLoading && offsetY + viewHeight >= this.rows.length * this.rowHeight) {
      // total number of results to load
      let limit = this.pageLimit;

      // check if we haven't fetched any results yet
      if (this.rows.length === 0) {
        // calculate the number of rows that fit within viewport
        const pageSize = Math.ceil(viewHeight / this.rowHeight);
        // change the limit to pageSize such that we fill the first page entirely
        // (otherwise, we won't be able to scroll past it)
        limit = Math.max(pageSize, this.pageLimit);
      }
      if (this.page == 0 || (this.page < this.totalPages)) {
        if (this.filterFlag != true) {
          this.loadPage(limit);
        }
      }
    }
  }

  // Sort
  onSort(event) {
    let limit = this.pageLimit;
    this.isLoading = true;
    this.page = 0;
    if (this.sortOrder == 'asc') {
      this.sortOrder = 'desc';
    }
    else {
      this.sortOrder = 'asc';
    }

    if (event.column.prop.toLowerCase() === 'name') {
      this.sortColumn = 'name';
    }
    else if (event.column.prop.toLowerCase() === 'date') {
      this.sortColumn = 'date';
    }

    if (this.filterFlag != true) {
      this.loadPage(limit);
    }
    else if (this.filterSavedListName) {
      this.getSavedListByName(this.filterSavedListName);
    }
  }

  // Display 'Saved List' on the Homepage
  private loadPage(limit: number) {
    this.isLoading = true;
    this.loadSavedList(limit);
  }

  // Load all saved list items in datatable.
  public loadSavedList(limit) {
    this._savedListService.getSavedList(this.rows.length, limit, this.userName, this.page, this.sortColumn, this.sortOrder).subscribe(results => {
      let data = [];
      data = results.content;
      for (let row in data) {
        data[row]['date'] = this.helper.getDate(data[row]['date'], 'DD MMM YYYY');
      }
      if (this.deleteFlag || this.page == 0) {
        this.deleteFlag = false;
        this.temp = [];
        this.rows_copy = [];
      }
      this.temp.push(...data); // Store saved list items in the filter array.
      this.rows_copy.push(...data); // Store saved list items in row_copy - which maintains a copy of the original list of the saved list items.
      this.rows = [];
      // rows[] which is displayed on the datatable with saved list items
      this.rows_copy.forEach(item => {
        this.rows.push(item);
      });
      this.isLoading = false;
      this.scrollIcon = true;
      this.page++; // Increment page counter
      if (results.first === true) {
        this.totalPages = results.totalPages;
      }
    });
  }


  // Get 'Saved List' by name.
  public getSavedListByName(savedListName) {
    this.filterSavedListName = savedListName;
    if (savedListName != '') {
      this._savedListService.getSavedListByName(this.userName, savedListName, this.userName, this.sortColumn, this.sortOrder)
        .subscribe(
          filteredSavedList => {
            const val = savedListName;
            let data = [];
            data = filteredSavedList.content;
            for (let row in data) {
              data[row]['date'] = this.helper.getDate(data[row]['date'], 'DD MMM YYYY');
            }
            this.temp = [];
            this.rows = [];
            this.temp.push(...data); // Store saved list items in the filter array.
            this.temp.forEach(item => {
              this.rows.push(item);
            });
          },
          error => {
            this.errorMessage = error;
          }
        );
    }
  }

  // Navigate to list of Properties for the selected 'Saved List' item.
  // 'list' - Object holding information about the saved list - id, name, notes, openSaveList, propertyIds.
  navigateToSavedListResults(list) {
    document.getElementsByTagName('html')[0].classList.add('loading');
    let query = '';
    this.savedListID = list.id;
    query = this._url + `/users/` + this.userName + `/lists/` + this.savedListID;
    this._savedListService.getPropertiesBySavedListId(this.userName, this.savedListID, 0, 20, 'min', null)
      .subscribe(savedList => {
        let saveListIDQueryStrValue = "";
        sessionStorage.removeItem('navigatedtosavedlistid');
        sessionStorage.setItem('navigatedtosavedlistid', this.savedListID);
        this.router.navigateByUrl('/object?savelist=' + this.savedListID);
      }, resFileError => {
        document.getElementsByTagName('html')[0].classList.remove('loading');
        this.errorMessage = resFileError;
      });
  }


  // List results matching the filter text.
  updateFilter(event) {
    this.filterFlag = true;
    if (event.target.value.length == 0) {
      this.clearFilter();
    }
    else {
      this.getSavedListByName(event.target.value.toLowerCase());
    }
  }

  // Clear input field on clicking 'x' icon.
  clearInput(ele) {
    (<HTMLInputElement>document.getElementById(ele)).value = '';
  }

  // Clear filter and show all 'Saved List' items on the table.
  clearFilter() {
    const val = '';
    const temp = this.temp.filter(function (d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });
    // When item is deleted from the filtered list
    if (this.deleteFlag) {
      this.page = 0;
      this.loadSavedList(this.pageLimit);
    }
    else {
      this.temp = [];
      this.rows = [];
      this.rows_copy.forEach(item => {
        this.temp.push(item);
        this.rows.push(item);
      });
    }
    this.filterFlag = false;
    this.filterSavedListName = null;
  }

  // Delete - Confirm Dialog
  deleteConfirm(row, rows) {
    document.getElementsByTagName("body")[0].classList.add("positionFixed");
    this._dialogService.addDialog(ConfirmComponent, {
      title: 'Delete Saved List',
      message: 'Are you sure you want to delete the saved list - ' + row.name + '?'
    })
      .subscribe((isConfirmed) => {
        document.getElementsByTagName("body")[0].classList.remove("positionFixed");
        this.confirmResult = isConfirmed;
        if (this.confirmResult) {
          this._savedListService.deleteSavedList(this.userName, row.id)
            .subscribe(deletedSavedList => {
              this.deleteFlag = true;
              this.temp = this.rows.filter(r => r !== row);
              this.rows = this.rows.filter(r => r !== row);
              this.rows_copy = this.rows_copy.filter(r => r !== row);
            }, resFileError => this.errorMessage = resFileError);
        }
      });
  }

  // Delete Saved List
  public deleteSavedList(row, rows) {
    this.deleteConfirm(row, rows);
  }

  // Populate 'Save List' modal form fields.
  // Bind 'Saved List' name, notes from DTO to input fields on 'Saved List' modal.
  populateSaveListModal(row) {
    this.savedListName = row.name;
    this.savedListNotes = row.notes;
    this.savedListID = row.id;
    this._savedListModalComponent.saveListForm.controls['name'].setValue(this.savedListName);
    this._savedListModalComponent.saveListForm.controls['notes'].setValue(this.savedListNotes);
  }

  onopenSharedSavedList() {
    document.getElementById('savedListShare').click();
  }

  // Dynamic update of saved list as and when lists are edited.
  updateSavedListRowByID(savedListItem) {
    document.getElementsByTagName('html')[0].classList.remove('loading');
    if (savedListItem.action) {
      let index = this.rows.findIndex(savedList => savedListItem.id == savedList.id);
      // When an owner name is updated, the previous owner should not have access to the list anymore
      if (savedListItem.action.toLowerCase() == 'changeowner') {
        this.rows_copy.splice(index, 1);
        this.deleteFlag = true;
        this.temp = this.rows.filter(r => r !== this.rows[index]);
        this.rows = this.rows.filter(r => r !== this.rows[index]);
        this.rows_copy = this.rows_copy.filter(r => r !== this.rows[index]);
      }
      // When a shared user deletes himself from the shared user list,
      // he should not have access to the saved list anymore.
      else if (this.rows[index].user != this.userName && savedListItem.action.toLowerCase() == 'deletebyshareduser') {
        this.rows_copy.splice(index, 1);
        this.deleteFlag = true;
        this.temp = this.rows.filter(r => r !== this.rows[index]);
        this.rows = this.rows.filter(r => r !== this.rows[index]);
        this.rows_copy = this.rows_copy.filter(r => r !== this.rows[index]);
      }
      // Saved List edit, addition of shared users.
      else if (savedListItem.action.toLowerCase() == 'deletebyowner' || savedListItem.action.toLowerCase() == 'editsavedlist') {
        this._savedListService.getSavedListById(this.userName, savedListItem.id)
          .subscribe(savedList => {
            this.rows[index].name = savedList.name;
            this.rows[index].notes = savedList.notes;
            if (savedList.sharedUsersList.length > 0) {
              this.rows[index].sharedUsersList = [];
              savedList.sharedUsersList.forEach(user => {
                this.rows[index].sharedUsersList.push(user);
              });
            }
            else {
              this.rows[index].sharedUsersList = [];
            }
          }, resFileError => this.errorMessage = resFileError);
      }
    }
  }
}