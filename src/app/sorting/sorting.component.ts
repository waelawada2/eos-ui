import { Component, EventEmitter, Output } from '@angular/core';
import { ArtistService } from '../artist/artist.service';
import { DialogService } from 'ng2-bootstrap-modal';
import { TosatService } from '../common/toasty-service';
import { CustomSortComponent } from '../../app/Merge/customesort.component';
import { Observable } from 'rxjs/Observable'

@Component({
  selector: 'sort',
  templateUrl: './sorting.component.html'
})

export class SortingComponent {
  public catalogSelected;
  public catalogArtistsCount = 0;
  public catalogs = [];
  public catalogIds = [];
  public errorMsg;
  public selectedValue = 'selected';
  public sortSelectClicked = 0; // tracks 'sort' <select> 'click' event count
  public hasCatalogs = false;

  @Output()
  public sorting: EventEmitter<any> = new EventEmitter();

  constructor(
    public _artistService: ArtistService,
    private dialogService: DialogService,
    private tosatservice: TosatService) {
    // Display CR options in 'sort' dropdown list.
    this.populateCatalogsSortOptions();

    // 'Sort' dropdown set selected value
    this.selectedValue = sessionStorage.getItem('selectSort')
      && sessionStorage.getItem('selectSort').length ? sessionStorage.getItem('selectSort') : 'selected';
  }

  // Display CR options in 'sort' dropdown list.
  populateCatalogsSortOptions() {
    this.catalogs = [];
    this.catalogIds = [];
    const searchParam = sessionStorage.getItem('searchparam')
    const saveListView = sessionStorage.getItem('savelistview')
    if (searchParam && searchParam.indexOf('artistIds') > -1 && saveListView === 'false') {
      const artistIds = this.getArtists().map(artist => artist.split('=')[1]);
      const artistObs = Observable.from(artistIds)
        .concatMap((artist) => this._artistService.getIndividualArtist(artist))

      this.catalogArtistsCount = artistIds.length;
      if (artistIds.length > 1) {
        this.catalogs.push('CR');
        artistObs
          .filter(artistObj => !this.hasCatalogs)
          .subscribe(artistObj => {
            // Check if artist(s) have catalogs
            this.hasCatalogs = artistObj.artistCatalogueRaisonees.length > 0
          }, resFileError => this.errorMsg = resFileError);
      } else {
        artistObs.subscribe((artistObj) => {
          if (artistObj.artistCatalogueRaisonees.length > 0) {
            artistObj.artistCatalogueRaisonees.forEach((cr, index) => {
              this.catalogs.push(`CR ${index + 1}: ${cr.author}`);
              this.catalogIds.push({ crId: cr.id });
            })
          } else { // When an object search includes only one artist, but the artist has no catalogs
            this.catalogs.push('CR');
          }
        }, resFileError => this.errorMsg = resFileError)
      }
    }
  }

  // Called when an object search is performed
  objectSearched() {
    this.populateCatalogsSortOptions();
  }

  private getArtists() {
    const searchParam = sessionStorage.getItem('searchparam')
    const saveListView = sessionStorage.getItem('savelistview')
    if (searchParam && searchParam.indexOf('artistIds') > -1 && saveListView === 'false') {
      return searchParam
        .substr(searchParam.indexOf('artistIds'), searchParam.length)
        .replace(/^&/, '')
        .split('&')
        .filter((artist) => artist !== '' && artist.indexOf('artistIds') !== -1)
    }
    return [];
  }

  selectSort(value: string) {
    if (value.indexOf('CR') > -1) {
      this.catalogSelected = value;
      sessionStorage.setItem('selectSort', this.catalogSelected);
      value = 'CR';
    } else {
      sessionStorage.setItem('selectSort', value);
    }

    switch (value) {
      case 'artist':
        this.sortDetails(`&sort=${SORTING_PARAMS.ARTIST.toString()}`)
        break;
      case 'titleEN':
        this.sortDetails(`&sort=${SORTING_PARAMS.TITLE_EN.toString()}`)
        break;
      case 'titleOrig':
        this.sortDetails(`&sort=${SORTING_PARAMS.TITLE_ORIG.toString()}`);
        break;
      case 'size':
        this.sortDetails(`&sort=${SORTING_PARAMS.SIZE.toString()}`);
        break;
      case 'height':
        this.sortDetails(`&sort=${SORTING_PARAMS.HEIGHT.toString()}`);
        break;
      case 'width':
        this.sortDetails(`&sort=${SORTING_PARAMS.WIDTH.toString()}`);
        break;
      case 'depth':
        this.sortDetails(`&sort=${SORTING_PARAMS.DEPTH.toString()}`);
        break;
      case 'year':
        this.sortDetails(`&sort=${SORTING_PARAMS.YEAR.toString()}`);
        break;
      case 'sortcode':
        this.sortDetails(`&sort=${SORTING_PARAMS.SORT_CODE.toString()}`);
        break;
      case 'owner':
        this.sortDetails(`&sort=${SORTING_PARAMS.OWNER.toString()}`);
        break;
      case 'CR':
        this.sortCRDetails();
        break;
    }
  }

  sortDetails(value) {
    this.sorting.emit(value);
  }

  sortCRDetails() {
    // Display message when user attempts to sort by CR for artist(s) with no catalogs.
    if (this.catalogArtistsCount > 0 && this.catalogs.length === 1 && this.catalogs[0] === 'CR' && !this.hasCatalogs) {
      if (this.catalogArtistsCount === 1) {
        this.tosatservice.addToast('Cannot sort by "CR" as the artist has no catalogue raisonne.', 'top-right');
      } else {
        this.tosatservice.addToast('Cannot sort by "CR" as the artists have no catalogue raisonnes.', 'top-right');
      }
    } else {
      if (this.catalogArtistsCount === 1) {
        // When an object search includes only one artist, but the artist has no catalogs
        if (this.catalogs.length === 1 && this.catalogs[0] === 'CR') {
          this.sortDetails(`&sort=${SORTING_PARAMS.CR().toString()}`)
        } else {
          const catalogId = this.catalogSelected.split(':')[0].split('CR ')[1];
          const artistCRId = this.catalogIds[catalogId - 1];
          this.sortDetails(`&sort=${SORTING_PARAMS.CR(artistCRId.crId).toString()}`)
        }
      } else {
        if (sessionStorage.getItem('lastcr') && sessionStorage.getItem('lastcr').indexOf(',') > -1) {
          const lastCRs = sessionStorage.getItem('lastcr');
          this.sortDetails(`&sort=${SORTING_PARAMS.CR(lastCRs).toString()}`)
        } else {
          this.sortDetails(`&sort=${SORTING_PARAMS.CR().toString()}`)
        }
      }
    }
  }

  // Enable 'click' event on 'sort' <select> list.
  // Enables selection of an already selected item on the list.
  customSortSelect(e) {
    this.sortSelectClicked++;
    const selected = sessionStorage.getItem('selectSort');
    if (this.sortSelectClicked > 1 && selected !== 'more') {
      this.sortSelectClicked = 0;
    } else if (this.sortSelectClicked > 1 && selected === 'more' && e.target.value === 'more') {
      const options = e.target.options;
      for (let i = 0; i < options.length; i++) {
        // Check if 'more' was clicked
        if (options[i].selected === true) {
          this.sortDetails('more');
          this.sortSelectClicked = 0;
          break;
        }
      }
    }
  }
}

const SORTING_PARAMS = {
  ARTIST: [
    'artists.lastName',
    'dateDetails.yearStart',
    'propertyCatalogueRaisonees.artistCatalogueRaisonee.id',
    'propertyCatalogueRaisonees.volume',
    'propertyCatalogueRaisonees.number',
    'sortCode',
    'title',
    'edition.editionType.id,edition.editionNumber',
    'category.name,dateDetails.castYearStart'
  ],
  CR(artistCatalogueRaisoneeIds = null) {
    const sort = [
      'propertyCatalogueRaisonees.artistCatalogueRaisonee.id',
      //'propertyCatalogueRaisonees.volume',
      'propertyCatalogueRaisonees.number',
    ];
    const artistCatRaissoneesIds = artistCatalogueRaisoneeIds;
    return {
      toString: () => {
        if (!artistCatRaissoneesIds) {
          return `${sort.toString()}`;
        }
        return `${sort.toString()}&artistCatalogueRaisoneeIds=${artistCatRaissoneesIds}`;
      }
    }
  },
  DEPTH: [
    'dimensions.depth',
    'dateDetails.yearStart',
    'propertyCatalogueRaisonees.artistCatalogueRaisonee.id',
    'propertyCatalogueRaisonees.volume',
    'propertyCatalogueRaisonees.number',
    'sortCode',
    'title',
    'edition.editionType.id',
    'edition.editionNumber',
    'category.name',
    'dateDetails.castYearStart',
  ],
  HEIGHT: [
    'dimensions.height',
    'dateDetails.yearStart',
    'propertyCatalogueRaisonees.artistCatalogueRaisonee.id',
    'propertyCatalogueRaisonees.volume',
    'propertyCatalogueRaisonees.number',
    'sortCode',
    'title',
    'edition.editionType.id',
    'edition.editionNumber',
    'category.name',
    'dateDetails.castYearStart',
  ],
  SIZE: [
    'artists.lastName',
    'dateDetails.yearStart',
    'propertyCatalogueRaisonees.artistCatalogueRaisonee.id',
    'propertyCatalogueRaisonees.volume',
    'propertyCatalogueRaisonees.number',
    'sortCode',
    'title',
    'edition.editionType.id',
    'edition.editionNumber',
    'category.name',
    'dateDetails.castYearStart',
  ],
  SORT_CODE: [
    'sortCode',
    'artists.lastName',
    'dateDetails.yearStart',
    'artists.lastName',
    'propertyCatalogueRaisonees.artistCatalogueRaisonee.id',
    'propertyCatalogueRaisonees.volume',
    'propertyCatalogueRaisonees.number',
    'sortCode',
    'title',
    'edition.editionType.id',
    'edition.editionNumber',
    'category.name',
    'dateDetails.castYearStart',
  ],
  OWNER: [
    'currentOwnerId',
    'artists.lastName',
    'dateDetails.yearStart',
  ],
  TITLE_EN: [
    'title',
    'dateDetails.yearStart',
    'propertyCatalogueRaisonees.artistCatalogueRaisonee.id',
    'propertyCatalogueRaisonees.volume',
    'propertyCatalogueRaisonees.number',
    'sortCode,edition.editionType.id',
    'edition.editionNumber',
    'category.name',
    'dateDetails.castYearStart',
  ],
  TITLE_ORIG: [
    'originalTitle',
    'dateDetails.yearStart',
    'propertyCatalogueRaisonees.artistCatalogueRaisonee.id',
    'propertyCatalogueRaisonees.volume',
    'propertyCatalogueRaisonees.number',
    'sortCode',
    'edition.editionType.id',
    'edition.editionNumber',
    'category.name',
    'dateDetails.castYearStart',
  ],
  WIDTH: [
    'dimensions.width',
    'dateDetails.yearStart',
    'propertyCatalogueRaisonees.artistCatalogueRaisonee.id',
    'propertyCatalogueRaisonees.volume',
    'propertyCatalogueRaisonees.number',
    'sortCode',
    'title',
    'edition.editionType.id',
    'edition.editionNumber',
    'category.name',
    'dateDetails.castYearStart',
  ],
  YEAR: [
    'dateDetails.yearStart',
    'artists.lastName',
    'propertyCatalogueRaisonees.artistCatalogueRaisonee.id',
    'propertyCatalogueRaisonees.volume',
    'propertyCatalogueRaisonees.number',
    'sortCode',
    'title',
    'edition.editionType.id',
    'edition.editionNumber',
    'category.name',
    'dateDetails.castYearStart',
  ],
}
