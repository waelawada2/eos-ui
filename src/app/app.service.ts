import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ConfigurationLoaderService } from 'app/configuration.service'

@Injectable()
export class AppService {
  private _url: string;

  constructor(private _http: Http, configuration: ConfigurationLoaderService) {
    this._url = configuration.getSettings().apiUrl
  }
  //countries
  getCountries() {
    return this._http.get(this._url + "/countries")
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  //categories
  getCategories() {
    return this._http.get(this._url + "/categories")
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  _errorHandler(_error: Response) {
    return Observable.throw(_error || "server error 404");
  }


  // Get 'Users' - returns list of application users.
  public getUsers() {
    return this._http.get(this._url + `/users?size=2000`)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
}