import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { ConfigurationLoaderService } from 'app/configuration.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AdminSavedListService {

  private _url: string

  constructor(private _http: Http, configuration: ConfigurationLoaderService) {
    this._url = configuration.getSettings().apiUrl
  }

  getSavedLists(page) {
    return this._http.get(this._url + '/users/lists/all?page=' + page)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error("Could not retrieve 'Saved List'." + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  getSavedNameList(page) {
    return this._http.get(this._url + '/users/lists/name?' + page)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error("Could not retrieve 'Saved List'." + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  updateSavedList(externalId, status) {
    return this._http.put(this._url + '/users/lists/' + externalId + '/researchStatus?status=' + status, {})
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error("Could not retrieve 'Saved List'." + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  getSaveListPropertyCount(id: any, username: any) {
    return this._http.get(this._url + '/users/' + username + '/lists/' + id + '/propertiesCount')
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error("Could not retrieve 'Saved List Complete Status'." + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  _errorHandler(_error: Response) {
    return Observable.throw(_error || "Server Error - 404.");
  }
}
