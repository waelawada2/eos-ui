import { Component } from '@angular/core';
import { AdminSavedListService } from './admin-saved-list.service';
import { Router, NavigationStart } from '@angular/router';
import { DialogService } from 'ng2-bootstrap-modal';
import { ConfirmComponent } from '../common/confirm-component';
import { TosatService } from '../common/toasty-service';
import { AlertComponent } from '../common/alert-component';
import { KeyboardkeysService } from '../../app/keyboardkeys/keyboardkeys.service';
import { HeaderService } from '../../app/header/header.service';

@Component({
  selector: 'admin-saved-list',
  templateUrl: './admin-saved-list.component.html',
  styleUrls: ['./admin-saved-list.component.css'],
  providers: [AdminSavedListService, KeyboardkeysService, HeaderService]
})

export class AdminSavedList {

  public savedListData: Array<any>;
  public savedListData_copy: Array<any>;
  public userName = window['keycloak'].tokenParsed.preferred_username;
  public totalpages: number = 0;
  public currentPage: number = 0;
  public saveListStatus: Array<any> = [];
  public keycloak = window['keycloak'];

  constructor(private _adminSavedListService: AdminSavedListService,
    private _router: Router,
    private _dialogService: DialogService,
    private _keyboardkeysService: KeyboardkeysService,
    private _tosatService: TosatService,
    private _headerService: HeaderService,
  ) { }

  ngOnInit() {
    var query = this.currentPage + this.defaultSort;
    if (this.queryParameter) {
      query = this.currentPage + this.queryParameter + this.defaultSort
    }
    document.getElementsByTagName('html')[0].classList.add('loading');
    this._adminSavedListService.getSavedLists(query)
      .subscribe(response => {
        this.savedListData = response.content;
        for (var k = 0; k < this.savedListData.length; k++) {
          this.saveListStatus.push(this.savedListData[k].saveListCompleteStatus);
        }
        this.savedListData_copy = response.content;
        this.totalpages = response.totalPages;
        this.currentPage++;
        document.getElementsByTagName('html')[0].classList.remove('loading');
      }, resFileError => {
        document.getElementsByTagName('html')[0].classList.remove('loading');
      })
  }

  getRequiredDate(presentDate) {
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    let month = parseInt(presentDate.split('-')[0]) - 1;
    let currentMonth = months[month];
    let day = presentDate.split('-')[1];
    let year = presentDate.split('-')[2];
    return (day + " " + currentMonth + " " + year);
  }

  checkboxChanged(event, i) {
    var flagVal = event.target.checked ? 'dropDownTrue' : 'dropDownFalse';
    this._dialogService.addDialog(ConfirmComponent, { title: flagVal, message: 'Select a Value' },
      { backdropColor: 'rgba(220,220,220,0.5)' })
      .subscribe((isConfirmed) => {
        if (isConfirmed == "incomplete") {
          this.saveListStatus[i] = false;
          event.target.checked = false;
        } else {
          if (!isConfirmed) {
            event.target.checked = !event.target.checked
          } else {
            this.saveListStatus[i] = true;
            event.target.checked = true;
          }
        }
      })
  }

  saveListSaveButton(i) {
    this._adminSavedListService.getSaveListPropertyCount(this.savedListData[i].id, window['keycloak'].tokenParsed.preferred_username)
      .subscribe(Response => {
        var statusText = this.saveListStatus[i] ? 'Complete' : 'Incomplete';
        this._dialogService.addDialog(ConfirmComponent, { title: '', message: 'Are you sure you would like to change all ' + Response + ' objects to Status to ' + statusText },
          { backdropColor: 'rgba(220,220,220,0.5)' })
          .subscribe((isConfirmed) => {
            if (isConfirmed) {
              this._adminSavedListService.updateSavedList(this.savedListData[i].id, this.saveListStatus[i])
                .subscribe(response => {
                  this._tosatService.addToast("savedList updated Successfully", "top-right");
                },
                  resFileError => {
                    if (JSON.parse(resFileError['_body']).details.indexOf('closed') > -1) {
                      this._dialogService.addDialog(AlertComponent,
                        { title: '', message: 'Unable to update as the saveList is closed' }, { backdropColor: 'rgba(220,220,220,0.5)' })
                    } else {
                      this._tosatService.addToastError("There is an Error while updating Savedlist", "top-right");
                    }
                  })
            }
          })
      })
  }

  public sortOrder = 'asc';
  public sortColumn = 'name';
  public defaultSort = '&sort=name,asc';
  public queryParameter;
  textboxValue(event) {
    document.getElementsByTagName('html')[0].classList.add('loading');
    if ((<HTMLInputElement>document.getElementById('savelist-admin-filter')).value) {
      this.currentPage = 0;
      this.queryParameter = "&name=" + (<HTMLInputElement>document.getElementById('savelist-admin-filter')).value;
      this._adminSavedListService.getSavedNameList(this.currentPage + this.queryParameter + this.defaultSort)
        .subscribe(response => {
          this.currentPage = 0;
          this.savedListData = response;
          this.savedListData_copy = response;
          this.saveListStatus = [];
          for (var k = 0; k < this.savedListData.length; k++) {
            this.saveListStatus.push(this.savedListData[k].saveListCompleteStatus);
          }
          this.totalpages = response.totalPages;
          this.currentPage++;
          document.getElementsByTagName('html')[0].classList.remove('loading');
        }, resFileError => {
          document.getElementsByTagName('html')[0].classList.remove('loading');
        })
    } else {
      this.currentPage = 0;
      this.queryParameter = '';
      this.ngOnInit();
      document.getElementsByTagName('html')[0].classList.remove('loading');
    }
  }

  // Sort
  onSort(event) {
    if (this.sortOrder == 'asc') {
      this.sortOrder = 'desc';
    } else {
      this.sortOrder = 'asc';
    }
    if (event === 'name') {
      this.sortColumn = 'name';
    } else if (event === 'date') {
      this.sortColumn = 'date';
    }
    this.defaultSort = '&sort=' + this.sortColumn + ',' + this.sortOrder;
    this.currentPage = 0;
    this.ngOnInit();
  }

  goToAdmin() {
    this._router.navigate(['/admin']);
  }

  scrolled() {
    var elem = document.getElementById('AdminSavedListContainer');
    if (elem.scrollHeight - elem.scrollTop === elem.clientHeight) {
      if (this.currentPage <= this.totalpages) {
        var query = this.currentPage + this.defaultSort;
        if (this.queryParameter) {
          document.getElementsByTagName('html')[0].classList.add('loading');
          query = this.currentPage + this.queryParameter + this.defaultSort;
          this._adminSavedListService.getSavedNameList(query)
            .subscribe(response => {
              this.savedListData = this.savedListData.concat(response.content);
              for (var k = 0; k < this.savedListData.length; k++) {
                this.saveListStatus.push(this.savedListData[k].saveListCompleteStatus);
              }
              this.savedListData_copy = this.savedListData_copy.concat(response.content);
              this.totalpages = response.totalPages;
              this.currentPage++;
              document.getElementsByTagName('html')[0].classList.remove('loading');
            })
        } else {
          document.getElementsByTagName('html')[0].classList.add('loading');
          this._adminSavedListService.getSavedLists(query)
            .subscribe(response => {
              this.savedListData = this.savedListData.concat(response.content);
              for (var k = 0; k < this.savedListData.length; k++) {
                this.saveListStatus.push(this.savedListData[k].saveListCompleteStatus);
              }
              this.savedListData_copy = this.savedListData_copy.concat(response.content);
              this.totalpages = response.totalPages;
              this.currentPage++;
              document.getElementsByTagName('html')[0].classList.remove('loading');
            })
        }
      }
    }
  }

  redirect(page) {
    switch (page) {
      case 'home':
        this._router.navigateByUrl('/');
        break;
      default:
        this._router.navigateByUrl('/admin');
    }
  }
  // Keyboard shortcuts
  keyboardShortCutSavedListFunction(e) {
    switch (this._keyboardkeysService.getKeyboardResponse(e)) {
      case 'duplicateWindow':
        let urlP = window.location.href;
        window.open(urlP, '_blank');
        break;
    }
  }

  // Header Drop-down toggle mode
  toggleFlag: number;
  toggleFunction(flag: number) {
    this.toggleFlag = this._headerService.headerToggle((flag === this.toggleFlag) ? 0 : flag);
  }
}
