import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { ConfigurationLoaderService } from 'app/configuration.service'

@Injectable()
export class ObjectService {

  private _url: string;

  constructor(private _http: Http, configuration: ConfigurationLoaderService) {
    this._url = configuration.getSettings().apiUrl;
  }

  getProperties(body: any) {
    let query = body ? body.replace('&&', '&') : '&lod=MIN';
    query = '?' + query;
    query = query.replace('?&', '?');
    if (body === undefined) { query = '?'; }
    //get same Object details
    return this._http.get(this._url + '/properties' + query)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          document.getElementsByTagName('html')[0].classList.remove('loading');
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  getSortedProperties(sortvalue: any) {
    return this._http.get(this._url + '/properties?sort=' + sortvalue)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  getProperty(id: string) {
    return this._http.get(this._url + '/properties/' + id)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  getPropertyMerge(id: string) {
    return this._http.get(this._url + '/properties/' + id)
      .map((response: Response) => {
        return response;
      })
      .catch(this._errorHandler);
  }

  // Getting the Categories
  getCategories() {
    return this._http.get(this._url + '/categories')
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  // Getting the preSizeTypes
  getPreSizeTypes() {
    return this._http.get(this._url + '/presize-types')
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // duplicate object creation
  duplicateProp(body: Object) {
    return this._http.post(this._url + '/properties/' + body, {})
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  // Add a new Object/Property
  addProperty(body: Object) {
    return this._http.post(this._url + '/properties', body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  // Update Object/Property
  updateProperty(propertyId: string, body: Object) {
    return this._http.put(this._url + `/properties/${propertyId}`, body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  // Delete Object/Property
  removeProperty(id: string) {
    return this._http.delete(this._url + `/properties/${id}`)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  // remove Dropzone Image
  removeImage(url) {
    return this._http.delete(url)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // check image url
  checkImageUrl(url) {
    return this._http.get(url)
      .map((response: Response) => {
        if (response.status === 200 || response.status === 201) {
          return response.status;
        }
      })
      .catch((error: any) => {
        return Observable.throw(new Error(error.status));
      });
  }

  // Get 'Edition Size Types'
  getEditionSizeTypes() {
    return this._http.get(this._url + '/edition-size-types')
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Could not retrieve Edition Size Types.' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  // Get 'Edition Types'
  getEditionTypes() {
    return this._http.get(this._url + '/edition-types')
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('Could not retrieve Edition Size Types.' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  // Image Upload Object/Property
  imageUpload(id: any, body: Object) {
    return this._http.post(this._url + '/properties/' + id + '/images', body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  mergeObject(targetId: string, sourceId: string, body: Object) {
    //get same Object details
    return this._http.put(this._url + '/properties/' + targetId + '&action=merge&mergeFromId=' + sourceId, body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  //delete objects page by page
  deleteProperties(body: object) {
    return this._http.post(this._url + '/properties/delete/propertyIds', body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler)
  }
  _errorHandler(_error: Response) {
    document.getElementsByTagName('html')[0].classList.remove('loading');
    return Observable.throw(_error || 'server error 404');
  }

  // Getting the Image Subjects
  getImageSubjects() {
    return this._http.get(this._url + '/imageSubjects')
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  getImageGenders() {
    return this._http.get(this._url + '/imageGenders')
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  getImagePositions() {
    return this._http.get(this._url + '/imagePositions')
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  getImageFigures() {
    return this._http.get(this._url + '/imageFigures')
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  getMainColors() {
    return this._http.get(this._url + '/imageMainColours')
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  getImageAnimals(name: string, headerFlag: string) {
    return this._http.get(this._url + `/imageAnimals/${name}`)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          // return response.json();
          return this.animalDataFormatter(response.json(), name, headerFlag);
        }
      })
      .catch(this._errorHandler);
  }
  getSitters(body: Object, tempSitterFName: string, tempSitterLName: string, headerFlag: string) {
    return this._http.get(this._url + '/sitters?' + body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return this.sitterDataFormatter(response.json(), tempSitterFName, tempSitterLName, headerFlag);
          //     return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  // Add a new Sitter
  addSitter(body: Object) {
    return this._http.post(this._url + "/sitters", body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  // Add a new Animal
  addAnimal(body: Object) {
    return this._http.post(this._url + "/imageAnimals", body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  getOtherTags(name: string, headerFlag: string) {
    return this._http.get(this._url + '/otherTags?name=' + name)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          //return response.json();
          return this.otherTagDataFormatter(response.json(), name, headerFlag);
        }
      })
      .catch(this._errorHandler);
  }
  addOtherTag(body: Object) {
    return this._http.post(this._url + '/otherTags', body)
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  getAllOrientations() {
    return this._http.get(this._url + '/automatedTags/orientations')
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }
  getAllScales() {
    return this._http.get(this._url + '/automatedTags/scales')
      .map((response: Response) => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error('This request has failed ' + response.status)
        } else {
          return response.json();
        }
      })
      .catch(this._errorHandler);
  }

  //Get sitter with Value
  sitterDataFormatter = (response: any, tempSitterFName: string, tempSitterLName: string, headerFlag: string): Observable<any> => {
    var checkExists = false;
    if (response && response.length > 0) {
      response.forEach(data => {
        var name = (data.firstName ? data.firstName + ' ' : '') + (data.lastName ? data.lastName : '');
        data.name = name.trim();
        var fName = data.firstName ? data.firstName.trim() : '';
        var lName = data.lastName ? data.lastName.trim() : '';
        if ((fName.toUpperCase() == tempSitterFName.trim().toUpperCase())
          && (lName.trim().toUpperCase() == tempSitterLName.trim().toUpperCase())) {
          checkExists = true;
        }
      });
      response.sort(function (a, b) {
        var x = a.name.toLowerCase();
        var y = b.name.toLowerCase();
        if (x < y) { return -1; }
        if (x > y) { return 1; }
        return 0;
      });
    }
    if (!checkExists && headerFlag != 'search') {
      response.push({
        name: (tempSitterFName + ' ' + tempSitterLName).trim(),
        firstName: tempSitterFName,
        lastName: tempSitterLName,
        id: '-1'
      });
    }
    return response;
  }

  //Get animal with value
  animalDataFormatter = (response: any, tempAnimalName: string, headerFlag: string): Observable<any> => {
    var checkExists = false;
    if (response && response.length > 0) {
      response.forEach(data => {
        if (data.name && data.name.toUpperCase() == tempAnimalName.toUpperCase()) {
          checkExists = true;
        }
      });
    }
    if (!checkExists && headerFlag != 'search') {
      response.push({
        name: (tempAnimalName.toUpperCase()),
        id: '-1'
      });
    }
    return response;
  }

  //Get other tags with value
  otherTagDataFormatter = (response: any, tempOtherTagName: string, headerFlag: string): Observable<any> => {
    var sitterOverFlowLimit = 50;
    var sitterOverFlowNewLabel = 35;
    var dispName = '';
    var checkExists = false;
    if (response && response.length > 0) {
      response.forEach(data => {
        dispName = data ? ((data.name && data.name.length > sitterOverFlowLimit) ?
          data.name.slice(0, sitterOverFlowLimit) + "..." : data.name) : '';
        data.displayName = dispName;
        if (data.name && data.name.toUpperCase() == tempOtherTagName.toUpperCase()) {
          checkExists = true;
        }
      });
    }
    if (!checkExists && headerFlag != 'search') {
      dispName = (tempOtherTagName.length > sitterOverFlowNewLabel) ?
        tempOtherTagName.slice(0, sitterOverFlowNewLabel) + "..." : tempOtherTagName;
      response.push({
        displayName: dispName,
        name: tempOtherTagName ? tempOtherTagName : '',
        id: '-1'
      });
    }
    return response;
  }
}

//Checked CR and ArchiveNumber undefined values
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'CRNumberCheck' })
export class CRNumberPipe implements PipeTransform {
  transform(CRdata) {
    return CRdata.filter(CR => {
      if (CR.number) { return CR; }
    });
  }
}

@Pipe({ name: 'archiveNumberCheck' })
export class ArchiveNumberPipe implements PipeTransform {
  transform(archiveNumberData) {
    return archiveNumberData.filter(archive => {
      if (archive.archiveNumber) { return archive; }
    });
  }
}