﻿import { Component, OnInit, ElementRef, Input, ViewChild, EventEmitter, Output, HostListener } from '@angular/core';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Http, Response } from '@angular/http';
import { ActivatedRoute, Router, Params, NavigationStart, NavigationEnd } from '@angular/router';
import { AppService } from '../app.service';
import { ObjectService } from './object.service';
import { ArtistService } from '../artist/artist.service';
import { Observable } from 'rxjs/Observable';
import { KeyboardkeysService } from '../../app/keyboardkeys/keyboardkeys.service';
import { PaginationService } from '../../app/pagination/pagination.service';
import { HeaderService } from '../../app/header/header.service';
import { FooterService, FooterUser } from '../../app/footer/footer.service';
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";
import 'rxjs/Rx';
import { ImageResult, ResizeOptions } from 'ng2-imageupload';
import { CompleterCmp, CompleterService, CompleterData, CompleterItem } from 'ng2-completer';
import { ObjectModalComponent } from '../../app/modal/newObjectModal.component';
import { DropuploadComponent } from '../../app/upload/dropupload/dropupload.component'
import { Helper } from '../utils/helper';
import { SavedSearchComponent } from '../saved-search/saved-search.component';
import { SaveSearchModalComponent } from '../saved-search/saved-search-modal.component';
import { SavedSearchService } from '../saved-search/saved-search.service';
import { SavedListComponent } from '../saved-list/saved-list.component';
import { SaveListModalComponent } from '../saved-list/saved-list-modal.component';
import { SaveListShareModalComponent } from '../saved-list/saved-list-share-modal.component';
import { SavedListService } from '../saved-list/saved-list.service'
import { EventReportService } from '../shared/services';
import { ModalModule } from 'ngx-modal';
import { MergeComponent } from '../Merge/Merge.component';
import { MergeTwoComponent } from '../Merge/mergetwo-component';
import { TosatService } from '../common/toasty-service';
import { DialogService } from 'ng2-bootstrap-modal';
import { ConfirmComponent } from '../common/confirm-component';
import { AlertComponent } from '../common/alert-component';
import { EditionModalComponent } from '../../app/modal/newEditionModal.component';
import { SortingComponent } from '../sorting/sorting.component';
import { CustomSortComponent } from '../../app/Merge/customesort.component';
import { EventReportComponent } from './event-report/event-report.component';
import { AddObjectsItemComponent } from './add-objects-item/add-objects-item.component';
import { ItemsService } from '../event/shared/services/items.service';
import { NotificationsService } from 'angular2-notifications';
import { GeneralConstants, GeneralDictionary } from '../shared/constants';
import { getErrorStringFromServer } from './../event/shared/utils';
import { ArtistComponent } from 'app/artist/artist.component';
import { SavedListBackgroundService } from '../SavedlListBackground/savedListBackground.service';
import { ConfigurationLoaderService } from 'app/configuration.service'

import * as _ from 'lodash';

@Component({
  templateUrl: './object.component.html',
  styleUrls: ['./object.component.css'],
  providers: [AppService, ObjectService, ArtistService, KeyboardkeysService, PaginationService, HeaderService,
    FooterService, SaveSearchModalComponent, Helper, SavedSearchComponent, SaveListModalComponent, SavedListComponent,
    SavedListService, SaveListShareModalComponent],
})
export class ObjectComponent implements OnInit {
  @ViewChild('categoryList') categoryList: CompleterCmp;
  @ViewChild('subCategoryList') subCategoryList: CompleterCmp;
  @ViewChild('presizeNameList') presizeNameList: CompleterCmp;
  @ViewChild(DropuploadComponent)
  private dropuploadComponent: DropuploadComponent;
  @ViewChild(EventReportComponent) form: EventReportComponent;
  public confirmResult: boolean = null;
  promptMessage: string = '';
  public column: NodeListOf<Element> = document.getElementsByClassName('column');
  public navbar: NodeListOf<Element> = document.getElementsByClassName('navbar');
  public columnHeight: any;
  public keycloak = window['keycloak'];
  public keycloakUserName = this.keycloak.tokenParsed && this.keycloak.tokenParsed.preferred_username ?
    this.keycloak.tokenParsed.preferred_username : null;
  public reset = true;
  private URL: string;
  public sArtist = [];
  public sArtistAll = [];
  public titleEng = [];
  public titleOrig = [];
  objectId;
  private currentProperty;
  artists: any[];
  selectArtist: Object = {};
  public temp;
  public externalId;
  public fullPath: string;
  public myForm: FormGroup;
  public objectDetailsForm: FormGroup;
  public editionSizeForm: FormGroup;
  public selected = [];
  public filteredList;
  public errorMsg;
  public alertMsg = false;
  src: string = "";
  public saveListSortParam;
  public queryResults = '&';
  public sortingselected = false;
  public artistChecked = false;
  public savedlistclicked = false;
  public thumbnailtoobject = false;
  public sortingqueryparam;

  public categories = [];
  public subcategories = [];
  public presizeNames = [];
  public editionSizeTypes = [];
  public editionTypes = [];


  // archive number
  public archiveNumbers = [];
  public archiveNoTags = [];

  //pagination
  public current_page = 1;
  public total_pages = 0;
  public curPageValue = 1;
  public pageNo;
  public pageNumber = 0;
  public pageData = [];
  public singleQuery;
  public queryParams;
  public queParam;
  public queryValue;

  // Saved List
  public saveAllQueryParams;
  public saveAllTotalElements;
  public savedListId;
  public savedListProperties = [];
  public saveListView = false;
  public openSaveListModal = false;

  public emptyPagination = '';
  public widthStyle: number = 85;

  //To keep track of the mode you're in - e.g. 'searchMode'
  flag = null;

  //Catalogue Raisonnes array
  public propertyCatalogRaisonnee = [];

  //Search, Edit, Save Mode flag
  public editModeFlag = true;
  public backModeFlag = false;
  public headerFlag = 'search';

  //Error message flag
  public requiredFlag = false;
  public confirmFlag = false;

  // Edition Size Fields
  public editionSizeSeries = null;
  public editionSizeArtistProof = null;
  public editionSizeTrialProof = null;
  public editionSizeBonATirerProof = null;
  public editionSizePrintersProof = null;
  public editionSizeHorsCommerceProof = null;
  public editionSizeDedicatedProof = null;
  public editionSizeMuseumProof = null;
  public editionSizeEditionOverride = null;
  public editionSizeDisplayTotal = { total: null, display: null };
  public editionSizeNotes = null;

  // Flags Fields
  public country: string;
  public countries: Array<object> = [];
  public errorMessage: string;
  protected countryService: CompleterData;
  protected categoryService: CompleterData;
  protected subCategoryService: CompleterData;
  protected presizeTypesService: CompleterData;
  protected editionTypeService: CompleterData;
  //For Tags
  protected imageFigureService: CompleterData;
  protected autoOrientationService: CompleterData;
  protected autoScaleService: CompleterData;
  protected autoImageService: CompleterData;

  public flagsCountryValue = null;

  // Researcher Notes
  public researchNotesDate: Date;

  // Pagination
  public propertyNumber: number = 0;
  public isConfirmWindowOpen = false;
  // Select List 'OPEN/CLOSE' status
  private categoryListOpen = false;
  private subCategoryListOpen = false;
  private presizeNameListOpen = false;
  private editionNameListOpen = false;
  private flagsCountryListOpen = false;
  private presentationView = false;
  private dropdownView = 'mainView';
  //Tags
  private showProjects = 'main';
  private imageFigureListOpen = false;
  private autoOrientationListOpen = false;
  private autoScaleListOpen = false;
  private autoImageListOpen = false;
  public imageFigures = [];
  public autoOrientations = [];
  public autoScales = [];
  public autoImages = [{ name: "Yes" }, { name: "No" }];

  public nGender = [];
  public allGendersList = [];
  public selectedGender = [];

  public nPosition = [];
  public allPositionsList = [];
  public selectedPosition = [];

  public selectedMainColor = [];
  public nMainColor = [];
  public allMainColorList = [];

  public nSubject = [];
  public selectedSubject = [];
  public allSubjectsList = [];

  public selectedAnimal = [];
  public selectedSitter = [];
  public nOtherTag = [];
  public selectedOtherTag = [];
  public tempFigures: Array<any> = [];

  public displayProgressBar = false;
  //Search thumbnails page flag
  public thumbnailFlag: boolean = false;

  public options = GeneralConstants.NOTIFICATIONS.SETUP;
  public sortFlag = true;
  dictionary = GeneralDictionary;
  // Last CR of selected artists
  public lastCR = [];

  // EOS-1173 Sort Order
  public sortOrder = 'asc';
  public sorted = false;

  //1818 Remove from list
  public openedSaveList: boolean = false;
  public tempFigureIntialValue;

  // Access to 'Save Search' Modal
  @ViewChild(SaveSearchModalComponent) public saveSearchModal: SaveSearchModalComponent;

  // Access to 'Save List' Modal
  @ViewChild(SaveListModalComponent) public saveListModal: SaveListModalComponent;

  // Access to 'sort' component
  @ViewChild('sortComponent') private sortComponent: SortingComponent;

  //constructor method
  constructor(private tosatservice: TosatService,
    private builder: FormBuilder,
    private _sanitizer: DomSanitizer,
    private http: Http,
    private el: ElementRef,
    private route: ActivatedRoute,
    private _objectService: ObjectService,
    private _artistService: ArtistService,
    private _keyboardkeysService: KeyboardkeysService,
    private _paginationService: PaginationService,
    private _headerService: HeaderService,
    private _footerService: FooterService,
    private router: Router,
    private completerService: CompleterService,
    private _appService: AppService,
    private dialogService: DialogService,
    private eventRService: EventReportService,
    private _savedSearchModalComponent: SaveSearchModalComponent,
    private _savedSearchService: SavedSearchService,
    private _savedListService: SavedListService,
    private _savedListComponent: SavedListComponent,
    private _savedListModalComponent: SaveListModalComponent,
    private ItemSvc: ItemsService,
    private notificationSvc: NotificationsService,
    private _savedListBackgroundService: SavedListBackgroundService,
    configuration: ConfigurationLoaderService,
  ) {

    this.URL = configuration.getSettings().apiUrl;

    const objectView = sessionStorage.getItem('objectView')

    if (objectView) {
      sessionStorage.removeItem('objectView')
      if (this.dictionary.OBJECT_VIEW.PRESENTATION.toLowerCase() === objectView) {
        this.onNavigate(objectView)
      }
    }

    router.events.subscribe((val) => {
      // 'viewobjectid' is set when a thumbnail is clicked on 'thumbnail view'.
      if (val instanceof NavigationStart || val instanceof NavigationEnd) {
        sessionStorage.removeItem('viewobjectid');
      }
    });

    setTimeout(() => {
      // Navigate to 'Saved Search' results.
      if (window.location.search.indexOf("savesearchshareid=") >= 0) {
        let savedSearchID;
        let searchCriterias;
        // Get 'Saved Search ID'.
        if (window.location.search.split("savesearchshareid=")[1]) {
          savedSearchID = window.location.search.split("savesearchshareid=")[1];
        }
        if (savedSearchID) {
          // Get 'Saved Search' query params.
          this._savedSearchService.getSharedSavedSearchById(savedSearchID)
            .subscribe(savedSearch => {
              searchCriterias = savedSearch.searchCriterias;
              let query = '';
              for (let i in searchCriterias) {
                query += searchCriterias[i].paramName + "=" + searchCriterias[i].paramValue + "&";
              }
              query = query.charAt(query.length - 1) == '&' ? query.substr(0, query.length - 1) : query;
              // Find 'Objects' matching the 'Saved Search' criteria.
              this.searchProperty('savesearch', query);
            }, resFileError => this.errorMessage = resFileError);
        }
      }
      else if (window.location.search.indexOf("savesearch=") >= 0) {
        // Get 'Saved Search ID'.
        if (window.location.search.split("savesearch=")[1]) {
          let saveSearchID = (window.location.search.split("savesearch=")[1]).split("&")[0];
        }
        // Get query params related to the selected 'Saved Search'.
        let query = window.location.search.substr(window.location.search.indexOf("&"), window.location.search.length);
        // Perform search based on saved search criteria.
        sessionStorage.setItem("MainViewQuery", query);
        this.searchProperty('savesearch', query);
        this.resetSaveListSession();
      }
      else if (window.location.search.indexOf("savelist") >= 0) {
        this.openedSaveList = true;
        this.flag = "savelist";
        document.getElementsByTagName('html')[0].classList.add('loading');
        this.saveListView = true;
        sessionStorage.removeItem('searchparam');
        sessionStorage.setItem('savelistview', 'true');
        // Get property IDs from 'Saved List'.
        if (sessionStorage.getItem('navigatedtosavedlistid')) {
          this.savedListId = sessionStorage.getItem('navigatedtosavedlistid');
        } else {
          var sl = window.location.search.split("savelist=");
          this.savedListId = sl.length ? sl[1] : null;
          sessionStorage.setItem('navigatedtosavedlistid', this.savedListId)
        }
        this.getSavedListProperties(0);
      }
      else if (sessionStorage.getItem("refreshPage") &&
        this.router.url.indexOf('search') > -1) {
        if (!(window.location.pathname.indexOf('thumbnail') > -1)) {
          if (sessionStorage.getItem("MainViewQuerySaveDlist") == "null") {
            this.thumbnailtoobject = true;
            var queryId = sessionStorage.getItem("refreshPage").split('resultQuery=');
            queryId = queryId[1].split('&q=');
            this.queryResults = queryId[0].replace(/%26/g, "&").replace(/%3D/g, "=").replace(/%20/g, " ").replace(/%2C/g, ",").replace(/%252C/g, ",").replace(/%2520/g, "");
            if (queryId[0].indexOf("sort") > -1) {
              if (this.queryResults.indexOf(",asc") > -1) {
                this.sortOrder = 'asc';
              }
              else if (this.queryResults.indexOf(",desc") > -1) {
                this.sortOrder = 'desc';
              }
              this.searchPropertySort('editMode', this.queryResults)
            } else {
              this.searchProperty('editMode', this.queryResults)
            }
          } else {
            this.savedlistclicked = true;
            this.saveListView = true;
            sessionStorage.setItem('savelistview', 'true');
            this.savedListId = sessionStorage.getItem('MainViewQuerySaveDlist');
            if (sessionStorage.getItem("Savedlistsortingquery") != "null" && sessionStorage.getItem("Savedlistsortingquery")) {
              this.saveListSortParam = sessionStorage.getItem("Savedlistsortingquery").trim();
            } else {
              this.saveListSortParam = null;
            }
            this.getSavedListProperties(0);
          }
        }
      } else {
        this.openedSaveList = false;
        sessionStorage.setItem('savelistview', 'false');
        this.ngOnInit();
        if (window.location.search.indexOf("id=") >= 0) {
          this.queParam = window.location.search.split('id=');
          this.queParam = { id: this.queParam[1] };
          this.total_pages = 1;
          this.displayPropertyDetails(this.queParam);
        }
      }

      if (window.location.search.indexOf("savelist") >= 0) {
        this.savedlistclicked = true;
      } else {
        this.savedlistclicked = false;
      }

      if (window.location.search.indexOf("backSearch") >= 0) {
        this.backMode();
        this.thumbnailFlag = true;
        if (sessionStorage.getItem('thumbnailnorecords') === 'true') {
          this.emptyPagination = 'No records found';
          sessionStorage.setItem('thumbnailnorecords', 'false');
        }
      } else if (window.location.search.indexOf("clearSearch") >= 0) {
        this.resetFullForm();
        this.thumbnailFlag = true;
        this.reset = true;
        this.editModeFlag = true;
      }

      if (!(window.location.pathname.indexOf('thumbnail') > -1) && window.location.href.indexOf('resultQuery') > -1) {
        if (sessionStorage.getItem("MainViewQuerySaveDlist") == "null") {
          this.thumbnailtoobject = true;
          sessionStorage.setItem("refreshPage", window.location.search)
          var queryId = window.location.search.split('resultQuery=');
          queryId = queryId[1].split('&q=');
          this.queryResults = queryId[0].replace(/%26/g, "&").replace(/%3D/g, "=").replace(/%20/g, " ").replace(/%2C/g, ",").replace(/%252C/g, ",").replace(/%2520/g, "");
          if (queryId[0].indexOf("sort") > -1) {
            if (this.queryResults.indexOf(",asc") > -1) {
              this.sortOrder = 'asc';
            }
            else if (this.queryResults.indexOf(",desc") > -1) {
              this.sortOrder = 'desc';
            }
            this.searchPropertySort('editMode', this.queryResults)
          } else {
            this.searchProperty('editMode', this.queryResults)
          }
        } else {
          this.savedlistclicked = true;
          this.saveListView = true;
          sessionStorage.setItem('savelistview', 'true');
          this.savedListId = sessionStorage.getItem('MainViewQuerySaveDlist');
          if (sessionStorage.getItem("Savedlistsortingquery") != "null") {
            this.saveListSortParam = sessionStorage.getItem("Savedlistsortingquery").trim();
          } else {
            this.saveListSortParam = null;
          }

          this.getSavedListProperties(0);
        }
      } else {
        this.thumbnailtoobject = false;
      }
      this.keycloakUserName = this.keycloak.tokenParsed && this.keycloak.tokenParsed.preferred_username ? this.keycloak.tokenParsed.preferred_username : null;
    }, 500);
  }

  ngOnInit() {
    this.showProjects = 'main';
    this.categories = [];
    this.countries = [];
    this.presizeNames = [];
    this.editionTypes = [];
    this.subcategories = [];
    this.imageFigures = [];
    this.flagsCountryValue = null;
    window.scrollTo(0, 0);

    if (window.location.href.indexOf('savelist') > -1 || sessionStorage.getItem('savelistview') == "true" || (sessionStorage.getItem('navigatedtosavedlistid') && sessionStorage.getItem('navigatedtosavedlistid').length > 0)) {
      sessionStorage.setItem('savelistview', 'true');
    } else {
      this.resetSaveListSession();
    }
    document.getElementById('uploadtext').style.display = "none";
    if (window.location.pathname.indexOf('object') > -1) {
      this._objectService
        .getCategories()
        .subscribe(categories => {
          categories.forEach((category, index) => {
            if (this.categories.findIndex(item => item.id == category.id) < 0) {
              this.categories.push({ 'name': category.name, 'id': category.id, "description": category.description });
            }
          });
        }, resFileError => this.errorMsg = resFileError);

      this._objectService
        .getEditionSizeTypes()
        .subscribe(Response => {
          for (var i = 0; i < Response.length; i++) {
            this.editionSizeTypes = Response;
          }
        }, resFileError => this.errorMsg = resFileError);

      this._appService
        .getCountries()
        .subscribe(countries => {
          countries.forEach((country, index) => {
            if (this.countries.findIndex(item => item['countryName'] == country.countryName) < 0)
              if (country && country.countryName) {
                this.countries.push({ 'name': country.countryName });
              };
          });
        }, resFileError => this.errorMsg = resFileError);

      // Dynamically set column height to span to the height of the browser window
      this.columnHeight = (document.body.clientHeight - this.navbar[0].clientHeight - 20) > 1200 ? (document.body.clientHeight - this.navbar[0].clientHeight - 20) : 1200;
      this.flag = 'searchMode';
      this.myForm = this.builder.group({
        continent: "",
      });

      this._objectService
        .getPreSizeTypes()
        .subscribe(presizeType => {
          presizeType.forEach((presizeType, index) => {
            if (this.presizeNames.findIndex(item => item.id == presizeType.id) < 0)
              this.presizeNames.push({ 'name': presizeType.name, 'id': presizeType.id, "description": presizeType.description });
          });
        }, resFileError => this.errorMsg = resFileError);

      this._objectService
        .getEditionTypes()
        .subscribe(editionTypes => {
          editionTypes.forEach((editionType, index) => {
            if (this.editionTypes.findIndex(item => item.id == editionType.id) < 0)
              this.editionTypes.push({ 'name': editionType.name, 'id': editionType.id, "description": editionType.description });
          });
        }, resFileError => this.errorMsg = resFileError);
      //Tags Service	   	
      this._objectService
        .getImageFigures()
        .subscribe(imageFigures => {

          imageFigures.forEach((imageFigure, index) => {
            if (this.imageFigures.findIndex(item => item.id == imageFigure.id) < 0)
              this.imageFigures.push({ 'name': imageFigure.name, 'id': imageFigure.id });
            this.tempFigures.push({ 'name': imageFigure.name, 'id': imageFigure.id });
          });
          this.imageFigures.sort(function (a, b) {
            if (a.id < b.id)
              return -1;
            if (a.id > b.id)
              return 1;
            return 0;
          })


        }, resFileError => this.errorMsg = resFileError);
      this._objectService
        .getAllOrientations()
        .subscribe(Response => {
          this.autoOrientations.splice(0, this.autoOrientations && this.autoOrientations.length);
          Response.forEach((orientation, index) => {
            if (this.autoOrientations.findIndex(item => item == orientation) < 0)
              this.autoOrientations.push({ 'name': orientation });
          });
        }, resFileError => this.errorMsg = resFileError);
      this._objectService
        .getAllScales()
        .subscribe(Response => {
          this.autoScales.splice(0, this.autoScales && this.autoScales.length);
          Response.forEach((scale, index) => {
            if (this.autoScales.findIndex(item => item == scale) < 0)
              this.autoScales.push({ 'name': scale });
          });
        }, resFileError => this.errorMsg = resFileError);
      this._objectService
        .getImageGenders()
        .subscribe(Response => {
          this.allGendersList = Response;
          this.nGender.splice(0, this.nGender && this.nGender.length);
          for (var i = 0; i < this.allGendersList.length; i++) {
            if (this.allGendersList[i] && this.allGendersList[i].name) {
              this.nGender.push(this.allGendersList[i].name);
            }
          }
          this.nGender.sort();
        }, resFileError => this.errorMsg = resFileError);
      this._objectService
        .getImageSubjects()
        .subscribe(Response => {
          this.allSubjectsList = Response;
          this.nSubject.splice(0, this.nSubject && this.nSubject.length);
          for (var i = 0; i < this.allSubjectsList.length; i++) {
            if (this.allSubjectsList[i] && this.allSubjectsList[i].name) {
              this.nSubject.push(this.allSubjectsList[i].name);
            }
          }
          this.nSubject.sort();
        }, resFileError => this.errorMsg = resFileError);
      this._objectService
        .getImagePositions()
        .subscribe(Response => {
          this.allPositionsList = Response;
          this.nPosition.splice(0, this.nPosition && this.nPosition.length);
          for (var i = 0; i < this.allPositionsList.length; i++) {
            if (this.allPositionsList[i] && this.allPositionsList[i].name) {
              this.nPosition.push(this.allPositionsList[i].name);
            }
          }
          this.nPosition.sort();
        }, resFileError => this.errorMsg = resFileError);
      this._objectService
        .getMainColors()
        .subscribe(Response => {
          this.allMainColorList = Response;
          this.nMainColor.splice(0, this.nMainColor && this.nMainColor.length);
          for (var i = 0; i < this.allMainColorList.length; i++) {
            if (this.allMainColorList[i] && this.allMainColorList[i].name) {
              this.nMainColor.push(this.allMainColorList[i].name);
            }
          }
          this.nMainColor.sort();
        }, resFileError => this.errorMsg = resFileError);

      // Maps Countries to 'Countries' dropdown in Flags section using ng-completer component
      this.countryService = <any>this.completerService.local(this.countries, 'name', 'name');

      // Maps Categories to 'Category' dropdown using ng-completer component
      this.categoryService = <any>this.completerService.local(this.categories, 'name', 'name');

      // Maps Categories to 'Category' dropdown using ng-completer component
      if (this.subcategories.length > 0) {
        this.subCategoryService = <any>this.completerService.local(this.subcategories, 'name', 'name');
      } else {
        this.subCategoryService = <any>this.completerService.local(this.subcategories, 'name', 'name');
      }

      // Maps Presize Types to 'Presize Type' dropdown using ng-completer component
      this.presizeTypesService = <any>this.completerService.local(this.presizeNames, 'name', 'name');

      // Maps Edition Types to 'Presize Type' dropdown using ng-completer component
      this.editionTypeService = <any>this.completerService.local(this.editionTypes, 'name', 'name');

      // Maps Image Figures to 'Figure' dropdown in Tags using ng-completer component
      this.imageFigureService = <any>this.completerService.local(this.imageFigures, 'name', 'name');

      this.autoOrientationService = <any>this.completerService.local(this.autoOrientations, 'name', 'name');

      this.autoScaleService = <any>this.completerService.local(this.autoScales, 'name', 'name');

      this.autoImageService = <any>this.completerService.local(this.autoImages, 'name', 'name');

    }
    this.objectDetailsForm = this.builder.group({
      selectArtist: new FormControl(""),
      artistData: new FormControl("", [Validators.required]),
      imagePath: new FormControl(""),
      title: new FormControl(""),
      originalTitle: new FormControl(""),
      signature: new FormControl(""),
      medium: new FormControl(""),
      category: new FormControl(),
      subCategory: new FormControl(),
      presizeName: new FormControl(),
      parts: new FormControl("", [Validators.pattern('[0-9]*')]),
      heightCm: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
      widthCm: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
      depthCm: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
      heightIn: new FormControl(""),
      widthIn: new FormControl(""),
      depthIn: new FormControl(""),
      sizeText: new FormControl(""),
      yearStart1: new FormControl("", [Validators.pattern('^[0-9]{4}(([.]{3})[0-9]{4})?$')]),
      yearStart: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
      yearEnd: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
      castYearStart: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
      castYearEnd: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
      yearText: new FormControl(""),
      yearCirca: new FormControl(""),
      castYearCirca: new FormControl(""),
      posthumous: new FormControl(""),
      editionNumber: new FormControl(""),
      editionName: new FormControl(),
      editionSizeTotal: new FormControl(""),
      weight: new FormControl("", [Validators.pattern('^[1-9][0-9]{3}$')]),
      foundry: new FormControl(),
      certificate: new FormControl(),
      signed: new FormControl(),
      stamped: new FormControl(),
      authentication: new FormControl(),
      archiveNumbers: new FormControl(),
      sortCode: new FormControl(),
      flags: this.builder.group({
        flagAuthenticity: new FormControl(),
        flagRestitution: new FormControl(),
        flagSfs: new FormControl(),
        flagCondition: new FormControl,
        flagMedium: new FormControl(),
        flagsCountry: new FormControl()
      }),
      researcherName: new FormControl(),
      researcherNotesDate: new FormControl("", Validators.pattern('^[0-3]?[0-9] [A-Za-z]{3} (?:[0-9]{2})?[0-9]{2}$')),
      researcherNotesText: new FormControl(),
      artistCatalogueRaisoneeAuthor: new FormControl(),
      researchCompleteCheck: new FormControl(false),
      volume: new FormControl(),
      number: new FormControl(),

      tags: this.builder.group({
        imageSubjects: new FormControl(),
        imageFigure: new FormControl(""),
        imageGenders: new FormControl(),
        imagePositions: new FormControl(),
        imageAnimals: new FormControl(),
        imageMainColours: new FormControl(),
        imageText: new FormControl(""),
        expertiseLocation: new FormControl(""),
        expertiseSitters: new FormControl(),
        expertiseSittersFullList: new FormControl(),
        expertiseSitterFName: new FormControl(),
        expertiseSitterLName: new FormControl(),
        autoOrientation: new FormControl(""),
        autoImage: new FormControl(""),
        autoScale: new FormControl(""),
        otherTags: new FormControl(""),
        autoAreaCm: new FormControl(""),
        autoAreaIn: new FormControl(""),
        autoVolume: new FormControl(""),
        autoVolumeUnit: new FormControl("")
      })

    });

    // Form for Edition Size Pop-up
    this.editionSizeForm = this.builder.group({
      editionSizeType: ['Unspecified', Validators.required],
      editionSizeSeries: ['', [this.checkInvalidEditionSize]],
      editionSizeArtistProof: ['', [this.checkInvalidEditionSize]],
      editionSizeTrialProof: ['', [this.checkInvalidEditionSize]],
      editionSizeBonATirerProof: ['', [this.checkInvalidEditionSize]],
      editionSizePrintersProof: ['', [this.checkInvalidEditionSize]],
      editionSizeHorsCommerceProof: ['', [this.checkInvalidEditionSize]],
      editionSizeDedicatedProof: ['', [this.checkInvalidEditionSize]],
      editionSizeMuseumProof: ['', [this.checkInvalidEditionSize]],
      editionSizeEditionOverride: ['', Validators.maxLength],
      editionSizeTotal: [''],
      editionSizeDisplay: [''],
      editionSizeNotes: ['', Validators.maxLength],
    });

  }

  showDialog() {
    this.displayProgressBar = true;
  }

  removeDialog() {
    this.displayProgressBar = false;
  }

  //For tags
  // subject change function
  subjectValChange(subjectVal) {
    if (this.allSubjectsList.length < 1) {
      this.loadSubjects(subjectVal);
    }
    if (subjectVal && this.allSubjectsList.length > 0) {
      this.temp = this.allSubjectsList.filter(function (el) {
        return el.name == subjectVal;
      });
      this.selectedSubject.push(this.temp[0]);
      this.objectDetailsForm.controls['tags'].patchValue({
        'imageSubjects': ''
      });
      this.nSubject = this.nSubject.filter(function (el) {
        return el !== subjectVal;
      });
      this.nSubject.sort();
    }
  }
  //load all subjects
  loadSubjects(subjectValue) {
    this._objectService
      .getImageSubjects()
      .subscribe(Response => {
        this.allSubjectsList = Response;
        this.nSubject.splice(0, this.nSubject && this.nSubject.length);
        for (var i = 0; i < this.allSubjectsList.length; i++) {
          if (this.allSubjectsList[i] && this.allSubjectsList[i].name) {
            this.nSubject.push(this.allSubjectsList[i].name);
          }
        }
        this.nSubject.sort();
        this.subjectValChange(subjectValue);
      }, resFileError => this.errorMsg = resFileError);
  }
  // position change function
  positionValChange(positionVal) {
    if (this.allPositionsList.length < 1) {
      this.loadPositions(positionVal);
    }
    if (positionVal && this.allPositionsList.length > 0) {
      this.temp = this.allPositionsList.filter(function (el) {
        return el.name == positionVal;
      });
      this.selectedPosition.push(this.temp[0]);
      this.objectDetailsForm.controls['tags'].patchValue({
        'imagePositions': ''
      });
      this.nPosition = this.nPosition.filter(function (el) {
        return el !== positionVal;
      });
      this.nPosition.sort();
    }
  }
  valueChanged() {
    this.headerFlag = this.externalId ? 'edit' : this.headerFlag;
  }
  //load all positions
  loadPositions(positionValue) {
    this._objectService
      .getImagePositions()
      .subscribe(Response => {
        this.allPositionsList = Response;
        this.nPosition.splice(0, this.nPosition && this.nPosition.length);
        for (var i = 0; i < this.allPositionsList.length; i++) {
          if (this.allPositionsList[i] && this.allPositionsList[i].name) {
            this.nPosition.push(this.allPositionsList[i].name);
          }
        }
        this.nPosition.sort();
        this.positionValChange(positionValue);
      }, resFileError => this.errorMsg = resFileError);
  }

  // gender change function
  genderValChange(genderVal) {
    if (this.allGendersList.length < 1) {
      this.loadGenders(genderVal);
    }
    if (genderVal && this.allGendersList.length > 0) {
      this.temp = this.allGendersList.filter(function (el) {
        return el.name == genderVal;
      });
      this.selectedGender.push(this.temp[0]);
      this.objectDetailsForm.controls['tags'].patchValue({
        'imageGenders': ''
      });
      this.nGender = this.nGender.filter(function (el) {
        return el !== genderVal;
      });
      this.nGender.sort();
    }
  }
  //load all genders
  loadGenders(genderValue) {
    this._objectService
      .getImageGenders()
      .subscribe(Response => {
        this.allGendersList = Response;
        this.nGender.splice(0, this.nGender && this.nGender.length);
        for (var i = 0; i < this.allGendersList.length; i++) {
          if (this.allGendersList[i] && this.allGendersList[i].name) {
            this.nGender.push(this.allGendersList[i].name);
          }
        }
        this.nGender.sort();
        this.genderValChange(genderValue);
      }, resFileError => this.errorMsg = resFileError);
  }
  // main color change function
  mainColorsValChange(mainColorVal) {
    if (this.allMainColorList.length < 1) {
      this.loadMainColors(mainColorVal);
    }
    if (mainColorVal && this.allMainColorList.length > 0) {
      this.temp = this.allMainColorList.filter(function (el) {
        return el.name == mainColorVal;
      });
      if (this.temp && this.temp.length > 0) {
        if (this.selectedMainColor.length < 3) {
          this.selectedMainColor.push(this.temp[0]);
        } else {
          this.tosatservice.addToastError('You can add only upto 3 main colours.', 'top-right');
        }
      }
      this.objectDetailsForm.controls['tags'].patchValue({
        'imageMainColours': ''
      });
      this.nMainColor = this.nMainColor.filter(function (el) {
        return el !== mainColorVal;
      });
      this.nMainColor.sort();
    }
  }
  //load all main colors
  loadMainColors(colorValue) {
    this._objectService
      .getMainColors()
      .subscribe(Response => {
        this.allMainColorList = Response;
        this.nMainColor.splice(0, this.nMainColor && this.nMainColor.length);
        for (var i = 0; i < this.allMainColorList.length; i++) {
          if (this.allMainColorList[i] && this.allMainColorList[i].name) {
            this.nMainColor.push(this.allMainColorList[i].name);
          }
        }
        this.nMainColor.sort();
        this.mainColorsValChange(colorValue);
      }, resFileError => this.errorMsg = resFileError);
  }

  //THUMBNAIL VIEW NAVIGATION
  onNavigate(location: string) {
    this.presentationView = location === this.dictionary.OBJECT_VIEW.PRESENTATION.toLowerCase();
    this.dropdownView = location;
    if (this.headerFlag === "edit") {
      this.showConfirm(location, 'FLValue');
    } else {
      if (location == 'thumbnail') {
        let q = '';
        // EOS - 2798 If new tab was opened by clicking an item on an existing thumbnail view with search results..
        if (sessionStorage.getItem('searchparam') && sessionStorage.getItem('searchparam').length > 0) {
          let newTabQueryParam = sessionStorage.getItem('searchparam').split('&');
          let query = '';
          for (let index in newTabQueryParam) {
            if (newTabQueryParam[index] != '') {
              query += newTabQueryParam[index] + '&'
            }
            if (parseInt(index) == newTabQueryParam.length - 1) {
              this.queryParams = query;
              this.queryValue = query;
            }
          }
        }
        else if (sessionStorage.getItem('savelistview') && sessionStorage.getItem('savelistview') === 'true') {
          this.saveListView = true;
        } // end of EOS - 2798
        // EOS-1173
        if (this.sortOrder === 'asc') {
          if (this.queryParams && this.queryParams.indexOf('desc') > -1) {
            this.queryParams.replace(/,desc/i, ',asc');
          }
        }
        else if (this.sortOrder === 'desc') {
          if (this.queryParams && this.queryParams.indexOf('asc') > -1) {
            this.queryParams.replace(/,asc/i, ',desc');
          }
        }
        let tempQueryParams = this.queryParams;
        let queryParamList = tempQueryParams && tempQueryParams.length && tempQueryParams.split('&');
        if (this.queryParams) {
          sessionStorage.setItem("pureObject", "false");
        } else {
          if (this.queryParams === "") {
            sessionStorage.setItem("pureObject", "false");
          } else {
            sessionStorage.setItem("pureObject", "true");
          }
        }
        // EOS - 447 - get artist id from search query to retrieve artist catalog raisonnee.
        for (let index in queryParamList) {
          if (queryParamList[index] != '' && (queryParamList && queryParamList[index] && queryParamList[index].split('=')[0] == 'artistIds')) {
            q += 'artistIds:' + queryParamList[index].split('=')[1] + '&';
          }
        }
        if (this.queryParams && this.queryParams.indexOf("&sort=") > -1) {
          this.queryParams = this.queryParams;
        } else {
          this.queryParams = this.queryParams + '&sort=createdDate,desc';
        }
        var resQuery = this.queryValue;
        if (resQuery && resQuery.length) {
          resQuery = resQuery.replace(/artistIds/g, "artist");
        }
        if (this.saveListView) {
          sessionStorage.setItem("pureObject", "false");
          let savedListId = sessionStorage.getItem('navigatedtosavedlistid') ? sessionStorage.getItem('navigatedtosavedlistid') : sessionStorage.getItem('MainViewQuerySaveDlist') ? sessionStorage.getItem('MainViewQuerySaveDlist') : null;
          if (savedListId) {
            resQuery = '';
            resQuery = `savelist`;
            sessionStorage.setItem("MainViewQuerySaveDlist", savedListId);
            if (this.saveListSortParam != null) {
              sessionStorage.setItem("Savedlistsortingquery", this.saveListSortParam + "");
            } else {
              sessionStorage.setItem("Savedlistsortingquery", "null");
            }
          }
        } else {
          this.resetSaveListSession();
          sessionStorage.setItem("MainViewQuerySaveDlist", null);
          sessionStorage.setItem("MainViewQuery", this.queryParams);
          if (this.saveAllQueryParams) {
            sessionStorage.setItem("searchparam", this.saveAllQueryParams);
          } else {
            sessionStorage.setItem("searchparam", '&');
          }
        }
        this.router.navigate(['/thumbnail'], { queryParams: { resultQuery: resQuery, q } });
      } else {
        sessionStorage.setItem('thumbnailselectall', 'false');
        // EOS-1173 Ascending/Descending Sort Order
        if (this.queryParams && this.queryParams.indexOf("&sort=") > -1) {
          this.sorted = true;
          if (this.queryParams.indexOf(",asc") > -1) {
            this.sortOrder = 'asc';
            this.highlightSortIcon('asc');
          }
          else if (this.queryParams.indexOf(",desc") > -1) {
            this.sortOrder = 'desc';
            this.highlightSortIcon('desc');
          }
        }
        else {
          this.sorted = false;
          this.highlightSortIcon(null);
        }
      }
    }
    if (sessionStorage.getItem('selectSort') && sessionStorage.getItem('selectSort').length > 0) {
      this.sorted = true;
    }
  }

  onSelect(event: string) {
    if (event) {
      this.router.navigate(['/thumbnail'], { queryParams: { select: event } });
    } else {
      this.router.navigate(['/thumbnail']);
    }
  }
  // Check Edition Size fields for invalid entry
  checkInvalidEditionSize(control: FormControl) {
    if (control) {
      if (control.value) {
        if (isNaN(parseInt(control.value))) {
          return { "notANumberError": "Please enter ONLY numbers." };
        } else if (parseInt(control.value) < -1 || parseInt(control.value) > 99000) {
          return { "rangeError": "Please enter a number in the range -1 to 99000." };
        } else if (control.value.toString().indexOf('.') > - 1) {
          return { "decimalError": "Please enter only whole numbers. Decimals are not accepted." };
        } else {
          return null;
        }
      }
    }
  }

  // Header Drop-down toggle mode
  toggleFlag: number;
  toggleFunction(flag: number) {
    this.toggleFlag = this._headerService.headerToggle((flag === this.toggleFlag) ? 0 : flag);
  }

  //selectArtist autocomplete function
  nameListFormatter = (data: any) => {
    this.artistMap = "";
    if (data.birthYear && data.deathYear && data.nationality) {
      this.artistMap = `<span><i class='fa fa-birthday-cake' aria-hidden='true'></i> ` + data.birthYear + ` &nbsp;&nbsp;&nbsp;<i class='fa fa-bed' aria-hidden='true'> </i> ` + data.deathYear + ` &nbsp;&nbsp;&nbsp;<i class='fa fa-flag-o' aria-hidden='true'> </i> ` + data.nationality + `</span>`
    } else if (data.birthYear && data.nationality) {
      this.artistMap = `<span><i class='fa fa-birthday-cake' aria-hidden='true'></i> ` + data.birthYear + ` &nbsp;&nbsp;&nbsp;<i class='fa fa-flag-o' aria-hidden='true'> </i> ` + data.nationality + `</span>`
    } else if (data.deathYear && data.nationality) {
      this.artistMap = `<i class='fa fa-bed' aria-hidden='true'> </i> ` + data.deathYear + ` &nbsp;&nbsp;&nbsp;<i class='fa fa-flag-o' aria-hidden='true'> </i> ` + data.nationality + `</span>`
    } else if (data.birthYear && data.deathYear) {
      this.artistMap = `<span><i class='fa fa-birthday-cake' aria-hidden='true'></i> ` + data.birthYear + ` &nbsp;&nbsp;&nbsp;<i class='fa fa-bed' aria-hidden='true'> </i> ` + data.deathYear + ` </span>`
    } else if (data.birthYear) {
      this.artistMap = `<span><i class='fa fa-birthday-cake' aria-hidden='true'></i> ` + data.birthYear;
    }
    let html = `<span class="auto-title">` + data.firstName + `</span><br/>` + this.artistMap;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  }

  //selectSitter autocomplete function
  sitterListFormatter = (data: any) => {
    let html = '';
    if (data.id != '-1') {
      html = `<span class="auto-title">` + data.name + `</span>`;
    } else {
      html = `<span class="auto-title">` + data.name + ` (New Value)</span>`;
    }
    return this._sanitizer.bypassSecurityTrustHtml(html);
  }
  //animal autocomplete function
  animalListFormatter = (data: any) => {
    let html = '';
    if (data.id != '-1') {
      html = `<span class="auto-title">` + data.name + `</span>`;
    } else {
      html = `<span class="auto-title">` + data.name + ` (New Value)</span>`;
    }
    return this._sanitizer.bypassSecurityTrustHtml(html);
  }

  otherTagListFormatter = (data: any) => {
    let html = '';
    if (data.id != '-1') {
      html = `<span class="auto-title" title="` + data.name + `" >` + data.displayName + `</span>`;
    } else {
      html = `<span class="auto-title" title="` + data.name + `">` + data.displayName + ` (New Value)</span>`;
    }
    return this._sanitizer.bypassSecurityTrustHtml(html);
  }

  searchProperty(flag, value: any) {
    if (flag == 'pagination') {
      this.queryValue = this.queryParams;
      this.queryParams = this.queryParams + "&page=" + value;
    } else {
      if (flag == 'savesearch') {
        this.queryParams = value;
        this.reset = false;
      } else {
        if (this.thumbnailtoobject || this.sortingselected) {
          this.queryParams = typeof value === 'object' ? this.getQueryStringValue(value, "search") : value;
        } else {
          this.sortingselected = false;
          this.queryParams = this.getQueryStringValue(value, "search");
        }
      }
      if (this.queryParams.indexOf('&lod=MIN') > -1) {
        var tempQueryParam = this.queryParams.split('&');
        tempQueryParam.forEach((value, index) => {
          if (value == 'lod=MIN')
            tempQueryParam.splice(index);
        });
        this.queryParams = tempQueryParam.join('&');
      }

      this.checkedArtist.forEach(data => {
        this.artistChecked = true;
        this.queryParams += "&artistIds=" + data.id;
      });
      if (flag == "editMode") {
        if (value.artistData == "*") {
          this.queryParams += "&artistIds=*";
        }
      }
      this.queryValue = this.queryParams;
      this.saveAllQueryParams = this.queryParams;
      this.queryParams = this.queryParams ? this.queryParams + '&lod=MIN' : '&lod=MIN';
      // Session Storage of 'search' parameters to create 'saved search' and 'saved list'.
      sessionStorage.removeItem('searchparam');
      sessionStorage.setItem("searchparam", this.saveAllQueryParams);
      // Set to --false-- when object search is performed.
      this.resetSaveListSession();
    }
    document.getElementsByTagName("html")[0].classList.add("loading");
    if (this.saveListView) {
      this.queryParams = this.queryValue;
      if (flag == "pagination") {
        value = parseInt(value) - 1;
        value = (this.curPageValue - 1) % 20;
      }
      else {
        value = 0;
      }
      this.getSavedListProperties(value);
    }
    else {
      this.sortingqueryparam = this.queryParams;
      if (this.thumbnailFlag) {
        this.onNavigate('thumbnail');
      } else {
        this._objectService
          .getProperties(this.queryParams)
          .subscribe(Response => {
            sessionStorage.setItem('totalelements', Response.totalElements);
            this.flag = 'editMode';
            let properties = Response.content;
            this.queryParams = this.queryValue;
            this.total_pages = Response.totalPages;
            this.saveAllTotalElements = Response.totalElements;
            // Display property details in Object page
            if (flag == "pagination") {
              value = parseInt(value) - 1;
              value = (this.curPageValue - 1) % 20;
            } else {
              value = 0;
            }
            if (Response.content.length > 0) {
              this.sortComponent.objectSearched();
              this.total_pages = Response.totalElements;
              this.pageData = Response.content;
              this.singleQuery = { id: Response.content[value].id };
              this.displayPropertyDetails(this.singleQuery);
            } else {
              this.reset = true;
              this.editModeFlag = true;
              this.resetFullForm();
              this.emptyPagination = 'No records found';
              this.appendSearchData()
              this.widthStyle = 115;
            }
          }, resFileError => this.errorMsg = resFileError);
      }
    }
  }
  autocompleListFormatter = (data: any) => {
    let html = "<span>${data.name} ${data.id} </span>";
    return this._sanitizer.bypassSecurityTrustHtml(html);
  }

  //Image Upload
  resizeOptions: ResizeOptions = {
    resizeMaxHeight: 128,
    resizeMaxWidth: 128
  };

  selectedImg(imageResult: ImageResult) {
    this.src = imageResult.resized
      && imageResult.resized.dataURL
      || imageResult.dataURL;
  }

  removeImage(imageResult: ImageResult) {
    this.src = "";
  }

  isNumberKey(event) {
    if (event.keyCode > 31 && (event.keyCode < 48 || event.keyCode > 57)) {
      return false;
    }
    return true;
  }
  duplicateProperty(duplicateVal) {
    if (duplicateVal > 31) {
      this.alertMsg = true;
      //wait 3 Seconds and hide
      setTimeout(function () {
        this.alertMsg = false;
      }.bind(this), 3000);
    } else {
      this.duplicateObject(this.externalId, duplicateVal);
    }
  }

  duplicateObject(flag, value) {
    this._objectService
      .duplicateProp(flag + '?action=duplicate&numberOfDuplicates=' + value)
      .subscribe(Response => {
        this.tosatservice.addToast('Duplicate record created successfully.', 'top-right');
      }, resFileError => this.errorMsg = resFileError);
  }

  deleteConfirm(data: any) {
    if (this.saveListView) {
      this._savedListService.getSavedListById(this.keycloakUserName, sessionStorage.getItem('navigatedtosavedlistid'))
        .subscribe(result => {
          if (result.openSaveList || (this.keycloakUserName === result.user)) {
            if (this.headerFlag === "result" || this.headerFlag === "edit") {
              this.checkObjectItems(this.externalId)
                .subscribe((hasItems: boolean) => {
                  if (hasItems) {
                    this.tosatservice.addToastError(`${this.externalId} ${this.dictionary.NOTIFICATIONS.objectDeleteErrorItems}`, 'top-right');
                  } else {
                    document.getElementsByTagName("body")[0].classList.add("positionFixed");
                    this.dialogService.addDialog(ConfirmComponent, {
                      title: '',
                      message: 'Are you sure you want to delete this object?'
                    }, { backdropColor: 'rgba(220,220,220,0.5)' })
                      .subscribe((isConfirmed) => {
                        document.getElementsByTagName("body")[0].classList.remove("positionFixed");
                        this.confirmResult = isConfirmed;
                        if (this.confirmResult) {
                          this.onSubmit('deleteMode', data);
                        }
                      });
                  }
                });
            } else {
              document.getElementsByTagName("body")[0].classList.add("positionFixed");
              document.getElementsByTagName('html')[0].classList.remove('loading');
              this.dialogService.addDialog(AlertComponent,
                { title: 'Warning...!', message: "This list's status is Closed. Please reach out to " + '"' + result.user + '"' + " to amend list." }, { backdropColor: 'rgba(220,220,220,0.5)' })
                .subscribe((isConfirmed) => {
                  document.getElementsByTagName("body")[0].classList.remove("positionFixed");
                });
            }
          }
        }, error => {
          document.getElementsByTagName('html')[0].classList.remove('loading');
        });
    } else {
      if (this.headerFlag === "result" || this.headerFlag === "edit") {
        document.getElementsByTagName("body")[0].classList.add("positionFixed");
        this.dialogService.addDialog(ConfirmComponent, {
          title: '',
          message: 'Are you sure you want to delete this object?'
        }, { backdropColor: 'rgba(220,220,220,0.5)' })
          .subscribe((isConfirmed) => {
            document.getElementsByTagName("body")[0].classList.remove("positionFixed");
            this.confirmResult = isConfirmed;
            if (this.confirmResult) {
              this.onSubmit('deleteMode', data);
            }
          });
      }
    }
  }

  showConfirm(flag: string, value: any) {
    document.getElementsByTagName("body")[0].classList.add("positionFixed");
    let disposable = this.dialogService.addDialog(ConfirmComponent, {
      title: 'Confirm',
      message: 'There are some unsaved changes made to this record, would you like to save them?'
    })
      .subscribe((isConfirmed) => {
        document.getElementsByTagName('body')[0].classList.remove('positionFixed');
        this.confirmFlag = true;
        if (isConfirmed) {
          if (flag === 'thumbnail') {
            (<HTMLSelectElement>document.getElementById("dropDownView")).selectedIndex = 0;
          }
          this.onSubmit('updateMode', this.objectDetailsForm.value);
          this.confirmFlag = false;
          if (this.saveListView === true) {
            sessionStorage.setItem('savelistview', 'true');
          } else if (this.saveListView === false) {
            this.resetSaveListSession();
          }
        }
        else {
          this.headerFlag = "search";
          switch (flag) {
            case 'backMode':
              this.backMode();
              break;
            case 'defaultMode':
              this.resetFullForm();
              this.reset = true;
              this.editModeFlag = true;
              this.toggleFlag = 0;
              this.resetSaveListSession();
              break;
            case 'addMode':
              this.headerFlag = "create";
              if (this.saveListView === true) {
                sessionStorage.setItem('savelistview', 'true');
              }
              break;
            case 'redirect':
              this.router.navigateByUrl('/');
              this.resetSaveListSession();
              break;
            case 'pageFunction':
              this.pageFunction(value);
              break;
            case 'deleteMode':
              this.headerFlag = "result";
              this.deleteConfirm(this.objectDetailsForm.value);
              break;
            case 'NewObjectModal':
              this.headerFlag = "result";
              this.NewObjectModalPrompt();
              break;
            case 'lastPage':
              if (this.total_pages > 0) {
                this.curPageValue = this.total_pages;
                this.getCurrValue(this.total_pages - 1, 'lastPage');
              }
              break;
            case 'firstPage':
              if (this.total_pages > 0) {
                this.getCurrValue(0, 'firstPage');
              }
              break;
            case 'thumbnail':
              this.onNavigate("thumbnail")
              break;

          }
        }
      });

    if (this.saveListView === false) {
      this.resetSaveListSession();
    }
  }

  confirmTagSave(fieldName: string, value: any) {
    this.isConfirmWindowOpen = true;
    document.getElementsByTagName("body")[0].classList.add("positionFixed");
    let disposable = this.dialogService.addDialog(ConfirmComponent, {
      title: 'Confirm',
      message: 'Are you sure you would like to create the new ' + fieldName + ' "' + value.name + '"?'
    })
      .subscribe((isConfirmed) => {
        this.isConfirmWindowOpen = false;
        document.getElementsByTagName('body')[0].classList.remove('positionFixed');
        if (isConfirmed) {
          if (fieldName == 'Sitter') {
            this.addSitterData(value);
          } else if (fieldName == 'Animal') {
            this.addAnimalData(value.name);
          } else if (fieldName == 'OtherTags') {
            this.addOtherTagData(value.name);
          }
        } else {
          this.objectDetailsForm.controls['tags'].patchValue({
            'expertiseSitterFName': null,
            'expertiseSitterLName': null
          });
        }
      }
      );
  }

  //Submit
  onSubmit(flag: string, value: any) {
    this.artistChecked = false;;
    sessionStorage.setItem("MainQueryFlag", flag);
    sessionStorage.setItem("MainQueryValue", value);
    if (flag == 'saveMode') {
      var artistArr = [];
      this.checkedArtist.forEach(data => {
        artistArr.push(data.id);
      });
      delete value.selectArtist;
      delete value.artistData;
      delete value.archiveNumbers;
      delete value.researcherNotesDate;
      value.artistIds = artistArr;
      value.archiveNumbers = this.archiveNumbers;
      value.editionSizeTypeName = (this.editionSizeForm && this.editionSizeForm.value.editionSizeType) ? this.editionSizeForm.value.editionSizeType : null;
      value.editionSizeTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizeTotal) ? this.editionSizeForm.value.editionSizeTotal : null;
      value.seriesTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizeSeries) ? this.editionSizeForm.value.editionSizeSeries : null;
      value.trialProofTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizeTrialProof) ? this.editionSizeForm.value.editionSizeTrialProof : null;
      value.artistProofTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizeArtistProof) ? this.editionSizeForm.value.editionSizeArtistProof : null;
      value.bonATirerTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizeBonATirerProof) ? this.editionSizeForm.value.editionSizeBonATirerProof : null;
      value.printersProofTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizePrintersProof) ? this.editionSizeForm.value.editionSizePrintersProof : null;
      value.horsCommerceProofTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizeHorsCommerceProof) ? this.editionSizeForm.value.editionSizeHorsCommerceProof : null;
      value.dedicatedProofTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizeDedicatedProof) ? this.editionSizeForm.value.editionSizeDedicatedProof : null;
      value.museumProofTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizeMuseumProof) ? this.editionSizeForm.value.editionSizeMuseumProof : null;
      value.editionOverride = (this.editionSizeForm && this.editionSizeForm.value.editionSizeEditionOverride) ? this.editionSizeForm.value.editionSizeEditionOverride : null;
      value.editionSizeNotes = (this.editionSizeForm && this.editionSizeForm.value.editionSizeNotes) ? this.editionSizeForm.value.editionSizeNotes : null;

      if (this.objectDetailsForm.controls['researcherNotesDate'].value) {
        value.researcherNotesDate = this.getDate(this.objectDetailsForm.controls['researcherNotesDate'].value, "mm-dd-yyyy");
      }
      value.propertyCatalogueRaisonees = this.updatePropertyCatalogRaisonnees('save');
      this.requiredFlag = true;

      if (value.category == "") {
        value.category = null;
      }
      if (value.subCategory == "") {
        value.subCategory = null;
      }
      if (value.presizeName == "") {
        value.presizeName = null;
      }
      if (value.editionName == "") {
        value.editionName = null;
      }
      if (value.editionSizeEditionOverride == "") {
        value.editionSizeEditionOverride = null;
      }
      if (value.imageFigure == "") {
        value.imageFigure = null;
      }
      if (value.imagePosition == "") {
        value.imagePosition = null;
      }
      if (value.imageGender == "") {
        value.imageGender = null;
      }


      if (value.category == "") {
        value.category = null;
      }
      if (value.subCategory == "") {
        value.subCategory = null;
      }
      if (value.presizeName == "") {
        value.presizeName = null;
      }
      if (value.editionName == "") {
        value.editionName = null;
      }
      if (value.editionSizeEditionOverride == "") {
        value.editionSizeEditionOverride = null;
      }

      if ((this.objectDetailsForm.valid || this.requiredFlag) && this.checkedArtist.length) {
        this._objectService
          .addProperty(value)
          .subscribe(Response => {
            this.tosatservice.addToast('Object created successfully.', 'top-right');
            this.displayPropertyDetails(Response);

          }, resFileError => {
            this.tosatservice.addToastError('There was an error while saving your record.', 'top-right');
            this.errorMsg = resFileError;
          });
      } else {
        this.tosatservice.addToastError('There was an error while saving your record.', 'top-right');
      }
    } else if (flag == 'updateMode') {
      var artistArr = [];
      this.checkedArtist.forEach(data => {
        artistArr.push(data.id);
      });
      var animalArr = [];
      this.selectedAnimal.forEach(data => {
        animalArr.push(data.name);
      });
      var sitterArr = [];
      var tempSitterObject = {};
      this.selectedSitter.forEach(data => {
        tempSitterObject = { "id": data.id };
        sitterArr.push(tempSitterObject);
      });
      var otherTagArr = [];
      this.selectedOtherTag.forEach(data => {
        otherTagArr.push(data.name);
      });
      delete value.selectArtist;
      delete value.artistData;
      delete value.tags.expertiseSitters;
      delete value.archiveNumbers;
      delete value.researcherNotesDate;
      delete value.depthIn;
      delete value.heightIn;
      delete value.imagePath;
      delete value.widthIn;
      delete value.tags.imageAnimals;
      delete value.tags.imageSubjects;
      delete value.tags.imageMainColours;
      delete value.tags.otherTags;
      delete value.tags.imagePositions;
      delete value.tags.imageGenders;
      value.artistIds = artistArr;
      value.tags.imageAnimals = animalArr;
      value.tags.expertiseSitters = sitterArr;
      value.tags.otherTags = otherTagArr;
      value.archiveNumbers = this.getAllArchiveNumbers();
      value.editionSizeTypeName = (this.editionSizeForm && this.editionSizeForm.value.editionSizeType) ? this.editionSizeForm.value.editionSizeType : null;
      value.editionSizeTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizeTotal) ? this.editionSizeForm.value.editionSizeTotal : null;
      value.seriesTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizeSeries) ? this.editionSizeForm.value.editionSizeSeries : null;
      value.trialProofTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizeTrialProof) ? this.editionSizeForm.value.editionSizeTrialProof : null;
      value.artistProofTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizeArtistProof) ? this.editionSizeForm.value.editionSizeArtistProof : null;
      value.bonATirerTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizeBonATirerProof) ? this.editionSizeForm.value.editionSizeBonATirerProof : null;
      value.printersProofTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizePrintersProof) ? this.editionSizeForm.value.editionSizePrintersProof : null;
      value.horsCommerceProofTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizeHorsCommerceProof) ? this.editionSizeForm.value.editionSizeHorsCommerceProof : null;
      value.dedicatedProofTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizeDedicatedProof) ? this.editionSizeForm.value.editionSizeDedicatedProof : null;
      value.museumProofTotal = (this.editionSizeForm && this.editionSizeForm.value.editionSizeMuseumProof) ? this.editionSizeForm.value.editionSizeMuseumProof : null;
      value.editionOverride = (this.editionSizeForm && this.editionSizeForm.value.editionSizeEditionOverride) ? this.editionSizeForm.value.editionSizeEditionOverride : null;
      value.editionSizeNotes = (this.editionSizeForm && this.editionSizeForm.value.editionSizeNotes) ? this.editionSizeForm.value.editionSizeNotes : null;

      var mainColorArr = [];
      this.selectedMainColor.forEach(data => {
        mainColorArr.push(data.name);
      });
      value.tags.imageMainColours = mainColorArr;

      var genderArray = [];
      this.selectedGender.forEach(data => {
        genderArray.push(data.name);
      });
      value.tags.imageGenders = genderArray;

      var subArray = [];
      this.selectedSubject.forEach(data => {
        subArray.push(data.name);
      });
      value.tags.imageSubjects = subArray;

      var positionArray = [];
      this.selectedPosition.forEach(data => {
        positionArray.push(data.name);
      });
      value.tags.imagePositions = positionArray;
      if (this.objectDetailsForm.controls['researcherNotesDate'].value) {
        value.researcherNotesDate = this.getDate(this.objectDetailsForm.controls['researcherNotesDate'].value, "mm-dd-yyyy");
      }
      value.propertyCatalogueRaisonees = this.updatePropertyCatalogRaisonnees('update');
      this.editModeFlag = false;
      this.requiredFlag = true;
      if (value.tags.imageFigure == "") {
        value.tags.imageFigure = null;
      }
      if (this.objectDetailsForm.controls['researcherNotesDate'].value) {
        value.researcherNotesDate = this.getDate(this.objectDetailsForm.controls['researcherNotesDate'].value, "mm-dd-yyyy");
      }
      value.propertyCatalogueRaisonees = this.updatePropertyCatalogRaisonnees('update');
      this.editModeFlag = false;
      this.requiredFlag = true;

      if (value.researcherName) {
        value.researcherName = this.keycloak.tokenParsed.preferred_username;
      }

      if ((this.objectDetailsForm.valid || this.requiredFlag) && this.checkedArtist.length) {
        var regex = '^0*([1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9])$';
        if (value && (value && value.yearStart && !value.yearStart.toString().match(regex) ? false : true)
          && (value && value.yearEnd && !value.yearEnd.toString().match(regex) ? false : true)
          && (value && value.castYearStart && !value.castYearStart.toString().match(regex) ? false : true)
          && (value && value.castYearEnd && !value.castYearEnd.toString().match(regex) ? false : true)) {
          if (value.yearStart && !value.yearEnd) {
            this.tosatservice.addToastError('Please enter Year2', 'top-right');
          } else {
            if (value.castYearStart && !value.castYearEnd) {
              this.tosatservice.addToastError('Please enter CastYear2', 'top-right');
            } else {
              if (value.yearStart && value.yearEnd && value.yearEnd < value.yearStart) {
                this.tosatservice.addToastError('Year2 must be greater than or equal to Year1', 'top-right');
              } else {
                if (value.castYearStart && value.castYearEnd && value.castYearEnd < value.castYearStart) {
                  this.tosatservice.addToastError('CastYear2 must be greater than or equal to CastYear1', 'top-right');
                } else {
                  if (!value.yearStart && value.yearEnd) {
                    this.tosatservice.addToastError('Please enter Year1', 'top-right');
                  } else {
                    if (!value.castYearStart && value.castYearEnd) {
                      this.tosatservice.addToastError('Please enter CastYear1', 'top-right');
                    } else {
                      delete value.yearStart1;
                      this._objectService
                        .updateProperty(this.externalId, value)
                        .subscribe(Response => {
                          this.tosatservice.addToast('Object updated successfully.', 'top-right');
                          this.displayPropertyDetails(Response);

                        }, resFileError => {
                          this.tosatservice.addToastError('There was an error while saving your record.', 'top-right');
                          this.errorMsg = resFileError;
                        });
                    }
                  }
                }
              }
            }
          }
        } else {
          this.tosatservice.addToastError('There was an error while saving your record.', 'top-right');
        }
      } else {
        this.tosatservice.addToastError('There was an error while saving your record.', 'top-right');
      }
    } else if (flag == 'deleteMode') {
      this._objectService
        .removeProperty(this.externalId)
        .subscribe(Response => {
          this.tosatservice.addToast('Object deleted successfully.', 'top-right');
          if (this.total_pages === 1 && this.curPageValue - 1 === 0) {
            this.objectDetailsForm.reset();
            this.resetFullForm();
          } else {
            this.total_pages = this.total_pages - 1;
            if (this.curPageValue == 1) {
              this.searchProperty("pagination", 0);
            } else {
              this.pageObject = {
                pageNo: this.pageNo,
                pageNumber: this.pageNumber,
                curPageValue: this.curPageValue,
                pageData: this.pageData,
                flagPage: 'prevPage'
              };
              this.pageObject = this._paginationService.getNextPage(this.pageObject);
              this.curPageValue = this.pageObject.curPageValue;
              this.pageNumber = this.pageObject.pageNo;
              this.pageNo = this.pageObject.pageNo;
              this.searchProperty('pagination', this.pageNumber);

            }

          }
        }, resFileError => {
          this.errorMsg = resFileError;
          this.tosatservice.addToastError('An error occurred while attempting to delete this object.', 'top-right');
        });
    } else {
      if (flag === "editMode") {
        this.flag = "editMode";
      }
      this.current_page = 1;
      this.total_pages = 0;
      this.curPageValue = 1;
      const regex = '^[0-9]{4}(([.]{3})[0-9]{4})?$';
      sessionStorage.setItem('searchData', "null");
      sessionStorage.removeItem('indexSort');
      if (this.checkedArtist.length > 0 || this.getQueryStringValue(value, "search").length > 1 || (this.headerFlag == "search" && value.artistData == "*")
        || (this.selectedSubject && this.selectedSubject.length > 0) || (this.selectedPosition && this.selectedPosition.length > 0)
        || (this.selectedGender && this.selectedGender.length > 0) || (this.selectedMainColor && this.selectedMainColor.length > 0)
        || (this.selectedSitter && this.selectedSitter.length > 0) || (this.selectedAnimal && this.selectedAnimal.length > 0)
        || (this.selectedOtherTag && this.selectedOtherTag.length > 0)) {
        if ((value && value.yearStart1 && value.yearStart1.match(regex)) || (this.headerFlag == "search" && value.yearStart1 == "*")) {
          if (value.yearStart1.length == 4 && value.yearStart1 && !value.yearEnd) {
            this.tosatservice.addToastError('Please enter Year2', 'top-right');
          } else {
            if (value.castYearStart && !value.castYearEnd) {
              this.tosatservice.addToastError('Please enter CastYear2', 'top-right');
            } else {
              if (value.yearStart1.length == 4 && value.yearStart1 && value.yearEnd && value.yearEnd < value.yearStart1) {
                this.tosatservice.addToastError('Year2 must be greater than or equal to Year1', 'top-right');
              } else {
                if (value.castYearStart && value.castYearEnd && value.castYearEnd < value.castYearStart) {
                  this.tosatservice.addToastError('CastYear2 must be greater than or equal to CastYear1', 'top-right');
                } else {
                  if (!value.yearStart && value.yearEnd) {
                    this.tosatservice.addToastError('Please enter Year1', 'top-right');
                  } else {
                    if (!value.castYearStart && value.castYearEnd) {
                      this.tosatservice.addToastError('Please enter CastYear1', 'top-right');
                    } else {
                      this.searchData = value;
                      this.searchData.checkedArtist = this.checkedArtist;
                      if (this.selectedSubject && this.selectedSubject.length > 0) {
                        var subArray = [];
                        for (var i = 0; i < this.selectedSubject.length; i++) {
                          subArray.push(this.selectedSubject[i].name);
                        }
                        this.searchData.tags.imageSubjects = subArray;
                      }
                      if (this.selectedPosition && this.selectedPosition.length > 0) {
                        var posArray = [];
                        for (var i = 0; i < this.selectedPosition.length; i++) {
                          posArray.push(this.selectedPosition[i].name);
                        }
                        this.searchData.tags.imagePositions = posArray;
                      }
                      if (this.selectedGender && this.selectedGender.length > 0) {
                        var genArray = [];
                        for (var i = 0; i < this.selectedGender.length; i++) {
                          genArray.push(this.selectedGender[i].name);
                        }
                        this.searchData.tags.imageGenders = genArray;
                      }
                      if (this.selectedMainColor && this.selectedMainColor.length > 0) {
                        var colourArray = [];
                        for (var i = 0; i < this.selectedMainColor.length; i++) {
                          colourArray.push(this.selectedMainColor[i].name);
                        }
                        this.searchData.tags.imageMainColours = colourArray;
                      }
                      this.searchData.tags.expertiseSittersFullList = this.selectedSitter;
                      if (this.selectedSitter && this.selectedSitter.length > 0) {
                        var sitterArray = [];
                        for (var i = 0; i < this.selectedSitter.length; i++) {
                          sitterArray.push(this.selectedSitter[i].id);
                        }
                        this.searchData.tags.expertiseSitters = sitterArray;
                      }
                      if (this.selectedAnimal && this.selectedAnimal.length > 0) {
                        var animalArray = [];
                        for (var i = 0; i < this.selectedAnimal.length; i++) {
                          animalArray.push(this.selectedAnimal[i].name);
                        }
                        this.searchData.tags.imageAnimals = animalArray;
                      }
                      if (this.selectedOtherTag && this.selectedOtherTag.length > 0) {
                        var otherTagArray = [];
                        for (var i = 0; i < this.selectedOtherTag.length; i++) {
                          otherTagArray.push(this.selectedOtherTag[i].name);
                        }
                        this.searchData.tags.otherTags = otherTagArray;
                      }

                      //		this.searchData.tags.expertiseSitters = (this.selectedSitter && this.selectedSitter.length > 0) ? this.selectedSitter : null;
                      //		this.searchData.tags.imageAnimals = (this.selectedAnimal && this.selectedAnimal.length > 0) ? this.selectedAnimal : null;
                      //		this.searchData.tags.otherTags = (this.selectedOtherTag && this.selectedOtherTag.length > 0) ? this.selectedOtherTag : null;										
                      this.searchData.propertyCatalogueRaisonees = this.searchModePropertyCatalogRaisonnees();
                      sessionStorage.setItem('searchData', JSON.stringify(this.searchData));
                      this.searchthumbnail('editMode', value);
                    }
                  }
                }
              }
            }
          }
        } else if (value && value.yearStart1) {
          this.tosatservice.addToastError('Please enter valid input.', 'top-right');
        } else {
          if (value.yearStart1 && !value.yearEnd) {
            this.tosatservice.addToastError('Please enter Year2', 'top-right');
          } else {
            if (value.castYearStart && !value.castYearEnd) {
              this.tosatservice.addToastError('Please enter CastYear2', 'top-right');
            } else {
              if (value.yearStart1 && value.yearEnd && value.yearEnd < value.yearStart1) {
                this.tosatservice.addToastError('Year2 must be greater than or equal to Year1', 'top-right');
              } else {
                if (value.castYearStart && value.castYearEnd && value.castYearEnd < value.castYearStart) {
                  this.tosatservice.addToastError('CastYear2 must be greater than or equal to CastYear1', 'top-right');
                } else {
                  if (!value.yearStart && value.yearEnd) {
                    this.tosatservice.addToastError('Please enter Year1', 'top-right');
                  } else {
                    if (!value.castYearStart && value.castYearEnd) {
                      this.tosatservice.addToastError('Please enter CastYear1', 'top-right');
                    } else {
                      this.searchData = value;
                      this.searchData.checkedArtist = this.checkedArtist;
                      if (this.selectedSubject && this.selectedSubject.length > 0) {
                        var subArray = [];
                        for (var i = 0; i < this.selectedSubject.length; i++) {
                          subArray.push(this.selectedSubject[i].name);
                        }
                        this.searchData.tags.imageSubjects = subArray;
                      }
                      if (this.selectedPosition && this.selectedPosition.length > 0) {
                        var posArray = [];
                        for (var i = 0; i < this.selectedPosition.length; i++) {
                          posArray.push(this.selectedPosition[i].name);
                        }
                        this.searchData.tags.imagePositions = posArray;
                      }
                      if (this.selectedGender && this.selectedGender.length > 0) {
                        var genArray = [];
                        for (var i = 0; i < this.selectedGender.length; i++) {
                          genArray.push(this.selectedGender[i].name);
                        }
                        this.searchData.tags.imageGenders = genArray;
                      }
                      if (this.selectedMainColor && this.selectedMainColor.length > 0) {
                        var colourArray = [];
                        for (var i = 0; i < this.selectedMainColor.length; i++) {
                          colourArray.push(this.selectedMainColor[i].name);
                        }
                        this.searchData.tags.imageMainColours = colourArray;
                      }
                      this.searchData.tags.expertiseSittersFullList = this.selectedSitter;
                      if (this.selectedSitter && this.selectedSitter.length > 0) {
                        var sitterArray = [];
                        for (var i = 0; i < this.selectedSitter.length; i++) {
                          sitterArray.push(this.selectedSitter[i].id);
                        }
                        this.searchData.tags.expertiseSitters = sitterArray;
                      }
                      if (this.selectedAnimal && this.selectedAnimal.length > 0) {
                        var animalArray = [];
                        for (var i = 0; i < this.selectedAnimal.length; i++) {
                          animalArray.push(this.selectedAnimal[i].name);
                        }
                        this.searchData.tags.imageAnimals = animalArray;
                      }
                      if (this.selectedOtherTag && this.selectedOtherTag.length > 0) {
                        var otherTagArray = [];
                        for (var i = 0; i < this.selectedOtherTag.length; i++) {
                          otherTagArray.push(this.selectedOtherTag[i].name);
                        }
                        this.searchData.tags.otherTags = otherTagArray;
                      }
                      //		this.searchData.tags.expertiseSitters = (this.selectedSitter && this.selectedSitter.length > 0) ? this.selectedSitter : null;
                      //		this.searchData.tags.imageAnimals = (this.selectedAnimal && this.selectedAnimal.length > 0) ? this.selectedAnimal : null;
                      //		this.searchData.tags.otherTags = (this.selectedOtherTag && this.selectedOtherTag.length > 0) ? this.selectedOtherTag : null;										
                      this.searchData.propertyCatalogueRaisonees = this.searchModePropertyCatalogRaisonnees();
                      sessionStorage.setItem('searchData', JSON.stringify(this.searchData));
                      this.searchthumbnail('editMode', value);
                    }
                  }
                }
              }
            }
          }
        }
      } else {
        this.tosatservice.addToastError('Please specify at least one criteria to perform a search.', 'top-right');
      }
    }
  }

  searchthumbnail(flag, value) {
    if (flag == 'savesearch') {
      this.queryParams = value;
      this.reset = false;
    } else {
      if (this.thumbnailtoobject || this.sortingselected) {
        this.queryParams = typeof value === 'object' ? this.getQueryStringValue(value, "search") : value;
      } else {
        this.sortingselected = false;
        this.queryParams = this.getQueryStringValue(value, "search");
      }
    }
    this.checkedArtist.forEach(data => {
      this.artistChecked = true;
      this.queryParams += "&artistIds=" + data.id;
    });
    if (flag == "editMode") {
      if (value.artistData == "*") {
        this.queryParams += "&artistIds=*";
      }
    }
    if (value.propertyCatalogueRaisonees.length > 0) {
      value.propertyCatalogueRaisonees.forEach(element => {
        if (element.number) {
          this.queryParams += "&numbers=" + element.number + ":crId=" + element.artistCatalogueRaisoneeId;
        }
      });
    }
    this.queryValue = this.queryParams;
    this.saveAllQueryParams = this.queryParams;
    this.queryParams = this.queryParams ? this.queryParams + '&lod=MIN' : '&lod=MIN';
    // Session Storage of 'search' parameters to create 'saved search' and 'saved list'.
    sessionStorage.removeItem('searchparam');
    sessionStorage.setItem("searchparam", this.saveAllQueryParams);
    // Set to --false-- when object search is performed.
    this.resetSaveListSession();

    if (this.saveListView) {
      this.queryParams = this.queryValue;
      if (flag == "pagination") {
        value = parseInt(value) - 1;
        value = (this.curPageValue - 1) % 20;
      }
      else {
        value = 0;
      }
      this.getSavedListProperties(value);
    } else {
      document.getElementsByTagName('html')[0].classList.add('loading');
      this._objectService
        .getProperties(this.queryParams)
        .subscribe(Response => {
          if (Response.content.length) {
            this.onNavigate('thumbnail');
          } else {
            document.getElementsByTagName('html')[0].classList.remove('loading');
            this.headerFlag = "search";
            this.emptyPagination = 'No record found';
            this.widthStyle = 115;
          }
        }, resError => {
          document.getElementsByTagName('html')[0].classList.remove('loading');
          this.tosatservice.addToastError('Please enter valid input.', 'top-right');
        });
    }
  }

  @HostListener('window:input', ['$event'])
  onInput(e) {
    if (e && ((e.target && (JSON.stringify(e.target.innerHTML.toString()).indexOf('thumbViewSelect') > -1 || JSON.stringify(e.target.innerHTML.toString()).indexOf('selectSortValue') > -1 || JSON.stringify(e.target.classList).indexOf('currentValueMode') > -1)) || (e.srcElement && e.srcElement.form && JSON.stringify(e.srcElement.form.classList).indexOf('noModeChange') > -1))) {
    } else {
      this.headerFlag = this.externalId ? 'edit' : this.headerFlag;
    }
  }

  onChangeFunction(event) {
    // Artist Name
    if (!event) {
      if (this.objectDetailsForm.controls['artistData'].value != null && this.objectDetailsForm.controls['artistData'].value != '') {
        this.headerFlag = this.externalId ? 'edit' : this.headerFlag;
      }
    }
    // 'ng2-completer' drop down value change
    else if (event.originalObject && event.originalObject.name) {
      this.headerFlag = this.externalId ? 'edit' : this.headerFlag;
    }
    // Check if 'left', 'right' arrow keys
    else if (event.which != 18 && event.which != 20 && event.which != 33 && event.which != 34 && event.which != 35 && event.which != 36 && event.which != 37 && event.which != 39 && event.which != 45 && event.keyCode != 17 && event.keyCode != 27 && event.keyCode != 145 && event.keyCode != 91 && event.keyCode != 93) {
      if (event === null) {
        this.headerFlag = this.externalId ? 'edit' : this.headerFlag;
      }
      // Text field
      else if ((event.target.type == 'search') && event.which != 38 && event.which != 40 && event.keyCode != 9 && event.which != 13 && event.keyCode != 16) {
        this.headerFlag = this.externalId ? 'edit' : this.headerFlag;
      }
      // Number field
      else if (event.target.type == 'number' && event.which !== 0 && !event.ctrlKey && !event.metaKey && !event.altKey
        && event.keyCode != 9 && event.which != 13 && event.keyCode != 16 && ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 46 || event.type == 'change')) {
        this.headerFlag = this.externalId ? 'edit' : this.headerFlag;
      }
      // Radio & checkbox
      else if (event.target.type == 'radio' || event.target.type == 'checkbox') {
        this.headerFlag = this.externalId ? 'edit' : this.headerFlag;
      }
      // Remove Artist Name
      else if (event.type == 'click') {
        this.headerFlag = this.externalId ? 'edit' : this.headerFlag;
      }
    }
  }


  resetAreaVolume(fieldName: any) {
    this.objectDetailsForm.value.tags.autoVolume = null;
    this.objectDetailsForm.value.tags.autoVolumeUnit = null;
    if (fieldName == 'autoAreaCm') {
      this.objectDetailsForm.controls['tags'].patchValue({
        'autoAreaIn': null
      });
      if (this.objectDetailsForm.value.tags.autoAreaCm && !isNaN(this.objectDetailsForm.value.tags.autoAreaCm)) {
        this.objectDetailsForm.value.tags.autoVolume = this.objectDetailsForm.value.tags.autoAreaCm.trim();
        this.objectDetailsForm.value.tags.autoVolumeUnit = 'cm';
      } else if (this.objectDetailsForm.value.tags.autoAreaCm && this.objectDetailsForm.value.tags.autoAreaCm.trim() == '*') {
        this.objectDetailsForm.value.tags.autoVolume = '*';
        this.objectDetailsForm.value.tags.autoVolumeUnit = 'cm';
      }
    } else if (fieldName == 'autoAreaIn') {
      this.objectDetailsForm.controls['tags'].patchValue({
        'autoAreaCm': null
      });
      if (this.objectDetailsForm.value.tags.autoAreaIn && !isNaN(this.objectDetailsForm.value.tags.autoAreaIn)) {
        this.objectDetailsForm.value.tags.autoVolume = this.objectDetailsForm.value.tags.autoAreaIn.trim();
        this.objectDetailsForm.value.tags.autoVolumeUnit = 'in';
      } else if (this.objectDetailsForm.value.tags.autoAreaIn && this.objectDetailsForm.value.tags.autoAreaIn.trim() == '*') {
        this.objectDetailsForm.value.tags.autoVolume = '*';
        this.objectDetailsForm.value.tags.autoVolumeUnit = 'in';
      }
    }
  }


  flagMode(value: any) {
    if (value == 'addMode') {
      this.flag = "addMode";
      this.editModeFlag = false;
      this.emptyPagination = '';
      this.widthStyle = 85;
      this.resetFullForm();
      this.headerFlag = 'create';
    }
  }

  getQueryStringValue(queryStr, action) {
    let queryStringParams = <any>[];
    let query: string = "";
    let artistIds = [];
    let flags = [];
    for (let param in queryStr) {
      if (this.archiveNumbers.length > 0 && query.indexOf('&archiveNumbers=') === -1) {
        let archiveNumbers = "";
        for (let i in this.archiveNumbers) {
          if (this.archiveNumbers[i].archiveNumber.length === 0 || archiveNumbers.indexOf('archiveNumbers=' + this.archiveNumbers[i].archiveNumber) > -1) {
            continue;
          }
          archiveNumbers += "archiveNumbers=" + this.archiveNumbers[i].archiveNumber + "&";
        }
        query += "&" + archiveNumbers;
      }
      if (param == 'authentication' && queryStr['authentication']) {
        query += "&authentication=true";
      }
      if (param == 'castYearCirca' && queryStr['castYearCirca']) {
        query += "&castYearCirca=true";
      }
      if (param == 'castYearStart' && queryStr['castYearStart']) {
        query += "&castYearStart=" + this.objectDetailsForm.controls['castYearStart'].value;
      }
      if (param == 'castYearEnd' && queryStr['castYearEnd']) {
        query += "&castYearEnd=" + this.objectDetailsForm.controls['castYearEnd'].value;
      }
      if (param == 'subCategory' && queryStr['subCategory']) {
        query += "&category=" + queryStr['subCategory'];
      }
      else if (param == 'category' && queryStr['category']) {
        query += "&category=" + queryStr['category'];
      }
      if (param == 'certificate' && queryStr['certificate']) {
        query += "&certificate=true";
      }
      if (param == 'depthCm' && queryStr['depthCm']) {
        query += "&depthCm=" + this.objectDetailsForm.controls['depthCm'].value;
      }
      if (param == 'editionNumber' && queryStr['editionNumber'] && this.objectDetailsForm.controls['editionNumber'].value.trim()) {
        query += "&editionNumber=" + this.objectDetailsForm.controls['editionNumber'].value.trim();
      }
      if (param == 'editionSizeTotal' && queryStr['editionSizeTotal'] && this.objectDetailsForm.controls['editionSizeTotal'].value.trim()) {
        query += "&editionSizeTotal=" + this.objectDetailsForm.controls['editionSizeTotal'].value.trim();
      }
      if (param == 'foundry' && queryStr['foundry'] && this.objectDetailsForm.controls['foundry'].value.trim()) {
        query += "&foundry=" + this.objectDetailsForm.controls['foundry'].value.trim();
      }
      if (param == 'heightCm' && queryStr['heightCm']) {
        query += "&heightCm=" + this.objectDetailsForm.controls['heightCm'].value;
      }
      if (param == 'medium' && queryStr['medium'] && this.objectDetailsForm.controls['medium'].value.trim()) {
        query += "&medium=" + this.objectDetailsForm.controls['medium'].value.trim();
      }
      if (param == 'originalTitle' && queryStr['originalTitle'] && this.objectDetailsForm.controls['originalTitle'].value.trim()) {
        query += "&originalTitle=" + this.objectDetailsForm.controls['originalTitle'].value.trim();
      }
      if (param == 'posthumous' && queryStr['posthumous']) {
        query += "&posthumous=true";
      }
      if (param == 'parts' && queryStr['parts']) {
        query += "&parts=" + this.objectDetailsForm.controls['parts'].value;
      }
      if (param == 'presizeName' && queryStr['presizeName'] && this.objectDetailsForm.controls['presizeName'].value.trim()) {
        query += "&presizeName=" + this.objectDetailsForm.controls['presizeName'].value.trim();
      }
      if (param == 'sizeText' && queryStr['sizeText'] && this.objectDetailsForm.controls['sizeText'].value.trim()) {
        query += "&sizeText=" + this.objectDetailsForm.controls['sizeText'].value.trim();
      }
      if (param == 'editionName' && queryStr['editionName'] && this.objectDetailsForm.controls['editionName'].value.trim()) {
        query += "&editionName=" + this.objectDetailsForm.controls['editionName'].value.trim();
      }
      if (param == 'flags' && queryStr.flags.flagAuthenticity) {
        query += "&flags.flagAuthenticity=true";
      }
      if (param == 'flags' && queryStr.flags.flagCondition) {
        query += "&flags.flagCondition=true";
      }
      if (param == 'flags' && queryStr.flags.flagMedium) {
        query += "&flags.flagMedium=true";
      }
      if (param == 'flags' && queryStr.flags.flagRestitution) {
        query += "&flags.flagRestitution=true";
      }
      if (param == 'flags' && queryStr.flags.flagSfs) {
        query += "&flags.flagSfs=true";
      }
      if (param == 'flags' && queryStr.flags.flagsCountry) {
        query += "&flags.flagsCountry=" + queryStr.flags.flagsCountry;
      }
      if (param == 'researchCompleteCheck' && queryStr.researchCompleteCheck) {
        query += "&researchCompleteCheck=" + queryStr.researchCompleteCheck;
      }
      if (param == 'researcherName' && queryStr['researcherName'] && this.objectDetailsForm.controls['researcherName'].value.trim()) {
        query += "&researcherName=" + this.objectDetailsForm.controls['researcherName'].value.trim();
      }
      if (param == 'researcherNotesDate' && queryStr['researcherNotesDate']) {
        if (!((this.headerFlag == "search" || this.headerFlag == "result") && queryStr['researcherNotesDate'] == "*")) {
          query += "&researcherNotesDate=" + this.getDate(this.objectDetailsForm.controls['researcherNotesDate'].value, "mm-dd-yyyy");
        } else {
          query += "&researcherNotesDate=*";
        }
      }
      if (param == 'signature' && queryStr['signature'] && this.objectDetailsForm.controls['signature'].value.trim()) {
        query += "&signature=" + this.objectDetailsForm.controls['signature'].value.trim();
      }
      if (param == 'signed' && queryStr['signed']) {
        query += "&signed=true";
      }
      if (param == 'sortCode' && queryStr['sortCode'] && this.objectDetailsForm.controls['sortCode'].value.trim()) {
        query += "&sortCode=" + this.objectDetailsForm.controls['sortCode'].value.trim();
      }
      if (param == 'stamped' && queryStr['stamped']) {
        query += "&stamped=true";
      }
      if (param == 'title' && queryStr['title'] && this.objectDetailsForm.controls['title'].value.trim()) {
        query += "&title=" + this.objectDetailsForm.controls['title'].value.trim();
      }
      if (param == 'artistCatalogueRaisoneeAuthor' && queryStr['artistCatalogueRaisoneeAuthor'] && this.objectDetailsForm.controls['artistCatalogueRaisoneeAuthor'].value.trim()) {
        query += "&artistCatalogueRaisoneeAuthor=" + this.objectDetailsForm.controls['artistCatalogueRaisoneeAuthor'].value.trim();
      }
      if (param == 'volume' && queryStr['volume'] && this.objectDetailsForm.controls['volume'].value.trim()) {
        query += "&volume=" + this.objectDetailsForm.controls['volume'].value.trim();
      }
      if (param == 'number' && queryStr['number'] && this.objectDetailsForm.controls['number'].value.trim()) {
        query += "&numbers=" + this.objectDetailsForm.controls['number'].value.trim();
      }
      if (param == 'weight' && queryStr['weight']) {
        query += "&weight=" + this.objectDetailsForm.controls['weight'].value;
      }
      if (param == 'widthCm' && queryStr['widthCm'] && this.objectDetailsForm.controls['widthCm'].value) {
        query += "&widthCm=" + this.objectDetailsForm.controls['widthCm'].value;
      }
      if (param == 'yearCirca' && queryStr['yearCirca']) {
        query += "&yearCirca=true";
      }
      if (param == 'yearEnd' && queryStr['yearEnd']) {
        query += "&yearEnd=" + this.objectDetailsForm.controls['yearEnd'].value;
      }
      if (param == 'yearStart1' && queryStr['yearStart1'] && this.objectDetailsForm.controls['yearStart1'].value.trim()) {
        query += "&yearStart=" + this.objectDetailsForm.controls['yearStart1'].value.trim();
      }
      if (param == 'yearText' && queryStr['yearText'] && this.objectDetailsForm.controls['yearText'].value.trim()) {
        query += "&yearText=" + this.objectDetailsForm.controls['yearText'].value.trim();
      }
      if (param == 'tags' && queryStr.tags.expertiseLocation) {
        query += "&tags.expertiseLocation=" + queryStr.tags.expertiseLocation;
      }
      if (param == 'tags' && queryStr.tags.expertiseSitters) {
        query += "&tags.expertiseSitters=" + queryStr.tags.expertiseSitters;
      }
      if (param == 'tags' && queryStr.tags.imageAnimals) {
        query += "&tags.imageAnimals=" + queryStr.tags.imageAnimals;
      }
      if (param == 'tags' && queryStr.tags.imageSubjects) {
        query += "&tags.imageSubjects=" + queryStr.tags.imageSubjects;
      }
      if (param == 'tags' && queryStr.tags.imageGenders) {
        query += "&tags.imageGenders=" + queryStr.tags.imageGenders;
      }
      if (param == 'tags' && queryStr.tags.imageFigure) {
        query += "&tags.imageFigure=" + queryStr.tags.imageFigure;
      }
      if (param == 'tags' && queryStr.tags.imagePositions) {
        query += "&tags.imagePositions=" + queryStr.tags.imagePositions;
      }
      if (param == 'tags' && queryStr.tags.imageMainColours) {
        query += "&tags.imageMainColours=" + queryStr.tags.imageMainColours;
      }
      if (param == 'tags' && queryStr.tags.imageText) {
        query += "&tags.imageText=" + queryStr.tags.imageText;
      }
      if (param == 'tags' && queryStr.tags.otherTags) {
        query += "&tags.otherTags=" + queryStr.tags.otherTags;
      }
      if (param == 'tags' && queryStr.tags.autoOrientation) {
        query += "&tags.autoOrientation=" + queryStr.tags.autoOrientation.toUpperCase();
      }
      if (param == 'tags' && queryStr.tags.autoScale) {
        query += "&tags.autoScale=" + queryStr.tags.autoScale.toUpperCase();
      }
      if (param == 'tags' && queryStr.tags.autoImage) {
        if (queryStr.tags.autoImage.toLowerCase() == "yes") {
          query += "&tags.autoImage=true";
        } else if (queryStr.tags.autoImage.toLowerCase() == "no") {
          query += "&tags.autoImage=false";
        }
      }
      if (param == 'tags' && queryStr.tags.autoVolume) {
        query += ("&tags.autoVolume=" + queryStr.tags.autoVolume + "&tags.autoVolumeUnit=" + queryStr.tags.autoVolumeUnit);
      }
    }
    switch (action) {
      case "search":
        return query;
    }
  }

  //CreatedBy and UpdateBy userName
  userName: FooterUser[];
  public footerName: FooterUser;
  @Output() changed = new EventEmitter<FooterUser>();
  onFullPathChange(event) {
    if (this.externalId && event.indexOf('properties') > -1) {
      this.fullPath = event;
    }
  }
  // Display property details in Object page
  displayPropertyDetails(properties) {
    if (properties.id) {
      let property = null;
      this.objectId = properties.id;
      this._objectService
        .getProperty(properties.id)
        .subscribe(Response => {
          this.objectDetailsForm.controls['artistData'].setValue('');
          let property = Response;
          this.currentProperty = property;
          this.headerFlag = 'result';
          this.externalId = Response.id;
          if ((<HTMLInputElement>document.getElementsByClassName('dz-hidden-input')[0]))
            (<HTMLInputElement>document.getElementsByClassName('dz-hidden-input')[0]).disabled = false;
          if (Response.imagePath) {
            var d = new Date();
            var n = d.getTime();
            this.fullPath = this.URL + "/properties/" + this.externalId + "/images/default?" + n;
            document.getElementById('deleteimagebutton').style.display = "block";
          } else {
            document.getElementById('deleteimagebutton').style.display = "none"
            var urlValue = window.location.href.indexOf('object');
            var newURL = window.location.href.substring(0, urlValue);
            this.fullPath = newURL + "/assets/images/default.png";
            document.getElementById('deleteimagebutton').style.display = "none";
          }
          this.dropuploadComponent.showDiv(this.externalId);
          window.history.replaceState(null, null, "object?id=" + Response.id);
          this.checkedArtist = [];
          this.artistName.splice(0, this.artistName.length);
          Response.artists.forEach(data => {
            var bd = ((data.birthYear && data.deathYear) ? "(" + data.birthYear + "-" + data.deathYear + ")" : (data.birthYear ? "(b." + data.birthYear + ")" : ''));
            this.artistName.push({
              firstName: (data.firstName ? data.firstName + ' ' : '') + data.lastName,
              display: data.display ? data.display : ((data.firstName ? data.firstName + ' ' : '') + data.lastName + bd),
              birthYear: data.birthYear,
              deathYear: data.deathYear,
              id: data.id
            });
          });
          this.checkedArtist = this.checkedArtist.concat(this.artistName);

          if (property && property.imagePath) {
            if (this.URL + "/properties/" + properties.id + "/images/default") {
              this.src = "/property-service/v1/properties/" + properties.id + "/images/default";
              this.src = this.URL + "/properties/" + properties.id + "/images/default";
              this.srcFlag = true;
            }
          }
          this.objectDetailsForm.controls['title'].setValue(property.title ? property.title : null);
          this.objectDetailsForm.controls['originalTitle'].setValue(property.originalTitle ? property.originalTitle : null);
          this.objectDetailsForm.controls['signature'].setValue(property.signature ? property.signature : null);
          this.objectDetailsForm.controls['medium'].setValue(property.medium ? property.medium : null);
          this.objectDetailsForm.controls['category'].setValue(property.parentCategory ? property.parentCategory.name : null);
          this.objectDetailsForm.controls['subCategory'].setValue(property.subCategory ? property.subCategory.name : null);
          this.objectDetailsForm.controls['presizeName'].setValue(property.presizeType ? property.presizeType.name : null);
          this.objectDetailsForm.controls['parts'].setValue(property.parts ? property.parts : null);
          this.objectDetailsForm.controls['heightCm'].setValue(property.heightCm ? property.heightCm : null);
          this.objectDetailsForm.controls['widthCm'].setValue(property.widthCm ? property.widthCm : null);
          this.objectDetailsForm.controls['depthCm'].setValue(property.depthCm ? property.depthCm : null);
          this.objectDetailsForm.controls['heightIn'].setValue(property.heightIn ? property.heightIn : null);
          this.objectDetailsForm.controls['widthIn'].setValue(property.widthIn ? property.widthIn : null);
          this.objectDetailsForm.controls['depthIn'].setValue(property.depthIn ? property.depthIn : null);
          if (property.heightCm) {
            this.CmToInViceVersaChange('heightIn', 'CmToIn', property.heightCm);
          }
          if (property.widthCm) {
            this.CmToInViceVersaChange('widthIn', 'CmToIn', property.widthCm);
          }
          if (property.depthCm) {
            this.CmToInViceVersaChange('depthIn', 'CmToIn', property.depthCm);
          }
          this.objectDetailsForm.controls['sizeText'].setValue(property.sizeText ? property.sizeText : null);
          this.objectDetailsForm.controls['yearStart'].setValue(property.yearStart ? property.yearStart : null);
          this.objectDetailsForm.controls['yearEnd'].setValue(property.yearEnd ? property.yearEnd : null);
          this.objectDetailsForm.controls['castYearStart'].setValue(property.castYearStart ? property.castYearStart : null);
          this.objectDetailsForm.controls['castYearEnd'].setValue(property.castYearEnd ? property.castYearEnd : null);
          this.objectDetailsForm.controls['yearCirca'].setValue(property.yearCirca ? property.yearCirca : null);
          this.objectDetailsForm.controls['castYearCirca'].setValue(property.castYearCirca ? property.castYearCirca : null);
          this.objectDetailsForm.controls['posthumous'].setValue(property.posthumous ? property.posthumous : null);
          this.objectDetailsForm.controls['yearText'].setValue(property.yearText ? property.yearText : null);
          this.objectDetailsForm.controls['editionNumber'].setValue(property.editionNumber ? property.editionNumber : null);
          this.objectDetailsForm.controls['editionName'].setValue(property.editionName ? property.editionName : null);
          this.objectDetailsForm.controls['sortCode'].setValue(property.sortCode ? property.sortCode : null);
          this.objectDetailsForm.controls['certificate'].setValue(property.certificate ? property.certificate : null);
          this.objectDetailsForm.controls['signed'].setValue(property.signed ? property.signed : null);
          this.objectDetailsForm.controls['stamped'].setValue(property.stamped ? property.stamped : null);
          this.objectDetailsForm.controls['authentication'].setValue(property.authentication ? property.authentication : null);
          this.objectDetailsForm.controls['flags'].setValue({
            'flagAuthenticity': (property && property.flags && property.flags.flagAuthenticity) ? property.flags.flagAuthenticity : null,
            'flagRestitution': (property && property.flags && property.flags.flagRestitution) ? property.flags.flagRestitution : null,
            'flagSfs': (property && property.flags && property.flags.flagSfs) ? property.flags.flagSfs : null,
            'flagCondition': (property && property.flags && property.flags.flagCondition) ? property.flags.flagCondition : null,
            'flagMedium': (property && property.flags && property.flags.flagMedium) ? property.flags.flagMedium : null,
            'flagsCountry': (property && property.flags && property.flags.flagsCountry) ? property.flags.flagsCountry : null
          });
          this.flagsCountryValue = (property && property.flags && property.flags.flagsCountry) ? property.flags.flagsCountry : null;
          // Set Edition Size values
          this.editionSizeForm.controls['editionSizeType'].setValue(property.editionSizeTypeName ? property.editionSizeTypeName : "Unspecified");
          this.editionSizeForm.controls['editionSizeSeries'].setValue(property.seriesTotal ? property.seriesTotal : null);
          this.editionSizeForm.controls['editionSizeArtistProof'].setValue(property.artistProofTotal ? property.artistProofTotal : null);
          this.editionSizeForm.controls['editionSizeTrialProof'].setValue(property.trialProofTotal ? property.trialProofTotal : null);
          this.editionSizeForm.controls['editionSizeBonATirerProof'].setValue(property.bonATirerTotal ? property.bonATirerTotal : null);
          this.editionSizeForm.controls['editionSizePrintersProof'].setValue(property.printersProofTotal ? property.printersProofTotal : null);
          this.editionSizeForm.controls['editionSizeHorsCommerceProof'].setValue(property.horsCommerceProofTotal ? property.horsCommerceProofTotal : null);
          this.editionSizeForm.controls['editionSizeDedicatedProof'].setValue(property.dedicatedProofTotal ? property.dedicatedProofTotal : null);
          this.editionSizeForm.controls['editionSizeMuseumProof'].setValue(property.museumProofTotal ? property.museumProofTotal : null);
          this.editionSizeForm.controls['editionSizeEditionOverride'].setValue(property.editionOverride ? property.editionOverride : null);
          this.editionSizeForm.controls['editionSizeNotes'].setValue(property.editionSizeNotes ? property.editionSizeNotes : null);
          this.editionSizeForm.controls['editionSizeDisplay'].setValue(this.objectDetailsForm.value.editionSizeTotal ? this.objectDetailsForm.value.editionSizeTotal : null);
          this.editionSizeForm.controls['editionSizeTotal'].setValue(((parseInt(property.seriesTotal) ? parseInt(property.seriesTotal) : 0) + (parseInt(property.artistProofTotal) ? parseInt(property.artistProofTotal) : 0) + (parseInt(property.trialProofTotal) ? parseInt(property.trialProofTotal) : 0) + (parseInt(property.bonATirerTotal) ? parseInt(property.bonATirerTotal) : 0) + (parseInt(property.printersProofTotal) ? parseInt(property.printersProofTotal) : 0) + (parseInt(property.horsCommerceProofTotal) ? parseInt(property.horsCommerceProofTotal) : 0) + (parseInt(property.dedicatedProofTotal) ? parseInt(property.dedicatedProofTotal) : 0) + (parseInt(property.museumProofTotal) ? parseInt(property.museumProofTotal) : 0)));
          if ((property.editionSizeTypeName === "Unknown") || (property.editionSizeTypeName === "Unique") || (property.editionSizeTypeName === "Open")) {
            this.objectDetailsForm.controls['editionSizeTotal'].setValue(property.editionOverride ? property.editionOverride : (property.editionSizeTypeName ? property.editionSizeTypeName : null));
          } else {
            this.objectDetailsForm.controls['editionSizeTotal'].setValue(
              property.editionOverride ? property.editionOverride :
                ((this.editionSizeForm.value.editionSizeSeries) != null ? (parseInt(this.editionSizeForm.value.editionSizeSeries) >= -1 ? (parseInt(this.editionSizeForm.value.editionSizeSeries) === -1 ? 1 : parseInt(this.editionSizeForm.value.editionSizeSeries)) : '') : '') +
                ((this.editionSizeForm.value.editionSizeArtistProof) != null ? ' + ' + (parseInt(this.editionSizeForm.value.editionSizeArtistProof) >= 0 ? parseInt(this.editionSizeForm.value.editionSizeArtistProof) : '') + 'AP' : (parseInt(this.editionSizeForm.value.editionSizeArtistProof) == -1 ? '+AP' : '')) +
                ((this.editionSizeForm.value.editionSizeTrialProof) != null ? ' + ' + (parseInt(this.editionSizeForm.value.editionSizeTrialProof) >= 0 ? parseInt(this.editionSizeForm.value.editionSizeTrialProof) : '') + 'TP' : (parseInt(this.editionSizeForm.value.editionSizeTrialProof) == -1 ? '+TP' : '')) +
                ((this.editionSizeForm.value.editionSizeBonATirerProof) != null ? ' + ' + (parseInt(this.editionSizeForm.value.editionSizeBonATirerProof) >= 0 ? parseInt(this.editionSizeForm.value.editionSizeBonATirerProof) : '') + 'BAT' : (parseInt(this.editionSizeForm.value.editionSizeBonATirerProof) == -1 ? '+BAT' : '')) +
                ((this.editionSizeForm.value.editionSizePrintersProof) != null ? ' + ' + (parseInt(this.editionSizeForm.value.editionSizePrintersProof) >= 0 ? parseInt(this.editionSizeForm.value.editionSizePrintersProof) : '') + 'PP' : (parseInt(this.editionSizeForm.value.editionSizePrintersProof) == -1 ? '+PP' : '')) +
                ((this.editionSizeForm.value.editionSizeHorsCommerceProof) != null ? ' + ' + (parseInt(this.editionSizeForm.value.editionSizeHorsCommerceProof) >= 0 ? parseInt(this.editionSizeForm.value.editionSizeHorsCommerceProof) : '') + 'HC' : (parseInt(this.editionSizeForm.value.editionSizeHorsCommerceProof) == -1 ? '+HC' : '')) +
                ((this.editionSizeForm.value.editionSizeDedicatedProof) != null ? ' + ' + (parseInt(this.editionSizeForm.value.editionSizeDedicatedProof) >= 0 ? parseInt(this.editionSizeForm.value.editionSizeDedicatedProof) : '') + 'DP' : (parseInt(this.editionSizeForm.value.editionSizeDedicatedProof) == -1 ? '+DP' : '')) +
                ((this.editionSizeForm.value.editionSizeMuseumProof) != null ? ' + ' + (parseInt(this.editionSizeForm.value.editionSizeMuseumProof) >= 0 ? parseInt(this.editionSizeForm.value.editionSizeMuseumProof) : '') + 'MP' : (parseInt(this.editionSizeForm.value.editionSizeMuseumProof) == -1 ? '+MP' : ''))
            );
          }
          this.objectDetailsForm.controls['weight'].setValue(property.weight ? property.weight : null);
          this.objectDetailsForm.controls['foundry'].setValue(property.foundry ? property.foundry : null);
          // Archive Numbers
          this.objectDetailsForm.controls['archiveNumbers'].setValue("");
          this.archiveNoTags = [];
          for (let i in property.archiveNumbers) {
            this.archiveNoTags.push(property.archiveNumbers[i].archiveNumber);
          }
          this.objectDetailsForm.controls['researcherName'].setValue(property.researcherName ? property.researcherName : null);
          this.objectDetailsForm.controls['researcherNotesDate'].setValue((property.researcherNotesDate && property.researcherNotesDate != '') ? this.getDate(property.researcherNotesDate, "DD MMM YYYY") : null);
          this.objectDetailsForm.controls['researcherNotesText'].setValue(property.researcherNotesText ? property.researcherNotesText : null);
          this.objectDetailsForm.controls['researchCompleteCheck'].setValue(property.researchCompleteCheck ? property.researchCompleteCheck : false);
          this.objectDetailsForm.controls['volume'].setValue(property.volume ? property.volume : null);
          this.objectDetailsForm.controls['number'].setValue(property.number ? property.number : null);
          this.requiredFlag = true;
          //settingTagProperties

          this.objectDetailsForm.controls['tags'].patchValue({
            'imageText': (property && property.tags && property.tags.imageText) ? property.tags.imageText : null,
            'imageFigure': (property && property.tags && property.tags.imageFigure) ? property.tags.imageFigure : 0,
            'expertiseLocation': (property && property.tags && property.tags.expertiseLocation) ? property.tags.expertiseLocation : null
          });
          this.selectedGender = [];
          property.tags.imageGenders.forEach(x => {
            this.genderValChange(x);
          });
          this.selectedPosition = [];
          property.tags.imagePositions.forEach(x => {
            this.positionValChange(x);
          });
          this.selectedMainColor = [];
          property.tags.imageMainColours.forEach(x => {
            this.mainColorsValChange(x);
          });

          this.selectedSitter = [];
          var sitterList = (property && property.tags && property.tags.expertiseSitters) ? property.tags.expertiseSitters : [];
          sitterList.forEach(data => {
            if (data) {
              var name = (data.firstName ? data.firstName + ' ' : '') + (data.lastName ? data.lastName : '');
              this.selectedSitter.push(
                {
                  id: data.id,
                  name: name.trim()
                }
              );
            }
          });
          this.selectedAnimal = [];
          var animalList = (property && property.tags && property.tags.imageAnimals) ? property.tags.imageAnimals : [];
          animalList.forEach(data => {
            if (data) {
              this.selectedAnimal.push({ name: data });
            }
          });

          this.selectedSubject = [];
          property.tags.imageSubjects.forEach(x => {
            this.subjectValChange(x);
          });
          this.selectedOtherTag = [];
          var otherTagList = (property && property.tags && property.tags.otherTags) ? property.tags.otherTags : [];
          otherTagList.forEach(data => {
            if (data) {
              this.selectedOtherTag.push({ name: data });
            }
          });

          //areaCm = (property)?((property.heightCm?parseFloat(property.heightCm):1)*(property.widthCm?parseFloat(property.widthCm):1)*((property.depthCm?parseFloat(property.depthCm):1))).toFixed(2):0;
          let areaCm: any = (property && property.tags && property.tags.autoVolume) ? (property.tags.autoVolume).toFixed(2) : null;
          let areaIn: any = null;
          if (areaCm && property && property.heightCm && property.widthCm && property.depthCm) {
            areaIn = (areaCm * 0.393701 * 0.393701 * 0.393701).toFixed(2);
          } else if (areaCm) {
            areaIn = (areaCm * 0.393701 * 0.393701).toFixed(2);
          }

          /*	var areaCm;
            var areaIn;
            if((!property.heightCm && !property.widthCm && ! property.depthCm) || (property.heightCm && !property.widthCm && ! property.depthCm) || (!property.heightCm && property.widthCm && ! property.depthCm) || (!property.heightCm && !property.widthCm && property.depthCm)){
              areaCm = 0;
              areaIn = 0;	
            } else {
              //areaCm = (property)?((property.heightCm?parseFloat(property.heightCm):1)*(property.widthCm?parseFloat(property.widthCm):1)*((property.depthCm?parseFloat(property.depthCm):1))).toFixed(2):0;
              areaCm = (property && property.tags && property.tags.autoVolume)?(property.tags.autoVolume).toFixed(2):0;
              if(property && property.heightCm && property.widthCm && property.depthCm){
                areaIn = (areaCm * 0.393701 * 0.393701 * 0.393701).toFixed(2);
              areaIn =
              }else{
                areaIn = (areaCm * 0.393701 * 0.393701).toFixed(2);
            }				
              //areaIn = (property)?((property.heightCm?parseFloat(property.heightCm):1)*(property.widthCm?parseFloat(property.widthCm):1)*((property.depthCm?parseFloat(property.depthCm):1)) * 0.393701).toFixed(2):0;
            } */
          this.objectDetailsForm.controls['tags'].patchValue({
            'autoOrientation': (property && property.tags && property.tags.autoOrientation) ? property.tags.autoOrientation : null,
            'autoImage': (property && property.tags && property.tags.autoImage) ? 'Yes' : 'No',
            'autoScale': (property && property.tags && property.tags.autoScale) ? property.tags.autoScale : null,
            'autoAreaCm': areaCm,
            'autoAreaIn': areaIn
          });

          this.propertyCatalogRaisonnee = [];

          property.artists.forEach(data => {
            data.artistCatalogueRaisonees.forEach(catalog => {
              this.propertyCatalogRaisonnee.push({ artistCatalogueRaisoneeId: catalog.id, artistCatalogueRaisoneeAuthor: catalog.author, artistCatalogueRaisoneeType: catalog.type, sortId: catalog.sortId, number: '', volume: '', });
            })
          });
          property.propertyCatalogueRaisonees.forEach(data => {
            if (data.artistCatalogueRaisoneeType == "" && data.artistCatalogueRaisoneeAuthor == "" && data.volume == "" && data.number == "") {
              return;
            } else {
              for (let i = 0; i < this.propertyCatalogRaisonnee.length; i++) {
                if (this.propertyCatalogRaisonnee[i].artistCatalogueRaisoneeId == data.artistCatalogueRaisoneeId) {
                  if (!(data.volume == "" && data.number == "")) {
                    this.propertyCatalogRaisonnee[i].number = data.number;
                    this.propertyCatalogRaisonnee[i].volume = data.volume;
                  }
                  break;
                }
              }
            }
          });

          this.propertyCatalogRaisonnee = this.sort(this.propertyCatalogRaisonnee);
          var footerDetails;
          let createdByFName, createdByLName, updatedByFName, updatedByLName;
          var localCreatedDate;
          var localUpdatedDate;
          if (Response.createdDate) {
            localCreatedDate = this.getLocalDate(Response.createdDate);
          }
          if (Response.updatedDate) {
            localUpdatedDate = this.getLocalDate(Response.updatedDate)
          }
          if (property && property.createdBy) {
            createdByFName = property.createdBy.firstName ? property.createdBy.firstName : '';
            createdByLName = property.createdBy.lastName ? property.createdBy.lastName : '';
          }
          if (property && property.updatedBy) {
            updatedByFName = property.updatedBy.firstName ? property.updatedBy.firstName : '';
            updatedByLName = property.updatedBy.lastName ? property.updatedBy.lastName : '';
          }
          this.footerName = {
            createdDate: Response.createdDate ? Response.createdDate : null,
            createdBy: createdByFName && createdByLName ? createdByFName + " " + createdByLName : null,
            updatedDate: Response.updatedDate ? Response.updatedDate : null,
            updatedBy: updatedByFName && updatedByLName ? updatedByFName + " " + updatedByLName : null
          }
          this.changed.emit(this.footerName);
        }, resFileError => {
          let errBody = JSON.parse(resFileError._body);
          if (resFileError.status === 404) {
            this.reset = true;
            this.editModeFlag = true;
            this.resetFullForm();
            this.emptyPagination = 'No record found';
            this.widthStyle = 115;
          }
          if (errBody.mergedTo && errBody.mergedTo != '') {
            this.dialogService.addDialog(ConfirmComponent, {
              title: 'Object Merged',
              message: 'Object ID:' + properties.id + ' has been merged into Object ID:' + errBody.mergedTo +
                '. Would you like to proceed to Object ID:' + errBody.mergedTo + '?'
            }, { backdropColor: 'rgba(220,220,220,0.5)' })
              .subscribe((isConfirmed) => {
                if (isConfirmed) {
                  this.displayPropertyDetails({ "id": errBody.mergedTo });
                } else {
                  window.close();
                }
              });
          }
          else {
            this.errorMsg = resFileError;
          }
        });
      document.getElementsByTagName('html')[0].classList.remove('loading');
    }
  }

  sort(arr) {
    var len = arr.length;
    for (var i = len - 1; i >= 0; i--) {
      for (var j = 1; j <= i; j++) {
        if (arr[j - 1].sortId > arr[j].sortId) {
          var temp = arr[j - 1];
          arr[j - 1] = arr[j];
          arr[j] = temp;
        }
      }
    }
    return arr;
  }

  //pagination
  public pageObject;
  public pageEnter: boolean = false;
  pageFunction(flagNo: string) {
    if (this.headerFlag === "edit") {
      this.showConfirm('pageFunction', flagNo);
    } else {
      document.getElementById('deletebutton').style.display = "none";
      this.pageObject = {
        pageNo: this.pageNo,
        pageNumber: this.pageNumber,
        curPageValue: this.curPageValue,
        pageData: this.pageData,
        flagPage: (flagNo == 'nextPage') ? 'nextPage' : 'prevPage'
      };
      this.pageObject = this._paginationService.getNextPage(this.pageObject);
      this.curPageValue = this.pageObject.curPageValue;
      this.pageNumber = this.pageObject.pageNo;
      this.pageNo = this.pageObject.pageNo;
      if (this.pageObject.pageFlag === "single") {
        var objectId = { id: this.pageData[this.pageObject.arryIndex].id };
        this.displayPropertyDetails(objectId);
      } else {
        this.searchProperty('pagination', this.pageNumber);
      }
    }
  }
  //jump user input page
  getCurrValue(inputvalue, event) {
    this.pageEnter = true;
    if (event === "firstPage" || event === "lastPage" || event === "enter") {
      this.pageEnter = false;
      if (inputvalue >= 0 && inputvalue < this.total_pages) {
        this.curPageValue = inputvalue;
        this.current_page = this.curPageValue + 1;
        this.pageFunction('nextPage');
      } else {
        if (inputvalue >= this.total_pages) {
          this.getCurrValue(this.total_pages - 1, 'lastPage');
        } else {
          if (inputvalue <= -1) {
            this.getCurrValue(0, 'firstPage');
          }
        }
      }
    }
  }

  // Get 'Archive Numbers'
  public getArchiveNumbers(archiveNum) {
    this.archiveNumbers = [];
    if (typeof archiveNum === 'string' && this.headerFlag == 'search') {
      if (archiveNum.trim().length > 0) {
        this.archiveNumbers.push({ "archiveNumber": archiveNum.trim() });
      }
    }
    if (this.objectDetailsForm.controls['archiveNumbers'].value) {
      let tempArchiveNo = this.objectDetailsForm.controls['archiveNumbers'].value;
      for (let archiveNo in tempArchiveNo) {
        if (tempArchiveNo[archiveNo].value != '') {
          if (tempArchiveNo[archiveNo].hasOwnProperty('value')) {
            this.archiveNumbers.push({ "archiveNumber": tempArchiveNo[archiveNo].value });
          }
          else {
            this.archiveNumbers.push({ "archiveNumber": tempArchiveNo[archiveNo] });
          }
        }
      }
    }
    return this.archiveNumbers;
  }

  public getAllArchiveNumbers() {
    let tempArchiveNo = this.objectDetailsForm.controls['archiveNumbers'].value;
    this.archiveNumbers = [];
    for (let archiveNo in tempArchiveNo) {
      if (tempArchiveNo[archiveNo].hasOwnProperty('value')) {
        this.archiveNumbers.push({ "archiveNumber": tempArchiveNo[archiveNo].value });
      }
      else {
        this.archiveNumbers.push({ "archiveNumber": tempArchiveNo[archiveNo] });
      }
    }
    return this.archiveNumbers;
  }

  public artistName = [];
  public checkedArtist = [];
  public tempArtist = [];
  public tempAnimal = [];
  public tempSitter = [];
  public tempOtherTag = [];
  public artistMap;
  public artistQuery;
  public checkExists = false;

  //Artist change
  artistChange(flag: string, value: any) {
    if (flag === 'add' && typeof value === 'object') {
      this.headerFlag = (this.headerFlag === "result") ? "edit" : this.headerFlag;
      this.tempArtist = this.checkedArtist.filter(function (art) {
        if (value)
          return art.id == value.id;
      });
      if (this.tempArtist.length > 0) {
        this.objectDetailsForm.controls['artistData'].setValue("");
      } else if (value) {
        this.checkedArtist.push(value);
        this.objectDetailsForm.controls['artistData'].setValue("");
        this.getArtistCatalogRaisonnees(value.id);
      }
    } else if (flag === 'remove' && typeof value === 'object') {
      this.tempArtist = this.checkedArtist.filter(function (art) {
        return art.id != value.id;
      });
      this.checkedArtist = this.tempArtist;
      this.removeArtistCatalogRaisonnees(value.id);
    } else {
      if (value) {
        var tempArtistData = [];
        value = value.split(' ');
        if (value[0] && value[1]) {
          tempArtistData = value.splice(1, value.length).join(' ');
          this.artistQuery = `firstName=` + value[0] + `&lastName=` + tempArtistData + `&lod=MAX`;
          this.refreshData(this.artistQuery);
        } else if (value[0]) {
          this.artistQuery = `name=` + value[0] + `&lod=MAX`;
          this.refreshData(this.artistQuery);
        }
      }
    }
  }

  //animal change
  animalChange(flag: string, value: any) {
    if (flag === 'add' && typeof value === 'object') {
      this.headerFlag = (this.headerFlag === "result") ? "edit" : this.headerFlag;
      this.tempAnimal = this.selectedAnimal.filter(function (ani) {
        if (value)
          return ani.name == value.name;
      });
      if (this.tempAnimal.length > 0) {
        this.objectDetailsForm.controls['tags'].patchValue({
          'imageAnimals': null
        });
      } else if (value) {
        if (value.id != '-1') {
          this.selectedAnimal.push(value);
          this.objectDetailsForm.controls['tags'].patchValue({
            'imageAnimals': null
          });
        } else {
          if (!this.isConfirmWindowOpen) {
            if (value && value.name.length > 0) {
              this.confirmTagSave('Animal', value);
            }
          }
        }
      }
    } else if (flag === 'remove' && typeof value === 'object') {
      this.tempAnimal = this.selectedAnimal.filter(function (ani) {
        return ani.name != value.name;
      });
      this.selectedAnimal = this.tempAnimal;
      this.valueChanged();
    }
  }

  //Sitter change
  sitterChangefirstName(flag: string, value: any, tag: string) {
    if (flag === 'add' && typeof value === 'object') {
      this.headerFlag = (this.headerFlag === "result") ? "edit" : this.headerFlag;
      this.tempSitter = this.selectedSitter.filter(function (sit) {
        if (value)
          return (sit.id == value.id);
      });
      if (this.tempSitter.length > 0) {
        this.objectDetailsForm.controls['tags'].patchValue({
          'expertiseSitterFName': null,
          'expertiseSitterLName': null
        });
      } else if (value) {
        if (value.id != '-1') {
          this.selectedSitter.push(value);
          this.objectDetailsForm.controls['tags'].patchValue({
            'expertiseSitterFName': null,
            'expertiseSitterLName': null
          });
        } else {
          if (!this.isConfirmWindowOpen) {
            this.objectDetailsForm.controls['tags'].patchValue({
              'expertiseSitterFName': null,
              'expertiseSitterLName': null
            });
            if (value && value.name.length > 0) {
              this.confirmTagSave('Sitter', value);
            }
          }
        }
      } else {
        if ((value && value.id != '-1') && this.objectDetailsForm.value.tags.expertiseSitterFName != null) {
          let val = { name: this.objectDetailsForm.value.tags.expertiseSitterFName };
          this.confirmTagSave('Sitter', val);
        }
      }
    } else if (flag === 'remove' && typeof value === 'object') {
      this.tempSitter = this.selectedSitter.filter(function (sit) {
        return sit.name != value.name;
      });
      this.selectedSitter = this.tempSitter;
    }
  }

  sitterChangelastName(flag: string, value: any, tag: string) {
    if (flag === 'add' && typeof value === 'object') {
      this.headerFlag = (this.headerFlag === "result") ? "edit" : this.headerFlag;
      this.tempSitter = this.selectedSitter.filter(function (sit) {
        if (value)
          return (sit.id == value.id);
      });
      if (this.tempSitter.length > 0) {
        this.objectDetailsForm.controls['tags'].patchValue({
          'expertiseSitterFName': null,
          'expertiseSitterLName': null
        });
      } else if (value) {
        if (value.id != '-1') {
          this.selectedSitter.push(value);
          this.objectDetailsForm.controls['tags'].patchValue({
            'expertiseSitterFName': null,
            'expertiseSitterLName': null
          });
        } else {
          if (!this.isConfirmWindowOpen) {
            this.objectDetailsForm.controls['tags'].patchValue({
              'expertiseSitterFName': null,
              'expertiseSitterLName': null
            });
            if (value && value.name.length > 0) {
              this.confirmTagSave('Sitter', value);
            }
          }
        }
      } else {
        if ((value && value.id != '-1') && this.objectDetailsForm.value.tags.expertiseSitterLName != null) {
          let val = { name: this.objectDetailsForm.value.tags.expertiseSitterLName };
          this.confirmTagSave('Sitter', val);
        }
      }
    } else if (flag === 'remove' && typeof value === 'object') {
      this.tempSitter = this.selectedSitter.filter(function (sit) {
        return sit.name != value.name;
      });
      this.selectedSitter = this.tempSitter;
    }
  }

  observableSource = (value: any): Observable<any> => {
    let tempSitterFName: string = (<HTMLInputElement>document.getElementById('expertiseFName')).value.trim();
    let tempSitterLName: string = (<HTMLInputElement>document.getElementById('expertiseLName')).value.trim();
    if (tempSitterFName || tempSitterLName) {
      var reqFParam = tempSitterFName ? `firstName=` + tempSitterFName : '';
      var reqLParam = tempSitterLName ? `lastName=` + tempSitterLName : '';
      var reqParam = '';
      if (reqFParam) {
        reqParam = reqFParam;
      }
      if (reqLParam) {
        reqParam = reqParam ? reqParam + '&' + reqLParam : reqLParam;
      }
      return this._objectService.getSitters(reqParam, tempSitterFName, tempSitterLName, this.headerFlag);
    } else {
      return Observable.empty<any>();
    }
  }

  addSitterData(sitter) {
    let sitterObj = { firstName: null, lastName: null };
    if (sitter && ((sitter.firstName && sitter.firstName.match(/^[-\sa-zA-Z]+$/))
      || (sitter.lastName && sitter.lastName.match(/^[-\sa-zA-Z]+$/)))) {
      sitterObj.firstName = sitter.firstName ? sitter.firstName.trim() : null;
      sitterObj.lastName = sitter.lastName ? sitter.lastName.trim() : null;
      this._objectService
        .addSitter(sitterObj)
        .subscribe(Response => {
          var name = (Response.firstName ? Response.firstName + ' ' : '') + (Response.lastName ? Response.lastName : '');
          Response.name = name.trim();
          this.tosatservice.addToast('Sitter created successfully.', 'top-right');
          this.selectedSitter.push(Response);
          this.objectDetailsForm.controls['tags'].patchValue({
            'expertiseSitterFName': null,
            'expertiseSitterLName': null
          });
        }, resFileError => {
          this.tosatservice.addToastError('There was an error while saving your record.', 'top-right');
          this.errorMsg = resFileError;
        });
    }
    else {
      this.tosatservice.addToastError('There was an error while saving your record. Please enter a valid name.', 'top-right');
    }
  }

  addAnimalData(animalName) {
    let animalObj = { name: animalName };
    if (animalName && animalName.trim() && animalName.match(/^[-\sa-zA-Z]+$/)) {
      this._objectService
        .addAnimal(animalObj)
        .subscribe(Response => {
          this.tosatservice.addToast('Animal created successfully.', 'top-right');
          this.selectedAnimal.push(Response);
          this.objectDetailsForm.controls['tags'].patchValue({
            'imageAnimals': null
          });
        }, resFileError => {
          this.tosatservice.addToastError('There was an error while saving your record.', 'top-right');
          this.errorMsg = resFileError;
        });
    }
    else {
      this.tosatservice.addToastError('There was an error while saving your record. Please enter a valid name.', 'top-right');
    }
  }

  //Get animal with value
  observableAnimalSource = (value: any): Observable<any> => {
    if (value && value.trim()) {
      return this._objectService.getImageAnimals(value.trim(), this.headerFlag);
    } else {
      return Observable.empty<any>();
    }
  }

  //Get other tag with value
  observableOtherTagSource = (value: any): Observable<any> => {
    if (value && value.trim()) {
      return this._objectService.getOtherTags(value.trim(), this.headerFlag);
    } else {
      return Observable.empty<any>();
    }
  }

  //otherTag change
  otherTagChange(flag: string, value: any) {
    if (flag === 'add' && typeof value === 'object') {
      this.headerFlag = (this.headerFlag === "result") ? "edit" : this.headerFlag;
      this.tempOtherTag = this.selectedOtherTag.filter(function (oth) {
        if (value)
          return oth.name == value.name;
      });
      if (this.tempOtherTag.length > 0) {
        this.objectDetailsForm.controls['tags'].patchValue({
          'otherTags': null
        });
      } else if (value) {
        if (value.id != '-1') {
          this.selectedOtherTag.push(value);
          this.objectDetailsForm.controls['tags'].patchValue({
            'otherTags': null
          });
        } else {
          if (!this.isConfirmWindowOpen) {
            if (value && value.name.length > 0) {
              this.confirmTagSave('OtherTags', value);
            }
          }
        }
      }
    } else if (flag === 'remove' && typeof value === 'object') {
      this.tempOtherTag = this.selectedOtherTag.filter(function (oth) {
        return oth.name != value.name;
      });
      this.selectedOtherTag = this.tempOtherTag;
      this.valueChanged();
    }
  }
  addOtherTagData(otherTagName) {
    let otherTagObj = { name: otherTagName };
    if (otherTagName && otherTagName.trim() && otherTagName.match(/^[-\sa-zA-Z]+$/)) {
      this._objectService
        .addOtherTag(otherTagObj)
        .subscribe(Response => {
          this.tosatservice.addToast('OtherTag created successfully.', 'top-right');
          this.selectedOtherTag.push(Response);
          this.objectDetailsForm.controls['tags'].patchValue({
            'otherTags': null
          });
        }, resFileError => {
          this.tosatservice.addToastError('There was an error while saving your record.', 'top-right');
          this.errorMsg = resFileError;
        });
    }
    else {
      this.tosatservice.addToastError('There was an error while saving your record. Please enter a valid name.', 'top-right');
    }
  }

  //Get artist with Value
  refreshData(event) {
    this._artistService
      .getArtist(event)
      .subscribe(Response => {
        this.artistName.splice(0, this.artistName.length);
        Response.content.forEach(data => {
          var countryNames = "";
          for (var i = 0; i <= 2; i++) {
            if (data.countries[i] && data.countries[i].countryName && i == 0) {
              countryNames = data.countries[i].countryName;
            } else if (data.countries[i] && data.countries[i].countryName) {
              countryNames = countryNames + ", " + data.countries[i].countryName;
            }
          }
          var bd = ((data.birthYear && data.deathYear) ? "(" + data.birthYear + "-" + data.deathYear + ")" : (data.birthYear ? "(b." + data.birthYear + ")" : ''));
          this.artistName.push({
            firstName: (data.firstName ? data.firstName + ' ' : '') + data.lastName,
            display: data.display ? data.display : ((data.firstName ? data.firstName + ' ' : '') + data.lastName + bd),
            birthYear: data.birthYear,
            deathYear: data.deathYear,
            nationality: countryNames,
            id: data.id
          });
        });
      }, resFileError => this.errorMsg = resFileError);
  }

  // Get current date
  public getToday(format) {
    let today = null;
    let now = new Date();
    let day = ("0" + now.getDate()).slice(-2);
    let month = now.getMonth();
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    let currentMonth = months[month];

    if (format.toLowerCase() == "mm-dd-yyyy") {
      today = (month + 1) + "-" + day + "-" + now.getFullYear();
    } else {
      today = day + " " + currentMonth + " " + now.getFullYear();
    }
    return today;
  }

  // Convert given date to formats: MM-DD-YYYY (or) YYYY-MM-DD (or) DD MMM YYYY
  public getDate(dt, format) {
    let day, month, year;
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    // If date has '-', 'T' and in the format YYYY-MM-DD (e.g. 2017-05-01T19:32:16.013Z)
    if (dt && (dt.indexOf("T") > -1) && (dt.indexOf("-") > -1)) {
      dt = dt.slice(0, dt.indexOf("T"));
      day = dt.split("-")[1];
      month = dt.split("-")[0];
      year = dt.split("-")[2];
      if (month.charAt(0) == "0") {
        month = month.slice(1, 2);
      }

      month = months[month - 1];
    }
    // If date has '-' and in the format MM-DD-YYYY (e.g. 05-01-2017)
    else if (dt && dt.indexOf("-") > -1) {
      dt = dt;
      day = dt.split("-")[1];
      month = dt.split("-")[0];
      year = dt.split("-")[2];
      if (month.charAt(0) == "0") {
        month = month.slice(1, 2);
      }
      month = months[month - 1];
    }
    // If date has ' ' (space) and in the format DD MMM YYYY (e.g. 01 May 2017)
    else if (dt && dt.indexOf(" ") > -1) {
      dt = dt;
      day = dt.split(" ")[0];
      month = dt.split(" ")[1];
      year = dt.split(" ")[2];
      if (/^[a-zA-Z()]$/.test(month.charAt(0))) {
        month = months.indexOf(month) + 1;
        month = month.length == 1 ? "0" + month : month;
        day = day.length == 1 ? "0" + day : day;
      }
      else if (!/^[a-zA-Z()]$/.test(month.charAt(0))) {
        if (month.charAt(0) == "0") {
          month = month.slice(1, 2);
        }
        month = months[month];
      }
    }

    if (format) {
      if (format.toLowerCase() == "mm-dd-yyyy") {
        return (month + "-" + day + "-" + year);
      }
      else if (format.toLowerCase() == "dd mmm yyyy") {
        return (day + " " + month + " " + year);
      }
    }
    else {
      return (day + " " + month + " " + year);
    }
  }


  // Set 'Researcher Name' and 'Date'
  public setResearcherInfo(event) {
    let researcherName = this.keycloak.tokenParsed.name;
    if (this.objectDetailsForm.controls['researcherNotesText'].value) {
      this.objectDetailsForm.controls['researcherName'].setValue(researcherName);
      this.researchNotesDate = this.getToday("dd mmm yyyy");
      this.objectDetailsForm.controls['researcherNotesDate'].setValue(this.researchNotesDate);
    }
    else {
      researcherName = "";
      this.objectDetailsForm.controls['researcherName'].reset();
      this.objectDetailsForm.controls['researcherNotesDate'].reset();
    }
    if (event && event.keyCode && event.keyCode == 13) {
      this.objectDetailsForm.controls['researcherNotesText'].setValue(this.objectDetailsForm.controls['researcherNotesText'].value == null ? (" " + "\n") : this.objectDetailsForm.controls['researcherNotesText'].value + "\n");
    }
  }

  public convertedHeight;

  // Conversion Cm to Inches and Inches to Cm viceversa
  CmToInViceVersaChange(flag: string, conversionFlag: string, value: any) {
    if (!(this.headerFlag == "search" && value == "*")) {
      if (value && parseFloat(value)) {
        var cal = (conversionFlag === 'CmToIn') ? 0.393701 : 2.54;
        if ((parseFloat(value) * cal) && conversionFlag == 'CmToIn') {
          var decimal = (parseFloat(value) * cal) % 1;
          var offset = null;
          if (decimal < 1 / 8) {
            offset = null;
          }

          if (decimal >= 1 / 8 && decimal < (1 / 8 + 1 / 4) / 2) {
            offset = "1/8";
          }

          if ((decimal >= (1 / 8 + 1 / 4) / 2 && decimal < 1 / 4) || (decimal >= 1 / 4 && decimal < (1 / 4 + 3 / 8) / 2)) {
            offset = "1/4";
          }

          if ((decimal >= (1 / 4 + 3 / 8) / 2 && decimal < 3 / 8) || (decimal >= 3 / 8 && decimal < (3 / 8 + 1 / 2) / 2)) {
            offset = "3/8";
          }

          if ((decimal >= (3 / 8 + 1 / 2) / 2 && decimal < 1 / 2) || (decimal >= 1 / 2 && decimal < (1 / 2 + 5 / 8) / 2)) {
            offset = "1/2";
          }

          if ((decimal >= (1 / 2 + 5 / 8) / 2 && decimal < 5 / 8) || (decimal >= 5 / 8 && decimal < (5 / 8 + 3 / 4) / 2)) {
            offset = "5/8";
          }

          if ((decimal >= (5 / 8 + 3 / 4) / 2 && decimal < 3 / 4) || (decimal >= 3 / 4 && decimal < (3 / 4 + 7 / 8) / 2)) {
            offset = "3/4";
          }

          if ((decimal >= (3 / 4 + 7 / 8) / 2 && decimal < 7 / 8) || (decimal >= 7 / 8 && decimal < (7 / 8 + 1) / 2)) {
            offset = "7/8";
          }

          if (decimal >= (7 / 8 + 1) / 2) {
            offset = '1';
          }

          if (offset == '1') {
            this.objectDetailsForm.controls[flag].setValue(Math.trunc((parseFloat(value) * cal)) + 1);
          } else {
            if (Math.trunc((parseFloat(value) * cal)) == 0) {
              this.objectDetailsForm.controls[flag].setValue(offset);
            } else {
              if (offset == null) {
                this.objectDetailsForm.controls[flag].setValue(Math.trunc((parseFloat(value) * cal)));
              } else {
                this.objectDetailsForm.controls[flag].setValue(Math.trunc((parseFloat(value) * cal)) + ' ' + offset);
              }
            }
          }
        } else {
          this.objectDetailsForm.controls[flag].setValue(((parseFloat(value) * cal).toFixed(2)));
        }
      } else {
        this.objectDetailsForm.controls[flag].setValue(null);
      }
    } else {
      this.objectDetailsForm.controls[flag].setValue(value);
    }
  }

  resetFullForm() {
    if (this.headerFlag == "edit" && !this.confirmFlag && sessionStorage.getItem('MainQueryFlag') != 'deleteMode') {
      this.showConfirm('defaultMode', this.objectDetailsForm.value);
    } else {
      this.objectId = null;
      this.currentProperty = null;
      this.sortFlag = !this.sortFlag;
      sessionStorage.removeItem('selectSort');
      sessionStorage.removeItem('indexSort');
      this.confirmFlag = false;
      this.ngOnInit();
      this.dropuploadComponent.reset();
      this.reset = true;
      this.pageData = [];
      this.pageNo = 0;
      this.pageNumber = 0;
      this.current_page = 1;
      this.total_pages = 0;
      this.curPageValue = 1;
      this.propertyCatalogRaisonnee = [];
      this.objectDetailsForm.reset();
      this.editionSizeForm.reset();
      this.artistName = [];
      this.checkedArtist = [];
      this.selectedSitter = [];
      this.selectedAnimal = [];
      this.selectedSubject = [];
      this.selectedOtherTag = [];
      this.selectedGender = [];
      this.selectedPosition = [];
      this.selectedMainColor = [];
      this.tempArtist = [];
      this.artistMap;
      this.artistQuery;
      this.externalId = null;
      this.headerFlag = 'search';
      this.emptyPagination = '';
      this.requiredFlag = false;
      this.thumbnailFlag = false;
      this.sorted = false;
      this.footerName = {
        createdDate: null,
        createdBy: null,
        updatedDate: null,
        updatedBy: null
      };
      this.changed.emit(this.footerName);
      window.history.replaceState(null, null, "object");
      this.resetSaveListSession();
      this.highlightSortIcon(null);
    }
  }

  // remove the main color function
  removeMainColor(item) {
    this.nMainColor.push(item.name);
    this.nMainColor.sort();
    this.selectedMainColor.splice(this.selectedMainColor.indexOf(item), 1);
    this.valueChanged();
  }
  removeAllMainColors(event) {
    this.objectDetailsForm.controls['tags'].patchValue({
      'imageMainColors': ''
    });
  }

  // remove the subject function
  removeSubject(item) {
    this.nSubject.push(item.name);
    this.nSubject.sort();
    this.selectedSubject.splice(this.selectedSubject.indexOf(item), 1);
    this.valueChanged();
  }
  removeAllSubjects(event) {
    this.objectDetailsForm.controls['tags'].patchValue({
      'imageSubjects': ''
    });
  }

  removeSitterOnBlur(event) {
    if (event && event.currentTarget && event.relatedTarget && event.currentTarget.id && event.relatedTarget.id) {
      if (!((event.currentTarget.id == 'expertiseFName' && event.relatedTarget.id == 'expertiseLName')
        || (event.currentTarget.id == 'expertiseLName' && event.relatedTarget.id == 'expertiseFName'))) {
        this.objectDetailsForm.controls['tags'].patchValue({
          'expertiseSitterFName': null,
          'expertiseSitterLName': null
        });
      }
    } else {
      this.objectDetailsForm.controls['tags'].patchValue({
        'expertiseSitterFName': null,
        'expertiseSitterLName': null
      });
    }
  }

  // remove the gender function
  removeGender(item) {
    this.nGender.push(item.name);
    this.nGender.sort();
    this.selectedGender.splice(this.selectedGender.indexOf(item), 1);
    this.valueChanged();
  }
  removeAllGenders(event) {
    this.objectDetailsForm.controls['tags'].patchValue({
      'imageGenders': ''
    });
  }
  // remove the position function
  removeAllPositions(event) {
    this.objectDetailsForm.controls['tags'].patchValue({
      'imagePositions': ''
    });
  }
  removePosition(item) {
    this.nPosition.push(item.name);
    this.nPosition.sort();
    this.selectedPosition.splice(this.selectedPosition.indexOf(item), 1);
    this.valueChanged();
  }

  resetSaveListSession() {
    this.saveListView = false;
    sessionStorage.setItem('savelistview', 'false');
    sessionStorage.removeItem('navigatedtosavedlistid');
    sessionStorage.removeItem('thumbnailselectall');
  }

  // Get Artist Catalog Raisonnees
  public selArtist;
  public getArtistCatalogRaisonnees(data) {
    this._artistService.getIndividualArtist(data)
      .subscribe(Response => {
        if (Response.artistCatalogueRaisonees.length > 0) {
          Response.artistCatalogueRaisonees.forEach((data, index) => {
            if (this.externalId) {
              var CRval = [{
                artistCatalogueRaisoneeType: data.type, artistCatalogueRaisoneeAuthor: data.author,
                volume: data.volume, number: data.number, artistCatalogueRaisoneeId: data.id
              }];
              this.propertyCatalogRaisonnee = this.propertyCatalogRaisonnee.concat(CRval);
            } else {
              this.propertyCatalogRaisonnee = this.propertyCatalogRaisonnee.concat(data);
            }
            // Add artist's last CR to 'lastCR []'
            if (index === (Response.artistCatalogueRaisonees.length - 1)) {
              this.lastCR.push({ "artistId": Response.id, "CRId": data.id });
            }
            if (this.checkedArtist.length > 1) {
              this.getLastCR();
            } else {
              sessionStorage.removeItem('lastcr');
            }
          });
        }
      }, resFileError => this.errorMsg = resFileError);
  }

  // Remove Property Catalog Raisonnees
  public removeArtistCatalogRaisonnees(data) {
    this._artistService.getIndividualArtist(data)
      .subscribe(Response => {
        if (Response.artistCatalogueRaisonees.length > 0) {
          Response.artistCatalogueRaisonees.forEach(data => {
            if (this.externalId) {
              this.propertyCatalogRaisonnee = this.propertyCatalogRaisonnee.filter(function (CRdata) {
                return data.id != CRdata.artistCatalogueRaisoneeId;
              });
            } else {
              this.propertyCatalogRaisonnee = this.propertyCatalogRaisonnee.filter(function (CRdata) {
                if (CRdata.id)
                  return data.id != CRdata.id;
                else
                  return data.id != CRdata.artistCatalogueRaisoneeId;
              });
            }
          });
          // Remove artist last CR from 'lastCR []'
          this.lastCR = this.lastCR.filter(artist => {
            return artist.artistId != data;
          });
          if (this.checkedArtist.length > 1) {
            this.getLastCR();
          } else {
            sessionStorage.removeItem('lastcr');
          }
        }
      }, resFileError => this.errorMsg = resFileError);
  }

  // Update Property Catalog Raisonnees
  public updatePropertyCatalogRaisonnees(flag: string) {
    var finalCRarr = [];
    this.propertyCatalogRaisonnee.forEach(data => {
      if (data.id && (flag == 'save') && data.volumeCR != null || data.numberCR != null) {
        if (data.numberCR || data.volumeCR) {
          finalCRarr.push({ "artistCatalogueRaisoneeId": data.id, "number": data.numberCR, "volume": data.volumeCR });
        }
      } else if (data.artistCatalogueRaisoneeId && flag == 'update' && (data.volume != null || data.volume != undefined || data.number != null || data.number != undefined)) {
        if (data.number || data.volume) {
          finalCRarr.push({ "artistCatalogueRaisoneeId": data.artistCatalogueRaisoneeId, "number": data.number, "volume": data.volume });
        }
      }
    });
    this.propertyCatalogRaisonnee = finalCRarr;
    return this.propertyCatalogRaisonnee;
  }

  // search Back Property Catalog Raisonnees
  public searchModePropertyCatalogRaisonnees() {
    var finalCRarr = [];
    this.propertyCatalogRaisonnee.forEach(data => {
      if (data.artistCatalogueRaisoneeId) {
        finalCRarr.push({ "artistCatalogueRaisoneeId": data.artistCatalogueRaisoneeId, "author": data.author, "type": data.type, "number": data.numberCR, "numberCR": data.numberCR, "volume": data.volumeCR, "volumeCR": data.volumeCR });
      } else {
        finalCRarr.push({ "artistCatalogueRaisoneeId": data.id, "author": data.author, "type": data.type, "number": data.numberCR, "numberCR": data.numberCR, "volume": data.volumeCR, "volumeCR": data.volumeCR });
      }
    });
    this.propertyCatalogRaisonnee = finalCRarr;
    return this.propertyCatalogRaisonnee;
  }

  public srcFlag = true;
  //Image Present or not
  errorHandler() {
    this.srcFlag = false;
  }

  redirectFLPage(flag: string) {
    if (this.headerFlag === "edit") {
      this.showConfirm(flag, 'FLValue');
    } else {
      if (flag === 'lastPage' && this.total_pages > 0) {
        this.curPageValue = this.total_pages;
        this.getCurrValue(this.total_pages - 1, 'lastPage');
      }
      if (flag === 'firstPage' && this.total_pages > 0) {
        this.getCurrValue(0, 'firstPage');
      }
    }
  }

  // Keyboard shortcuts
  keyboardShortCutFunction(e) {
    this.toggleFlag = 0;
    switch (this._keyboardkeysService.getKeyboardResponse(e)) {
      case 'save':
        if (this.headerFlag === 'result' || this.headerFlag === 'edit') {
          this.onSubmit(this.externalId ? 'updateMode' : 'saveMode', this.objectDetailsForm.value);
          this.reset = false;
        }
        break;
      case 'find':
        if (window.location.pathname.indexOf('thumbnail') > -1) {
          this.onSearch("clearSearch");
        } else if (this.headerFlag === 'search') {
          this.onSubmit('editMode', this.objectDetailsForm.value);
          this.reset = false;
        }
        break;
      case 'amendFind':
        if (this.headerFlag === 'result' || this.headerFlag === 'edit') {
          this.backMode();
        } else if (window.location.pathname.indexOf('thumbnail') > -1) {
          this.onSearch("backSearch");
        }
        break;
      case 'add':
        if (this.headerFlag === 'search' || this.headerFlag === 'result' || this.headerFlag === 'edit') {
          this.NewObjectModalPrompt();
          this.reset = false;
        }
        break;
      case 'next':
        if ((this.total_pages > 0) && (this.total_pages > this.curPageValue)) {
          this.pageFunction('nextPage');
        }
        break;
      case 'prev':
        if ((this.total_pages > 0) && (this.curPageValue > 1)) {
          this.pageFunction('prevPage');
        }
        break;
      case 'enter':
        if (!this.externalId && this.headerFlag != 'create' && !(window.location.href.indexOf('thumbnail') > -1)) {
          this.reset = false;
          this.onSubmit('editMode', this.objectDetailsForm.value);
        } else if (this.pageEnter && !(window.location.href.indexOf('thumbnail') > -1)) {
          this.getCurrValue(this.curPageValue - 1, 'enter');
        }
        break;
      case 'addSingleRecord':
        if (window.location.pathname.indexOf('thumbnail') > -1) {
          var button = document.getElementById('addThumbObjectList');
          button.click();
        } else if (this.headerFlag === 'result' || this.headerFlag === 'edit') {
          var button = document.getElementById('addObjectList');
          button.click();
        }
        break;
      case 'addAllRecord':
        if (window.location.pathname.indexOf('thumbnail') > -1) {
          var button = document.getElementById('addThumbAllObject');
          button.click();
        } else if (this.headerFlag === 'result' || this.headerFlag === 'edit') {
          var button = document.getElementById('addAllObject');
          button.click();
        }
        break;
      case 'thumbnailView':
        if (this.headerFlag === 'result' || this.headerFlag === 'edit' || this.headerFlag === 'search') {
          this.onNavigate('thumbnail');
        }
        break;
      case 'homeView':
        if (this.headerFlag === 'result' || this.headerFlag === 'edit' || this.headerFlag === 'search') {
          this.redirectPage();
        }
        break;
      case 'editMode':
        if (this.headerFlag === 'edit') {
          this.headerFlag = 'result';
          if (this.total_pages > 0) {
            this.getCurrValue(this.curPageValue - 1, 'enter');
          }
        }
        break;
      case 'duplicateWindow':
        if (this.headerFlag === 'search') {
          let urlP = window.location.href;
          window.open(urlP, '_blank');
        } else if (sessionStorage.getItem('savelistview')) {
          if (window.location.pathname.indexOf('thumbnail') > -1) {
            var urlP: any = window.location.href.split('/object');
            urlP = urlP[0] + '/object?savelist=' + this.savedListId;
            window.open(urlP, '_blank');
          } else {
            let urlP = window.location.href;
            window.open(urlP, '_blank');
          }
        } else {
          let urlP = window.location.href;
          window.open(urlP, '_blank');
        }
        break;
      case 'objectView':
        if (window.location.pathname.indexOf('thumbnail') > -1) {
          this.router.navigateByUrl('/object');
        }
        break;
    }
  }

  //New Object Open Modal
  NewObjectModalPrompt() {
    if (this.headerFlag == "edit" && !this.confirmFlag) {
      this.showConfirm('NewObjectModal', this.objectDetailsForm.value);
    } else {
      document.getElementsByTagName("body")[0].classList.add("positionFixed");
      this.confirmFlag = false;
      this.headerFlag = "create";
      this.dialogService.addDialog(ObjectModalComponent, { modeFlag: 'createMode' }, { backdropColor: 'rgba(220,220,220,0.5)' })
        .subscribe((message) => {
          document.getElementsByTagName('body')[0].classList.remove('positionFixed');
          this.headerFlag = this.externalId ? "result" : "search";
        });
    }
  }
  public searchData;

  redirectPage() {
    if (this.headerFlag == "edit" && !this.confirmFlag) {
      this.showConfirm('redirect', this.objectDetailsForm.value);
    }
    else {
      this.confirmFlag = false;
      this.resetSaveListSession();
      this.router.navigateByUrl('/');
    }
  }



  //back button 
  backMode() {
    document.getElementById('deleteimagebutton').style.display = "none";
    (<HTMLInputElement>document.getElementsByClassName('dz-hidden-input')[0]).disabled = true;
    if (this.headerFlag == "edit" && !this.confirmFlag) {
      this.showConfirm('backMode', this.objectDetailsForm.value);

      this.backModeFlag = true;
    }
    else {
      this.confirmFlag = false;
      this.thumbnailtoobject = false;
      this.sortingselected = false;
      if (this.backModeFlag) {
        this.confirmFlag = true;
      }
      this.resetFullForm();
      this.backModeFlag = false;
      this.reset = true;
      this.editModeFlag = true;
      this.appendSearchData();
    }
  }

  onopenSavedListpopup(event) {
    switch (event) {
      case "single": document.getElementById('savedlistpopupsingle').click();
        sessionStorage.setItem("typeSelected", "false");
        break;
      case "all": sessionStorage.setItem("typeSelected", "false");
        document.getElementById('savedlistpopup').click();
        break;
      case "selected": sessionStorage.setItem("typeSelected", "true");
        document.getElementById('savedlistpopup').click();
        break;
    }
    this.toggleFlag = 0;
  }

  onOpenSaveSearch() {
    document.getElementById('saveSearch').click();
    this.toggleFlag = 0;
  }

  // Get Selected Property IDs - to add to saved list.
  public getSelectedPropertyIds() {
    if (sessionStorage.getItem('addtosavelistobjects') && sessionStorage.getItem('addtosavelistobjects').length > 0 && sessionStorage.getItem('thumbnailselectall') == 'false') {
      let tempPropertyIDs = sessionStorage.getItem('addtosavelistobjects').split(',');
      this.openSaveListModal = true;
      for (let index in tempPropertyIDs) {
        if (tempPropertyIDs[index] != '') {
          this.saveListModal.selectedPropertyIds.push(tempPropertyIDs[index]);
        }
      }
    }
    // Display ALERT message if no properties are selected.
    else if ((!sessionStorage.getItem('addtosavelistobjects') && sessionStorage.getItem('thumbnailselectall') != 'true')
      || (sessionStorage.getItem('objectsUnchecked') && (sessionStorage.getItem('objectsUnchecked').split(',').length - 1) == parseInt(sessionStorage.getItem('totalelements')))) {
      this.openSaveListModal = false;
      document.getElementsByTagName("body")[0].classList.add("positionFixed");
      this.dialogService.addDialog(AlertComponent,
        { title: 'Select Properties', message: 'No objects selected. Please select the objects to be added to the list.' }, { backdropColor: 'rgba(220,220,220,0.5)' })
        .subscribe((isConfirmed) => { document.getElementsByTagName("body")[0].classList.remove("positionFixed"); });
    }
    else {
      this.openSaveListModal = true;
    }
    this.saveListModal.saveListParam = this.saveListModal.saveListParam;
    this.saveListModal.saveListMode = 'AddSaveList';
    this.saveListModal.saveListAddType = 'selected';
  }

  // Check if user has selected the properties to be added to the list.
  public addAllToSavedList() {
    this.openSaveListModal = true;
    this.saveListModal.saveListParam = this.saveListModal.saveListParam;
    this.saveListModal.saveListMode = 'AddSaveList';
    this.saveListModal.saveListAddType = 'all';
    return true;
  }

  // Validate user entered value for select lists.
  // System should not accept user entered values that are not part of the drop-down list.
  validateSelectedItem(selectedItem, list) {
    if (selectedItem) {
      selectedItem = selectedItem.toLowerCase();
      let selectListObject;
      if (list === "category") {
        let index = this.categories.findIndex(list => list.name.toLowerCase() === selectedItem);
        if (!(index > -1)) {
          if (!(selectedItem == "*" && this.flag == "searchMode")) {
            this.objectDetailsForm.controls['category'].setValue('');
          }
        }
      }
      else if (list === "subCategory") {
        let index = this.subcategories.findIndex(list => list.name.toLowerCase() === selectedItem);
        if (!(index > -1)) {
          if (!(selectedItem == "*" && this.flag == "searchMode")) {
            this.objectDetailsForm.controls['subCategory'].setValue('');
          }
        }
      }
      else if (list === "presizeName") {
        let index = this.presizeNames.findIndex(list => list.name.toLowerCase() === selectedItem);
        if (!(index > -1)) {
          if (!(selectedItem == "*" && this.flag == "searchMode")) {
            this.objectDetailsForm.controls['presizeName'].setValue('');
          }
        }
      }
      else if (list === "editionName") {
        let index = this.editionTypes.findIndex(list => list.name.toLowerCase() === selectedItem);
        if (!(index > -1)) {
          if (!(selectedItem == "*" && this.flag == "searchMode")) {
            this.objectDetailsForm.controls['editionName'].setValue('');
          }
        }
      }
      else if (list === "flagsCountry") {
        let index = this.countries.findIndex(list => list['name'].toLowerCase() === selectedItem);
        if (!(index > -1)) {
          if (!(selectedItem == "*" && this.flag == "searchMode")) {
            this.objectDetailsForm.patchValue({
              'flags': { flagsCountry: '' }
            });
          }
        }
      }
      else if (list === "imageFigure") {
        let index = this.imageFigures.findIndex(list => list.name.toLowerCase() === selectedItem);
        if (!(index > -1)) {
          this.tosatservice.addToastError('Please enter valid values for Number of Figures', 'top-right');
          if (!(selectedItem == "*" && this.flag == "searchMode")) {
            this.objectDetailsForm.patchValue({
              'tags': { imageFigure: '' }
            })
          }
        }
      }
      else if (list === "autoOrientation") {
        let index = this.autoOrientations.findIndex(list => list.name.toLowerCase() === selectedItem.toLowerCase());
        if (!(index > -1)) {
          if (!(selectedItem == "*")) {
            this.objectDetailsForm.patchValue({
              'tags': { autoOrientation: null }
            })
          }
        }
      }
      else if (list === "autoScale") {
        let index = this.autoScales.findIndex(list => list.name.toLowerCase() === selectedItem.toLowerCase());
        if (!(index > -1)) {
          if (!(selectedItem == "*")) {
            this.objectDetailsForm.patchValue({
              'tags': { autoScale: null }
            })
          }
        }
      }
      else if (list === "autoImage") {
        let index = this.autoImages.findIndex(list => list.name.toLowerCase() === selectedItem.toLowerCase());
        if (!(index > -1)) {
          if (!(selectedItem == "*")) {
            this.objectDetailsForm.patchValue({
              'tags': { autoImage: null }
            })
          }
        }
      }
      else {
        return false;
      }
    }
  }

  // Set value for select drop-down list.
  setSelectListValue(selectedItem, selectListObject, listControlName) {
    if (!(selectListObject.some(list => list.name.toLowerCase() == selectedItem))) {
      if (listControlName == "flagsCountry") {
        this.flagsCountryValue = null;
      }
      else {
        this.objectDetailsForm.controls[listControlName].setValue(null);
      }
    }
    else if (selectListObject.some(list => list.name.toLowerCase() == selectedItem)) {
      let index = selectListObject.findIndex(list => list.name.toLowerCase() == selectedItem);
      if (listControlName == "flagsCountry") {
        this.flagsCountryValue = selectListObject[index].name;
      }
      else {
        this.objectDetailsForm.controls[listControlName].setValue(selectListObject[index].name);
      }
    }
  }
  //edition number modal
  EditionModalPrompt(event) {
    document.getElementsByTagName("body")[0].classList.add("positionFixed");
    this.dialogService.addDialog(EditionModalComponent,
      { editionSizeTypes: this.editionSizeTypes, editionSizeObject: this.editionSizeForm.value, newObject: false },
      { backdropColor: 'rgba(220,220,220,0.5)' })
      .subscribe((message: any) => {
        document.getElementsByTagName("body")[0].classList.remove("positionFixed");
        if (message.editionSizeType != this.editionSizeForm.value.editionSizeType ||
          message.editionSizeSeries != this.editionSizeForm.value.editionSizeSeries ||
          message.editionSizeArtistProof != this.editionSizeForm.value.editionSizeArtistProof ||
          message.editionSizeTrialProof != this.editionSizeForm.value.editionSizeTrialProof ||
          message.editionSizeBonATirerProof != this.editionSizeForm.value.editionSizeBonATirerProof ||
          message.editionSizePrintersProof != this.editionSizeForm.value.editionSizePrintersProof ||
          message.editionSizeHorsCommerceProof != this.editionSizeForm.value.editionSizeHorsCommerceProof ||
          message.editionSizeDedicatedProof != this.editionSizeForm.value.editionSizeDedicatedProof ||
          message.editionSizeMuseumProof != this.editionSizeForm.value.editionSizeMuseumProof ||
          message.editionSizeHorsCommerceProof != this.editionSizeForm.value.editionSizeHorsCommerceProof ||
          message.editionSizeEditionOverride != this.editionSizeForm.value.editionSizeEditionOverride ||
          message.editionSizeTotal != this.editionSizeForm.value.editionSizeTotal) {
          this.headerFlag = this.externalId ? 'edit' : this.headerFlag;
        }
        this.editionSizeForm.controls['editionSizeType'].setValue(message.editionSizeType ? message.editionSizeType : "Unspecified");
        this.editionSizeForm.controls['editionSizeSeries'].setValue((message && message.editionSizeSeries) ? message.editionSizeSeries : null);
        this.editionSizeForm.controls['editionSizeArtistProof'].setValue((message && message.editionSizeArtistProof) ? message.editionSizeArtistProof : null);
        this.editionSizeForm.controls['editionSizeTrialProof'].setValue((message && message.editionSizeTrialProof) ? message.editionSizeTrialProof : null);
        this.editionSizeForm.controls['editionSizeBonATirerProof'].setValue((message && message.editionSizeBonATirerProof) ? message.editionSizeBonATirerProof : null);
        this.editionSizeForm.controls['editionSizePrintersProof'].setValue((message && message.editionSizePrintersProof) ? message.editionSizePrintersProof : null);
        this.editionSizeForm.controls['editionSizeHorsCommerceProof'].setValue((message && message.editionSizeHorsCommerceProof) ? message.editionSizeHorsCommerceProof : null);
        this.editionSizeForm.controls['editionSizeDedicatedProof'].setValue((message && message.editionSizeDedicatedProof) ? message.editionSizeDedicatedProof : null);
        this.editionSizeForm.controls['editionSizeMuseumProof'].setValue((message && message.editionSizeMuseumProof) ? message.editionSizeMuseumProof : null);
        this.editionSizeForm.controls['editionSizeEditionOverride'].setValue((message && message.editionSizeEditionOverride) ? message.editionSizeEditionOverride : null);
        this.editionSizeForm.controls['editionSizeNotes'].setValue((message && message.editionSizeNotes) ? message.editionSizeNotes : null);
        this.editionSizeForm.controls['editionSizeDisplay'].setValue((message && message.editionSizeDisplay) ? message.editionSizeDisplay : null);
        this.editionSizeForm.controls['editionSizeTotal'].setValue((message && message.editionSizeTotal) ? message.editionSizeTotal : null);
        this.objectDetailsForm.controls['editionSizeTotal'].setValue(message.editionSizeEditionOverride ? message.editionSizeEditionOverride : message.editionSizeDisplay);
      });
  }

  // Get properties from saved list.
  getSavedListProperties(value) {
    this._savedListComponent._savedListService.getPropertiesBySavedListId(this.keycloakUserName, this.savedListId, this.pageNumber, 20, 'min', this.saveListSortParam)
      .subscribe(savedList => {
        sessionStorage.setItem('totalelements', savedList.totalElements);
        this.flag = 'editMode';
        if (savedList.content) {
          document.getElementsByTagName('html')[0].classList.add('loading');
          if (savedList.content.length > 0) {
            this.pageData = [];
            this.pageData = savedList.content;
            for (let property in savedList.content) {
              this.savedListProperties.push(savedList.content[property]);
            }
            this.total_pages = savedList.totalElements;
            this.saveAllTotalElements = savedList.totalElements;
            this.displayPropertyDetails(this.pageData[value]);
            if (sessionStorage.getItem('selectSort') && sessionStorage.getItem('selectSort').length > 0) {
              this.sorted = true;
              if (this.saveListSortParam && this.saveListSortParam.indexOf(',asc') > -1) {
                this.sortOrder = 'asc';
                this.highlightSortIcon('asc');
              }
              else if (this.saveListSortParam && this.saveListSortParam.indexOf(',desc') > -1) {
                this.sortOrder = 'desc';
                this.highlightSortIcon('desc');
              }
              else {
                this.highlightSortIcon(null);
              }
            }
          }
          else {
            document.getElementsByTagName('html')[0].classList.remove('loading');
            this.reset = true;
            this.editModeFlag = true;
            this.resetFullForm();
            this.emptyPagination = 'No records found';
            this.appendSearchData();
            this.widthStyle = 115;
            this.sorted = false;
            this.highlightSortIcon(null);
          }
        }
      }, resFileError => {
        document.getElementsByTagName('html')[0].classList.remove('loading');
        this.tosatservice.addToastError('Saved List ID is not available.<br/> Please try again..!', 'top-right');
        this.errorMessage = resFileError;
        this.sorted = false;
        this.highlightSortIcon(null);
      });
  }

  sortDetails(value: any) {
    this.sortingselected = true;
    // Get 'Sort' param for saved search.
    let sortParam = value;
    sessionStorage.setItem('sortparam', sortParam.substring(6, sortParam.length));
    if (this.flag == 'savelist' || this.savedlistclicked) {
      this.saveListSortParam = value + ',' + this.sortOrder;
      this.queryParams = '';
    }
    else {
      this.saveListSortParam = null;
      this.sortingqueryparam = this.sortingqueryparam.slice(0, this.sortingqueryparam.indexOf('&lod=MIN'));
      if (this.queryParams.indexOf("&sort=") > -1) {
        var tempQuery = this.queryParams;
        var tempQueryArray = tempQuery.split('&');
        tempQueryArray.forEach(function (data, i) {
          if (data.split('=')[0] === 'artistCatalogueRaisoneeIds') {
            tempQueryArray.splice(i, 1);
            i--;
            return;
          }
          else if (data.indexOf("sort=") > -1) {
            tempQueryArray[i] = value.substr(1, value.length);
          }
        })
        this.queryParams = tempQueryArray.join('&');
      } else {
        this.queryParams = this.sortingqueryparam + value;
      }
    }
    if (this.queryParams.indexOf('&artistCatalogueRaisoneeIds=') > -1) {
      let tempQuery = this.queryParams.split('&artistCatalogueRaisoneeIds=');
      tempQuery[0] += ',' + this.sortOrder;
      this.queryParams = '';
      this.queryParams = tempQuery.join('&artistCatalogueRaisoneeIds=');
    }
    else {
      this.queryParams = this.queryParams + ',' + this.sortOrder;
    }
    this.searchPropertySort('editMode', this.queryParams);
  }


  onSort(value) {
    this.sortOrder = 'asc';
    if (value == "more") {
      document.getElementsByTagName("body")[0].classList.add("positionFixed");
      this.dialogService.addDialog(CustomSortComponent, { customSort: "customSort" }, { backdropColor: 'rgba(220,220,220,0.5)' })
        .subscribe((response) => {
          document.getElementsByTagName("body")[0].classList.remove("positionFixed");
          if (response === "false") {
          } else if (this.flag === 'savelist' || this.savedlistclicked) {
            this.current_page = 1;
            this.curPageValue = 1;
            this.sortDetails(response);
          } else {
            this.sortingqueryparam = this.sortingqueryparam.indexOf('&artistCatalogueRaisoneeIds') > -1 ? this.sortingqueryparam.substring(0, this.sortingqueryparam.indexOf('&artistCatalogueRaisoneeIds')) : this.sortingqueryparam;
            this.sortingqueryparam = this.sortingqueryparam.indexOf('&lod=MIN') > -1 ? this.sortingqueryparam.substring(0, this.sortingqueryparam.indexOf('&lod=MIN')) : this.sortingqueryparam;
            // EOS-1173
            if (this.sortOrder === 'asc') {
              response = response + ',asc';
            }
            else if (this.sortOrder === 'desc') {
              response = response + ',desc';
            }
            if (this.sortingqueryparam.indexOf("&sort=") > -1) {
              var tempQuery = this.sortingqueryparam;
              var tempQueryArray = tempQuery.split('&');
              tempQueryArray.forEach(function (data, i) {
                if (data.indexOf("sort=") > -1) {
                  tempQueryArray[i] = response.substr(1, response.length);
                }
              })
              this.queryParams = tempQueryArray.join('&');
            } else {
              this.queryParams = this.sortingqueryparam + response;
            }
            this.current_page = 1;
            this.curPageValue = 1;
            this.searchPropertySort('editMode', this.queryParams);
          }
        });
    } else {
      this.current_page = 1;
      this.curPageValue = 1;
      this.sortDetails(value);
    }
  }

  searchPropertySort(flag, value: any) {
    if (flag == 'pagination') {
      this.queryValue = this.queryParams;
      this.queryParams = this.queryParams + "&page=" + value;
    } else {
      if (flag == 'savesearch') {
        this.queryParams = value;
        this.reset = false;
      } else {
        this.queryParams = value;
      }
      this.sortingselected = false;
      if (this.artistChecked) {
        this.checkedArtist.forEach(data => {
          if (this.queryParams.indexOf(data.id) == -1) {
            this.queryParams += "&artistIds=" + data.id;
          }
        });
      }
      this.queryValue = this.queryParams;
      this.saveAllQueryParams = this.queryParams;
      this.queryParams = this.queryParams + '&lod=MIN';
      // Session Storage of 'search' parameters to create 'saved search' and 'saved list'.
      sessionStorage.removeItem('searchparam');
      sessionStorage.setItem("searchparam", this.saveAllQueryParams.substring(0, this.saveAllQueryParams.indexOf('&artistCatalogueRaisonee')));
      // Set to --false-- when object search is performed.
      if (!sessionStorage.getItem('savelistview')) {
        this.resetSaveListSession();
      }
    }
    document.getElementsByTagName("html")[0].classList.add("loading");
    if (this.saveListView) {
      this.queryParams = this.queryValue;
      if (flag == "pagination") {
        value = parseInt(value) - 1;
        value = (this.curPageValue - 1) % 20;
      }
      else {
        value = 0;
      }
      this.getSavedListProperties(value);
    }
    else {
      this.sortingqueryparam = this.queryParams;
      this._objectService
        .getProperties(this.queryParams)
        .subscribe(Response => {
          this.flag = 'editMode';
          let properties = Response.content;
          this.queryParams = this.queryValue;
          this.total_pages = Response.totalPages;
          this.saveAllTotalElements = Response.totalElements;
          // Display property details in Object page
          if (flag == "pagination") {
            value = parseInt(value) - 1;
            value = (this.curPageValue - 1) % 20;
          } else {
            value = 0;
          }
          if (Response.content.length > 0) {
            this.total_pages = Response.totalElements;
            this.pageData = Response.content;
            this.singleQuery = { id: Response.content[value].id };
            this.displayPropertyDetails(this.singleQuery);
            if (sessionStorage.getItem('selectSort') && sessionStorage.getItem('selectSort').length > 0) {
              this.sorted = true;
              this.highlightSortIcon(this.sortOrder);
            }
          } else {
            this.reset = true;
            this.editModeFlag = true;
            this.resetFullForm();
            this.emptyPagination = 'No records found';
            this.appendSearchData();
            this.widthStyle = 115;
            this.sorted = false;
            this.highlightSortIcon(null);
          }
        }, resFileError => {
          this.errorMsg = resFileError;
          this.sorted = false;
          this.highlightSortIcon(null);
        });
    }
  }

  public progressBarNo = 0;
  public progressBarPercentage = 0;
  public totalRecords = 0;
  onprogressnumber(event) {
    this._savedListBackgroundService.add(event);
    // this.showDialog();
    // this.progressBarNo = event.valuesadded;
    // this.progressBarPercentage = event.percentage;
    // this.totalRecords = event.totalRecords;		 
  }

  onRemoveDialog() {
    this.removeDialog();
  }

  onPaste(event) {
    if (parseInt(event.clipboardData.getData( 'text/plain' )) < 0 || isNaN(parseInt(event.clipboardData.getData( 'text/plain' ))))
      return false;
    return true;
  }

  onSignaturePaste(event) {
    if (event.clipboardData.getData('text/plain').length > 500)
      return false;
    return true;
  }

  // 'Caret' (select list) click event 
  private openList(list, listName) {
    if (listName == 'category') {
      this.categoryListOpen = !this.categoryListOpen;
      if (this.categoryListOpen === true) {
        list.open();
        this.categoryService.search('');
      }
      else {
        list.close();
      }
    }
    if (listName == 'subcategory') {
      this.subCategoryListOpen = !this.subCategoryListOpen;
      if (this.subCategoryListOpen === true) {
        list.open();
        this.subCategoryService.search('');
      }
      else {
        list.close();
      }
    }
    if (listName == 'presizename') {
      this.presizeNameListOpen = !this.presizeNameListOpen;
      if (this.presizeNameListOpen === true) {
        list.open();
        this.presizeTypesService.search('');
      }
      else {
        list.close();
      }
    }
    if (listName == 'editionname') {
      this.editionNameListOpen = !this.editionNameListOpen;
      if (this.editionNameListOpen === true) {
        list.open();
        this.editionTypeService.search('');
      }
      else {
        list.close();
      }
    }
    if (listName == 'flagscountry') {
      this.flagsCountryListOpen = !this.flagsCountryListOpen;
      if (this.flagsCountryListOpen === true) {
        list.open();
        this.countryService.search('');
      }
      else {
        list.close();
      }
    }
    if (listName == 'imageFigure') {
      this.imageFigureListOpen = !this.imageFigureListOpen;
      if (this.imageFigureListOpen === true) {
        list.open();
        this.imageFigureService.search('');
      }
      else {
        list.close();
      }
    }
    if (listName == 'autoOrientation') {
      this.autoOrientationListOpen = !this.autoOrientationListOpen;
      if (this.autoOrientationListOpen === true) {
        list.open();
        this.autoOrientationService.search('');
      }
      else {
        list.close();
      }
    }
    if (listName == 'autoScale') {
      this.autoScaleListOpen = !this.autoScaleListOpen;
      if (this.autoScaleListOpen === true) {
        list.open();
        this.autoScaleService.search('');
      }
      else {
        list.close();
      }
    }
    if (listName == 'autoImage') {
      this.autoImageListOpen = !this.autoImageListOpen;
      if (this.autoImageListOpen === true) {
        list.open();
        this.autoImageService.search('');
      }
      else {
        list.close();
      }
    }
  }

  // Close drop-down lists
  closeList(list, listName) {
    list.close();
    if (listName == 'category') {
      this.categoryListOpen = !this.categoryListOpen;
    }
    if (listName == 'subcategory') {
      this.subCategoryListOpen = !this.subCategoryListOpen;
    }
    if (listName == 'presizename') {
      this.presizeNameListOpen = !this.presizeNameListOpen;
    }
    if (listName == 'editionname') {
      this.editionNameListOpen = !this.editionNameListOpen;
    }
    if (listName == 'flagscountry') {
      this.flagsCountryListOpen = !this.flagsCountryListOpen;
    }
    if (listName == 'imageFigure') {
      this.imageFigureListOpen = !this.imageFigureListOpen;
    }
    if (listName == 'autoOrientation') {
      this.autoOrientationListOpen = !this.autoOrientationListOpen;
    }
    if (listName == 'autoScale') {
      this.autoScaleListOpen = !this.autoScaleListOpen;
    }
    if (listName == 'autoImage') {
      this.autoImageListOpen = !this.autoImageListOpen;
    }
  }

  // Alert displayed when saved list size is > 50000 objects.
  private alertLargeSizedSavedList(addType) {
    document.getElementsByTagName("body")[0].classList.add("positionFixed");
    if (!(this.saveAllTotalElements == undefined && window.location.pathname.indexOf('object') > -1)) {
      if (addType == 'selected' && sessionStorage.getItem('thumbnailselectall') == 'true' && !(sessionStorage.getItem("objectsUnchecked"))) {
        this.openSaveListModal = false;
        this.dialogService.addDialog(AlertComponent,
          { title: 'Large Sized List', message: 'There was an error saving your list. The max number of records you can add to a list is 50,000.' }, { backdropColor: 'rgba(220,220,220,0.5)' })
          .subscribe((isConfirmed) => { document.getElementsByTagName("body")[0].classList.remove("positionFixed"); });
      }
      else if (addType == 'selected' &&
        (((!sessionStorage.getItem('addtosavelistobjects') && sessionStorage.getItem('thumbnailselectall') != 'true') && window.location.pathname.indexOf('thumbnail') > -1)
          || !(sessionStorage.getItem('addtosavelistobjects').length > 0 || sessionStorage.getItem('objectsUnchecked').length > 0))) {
        this.openSaveListModal = false;
        this.dialogService.addDialog(AlertComponent,
          { title: 'Select Properties', message: 'No objects selected. Please select the objects to be added to the list.' }, { backdropColor: 'rgba(220,220,220,0.5)' })
          .subscribe((isConfirmed) => { document.getElementsByTagName("body")[0].classList.remove("positionFixed"); });
      }
      else {
        if ((sessionStorage.getItem("typeSelected") == "true")) {
          document.getElementById('savedlistpopup').click();

        } else {
          this.openSaveListModal = false;
          this.dialogService.addDialog(AlertComponent,
            { title: 'Large Sized List', message: 'There was an error saving your list. The max number of records you can add to a list is 50,000.' }, { backdropColor: 'rgba(220,220,220,0.5)' })
            .subscribe((isConfirmed) => { document.getElementsByTagName("body")[0].classList.remove("positionFixed"); });
        }
      }
    }
    else {
      if (this.saveAllTotalElements == undefined && window.location.pathname.indexOf('object') > -1) {
        this.openSaveListModal = false;
        this.dialogService.addDialog(AlertComponent,
          { title: 'Large Sized List', message: 'There was an error saving your list. The max number of records you can add to a list is 50,000.' }, { backdropColor: 'rgba(220,220,220,0.5)' })
          .subscribe((isConfirmed) => { document.getElementsByTagName("body")[0].classList.remove("positionFixed"); });
      }

    }
  }


  // Display error message if saved list size is > 50000 objects.
  private savedListSize = function (addType) {

    if ((sessionStorage.getItem("typeSelected") == "true")) {
      return false;
    }


    let propertiesToBeRemoved;
    let listSize = 0;

    // Check if objects were unchecked after '--Select All--' option was selected
    if (window.location.pathname.indexOf("thumbnail") > 0) {
      if ((sessionStorage.getItem("objectsUnchecked")) && (sessionStorage.getItem("objectsUnchecked").split(',').length >= 1)) {
        propertiesToBeRemoved = sessionStorage.getItem("objectsUnchecked").split(',');
        propertiesToBeRemoved.splice(propertiesToBeRemoved.length - 1, 1);
      }
    }
    listSize = parseInt(sessionStorage.getItem('totalelements')) ? parseInt(sessionStorage.getItem('totalelements')) : 0;
    if (propertiesToBeRemoved && propertiesToBeRemoved.length > 0) {
      listSize = listSize > 5000 && propertiesToBeRemoved.length < listSize ? listSize - propertiesToBeRemoved.length : listSize;
    }
    if (listSize > 50000) {
      if (addType == 'selected' && sessionStorage.getItem('thumbnailselectall') == 'true' && (sessionStorage.getItem('objectsUnchecked') && sessionStorage.getItem('objectsUnchecked').length > 0)) {
        return true;
      }
      else if (addType == 'selected' && (sessionStorage.getItem('addtosavelistobjects') && sessionStorage.getItem('addtosavelistobjects').length > 0)) {
        return false;
      }
      return true;
    }
    return false;
  }

  private modalAddObjectToItem(screen) {
    let objectsToAdd = null;
    if (screen === 'single') {
      objectsToAdd = [this.objectId]
    } else {
      if (!sessionStorage.getItem('addtosavelistobjects')) {
        this.showMessage(this.dictionary.NOTIFICATIONS.NO_OBJECT_SELECTED,
          _.toLower(this.dictionary.NOTIFICATIONS.error));
        return;
      } else {
        objectsToAdd = sessionStorage.getItem('addtosavelistobjects').split(',');
      }
    }
    this.createItemDialog(objectsToAdd);
  }

  createItemDialog(objectsToAdd, itemId = '') {
    this.dialogService.addDialog(AddObjectsItemComponent, { itemId }).take(1)
      .subscribe(result => {
        if (!result) {
          return;
        }
        if (result.value) {
          let elementSubscribe, errorSubscribe;
          this.ItemSvc.getElement(result.value);
          elementSubscribe = this.ItemSvc.element.skip(1).take(1).subscribe((res) => {
            if (res) {
              this.addObjectToItem(res, objectsToAdd)
              errorSubscribe.unsubscribe();
            }
          });
          errorSubscribe = this.ItemSvc.error.skip(1).take(1).subscribe((res) => {
            this.createItemDialog(objectsToAdd, result.value);
            this.showMessage(getErrorStringFromServer(res, this.dictionary.NOTIFICATIONS.ITEM_NOT_FOUND),
              _.toLower(this.dictionary.NOTIFICATIONS.error));
            elementSubscribe.unsubscribe();
          });

        } else {
          this.showMessage(this.dictionary.NOTIFICATIONS.EMPTY_ID,
            _.toLower(this.dictionary.NOTIFICATIONS.error));
        }
      })
  }

  private addObjectToItem(item, objectsToAdd) {
    if (item && objectsToAdd) {
      for (const obj in objectsToAdd) {
        if (item.objects) {
          if (objectsToAdd[obj]) {
            if (!this.existsObjectId(objectsToAdd[obj], item.objects)) {
              item.objects.push({ objectId: objectsToAdd[obj] });
            }
          }
        } else {
          item.objects = [{ objectId: objectsToAdd[obj] }];
        }
      }
      this.ItemSvc.amend({ ...item, id: null }, item.id).subscribe((response) => {
        this.showMessage(this.dictionary.NOTIFICATIONS.OBJECT_ADDED_ITEM_SUCCESSFULLY,
          _.toLower(this.dictionary.NOTIFICATIONS.SUCCESS));
      }, error => {
        this.showMessage(this.dictionary.NOTIFICATIONS.OBJECT_ADDED_ITEM_ERROR,
          _.toLower(this.dictionary.NOTIFICATIONS.error));
      })
    }
  }

  protected showMessage(content, type) {
    if (type === _.toLower(this.dictionary.NOTIFICATIONS.SUCCESS)) {
      this.tosatservice.addToast(content, GeneralConstants.ADD_OBJECTS_ITEM.TOP_RIGHT);
    } else {
      this.tosatservice.addToastError(content, GeneralConstants.ADD_OBJECTS_ITEM.TOP_RIGHT);
    }
  }

  private existsObjectId(objectId, objectArray) {
    return _.find(objectArray, o => o.objectId === objectId)
  }


  // Record last CR for multiple artists in sessionStorage.
  getLastCR() {
    let lastCR = '';
    this.lastCR.forEach((artist, index) => {
      if (index === this.lastCR.length - 1) {
        lastCR += artist.CRId;
      }
      else {
        lastCR += artist.CRId + ",";
      }
    });
    sessionStorage.setItem('lastcr', lastCR);
  }

  getLocalDate(utcDate) {
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    let month = new Date(utcDate).getMonth();
    let currentMonth = months[month];
    return new Date(utcDate).getDate() + '-' + currentMonth + '-' + new Date(utcDate).getFullYear()
  }
  validateEnteredTextForSearch(event) {
    if ((event.keyCode == 65 && (event.ctrlKey === true || event.metaKey === true)) ||
      (event.keyCode == 67 && (event.ctrlKey === true || event.metaKey === true)) ||
      (event.keyCode == 88 && (event.ctrlKey === true || event.metaKey === true)) ||
      (event.keyCode == 86) && (event.ctrlKey === true || event.metaKey === true)) {
      return;
    }
    if (!((parseInt(event.key) >= 0 && parseInt(event.key) <= 9) || event.key == "*" || event.key == "." || event.key == "Tab" || event.key == "Backspace"
      || event.key == "ArrowUp" || event.key == "ArrowRight" || event.key == "ArrowLeft" || event.key == "ArrowDown" || event.key == "Control")) {
      return false;
    }
  }

  onPasteSearchMode(event) {
    if (event.clipboardData.getData('text/plain') == '*') {
      return;
    }
    if (!(isNaN(parseInt(event.clipboardData.getData('text/plain'))))) {
      return;
    }
    return false;
  }

  validateDataOnSearch(type) {
    if (this.headerFlag == "search" && this.objectDetailsForm.controls[type].value.length == 1 && this.objectDetailsForm.controls[type].value == '*') {
      this.objectDetailsForm.controls[type].setErrors(null);
    } else {
      return
    }
  }
  // EOS-838 - Reject new values in autocomplete list (Artist Name field).
  resetArtistNameInput() {
    if (!(this.headerFlag == "search" && this.objectDetailsForm.controls['artistData'].value == '*')) {
      this.objectDetailsForm.controls['artistData'].setValue('');
    }
  }
  //Search on thumbnails page
  onSearch(flag: string) {
    if (flag === "backSearch") {
      this.router.navigate(['/object'], { queryParams: { backSearch: `lastSearch` } });
    } else if (flag === "clearSearch") {
      this.router.navigate(['/object'], { queryParams: { clearSearch: `clearSearch` } });
    } else if (flag === "createSearch") {
      document.getElementsByTagName("body")[0].classList.add("positionFixed");
      this.dialogService.addDialog(ObjectModalComponent, { modeFlag: 'createMode' }, { backdropColor: 'rgba(220,220,220,0.5)' })
        .subscribe((message) => {
          document.getElementsByTagName("body")[0].classList.remove("positionFixed");
        });
    }
  }

  resetSubjectNameInput() {
    this.objectDetailsForm.controls['tags'].patchValue({
      'imageSubjects': null
    });
  }
  resetAnimalNameInput() {
    this.objectDetailsForm.controls['tags'].patchValue({
      'imageAnimals': null
    });
  }
  resetOtherTagNameInput() {
    this.objectDetailsForm.controls['tags'].patchValue({
      'otherTags': null
    });
  }
  resetSitterNameInput() {
    this.objectDetailsForm.controls['tags'].patchValue({
      'expertiseSitterFName': null,
      'expertiseSitterLName': null
    });
  }
  appendSearchData() {
    var property;
    if (!this.searchData) {
      if (sessionStorage.getItem("searchData") != "null") {
        property = JSON.parse(sessionStorage.getItem("searchData"));
      } else {
        property = this.searchData;
      }
    } else {
      property = this.searchData;
    }
    this.headerFlag = 'search';
    window.history.replaceState(null, null, "object");
    this.checkedArtist = [];
    this.artistName.splice(0, this.artistName.length);
    if (this.searchData && this.searchData.checkedArtist && this.searchData.checkedArtist.length) {
      this.searchData.checkedArtist.forEach(data => {
        if (data && data.id) {
          var bd = ((data.birthYear && data.deathYear) ? "(" + data.birthYear + "-" + data.deathYear + ")" : (data.birthYear ? "(b." + data.birthYear + ")" : ''));
          this.artistName.push({
            firstName: (data.firstName ? data.firstName + ' ' : '') + data.lastName,
            display: data.display ? data.display : ((data.firstName ? data.firstName + ' ' : '') + data.lastName + bd),
            birthYear: data.birthYear,
            deathYear: data.deathYear,
            id: data.id
          });
        }
      });
    } else {
      if (sessionStorage.getItem("searchData") != "null" && !this.searchData && property) {
        property.checkedArtist.forEach(data => {
          if (data && data.id) {
            var bd = ((data.birthYear && data.deathYear) ? "(" + data.birthYear + "-" + data.deathYear + ")" : (data.birthYear ? "(b." + data.birthYear + ")" : ''));
            this.artistName.push({
              firstName: (data.firstName ? data.firstName + ' ' : '') + data.lastName,
              display: data.display ? data.display : ((data.firstName ? data.firstName + ' ' : '') + data.lastName + bd),
              birthYear: data.birthYear,
              deathYear: data.deathYear,
              id: data.id
            });
          }
        });
      }
    }
    this.checkedArtist = this.checkedArtist.concat(this.artistName);
    this.objectDetailsForm.controls['title'].setValue(property && property.title ? property.title : null);
    this.objectDetailsForm.controls['originalTitle'].setValue(property && property.originalTitle ? property.originalTitle : null);
    this.objectDetailsForm.controls['signature'].setValue(property && property.signature ? property.signature : null);
    this.objectDetailsForm.controls['medium'].setValue(property && property.medium ? property.medium : null);
    this.objectDetailsForm.controls['category'].setValue(property && property.category ? property.category : null);
    this.objectDetailsForm.controls['subCategory'].setValue(property && property.subCategory ? property.subCategory : null);
    this.objectDetailsForm.controls['presizeName'].setValue(property && property.presizeName ? property.presizeName : null);
    this.objectDetailsForm.controls['parts'].setValue(property && property.parts ? property.parts : null);
    this.objectDetailsForm.controls['heightCm'].setValue(property && property.heightCm ? property.heightCm : null);
    this.objectDetailsForm.controls['widthCm'].setValue(property && property.widthCm ? property.widthCm : null);
    this.objectDetailsForm.controls['depthCm'].setValue(property && property.depthCm ? property.depthCm : null);
    this.objectDetailsForm.controls['heightIn'].setValue(property && property.heightIn ? property.heightIn : null);
    this.objectDetailsForm.controls['widthIn'].setValue(property && property.widthIn ? property.widthIn : null);
    this.objectDetailsForm.controls['depthIn'].setValue(property && property.depthIn ? property.depthIn : null);
    if (property && property.heightCm) {
      this.CmToInViceVersaChange('heightIn', 'CmToIn', property.heightCm);
    }
    if (property && property.widthCm) {
      this.CmToInViceVersaChange('widthIn', 'CmToIn', property.widthCm);
    }
    if (property && property.depthCm) {
      this.CmToInViceVersaChange('depthIn', 'CmToIn', property.depthCm);
    }
    this.objectDetailsForm.controls['sizeText'].setValue(property && property.sizeText ? property.sizeText : null);
    this.objectDetailsForm.controls['yearStart1'].setValue(property && property.yearStart1 ? property.yearStart1 : null);
    this.objectDetailsForm.controls['yearStart'].setValue(property && property.yearStart ? property.yearStart : null);
    this.objectDetailsForm.controls['yearEnd'].setValue(property && property.yearEnd ? property.yearEnd : null);
    this.objectDetailsForm.controls['castYearStart'].setValue(property && property.castYearStart ? property.castYearStart : null);
    this.objectDetailsForm.controls['castYearEnd'].setValue(property && property.castYearEnd ? property.castYearEnd : null);
    this.objectDetailsForm.controls['yearCirca'].setValue(property && property.yearCirca ? property.yearCirca : null);
    this.objectDetailsForm.controls['castYearCirca'].setValue(property && property.castYearCirca ? property.castYearCirca : null);
    this.objectDetailsForm.controls['posthumous'].setValue(property && property.posthumous ? property.posthumous : null);
    this.objectDetailsForm.controls['yearText'].setValue(property && property.yearText ? property.yearText : null);
    this.objectDetailsForm.controls['editionNumber'].setValue(property && property.editionNumber ? property.editionNumber : null);
    this.objectDetailsForm.controls['editionName'].setValue(property && property.editionName ? property.editionName : null);
    this.objectDetailsForm.controls['sortCode'].setValue(property && property.sortCode ? property.sortCode : null);
    this.objectDetailsForm.controls['certificate'].setValue(property && property.certificate ? property.certificate : null);
    this.objectDetailsForm.controls['signed'].setValue(property && property.signed ? property.signed : null);
    this.objectDetailsForm.controls['stamped'].setValue(property && property.stamped ? property.stamped : null);
    this.objectDetailsForm.controls['authentication'].setValue(property && property.authentication ? property.authentication : null);
    this.objectDetailsForm.controls['flags'].setValue({
      'flagAuthenticity': (property && property.flags && property.flags.flagAuthenticity) ? property.flags.flagAuthenticity : null,
      'flagRestitution': (property && property.flags && property.flags.flagRestitution) ? property.flags.flagRestitution : null,
      'flagSfs': (property && property.flags && property.flags.flagSfs) ? property.flags.flagSfs : null,
      'flagCondition': (property && property.flags && property.flags.flagCondition) ? property.flags.flagCondition : null,
      'flagMedium': (property && property.flags && property.flags.flagMedium) ? property.flags.flagMedium : null,
      'flagsCountry': (property && property.flags && property.flags.flagsCountry) ? property.flags.flagsCountry : null
    });
    this.flagsCountryValue = (property && property.flags && property.flags.flagsCountry) ? property.flags.flagsCountry : null;
    this.objectDetailsForm.controls['editionSizeTotal'].setValue(property && property.editionSizeTotal ? property.editionSizeTotal : null);
    this.objectDetailsForm.controls['weight'].setValue(property && property.weight ? property.weight : null);
    this.objectDetailsForm.controls['foundry'].setValue(property && property.foundry ? property.foundry : null);
    // Archive Numbers
    this.objectDetailsForm.controls['archiveNumbers'].setValue("");
    this.archiveNoTags = [];
    if (property) {
      for (let i in property.archiveNumbers) {
        this.archiveNoTags.push(property.archiveNumbers[i].display);
      }
    }
    this.objectDetailsForm.controls['researcherName'].setValue(property && property.researcherName ? property.researcherName : null);
    if (property && property.researcherNotesDate) {
      this.objectDetailsForm.controls['researcherNotesDate'].setValue(property && property.researcherNotesDate ? property.researcherNotesDate : null);
    }
    this.objectDetailsForm.controls['researcherNotesText'].setValue(property && property.researcherNotesText ? property.researcherNotesText : null);
    this.objectDetailsForm.controls['researchCompleteCheck'].setValue(property && property.researchCompleteCheck ? property.researchCompleteCheck : false);
    this.objectDetailsForm.controls['volume'].setValue(property && property.volume ? property.volume : null);
    this.objectDetailsForm.controls['number'].setValue(property && property.number ? property.number : null);
    this.objectDetailsForm.controls['artistCatalogueRaisoneeAuthor'].setValue(property && property.artistCatalogueRaisoneeAuthor ? property.artistCatalogueRaisoneeAuthor : null);
    // this.requiredFlag = true;
    this.propertyCatalogRaisonnee = [];
    if (property && property.propertyCatalogueRaisonees) {
      property.propertyCatalogueRaisonees.forEach(data => {
        if (data.artistCatalogueRaisoneeType == "" && data.artistCatalogueRaisoneeAuthor == "" && data.volume == "" && data.number == "") {
          return;
        }
        else {
          this.propertyCatalogRaisonnee.push(data);
        }
      });
    }

    this.selectedGender = [];
    if (property && property.tags && property.tags.imageGenders) {
      property.tags.imageGenders.forEach(x => {
        this.genderValChange(x);
      });
    }
    this.selectedPosition = [];
    if (property && property.tags && property.tags.imagePositions) {
      property.tags.imagePositions.forEach(x => {
        this.positionValChange(x);
      });
    }
    this.selectedMainColor = [];
    if (property && property.tags && property.tags.imageMainColours) {
      property.tags.imageMainColours.forEach(x => {
        this.mainColorsValChange(x);
      });
    }

    this.selectedSitter = [];
    var sitterList = (property && property.tags && property.tags.expertiseSittersFullList) ? property.tags.expertiseSittersFullList : [];
    sitterList.forEach(data => {
      if (data && data.name) {
        this.selectedSitter.push(data);
      }
    });
    this.selectedAnimal = [];
    var animalList = (property && property.tags && property.tags.imageAnimals) ? property.tags.imageAnimals : [];
    animalList.forEach(data => {
      if (data) {
        this.selectedAnimal.push({ name: data });
      }
    });

    this.selectedSubject = [];
    if (property && property.tags && property.tags.imageSubjects) {
      property.tags.imageSubjects.forEach(x => {
        this.subjectValChange(x);
      });
    }
    this.selectedOtherTag = [];
    var otherTagList = (property && property.tags && property.tags.otherTags) ? property.tags.otherTags : [];
    otherTagList.forEach(data => {
      if (data) {
        this.selectedOtherTag.push({ name: data });
      }
    });

    let areaCm: any = (property && property.tags && property.tags.autoAreaCm) ? (property.tags.autoAreaCm) : null;
    let areaIn: any = (property && property.tags && property.tags.autoAreaIn) ? (property.tags.autoAreaIn) : null;

    this.objectDetailsForm.controls['tags'].patchValue({
      'imageText': (property && property.tags && property.tags.imageText) ? property.tags.imageText : null,
      'imageFigure': (property && property.tags && property.tags.imageFigure) ? property.tags.imageFigure : null,
      'expertiseLocation': (property && property.tags && property.tags.expertiseLocation) ? property.tags.expertiseLocation : null,
      'autoOrientation': (property && property.tags && property.tags.autoOrientation) ? property.tags.autoOrientation : null,
      'autoImage': (property && property.tags && property.tags.autoImage) ? property.tags.autoImage : null,
      'autoScale': (property && property.tags && property.tags.autoScale) ? property.tags.autoScale : null,
      'autoAreaCm': areaCm,
      'autoAreaIn': areaIn
    });

    this.saveListView = false;
    this.resetSaveListSession();

  }

  private checkObjectItems(objectId: string): Observable<boolean> {
    this.eventRService.getItemsByUrl(this.eventRService.getURLToSearch('&size=1', objectId), true);

    return this.eventRService.elements
      .skip(1)
      .take(1)
      .map((items: any) => items.length > 0)
  }

  // EOS-1173 Ascending/Descending Sort Order
  // Sort Ascending
  public sortAscending() {
    if (this.sorted && this.sortOrder != 'asc' && this.total_pages > 1) {
      this.sortOrder = 'asc';
      // Saved List
      if (this.flag == 'savelist' || this.savedlistclicked) {
        if (this.saveListSortParam.indexOf(',asc') === -1) {
          if (this.saveListSortParam.indexOf(',desc') >= 0) {
            this.saveListSortParam = this.saveListSortParam.replace(/,desc/i, ',asc');
          }
        }
        this.curPageValue = 1;
        this.getSavedListProperties(0);
      }
      // Object Search
      else {
        if (this.queryParams.indexOf(',asc') === -1) {
          if (this.queryParams.indexOf(',desc') >= 0) {
            this.queryParams = this.queryParams.replace(/,desc/i, ',asc');
          }
        }
        this.curPageValue = 1;
        this.searchPropertySort('editMode', this.queryParams);
      }

      this.highlightSortIcon('asc');
    }
  }

  // EOS-1173 Ascending/Descending Sort Order
  // Sort Descending
  public sortDescending() {
    if (this.sorted && this.sortOrder != 'desc' && this.total_pages > 1) {
      this.sortOrder = 'desc';
      // Saved List
      if (this.flag == 'savelist' || this.savedlistclicked) {
        if (this.saveListSortParam.indexOf(',desc') === -1) {
          if (this.saveListSortParam.indexOf(',asc') >= 0) {
            this.saveListSortParam = this.saveListSortParam.replace(/,asc/i, ',desc');
          }
        }
        this.curPageValue = 1;
        this.getSavedListProperties(0);
      }
      // Object Search
      else {
        if (this.queryParams.indexOf(',desc') === -1) {
          if (this.queryParams.indexOf(',asc') >= 0) {
            this.queryParams = this.queryParams.replace(/,asc/i, ',desc');
          }
        }
        this.curPageValue = 1;
        this.searchPropertySort('editMode', this.queryParams);
      }
      this.highlightSortIcon('desc');
    }
  }

  highlightSortIcon(sortOrder) {
    let sortIconAsc = document.getElementById('sort-asc') as HTMLImageElement;
    let sortIconDesc = document.getElementById('sort-desc') as HTMLImageElement;
    if (sortOrder === 'asc' && this.total_pages > 1) {
      (<HTMLElement>sortIconAsc).style.color = '#fff';
      (<HTMLElement>sortIconDesc).style.color = '#999';
    }
    else if (sortOrder === 'desc' && this.total_pages > 1) {
      (<HTMLElement>sortIconDesc).style.color = '#fff';
      (<HTMLElement>sortIconAsc).style.color = '#999';
    }
    else {
      (<HTMLElement>sortIconDesc).style.color = '#999';
      (<HTMLElement>sortIconAsc).style.color = '#999';
    }
  }
  //checkbox focus event
  addOutline(event) {
    let parent = event.srcElement.parentElement;
    parent.querySelector(".fa-check").classList.add("outline");
  }
  removeOutline(event) {
    let parent = event.srcElement.parentElement;
    parent.querySelector(".fa-check").classList.remove("outline");
  }
  removeFromList() {
    this._savedListService.getSavedListById(this.keycloakUserName, this.savedListId)
      .subscribe(Response => {
        if (Response.openSaveList || (this.keycloak.tokenParsed.preferred_username === Response.user)) {
          this.dialogService.addDialog(ConfirmComponent, { title: '', message: 'Are you sure you would like to remove this object from Saved List, " ' + Response.name + ' "?' },
            { backdropColor: 'rgba(220,220,220,0.5)' })
            .subscribe((isConfirmed) => {
              if (isConfirmed) {
                if (this.keycloak.tokenParsed.preferred_username == Response.user || (JSON.stringify(Response.sharedUsersList).indexOf(this.keycloak.tokenParsed.preferred_username) > -1 && Response.openSaveList)) {
                  if (Response.user != 'property-importer') {
                    this._savedListService.removeFromList(this.keycloak.tokenParsed.preferred_username, this.savedListId, this.externalId)
                      .subscribe(response => {
                        this.tosatservice.addToast('1 object(s) have been removed from the Saved List, "' + Response.name + ' "', 'top-right');
                        if (this.total_pages === 1 && this.curPageValue - 1 === 0) {
                          this.resetFullForm();
                          this.reset = true;
                          this.editModeFlag = true;
                        } else {
                          this.total_pages = this.total_pages - 1;
                          if (this.curPageValue == 1) {
                            this.searchProperty("pagination", 0);
                          }
                          else {
                            this.pageObject = {
                              pageNo: this.pageNo,
                              pageNumber: this.pageNumber,
                              curPageValue: this.curPageValue,
                              pageData: this.pageData,
                              flagPage: 'prevPage'
                            };
                            this.pageObject = this._paginationService.getNextPage(this.pageObject);
                            this.curPageValue = this.pageObject.curPageValue;
                            this.pageNumber = this.pageObject.pageNo;
                            this.pageNo = this.pageObject.pageNo;
                            this.searchProperty('pagination', this.pageNumber);

                          }

                        }
                      }, resFileError => {

                      })

                  } else {
                    this.tosatservice.addToastError('Objects may not be removed from Import Saved Lists.', 'top-right')
                  }
                } else {
                  this.tosatservice.addToastError('You do not have access to remove objects from this list. Please contact ' + Response.user + ' if you would like to make any changes', 'top-right')
                }
              }
            })
        } else {
          document.getElementsByTagName("body")[0].classList.add("positionFixed");
          document.getElementsByTagName('html')[0].classList.remove('loading');
          this.dialogService.addDialog(AlertComponent,
            { title: 'Warning...!', message: "This list's status is Closed. Please reach out to " + '"' + Response.user + '"' + " to amend list." }, { backdropColor: 'rgba(220,220,220,0.5)' })
            .subscribe((isConfirmed) => {
              document.getElementsByTagName("body")[0].classList.remove("positionFixed");
            });
        }
      })
  }

  gendersClicked() {
    (<HTMLInputElement>document.getElementById('gender-input')).focus();
    (<HTMLInputElement>document.getElementById('gender-input')).value = '';
  }

  positionsClicked() {
    (<HTMLInputElement>document.getElementById('position-input')).focus();
    (<HTMLInputElement>document.getElementById('position-input')).value = '';
  }

  subjectClicked() {
    (<HTMLInputElement>document.getElementById('subject-Data')).focus();
    (<HTMLInputElement>document.getElementById('subject-Data')).value = '';
  }

  imageTextNextline(event) {
    if (event.keyCode == 13) {
      this.objectDetailsForm.controls['tags']['controls']['imageText'].setValue(this.objectDetailsForm.controls['tags']['controls']['imageText'].value == null ? (" " + "\n") : this.objectDetailsForm.controls['tags']['controls']['imageText'].value + "\n")
    }
  }

  onEnterFigure(event) {
    if (event.keyCode == '13') {
      this.onSubmit("editMode", this.objectDetailsForm.value);
    }
  }
}