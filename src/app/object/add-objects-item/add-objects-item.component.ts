import { OnInit, Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { GeneralDictionary } from '../../shared/constants';
import * as _ from 'lodash';
interface Data {
  itemId: any,
}

@Component({
  styleUrls: ['./add-objects-item.component.scss'],
  templateUrl: './add-objects-item.component.html',
})
export class AddObjectsItemComponent extends DialogComponent<Data, any> implements Data, OnInit {

  itemId: any;
  title: string;
  addObjectsItemForm: FormGroup;
  dictionary = GeneralDictionary;

  constructor(public dialogService: DialogService, private fb: FormBuilder) {
    super(dialogService);
    this.title = `${_.upperFirst(_.toLower(GeneralDictionary.DIALOG.ADD_OBJECTS_TO_ITEM.TITLE))}`;
  }

  ngOnInit() {
    this.addObjectsItemForm = this.fb.group({
      itemId: [this.itemId],
    });
  }

  save() {
    this.result = this.getFormData();
    this.close();
  }

  getFormData() {
    return this.addObjectsItemForm.get('itemId');
  }

}
