import { Component, Input, OnDestroy } from '@angular/core'
import { ObjectService } from '../../shared/services'
import { ExternalClientService, PricingItemService, TransactionItemService, PublicationItemService } from '../../event/shared/services'
import { createName, randomlyAddFields } from '../../event/shared/mocks';
import { ObjectComponent } from '../../event/shared/components'
import { Property } from 'app/event/shared/models';
import { GeneralDictionary, GeneralConstants } from 'app/shared/constants';

@Component({
  providers: [ExternalClientService, PricingItemService, TransactionItemService, PublicationItemService,],
  selector: 'app-presentation-view',
  templateUrl: './presentation.component.html',
  styleUrls: ['./presentation.component.scss'],
})
export class PresentationComponent implements OnDestroy {

  private object
  private owner: any
  private latestPricing: any
  private auctions = [];
  private publications = [];
  private ownerSubscription
  private latestPricingSubscription
  private auctionsSubscription
  private publicationsSubscription

  private dictionary = GeneralDictionary
  private eventsDictionary = this.dictionary.EVENT_REPORT.ENTITY_EVENT_FILTER
  private eventsConstants = GeneralConstants.ENTITY_EVENT_FILTER
  private formatDate = GeneralConstants.DATE_FORMAT.formatDatePresentationView
  private dictionaryPricing = GeneralDictionary.EVENT_REPORT.ENTITY_EVENT_FILTER.EVENT_PRICING
  private dictionaryTransaction = GeneralDictionary.EVENT_REPORT.ENTITY_EVENT_FILTER.EVENT_TRANSACTION
  private auctionStatusId = {
    boughtIn: this.eventsDictionary.EVENT_TRANSACTION.BOUGHTIN.replace(/\s+/g, '-').toUpperCase(),
    sold: this.eventsDictionary.EVENT_TRANSACTION.SOLD.toUpperCase()
  }

  @Input() set setObject(object: Property) {
    if (object && typeof object === 'object') {
      this.object = this.objectService.transformElements([object])[0]

      if (this.object.currentOwnerId) {
        this.ownerSubscription = this.clientService.getClients([this.object.currentOwnerId]).subscribe((owner: any) => {
          if (owner.city || owner.state || owner.country || owner.region) {
            const city = owner.city ? owner.city : '';
            const state = owner.state && owner.city ? `, ${owner.state}` : owner.state ? owner.state : '';
            const country = owner.country && (city || state) ? `, ${owner.country}` : owner.country ? owner.country : '';
            const region = owner.region && (city || state || country) ? `, ${owner.region}` : owner.region ? owner.region : '';
            owner.location = `${city}${state}${country}${region}`.toLowerCase()
          }

          this.owner = owner
        })
      } else {
        this.owner = {}
      }


      this.pricingService.getElements({ objects: this.object.id, size: 1, sort: 'eventDate,desc', type: 'PRICING' })

      this.auctionService.getElements({ objects: this.object.id, sort: 'eventDate,asc', type: 'TRANSACTION', transactionType: 'AUCTION' })

      this.publicationService.getElements({ objects: this.object.id, sort: 'eventDate,asc', type: 'PUBLICATION' })
    } else {
      this.resetObject()
    }
  };

  constructor(
    private objectService: ObjectService,
    private clientService: ExternalClientService,
    private pricingService: PricingItemService,
    private auctionService: TransactionItemService,
    private publicationService: PublicationItemService,
  ) {
    this.latestPricingSubscription = this.pricingService.elements
      .map(pricings => pricings.map((pricingItem: any) => ({
        ...pricingItem,
        pipelineAuction: createName(pricingItem.pipelineAuction),
        pipelinePrivateSale: createName(pricingItem.pipelinePrivateSale),
        pipelineExhibition: createName(randomlyAddFields(pricingItem.pipelineExhibition)),
      })))
      .subscribe(latestPricing => this.latestPricing = latestPricing[0])
    this.auctionsSubscription = this.auctionService.elements
      .subscribe(auctions => this.auctions = auctions)
    this.publicationsSubscription = this.publicationService.elements
      .subscribe(publications => this.publications = publications)
  }

  ngOnDestroy() {
    if (this.ownerSubscription) {
      this.ownerSubscription.unsubscribe()
    }

    this.latestPricingSubscription.unsubscribe()
    this.auctionsSubscription.unsubscribe()
    this.publicationsSubscription.unsubscribe()
  }

  resetObject() {
    this.object = {}
    this.owner = {}
    this.latestPricing = {}
    this.auctions = []
    this.publications = []
  }
}
