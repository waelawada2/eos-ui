import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { GeneralConstants, GeneralDictionary } from '../../../shared/constants';

@Component({
  selector: 'app-event-card-transaction-component',
  templateUrl: './event-card-transaction.component.html'
})
export class EventCardsComponent {
  public eventCard: any[] = [];
  public constant = GeneralConstants.ENTITY_EVENT_FILTER;
  public dictionary = GeneralDictionary.EVENT_REPORT.ENTITY_EVENT_FILTER;

  constructor(
    private router: Router
  ) { }


  @Input() set eventItems(eventItem) {
    this.eventCard = eventItem;
  }

  goToEvent(page): void {
    this.router.navigate([page]);
  }
}
