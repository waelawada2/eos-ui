import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventCardPublicationComponent } from './event-card-publication.component';

describe('EventCardPublicationComponent', () => {
  let component: EventCardPublicationComponent;
  let fixture: ComponentFixture<EventCardPublicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventCardPublicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCardPublicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
