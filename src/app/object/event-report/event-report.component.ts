import { AfterViewInit, Component, Input, OnDestroy } from '@angular/core';
import { GeneralConstants, GeneralDictionary } from '../../shared/constants';
import { EventReportService } from '../../shared/services';
import { NotificationsService } from 'angular2-notifications';
import { searchByAnyValue } from '../../event/shared/utils'
import { Subscription } from 'rxjs/Subscription';
import * as _ from 'lodash';
import { AddObjectToEventService } from 'app/event/shared/services';
import { Router } from '@angular/router';
import { ExternalClientService } from '../../event/shared/services/client/external-client.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/concat';


@Component({
  providers: [EventReportService, ExternalClientService],
  selector: 'app-event-report-component',
  templateUrl: './event-report.component.html',
  styleUrls: ['./event-report.component.scss']
})
export class EventReportComponent implements OnDestroy {

  public dictionary = GeneralDictionary;
  public filter: string[] = GeneralConstants.ENTITY_EVENT_FILTER.EVENT_FILTER;
  public currentFilter: any = this.filter[0]; // first will be selected by default by browser
  public eventFilter: string;
  public currentObjectId: string;
  public eventItems: any = [];
  public options = GeneralConstants.NOTIFICATIONS.SETUP;
  public dictionaryCard = this.dictionary.EVENT_REPORT.ENTITY_EVENT_FILTER;
  public eventsSubscription: Subscription
  private data: string;
  private clientData = {};
  private renderOptions = {
    add: true,
    filter: true,
    headline: true,
    height: 730,
    search: true,
    title: true,
  }

  @Input() set objectId(objectId) {
    this.currentObjectId = objectId;
    if (objectId) {
      this.addObjectToEventService.objectId = objectId;
      this.getEventsByObjectId(_.toUpper(this.currentFilter), this.currentObjectId);
      this.doSubscription();
    } else {
      this.eventItems = []
    }
  }

  @Input() set setOptions(_options) {
    if (_options && _.isObject(_options)) {
      this.renderOptions = {
        ...this.renderOptions,
        ..._options,
      }
    }
  }

  constructor(
    private eventRService: EventReportService,
    private notificationSvc: NotificationsService,
    private addObjectToEventService: AddObjectToEventService,
    private clientService: ExternalClientService,
    private router: Router,
  ) {
    this.addObjectToEventService.reset()
  }

  doSubscription() {
    if (this.eventsSubscription) {
      return;
    }
    this.eventsSubscription = this.eventRService.elements
      .subscribe((eventsData) => {
        this.eventItems = [];
        if (eventsData) {
          this.clientService.getClients(this.extractClientIds(eventsData)).
            subscribe(client => this.clientData[client.entityId] = client, null, () => this.updateClientInfo(eventsData));
        }
      })
  }

  private extractClientIds(items: any[]): any[] {
    const clientIds = [];
    items.forEach(item => {
      switch (_.toUpper(item.type)) {
        case GeneralConstants.EVENT_TYPES.PRICING:
          if (item.event.mainClient) {
            clientIds.push(item.event.mainClient.clientId);
          }

          if (item.event.affiliatedClient) {
            clientIds.push(item.event.affiliatedClient.clientId);
          }
          break;
        case GeneralConstants.EVENT_TYPES.PUBLICATION:
          if (!item.consignmentEvent) {
            break;
          }

          if (item.consignmentEvent.mainClient) {
            clientIds.push(item.consignmentEvent.mainClient.clientId);
          }

          if (item.consignmentEvent.affiliatedClient) {
            clientIds.push(item.consignmentEvent.affiliatedClient.clientId);
          }
          break;
        case GeneralConstants.EVENT_TYPES.TRANSACTION:
          if (item.consignmentEvent) {
            if (item.consignmentEvent.mainClient) {
              clientIds.push(item.consignmentEvent.mainClient.clientId);
            }

            if (item.consignmentEvent.affiliatedClient) {
              clientIds.push(item.consignmentEvent.affiliatedClient.clientId);
            }
          }

          if (item.purchaseEvent) {
            if (item.purchaseEvent.mainClient) {
              clientIds.push(item.purchaseEvent.mainClient.clientId);
            }

            if (item.purchaseEvent.affiliatedClient) {
              clientIds.push(item.purchaseEvent.affiliatedClient.clientId);
            }
          }
          break;
      }
    });
    return _.uniq(clientIds);
  }

  private updateClientInfo(eventsData: any[]): void {
    const filterCurrent = this.data ? searchByAnyValue(eventsData, this.data) : eventsData;
    if (filterCurrent.length > 0) {
      this.eventItems = filterCurrent.map(this.setEventTarget.bind(this));
      if (this.eventItems.length === 0) {
        this.notificationSvc.error(GeneralDictionary.NOTIFICATIONS.error, GeneralDictionary.NOTIFICATIONS.notFound,
          GeneralConstants.NOTIFICATIONS.OPTIONS);
      }
    }
  }

  ngOnDestroy() {
    if (this.eventsSubscription) {
      this.eventsSubscription.unsubscribe();
    }
  }

  filterItems(data) {
    this.data = data;
    const options = GeneralConstants.NOTIFICATIONS.OPTIONS;
    const params = (_.toUpper(this.currentFilter) === _.toUpper(GeneralConstants.ENTITY_EVENT_FILTER.ALL))
      ? '&size=1000&sort=eventDate,desc' : `&type=${_.toUpper(this.currentFilter)}&size=1000&sort=eventDate,desc`;
    this.eventRService.getItemsByUrl(this.eventRService.getURLToSearch(params, this.currentObjectId), true)

  }

  getEventsByObjectId(currentFilter: any, objectId: any): void {
    this.data = '';
    this.currentFilter = currentFilter;
    if (objectId) {
      const options = GeneralConstants.NOTIFICATIONS.OPTIONS;
      const params = (_.toUpper(this.currentFilter) === _.toUpper(GeneralConstants.ENTITY_EVENT_FILTER.ALL))
        ? '&size=10&sort=eventDate,desc'
        : `&type=${_.toUpper(this.currentFilter)}&sort=eventDate,desc`;
      this.eventRService.getItemsByUrl(this.eventRService.getURLToSearch(params, objectId), true);
    }
  }

  setEventTarget(eventsData) {
    switch (_.toUpper(eventsData.type)) {
      case GeneralConstants.EVENT_TYPES.PRICING:
        return this.setJsonPricing(eventsData);
      case GeneralConstants.EVENT_TYPES.PUBLICATION:
        return this.setJsonPublication(eventsData);
      case GeneralConstants.EVENT_TYPES.TRANSACTION:
        return this.setJsonTransaction(eventsData);
    }
  }

  private getClient(clientId: string): any {
    return clientId ? this.clientData[clientId] : {};
  }

  private getClientName(object: any, clientProperty: string): string {
    const client = this.getClient(_.get(object, clientProperty));
    return client ? client.name : '';
  }

  private getMainClientName(object: any, clientProperty: string): string {
    const client = this.getClient(_.get(object, clientProperty));
    if (!client) {
      return '';
    }
    const name = _.get(client, 'mainClient.name');
    return name ? ` (${name})` : '';
  }

  private getAffiliatedText(object: any, affiliatedTextProperty: string): string {
    return _.get(object, affiliatedTextProperty) ? `, ${_.get(object, affiliatedTextProperty)} ` : '';
  }

  setJsonTransaction(eventsData) {
    if (_.get(eventsData, 'transactionType') === this.dictionaryCard.EVENT_TRANSACTION.AUCTION) {
      return this.returnAuction(eventsData);
    } else {
      return this.returnPrivateSale(eventsData);
    }
  }

  returnAuction(eventsData) {
    return {
      // Event
      typeEvent: `${_.capitalize(_.get(eventsData, 'type', ''))} (${_.capitalize(_.get(eventsData, 'transactionType', ''))})`,
      date: _.get(eventsData, 'event.date', ''),
      venue: _.get(eventsData, 'event.establishment.name', ''),
      saleTitle: _.get(eventsData, 'event.title', ''),
      lotNumber: _.get(eventsData, 'lotNumber') ?
        `, ${this.dictionaryCard.EVENT_TRANSACTION.LOT} ${_.get(eventsData, 'lotNumber')}` : '',

      // Values
      eor: _.get(eventsData, 'auction.estimateOnRequest') ? `${this.dictionaryCard.EVENT_TRANSACTION.EOR} ` : '',
      currency: _.get(eventsData, 'event.currency.code', ''),
      lowEstimate: _.get(eventsData, 'auction.lowEstimate') ?
        ` ${_.get(eventsData, 'auction.lowEstimate')} -` : '',
      highEstimate: _.get(eventsData, 'auction.highEstimate') ?
        ` ${_.get(eventsData, 'auction.highEstimate')} ${this.dictionaryCard.EVENT_TRANSACTION.SALE_TERM}` : '',
      valuesShow: !!(_.get(eventsData, 'event.currency.code', '') && !!_.get(eventsData, 'auction.lowEstimate', '')
        && !!_.get(eventsData, 'auction.highEstimate', '')),
      // Results
      status: _.get(eventsData, 'status.name', ''),
      hammer: _.get(eventsData, 'auction.hammer') ?
        ` ${_.get(eventsData, 'event.currency.code')} ${_.get(eventsData, 'auction.hammer')} ${this.dictionaryCard.EVENT_TRANSACTION.H},`
        : '',
      purchasersPremium: _.get(eventsData, 'auction.premium') ?
        ` ${_.get(eventsData, 'event.currency.code')} ${_.get(eventsData, 'auction.premium')} ${this.dictionaryCard.EVENT_TRANSACTION.BP}`
        : '',
      resultsShow: !!(_.get(eventsData, 'status.name', '') && !!_.get(eventsData, 'auction.hammer', '')
        && !!_.get(eventsData, 'auction.premium', '')),
      // Source
      consignorSource: _.get(eventsData, 'consignmentEvent.mainClient.clientSource.name', ''),
      // Consignor
      consignorClient: this.getClientName(eventsData, 'consignmentEvent.mainClient.clientId'),
      consignorMainClient: _.get(eventsData, 'consingmentEvent.mainClient.clientId') !==
        _.get(this.getClient(_.get(eventsData, 'consignmentEvent.mainClient.clientId')), 'mainClient.clientId') ?
        this.getMainClientName(eventsData, 'consignmentEvent.mainClient.clientId') : '',
      consignorAffiliationText: this.getAffiliatedText(eventsData, 'consignmentEvent.affiliatedClient.affiliatedType.name'),
      consignorAffiliatedClient: this.getClientName(eventsData, 'consignmentEvent.affiliatedClient.clientId'),
      // Source
      purchaserSource: _.get(eventsData, 'purchaseEvent.mainClient.clientSource.name', ''),
      // Purchaser
      purchaserClient: this.getClientName(eventsData, 'purchaseEvent.mainClient.clientId'),
      purchaserMainClient: _.get(eventsData, 'purchaseEvent.mainClient.clientId') !==
        _.get(this.getClient(_.get(eventsData, 'purchaseEvent.mainClient.clientId')), 'mainClient.clientId') ?
        this.getMainClientName(eventsData, 'purchaseEvent.mainClient.clientId') : '',
      purchaserAffiliationText: this.getAffiliatedText(eventsData, 'purchaseEvent.affiliatedClient.affiliatedType.name'),
      purchaserAffiliatedClient: this.getClientName(eventsData, 'purchaseEvent.affiliatedClient.clientId'),
      // Notes
      notes: _.get(eventsData, 'comments', ''),
      url: _.get(eventsData, 'event._links.self.href', ''),
      transactionType: _.get(eventsData, 'transactionType', ''),
      id: `/transaction/${_.get(eventsData, 'event.id', '')}`,
      type: _.get(eventsData, 'type', ''),
    };
  }

  returnPrivateSale(eventsData) {
    return {
      // Event
      typeEvent: `${_.capitalize(_.get(eventsData, 'type', ''))} (${_.capitalize(_.startCase(_.get(eventsData, 'transactionType', '')))})`,
      date: _.get(eventsData, 'event.date', ''),
      venue: _.get(eventsData, 'event.establishment.name', ''),
      saleTitle: _.get(eventsData, 'event.title', ''),
      // Values
      netVendor: _.get(eventsData, 'privateSale.netVendor') ?
        `${_.get(eventsData, 'event.currency.code')} ${_.get(eventsData, 'privateSale.netVendor')}
       ${this.dictionaryCard.EVENT_TRANSACTION.V}` : '',
      netPurchaser: _.get(eventsData, 'privateSale.netBuyer') ?
        `, ${_.get(eventsData, 'event.currency.code')} ${_.get(eventsData, 'privateSale.netBuyer')}
       ${this.dictionaryCard.EVENT_TRANSACTION.B}` : '',
      netShow: !!(_.get(eventsData, 'privateSale.netVendor', '')
        && !!_.get(eventsData, 'privateSale.netBuyer', '')
        && !!_.get(eventsData, 'event.currency.code', '')),
      // Results
      status: _.get(eventsData, 'status.name', ''),
      // Source
      consignorSource: _.get(eventsData, 'consignmentEvent.mainClient.clientSource.name', ''),
      // Consignor
      consignorClient: this.getClientName(eventsData, 'consignmentEvent.mainClient.clientId'),
      consignorMainClient: _.get(eventsData, 'consingmentEvent.mainClient.clientId') !==
        _.get(this.getClient(_.get(eventsData, 'consignmentEvent.mainClient.clientId')), 'mainClient.clientId') ?
        this.getMainClientName(eventsData, 'consignmentEvent.mainClient.clientId') : '',
      consignorAffiliationText: this.getAffiliatedText(eventsData, 'consignmentEvent.affiliatedClient.affiliatedType.name'),
      consignorAffiliatedClient: this.getClientName(eventsData, 'consignmentEvent.affiliatedClient.clientId'),
      // Source
      purchaserSource: _.get(eventsData, 'purchaseEvent.mainClient.clientSource.name', ''),
      // Purchaser
      purchaserClient: this.getClientName(eventsData, 'purchaseEvent.mainClient.clientId'),
      purchaserMainClient: _.get(eventsData, 'purchaseEvent.mainClient.clientId') !==
        _.get(this.getClient(_.get(eventsData, 'purchaseEvent.mainClient.clientId')), 'mainClient.clientId') ?
        this.getMainClientName(eventsData, 'purchaseEvent.mainClient.clientId') : '',
      purchaserAffiliationText: this.getAffiliatedText(eventsData, 'purchaseEvent.affiliatedClient.affiliatedType.name'),
      purchaserAffiliatedClient: this.getClientName(eventsData, 'purchaseEvent.affiliatedClient.clientId'),

      // Notes
      notes: _.get(eventsData, 'comments', ''),
      url: _.get(eventsData, 'event._links.self.href', ''),
      transactionType: _.get(eventsData, 'transactionType', ''),
      id: `/transaction/${_.get(eventsData, 'event.id', '')}`,
      type: _.get(eventsData, 'type'),
    };
  }

  setJsonPricing(eventsData) {
    return {
      // Values
      typeEvent: `${_.capitalize(_.get(eventsData, 'type', ''))} (${_.capitalize(_.get(eventsData, 'event.valuationType.name', ''))})`,
      date: _.get(eventsData, 'event.date', ''),
      currency: _.get(eventsData, 'auctionCurrency.code', ''),
      auctionLow: _.get(eventsData, 'auctionLowValue', ''),
      auctionHigh: _.get(eventsData, 'auctionHighValue', ''),
      currencLowHigh: `${_.get(eventsData, 'auctionCurrency.code')} ${_.get(eventsData, 'auctionLowValue')}
       - ${_.get(eventsData, 'auctionHighValue')} ${this.dictionaryCard.EVENT_PRICING.A}`,
      auctionShow: !!(_.get(eventsData, 'auctionCurrency.code', '') && !!_.get(eventsData, 'auctionHighValue', '')
        && !!_.get(eventsData, 'auctionLowValue', '')),

      privateSaleCode: _.get(eventsData, 'privateSaleCurrency.code', ''),
      privateSaleValue: _.get(eventsData, 'privateSaleNetSeller', ''),
      privatePS: `${_.get(eventsData, 'privateSaleCurrency.code')} ${_.get(eventsData, 'privateSaleNetSeller')}
       ${this.dictionaryCard.EVENT_PRICING.PS}`,
      privateShow: !!(_.get(eventsData, 'privateSaleCurrency.code', '') && !!_.get(eventsData, 'privateSaleNetSeller', '')),

      fmvCode: _.get(eventsData, 'fairMarketValueCurrency.code', ''),
      fmvValue: _.get(eventsData, 'fairMarketValuePrice', ''),
      fmv: `${_.get(eventsData, 'fairMarketValueCurrency.code')} ${_.get(eventsData, 'fairMarketValuePrice')}
       ${this.dictionaryCard.EVENT_PRICING.FMV}`,
      fmvShow: !!(_.get(eventsData, 'fairMarketValueCurrency.code', '') && !!_.get(eventsData, 'fairMarketValuePrice', '')),

      insuranceCode: _.get(eventsData, 'insuranceCurrency.code', ''),
      insuranceValue: _.get(eventsData, 'insurancePrice', ''),
      insuranceI: `${_.get(eventsData, 'insuranceCurrency.code')} ${_.get(eventsData, 'insurancePrice')}
       ${this.dictionaryCard.EVENT_PRICING.I}`,
      insuranceShow: !!(_.get(eventsData, 'insuranceCurrency.code', '') && !!_.get(eventsData, 'insurancePrice', '')),

      // Source
      source: _.get(eventsData, 'event.mainClient.clientSource.name', ''),
      // Client
      client: this.getClientName(eventsData, 'event.mainClient.clientId'),
      mainClient: this.getMainClientName(eventsData, 'event.mainClient.clientId'),
      affiliationText: this.getAffiliatedText(eventsData, 'event.affiliatedClient.affiliatedType.name'),
      affiliatedClient: this.getClientName(eventsData, 'event.affiliatedClient.clientId'),
      // Notes
      notes: _.get(eventsData, 'comments', ''),
      url: _.get(eventsData, 'event._links.self.href', ''),
      id: `/pricing/${_.get(eventsData, 'event.id', '')}`,
      type: _.get(eventsData, 'type', ''),
    };
  }

  setJsonPublication(eventsData): any {
    return {
      // Event
      typeEvent: `${_.capitalize(_.get(eventsData, 'type', ''))} (${_.get(eventsData, 'event.publicationType.value', '')})`,
      date: _.get(eventsData, 'event.date', ''),
      author: _.get(eventsData, 'event.author', '') ?
        `${_.get(eventsData, 'event.author')}, ` : '',
      publisher: _.get(eventsData, 'event.establishment.name') ?
        `${_.get(eventsData, 'event.establishment.name')}, ` : '',
      bookTitle: _.get(eventsData, 'event.title') ?
        `${_.get(eventsData, 'event.title')}` : '',
      // Source
      source: _.get(eventsData, 'consignmentEvent.mainClient.clientSource.name', ''),
      // Client
      client: this.getClientName(eventsData, 'consignmentEvent.mainClient.clientId'),
      mainClient: this.getMainClientName(eventsData, 'consignmentEvent.mainClient.clientId'),
      // Notes
      notes: _.get(eventsData, 'comments', ''),
      url: _.get(eventsData, 'event._links.self.href', ''),
      id: `/publication/${_.get(eventsData, 'event.id', '')}`,
      type: _.get(eventsData, 'type', ''),
    };
  }

  eventScrolled() {
    const totalElements = this.eventRService.page.value.totalElements;
    if (!this.data && totalElements > this.eventItems.length) {
      this.eventRService.getNextElements()
    }
  }

  addToEvent(event) {
    switch (event) {
      case this.dictionary.SECTIONS.PUBLICATION:
        this.router.navigate(['publication-add-object']);
        break;
      case this.dictionary.SECTIONS.TRANSACTION:
        this.router.navigate(['transaction-add-object']);
        break;
      case this.dictionary.SECTIONS.PRICING:
        this.router.navigate(['pricing-add-object']);
        break;
      case this.dictionary.SECTIONS.PURCHASE:
        break;
      default:
    }
  }
}
