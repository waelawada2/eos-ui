import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventCardPricingComponent } from './event-card-pricing.component';

describe('EventCardPricingComponent', () => {
  let component: EventCardPricingComponent;
  let fixture: ComponentFixture<EventCardPricingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventCardPricingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCardPricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
