import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, Output, ViewChild } from '@angular/core'
import { ExternalClientService } from '../../event/shared/services/client/external-client.service'
import { ExternalClientFormComponent } from '../../event/shared/components'
import { ExternalClient } from '../../event/shared/models'
import { getErrorStringFromServer } from 'app/event/shared/utils'
import { GeneralDictionary } from '../../shared/constants'
import * as _ from 'lodash';
import { Subscription } from 'rxjs/Subscription'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'

@Component({
  providers: [ExternalClientService],
  selector: 'app-object-current-client',
  templateUrl: './current.owner.component.html',
  styleUrls: [
    '../../event/shared/styles/panels.scss',
  ]
})
export class CurrentOwnerComponent implements AfterViewInit, OnDestroy {

  private currentObject;
  private client: ExternalClient;
  private clientSubscription: Subscription;
  private errorSubscription: Subscription;
  private dictionary = GeneralDictionary;

  @ViewChild(ExternalClientFormComponent) externalClientForm: ExternalClientFormComponent;

  @Input() set object(object) {
    this.currentObject = object;
    this.updateOwner();
  }
  @Output()
  private errorEvent: EventEmitter<string> = new EventEmitter();

  constructor(private clientService: ExternalClientService) {

  }

  ngAfterViewInit() {
    this.clientSubscription = this.clientService.element
      .distinctUntilChanged()
      .skip(1)
      .do((client) => {
        this.externalClientForm.reset()
      })
      .filter(client => !!client)
      .subscribe((client) => {
        this.externalClientForm.updateDataForm(client)
      });

    this.errorSubscription = this.clientService.error
      .distinctUntilChanged()
      .skip(1)
      .do(() => this.externalClientForm.updateDataForm({ id: _.get(this.currentObject, 'currentOwnerId') }))
      .subscribe(this.handleClientError.bind(this))
  }

  ngOnDestroy() {
    this.clientSubscription.unsubscribe();
    this.errorSubscription.unsubscribe();
  }

  private handleClientError(error) {
    this.errorEvent.emit(
      getErrorStringFromServer(error,
        GeneralDictionary.NOTIFICATIONS.ERR_NOT_FOUND_CLIENT(_.get(this.currentObject, 'currentOwnerId')))
    )
  }

  private updateOwner() {
    const clientId = _.get(this.currentObject, 'currentOwnerId')
    if (clientId) {
      this.clientService.getElement(clientId)
    } else {
      this.client = null;
      if (this.externalClientForm.form) {
        this.externalClientForm.reset();
      }
    }
  }

}
