﻿import { Component, Input, OnInit, ViewChild, ViewContainerRef, ChangeDetectorRef, ComponentFactoryResolver, ElementRef, HostListener, Attribute, Inject, ChangeDetectionStrategy, NgZone } from '@angular/core'
import { ArtistService } from '../artist/artist.service';
import { ObjectService } from '../object/object.service';
import { EventReportService } from '../shared/services';
import { ObjectComponent } from '../object/object.component';
import { HeaderService } from '../../app/header/header.service';
import { KeyboardkeysService } from '../../app/keyboardkeys/keyboardkeys.service';
import { DialogService } from 'ng2-bootstrap-modal';
import { MergeTwoComponent } from './mergetwo-component';
import { ActivatedRoute, Router, Params, NavigationEnd } from '@angular/router';
import { CustomSortComponent } from '../../app/Merge/customesort.component';
import { SavedListService } from '../saved-list/saved-list.service';
import { BrowserModule } from '@angular/platform-browser';
import { DOCUMENT } from '@angular/platform-browser';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { GeneralDictionary } from '../shared/constants';
import { ConfirmComponent } from '../common/confirm-component';
import { AlertComponent } from '../common/alert-component';
import { DeleteObjectStatusComponent } from '../modal/deleteObjectStatusModal.component';
import { TosatService } from '../common/toasty-service';
import { ConfigurationLoaderService } from 'app/configuration.service'
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Component({
  selector: 'simple-dnd',
  templateUrl: './Merge.component.html',
  styleUrls: ['./Merge.component.css'],
  providers: [ArtistService, ObjectService, KeyboardkeysService, HeaderService, SavedListService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MergeComponent implements OnInit {
  @ViewChild('selectView') selectView: ElementRef;
  URL: string;
  confirmResult: boolean = null;
  promptMessage = '';
  Dragged: any;
  public errorMsg;
  isChecked = false;
  public selectedPropertyIds = [];
  @ViewChild('target', { read: ViewContainerRef }) target: ViewContainerRef;
  public allDetails: any;
  public selectAllFlag = false;
  public objectsUnchecked = [];
  public saveListSortParam;
  // The nth 'property' that was last fetched
  public savedListLastReturnedItem = 0;
  public totalElements = 0;
  public arrayList = [];
  public timeStamp;
  public loading = false;
  private dictionary = GeneralDictionary;

  public allRecord;
  public queryParam = '&size=16&lod=MAX&page=';
  public queryResults = '&';
  public totalPageResult;
  public flag;
  public keycloak = window['keycloak'];
  // The nth 'properties' page that was last fetched
  public savedListResultsPage = 0;
  public pageSize = 16;
  public userName;
  public savedListId;
  public savedListProperties;
  //empty no records found
  public emptyPagination = '';
  public widthStyle: number = 85;
  public showDeleteProgressbar: boolean = false;
  public totalobjectstobedeleted: number = 0;
  public deletedObjectsCount: number = 0;
  public updateTotalObjectsToBeDeleted: boolean = true;
  // EOS-1173 Sort Order
  public sortOrder = 'asc';
  public sorted = false;
  public objectsToBeRemoved: Array<any> = [];
  public objectsRemovedCount: number = 0;
  public savedListName: string = ' ';
  constructor(
    private _http: Http,
    private _artistService: ArtistService,
    private dialogService: DialogService,

    private router: Router,
    private objectservice: ObjectService,
    private eventRService: EventReportService,
    private _savedListService: SavedListService,
    private cfr: ComponentFactoryResolver,
    private _headerService: HeaderService,
    private _keyboardkeysService: KeyboardkeysService,
    private cd: ChangeDetectorRef,
    private _zone: NgZone,
    private tosatservice: TosatService,
    configuration: ConfigurationLoaderService,
    @Inject(DOCUMENT) private document: Document,
    @Attribute("checked") checked) {
    sessionStorage.removeItem('refreshPage');
    setInterval(() => {
      this.cd.markForCheck();
    }, 0);

    this.URL = configuration.getSettings().apiUrl;
  }

  getDetails(page) {
    this._zone.runOutsideAngular(() => {
      // Save List
      if (this.flag === 'savelist') {
        document.getElementById('removeSavedListButton').style.display = "block";
        this.userName = this.keycloak.tokenParsed ? this.keycloak.tokenParsed.preferred_username : null;
        // Session param 'navigatedtosavedlistid' keeps track of the saved list record that was clicked on the homepage.
        this.savedListId = sessionStorage.getItem('navigatedtosavedlistid') ? sessionStorage.getItem('navigatedtosavedlistid') : null;
        if (page === 0 && this.savedListId) {
          sessionStorage.removeItem('savedlistids');
          // Initialization
          this.allDetails = {};
          this.allDetails.content = [];
          this.total_pages = 0;
          this.curPageValue = 1;
          this.page = 0;
          this.savedListProperties = [];
          // Get 'Property IDs' from saved list using 'Saved List ID'.
          this.getSavedListProperties(this.userName, this.savedListId, 32);
          this.page = 1;
          setTimeout(() => {
            sessionStorage.setItem('savelistview', "true");
          }, 500);
        }
      }
      else {
        document.getElementById('removeSavedListButton').style.display = "none";
        sessionStorage.setItem('savelistview', "false");
        sessionStorage.removeItem('navigatedtosavedlistid');
        if (page === 0) {
          var startQuery = this.queryParam.replace("size=16", "size=32");
          this.total_pages = 0;
          this.curPageValue = 1;
          this.page = 1;
          this.objectservice
            .getProperties(startQuery + page)
            .subscribe(Response => {
              document.getElementsByTagName("html")[0].classList.remove('loading');
              this.loading = false;
              this.widthStyle = 85;
              if (Response.content.length > 0) {
                sessionStorage.setItem('totalelements', Response.totalElements);
                // Remove duplicate objects from 'content' array
                Response.content = this.removeDuplicates(Response.content);
                this.allDetails = Response;
                this.total_pages = ((Response.totalElements % 16) === 0) ? Response.totalElements / 16 : Math.ceil(Response.totalElements / 16);
                this.totalPageResult = ((Response.totalElements % 16) === 0) ? Response.totalElements / 16 : Math.ceil(Response.totalElements / 16);
                // EOS-1173 Ascending/Descending Sort Order
                if (this.queryParam && this.queryParam.indexOf("&sort=") > -1) {
                  this.sorted = true;
                  if (this.queryParam.indexOf(",asc") > -1) {
                    this.sortOrder = 'asc';
                    this.highlightSortIcon('asc');
                  }
                  else if (this.queryParam.indexOf(",desc") > -1) {
                    this.sortOrder = 'desc';
                    this.highlightSortIcon('desc');
                  }
                }
                else {
                  this.sorted = false;
                  this.highlightSortIcon(null);
                }
              } else {
                this.emptyPagination = "No records found.";
                sessionStorage.setItem('thumbnailnorecords', 'true');
                document.getElementById('back-button').click();
                this.widthStyle = 115;
                // EOS - 2616 fix
                this.allDetails = [];
                this.sorted = false;
                this.highlightSortIcon(null);
              }
            }, resFileError => {
              this.loading = false;
              this.sorted = false;
              this.highlightSortIcon(null);
              document.getElementsByTagName('html')[0].classList.remove('loading');

            });
        } else {
          this.loading = true;
          document.getElementsByTagName("html")[0].classList.add('loading');
          this.objectservice
            .getProperties(this.queryParam + page)
            .subscribe(Response => {
              this.loading = false;
              document.getElementsByTagName('html')[0].classList.remove('loading');
              // Remove duplicate objects from 'content' array
              Response.content = this.removeDuplicates(Response.content);
              this.allDetails = Response;
              this.total_pages = Response.totalPages;
              this.totalPageResult = Response.totalPages;
              // EOS-1173 Ascending/Descending Sort Order
              if (this.queryParam && this.queryParam.indexOf("&sort=") > -1) {
                this.sorted = true;
                if (this.queryParam.indexOf(",asc") > -1) {
                  this.sortOrder = 'asc';
                  this.highlightSortIcon('asc');
                }
                else if (this.queryParam.indexOf(",desc") > -1) {
                  this.sortOrder = 'desc';
                  this.highlightSortIcon('desc');
                }
              }
              else {
                this.sorted = false;
                this.highlightSortIcon(null);
              }
            }, resFileError => {
              this.loading = false;
              this.sorted = false;
              this.highlightSortIcon(null);
              document.getElementsByTagName('html')[0].classList.remove('loading');
            });
        }
      }
    });
  }

  getDetailsData(page) {
    this._zone.runOutsideAngular(() => {
      this.loading = true;
      this.objectservice
        .getProperties(this.queryParam + page)
        .subscribe(Response => {
          this.loading = false;
          var allObjects = [], duplicates = [], responseObjects = [], response_duplicates = []; // EOS-2791
          var flag = this.allDetails.content;
          this.total_pages = ((Response.totalElements % 16) === 0) ? Response.totalElements / 16 : Math.ceil(Response.totalElements / 16);
          this.totalPageResult = ((Response.totalElements % 16) === 0) ? Response.totalElements / 16 : Math.ceil(Response.totalElements / 16);
          // Remove duplicate objects from 'content' array
          var filterData = this.allDetails.content.concat(Response.content);
          allObjects = filterData.map(a => a.id).sort(); // EOS-2791
          responseObjects = Response.content.map(a => a.id).sort(); // EOS-2791

          // EOS-2791 Find duplicates from ALL objects on thumbnail view.
          for (var i = 0; i < allObjects.length; i++) {
            if (allObjects[i + 1] == allObjects[i]) {
              duplicates.push(allObjects[i]);
            }
            if (i === allObjects.length - 1) {
              if (duplicates.length === 0) {
                console.log("PAGE " + page + " - No duplicates on thumbnail view.");
              }
              else {
                console.log("Total duplicates found until PAGE " + page + ": " + duplicates.length);
                console.log(duplicates);
              }
            }
          }

          // EOS-2791 Find duplicates from objects fetched in the page Response.content[].
          for (var i = 0; i < responseObjects.length; i++) {
            if (responseObjects[i + 1] == responseObjects[i]) {
              response_duplicates.push(responseObjects[i]);
            }
            if (i === responseObjects.length - 1) {
              if (response_duplicates.length === 0) {
                console.log("No duplicates on PAGE " + page + ".");
              }
              else {
                console.log("Total duplicates found until PAGE " + page + ": " + response_duplicates.length);
                console.log(response_duplicates);
              }
            }
          }
          console.log('---------------------------------------------------------------------------------');
          // EOS-2791

          var filterSplit = filterData.splice(-64, filterData.length);
          var finalResult = this.removeDuplicatesData(filterSplit, 'id');
          this.allDetails.content = filterData.concat(finalResult);
          if (flag.length <= 32) {
            var elmnt = document.getElementById("jumpSPage");
            if (elmnt && !this.flagenter)
              elmnt.scrollIntoView();
          }
        }, resFileError => {
          this.loading = false;
        });
    });
  }

  removeDuplicatesData(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }

  ngOnInit() {
    document.getElementsByTagName("html")[0].classList.add('loading');
    this._zone.runOutsideAngular(() => {
      // Event listener to enable dragover of thumbnail across pages.
      document.addEventListener("dragover", (ev: DragEvent) => {
        let windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
        // Scroll up
        if (ev.y < 50 && window.pageYOffset != 0) {
          window.scrollTo(0, window.pageYOffset - 20);
        }
        // Scroll down
        else if (ev.y > (windowHeight - 50)) {
          let body = document.body, html = document.documentElement;
          let docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
          let windowBottom = windowHeight + window.pageYOffset;
          if (windowBottom < docHeight) {
            window.scrollTo(0, window.pageYOffset + 20);
          }
        }
      });
    });

    sessionStorage.removeItem('thumbnailselectall');
    sessionStorage.removeItem('addtosavelistobjects');
    sessionStorage.removeItem("objectsUnchecked");
    this.target.clear();
    let compFactory = this.cfr.resolveComponentFactory(ObjectComponent);
    this.target.createComponent(compFactory);
    document.getElementById('thumbnailheader').style.display = "none";
    document.getElementById('navHeaderValue').style.display = "none";
    document.getElementById('navRightDropdown').style.display = "block";

    if (window.location.href.indexOf('savelist') > -1) {
      this.flag = 'savelist';
      history.pushState('', '', "thumbnail");
      sessionStorage.setItem('savelistview', "true");
      if (sessionStorage.getItem("Savedlistsortingquery") != "null")
        this.saveListSortParam = sessionStorage.getItem("Savedlistsortingquery").trim();
      else
        this.saveListSortParam = null;
      this.getDetails(0);
    }
    else if (window.location.href.indexOf('resultQuery') > -1) {
      var queryId = window.location.search.split('resultQuery=');
      queryId = queryId[1].split('&q=');
      this.queryResults = queryId[0].replace(/%26/g, "&").replace(/%3D/g, "=").replace(/%20/g, " ").replace(/%2C/g, ",").replace(/%252C/g, ",").replace(/%2520/g, " ");
      this.queryResults = this.queryResults.replace(/artists/g, "tempurlquery");
      this.queryResults = this.queryResults.replace(/artistCatalogueRaisonee/g, "aCatalogueRaisonee");
      this.queryResults = this.queryResults.replace(/artist/g, "artistIds");
      this.queryResults = this.queryResults.replace(/tempurlquery/g, "artists");
      this.queryResults = this.queryResults.replace(/aCatalogueRaisonee/g, "artistCatalogueRaisonee");
      this.queryResults = this.queryResults.replace(/%26/g, "&").replace(/%3D/g, "=").replace(/%20/g, " ").replace(/%2C/g, ",");
      if (sessionStorage.getItem('MainViewQuery') != "undefined" && sessionStorage.getItem('MainViewQuery') != "null") {
        this.queryParam = sessionStorage.getItem('MainViewQuery').replace('&lod=MIN', '&') + this.queryParam;
      } else {
        this.queryParam = this.queryParam;
      }
      this.getDetails(0);
    } else if (window.location.href.indexOf('clientId=') > -1) {
      const clientId = /clientId\=(EOS-[0-9A-Z]+|[0-9]+).*/.exec(window.location.href)[1];
      this.queryParam = `&currentOwnerIds=${clientId}&sort=artists.lastName,artists.firstName,dateDetails.yearStart,category${this.queryParam}`;
      this.getDetails(0);
    } else {
      if (sessionStorage.getItem('MainViewQuery') != "undefined" && sessionStorage.getItem('MainViewQuery') != "null" && sessionStorage.getItem('MainViewQuery') != null) {
        this.queryParam = sessionStorage.getItem('MainViewQuery').replace('&lod=MIN', '&') + this.queryParam;
      } else {
        this.queryParam = this.queryParam;
      }
      this.getDetails(0);
    }
  }

  //Select Sort
  addToBasket($event: any, event: any) {
    this._zone.runOutsideAngular(() => {
      this.Dragged = $event.dragData;
      this.MergePopup(this.Dragged, event);
    });
  }

  MergePopup(Dragged: any, Drop: any) {
    if (Drop.id != Dragged.id) {
      this.objectservice
        .getProperty(Drop.id)
        .subscribe(DropResponse => {
          this.objectservice
            .getProperty(Dragged.id)
            .subscribe(DraggedResponse => {
              let body = document.getElementsByTagName("body")[0];
              let preWidth = body.offsetWidth;
              body.style.overflow = "hidden";
              body.style.marginRight = body.offsetWidth - preWidth + "px";
              this.dialogService.addDialog(MergeTwoComponent, {
                Dragged: DraggedResponse,
                Drop: DropResponse
              }, { backdropColor: 'rgba(220,220,220,0.5)' }).subscribe((isConfirmed) => {
                this.confirmResult = isConfirmed;
                var d = new Date();
                var n = d.getTime();
                this.timeStamp = n;
                body.style.overflowY = "auto";
                body.style.marginRight = "";
                var index = this.allDetails.content.findIndex(i => i.id === isConfirmed);
                if (index > -1) {
                  this.allDetails.content.splice(index, 1);
                }
                this.objectservice
                  .getProperty(Drop.id)
                  .subscribe(DropResponse2 => {
                    var iDrop = this.allDetails.content.findIndex(x => x.id === Drop.id);
                    if (iDrop > -1) {
                      this.allDetails.content[iDrop] = DropResponse2;
                    }
                  }, resFileError => resFileError);
                this.objectservice
                  .getProperty(Dragged.id)
                  .subscribe(DraggedResponse2 => {
                    var iDrag = this.allDetails.content.findIndex(x => x.id === Dragged.id);
                    if (iDrag > -1) {
                      this.allDetails.content[iDrag] = DraggedResponse2;
                    }
                  }, resFileError => resFileError);
              })
            }, resFileError => resFileError);
        }, resFileError => resFileError);
    }
  }

  orderedProduct($event: any) {
  }

  viewObject(event) {
    // Set 'viewobjectid' when a thumbnail is clicked on 'thumbnail view'.
    sessionStorage.setItem('viewobjectid', event.id);
  }

  public page = 0;
  onScrollDown(event) {
    this._zone.runOutsideAngular(() => {
      this.page = (this.page == 0) ? 1 : this.page + 1;
      if (this.flag === 'savelist' && (this.page >= 1) && (this.page <= this.total_pages)) {
        this.getSavedListProperties(this.userName, this.savedListId, 16);
      }
      else if (this.flag != 'savelist') {
        if (this.totalPageResult >= this.page) {
          this.getDetailsData(this.page);
        }
      }
    });
  }

  getDetailsDataUp(page) {
    this._zone.runOutsideAngular(() => {
      this.loading = true;
      this.objectservice
        .getProperties(this.queryParam + page)
        .subscribe(Response => {
          this.loading = false;
          // Remove duplicate objects from 'content' array
          var filterData = Response.content.concat(this.allDetails.content);
          var filterSplit = filterData.splice(0, 64);
          var finalResult = this.removeDuplicatesData(filterSplit, 'id');
          this.allDetails.content = finalResult.concat(filterData);
          setTimeout(() => {
            window.scrollTo(0, 10);
            document.getElementsByTagName("html")[0].classList.remove("loading");
          }, 1500);
          this.total_pages = ((Response.totalElements % 16) === 0) ? Response.totalElements / 16 : Math.ceil(Response.totalElements / 16);
          this.totalPageResult = ((Response.totalElements % 16) === 0) ? Response.totalElements / 16 : Math.ceil(Response.totalElements / 16);
        }, resFileError => {
          this.loading = false;
          document.getElementsByTagName("html")[0].classList.remove("loading");
        });
    });
  }

  //Scroll Up value
  getSavedListPropertiesUp(userName, savedListId, size, page) {
    this.loading = true;
    this._savedListService.getPropertiesBySavedListId(userName, this.savedListId, page, size, 'max', this.saveListSortParam)
      .subscribe(savedList => {
        if (savedList.content.length > 0) {
          this.loading = false;
          this.allDetails.content = savedList.content.concat(this.allDetails.content);
          // Remove duplicate objects from 'content' array
          var filterData = savedList.content.concat(this.allDetails.content);
          var filterSplit = filterData.splice(0, 64);
          var finalResult = this.removeDuplicatesData(filterSplit, 'id');
          this.allDetails.content = finalResult.concat(filterData);
          setTimeout(() => {
            window.scrollTo(0, 10);
            document.getElementsByTagName("html")[0].classList.remove("loading");
          }, 1500);
          for (let property in savedList.content) {
            this.savedListProperties.concat(savedList.content[property].id);
          }
          this.total_pages = ((savedList.totalElements % 16) === 0) ? savedList.totalElements / 16 : Math.ceil(savedList.totalElements / 16);
          this.totalPageResult = ((savedList.totalElements % 16) === 0) ? savedList.totalElements / 16 : Math.ceil(savedList.totalElements / 16);
        }
        else {
          document.getElementsByTagName("html")[0].classList.remove("loading");
          this.loading = false;
        }
      }, resFileError => {
        document.getElementsByTagName("html")[0].classList.remove("loading");
        this.loading = false;
      });
  }

  // scroll Up
  onScrollUp(event) {
    this._zone.runOutsideAngular(() => {
      if (this.enterP > 0) {
        document.getElementsByTagName("html")[0].classList.add("loading");
        this.enterP = (this.enterPageNo === this.enterP) ? this.enterP : this.enterP - 1;
        if (this.flag == 'savelist') {
          this.getSavedListPropertiesUp(this.userName, this.savedListId, 16, this.enterP);
        } else {
          this.getDetailsDataUp(this.enterP);
        }
      }
    });
  }

  // Header Drop-down toggle mode
  toggleFlag: number;
  toggleFunction(flag: number) {
    this.toggleFlag = this._headerService.headerToggle((flag === this.toggleFlag) ? 0 : flag);
  }

  // Keyboard shortcuts
  keyboardShortCutFunction(e) {
    this.toggleFlag = 0;
    switch (this._keyboardkeysService.getKeyboardResponse(e)) {
      case 'next':
        if ((this.total_pages > 0) && (this.total_pages > this.curPageValue)) {
          this.pageFunction('nextPage');
        }
        break;
      case 'prev':
        if ((this.total_pages > 0) && (this.curPageValue > 1)) {
          this.pageFunction('prevPage');
        }
        break;
      case 'objectView':
        if (window.location.pathname.indexOf('thumbnail') > -1) {
          this.onNavigate('mainView')
        }
        break;
      // case 'find':
      // 	if(this.headerFlag==='search'){
      // 		this.onSubmit('editMode',this.objectDetailsForm.value);
      // 		this.reset=false;
      // 	}
      // break;
      // case 'amendFind':
      // 	if(this.headerFlag==='result'||this.headerFlag==='edit'){
      // 		this.backMode();
      // 	}
      // break;
      // case 'add':
      // 	if(this.headerFlag==='search'||this.headerFlag==='result'||this.headerFlag==='edit'){
      // 		this.NewObjectModalPrompt();
      // 		this.reset=false;
      // 	}
      // break;
      // case 'last':
      // 	if(this.total_pages>0){
      // 	  this.curPageValue = this.total_pages;
      // 	  this.getCurrValueThumnails(this.total_pages,'enter');
      // 	}
      // break;
      // case 'first':
      // 	if(this.total_pages>0){
      // 	  this.getCurrValueThumnails(1,'enter');
      // 	}
      // break;
      case 'enter':
        if (this.pageEnter && (this.curPageValue <= this.total_pages)) {
          if (this.curPageValue < 1) {
            this.getCurrValueThumnails(1, 'enter');
          } else {
            this.getCurrValueThumnails(this.curPageValue, 'enter');
          }
        } else if (this.pageEnter && (this.curPageValue > this.total_pages)) {
          this.getCurrValueThumnails(1, 'enter');
        }
        break;
    }
  }
  //pagination
  public pageEnter: boolean = false;
  public total_pages = 0;
  public curPageValue = 1;
  public previousheight = 0;
  @HostListener("window:scroll", [])

  pageFunction(flagNo: string) {
    if (flagNo === 'nextPage') {
      if (this.total_pages >= this.page && this.page < this.curPageValue) {
        this.page++;
        if (this.flag == 'savelist') {
          this.getSavedListProperties(this.userName, this.savedListId, 16);
        }
        else {
          this.getCurrValueThumnails(this.curPageValue + 1, "enter");
        }
      } else {
        this.getCurrValueThumnails(this.curPageValue + 1, "enter");
      }
    } else if (flagNo === 'prevPage' && this.curPageValue >= 0) {
      // this.curPageValue--;
      this.getCurrValueThumnails(this.curPageValue - 1, "enter");
    }
  }

  //jump user input page
  public flagenter: boolean = false;
  public enterPageNo: number = 1;
  public enterP: number = 0;
  getCurrValueThumnails(inputvalue, event) {
    this.pageEnter = true;
    inputvalue = inputvalue - 1;
    if (event === "enter" || event === "firstPage" || event === "lastPage") {
      document.getElementsByTagName("html")[0].classList.add("loading");
      this.allDetails = [];
      this.flagenter = true;
      this.enterPageNo = parseInt(inputvalue) + 1;
      this.enterP = parseInt(inputvalue);
      this.curPageValue = inputvalue + 1;
      this.page = parseInt(inputvalue);
      this.getDetailsFunction(parseInt(inputvalue));
    }
  }
  //Standard sort
  sortDetails(value: any) {
    // Get 'Sort' param for saved search.
    let sortParam = value;
    sessionStorage.setItem('sortparam', sortParam.substring(6, sortParam.length));
    if (this.flag == 'savelist') {
      this.saveListSortParam = value + ',' + this.sortOrder;
      sessionStorage.setItem("Savedlistsortingquery", this.saveListSortParam);
    }
    else {
      // EOS - 2616 Remove query param 'artistCatalogueRaisoneeIds' from queryResults
      if (this.queryResults.indexOf('&artistCatalogueRaisoneeIds=') != -1) {
        this.queryResults = this.queryResults.split('&artistCatalogueRaisoneeIds=')[0];
      }
      this.queryResults = this.queryResults.replace('&sort=createdDate,asc', '&');
      // END OF EOS - 2616 fix
      if (this.queryResults.indexOf("&sort=") > -1) {
        var tempQuery = this.queryResults;
        var tempQueryArray = tempQuery.split('&');
        tempQueryArray.forEach(function (data, i) {
          if (data.split('=')[0] === 'artistCatalogueRaisonee') {
            tempQueryArray.splice(i, 1);
            i--;
            return;
          }
          else if (data.indexOf("sort=") > -1) {
            tempQueryArray[i] = value.substr(1, value.length);
          }
        })
        this.queryParam = tempQueryArray.join('&');
        sessionStorage.setItem("MainViewQuery", this.queryParam);
        if (this.queryParam.indexOf('&artistCatalogueRaisoneeIds=') > -1) {
          let tempQuery = this.queryParam.split('&artistCatalogueRaisoneeIds=');
          tempQuery[0] += ',' + this.sortOrder;
          this.queryParam = '';
          this.queryParam = tempQuery.join('&artistCatalogueRaisoneeIds=');
        }
        else {
          this.queryParam = this.queryParam + ',' + this.sortOrder;
        }
        this.queryParam = this.queryParam + '&size=16&lod=MAX&page=';
      } else {
        if (value.indexOf('&artistCatalogueRaisoneeIds=') > -1) {
          let tempQuery = value.split('&artistCatalogueRaisoneeIds=');
          tempQuery[0] += ',' + this.sortOrder;
          value = '';
          value = tempQuery.join('&artistCatalogueRaisoneeIds=');
          this.queryParam = this.queryResults + value + '&size=16&lod=MAX&page=';
        }
        else {
          this.queryParam = this.queryResults + value + ',' + this.sortOrder + '&size=16&lod=MAX&page=';
        }
        sessionStorage.setItem("MainViewQuery", this.queryResults + value);
      }
    }
    document.getElementsByTagName("html")[0].classList.add("loading");
    this.allDetails = [];
    this.getDetails(0);
  }

  public selectedValue = "selected";
  // Get property ID of selected objects in thumbnail view.
  getSelectedObjects($event, property) {
    this.selectView.nativeElement.value = 'selected';
    let url = window.location.href;
    let selectedPropertyIdParam = '';
    if (!sessionStorage.getItem('addtosavelistobjects')) {
      this.selectedPropertyIds = [];
    }
    // If --Select All- option has been selected
    if (this.selectAllFlag && $event.target.checked == true) {
      if (this.objectsUnchecked.indexOf(property.id) > -1) {
        this.objectsUnchecked.splice(this.objectsUnchecked.indexOf(property.id), 1);
      }
    }
    else if (this.selectAllFlag && $event.target.checked == false) {
      if (!(this.objectsUnchecked.indexOf(property.id) > -1)) {
        this.objectsUnchecked.push(property.id);
      }
      sessionStorage.removeItem("objectsUnchecked");
    }
    if (this.selectAllFlag) {
      let objects = '';
      let selectedPropertyIdParam = '';
      sessionStorage.removeItem('objectsUnchecked');
      for (let id in this.objectsUnchecked) {
        if (this.objectsUnchecked[id]) {
          objects += this.objectsUnchecked[id] + ',';
          sessionStorage.setItem("objectsUnchecked", objects);
        }
      }
    }
    // If checkbox checked
    if ($event.target.checked == true) {
      if (!(this.selectedPropertyIds.indexOf(property.id) > -1)) {
        this.selectedPropertyIds.push(property.id);
      }
    }
    // If checkbox unchecked
    else {
      if (this.selectedPropertyIds.indexOf(property.id) > -1) {
        this.selectedPropertyIds.splice(this.selectedPropertyIds.indexOf(property.id), 1);
      }
    }
    // Track list of selected objects
    for (let id in this.selectedPropertyIds) {
      if (this.selectedPropertyIds[id] != '') {
        selectedPropertyIdParam += this.selectedPropertyIds[id] + ',';
      }
    }
    sessionStorage.setItem('addtosavelistobjects', selectedPropertyIdParam);
  }

  onSelect(event: string) {
    if (event) {
      if (event === 'selectAll') {
        this.selectAllFlag = true;
        sessionStorage.setItem('thumbnailselectall', 'true');
        sessionStorage.removeItem('addtosavelistobjects');
        sessionStorage.removeItem('objectsUnchecked');
        this.isChecked = !(this.isChecked);
      }
      else if (event === 'deselectAll') {
        this.selectAllFlag = false;
        sessionStorage.setItem('thumbnailselectall', 'false');
        sessionStorage.removeItem('addtosavelistobjects');
        sessionStorage.removeItem('objectsUnchecked');
        this.selectedPropertyIds = [];
        this.objectsUnchecked = [];
        this.isChecked = !(this.isChecked);
      }
      else {
        this.selectAllFlag = false;
        sessionStorage.setItem('thumbnailselectall', 'false');
      }
      setTimeout(() => {
        if ((event === 'selectAll') && this.isChecked === true) {
          this.isChecked = !(!(this.isChecked));
        }
        else if ((event === 'selectAll') && this.isChecked === false) {
          this.isChecked = !(this.isChecked);
        }
        else if ((event === 'deselectAll') && this.isChecked === true) {
          this.isChecked = !(this.isChecked);
        }
        else if ((event === 'deselectAll') && this.isChecked === false) {
          this.isChecked = !(!(this.isChecked));
        }
        else {
          this.isChecked = false;
        }
      }, 1);
    }
    else {
      this.router.navigate(['/thumbnail']);
    }
  }

  redirectPage() {
    this.router.navigateByUrl('/');
  }


  // GET properties linked to a 'Saved List'.
  getSavedListProperties(userName, savedListId, size) {
    if (this.page != 0) {
      this.loading = true;
    }
    this._savedListService.getPropertiesBySavedListId(userName, this.savedListId, this.page, size, 'max', this.saveListSortParam)
      .subscribe(savedList => {
        if (savedList.content.length > 0) {
          if (this.page == 0) {
            this.widthStyle = 85;
            if (savedList.content.length > 0) {
              // Remove duplicate objects from 'content' array
              savedList.content = this.removeDuplicates(savedList.content);
              this.allDetails.content = savedList.content;
            } else {
              this.emptyPagination = "No records found";
              this.widthStyle = 115;
              this.sorted = false;
              this.highlightSortIcon(null);
            }
          } else {
            var flag = this.allDetails.content;
            // Remove duplicate objects from 'content' array
            var filterData = this.allDetails.content.concat(savedList.content);
            var filterSplit = filterData.splice(-64, filterData.length);
            var finalResult = this.removeDuplicatesData(filterSplit, 'id');
            this.allDetails.content = filterData.concat(finalResult);
            if (flag.length <= 32) {
              var elmnt = document.getElementById("jumpSPage");
              if (elmnt && !this.flagenter)
                elmnt.scrollIntoView();
            }
          }
          this.total_pages = ((savedList.totalElements % 16) === 0) ? savedList.totalElements / 16 : Math.ceil(savedList.totalElements / 16);
          this.totalPageResult = ((savedList.totalElements % 16) === 0) ? savedList.totalElements / 16 : Math.ceil(savedList.totalElements / 16);
          this.totalElements = savedList.totalElements;
          for (let property in savedList.content) {
            this.savedListProperties.push(savedList.content[property].id);
          }
          document.getElementsByTagName("html")[0].classList.remove("loading");
          this.loading = false;
          // EOS-1173 Ascending/Descending Sort Order
          if (this.saveListSortParam && this.saveListSortParam.indexOf("&sort=") > -1) {
            this.sorted = true;
            if (this.saveListSortParam.indexOf(",asc") > -1) {
              this.sortOrder = 'asc';
              this.highlightSortIcon('asc');
            }
            else if (this.saveListSortParam.indexOf(",desc") > -1) {
              this.sortOrder = 'desc';
              this.highlightSortIcon('desc');
            }
          }
          else {
            this.sorted = false;
            this.highlightSortIcon(null);
          }
        }
        else {
          this.sorted = false;
          this.highlightSortIcon(null);
          document.getElementsByTagName('html')[0].classList.remove('loading');
          this.loading = false;
        }
      }, resFileError => {
        this.page = this.page - 1;
        document.getElementsByTagName('html')[0].classList.remove('loading');
        this.loading = false;
        this.sorted = false;
        this.highlightSortIcon(null);
      });
  }

  onSort(value) {
    this.sortOrder = 'asc';
    if (value == "more") {
      document.getElementsByTagName("body")[0].classList.add("positionFixed");
      this.dialogService.addDialog(CustomSortComponent, { customSort: "customSort" }, { backdropColor: 'rgba(220,220,220,0.5)' })
        .subscribe((response) => {
          document.getElementsByTagName("body")[0].classList.remove("positionFixed");
          if (response === "false") {
            this.selectedValue = "selected";
          } else if (this.flag === 'savelist') {
            this.enterP = 0;
            this.sortDetails(response);
          } else {
            this.queryResults = this.queryResults.indexOf('&artistCatalogueRaisoneeIds') > -1 ? this.queryResults.substring(0, this.queryResults.indexOf('&artistCatalogueRaisoneeIds')) : this.queryResults;
            this.queryResults = this.queryResults.indexOf('&lod=MIN') > -1 ? this.queryResults.substring(0, this.queryResults.indexOf('&lod=MIN')) : this.queryResults;
            // EOS-1173
            if (this.sortOrder === 'asc') {
              response = response + ',asc';
            }
            else if (this.sortOrder === 'desc') {
              response = response + ',desc';
            }
            if (this.queryResults.indexOf("&sort=") > -1) {
              var tempQuery = this.queryResults;
              var tempQueryArray = tempQuery.split('&');
              tempQueryArray.forEach(function (data, i) {
                if (data.indexOf("sort=") > -1) {
                  tempQueryArray[i] = response.substr(1, response.length);
                }
              })
              this.queryResults = tempQueryArray.join('&');
              this.queryParam = this.queryResults + '&size=16&lod=MAX&page=';
              sessionStorage.setItem("MainViewQuery", this.queryResults);
            } else {
              this.queryParam = this.queryResults + response + '&size=16&lod=MAX&page=';
              sessionStorage.setItem("MainViewQuery", this.queryResults + response);
            }
            this.allDetails = [];
            this.getDetails(0);
            this.page = 1;
            this.total_pages = 0;
            this.curPageValue = 1;
            this.enterP = 0;
          }
        });
    } else {
      this.enterP = 0;
      this.sortDetails(value);
    }
  }

  onNavigate(value) {
    sessionStorage.setItem('objectView', value)
    value = value === 'presentation' ? 'mainView' : value // presentation will follow the same flow than main

    if (value === 'mainView' && sessionStorage.getItem('savelistview') === 'true') {
      sessionStorage.setItem('pureObject', 'false');
      const savedListId = sessionStorage.getItem('navigatedtosavedlistid') ? sessionStorage.getItem('navigatedtosavedlistid') : null;
      if (savedListId) {
        sessionStorage.setItem('MainViewQuerySaveDlist', savedListId);
        if (this.saveListSortParam != null) {
          sessionStorage.setItem('Savedlistsortingquery', this.saveListSortParam + '');
        } else {
          sessionStorage.setItem('Savedlistsortingquery', 'null');
        }
      }
      this.router.navigate(['/object'], { queryParams: { resultQuery: `savelist` } });
    } else if (value === 'mainView') {
      // EOS-1173
      if (this.sortOrder === 'asc' && sessionStorage.getItem('MainViewQuery') && sessionStorage.getItem('MainViewQuery').indexOf('desc') > -1) {
        sessionStorage.setItem('MainViewQuery', sessionStorage.getItem('MainViewQuery').replace(/,desc/i, ',asc'));
      } else if (this.sortOrder === 'desc' && sessionStorage.getItem('MainViewQuery') && sessionStorage.getItem('MainViewQuery').indexOf('asc') > -1) {
        sessionStorage.setItem('MainViewQuery', sessionStorage.getItem('MainViewQuery').replace(/,asc/i, ',desc'));
      } else {
        if (sessionStorage.getItem('MainViewQuery')) {
          if (!(sessionStorage.getItem('MainViewQuery').indexOf('asc') > -1) && !(sessionStorage.getItem('MainViewQuery').indexOf('desc') > -1)) {
            let tempQuery = sessionStorage.getItem('MainViewQuery');
            if (tempQuery.indexOf('&artistCatalogueRaisoneeIds=') > -1) {
              const query = tempQuery.split('&artistCatalogueRaisoneeIds=');
              query[0] += ',' + this.sortOrder;
              sessionStorage.setItem('MainViewQuery', query.join('&artistCatalogueRaisoneeIds='));
            } else {
              tempQuery = tempQuery + ',' + this.sortOrder;
              sessionStorage.setItem('MainViewQuery', tempQuery);
            }
          }
        }
      }
      this.router.navigate(['/object'], { queryParams: { resultQuery: sessionStorage.getItem('MainViewQuery') } });
    }
  }

  getEnterSavedListProperties(userName, savedListId, size) {
    document.getElementsByTagName("html")[0].classList.add("loading");
    this._savedListService.getPropertiesBySavedListId(userName, this.savedListId, this.page, size, 'max', this.saveListSortParam)
      .subscribe(savedList => {
        if (savedList.content.length > 0) {
          this.loading = false;
          // Remove duplicate objects from 'content' array
          savedList.content = this.removeDuplicates(savedList.content);
          this.allDetails.content = this.allDetails.content.concat(savedList.content);
          this.total_pages = savedList.totalPages;
          this.totalPageResult = savedList.totalPages;
          this.totalElements = savedList.totalElements;
          for (let property in savedList.content) {
            this.savedListProperties.push(savedList.content[property].id);
          }
          document.getElementsByTagName("html")[0].classList.remove("loading");
          if (this.allDetails.content.length <= 32) {
            setTimeout(() => {
              window.scrollTo(0, 10);
            }, 1500);
          }
          //this.onScrollDown("scrollDown");
        }
        else {
          document.getElementsByTagName("html")[0].classList.remove("loading");
          this.loading = false;
        }
      }, resFileError => {
        this.page = this.page - 1;
        document.getElementsByTagName("html")[0].classList.remove("loading");
        this.loading = false;
      });
  }

  // Jump/Goto the page
  getDetailsFunction(page) {
    if (this.flag === 'savelist' && (this.page >= 0) && (this.page <= this.total_pages)) {
      this._savedListService.getPropertiesBySavedListId(this.userName, this.savedListId, this.page, 16, 'max', this.saveListSortParam)
        .subscribe(savedList => {
          if (savedList.content.length > 0) {
            this.loading = false;
            // Remove duplicate objects from 'content' array
            savedList.content = this.removeDuplicates(savedList.content);
            this.allDetails.content = savedList.content;
            this.total_pages = savedList.totalPages;
            this.totalPageResult = savedList.totalPages;
            this.totalElements = savedList.totalElements;
            this.page++;
            for (let property in savedList.content) {
              this.savedListProperties.push(savedList.content[property].id);
            }
            this.getEnterSavedListProperties(this.userName, this.savedListId, 16);
          }
          else {
            this.loading = false;
          }
        }, resFileError => {
          this.page = this.page - 1;
          this.loading = false;
        });
    } else {
      var startQuery = this.queryParam;
      this.objectservice
        .getProperties(startQuery + page)
        .subscribe(Response => {
          this.allDetails = Response;
          page++;
          this.objectservice
            .getProperties(startQuery + page)
            .subscribe(Response => {
              sessionStorage.setItem('totalelements', Response.totalElements);
              // Remove duplicate objects from 'content' array
              Response.content = this.removeDuplicates(Response.content);
              this.allDetails.content = this.allDetails.content.concat(Response.content);
              this.total_pages = Response.totalPages;
              this.totalPageResult = Response.totalPages;
              this.page++;
              document.getElementsByTagName("html")[0].classList.remove("loading");
              //this.onScrollDown("enter");
              if (this.allDetails.content.length <= 32) {
                setTimeout(() => {
                  window.scrollTo(0, 10);
                }, 1500);
              }
            });
        });
    }
  }

  // Mouse Dom event function 
  mouseDomFunction(event) {
    this.curPageValue = Math.floor(event / 16) + 1 + this.enterP;
  }

  //Mouse Header function
  mouseHeaderFunction() {
    const offset = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    this.previousheight = offset;
    var page = offset / 630;
    page = offset / (630 - (Math.ceil(page) / 2));
    var setPage = 0;
    if (Math.floor(page) >= 60 && Math.floor(page) % 10 === 0) {
      setPage = (Math.floor(page) / 10) - 5;
    } else if (Math.floor(page) >= 60) {
      setPage = Math.floor(Math.floor(page) / 10) - 5;
    }
    if (this.previousheight > offset) {
      this.curPageValue = (Math.floor(page) <= 60) ? Math.floor(page) + 1 : Math.floor(page) - setPage;
    } else {
      this.curPageValue = this.enterP + ((Math.floor(page) <= 60) ? Math.floor(page) + 1 : Math.floor(page) - setPage);
    }
    let windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
    let body = document.body, html = document.documentElement;
    let docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
    let windowBottom = windowHeight + window.pageYOffset;
    if (windowBottom >= docHeight) {
      if (this.curPageValue >= this.total_pages - 1) {
        this.curPageValue = this.total_pages;
      }
    }
  }

  // Error Handler
  _errorHandler(_error: Response) {
    return Observable.throw(_error || "Server Error - 404.");
  }

  public deletedObjects: number = 0;
  public deleteFailedObjects = [];

  getSavedListPage(i) {
    this._savedListService.getPropertiesBySavedListId(this.userName, this.savedListId, i, 32, 'min', this.saveListSortParam)
      .subscribe(savedList => {
        if (savedList) {
          this.deleteSavedListPage(savedList, i);
        }
      })
  }

  deleteSavedListPage(savedList, i) {
    let page = i;
    let objectsInResponseCount = savedList.content.length;
    let totalPages = savedList.totalPages;
    let count = 0;
    let deleteRequestData = [];
    let objectsUnchecked = [];
    if (sessionStorage.getItem('objectsUnchecked')) {
      objectsUnchecked = sessionStorage.getItem('objectsUnchecked').split(',');
    }
    if (this.updateTotalObjectsToBeDeleted) {
      this.totalobjectstobedeleted = savedList.totalElements - objectsUnchecked.length;
      objectsUnchecked.length > 0 ? this.totalobjectstobedeleted++ : this.totalobjectstobedeleted;
      this.updateTotalObjectsToBeDeleted = false;
    }
    let index = -1;
    savedList.content.forEach(property => {
      if ((index = (objectsUnchecked.findIndex(item => item == property.id))) <= -1) {
        deleteRequestData.push(property.id);
      }
      index = -1;
    });
    var deleteRequestObject = {};
    deleteRequestObject['externalIds'] = deleteRequestData;
    this.objectservice.deleteProperties(deleteRequestObject)
      .subscribe(Response => {
        deleteRequestData.forEach(element => {
          count++;
          this.deletedObjects++;
          var index = this.allDetails.content.findIndex(i => i.id === element);
          if (index > -1) {
            this.allDetails.content.splice(index, 1);
          }
        })
        this.startDeleteProgressBar();
        this.deletedObjectsCount = this.deletedObjectsCount + deleteRequestData.length;
        if (totalPages > 1) {
          this.getSavedListPage(0);
        } else {
          this.endDeleteProgressBar();
          this.total_pages = 0;
          this.curPageValue = 1;
          this.page = 0;
          this.emptyPagination = "No records found";
          this.widthStyle = 115;
          sessionStorage.removeItem('addtosavelistobjects');
          sessionStorage.removeItem('thumbnailselectall');
          this.selectedPropertyIds = [];
          setTimeout(() => {
            this.selectView.nativeElement.value = 'selected';
            this.tosatservice.addToast('Object(s) deleted successfully.', 'top-right');
          }, 500);
        }
      }, resFileError => {
        this.errorMsg = resFileError;
        deleteRequestData.forEach(element => {
          count++;
          this.deleteFailedObjects.push(element);
          // Turn 'off' the checkbox for the object thumbnail for which 'delete' failed.												
          let index = "chk" + this.allDetails.content.findIndex(i => i.id === element);
          let chkbox = document.getElementById(index) as HTMLInputElement;
          chkbox.checked = false;
        });
        if ((count === (objectsInResponseCount - 1) && (page === (totalPages - 1)))) {
          sessionStorage.removeItem('addtosavelistobjects');
          sessionStorage.removeItem('thumbnailselectall');
          this.selectedPropertyIds = [];
          setTimeout(() => {
            this.selectView.nativeElement.value = 'selected';
            this.showDeleteStatus(this.deletedObjects, this.deleteFailedObjects);
          }, 500);
        }
        if (totalPages > 1) {
          this.getSavedListPage(0);
        }
      })
  }


  getSearchDetails(i) {
    var query = this.queryParam.replace("&lod=MAX", "&lod=MIN");
    query = query.replace("&size=16", "&size=32");
    this.objectservice
      .getProperties(query + i)
      .subscribe(Response => {
        this.deleteSearchCriteria(Response, i);
      })
  }

  deleteSearchCriteria(response, i) {
    let page = i;
    let objectsInResponseCount = response.content.length;
    let totalPages = response.totalPages;
    let count = 0;
    let deleteRequestData = [];
    let objectsUnchecked = [];
    if (sessionStorage.getItem('objectsUnchecked')) {
      objectsUnchecked = sessionStorage.getItem('objectsUnchecked').split(',');
    }
    if (this.updateTotalObjectsToBeDeleted) {
      this.totalobjectstobedeleted = response.totalElements - objectsUnchecked.length;
      objectsUnchecked.length > 0 ? this.totalobjectstobedeleted++ : this.totalobjectstobedeleted;
      this.updateTotalObjectsToBeDeleted = false;
    }
    let index = -1;
    response.content.forEach(property => {
      if ((index = (objectsUnchecked.findIndex(item => item == property.id))) <= -1) {
        deleteRequestData.push(property.id);
      }
      index = -1;
    })
    var deleteRequestObject = {};
    deleteRequestObject['externalIds'] = deleteRequestData;
    this.objectservice.deleteProperties(deleteRequestObject)
      .subscribe(Response => {
        deleteRequestData.forEach(element => {
          count++;
          this.deletedObjects++;
          var index = this.allDetails.content.findIndex(i => i.id === element);
          if (index > -1) {
            this.allDetails.content.splice(index, 1);
          }
        })
        this.startDeleteProgressBar();
        this.deletedObjectsCount = this.deletedObjectsCount + deleteRequestData.length;
        if (totalPages > 1) {
          this.getSearchDetails(0);
        } else {
          this.endDeleteProgressBar();
          this.total_pages = 0;
          this.curPageValue = 1;
          this.page = 0;
          this.emptyPagination = "No records found";
          this.widthStyle = 115;
          sessionStorage.removeItem('addtosavelistobjects');
          sessionStorage.removeItem('thumbnailselectall');
          this.selectedPropertyIds = [];
          setTimeout(() => {
            this.selectView.nativeElement.value = 'selected';
            this.tosatservice.addToast('Object(s) deleted successfully.', 'top-right');
          }, 500);
        }

      }, resFileError => {
        this.errorMsg = resFileError;
        deleteRequestData.forEach(element => {
          count++;
          this.deleteFailedObjects.push(element);
          // Turn 'off' the checkbox for the object thumbnail for which 'delete' failed.												
          let index = "chk" + this.allDetails.content.findIndex(i => i.id === element);
          let chkbox = document.getElementById(index) as HTMLInputElement;
          chkbox.checked = false;
        })
        if ((count === (objectsInResponseCount - 1) && (page === (totalPages - 1)))) {
          sessionStorage.removeItem('addtosavelistobjects');
          sessionStorage.removeItem('thumbnailselectall');
          this.selectedPropertyIds = [];
          setTimeout(() => {
            this.selectView.nativeElement.value = 'selected';
            this.showDeleteStatus(this.deletedObjects, this.deleteFailedObjects);
          }, 500);
        }

        if (totalPages > 1) {
          this.getSearchDetails(0);
        }

      })
  }


  @HostListener('document:click', ['$event'])
  handleClick(e) {
    let deletedObjects: number = 0;
    let deleteFailedObjects = [];
    if (e && (e.target && e.target.classList && (JSON.stringify(e.target.classList).indexOf('deleteThumbnails') > -1))) {
      // Select ALL
      if (this.selectedPropertyIds && this.selectedPropertyIds.length > 0 || (sessionStorage.getItem('thumbnailselectall') === "true" ? true : false)) {
        if (this.flag === 'savelist') {
          this._savedListService.getSavedListById(this.userName, this.savedListId)
            .subscribe(result => {
              if (result.openSaveList || (this.userName === result.user)) {
                this.dialogService.addDialog(ConfirmComponent, { title: '', message: 'Are you sure you want to delete the(se) object(s)?' },
                  { backdropColor: 'rgba(220,220,220,0.5)' })
                  .subscribe((isConfirmed) => {
                    this.confirmResult = isConfirmed;
                    if (this.confirmResult && sessionStorage.getItem('thumbnailselectall') && sessionStorage.getItem('thumbnailselectall') === 'true' /*&& !sessionStorage.getItem('objectsUnchecked')*/) {
                      // Saved List - Select ALL
                      if (this.flag === 'savelist') {
                        this.updateTotalObjectsToBeDeleted = true;
                        this.getSavedListPage(0);
                      }
                      // Select ALL
                      else {
                        this.updateTotalObjectsToBeDeleted = true;
                        this.getSearchDetails(0)
                      }
                    }
                    // Select individual objects for 'delete' manually
                    else if (this.confirmResult) {
                      let deletedObjectsRequest = {};
                      this.selectedPropertyIds.forEach((dataId, i) => {
                        this.objectservice
                          .removeProperty(dataId)
                          .subscribe(Response => {
                            deletedObjects++;
                            var index = this.allDetails.content.findIndex(i => i.id === dataId);
                            if (index > -1) {
                              this.allDetails.content.splice(index, 1);
                            }
                            if (this.allDetails.content.length === 0 || i === (this.selectedPropertyIds.length - 1)) {
                              sessionStorage.removeItem('addtosavelistobjects');
                              this.selectedPropertyIds = [];
                              this.showDeleteStatus(deletedObjects, deleteFailedObjects);
                            }
                          }, resFileError => {
                            this.errorMsg = resFileError;
                            deleteFailedObjects.push(dataId);
                            let index = "chk" + this.allDetails.content.findIndex(i => i.id === dataId);
                            // Turn 'off' the checkbox for the object thumbnail for which 'delete' failed.
                            let chkbox = document.getElementById(index) as HTMLInputElement;
                            chkbox.checked = false;
                            if (this.allDetails.content.length === 0 || i === (this.selectedPropertyIds.length - 1)) {
                              sessionStorage.removeItem('addtosavelistobjects');
                              this.selectedPropertyIds = [];
                              setTimeout(() => {
                                this.showDeleteStatus(deletedObjects, deleteFailedObjects);
                              }, 500);
                            }
                          });
                      });
                    }
                  });
              } else {
                document.getElementsByTagName("body")[0].classList.add("positionFixed");
                document.getElementsByTagName('html')[0].classList.remove('loading');
                this.dialogService.addDialog(AlertComponent,
                  { title: 'Warning...!', message: "This list's status is Closed. Please reach out to " + '"' + result.user + '"' + " to amend list." }, { backdropColor: 'rgba(220,220,220,0.5)' })
                  .subscribe((isConfirmed) => {
                    document.getElementsByTagName("body")[0].classList.remove("positionFixed");
                  });
              }
            }, error => {
              document.getElementsByTagName('html')[0].classList.remove('loading');
            });
        } else {
          this.dialogService.addDialog(ConfirmComponent, { title: '', message: 'Are you sure you want to delete the(se) object(s)?' },
            { backdropColor: 'rgba(220,220,220,0.5)' })
            .subscribe((isConfirmed) => {
              this.confirmResult = isConfirmed;
              if (this.confirmResult && sessionStorage.getItem('thumbnailselectall') && sessionStorage.getItem('thumbnailselectall') === 'true' /*&& !sessionStorage.getItem('objectsUnchecked')*/) {
                // Saved List - Select ALL
                if (this.flag === 'savelist') {
                  this.updateTotalObjectsToBeDeleted = true;
                  this.getSavedListPage(0);
                }
                // Select ALL
                else {
                  this.updateTotalObjectsToBeDeleted = true;
                  this.getSearchDetails(0)
                }
              }
              // Select individual objects for 'delete' manually
              else if (this.confirmResult) {
                let deletedObjectsRequest = {};
                this.selectedPropertyIds.forEach((dataId, i) => {
                  this.objectservice
                    .removeProperty(dataId)
                    .subscribe(Response => {
                      deletedObjects++;
                      var index = this.allDetails.content.findIndex(i => i.id === dataId);
                      if (index > -1) {
                        this.allDetails.content.splice(index, 1);
                      }
                      if (this.allDetails.content.length === 0 || i === (this.selectedPropertyIds.length - 1)) {
                        sessionStorage.removeItem('addtosavelistobjects');
                        this.selectedPropertyIds = [];
                        this.showDeleteStatus(deletedObjects, deleteFailedObjects);
                      }
                    }, resFileError => {
                      this.errorMsg = resFileError;
                      deleteFailedObjects.push(dataId);
                      let index = "chk" + this.allDetails.content.findIndex(i => i.id === dataId);
                      // Turn 'off' the checkbox for the object thumbnail for which 'delete' failed.
                      let chkbox = document.getElementById(index) as HTMLInputElement;
                      chkbox.checked = false;
                      if (this.allDetails.content.length === 0 || i === (this.selectedPropertyIds.length - 1)) {
                        sessionStorage.removeItem('addtosavelistobjects');
                        this.selectedPropertyIds = [];
                        setTimeout(() => {
                          this.showDeleteStatus(deletedObjects, deleteFailedObjects);
                        }, 500);
                      }
                    });
                });
              }
            });
        }
      } else {
        this.dialogService.addDialog(AlertComponent,
          { title: 'Select Objects', message: "No object(s) selected for delete." }, { backdropColor: 'rgba(220,220,220,0.5)' })
          .subscribe((isConfirmed) => { });
      }
    } else {
      if (e && (e.target && e.target.classList && (JSON.stringify(e.target.classList).indexOf('removeFromList') > -1))) {
        this._savedListService.getSavedListById(this.keycloak.tokenParsed.preferred_username, this.savedListId)
          .subscribe(Response => {
            if (Response.openSaveList || (this.userName === Response.user)) {
              this.savedListName = Response.name;
              this.objectsToBeRemoved = sessionStorage.getItem('addtosavelistobjects').split(',');
              this.objectsToBeRemoved.splice(-1, 1);
              if (this.objectsToBeRemoved.length > 0) {
                this.dialogService.addDialog(ConfirmComponent, { title: '', message: 'Are you sure you would like to remove the selected ' + this.objectsToBeRemoved.length + '  object(s) from Saved List, " ' + Response.name + ' "?' },
                  { backdropColor: 'rgba(220,220,220,0.5)' })
                  .subscribe((isConfirmed) => {
                    if (isConfirmed) {
                      if (this.keycloak.tokenParsed.preferred_username == Response.user || (JSON.stringify(Response.sharedUsersList).indexOf(this.keycloak.tokenParsed.preferred_username) > -1 && Response.openSaveList)) {
                        if (Response.user != 'property-importer') {
                          this.objectsToBeRemoved = [];
                          this.objectsToBeRemoved = sessionStorage.getItem('addtosavelistobjects').split(',');
                          this.objectsToBeRemoved.splice(-1, 1);
                          this.removeFromList(this.objectsToBeRemoved[0], 0);

                        } else {
                          this.tosatservice.addToastError('Objects may not be removed from Import Saved Lists.', 'top-right')
                        }
                      } else {
                        this.tosatservice.addToastError('You do not have access to remove objects from this list. Please contact ' + Response.user + ' if you would like to make any changes', 'top-right')
                      }
                    }
                  })
              }
            } else {
              document.getElementsByTagName("body")[0].classList.add("positionFixed");
              document.getElementsByTagName('html')[0].classList.remove('loading');
              this.dialogService.addDialog(AlertComponent,
                { title: 'Warning...!', message: "This list's status is Closed. Please reach out to " + '"' + Response.user + '"' + " to amend list." }, { backdropColor: 'rgba(220,220,220,0.5)' })
                .subscribe((isConfirmed) => {
                  document.getElementsByTagName("body")[0].classList.remove("positionFixed");
                });
            }
          })
      }
    }
  }



  // Display Object delete status
  showDeleteStatus(deletedObjects, deleteFailedObjects) {
    document.getElementsByTagName("body")[0].classList.add("positionFixed");
    // If all objects were deleted successfully
    if (deleteFailedObjects.length === 0 && deletedObjects > 0) {
      this.getCurrValueThumnails(1, "enter");
      document.getElementsByTagName("body")[0].classList.remove("positionFixed");
      this.tosatservice.addToast('Object(s) deleted successfully.', 'top-right');
    }
    else {
      let info: {};
      // Partial delete successful
      if (deletedObjects > 0 && deleteFailedObjects.length > 0) {
        info = 'allFailed:false||failedTotal:' + deleteFailedObjects.length + '||failedObjects:' + deleteFailedObjects.join() + '||deleted:' + deletedObjects;
      }
      // No objects were deleted
      else if (deletedObjects == 0 && deleteFailedObjects.length > 0) {
        info = 'allFailed:true||failedTotal:' + deleteFailedObjects.length + '||failedObjects:' + deleteFailedObjects.join();
      }
      this.dialogService.addDialog(DeleteObjectStatusComponent,
        { title: 'Delete Object(s)', message: info })
        .subscribe((isConfirmed) => { document.getElementsByTagName("body")[0].classList.remove("positionFixed"); });
    }
  }

  // Remove duplicates from an array - EOS-2053
  removeDuplicates(arrayToBeFiltered) {
    var arr = arrayToBeFiltered;
    arr = arr.filter((object, index, self) => self.findIndex(property => property.id === object.id) === index);
    return arr;
  }

  // EOS-2061, EOS-2063 - Highlight merged/deleted objects in thumbnail view
  highlightThumbnail(imageId, objectId) {
    this.objectservice.getPropertyMerge(objectId)
      .subscribe((Response) => {
      }, resFileError => {
        let errBody = JSON.parse(resFileError._body);
        if ((errBody.details.indexOf('merged')) > -1 || (errBody.details.indexOf('exist')) > -1) {
          let thumbnailImg = document.getElementById('thumbnail_' + imageId) as HTMLImageElement;
          (<HTMLElement>thumbnailImg.closest('a.thumb-link')).style.backgroundColor = '#fdb9b9';
        }
      });

  }

  sort(arr) {
    var len = arr.length;
    for (var i = len - 1; i >= 0; i--) {
      for (var j = 1; j <= i; j++) {
        if (arr[j - 1].sortId > arr[j].sortId) {
          var temp = arr[j - 1];
          arr[j - 1] = arr[j];
          arr[j] = temp;
        }
      }
    }
    return arr;
  }

  startDeleteProgressBar() {
    this.showDeleteProgressbar = true;
  }

  endDeleteProgressBar() {
    this.deletedObjectsCount = 0;
    this.totalobjectstobedeleted = 0;
    this.showDeleteProgressbar = false;
  }
  // EOS-1173 Ascending/Descending Sort Order
  // Sort Ascending
  public sortAscending() {
    if (this.sorted && this.sortOrder != 'asc') {
      this.sortOrder = 'asc';
      // Saved List
      if (this.flag == 'savelist') {
        if (this.saveListSortParam.indexOf(',asc') === -1) {
          if (this.saveListSortParam.indexOf(',desc') >= 0) {
            this.saveListSortParam = this.saveListSortParam.replace(/,desc/i, ',asc');
          }
        }
        this.curPageValue = 1;
        this.enterP = 0;
        this.page = 0;
        this.loading = true;
        this.getSavedListProperties(this.userName, this.savedListId, 32);
      }
      // Object Search
      else {
        if (this.queryParam.indexOf(',asc') === -1) {
          if (this.queryParam.indexOf(',desc') >= 0) {
            this.queryParam = this.queryParam.replace(/,desc/i, ',asc');
          }
        }
        this.curPageValue = 1;
        this.enterP = 0;
        this.page = 0;
        this.loading = true;
        this.getDetails(0);
      }
      this.highlightSortIcon('asc');
    }
  }

  // EOS-1173 Ascending/Descending Sort Order
  // Sort Descending
  public sortDescending() {
    if (this.sorted && this.sortOrder != 'desc') {
      this.sortOrder = 'desc';
      // Saved List
      if (this.flag == 'savelist') {
        if (this.saveListSortParam.indexOf(',desc') === -1) {
          if (this.saveListSortParam.indexOf(',asc') >= 0) {
            this.saveListSortParam = this.saveListSortParam.replace(/,asc/i, ',desc');
          }
        }
        this.curPageValue = 1;
        this.enterP = 0;
        this.page = 0;
        this.loading = true;
        this.getSavedListProperties(this.userName, this.savedListId, 32);
      }
      // Object Search
      else {
        if (this.queryParam.indexOf(',desc') === -1) {
          if (this.queryParam.indexOf(',asc') >= 0) {
            this.queryParam = this.queryParam.replace(/,asc/i, ',desc');
          }
        }
        this.curPageValue = 1;
        this.enterP = 0;
        this.page = 0;
        this.loading = true;
        this.getDetails(0);
      }
      this.highlightSortIcon('desc');
    }
  }

  highlightSortIcon(sortOrder) {
    let sortIconAsc = document.getElementById('sort-asc') as HTMLImageElement;
    let sortIconDesc = document.getElementById('sort-desc') as HTMLImageElement;
    if (sortOrder === 'asc' && this.allDetails.content.length > 1) {
      (<HTMLElement>sortIconAsc).style.color = '#fff';
      (<HTMLElement>sortIconDesc).style.color = '#999';
    } else if (sortOrder === 'desc' && this.allDetails.content.length > 1) {
      (<HTMLElement>sortIconDesc).style.color = '#fff';
      (<HTMLElement>sortIconAsc).style.color = '#999';
    } else {
      (<HTMLElement>sortIconDesc).style.color = '#999';
      (<HTMLElement>sortIconAsc).style.color = '#999';
    }
  }

  private checkObjectItems(objectIds: any[]): Observable<any> {
    this.eventRService.getItemsByUrl(this.eventRService.getURLToSearch('', objectIds), true);

    return this.eventRService.elements
      .skip(1)
      .take(1)
      .map((items: any) => {
        const objectsWithItems = [].concat(...items
          .map(item => item.objects.map((object) => object.objectId)))
          .filter((value, index, array) => array.indexOf(value) === index);

        return {
          withItems: objectsWithItems,
          toRemove: objectIds.filter(objectId => objectsWithItems.indexOf(objectId) === -1)
        }
      })
  }

  private showNotifications(objects: any) {
    if (objects.withItems.length) {
      this.tosatservice.addToastError(`${objects.withItems.join(', ')} ${this.dictionary.NOTIFICATIONS.objectDeleteErrorItems}`, 'top-right');
    }
    if (objects.toRemove.length) {
      this.tosatservice.addToast(`${objects.toRemove.join(', ')} ${this.dictionary.NOTIFICATIONS.objectDeleteSuccess}`, 'top-right');
    }
  }
  removeFromList(propertyid, i) {
    this._savedListService.removeFromList(this.keycloak.tokenParsed.preferred_username, this.savedListId, propertyid)
      .subscribe(response => {
        this.objectsRemovedCount++;
        var index = this.allDetails.content.findIndex(i => i.id === propertyid);
        if (index > -1) {
          this.allDetails.content.splice(index, 1);
        }
        if (this.allDetails.content.length <= 0) {
          this.emptyPagination = "No records found";
          this.widthStyle = 115;
        }
        if (i < this.objectsToBeRemoved.length - 1) {
          ++i;
          this.removeFromList(this.objectsToBeRemoved[i], i);
        } else {
          sessionStorage.removeItem('addtosavelistobjects');
          sessionStorage.removeItem('thumbnailselectall');
          this.tosatservice.addToast(this.objectsRemovedCount + ' object(s) have been removed from the Saved List, "' + this.savedListName + ' "', 'top-right');
          this.objectsRemovedCount = 0;
          this.objectsToBeRemoved = [];
        }
      }, resFileError => {
        if (this.allDetails.content.length <= 0) {
          this.emptyPagination = "No records found";
          this.widthStyle = 115;
        }
        if (i < this.objectsToBeRemoved.length - 1) {
          ++i;
          this.removeFromList(this.objectsToBeRemoved[i], i);
        } else {
          sessionStorage.removeItem('addtosavelistobjects');
          sessionStorage.removeItem('thumbnailselectall');
          this.objectsToBeRemoved = [];
          this.tosatservice.addToast(this.objectsRemovedCount + ' object(s) have been removed from the Saved List, "' + this.savedListName + ' "', 'top-right');
          this.objectsRemovedCount = 0;
        }
      })
  }
}