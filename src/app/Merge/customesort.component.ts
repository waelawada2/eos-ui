import { Component, OnInit } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { ObjectService } from '../../app/object/object.service';

export interface CustomModal {
  customSort: any;
}

@Component({
  selector: 'prompt',
  providers: [ ObjectService ],
  template: `<div class="modal-dialog" style="width:680px;">
  <div class="modal-content">
    <div class="modal-header">
      <h4 class="modal-title pull-left">Custom Sort</h4>
      <div class="pull-right">
        <button type="button" class="btn btn-primary" (click)="updateOptions()">OK</button>
        <button type="button" class="btn btn-default" (click)="cancel()">Cancel</button>
      </div>
    </div>
    <div class="modal-body">
      <ng2-dual-list-box [data]="items" availableText="" selectedText="" onAvailableItemSelected="<" onSelectedItemsSelected=">" valueField="id" textField="name"
        (onAvailableItemSelected)="listChange($event)" (onSelectedItemsSelected)="listChange($event)"
        (onItemsMoved)="listChange($event)" [(ngModel)]="selected"></ng2-dual-list-box>
    </div>
    <div class="modal-footer"></div>
    </div>
    </div>`
})
export class CustomSortComponent extends DialogComponent<CustomModal, string> implements CustomModal, OnInit {

  customSort: any;
  items = [];

  constructor(private _fb: FormBuilder, dialogService: DialogService, private _objectService: ObjectService,) {
    super(dialogService);
  }

  ngOnInit() {
    let sessionData:any = sessionStorage.getItem('indexSort');
    if(sessionData&&sessionData.length){
      sessionData = sessionData.split(',');
      sessionData.forEach((d)=>{
        if(parseInt(d)>-1){
          var indexData = this.itemsData[d];
          this.selected.push(indexData.id);
          this.iSort.push(parseInt(d));
          this.items.push(indexData);
        }
      });
    }
    var rDuplicate = this.items.concat(this.itemsData);
    this.items = this.removeDuplicatesData(rDuplicate,'name');
  }

  //Remove duplicate record
  removeDuplicatesData(myArr, prop) {
		return myArr.filter((obj, pos, arr) => {
			return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
		});
  }
  
  //Items Data
  itemsData= [ {name:'Artist',id: 'artists.lastName'},
           {name:'Title (EN)', id: 'title'},
           {name:'Title (Orig)', id: 'originalTitle'},
           {name:'Medium', id: 'medium'},
           {name:'Category', id: 'category.name'},
           {name:'Sub-Category', id: 'category.name'},
           {name:'Parts', id: 'parts'},
           {name:'Year - From', id: 'dateDetails.yearStart'},
           {name:'Cast Year - From', id: 'dateDetails.castYearStart'},
           {name:'Edition No.', id: 'edition.editionNumber'},
           {name:'Edition Type', id: 'edition.editionType'},
           {name:'Edition Total', id: 'edition.editionSize.editionSizeTotal'},
           {name:'Weight (kg)', id: 'dimensions.weight'},
           {name:'Foundry', id: 'Foundry'},
           {name:'Sort Code', id: 'sortCode'},
           {name:'Researcher Name', id: 'researcherNotes.researcherName'},
           {name:'Researcher Notes Date', id: 'researcherNotes.date'},
           {name:'Object Created Date', id: 'createdDate'},
           {name:'Object Last Amended Date', id: 'updatedDate'},
           {name:'Height (cm)',id:'dimensions.height'},
           {name:'Width (cm)',id:'dimensions.width'},
           {name:'Depth (cm)',id:'dimensions.depth'},
           {name:'Height (in)',id:'dimensions.heightInches'},
           {name:'Width (in)',id:'dimensions.widthInches'},
           {name:'Depth (in)',id:'dimensions.depthInches'},
           {name:'Year - To', id: 'dateDetails.yearEnd'},
           {name:'Cast Year - To', id: 'dateDetails.castYearEnd'},
           {name:'Pre-size Type', id: 'presizeType.name'},
           {name:'Signature Details', id: 'signature'},
           {name:'Size Text', id: 'sizeText'},
           {name:'Year Text',id: 'dateDetails.yearText'},
           {name:'Archive No.',id: 'archiveNumbers.archiveNumber'},
           {name:'Flags - Authenticity',id: 'flags.flagAuthenticity'},
           {name:'Flags - Country',id: 'flags.flagsCountry'},
           {name:'Flags - Medium',id: 'flags.flagMedium'},
           {name:'Flags - Restitution',id: 'flags.flagRestitution'},
           {name:'Flags - Condition',id: 'flags.flagCondition'},
           {name:'Flags - SFS',id: 'flags.flagSfs'},
           {name:'Check - Signed',id: 'authenticity.signed'},
           {name:'Check - Certificate',id: 'authenticity.certificate'},
           {name:'Check - Stamped',id: 'authenticity.stamped'},
           {name:'Check - Authentication',id: 'authenticity.authentication'},
           {name:'Check - Year Circa',id: 'dateDetails.yearCirca'},
           {name:'Check - Cast Year Circa',id: 'dateDetails.castYearCirca'},
           {name:'Check - PH',id: 'posthumous'},
           {name:'Area/Volume',id: 'tags.autoVolume'}
           ];

  selected = []; 
  iSort:any = [];
  listChange(event){
    if(event && event.selected && event.selected.length){
      this.selected = event.selected;
      this.iSort = [];
      event.selected.forEach((s)=>{
        var i = this.itemsData.findIndex(i => i.name === s.text);
        this.iSort.push(i);
      });
    }
  }

  finalQuery =[];
  //submit details
  updateOptions() {
    document.getElementsByTagName( "html" )[0].classList.add( "loading" );
    this.finalQuery = [];
    this.selected.forEach((data,index) =>{
      if(data.indexOf('Inches') > -1){
        data = data.substr(0,data.length-6);
       }
      if(!index){
        this.finalQuery.push("&sort="+data);        
      } else{
        this.finalQuery.push(data);        
      }
    });
    this.iSort = this.selected.length>0?this.iSort:[]; 
    this.result = this.finalQuery.join(",");
    // Get 'Sort' param for saved search.
    let sortParam = <any>this.result;
    sortParam = sortParam.substring(6, sortParam.length);
		sessionStorage.setItem('sortparam', sortParam);
		sessionStorage.setItem('indexSort', this.iSort);
    this.close();
  }
  
  public cancelFlag = "false";
  //close pop-up
  cancel(){
    this.result = this.cancelFlag;
    this.close();
  }
}