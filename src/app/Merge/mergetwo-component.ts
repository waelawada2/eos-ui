import { Component, OnInit, HostListener } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { Http, Response } from '@angular/http';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { ObjectService } from '../../app/object/object.service';
import { AppService } from '../../app/app.service';
import { ArtistService } from '../../app/artist/artist.service';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";
import 'rxjs/Rx';
import { CompleterService, CompleterData } from 'ng2-completer';
import { TosatService } from '../common/toasty-service';
import { EditionModalComponent } from '../../app/modal/newEditionModal.component';
import { ConfirmComponent } from '../common/confirm-component';
import { ConfigurationLoaderService } from 'app/configuration.service'

export interface MergeModel {
  Dragged: any;
  Drop: any;
}

@Component({
  selector: 'MergeTwo',
  templateUrl: './MergeTwo.component.html',
  styleUrls: ['./MergeTwo.component.css'],
  providers: [AppService, ObjectService, ArtistService]
})
export class MergeTwoComponent extends DialogComponent<MergeModel, boolean> implements MergeModel, OnInit {
  Dragged: any;
  Drop: any;
  URL: string
  public DropImage: string;
  public DragImage: string;
  public thumbnail_deleteid: any = '-1';
  public dropDetailsFlagsCountryValue = null;
  public dragDetailsFlagsCountryValue = null;

  // Select List 'OPEN/CLOSE' status
  private dropCategoryListOpen = false;
  private dragCategoryListOpen = false;
  private dropSubCategoryListOpen = false;
  private dragSubCategoryListOpen = false;
  private dropPresizeNameListOpen = false;
  private dragPresizeNameListOpen = false;
  private dropEditionNameListOpen = false;
  private dragEditionNameListOpen = false;
  private dropFlagsCountryListOpen = false;
  private dragFlagsCountryListOpen = false;
  private eventHistoryOptions: object = { headline: false, height: 300 }

  //Drag & Drop images flag
  public imageDragImag1 = false;
  public imageDragImag2 = false;
  public expandImagePathDrag = null;
  public expandImagePathDrop = null;
  public displayProgressBar = true;
  public dropDisplayName: string = null;
  public dragDisplayName: string = null;
  public dropResearcherDate: string = null;
  public dragResearcherDate: string = null;

  ngOnInit() {
    this.presizeNames = [];
    this.editionTypes = [];
    var d = new Date();
    var n = d.getTime();
    if (this.Drop.imagePath) {
      this.DropImage = this.URL + '/properties/' + this.Drop.id + '/images/default?' + n;
      if (this.doesFileExist(this.DropImage)) {
        this.imageDragImag1 = true;
      }
    }
    if (this.Dragged.imagePath) {
      this.DragImage = this.URL + '/properties/' + this.Dragged.id + '/images/default?' + n;
      if (this.doesFileExist(this.DragImage)) {
        this.imageDragImag2 = true;
      }
    }
    // Dynamically set column height to span to the height of the browser window
    this._objectService
      .getPreSizeTypes()
      .subscribe(presizeType => {
        presizeType.forEach((presizeType, index) => {
          this.presizeNames.push({ 'name': presizeType.name, 'id': presizeType.id, "description": presizeType.description });
        });
      }, resFileError => this.errorMsg = resFileError);

    this._objectService
      .getEditionTypes()
      .subscribe(editionTypes => {
        editionTypes.forEach((editionType, index) => {
          if (this.editionTypes.findIndex(item => item.id == editionType.id) < 0)
            this.editionTypes.push({ 'name': editionType.name, 'id': editionType.id, "description": editionType.description });
        });
      }, resFileError => this.errorMsg = resFileError);

    // Maps Categories to 'Category' dropdown using ng-completer component
    this.categoryService = <any>this.completerService.local(this.categories, 'name', 'name');

    // Maps Categories to 'Category' dropdown using ng-completer component
    this.subCategoryService = <any>this.completerService.local(this.subcategories, 'name', 'name');

    // Maps Presize Types to 'Presize Type' dropdown using ng-completer component
    this.presizeTypesService = <any>this.completerService.local(this.presizeNames, 'name', 'name');

    // Maps Edition Types to 'Presize Type' dropdown using ng-completer component
    this.editionTypeService = <any>this.completerService.local(this.editionTypes, 'name', 'name');

    // Maps Countries to 'Countries' dropdown in Flags section using ng-completer component
    this.countryService = <any>this.completerService.local(this.countries, 'name', 'name');

    this.dropDetailsFlagsCountryValue = (this.Drop.flags && this.Drop.flags.flagsCountry) ? this.Drop.flags.flagsCountry : null;
    this.DropDetailsForm = this.builder.group({
      artistData: new FormControl("", [Validators.required]),
      imagePath: new FormControl(),
      title: new FormControl(this.Drop.title ? this.Drop.title : null),
      originalTitle: new FormControl(this.Drop.originalTitle ? this.Drop.originalTitle : null),
      signature: new FormControl(this.Drop.signature ? this.Drop.signature : null),
      medium: new FormControl(this.Drop.medium ? this.Drop.medium : null),
      category: new FormControl(this.Drop.parentCategory ? this.Drop.parentCategory.name : null),
      subCategory: new FormControl(this.Drop.subCategory ? this.Drop.subCategory.name : null),
      presizeName: new FormControl(this.Drop.presizeType ? this.Drop.presizeType.name : null),
      parts: new FormControl(this.Drop.parts, [Validators.pattern('^[1-9][0-9]{3}$')]),
      heightCm: new FormControl(this.Drop.heightCm ? this.Drop.heightCm : null, [Validators.pattern('^[1-9][0-9]{3}$')]),
      widthCm: new FormControl(this.Drop.widthCm ? this.Drop.widthCm : null, [Validators.pattern('^[1-9][0-9]{3}$')]),
      depthCm: new FormControl(this.Drop.depthCm ? this.Drop.depthCm : null, [Validators.pattern('^[1-9][0-9]{3}$')]),
      heightIn: new FormControl(this.Drop.heightIn ? this.Drop.heightIn : null),
      widthIn: new FormControl(this.Drop.widthIn ? this.Drop.widthIn : null),
      depthIn: new FormControl(this.Drop.depthIn ? this.Drop.depthIn : null),
      sizeText: new FormControl(this.Drop.sizeText ? this.Drop.sizeText : null),
      yearStart: new FormControl(this.Drop.yearStart ? this.Drop.yearStart : null, [Validators.pattern('^[1-9][0-9]{3}$')]),
      yearEnd: new FormControl(this.Drop.yearEnd ? this.Drop.yearEnd : null, [Validators.pattern('^[1-9][0-9]{3}$')]),
      castYearStart: new FormControl(this.Drop.castYearStart ? this.Drop.castYearStart : null, [Validators.pattern('^[1-9][0-9]{3}$')]),
      castYearEnd: new FormControl(this.Drop.castYearEnd ? this.Drop.castYearEnd : null, [Validators.pattern('^[1-9][0-9]{3}$')]),
      yearText: new FormControl(this.Drop.yearText ? this.Drop.yearText : null),
      yearCirca: new FormControl(this.Drop.yearCirca ? this.Drop.yearCirca : null),
      castYearCirca: new FormControl(this.Drop.castYearCirca ? this.Drop.castYearCirca : null),
      posthumous: new FormControl(this.Drop.posthumous ? this.Drop.posthumous : null),
      editionNumber: new FormControl(this.Drop.editionNumber ? this.Drop.editionNumber : null),
      editionName: new FormControl(this.Drop.editionName ? this.Drop.editionName : null),
      editionSizeTotal: new FormControl(this.Drop.editionOverride ? this.Drop.editionOverride : (this.Drop.editionSizeTotal ? this.Drop.editionSizeTotal : null)),
      weight: new FormControl(this.Drop.weight, [Validators.pattern('^[1-9][0-9]{3}$')]),
      foundry: new FormControl(this.Drop.foundry ? this.Drop.foundry : null),
      certificate: new FormControl(this.Drop.certificate ? this.Drop.certificate : null),
      signed: new FormControl(this.Drop.signed ? this.Drop.signed : null),
      stamped: new FormControl(this.Drop.stamped ? this.Drop.stamped : null),
      authentication: new FormControl(this.Drop.authentication ? this.Drop.authentication : null),
      archiveNumbers: new FormControl(),
      sortCode: new FormControl(this.Drop.sortCode ? this.Drop.sortCode : null),
      flags: this.builder.group({
        flagAuthenticity: new FormControl((this.Drop.flags && this.Drop.flags.flagAuthenticity) ? this.Drop.flags.flagAuthenticity : false),
        flagRestitution: new FormControl((this.Drop.flags && this.Drop.flags.flagRestitution) ? this.Drop.flags.flagRestitution : false),
        flagSfs: new FormControl((this.Drop.flags && this.Drop.flags.flagSfs) ? this.Drop.flags.flagSfs : false),
        flagCondition: new FormControl((this.Drop.flags && this.Drop.flags.flagCondition) ? this.Drop.flags.flagCondition : false),
        flagMedium: new FormControl((this.Drop.flags && this.Drop.flags.flagMedium) ? this.Drop.flags.flagMedium : false),
        flagsCountry: new FormControl((this.Drop.flags && this.Drop.flags.flagsCountry) ? this.Drop.flags.flagsCountry : null)
      }),
      researcherName: new FormControl(this.Drop.researcherName ? this.Drop.researcherName : null),
      researcherNotesDate: new FormControl(this.Drop.researcherNotesDate ? this.Drop.researcherNotesDate : null),
      researchCompleteCheck: new FormControl(this.Drop.researchCompleteCheck ? this.Drop.researchCompleteCheck : false),
      researcherNotesText: new FormControl(this.Drop.researcherNotesText ? this.Drop.researcherNotesText : null),
      volume: new FormControl(),
      number: new FormControl(),

      tags: this.builder.group({
        imageSubjects: new FormControl((this.Drop.tags && this.Drop.tags.imageSubjects) ? this.Drop.tags.imageSubjects : null),
        imageFigure: new FormControl((this.Drop.tags && this.Drop.tags.imageFigure) ? this.Drop.tags.imageFigure : null),
        imageGenders: new FormControl((this.Drop.tags && this.Drop.tags.imageGenders) ? this.Drop.tags.imageGenders : null),
        imagePositions: new FormControl((this.Drop.tags && this.Drop.tags.imagePositions) ? this.Drop.tags.imagePositions : null),
        imageAnimals: new FormControl((this.Drop.tags && this.Drop.tags.imageAnimals) ? this.Drop.tags.imageAnimals : null),
        imageMainColours: new FormControl((this.Drop.tags && this.Drop.tags.imageMainColours) ? this.Drop.tags.imageMainColours : null),
        imageText: new FormControl((this.Drop.tags && this.Drop.tags.imageText) ? this.Drop.tags.imageText : null),
        expertiseLocation: new FormControl((this.Drop.tags && this.Drop.tags.expertiseLocation) ? this.Drop.tags.expertiseLocation : null),
        expertiseSitters: new FormControl((this.Drop.tags && this.Drop.tags.expertiseSitters) ? this.Drop.tags.expertiseSitters : null),
        autoOrientation: new FormControl((this.Drop.tags && this.Drop.tags.autoOrientation) ? this.Drop.tags.autoOrientation : null),
        autoImage: new FormControl((this.Drop.tags && this.Drop.tags.autoImage) ? this.Drop.tags.autoImage : null),
        autoScale: new FormControl((this.Drop.tags && this.Drop.tags.autoScale) ? this.Drop.tags.autoScale : null),
        otherTags: new FormControl((this.Drop.tags && this.Drop.tags.otherTags) ? this.Drop.tags.otherTags : null),
        autoAreaCm: new FormControl((this.Drop.tags && this.Drop.tags.autoAreaCm) ? this.Drop.tags.autoAreaCm : null),
        autoAreaIn: new FormControl((this.Drop.tags && this.Drop.tags.autoAreaIn) ? this.Drop.tags.autoAreaIn : null)
      })
    });
    if (this.Drop.researcherNotesDate) {
      this.DropDetailsForm.controls['researcherNotesDate'].setValue(this.getReasearcherDate(this.Drop.researcherNotesDate, "mm-dd-yyyy"))
      this.dropResearcherDate = this.getReasearcherDate(this.Drop.researcherNotesDate, "mm-dd-yyyy");
    }
    if (this.Drop.researcherName) {
      this.DropDetailsForm.controls['researcherName'].setValue(this.getUpdatedResaearcherName(this.Drop.researcherName));
      this.dropDisplayName = this.getUpdatedResaearcherName(this.Drop.researcherName)
    }

    if (this.Drop.researcherNotesText && this.Drop.researcherNotesTex != null && this.Drop.researcherNotesText.length <= 0) {
      this.DragDetailsForm.controls['researcherNotesText'].setValue(null);
    }

    this.checkedArtist = [];
    this.artistName.splice(0, this.artistName.length);
    this.Drop.artists.forEach(data => {
      var bd = ((data.birthYear && data.deathYear) ? "(" + data.birthYear + "-" + data.deathYear + ")" : (data.birthYear ? "(b." + data.birthYear + ")" : ''));
      this.artistName.push({
        firstName: (data.firstName ? data.firstName + ' ' : '') + data.lastName,
        display: data.display ? data.display : ((data.firstName ? data.firstName + ' ' : '') + data.lastName + bd),
        birthYear: data.birthYear,
        deathYear: data.deathYear,
        id: data.id
      });
    });
    this.checkedArtist = this.checkedArtist.concat(this.artistName);
    this.checkedArtistDrag = [];
    this.artistNameDrag.splice(0, this.artistNameDrag.length);
    this.Dragged.artists.forEach(data => {
      var bd = ((data.birthYear && data.deathYear) ? "(" + data.birthYear + "-" + data.deathYear + ")" : (data.birthYear ? "(b." + data.birthYear + ")" : ''));
      this.artistNameDrag.push({
        firstName: (data.firstName ? data.firstName + ' ' : '') + data.lastName,
        display: data.display ? data.display : ((data.firstName ? data.firstName + ' ' : '') + data.lastName + bd),
        birthYear: data.birthYear,
        deathYear: data.deathYear,
        id: data.id
      });
    });
    this.checkedArtistDrag = this.checkedArtistDrag.concat(this.artistNameDrag);
    this.dragDetailsFlagsCountryValue = (this.Dragged.flags && this.Dragged.flags.flagsCountry) ? this.Dragged.flags.flagsCountry : null;
    this.DragDetailsForm = this.builder.group({
      artistDataDrag: new FormControl("", [Validators.required]),
      imagePath: new FormControl(""),
      title: new FormControl(this.Dragged.title ? this.Dragged.title : null),
      originalTitle: new FormControl(this.Dragged.originalTitle ? this.Dragged.originalTitle : null),
      signature: new FormControl(this.Dragged.signature ? this.Dragged.signature : null),
      medium: new FormControl(this.Dragged.medium ? this.Dragged.medium : null),
      category: new FormControl(this.Dragged.parentCategory ? this.Dragged.parentCategory.name : null),
      subCategory: new FormControl(this.Dragged.subCategory ? this.Dragged.subCategory.name : null),
      presizeName: new FormControl(this.Dragged.presizeType ? this.Dragged.presizeType.name : null),
      parts: new FormControl(this.Dragged.parts, [Validators.pattern('^[1-9][0-9]{3}$')]),
      heightCm: new FormControl(this.Dragged.heightCm ? this.Dragged.heightCm : null, [Validators.pattern('^[1-9][0-9]{3}$')]),
      widthCm: new FormControl(this.Dragged.widthCm ? this.Dragged.widthCm : null, [Validators.pattern('^[1-9][0-9]{3}$')]),
      depthCm: new FormControl(this.Dragged.depthCm ? this.Dragged.depthCm : null, [Validators.pattern('^[1-9][0-9]{3}$')]),
      heightIn: new FormControl(this.Dragged.heightIn ? this.Dragged.heightIn : null),
      widthIn: new FormControl(this.Dragged.widthIn ? this.Dragged.widthIn : null),
      depthIn: new FormControl(this.Dragged.depthIn ? this.Dragged.depthIn : null),
      sizeText: new FormControl(this.Dragged.sizeText ? this.Dragged.sizeText : null),
      yearStart: new FormControl(this.Dragged.yearStart ? this.Dragged.yearStart : null, [Validators.pattern('^[1-9][0-9]{3}$')]),
      yearEnd: new FormControl(this.Dragged.yearEnd ? this.Dragged.yearEnd : null, [Validators.pattern('^[1-9][0-9]{3}$')]),
      castYearStart: new FormControl(this.Dragged.castYearStart ? this.Dragged.castYearStart : null, [Validators.pattern('^[1-9][0-9]{3}$')]),
      castYearEnd: new FormControl(this.Dragged.castYearEnd ? this.Dragged.castYearEnd : null, [Validators.pattern('^[1-9][0-9]{3}$')]),
      yearText: new FormControl(this.Dragged.yearText ? this.Dragged.yearText : null),
      yearCirca: new FormControl(this.Dragged.yearCirca ? this.Dragged.yearCirca : null),
      castYearCirca: new FormControl(this.Dragged.castYearCirca ? this.Dragged.castYearCirca : null),
      posthumous: new FormControl(this.Dragged.posthumous ? this.Dragged.posthumous : null),
      editionNumber: new FormControl(this.Dragged.editionNumber ? this.Dragged.editionNumber : null),
      editionName: new FormControl(this.Dragged.editionName ? this.Dragged.editionName : null),
      editionSizeTotal: new FormControl(this.Dragged.editionOverride ? this.Dragged.editionOverride : (this.Dragged.editionSizeTotal ? this.Dragged.editionSizeTotal : null)),
      weight: new FormControl(this.Dragged.weight, [Validators.pattern('^[1-9][0-9]{3}$')]),
      foundry: new FormControl(this.Dragged.foundry ? this.Dragged.foundry : null),
      certificate: new FormControl(this.Dragged.certificate ? this.Dragged.certificate : null),
      signed: new FormControl(this.Dragged.signed ? this.Dragged.signed : null),
      stamped: new FormControl(this.Dragged.stamped ? this.Dragged.stamped : null),
      authentication: new FormControl(this.Dragged.authentication ? this.Dragged.authentication : null),
      archiveNumber: new FormControl(),
      sortCode: new FormControl(this.Dragged.sortCode ? this.Dragged.sortCode : null),
      flags: this.builder.group({
        flagAuthenticity: new FormControl((this.Dragged.flags && this.Dragged.flags.flagAuthenticity) ? this.Dragged.flags.flagAuthenticity : false),
        flagRestitution: new FormControl((this.Dragged.flags && this.Dragged.flags.flagRestitution) ? this.Dragged.flags.flagRestitution : false),
        flagSfs: new FormControl((this.Dragged.flags && this.Dragged.flags.flagSfs) ? this.Dragged.flags.flagSfs : false),
        flagCondition: new FormControl((this.Dragged.flags && this.Dragged.flags.flagCondition) ? this.Dragged.flags.flagCondition : false),
        flagMedium: new FormControl((this.Dragged.flags && this.Dragged.flags.flagMedium) ? this.Dragged.flags.flagMedium : false),
        flagsCountry: new FormControl((this.Dragged.flags && this.Dragged.flags.flagsCountry) ? this.Dragged.flags.flagsCountry : null)
      }),
      researcherName: new FormControl(this.Dragged.researcherName ? this.Dragged.researcherName : null),
      researchCompleteCheck: new FormControl(this.Dragged.researchCompleteCheck ? this.Dragged.researchCompleteCheck : false),
      researcherNotesDate: new FormControl(this.Dragged.researcherNotesDate ? this.Drop.researcherNotesDate : null),
      researcherNotesText: new FormControl(this.Dragged.researcherNotesText ? this.Dragged.researcherNotesText : null),
      volume: new FormControl(),
      number: new FormControl(),
      tags: this.builder.group({
        imageSubjects: new FormControl((this.Dragged.tags && this.Dragged.tags.imageSubjects) ? this.Dragged.tags.imageSubjects : null),
        imageFigure: new FormControl((this.Dragged.tags && this.Dragged.tags.imageFigure) ? this.Dragged.tags.imageFigure : null),
        imageGenders: new FormControl((this.Dragged.tags && this.Dragged.tags.imageGenders) ? this.Dragged.tags.imageGenders : null),
        imagePositions: new FormControl((this.Dragged.tags && this.Dragged.tags.imagePositions) ? this.Dragged.tags.imagePositions : null),
        imageAnimals: new FormControl((this.Dragged.tags && this.Dragged.tags.imageAnimals) ? this.Dragged.tags.imageAnimals : null),
        imageMainColours: new FormControl((this.Dragged.tags && this.Dragged.tags.imageMainColours) ? this.Dragged.tags.imageMainColours : null),
        imageText: new FormControl((this.Dragged.tags && this.Dragged.tags.imageText) ? this.Dragged.tags.imageText : null),
        expertiseLocation: new FormControl((this.Dragged.tags && this.Dragged.tags.expertiseLocation) ? this.Dragged.tags.expertiseLocation : null),
        expertiseSitters: new FormControl((this.Dragged.tags && this.Dragged.tags.expertiseSitters) ? this.Dragged.tags.expertiseSitters : null),
        autoOrientation: new FormControl((this.Dragged.tags && this.Dragged.tags.autoOrientation) ? this.Dragged.tags.autoOrientation : null),
        autoImage: new FormControl((this.Dragged.tags && this.Dragged.tags.autoImage) ? this.Dragged.tags.autoImage : null),
        autoScale: new FormControl((this.Dragged.tags && this.Dragged.tags.autoScale) ? this.Dragged.tags.autoScale : null),
        otherTags: new FormControl((this.Dragged.tags && this.Dragged.tags.otherTags) ? this.Dragged.tags.otherTags : null),
        autoAreaCm: new FormControl((this.Dragged.tags && this.Dragged.tags.autoAreaCm) ? this.Dragged.tags.autoAreaCm : null),
        autoAreaIn: new FormControl((this.Dragged.tags && this.Dragged.tags.autoAreaIn) ? this.Dragged.tags.autoAreaIn : null)
      })
    });
    if (this.Dragged.researcherNotesDate) {
      this.DragDetailsForm.controls['researcherNotesDate'].setValue(this.getReasearcherDate(this.Dragged.researcherNotesDate, "mm-dd-yyyy"))
      this.dragResearcherDate = this.getReasearcherDate(this.Dragged.researcherNotesDate, "mm-dd-yyyy");
    }

    if (this.Dragged.researcherName) {
      this.DragDetailsForm.controls['researcherName'].setValue(this.getUpdatedResaearcherName(this.Dragged.researcherName));
      this.dragDisplayName = this.getUpdatedResaearcherName(this.Dragged.researcherName);

    }

    if (this.Dragged.researcherNotesText && this.Dragged.researcherNotesText != null && this.Dragged.researcherNotesText.length <= 0) {
      this.DragDetailsForm.controls['researcherNotesText'].setValue(null);
    }

    this.archiveNoTags = [];
    for (let i in this.Drop.archiveNumbers) {
      this.archiveNoTags.push(this.Drop.archiveNumbers[i].archiveNumber);
    }
    this.checkedArtist = [];
    this.artistName.splice(0, this.artistName.length);
    this.Drop.artists.forEach(data => {
      var bd = ((data.birthYear && data.deathYear) ? "(" + data.birthYear + "-" + data.deathYear + ")" : (data.birthYear ? "(b." + data.birthYear + ")" : ''));
      this.artistName.push({
        firstName: (data.firstName ? data.firstName + ' ' : '') + data.lastName,
        display: data.display ? data.display : ((data.firstName ? data.firstName + ' ' : '') + data.lastName + bd),
        birthYear: data.birthYear,
        deathYear: data.deathYear,
        id: data.id
      });
    });
    this.checkedArtist = this.checkedArtist.concat(this.artistName);

    this.editionSizeObject = {
      editionSizeType: (this.Drop && this.Drop.editionSizeTypeName) ? this.Drop.editionSizeTypeName : null,
      editionSizeSeries: (this.Drop && this.Drop.seriesTotal) ? (this.Drop.seriesTotal) : null,
      editionSizeTrialProof: (this.Drop && this.Drop.trialProofTotal) ? (this.Drop.trialProofTotal) : null,
      editionSizeArtistProof: (this.Drop && this.Drop.artistProofTotal) ? (this.Drop.artistProofTotal) : null,
      editionSizeBonATirerProof: (this.Drop && this.Drop.bonATirerTotal) ? (this.Drop.bonATirerTotal) : null,
      editionSizePrintersProof: (this.Drop && this.Drop.printersProofTotal) ? (this.Drop.printersProofTotal) : null,
      editionSizeHorsCommerceProof: (this.Drop && this.Drop.horsCommerceProofTotal) ? (this.Drop.horsCommerceProofTotal) : null,
      editionSizeDedicatedProof: (this.Drop && this.Drop.dedicatedProofTotal) ? (this.Drop.dedicatedProofTotal) : null,
      editionSizeMuseumProof: (this.Drop && this.Drop.museumProofTotal) ? (this.Drop.museumProofTotal) : null,
      editionSizeEditionOverride: (this.Drop && this.Drop.editionOverride) ? this.Drop.editionOverride : null,
      editionSizeTotal: (this.Drop && this.Drop.editionOverride) ? this.Drop.editionOverride : (this.Drop && this.Drop.editionSizeTotal) ? this.Drop.editionSizeTotal : null,
      editionSizeDisplay: null,
      editionSizeNotes: (this.Drop && this.Drop.editionSizeNotes) ? this.Drop.editionSizeNotes : null
    };
    this.editionSizeObject.editionSizeDisplay = this.editionSizeObject.editionSizeEditionOverride ? this.editionSizeObject.editionSizeEditionOverride : (this.editionSizeObject.editionSizeSeries ? (this.editionSizeObject.editionSizeSeries == -1 ? 1 : this.editionSizeObject.editionSizeSeries) + '+' : '') + (this.editionSizeObject.editionSizeArtistProof ? (this.editionSizeObject.editionSizeArtistProof == -1 ? '' : this.editionSizeObject.editionSizeArtistProof) + 'AP+' : '') + (this.editionSizeObject.editionSizeTrialProof ? (this.editionSizeObject.editionSizeTrialProof == -1 ? '' : this.editionSizeObject.editionSizeTrialProof) + 'TP+' : '') + (this.editionSizeObject.editionSizeBonATirerProof ? (this.editionSizeObject.editionSizeBonATirerProof == -1 ? '' : this.editionSizeObject.editionSizeBonATirerProof) + 'BAT+' : '') + (this.editionSizeObject.editionSizePrintersProof ? (this.editionSizeObject.editionSizePrintersProof == -1 ? '' : this.editionSizeObject.editionSizePrintersProof) + 'PP+' : '') + (this.editionSizeObject.editionSizeHorsCommerceProof ? (this.editionSizeObject.editionSizeHorsCommerceProof == -1 ? '' : this.editionSizeObject.editionSizeHorsCommerceProof) + 'HC+' : '') + (this.editionSizeObject.editionSizeDedicatedProof ? (this.editionSizeObject.editionSizeDedicatedProof == -1 ? '' : this.editionSizeObject.editionSizeDedicatedProof) + 'DP+' : '') + (this.editionSizeObject.editionSizeMuseumProof ? (this.editionSizeObject.editionSizeMuseumProof == -1 ? '' : this.editionSizeObject.editionSizeMuseumProof) + 'MP' : '');
    this.DropDetailsForm.controls['editionSizeTotal'].setValue(this.editionSizeObject.editionSizeDisplay);

    this.propertyCatalogRaisonnee = [];
    this.Drop.artists.forEach(data => {
      data.artistCatalogueRaisonees.forEach(catalog => {
        this.propertyCatalogRaisonnee.push({ artistCatalogueRaisoneeId: catalog.id, artistCatalogueRaisoneeAuthor: catalog.author, artistCatalogueRaisoneeType: catalog.type, sortId: catalog.sortId, number: '', volume: '', });
      })
    });
    this.Drop.propertyCatalogueRaisonees.forEach(data => {
      if (data.artistCatalogueRaisoneeType == "" && data.artistCatalogueRaisoneeAuthor == "" && data.volume == "" && data.number == "") {
        return;
      } else {
        for (var i = 0; i < this.propertyCatalogRaisonnee.length; i++) {
          if (this.propertyCatalogRaisonnee[i].artistCatalogueRaisoneeId == data.artistCatalogueRaisoneeId) {
            if (!(data.volume == "" && data.number == "")) {
              this.propertyCatalogRaisonnee[i].number = data.number;
              this.propertyCatalogRaisonnee[i].volume = data.volume;
            }
            break;
          }
        }
      }
    });

    if (this.Drop.heightCm) {
      this.CmToInViceVersaChange('heightIn', 'CmToIn', this.Drop.heightCm);
    }
    if (this.Drop.widthCm) {
      this.CmToInViceVersaChange('widthIn', 'CmToIn', this.Drop.widthCm);
    }
    if (this.Drop.depthCm) {
      this.CmToInViceVersaChange('depthIn', 'CmToIn', this.Drop.depthCm);
    }
    this.archiveNoTagsDrag = [];
    for (let i in this.Dragged.archiveNumbers) {
      this.archiveNoTagsDrag.push(this.Dragged.archiveNumbers[i].archiveNumber);
    }
    this.checkedArtistDrag = [];
    this.artistNameDrag.splice(0, this.artistNameDrag.length);
    this.Dragged.artists.forEach(data => {
      var bd = ((data.birthYear && data.deathYear) ? "(" + data.birthYear + "-" + data.deathYear + ")" : (data.birthYear ? "(b." + data.birthYear + ")" : ''));
      this.artistNameDrag.push({
        firstName: (data.firstName ? data.firstName + ' ' : '') + data.lastName,
        display: data.display ? data.display : ((data.firstName ? data.firstName + ' ' : '') + data.lastName + bd),
        birthYear: data.birthYear,
        deathYear: data.deathYear,
        id: data.id
      });
    });
    this.checkedArtistDrag = this.checkedArtistDrag.concat(this.artistNameDrag);

    this.editionSizeObjectDrag = {
      editionSizeType: (this.Dragged && this.Dragged.editionSizeTypeName) ? this.Dragged.editionSizeTypeName : null,
      editionSizeSeries: (this.Dragged && this.Dragged.seriesTotal) ? (this.Dragged.seriesTotal) : null,
      editionSizeArtistProof: (this.Dragged && this.Dragged.artistProofTotal) ? (this.Dragged.artistProofTotal) : null,
      editionSizeTrialProof: (this.Dragged && this.Dragged.trialProofTotal) ? (this.Dragged.trialProofTotal) : null,
      editionSizeBonATirerProof: (this.Dragged && this.Dragged.bonATirerTotal) ? (this.Dragged.bonATirerTotal) : null,
      editionSizePrintersProof: (this.Dragged && this.Dragged.printersProofTotal) ? (this.Dragged.printersProofTotal) : null,
      editionSizeHorsCommerceProof: (this.Dragged && this.Dragged.horsCommerceProofTotal) ? (this.Dragged.horsCommerceProofTotal) : null,
      editionSizeDedicatedProof: (this.Dragged && this.Dragged.dedicatedProofTotal) ? (this.Dragged.dedicatedProofTotal) : null,
      editionSizeMuseumProof: (this.Dragged && this.Dragged.museumProofTotal) ? (this.Dragged.museumProofTotal) : null,
      editionSizeEditionOverride: (this.Dragged && this.Dragged.editionOverride) ? this.Dragged.editionOverride : null,
      editionSizeTotal: (this.Dragged && this.Dragged.editionOverride) ? this.Dragged.editionOverride : (this.Dragged && this.Dragged.editionSizeTotal) ? this.Dragged.editionSizeTotal : null,
      editionSizeDisplay: null,
      editionSizeNotes: (this.Dragged && this.Dragged.editionSizeNotes) ? this.Dragged.editionSizeNotes : null
    };
    this.editionSizeObjectDrag.editionSizeDisplay = this.editionSizeObjectDrag.editionSizeEditionOverride ? this.editionSizeObjectDrag.editionSizeEditionOverride : (this.editionSizeObjectDrag.editionSizeSeries ? (this.editionSizeObjectDrag.editionSizeSeries == -1 ? 1 : this.editionSizeObjectDrag.editionSizeSeries) + '+' : '') + (this.editionSizeObjectDrag.editionSizeArtistProof ? (this.editionSizeObjectDrag.editionSizeArtistProof == -1 ? '' : this.editionSizeObjectDrag.editionSizeArtistProof) + 'AP+' : '') + (this.editionSizeObjectDrag.editionSizeTrialProof ? (this.editionSizeObjectDrag.editionSizeTrialProof == -1 ? '' : this.editionSizeObjectDrag.editionSizeTrialProof) + 'TP+' : '') + (this.editionSizeObjectDrag.editionSizeBonATirerProof ? (this.editionSizeObjectDrag.editionSizeBonATirerProof == -1 ? '' : this.editionSizeObjectDrag.editionSizeBonATirerProof) + 'BAT+' : '') + (this.editionSizeObjectDrag.editionSizePrintersProof ? (this.editionSizeObjectDrag.editionSizePrintersProof == -1 ? '' : this.editionSizeObjectDrag.editionSizePrintersProof) + 'PP+' : '') + (this.editionSizeObjectDrag.editionSizeHorsCommerceProof ? (this.editionSizeObjectDrag.editionSizeHorsCommerceProof == -1 ? '' : this.editionSizeObjectDrag.editionSizeHorsCommerceProof) + 'HC+' : '') + (this.editionSizeObjectDrag.editionSizeDedicatedProof ? (this.editionSizeObjectDrag.editionSizeDedicatedProof == -1 ? '' : this.editionSizeObjectDrag.editionSizeDedicatedProof) + 'DP+' : '') + (this.editionSizeObjectDrag.editionSizeMuseumProof ? (this.editionSizeObjectDrag.editionSizeMuseumProof == -1 ? '' : this.editionSizeObjectDrag.editionSizeMuseumProof) + 'MP' : '');
    this.DragDetailsForm.controls['editionSizeTotal'].setValue(this.editionSizeObjectDrag.editionSizeDisplay);

    this.propertyCatalogRaisonneeDrag = [];
    this.Dragged.artists.forEach(data => {
      data.artistCatalogueRaisonees.forEach(catalog => {
        this.propertyCatalogRaisonneeDrag.push({ artistCatalogueRaisoneeId: catalog.id, artistCatalogueRaisoneeAuthor: catalog.author, artistCatalogueRaisoneeType: catalog.type, sortId: catalog.sortId, number: '', volume: '', });
      })
    });
    this.Dragged.propertyCatalogueRaisonees.forEach(data => {
      if (data.artistCatalogueRaisoneeType == "" && data.artistCatalogueRaisoneeAuthor == "" && data.volume == "" && data.number == "") {
        return;
      } else {
        for (var i = 0; i < this.propertyCatalogRaisonneeDrag.length; i++) {
          if (this.propertyCatalogRaisonneeDrag[i].artistCatalogueRaisoneeId == data.artistCatalogueRaisoneeId) {
            if (!(data.volume == "" && data.number == "")) {
              this.propertyCatalogRaisonneeDrag[i].number = data.number;
              this.propertyCatalogRaisonneeDrag[i].volume = data.volume;
            }
            break;
          }
        }
      }
    });

    var propertyDrop = this.propertyCatalogRaisonnee;
    var propertyDragegd = this.propertyCatalogRaisonneeDrag;

    this.propertyCatalogRaisonnee = [];
    this.propertyCatalogRaisonneeDrag = [];
    propertyDrop.forEach(data => {
      var index = propertyDragegd.findIndex(x => x.artistCatalogueRaisoneeId === data.artistCatalogueRaisoneeId);
      var draggedData = ((index > -1) ? ((propertyDragegd[index].volume === data.volume) ? ((propertyDragegd[index].number === data.number) ? false : true) : true) : true);
      data.color = draggedData;
      this.propertyCatalogRaisonnee.push(data);
    });
    propertyDragegd.forEach(data => {
      var index = propertyDrop.findIndex(x => x.artistCatalogueRaisoneeId === data.artistCatalogueRaisoneeId);
      var DropData = ((index > -1) ? ((propertyDrop[index].volume === data.volume) ? ((propertyDrop[index].number === data.number) ? false : true) : true) : true);
      data.color = DropData;
      this.propertyCatalogRaisonneeDrag.push(data);
    });

    this.propertyCatalogRaisonnee = this.sort(this.propertyCatalogRaisonnee);
    this.propertyCatalogRaisonneeDrag = this.sort(this.propertyCatalogRaisonneeDrag);

    if (this.Dragged.heightCm) {
      this.CmToInViceVersaChangedDrag('heightIn', 'CmToIn', this.Dragged.heightCm);
    }
    if (this.Dragged.widthCm) {
      this.CmToInViceVersaChangedDrag('widthIn', 'CmToIn', this.Dragged.widthCm);
    }
    if (this.Dragged.depthCm) {
      this.CmToInViceVersaChangedDrag('depthIn', 'CmToIn', this.Dragged.depthCm);
    }
  }

  public archiveNoTags = [];
  public archiveNoTagsDrag = [];
  public keycloak = this.keycloak;
  public DropDetailsForm: FormGroup;
  public DragDetailsForm: FormGroup;
  public flags: FormGroup;
  public Dragflags: FormGroup;
  public errorMsg;
  public categories = [];
  public subcategories = [];
  public presizeNames = [];
  public editionSizeTypes: any;
  public editionTypes = [];
  // archive number
  public archiveNumbers = [];
  public archiveNumber = [];
  //Catalogue Raisonnes array
  public propertyCatalogRaisonnee = [];
  public propertyCatalogRaisonneeDrag = [];
  //Error message flag
  public requiredFlag = true;
  // Flags Fields
  public country: string;
  public countries: Array<object> = [];
  public errorMessage: string;
  protected countryService: CompleterData;
  protected categoryService: CompleterData;
  protected subCategoryService: CompleterData;
  protected presizeTypesService: CompleterData;
  protected editionTypeService: CompleterData;

  // Researcher Notes
  public researchNotesDate: Date;

  // Pagination
  public propertyNumber: number = 0;

  //constructor method
  constructor(private builder: FormBuilder,
    private _sanitizer: DomSanitizer,
    private _objectService: ObjectService,
    private _artistService: ArtistService,
    private router: Router,
    private completerService: CompleterService,
    private _appService: AppService,
    dialogService: DialogService,
    private tosatservice: TosatService,
    configuration: ConfigurationLoaderService,
  ) {
    super(dialogService);


    this.URL = configuration.getSettings().apiUrl
    this._objectService
      .getCategories()
      .subscribe(categories => {
        categories.forEach((category, index) => {
          if (this.categories.findIndex(item => item.id == category.id) < 0)
            this.categories.push({ 'name': category.name, 'id': category.id, "description": category.description });
        });
      }, resFileError => this.errorMsg = resFileError);

    this._objectService
      .getEditionSizeTypes()
      .subscribe(Response => {
        this.editionSizeTypes = Response;
      }, resFileError => this.errorMsg = resFileError);

    this._appService.getCountries()
      .subscribe(countries => {
        countries.forEach((country, index) => {
          if (country && country.countryName) {
            this.countries.push({ 'name': country.countryName });
          }
        });
      },
        error => { this.errorMessage = error; }
      );
  }

  //selectArtist autocomplete function
  nameListFormatter = (data: any) => {
    this.artistMap = "";
    if (data.birthYear && data.deathYear) {
      this.artistMap = `<span><i class='fa fa-birthday-cake' aria-hidden='true'></i> ` + data.birthYear + ` &nbsp;&nbsp;&nbsp;<i class='fa fa-bed' aria-hidden='true'> </i> ` + data.deathYear + ` </span>`
    } else if (data.birthYear) {
      this.artistMap = `<span><i class='fa fa-birthday-cake' aria-hidden='true'></i> ` + data.birthYear;
    }
    let html = `<span class="auto-title">` + data.firstName + `</span><br/>` + this.artistMap;
    return this._sanitizer.bypassSecurityTrustHtml(html);
  }

  // Get 'Archive Numbers'
  public getArchiveNumbers() {
    let tempArchiveNo = this.DropDetailsForm.controls['archiveNumbers'].value;
    this.archiveNumbers = [];
    this.dropFlag = true;
    for (let archiveNo in tempArchiveNo) {
      if (tempArchiveNo[archiveNo].hasOwnProperty('value')) {
        this.archiveNumbers.push({ "archiveNumber": tempArchiveNo[archiveNo].value });
      }
      else {
        this.archiveNumbers.push({ "archiveNumber": tempArchiveNo[archiveNo] });
      }
    }
    return this.archiveNumbers;
  }

  // Get 'Archive Numbers'
  public getArchiveNumbersDrag() {
    let tempArchiveNo = this.DragDetailsForm.controls['archiveNumber'].value;
    this.archiveNumber = [];
    this.dragFlag = true;
    for (let archiveNo in tempArchiveNo) {
      if (tempArchiveNo[archiveNo].hasOwnProperty('value')) {
        this.archiveNumber.push({ "archiveNumber": tempArchiveNo[archiveNo].value });
      }
      else {
        this.archiveNumber.push({ "archiveNumber": tempArchiveNo[archiveNo] });
      }
    }
    return this.archiveNumber;
  }

  public artistName = [];
  public artistNameDrag = [];
  public checkedArtist = [];
  public checkedArtistDrag = [];
  public tempArtist = [];
  public artistMap;
  public artistQuery;

  //Artist change
  artistChange(flag: string, value: any) {
    if (flag === 'add' && typeof value === 'object') {
      this.dropFlag = true;
      this.tempArtist = this.checkedArtist.filter(function (art) {
        return art.id == value.id;
      });
      if (this.tempArtist.length > 0) {
        this.DropDetailsForm.controls['artistData'].setValue("");
      } else {
        this.checkedArtist.push(value);
        this.DropDetailsForm.controls['artistData'].setValue("");
        this.getArtistCatalogRaisonnees(value.id);
      }
    } else if (flag === 'remove' && typeof value === 'object') {
      this.dropFlag = true;
      this.tempArtist = this.checkedArtist.filter(function (art) {
        return art.id != value.id;
      });
      this.checkedArtist = this.tempArtist;
      this.removeArtistCatalogRaisonnees(value.id);
    } else {
      // this.objectDetailsForm.controls['artistData'].setValue(value);
      if (value) {
        value = value.split(' ');
        if (value[0] && value[1]) {
          this.artistQuery = `firstName=` + value[0] + `&lastName=` + value[1] + `&lod=MAX`;
          this.refreshData(this.artistQuery);
        } else if (value[0]) {
          this.artistQuery = `name=` + value[0] + `&lod=MAX`;
          this.refreshData(this.artistQuery);
        }
      }
    }
  }

  //Artist change
  artistChangeDrag(flag: string, value: any) {
    if (flag === 'add' && typeof value === 'object') {
      this.dragFlag = true;
      this.tempArtist = this.checkedArtistDrag.filter(function (art) {
        return art.id == value.id;
      });
      if (this.tempArtist.length > 0) {
        this.DragDetailsForm.controls['artistDataDrag'].setValue("");
      } else {
        this.checkedArtistDrag.push(value);
        this.DragDetailsForm.controls['artistDataDrag'].setValue("");
        this.getArtistCatalogRaisonneesDrag(value.id);
      }
    } else if (flag === 'remove' && typeof value === 'object') {
      this.dragFlag = true;
      this.tempArtist = this.checkedArtistDrag.filter(function (art) {
        return art.id != value.id;
      });
      this.checkedArtistDrag = this.tempArtist;
      this.removeArtistCatalogRaisonneesDrag(value.id);
    } else {
      // this.objectDetailsForm.controls['artistData'].setValue(value);
      if (value) {
        value = value.split(' ');
        if (value[0] && value[1]) {
          this.artistQuery = `firstName=` + value[0] + `&lastName=` + value[1] + `&lod=MAX`;
          this.refreshDataDrag(this.artistQuery);
        } else if (value[0]) {
          this.artistQuery = `name=` + value[0] + `&lod=MAX`;
          this.refreshDataDrag(this.artistQuery);
        }
      }
    }
  }
  //Get artist with Value
  refreshData(event) {
    this._artistService
      .getArtist(event)
      .subscribe(Response => {
        this.artistName.splice(0, this.artistName.length);
        Response.content.forEach(data => {
          var bd = ((data.birthYear && data.deathYear) ? "(" + data.birthYear + "-" + data.deathYear + ")" : (data.birthYear ? "(b." + data.birthYear + ")" : ''));
          this.artistName.push({
            firstName: (data.firstName ? data.firstName + ' ' : '') + data.lastName,
            display: data.display ? data.display : ((data.firstName ? data.firstName + ' ' : '') + data.lastName + bd),
            birthYear: data.birthYear,
            deathYear: data.deathYear,
            id: data.id
          });
        });
      }, resFileError => this.errorMsg = resFileError);
  }
  //Get artist with Value
  refreshDataDrag(event) {
    this._artistService
      .getArtist(event)
      .subscribe(Response => {
        this.artistNameDrag.splice(0, this.artistNameDrag.length);
        Response.content.forEach(data => {
          var bd = ((data.birthYear && data.deathYear) ? "(" + data.birthYear + "-" + data.deathYear + ")" : (data.birthYear ? "(b." + data.birthYear + ")" : ''));
          this.artistNameDrag.push({
            firstName: (data.firstName ? data.firstName + ' ' : '') + data.lastName,
            display: data.display ? data.display : ((data.firstName ? data.firstName + ' ' : '') + data.lastName + bd),
            birthYear: data.birthYear,
            deathYear: data.deathYear,
            id: data.id
          });
        });
      }, resFileError => this.errorMsg = resFileError);
  }
  // Get current date
  public getToday(format) {
    let today = null;
    let now = new Date();
    let day = ("0" + now.getDate()).slice(-2);
    let month = now.getMonth();
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    let currentMonth = months[month];
    if (format.toLowerCase() == "mm-dd-yyyy") {
      today = (month + 1) + "-" + day + "-" + now.getFullYear();
    }
    else {
      today = day + " " + currentMonth + " " + now.getFullYear();
    }
    return today;
  }
  // Convert given date to formats: MM-DD-YYYY (or) YYYY-MM-DD (or) DD MMM YYYY
  public getDate(dt, format) {
    let day, month, year;
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    // If date has '-', 'T' and in the format YYYY-MM-DD (e.g. 2017-05-01T19:32:16.013Z)
    if (dt && (dt.indexOf("T") > -1) && (dt.indexOf("-") > -1)) {
      dt = dt.slice(0, dt.indexOf("T"));
      day = dt.split("-")[1];
      month = dt.split("-")[0];
      year = dt.split("-")[2];
      if (month.charAt(0) == "0") {
        month = month.slice(1, 2);
      }
      month = months[month - 1];
    }
    // If date has '-' and in the format MM-DD-YYYY (e.g. 05-01-2017)
    else if (dt && dt.indexOf("-") > -1) {
      dt = dt;
      day = dt.split("-")[1];
      month = dt.split("-")[0];
      year = dt.split("-")[2];
      if (month.charAt(0) == "0") {
        month = month.slice(1, 2);
      }
      month = months[month - 1];
    }
    // If date has ' ' (space) and in the format DD MMM YYYY (e.g. 01 May 2017)
    else if (dt && dt.indexOf(" ") > -1) {
      dt = dt;
      day = dt.split(" ")[0];
      month = dt.split(" ")[1];
      year = dt.split(" ")[2];
      if (/^[a-zA-Z()]$/.test(month.charAt(0))) {
        month = months.indexOf(month) + 1;
        month = month.length == 1 ? "0" + month : month;
        day = day.length == 1 ? "0" + day : day;
      }
      else if (!/^[a-zA-Z()]$/.test(month.charAt(0))) {
        if (month.charAt(0) == "0") {
          month = month.slice(1, 2);
        }
        month = months[month];
      }
    }

    if (format) {
      if (format.toLowerCase() == "mm-dd-yyyy") {
        return (month + "-" + day + "-" + year);
      }
      else if (format.toLowerCase() == "dd mmm yyyy") {
        return (day + " " + month + " " + year);
      }
    }
    else {
      return (day + " " + month + " " + year);
    }
  }
  // Set 'Researcher Name' and 'Date'
  public setResearcherInfo(form) {
    if (form == 'drop') {
      if (this.DropDetailsForm.controls['researcherNotesText'].value) {
        let researcherName = window['keycloak'].tokenParsed.name;
        this.DropDetailsForm.controls['researcherName'].setValue(this.getUpdatedResaearcherName(window['keycloak'].tokenParsed.preferred_username));
        this.dropDisplayName = this.getUpdatedResaearcherName(window['keycloak'].tokenParsed.preferred_username);
        this.DropDetailsForm.controls['researcherNotesDate'].setValue(this.getToday("dd mmm yyyy"));
        this.dropResearcherDate = this.getToday("dd mmm yyyy");
      }
      else {
        this.DropDetailsForm.controls['researcherName'].reset();
        this.DropDetailsForm.controls['researcherNotesDate'].reset();
        this.dropDisplayName = null;
        this.dropResearcherDate = null;
      }
    }
    else if (form == 'drag') {
      if (this.DragDetailsForm.controls['researcherNotesText'].value) {
        let researcherName = window['keycloak'].tokenParsed.name;
        this.DragDetailsForm.controls['researcherName'].setValue(this.getUpdatedResaearcherName(window['keycloak'].tokenParsed.preferred_username));
        this.dragDisplayName = this.getUpdatedResaearcherName(window['keycloak'].tokenParsed.preferred_username);
        this.DragDetailsForm.controls['researcherNotesDate'].setValue(this.getToday("dd mmm yyyy"));
        this.dragResearcherDate = this.getToday("dd mmm yyyy");
      }
      else {
        this.DragDetailsForm.controls['researcherName'].reset();
        this.DragDetailsForm.controls['researcherNotesDate'].reset();
        this.dragDisplayName = null;
        this.dragResearcherDate = null;
      }
    }
  }
  // Conversion Cm to Inches and Inches to Cm viceversa
  CmToInViceVersaChange(flag: string, conversionFlag: string, value: any) {
    if (value) {
      var cal = (conversionFlag === 'CmToIn') ? 0.393701 : 2.54;
      if ((parseFloat(value) * cal) && conversionFlag == 'CmToIn') {
        var decimal = (parseFloat(value) * cal) % 1;
        var offset = null;
        if (decimal < 1 / 8) {
          offset = null;
        }

        if (decimal >= 1 / 8 && decimal < (1 / 8 + 1 / 4) / 2) {
          offset = "1/8";
        }

        if ((decimal >= (1 / 8 + 1 / 4) / 2 && decimal < 1 / 4) || (decimal >= 1 / 4 && decimal < (1 / 4 + 3 / 8) / 2)) {
          offset = "1/4";
        }

        if ((decimal >= (1 / 4 + 3 / 8) / 2 && decimal < 3 / 8) || (decimal >= 3 / 8 && decimal < (3 / 8 + 1 / 2) / 2)) {
          offset = "3/8";
        }

        if ((decimal >= (3 / 8 + 1 / 2) / 2 && decimal < 1 / 2) || (decimal >= 1 / 2 && decimal < (1 / 2 + 5 / 8) / 2)) {
          offset = "1/2";
        }

        if ((decimal >= (1 / 2 + 5 / 8) / 2 && decimal < 5 / 8) || (decimal >= 5 / 8 && decimal < (5 / 8 + 3 / 4) / 2)) {
          offset = "5/8";
        }

        if ((decimal >= (5 / 8 + 3 / 4) / 2 && decimal < 3 / 4) || (decimal >= 3 / 4 && decimal < (3 / 4 + 7 / 8) / 2)) {
          offset = "3/4";
        }

        if ((decimal >= (3 / 4 + 7 / 8) / 2 && decimal < 7 / 8) || (decimal >= 7 / 8 && decimal < (7 / 8 + 1) / 2)) {
          offset = "7/8";
        }

        if (decimal >= (7 / 8 + 1) / 2) {
          offset = '1';
        }

        if (offset == '1') {
          this.DropDetailsForm.controls[flag].setValue(Math.trunc((parseFloat(value) * cal)) + 1);
        } else {
          if (Math.trunc((parseFloat(value) * cal)) == 0) {
            this.DropDetailsForm.controls[flag].setValue(offset);
          } else {
            if (offset == null) {
              this.DropDetailsForm.controls[flag].setValue(Math.trunc((parseFloat(value) * cal)));
            } else {
              this.DropDetailsForm.controls[flag].setValue(Math.trunc((parseFloat(value) * cal)) + ' ' + offset);
            }
          }
        }
      } else {
        this.DropDetailsForm.controls[flag].setValue(((parseFloat(value) * cal).toFixed(2)));
      }
    } else {
      this.DropDetailsForm.controls[flag].setValue(null);
    }
  }
  // Conversion Cm to Inches and Inches to Cm viceversa
  CmToInViceVersaChangedDrag(flag: string, conversionFlag: string, value: any) {

    if (value) {
      var cal = (conversionFlag === 'CmToIn') ? 0.393701 : 2.54;
      if ((parseFloat(value) * cal) && conversionFlag == 'CmToIn') {
        var decimal = (parseFloat(value) * cal) % 1;
        var offset = null;
        if (decimal < 1 / 8) {
          offset = null;
        }

        if (decimal >= 1 / 8 && decimal < (1 / 8 + 1 / 4) / 2) {
          offset = "1/8";
        }

        if ((decimal >= (1 / 8 + 1 / 4) / 2 && decimal < 1 / 4) || (decimal >= 1 / 4 && decimal < (1 / 4 + 3 / 8) / 2)) {
          offset = "1/4";
        }

        if ((decimal >= (1 / 4 + 3 / 8) / 2 && decimal < 3 / 8) || (decimal >= 3 / 8 && decimal < (3 / 8 + 1 / 2) / 2)) {
          offset = "3/8";
        }

        if ((decimal >= (3 / 8 + 1 / 2) / 2 && decimal < 1 / 2) || (decimal >= 1 / 2 && decimal < (1 / 2 + 5 / 8) / 2)) {
          offset = "1/2";
        }

        if ((decimal >= (1 / 2 + 5 / 8) / 2 && decimal < 5 / 8) || (decimal >= 5 / 8 && decimal < (5 / 8 + 3 / 4) / 2)) {
          offset = "5/8";
        }

        if ((decimal >= (5 / 8 + 3 / 4) / 2 && decimal < 3 / 4) || (decimal >= 3 / 4 && decimal < (3 / 4 + 7 / 8) / 2)) {
          offset = "3/4";
        }

        if ((decimal >= (3 / 4 + 7 / 8) / 2 && decimal < 7 / 8) || (decimal >= 7 / 8 && decimal < (7 / 8 + 1) / 2)) {
          offset = "7/8";
        }

        if (decimal >= (7 / 8 + 1) / 2) {
          offset = '1';
        }

        if (offset == '1') {
          this.DragDetailsForm.controls[flag].setValue(Math.trunc((parseFloat(value) * cal)) + 1);
        } else {
          if (Math.trunc((parseFloat(value) * cal)) == 0) {
            this.DragDetailsForm.controls[flag].setValue(offset);
          } else {
            if (offset == null) {
              this.DragDetailsForm.controls[flag].setValue(Math.trunc((parseFloat(value) * cal)));
            } else {
              this.DragDetailsForm.controls[flag].setValue(Math.trunc((parseFloat(value) * cal)) + ' ' + offset);
            }
          }
        }
      } else {
        this.DragDetailsForm.controls[flag].setValue(((parseFloat(value) * cal).toFixed(2)));
      }
    } else {
      this.DragDetailsForm.controls[flag].setValue(null);
    }
  }
  // Get Artist Catalog Raisonnees
  public getArtistCatalogRaisonnees(data) {
    this._artistService.getIndividualArtist(data)
      .subscribe(Response => {
        if (Response.artistCatalogueRaisonees.length > 0) {
          Response.artistCatalogueRaisonees.forEach(data => {
            if (this.Drop.id) {
              var CRval = [{ artistCatalogueRaisoneeType: data.type, artistCatalogueRaisoneeAuthor: data.author, volume: data.volume, number: data.number, artistCatalogueRaisoneeId: data.id }];
              this.propertyCatalogRaisonnee = this.propertyCatalogRaisonnee.concat(CRval);
            } else {
              this.propertyCatalogRaisonnee = this.propertyCatalogRaisonnee.concat(data);
            }
          });
        }
      }, resFileError => this.errorMsg = resFileError);
  }
  // Remove Property Catalog Raisonnees
  public removeArtistCatalogRaisonnees(data) {
    this._artistService.getIndividualArtist(data)
      .subscribe(Response => {
        if (Response.artistCatalogueRaisonees.length > 0) {
          Response.artistCatalogueRaisonees.forEach(data => {
            if (this.Drop.id) {
              this.propertyCatalogRaisonnee = this.propertyCatalogRaisonnee.filter(function (CRdata) {
                return data.id != CRdata.artistCatalogueRaisoneeId;
              });
            } else {
              this.propertyCatalogRaisonnee = this.propertyCatalogRaisonnee.filter(function (CRdata) {
                return data.id != CRdata.id;
              });
            }
          });
        }
      }, resFileError => this.errorMsg = resFileError);
  }
  // Update Property Catalog Raisonnees
  public updatePropertyCatalogRaisonnees(flag: string) {
    var finalCRarr = [];
    this.propertyCatalogRaisonnee.forEach(data => {
      if (data.id && (flag == 'save') && data.volumeCR != null || data.numberCR != null) {
        if (data.volumeCR || data.numberCR) {
          finalCRarr.push({ "artistCatalogueRaisoneeId": data.id, "number": data.numberCR, "volume": data.volumeCR });
        }
      } else if (data.artistCatalogueRaisoneeId && flag == 'update' && (data.volume != null || data.volume != undefined || data.number != null || data.number != undefined)) {
        if (data.number || data.volume) {
          finalCRarr.push({ "artistCatalogueRaisoneeAuthor": data.artistCatalogueRaisoneeAuthor, "artistCatalogueRaisoneeType": data.artistCatalogueRaisoneeType, "artistCatalogueRaisoneeId": data.artistCatalogueRaisoneeId, "number": data.number, "volume": data.volume });
        }
      }
    });
    this.propertyCatalogRaisonnee = finalCRarr;
    return this.propertyCatalogRaisonnee;
  }
  // Get Artist Catalog Raisonnees
  public getArtistCatalogRaisonneesDrag(data) {
    this._artistService.getIndividualArtist(data)
      .subscribe(Response => {
        if (Response.artistCatalogueRaisonees.length > 0) {
          Response.artistCatalogueRaisonees.forEach(data => {
            if (this.Dragged.id) {
              var CRval = [{ artistCatalogueRaisoneeType: data.type, artistCatalogueRaisoneeAuthor: data.author, volume: data.volume, number: data.number, artistCatalogueRaisoneeId: data.id }];
              this.propertyCatalogRaisonneeDrag = this.propertyCatalogRaisonneeDrag.concat(CRval);
            } else {
              this.propertyCatalogRaisonneeDrag = this.propertyCatalogRaisonneeDrag.concat(data);
            }
          });
        }
      }, resFileError => this.errorMsg = resFileError);
  }
  // Remove Property Catalog Raisonnees
  public removeArtistCatalogRaisonneesDrag(data) {
    this._artistService.getIndividualArtist(data)
      .subscribe(Response => {
        if (Response.artistCatalogueRaisonees.length > 0) {
          Response.artistCatalogueRaisonees.forEach(data => {
            if (this.Dragged.id) {
              this.propertyCatalogRaisonneeDrag = this.propertyCatalogRaisonneeDrag.filter(function (CRdata) {
                return data.id != CRdata.artistCatalogueRaisoneeId;
              });
            } else {
              this.propertyCatalogRaisonneeDrag = this.propertyCatalogRaisonneeDrag.filter(function (CRdata) {
                return data.id != CRdata.id;
              });
            }
          });
        }
      }, resFileError => this.errorMsg = resFileError);
  }

  // Update Property Catalog Raisonnees
  public updatePropertyCatalogRaisonneesDrag(flag: string) {
    var finalCRarr = [];
    this.propertyCatalogRaisonneeDrag.forEach(data => {
      if (data.id && (flag == 'save') && data.volumeCR != null || data.numberCR != null) {
        if (data.volumeCR || data.numberCR) {
          finalCRarr.push({ "artistCatalogueRaisoneeId": data.id, "number": data.numberCR, "volume": data.volumeCR });
        }
      } else if (data.artistCatalogueRaisoneeId && flag == 'update' && (data.volume != null || data.volume != undefined || data.number != null || data.number != undefined)) {
        if (data.number || data.volume) {
          finalCRarr.push({ "artistCatalogueRaisoneeId": data.artistCatalogueRaisoneeId, "number": data.number, "volume": data.volume });
        }
      }
    });
    this.propertyCatalogRaisonneeDrag = finalCRarr;
    return this.propertyCatalogRaisonneeDrag;
  }

  onSubmitMerge(value, flag: string, callback) {
    var artistArr = [];
    this.checkedArtist.forEach(data => {
      artistArr.push(data.id);
    });
    delete value.artistData;
    delete value.archiveNumbers;
    delete value.editionSizeTotal;
    delete value.researcherName;
    delete value.researcherNotesDate;
    delete value.depthIn;
    delete value.heightIn;
    delete value.widthIn;
    value.artistIds = artistArr;
    value.archiveNumbers = this.getArchiveNumbers();
    value.editionSizeTypeName = (this.editionSizeObject && this.editionSizeObject.editionSizeType) ? this.editionSizeObject.editionSizeType : null;
    value.editionSizeTotal = (this.editionSizeObject && this.editionSizeObject.editionSizeTotal) ? this.editionSizeObject.editionSizeTotal : null;
    value.seriesTotal = (this.editionSizeObject && this.editionSizeObject.editionSizeSeries) ? this.editionSizeObject.editionSizeSeries : null;
    value.trialProofTotal = (this.editionSizeObject && this.editionSizeObject.editionSizeTrialProof) ? this.editionSizeObject.editionSizeTrialProof : null;
    value.artistProofTotal = (this.editionSizeObject && this.editionSizeObject.editionSizeArtistProof) ? this.editionSizeObject.editionSizeArtistProof : null;
    value.bonATirerTotal = (this.editionSizeObject && this.editionSizeObject.editionSizeBonATirerProof) ? this.editionSizeObject.editionSizeBonATirerProof : null;
    value.printersProofTotal = (this.editionSizeObject && this.editionSizeObject.editionSizePrintersProof) ? this.editionSizeObject.editionSizePrintersProof : null;
    value.horsCommerceProofTotal = (this.editionSizeObject && this.editionSizeObject.editionSizeHorsCommerceProof) ? this.editionSizeObject.editionSizeHorsCommerceProof : null;
    value.dedicatedProofTotal = (this.editionSizeObject && this.editionSizeObject.editionSizeDedicatedProof) ? this.editionSizeObject.editionSizeDedicatedProof : null;
    value.museumProofTotal = (this.editionSizeObject && this.editionSizeObject.editionSizeMuseumProof) ? this.editionSizeObject.editionSizeMuseumProof : null;
    value.editionOverride = (this.editionSizeObject && this.editionSizeObject.editionSizeEditionOverride) ? this.editionSizeObject.editionSizeEditionOverride : null;
    value.editionSizeNotes = (this.editionSizeObject && this.editionSizeObject.editionSizeNotes) ? this.editionSizeObject.editionSizeNotes : null;
    if (this.DropDetailsForm.controls['researcherNotesDate'].value) {
      value.researcherName = window['keycloak'].tokenParsed.preferred_username;
      value.researcherNotesDate = this.getDate(this.DropDetailsForm.controls['researcherNotesDate'].value, "mm-dd-yyyy");
    }
    value.propertyCatalogueRaisonees = this.updatePropertyCatalogRaisonnees('update');
    var ObjectId = (this.dropImageFlag) ? (this.Drop.id + '?dropImage=true') : this.Drop.id + '?';
    if (this.dropImageFlag) {
      value.propertyImage = this.dropImagePathUrl;
      delete value.imagePath;
    } else {
      value.imagePath = this.Drop.imagePath;
    }

    if (value.category == "") {
      value.category = null;
    }
    if (value.subCategory == "") {
      value.subCategory = null;
    }
    if (value.presizeName == "") {
      value.presizeName = null;
    }
    if (value.editionName == "") {
      value.editionName = null;
    }

    if (value.researcherName) {
      value.researcherName = window['keycloak'].tokenParsed.preferred_username;
    }

    if ((this.DropDetailsForm.valid || this.requiredFlag) && this.checkedArtist.length) {
      var regex = '^0*([1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9])$';
      if (value && (value && value.yearStart && !value.yearStart.toString().match(regex) ? false : true)
        && (value && value.yearEnd && !value.yearEnd.toString().match(regex) ? false : true)
        && (value && value.castYearStart && !value.castYearStart.toString().match(regex) ? false : true)
        && (value && value.castYearEnd && !value.castYearEnd.toString().match(regex) ? false : true)) {
        if (flag == "dropmergeclick") {
          const temp = Object.assign({}, this.Drop);
          this.Drop = null
          document.getElementsByTagName('html')[0].classList.add('loading');
          this._objectService
            .mergeObject(ObjectId, this.Dragged.id, value)
            .subscribe(Response => {
              this.thumbnail_deleteid = this.Dragged.id;
              document.getElementsByTagName('html')[0].classList.remove('loading');
              this.tosatservice.addToast('Merge Object successfully updated.', 'top-right');
              this.Drop = Object.assign({}, Response);
              this.ngOnInit();
              callback(true);
            }, resFileError => {
              this.Drop = temp;
              document.getElementsByTagName('html')[0].classList.remove('loading');
              this.tosatservice.addToastError('There is an error while saving your record', 'top-right');
              this.errorMsg = resFileError;
              callback(false);
            });
        } else {

          if (value.yearStart && !value.yearEnd) {
            this.tosatservice.addToastError('Please enter Year2', 'top-right');
          } else {
            if (value.castYearStart && !value.castYearEnd) {
              this.tosatservice.addToastError('Please enter CastYear2', 'top-right');
            } else {
              if (value.yearStart && value.yearEnd && value.yearEnd < value.yearStart) {
                this.tosatservice.addToastError('Year2 must be greater than or equal to Year1', 'top-right');
              } else {
                if (value.castYearStart && value.castYearEnd && value.castYearEnd < value.castYearStart) {
                  this.tosatservice.addToastError('CastYear2 must be greater than or equal to CastYear1', 'top-right');
                } else {
                  if (!value.yearStart && value.yearEnd) {
                    this.tosatservice.addToastError('Please enter Year1', 'top-right');
                  } else {
                    if (!value.castYearStart && value.castYearEnd) {
                      this.tosatservice.addToastError('Please enter CastYear1', 'top-right');
                    } else {
                      document.getElementsByTagName('html')[0].classList.add('loading');
                      this._objectService
                        .updateProperty(ObjectId, value)
                        .subscribe(Response => {
                          var tempEditionSizeTotal = Response.editionOverride ? Response.editionOverride : (Response.seriesTotal ? (Response.seriesTotal == -1 ? 1 : Response.seriesTotal) + '+' : '') + (Response.artistProofTotal ? (Response.artistProofTotal == -1 ? '' : Response.artistProofTotal) + 'AP+' : '') + (Response.trialProofTotal ? (Response.trialProofTotal == -1 ? '' : Response.trialProofTotal) + 'TP+' : '') + (Response.bonATirerTotal ? (Response.bonATirerTotal == -1 ? '' : Response.bonATirerTotal) + 'BAT+' : '') + (Response.printersProofTotal ? (Response.printersProofTotal == -1 ? '' : Response.printersProofTotal) + 'PP+' : '') + (Response.horsCommerceProofTotal ? (Response.horsCommerceProofTotal == -1 ? '' : Response.horsCommerceProofTotal) + 'HC+' : '') + (Response.dedicatedProofTotal ? (Response.dedicatedProofTotal == -1 ? '' : Response.dedicatedProofTotal) + 'DP+' : '') + (Response.museumProofTotal ? (Response.museumProofTotal == -1 ? '' : Response.museumProofTotal) + 'MP' : '');
                          this.DropDetailsForm.controls['editionSizeTotal'].setValue(tempEditionSizeTotal)
                          if (Response.researcherNotesDate) {
                            Response.researcherNotesDate = this.getReasearcherDate(Response.researcherNotesDate, "mm-dd-yyyy");
                            this.dropResearcherDate = Response.researcherNotesDate;
                            this.DropDetailsForm.controls['researcherNotesDate'].setValue(Response.researcherNotesDate);
                          }
                          if (Response.researcherNotesText != null && Response.researcherNotesText.length <= 0) {
                            this.DragDetailsForm.controls['researcherNotesText'].setValue(null);
                          }
                          if (Response.researcherName) {
                            this.DropDetailsForm.controls['researcherName'].setValue(this.getUpdatedResaearcherName(Response.researcherName));
                            this.dropDisplayName = this.getUpdatedResaearcherName(Response.researcherName);
                          }
                          if (flag == "dropmergeclick") {
                            this.tosatservice.addToast('Merge Object successfully updated.', 'top-right');
                          } else {
                            this.tosatservice.addToast('Object successfully updated', 'top-right');
                          }
                          var d = new Date();
                          var n = d.getTime();
                          if (Response.imagePath) {
                            this.DropImage = this.URL + '/properties/' + this.Drop.id + '/images/default?' + n;
                            if (this.doesFileExist(this.DropImage)) {
                              this.imageDragImag1 = true;
                            } else {
                              this.imageDragImag1 = false;
                            }
                          }

                          this.propertyCatalogRaisonnee = [];
                          Response.artists.forEach(data => {
                            data.artistCatalogueRaisonees.forEach(catalog => {
                              this.propertyCatalogRaisonnee.push({ artistCatalogueRaisoneeId: catalog.id, artistCatalogueRaisoneeAuthor: catalog.author, artistCatalogueRaisoneeType: catalog.type, sortId: catalog.sortId, number: '', volume: '', });
                            })
                          });
                          Response.propertyCatalogueRaisonees.forEach(data => {
                            if (data.artistCatalogueRaisoneeType == "" && data.artistCatalogueRaisoneeAuthor == "" && data.volume == "" && data.number == "") {
                              return;
                            } else {
                              for (var i = 0; i < this.propertyCatalogRaisonnee.length; i++) {
                                if (this.propertyCatalogRaisonnee[i].artistCatalogueRaisoneeId == data.artistCatalogueRaisoneeId) {
                                  if (!(data.volume == "" && data.number == "")) {
                                    this.propertyCatalogRaisonnee[i].number = data.number;
                                    this.propertyCatalogRaisonnee[i].volume = data.volume;
                                  }
                                  break;
                                }
                              }
                            }
                          });
                          var propertyDrop = this.propertyCatalogRaisonnee;
                          var propertyDragegd = this.propertyCatalogRaisonneeDrag;

                          this.propertyCatalogRaisonnee = [];
                          this.propertyCatalogRaisonneeDrag = [];
                          propertyDrop.forEach(data => {
                            var index = propertyDragegd.findIndex(x => x.artistCatalogueRaisoneeId === data.artistCatalogueRaisoneeId);
                            var draggedData = ((index > -1) ? ((propertyDragegd[index].volume === data.volume) ? ((propertyDragegd[index].number === data.number) ? false : true) : true) : true);
                            data.color = draggedData;
                            this.propertyCatalogRaisonnee.push(data);
                          });
                          propertyDragegd.forEach(data => {
                            var index = propertyDrop.findIndex(x => x.artistCatalogueRaisoneeId === data.artistCatalogueRaisoneeId);
                            var DropData = ((index > -1) ? ((propertyDrop[index].volume === data.volume) ? ((propertyDrop[index].number === data.number) ? false : true) : true) : true);
                            data.color = DropData;
                            this.propertyCatalogRaisonneeDrag.push(data);
                          });
                          this.propertyCatalogRaisonnee = this.sort(this.propertyCatalogRaisonnee);
                          this.propertyCatalogRaisonneeDrag = this.sort(this.propertyCatalogRaisonneeDrag);
                          document.getElementsByTagName('html')[0].classList.remove('loading');
                          callback(true);
                        }, resFileError => {
                          this.tosatservice.addToastError('There is an error while saving your record', 'top-right');
                          this.errorMsg = resFileError;
                          document.getElementsByTagName('html')[0].classList.remove('loading');
                          callback(false);
                        });
                    }
                  }
                }
              }
            }
          }

        }
      } else {
        this.tosatservice.addToastError('There is an error while saving your record', 'top-right');
        document.getElementsByTagName('html')[0].classList.remove('loading');
        callback(false);
      }
    } else {
      this.tosatservice.addToastError('There is an error while saving your record', 'top-right');
      document.getElementsByTagName('html')[0].classList.remove('loading');
      callback(false);
    }
  }

  onSubmitMergeDrag(value, flag: string, callback) {
    var artistArr = [];
    this.checkedArtistDrag.forEach(data => {
      artistArr.push(data.id);
    });
    delete value.artistDataDrag;
    delete value.archiveNumber;
    delete value.editionSizeTotal;
    delete value.researcherNotesDate;
    delete value.researcherName;
    delete value.depthIn;
    delete value.heightIn;
    delete value.widthIn;
    value.artistIds = artistArr;
    value.archiveNumbers = this.getArchiveNumbersDrag();
    value.editionSizeTypeName = (this.editionSizeObjectDrag && this.editionSizeObjectDrag.editionSizeType) ? this.editionSizeObjectDrag.editionSizeType : null;
    value.editionSizeTotal = (this.editionSizeObjectDrag && this.editionSizeObjectDrag.editionSizeTotal) ? this.editionSizeObjectDrag.editionSizeTotal : null;
    value.seriesTotal = (this.editionSizeObjectDrag && this.editionSizeObjectDrag.editionSizeSeries) ? this.editionSizeObjectDrag.editionSizeSeries : null;
    value.trialProofTotal = (this.editionSizeObjectDrag && this.editionSizeObjectDrag.editionSizeTrialProof) ? this.editionSizeObjectDrag.editionSizeTrialProof : null;
    value.artistProofTotal = (this.editionSizeObjectDrag && this.editionSizeObjectDrag.editionSizeArtistProof) ? this.editionSizeObjectDrag.editionSizeArtistProof : null;
    value.bonATirerTotal = (this.editionSizeObjectDrag && this.editionSizeObjectDrag.editionSizeBonATirerProof) ? this.editionSizeObjectDrag.editionSizeBonATirerProof : null;
    value.printersProofTotal = (this.editionSizeObjectDrag && this.editionSizeObjectDrag.editionSizePrintersProof) ? this.editionSizeObjectDrag.editionSizePrintersProof : null;
    value.horsCommerceProofTotal = (this.editionSizeObjectDrag && this.editionSizeObjectDrag.editionSizeHorsCommerceProof) ? this.editionSizeObjectDrag.editionSizeHorsCommerceProof : null;
    value.dedicatedProofTotal = (this.editionSizeObjectDrag && this.editionSizeObjectDrag.editionSizeDedicatedProof) ? this.editionSizeObjectDrag.editionSizeDedicatedProof : null;
    value.museumProofTotal = (this.editionSizeObjectDrag && this.editionSizeObjectDrag.editionSizeMuseumProof) ? this.editionSizeObjectDrag.editionSizeMuseumProof : null;
    value.editionOverride = (this.editionSizeObjectDrag && this.editionSizeObjectDrag.editionSizeEditionOverride) ? this.editionSizeObjectDrag.editionSizeEditionOverride : null;
    value.editionSizeNotes = (this.editionSizeObjectDrag && this.editionSizeObjectDrag.editionSizeNotes) ? this.editionSizeObjectDrag.editionSizeNotes : null;
    if (this.DragDetailsForm.controls['researcherNotesDate'].value) {
      value.researcherName = window['keycloak'].tokenParsed.preferred_username;
      value.researcherNotesDate = this.getDate(this.DragDetailsForm.controls['researcherNotesDate'].value, "mm-dd-yyyy");
    }
    value.propertyCatalogueRaisonees = this.updatePropertyCatalogRaisonneesDrag('update');
    var ObjectId = (this.dragImageFlag) ? (this.Dragged.id + '?dropImage=true') : this.Dragged.id + '?';
    if (this.dragImageFlag) {
      value.propertyImage = this.dragImagePathUrl;
      delete value.imagePath;
    } else {
      value.imagePath = this.Dragged.imagePath;
    }

    if (value.category == "") {
      value.category = null;
    }
    if (value.subCategory == "") {
      value.subCategory = null;
    }
    if (value.presizeName == "") {
      value.presizeName = null;
    }
    if (value.editionName == "") {
      value.editionName = null;
    }
    if (value.researcherName) {
      value.researcherName = window['keycloak'].tokenParsed.preferred_username;
    }


    if ((this.DragDetailsForm.valid || this.requiredFlag) && this.checkedArtistDrag.length) {
      var regex = '^0*([1-8][0-9]{3}|9[0-8][0-9]{2}|99[0-8][0-9]|999[0-9])$';
      if (value && (value && value.yearStart && !value.yearStart.toString().match(regex) ? false : true)
        && (value && value.yearEnd && !value.yearEnd.toString().match(regex) ? false : true)
        && (value && value.castYearStart && !value.castYearStart.toString().match(regex) ? false : true)
        && (value && value.castYearEnd && !value.castYearEnd.toString().match(regex) ? false : true)) {
        if (flag == "dragmergeclick") {
          document.getElementsByTagName('html')[0].classList.add('loading');
          const temp = Object.assign({}, this.Dragged);
          this.Dragged = null;
          this._objectService
            .mergeObject(ObjectId, this.Drop.id, value)
            .subscribe(Response => {
              document.getElementsByTagName('html')[0].classList.remove('loading');
              this.thumbnail_deleteid = this.Drop.id;
              this.tosatservice.addToast('Merge Object successfully updated.', 'top-right');
              this.Dragged = Response
              this.ngOnInit();
              callback(true);
            }, resFileError => {
              this.Dragged = temp;
              document.getElementsByTagName('html')[0].classList.remove('loading');
              this.tosatservice.addToastError('There is an error while saving your record', 'top-right');
              this.errorMsg = resFileError;
              callback(false);
            });
        } else {
          if (value.yearStart && !value.yearEnd) {
            this.tosatservice.addToastError('Please enter Year2', 'top-right');
          } else {
            if (value.castYearStart && !value.castYearEnd) {
              this.tosatservice.addToastError('Please enter CastYear2', 'top-right');
            } else {
              if (value.yearStart && value.yearEnd && value.yearEnd < value.yearStart) {
                this.tosatservice.addToastError('Year2 must be greater than or equal to Year1', 'top-right');
              } else {
                if (value.castYearStart && value.castYearEnd && value.castYearEnd < value.castYearStart) {
                  this.tosatservice.addToastError('CastYear2 must be greater than or equal to CastYear1', 'top-right');
                } else {
                  if (!value.yearStart && value.yearEnd) {
                    this.tosatservice.addToastError('Please enter Year1', 'top-right');
                  } else {
                    if (!value.castYearStart && value.castYearEnd) {
                      this.tosatservice.addToastError('Please enter CastYear1', 'top-right');
                    } else {
                      document.getElementsByTagName('html')[0].classList.add('loading');
                      this._objectService
                        .updateProperty(ObjectId, value)
                        .subscribe(Response => {
                          var tempEditionSizeTotal = Response.editionOverride ? Response.editionOverride : (Response.seriesTotal ? (Response.seriesTotal == -1 ? 1 : Response.seriesTotal) + '+' : '') + (Response.artistProofTotal ? (Response.artistProofTotal == -1 ? '' : Response.artistProofTotal) + 'AP+' : '') + (Response.trialProofTotal ? (Response.trialProofTotal == -1 ? '' : Response.trialProofTotal) + 'TP+' : '') + (Response.bonATirerTotal ? (Response.bonATirerTotal == -1 ? '' : Response.bonATirerTotal) + 'BAT+' : '') + (Response.printersProofTotal ? (Response.printersProofTotal == -1 ? '' : Response.printersProofTotal) + 'PP+' : '') + (Response.horsCommerceProofTotal ? (Response.horsCommerceProofTotal == -1 ? '' : Response.horsCommerceProofTotal) + 'HC+' : '') + (Response.dedicatedProofTotal ? (Response.dedicatedProofTotal == -1 ? '' : Response.dedicatedProofTotal) + 'DP+' : '') + (Response.museumProofTotal ? (Response.museumProofTotal == -1 ? '' : Response.museumProofTotal) + 'MP' : '');
                          this.DragDetailsForm.controls['editionSizeTotal'].setValue(tempEditionSizeTotal)
                          if (Response.researcherNotesDate) {
                            Response.researcherNotesDate = this.getReasearcherDate(Response.researcherNotesDate, "mm-dd-yyyy");
                            this.dragResearcherDate = Response.researcherNotesDate;
                            this.DragDetailsForm.controls['researcherNotesDate'].setValue(Response.researcherNotesDate);
                          }
                          if (Response.researcherNotesText != null && Response.researcherNotesText.length <= 0) {
                            this.DragDetailsForm.controls['researcherNotesText'].setValue(null);
                          }
                          if (Response.researcherName) {
                            this.DragDetailsForm.controls['researcherName'].setValue(this.getUpdatedResaearcherName(Response.researcherName));
                            this.dragDisplayName = this.getUpdatedResaearcherName(Response.researcherName);
                          }
                          if (flag == "dragmergeclick") {
                            this.tosatservice.addToast('Merge Object successfully updated.', 'top-right');
                          } else {
                            this.tosatservice.addToast('Object successfully updated', 'top-right');
                          }
                          var d = new Date();
                          var n = d.getTime();
                          if (Response.imagePath) {
                            this.DragImage = this.URL + '/properties/' + this.Dragged.id + '/images/default?' + n;
                            if (this.doesFileExist(this.DragImage)) {
                              this.imageDragImag2 = true;
                            } else {
                              this.imageDragImag2 = false;
                            }
                          }
                          this.propertyCatalogRaisonneeDrag = [];
                          Response.artists.forEach(data => {
                            data.artistCatalogueRaisonees.forEach(catalog => {
                              this.propertyCatalogRaisonneeDrag.push({ artistCatalogueRaisoneeId: catalog.id, artistCatalogueRaisoneeAuthor: catalog.author, artistCatalogueRaisoneeType: catalog.type, sortId: catalog.sortId, number: '', volume: '', });
                            })
                          });
                          Response.propertyCatalogueRaisonees.forEach(data => {
                            if (data.artistCatalogueRaisoneeType == "" && data.artistCatalogueRaisoneeAuthor == "" && data.volume == "" && data.number == "") {
                              return;
                            } else {
                              for (var i = 0; i < this.propertyCatalogRaisonneeDrag.length; i++) {
                                if (this.propertyCatalogRaisonneeDrag[i].artistCatalogueRaisoneeId == data.artistCatalogueRaisoneeId) {
                                  if (!(data.volume == "" && data.number == "")) {
                                    this.propertyCatalogRaisonneeDrag[i].number = data.number;
                                    this.propertyCatalogRaisonneeDrag[i].volume = data.volume;
                                  }
                                  break;
                                }
                              }
                            }
                          });
                          var propertyDrop = this.propertyCatalogRaisonnee;
                          var propertyDragegd = this.propertyCatalogRaisonneeDrag;

                          this.propertyCatalogRaisonnee = [];
                          this.propertyCatalogRaisonneeDrag = [];
                          propertyDrop.forEach(data => {
                            var index = propertyDragegd.findIndex(x => x.artistCatalogueRaisoneeId === data.artistCatalogueRaisoneeId);
                            var draggedData = ((index > -1) ? ((propertyDragegd[index].volume === data.volume) ? ((propertyDragegd[index].number === data.number) ? false : true) : true) : true);
                            data.color = draggedData;
                            this.propertyCatalogRaisonnee.push(data);
                          });
                          propertyDragegd.forEach(data => {
                            var index = propertyDrop.findIndex(x => x.artistCatalogueRaisoneeId === data.artistCatalogueRaisoneeId);
                            var DropData = ((index > -1) ? ((propertyDrop[index].volume === data.volume) ? ((propertyDrop[index].number === data.number) ? false : true) : true) : true);
                            data.color = DropData;
                            this.propertyCatalogRaisonneeDrag.push(data);
                          });

                          this.propertyCatalogRaisonnee = this.sort(this.propertyCatalogRaisonnee);
                          this.propertyCatalogRaisonneeDrag = this.sort(this.propertyCatalogRaisonneeDrag);
                          document.getElementsByTagName('html')[0].classList.remove('loading');
                          callback(true);
                        }, resFileError => {
                          this.tosatservice.addToastError('There is an error while saving your record', 'top-right');
                          this.errorMsg = resFileError;
                          document.getElementsByTagName('html')[0].classList.remove('loading');
                          callback(false);
                        });
                    }
                  }
                }
              }
            }
          }
        }
      } else {
        document.getElementsByTagName('html')[0].classList.remove('loading');
        this.tosatservice.addToastError('There was an error while saving your record.', 'top-right');
        callback(false);
      }
    } else {
      document.getElementsByTagName('html')[0].classList.remove('loading');
      this.tosatservice.addToastError('There was an error while saving your record.', 'top-right');
      callback(false);
    }
  }
  //edition number modal
  public editionSizeObject: any;
  EditionModalPrompt() {
    this.dialogService.addDialog(EditionModalComponent,
      { editionSizeTypes: this.editionSizeTypes, editionSizeObject: this.editionSizeObject, newObject: false },
      { backdropColor: 'rgba(220,220,220,0.5)' })
      .subscribe((message: any) => {
        if (message.editionSizeType != this.editionSizeObject.editionSizeType ||
          message.editionSizeSeries != this.editionSizeObject.editionSizeSeries ||
          message.editionSizeArtistProof != this.editionSizeObject.editionSizeArtistProof ||
          message.editionSizeTrialProof != this.editionSizeObject.editionSizeTrialProof ||
          message.editionSizeBonATirerProof != this.editionSizeObject.editionSizeBonATirerProof ||
          message.editionSizePrintersProof != this.editionSizeObject.editionSizePrintersProof ||
          message.editionSizeHorsCommerceProof != this.editionSizeObject.editionSizeHorsCommerceProof ||
          message.editionSizeDedicatedProof != this.editionSizeObject.editionSizeDedicatedProof ||
          message.editionSizeMuseumProof != this.editionSizeObject.editionSizeMuseumProof ||
          message.editionSizeHorsCommerceProof != this.editionSizeObject.editionSizeHorsCommerceProof ||
          message.editionSizeEditionOverride != this.editionSizeObject.editionSizeEditionOverride ||
          message.editionSizeTotal != this.editionSizeObject.editionSizeTotal) {
          this.dropFlag = true;
        }
        this.editionSizeObject = message;
        if (this.editionSizeObject && (this.editionSizeObject.editionSizeDisplay || this.editionSizeObject.editionSizeEditionOverride)) {
          this.DropDetailsForm.controls['editionSizeTotal'].setValue(this.editionSizeObject.editionSizeEditionOverride ? this.editionSizeObject.editionSizeEditionOverride : this.editionSizeObject.editionSizeDisplay);
        }
      });
  }

  public editionSizeObjectDrag: any;
  EditionModalPromptDrag() {
    this.dialogService.addDialog(EditionModalComponent,
      { editionSizeTypes: this.editionSizeTypes, editionSizeObject: this.editionSizeObjectDrag, newObject: false },
      { backdropColor: 'rgba(220,220,220,0.5)' })
      .subscribe((message: any) => {
        this.editionSizeObjectDrag = message;
        if (message.editionSizeType != this.editionSizeObjectDrag.editionSizeType ||
          message.editionSizeSeries != this.editionSizeObjectDrag.editionSizeSeries ||
          message.editionSizeArtistProof != this.editionSizeObjectDrag.editionSizeArtistProof ||
          message.editionSizeTrialProof != this.editionSizeObjectDrag.editionSizeTrialProof ||
          message.editionSizeBonATirerProof != this.editionSizeObjectDrag.editionSizeBonATirerProof ||
          message.editionSizePrintersProof != this.editionSizeObjectDrag.editionSizePrintersProof ||
          message.editionSizeHorsCommerceProof != this.editionSizeObjectDrag.editionSizeHorsCommerceProof ||
          message.editionSizeDedicatedProof != this.editionSizeObjectDrag.editionSizeDedicatedProof ||
          message.editionSizeMuseumProof != this.editionSizeObjectDrag.editionSizeMuseumProof ||
          message.editionSizeHorsCommerceProof != this.editionSizeObjectDrag.editionSizeHorsCommerceProof ||
          message.editionSizeEditionOverride != this.editionSizeObjectDrag.editionSizeEditionOverride ||
          message.editionSizeTotal != this.editionSizeObjectDrag.editionSizeTotal) {
          this.dragFlag = true;
        }
        this.editionSizeObjectDrag = message;
        if (this.editionSizeObjectDrag && (this.editionSizeObjectDrag.editionSizeDisplay || this.editionSizeObjectDrag.editionSizeEditionOverride)) {
          this.DragDetailsForm.controls['editionSizeTotal'].setValue(this.editionSizeObjectDrag.editionSizeEditionOverride ? this.editionSizeObjectDrag.editionSizeEditionOverride : this.editionSizeObjectDrag.editionSizeDisplay);
        }
      });
  }

  checkParam(dropVal, dragVal) {
    if (dropVal && dragVal) {
      if (dropVal === dragVal) {
        return (this.highlightFlag ? false : false);
      }
      else {
        return (this.highlightFlag ? true : false);
      }
    }
    else if (dropVal === null && dragVal === null) {
      return (this.highlightFlag ? false : false);
    }
    else {
      return (this.highlightFlag ? true : false);
    }
  }
  checkDropDownParam(dropVal, dragVal) {
    if (dropVal && dragVal) {
      if (dropVal.name === dragVal.name) {
        return (this.highlightFlag ? false : false);
      }
      else {
        return (this.highlightFlag ? true : false);
      }
    }
    else {
      return (this.highlightFlag ? false : false);
    }
  }

  getStyle(dropVal, dragVal) {
    var setStyle = '#ecf542';
    if (dropVal && dragVal) {
      if (dropVal === dragVal) {
        setStyle = '';
      }
    }
    else if ((dropVal && dragVal === null) || (dragVal && dropVal === null)) {
      setStyle = '#ecf542';
    }
    return (this.highlightFlag ? setStyle : '');
  }

  getClass(dropVal, dragVal) {
    var setCssClass = 'compare-field';
    if (dropVal && dragVal) {
      if (dropVal === dragVal) {
        setCssClass = '';
      }
    }
    else if ((dropVal && dragVal === null) || (dragVal && dropVal === null)) {
      setCssClass = 'compare-field';
    }
    return (this.highlightFlag ? setCssClass : '');
  }
  public highlightFlag: boolean = true;
  dropmerge() {
    this.onSubmitMerge(this.DropDetailsForm.value, "dropmergeclick", (dropData) => {
      if (dropData) {
        this.highlightFlag = false;
        this.dropFlag = false;
        this.dragFlag = false;
        document.getElementById('dragarea').style.display = 'none';
        document.getElementById('droparea').classList.add("col-md-12");
        document.getElementById('droparea').classList.remove("col-md-6");
      }
    });
  }

  dragmerge() {
    this.onSubmitMergeDrag(this.DragDetailsForm.value, "dragmergeclick", (dragData) => {
      if (dragData) {
        this.highlightFlag = false;
        this.dropFlag = false;
        this.dragFlag = false;
        document.getElementById('droparea').style.display = 'none';
        document.getElementById('dragarea').classList.add("col-md-12");
        document.getElementById('dragarea').classList.remove("col-md-6");
      }
    });
  }

  checkArtist() {
    if (this.artistName.length == this.artistNameDrag.length) {
      if ((this.artistName.length && this.artistNameDrag.length) != 0) {
        for (let i = 0; i < this.artistName.length; i++) {
          if (this.artistName[i].id != this.artistNameDrag[i].id) {
            return true;
          }
        }
      }
    } else {
      return true;
    }
  }
  compareArchive() {
    if (this.archiveNoTags.length == this.archiveNoTagsDrag.length) {
      if ((this.archiveNoTagsDrag.length && this.archiveNoTags.length) != 0) {
        for (let i = 0; i < this.archiveNoTags.length; i++) {
          if (this.archiveNoTags[i] != this.archiveNoTagsDrag[i]) {
            return true;
          } else {
            return false;
          }
        }
      }
    } else {
      return true;
    }
  }

  //Close modal 
  closeModal() {
    this.dropFlag = false;
    this.dragFlag = false;
    this.result = this.thumbnail_deleteid;
    this.close();
  }
  //warning message when change the status from result mode to edit mode
  modalClose() {
    if (this.dropFlag || this.dragFlag) {
      let disposable = this.dialogService.addDialog(ConfirmComponent,
        { title: 'Confirm', message: 'There are some unsaved changes made to these records, would you like to save them?' })
        .subscribe((isConfirmed) => {
          if (isConfirmed) {
            if (this.dropFlag && this.dragFlag) {
              this.onSubmitMerge(this.DropDetailsForm.value, 'update', (dropData) => {
                if (dropData) {
                  this.onSubmitMergeDrag(this.DragDetailsForm.value, 'update', (dragData) => {
                    if (dragData) {
                      this.closeModal();
                    }
                  });
                }
              });
            } else if (this.dropFlag) {
              this.onSubmitMerge(this.DropDetailsForm.value, 'update', (dropData) => {
                if (dropData) {
                  this.closeModal();
                }
              });
            } else if (this.dragFlag) {
              this.onSubmitMergeDrag(this.DragDetailsForm.value, 'update', (dragData) => {
                if (dragData) {
                  this.closeModal();
                }
              });
            }
          } else {
            this.result = this.thumbnail_deleteid;
            this.close();
          }
        })
    } else {
      this.result = this.thumbnail_deleteid;
      this.close();
    }
  }

  // submit merge asyn calls
  SubmitMerged(flag) {
    if (flag === "dropFlag") {
      this.onSubmitMerge(this.DropDetailsForm.value, 'update', () => {
        this.dropFlag = false;
      });
    } else if (flag === "dragFlag") {
      this.onSubmitMergeDrag(this.DragDetailsForm.value, 'update', () => {
        this.dragFlag = false;
      });
    }
  }

  public dropImageFlag: boolean = false;
  public dropImagePathUrl;
  public dragImageFlag: boolean = false;
  public dragImagePathUrl;
  addToBasket($event: any, event: any) {
    var d = new Date();
    var n = d.getTime();
    if (event === "dropFlag") {
      if ($event.dragData.indexOf(this.Dragged.id) <= -1) {
        this.dragImageFlag = true;
        this.dragFlag = true;
        this.dragImagePathUrl = this.Drop.imagePath;
        this.DragImage = this.URL + '/properties/' + this.Drop.id + '/images/default?' + n;
      }
    } else if (event === "draggedFlag") {
      if ($event.dragData.indexOf(this.Drop.id) <= -1) {
        this.dropImageFlag = true;
        this.dropFlag = true;
        this.dropImagePathUrl = this.Dragged.imagePath;
        this.DropImage = this.URL + '/properties/' + this.Dragged.id + '/images/default?' + n;
      }
    }
  }
  orderedProduct($event: any) {
  }

  validateSelectedItem(selectedItem, list) {
    if (selectedItem) {
      selectedItem = selectedItem.toLowerCase();
      let selectListObject;
      if (list === "category") {
        let index = this.categories.findIndex(list => list.name.toLowerCase() === selectedItem);
        if (!(index > -1)) {
          this.DropDetailsForm.controls['category'].setValue('');
        }
      }
      else if (list === "subCategory") {
        let index = this.subcategories.findIndex(list => list.name.toLowerCase() === selectedItem);
        if (!(index > -1)) {
          this.DropDetailsForm.controls['subCategory'].setValue('');
        }
      }
      else if (list === "presizeName") {
        let index = this.presizeNames.findIndex(list => list.name.toLowerCase() === selectedItem);
        if (!(index > -1)) {
          this.DropDetailsForm.controls['presizeName'].setValue('');
        }
      }
      else if (list === "editionName") {
        let index = this.editionTypes.findIndex(list => list.name.toLowerCase() === selectedItem);
        if (!(index > -1)) {
          this.DropDetailsForm.controls['editionName'].setValue('');
        }
      }
      else if (list === "flagsCountry") {
        let index = this.countries.findIndex(list => list['name'].toLowerCase() === selectedItem);
        if (!(index > -1)) {
          this.DropDetailsForm.patchValue({
            'flags': { flagsCountry: '' }
          });
        }
      } else {
        return false;
      }
    }
  }

  validateSelectedItemDrag(selectedItem, list) {
    if (selectedItem) {
      selectedItem = selectedItem.toLowerCase();
      let selectListObject;
      if (list === "category") {
        let index = this.categories.findIndex(list => list.name.toLowerCase() === selectedItem);
        if (!(index > -1)) {
          this.DragDetailsForm.controls['category'].setValue('');
        }
      }
      else if (list === "subCategory") {
        let index = this.subcategories.findIndex(list => list.name.toLowerCase() === selectedItem);
        if (!(index > -1)) {
          this.DragDetailsForm.controls['subCategory'].setValue('');
        }
      }
      else if (list === "presizeName") {
        let index = this.presizeNames.findIndex(list => list.name.toLowerCase() === selectedItem);
        if (!(index > -1)) {
          this.DragDetailsForm.controls['presizeName'].setValue('');
        }
      }
      else if (list === "editionName") {
        let index = this.editionTypes.findIndex(list => list.name.toLowerCase() === selectedItem);
        if (!(index > -1)) {
          this.DragDetailsForm.controls['editionName'].setValue('');
        }
      }
      else if (list === "flagsCountry") {
        let index = this.countries.findIndex(list => list['name'].toLowerCase() === selectedItem);
        if (!(index > -1)) {
          this.DragDetailsForm.patchValue({
            'flags': { flagsCountry: '' }
          });
        }
      } else {
        return false;
      }
    }
  }
  // Set value for select drop-down list.
  setSelectListValue(selectedItem, selectListObject, listControlName, form) {
    if (form == "DropDetailsForm") {
      if (!(selectListObject.some(list => list.name.toLowerCase() == selectedItem))) {
        if (listControlName == "flagsCountry") {
          this.dropDetailsFlagsCountryValue = null;
        }
        else {
          this.DropDetailsForm.controls[listControlName].setValue(null);
        }
      }
      else if (selectListObject.some(list => list.name.toLowerCase() == selectedItem)) {
        let index = selectListObject.findIndex(list => list.name.toLowerCase() == selectedItem);
        if (listControlName == "flagsCountry") {
          this.dropDetailsFlagsCountryValue = selectListObject[index].name;
        }
        else {
          this.DropDetailsForm.controls[listControlName].setValue(selectListObject[index].name);
        }
      }
    }
    else if (form == "DragDetailsForm") {
      if (!(selectListObject.some(list => list.name.toLowerCase() == selectedItem))) {
        if (listControlName == "flagsCountry") {
          this.dragDetailsFlagsCountryValue = null;
        }
        else {
          this.DragDetailsForm.controls[listControlName].setValue(null);
        }
      }
      else if (selectListObject.some(list => list.name.toLowerCase() == selectedItem)) {
        let index = selectListObject.findIndex(list => list.name.toLowerCase() == selectedItem);
        if (listControlName == "flagsCountry") {
          this.dragDetailsFlagsCountryValue = selectListObject[index].name;
        }
        else {
          this.DragDetailsForm.controls[listControlName].setValue(selectListObject[index].name);
        }
      }
    }
  }

  onPaste(event) {
    if (parseInt(event.clipboardData.getData('text/plain')) < 0 || isNaN(parseInt(event.clipboardData.getData('text/plain'))))
      return false;
    return true;
  }
  // 'Caret' (select list) click event 
  private openList(list, listName) {
    if (listName == 'dropcategory') {
      this.dropCategoryListOpen = !this.dropCategoryListOpen;
      if (this.dropCategoryListOpen === true) {
        list.open();
        this.categoryService.search('');
      }
      else {
        list.close();
      }
    }
    if (listName == 'dragcategory') {
      this.dragCategoryListOpen = !this.dragCategoryListOpen;
      if (this.dragCategoryListOpen === true) {
        list.open();
        this.categoryService.search('');
      }
      else {
        list.close();
      }
    }
    if (listName == 'dropsubcategory') {
      this.dropSubCategoryListOpen = !this.dropSubCategoryListOpen;
      if (this.dropSubCategoryListOpen === true) {
        list.open();
        this.subCategoryService.search('');
      }
      else {
        list.close();
      }
    }
    if (listName == 'dragsubcategory') {
      this.dragSubCategoryListOpen = !this.dragSubCategoryListOpen;
      if (this.dragSubCategoryListOpen === true) {
        list.open();
        this.subCategoryService.search('');
      }
      else {
        list.close();
      }
    }
    if (listName == 'droppresizename') {
      this.dropPresizeNameListOpen = !this.dropPresizeNameListOpen;
      if (this.dropPresizeNameListOpen === true) {
        list.open();
        this.presizeTypesService.search('');
      }
      else {
        list.close();
      }
    }
    if (listName == 'dragpresizename') {
      this.dragPresizeNameListOpen = !this.dragPresizeNameListOpen;
      if (this.dragPresizeNameListOpen === true) {
        list.open();
        this.presizeTypesService.search('');
      }
      else {
        list.close();
      }
    }
    if (listName == 'dropeditionname') {
      this.dropEditionNameListOpen = !this.dropEditionNameListOpen;
      if (this.dropEditionNameListOpen === true) {
        list.open();
        this.editionTypeService.search('');
      }
      else {
        list.close();
      }
    }
    if (listName == 'drageditionname') {
      this.dragEditionNameListOpen = !this.dragEditionNameListOpen;
      if (this.dragEditionNameListOpen === true) {
        list.open();
        this.editionTypeService.search('');
      }
      else {
        list.close();
      }
    }
    if (listName == 'dropflagscountry') {
      this.dropFlagsCountryListOpen = !this.dropFlagsCountryListOpen;
      if (this.dropFlagsCountryListOpen === true) {
        list.open();
        this.countryService.search('');
      }
      else {
        list.close();
      }
    }
    if (listName == 'dragflagscountry') {
      this.dragFlagsCountryListOpen = !this.dragFlagsCountryListOpen;
      if (this.dragFlagsCountryListOpen === true) {
        list.open();
        this.countryService.search('');
      }
      else {
        list.close();
      }
    }
  }

  // Close drop-down lists
  closeList(list, listName) {
    list.close();

    if (listName == 'dropcategory') {
      this.dropCategoryListOpen = !this.dropCategoryListOpen;
    }
    if (listName == 'dragcategory') {
      this.dragCategoryListOpen = !this.dragCategoryListOpen;
    }
    if (listName == 'dropsubcategory') {
      this.dropSubCategoryListOpen = !this.dropSubCategoryListOpen;
    }
    if (listName == 'dragsubcategory') {
      this.dragSubCategoryListOpen = !this.dragSubCategoryListOpen;
    }
    if (listName == 'droppresizename') {
      this.dropPresizeNameListOpen = !this.dropPresizeNameListOpen;
    }
    if (listName == 'dragpresizename') {
      this.dragPresizeNameListOpen = !this.dragPresizeNameListOpen;
    }
    if (listName == 'dropeditionname') {
      this.dropEditionNameListOpen = !this.dropEditionNameListOpen;
    }
    if (listName == 'drageditionname') {
      this.dragEditionNameListOpen = !this.dragEditionNameListOpen;
    }
    if (listName == 'dropflagscountry') {
      this.dropFlagsCountryListOpen = !this.dropFlagsCountryListOpen;
    }
    if (listName == 'dragflagscountry') {
      this.dragFlagsCountryListOpen = !this.dragFlagsCountryListOpen;
    }
  }

  doesFileExist(urlToFile) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', urlToFile, false);
    xhr.send();
    return xhr.status === 200;
  }
  //Sorting CR
  sort(arr) {
    var len = arr.length;
    for (var i = len - 1; i >= 0; i--) {
      for (var j = 1; j <= i; j++) {
        if (arr[j - 1].sortId > arr[j].sortId) {
          var temp = arr[j - 1];
          arr[j - 1] = arr[j];
          arr[j] = temp;
        }
      }
    }
    return arr;
  }

  // EOS-838 - Reject new values in autocomplete list (Artist Name field).
  resetArtistNameInput(formName) {
    if (formName == 'drop') {
      this.DropDetailsForm.controls['artistData'].setValue('');
    }
    else if (formName == 'drag') {
      this.DragDetailsForm.controls['artistDataDrag'].setValue('');
    }
  }

  expandImageDrag(type) {
    var image;
    var expandImage;
    var imageType;
    if (type == 'Drag') {
      this.draggableFlagDrag = true;
      this.expandImagePathDrag = this.DragImage;
      image = document.createElement('IMG');
      image.src = this.DragImage;
    } else {
      this.draggableFlagDrop = true;
      this.expandImagePathDrop = this.DropImage;
      image = document.createElement('IMG');
      image.src = this.DropImage;
    }
    expandImage = image as HTMLImageElement;
    var height = expandImage.naturalHeight;
    var width = expandImage.naturalWidth;
    if (height > width) {
      imageType = 'height';
    } else {
      if (height == width) {
        imageType = 'equal';
      } else {
        imageType = 'width';
      }
    }
    setTimeout(() => {
      switch (imageType) {
        case 'height': document.getElementById('expand-image-path-drag').style.height = "550px";
          break;
        case 'width': document.getElementById('expand-image-path-drag').style.width = "550px";
          break;
        case 'equal': document.getElementById('expand-image-path-drag').style.width = "550px";
          break;
      }
      document.getElementById('expand-image-drag').style.display = "block";
    }, 1000)
  }

  expandImageDrop(type) {
    var image;
    var expandImage;
    var imageType;
    if (type == 'Drag') {
      this.draggableFlagDrag = true;
      this.expandImagePathDrag = this.DragImage;
      image = document.createElement('IMG');
      image.src = this.DragImage;
    } else {
      this.draggableFlagDrop = true;
      this.expandImagePathDrop = this.DropImage;
      image = document.createElement('IMG');
      image.src = this.DropImage;
    }
    expandImage = image as HTMLImageElement;
    var height = expandImage.naturalHeight;
    var width = expandImage.naturalWidth;
    if (height > width) {
      imageType = 'height';
    } else {
      if (height == width) {
        imageType = 'equal';
      } else {
        imageType = 'width';
      }
    }
    setTimeout(() => {
      switch (imageType) {
        case 'height': document.getElementById('expand-image-path-drop').style.height = "550px";
          break;
        case 'width': document.getElementById('expand-image-path-drop').style.width = "550px";
          break;
        case 'equal': document.getElementById('expand-image-path-drop').style.width = "550px";
          break;
      }
      document.getElementById('expand-image-drop').style.display = "block";
    })
  }

  //Result mode to edit mode function
  public dropFlag: boolean = false;
  public dragFlag: boolean = false;
  @HostListener('window:input', ['$event'])
  onInput(e) {
    if (!(e && e.target && e.target.classList && JSON.stringify(e.target.classList).indexOf('completer-input') > -1)) {
      if (e && e.srcElement && e.srcElement.form && JSON.stringify(e.srcElement.form.classList).indexOf('DropForm') > -1) {
        this.dropFlag = true;
      } else if (e && e.srcElement && e.srcElement.form && JSON.stringify(e.srcElement.form.classList).indexOf('DragForm') > -1) {
        this.dragFlag = true;
      }
    }
  }
  @HostListener('document:click', ['$event'])
  handleClick(e) {
    if (e && ((e.target && e.target.classList && (JSON.stringify(e.target.classList).indexOf('checkboxEvent') > -1 || JSON.stringify(e.target.classList).indexOf('selectEvent') > -1)))) {
      if (e.srcElement && e.srcElement.form && JSON.stringify(e.srcElement.form.classList).indexOf('DropForm') > -1) {
        this.dropFlag = true;
      } else if (e.srcElement && e.srcElement.form && JSON.stringify(e.srcElement.form.classList).indexOf('DragForm') > -1) {
        this.dragFlag = true;
      }
    }
  }
  changeDropFunction(e) {
    if (e && e.originalObject && e.originalObject.name) {
      this.dropFlag = true;
    }
  }
  changeDragFunction(e) {
    if (e && e.originalObject && e.originalObject.name) {
      this.dragFlag = true;
    }
  }

  public draggableFlagDrag: boolean = false;
  public draggableFlagDrop: boolean = false;
  closeExpandImage(type) {
    if (type == 'drag') {
      this.draggableFlagDrag = false;
      this.expandImagePathDrag = null;
    } else {
      this.draggableFlagDrop = false;
      this.expandImagePathDrop = null;
    }
    document.getElementById('expand-image-' + type).style.display = "none";
  }

  onImageHover(type) {
    if (type == 'Drag') {
      if (this.DragImage) {
        var dragArray = document.getElementsByClassName('expand-icon-Drag') as HTMLCollectionOf<HTMLElement>
        dragArray[0].style.display = "block";
        dragArray[1].style.display = "block";
      }
    } else {
      if (this.DropImage) {
        var dropArray = document.getElementsByClassName('expand-icon-drop') as HTMLCollectionOf<HTMLElement>
        dropArray[0].style.display = "block";
        dropArray[1].style.display = "block";
      }
    }
  }

  onLeave(type) {
    if (type == 'Drag') {
      if (this.DragImage) {
        var dragArray = document.getElementsByClassName('expand-icon-Drag') as HTMLCollectionOf<HTMLElement>
        dragArray[0].style.display = "none";
        dragArray[1].style.display = "none";
      }
    } else {
      if (this.DropImage) {
        var dropArray = document.getElementsByClassName('expand-icon-drop') as HTMLCollectionOf<HTMLElement>
        dropArray[0].style.display = "none";
        dropArray[1].style.display = "none";
      }
    }
  }

  showCloseButton(type) {
    if (type == 'drag') {
      document.getElementById('expand-image-close-drag').style.display = "block";
    } else {
      document.getElementById('expand-image-close-drop').style.display = "block";
    }
  }

  RemoveCloseButton(type) {
    if (type == 'drag') {
      document.getElementById('expand-image-close-drag').style.display = "none";
    } else {
      document.getElementById('expand-image-close-drop').style.display = "none";
    }
  }
  //checkbox focus event
  addOutline(event) {
    let parent = event.srcElement.parentElement;
    parent.querySelector(".fa-check").classList.add("outline");
  }
  removeOutline(event) {
    let parent = event.srcElement.parentElement;
    parent.querySelector(".fa-check").classList.remove("outline");
  }

  getReasearcherDate(presentDate, format) {
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    let month = parseInt(presentDate.split('-')[0]) - 1;
    let currentMonth = months[month];
    let day = presentDate.split('-')[1];
    let year = presentDate.split('-')[2];
    return (day + " " + currentMonth + " " + year);
  }

  checkCR() {
    var propertyDrop = this.propertyCatalogRaisonnee;
    var propertyDragegd = this.propertyCatalogRaisonneeDrag;

    this.propertyCatalogRaisonnee = [];
    this.propertyCatalogRaisonneeDrag = [];
    propertyDrop.forEach(data => {
      var index = propertyDragegd.findIndex(x => x.artistCatalogueRaisoneeId === data.artistCatalogueRaisoneeId);
      var draggedData = ((index > -1) ? ((propertyDragegd[index].volume === data.volume) ? ((propertyDragegd[index].number === data.number) ? false : true) : true) : true);
      data.color = draggedData;
      this.propertyCatalogRaisonnee.push(data);
    });
    propertyDragegd.forEach(data => {
      var index = propertyDrop.findIndex(x => x.artistCatalogueRaisoneeId === data.artistCatalogueRaisoneeId);
      var DropData = ((index > -1) ? ((propertyDrop[index].volume === data.volume) ? ((propertyDrop[index].number === data.number) ? false : true) : true) : true);
      data.color = DropData;
      this.propertyCatalogRaisonneeDrag.push(data);
    });
    this.propertyCatalogRaisonnee = this.sort(this.propertyCatalogRaisonnee);
    this.propertyCatalogRaisonneeDrag = this.sort(this.propertyCatalogRaisonneeDrag);
  }

  getUpdatedResaearcherName(name) {
    //  var tempName = name.split('.');
    //  tempName[0] = tempName[0][0].toUpperCase()+tempName[0].substring(1);
    //  tempName[1] = tempName[1][0].toUpperCase()+tempName[1].substring(1);
    return name;//tempName.join(' ');
  }
}
