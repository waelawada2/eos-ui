import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { addInterceptorForXhrRequest } from './app/authentication/sothebys-http.interceptor';

import * as Keycloak from './../node_modules/keycloak-js/dist/keycloak';

const keycloak: any = Keycloak('./assets/js/keycloak.json');

if (environment.production) {
  enableProdMode();
}

keycloak.init({ onLoad: 'login-required' }).success(function (authenticated) {
  window['keycloak'] = keycloak
  console.info('User ' + keycloak.idTokenParsed.preferred_username + ' is ' + (authenticated ? 'authenticated' : 'not authenticated'));
  addInterceptorForXhrRequest()
  platformBrowserDynamic().bootstrapModule(AppModule);
}).error(function (e) {
  console.error('Failed to initialize authentication', e)
  alert('Failed to initialize authentication');
});

