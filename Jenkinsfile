#!/usr/bin/env groovy

properties([
  buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '5')),
  pipelineTriggers([pollSCM('H/10 * * * *')])
])

@Library('sothebys-jenkins-shared-library-javascript@1.x') _

def utils = new com.sothebys.jenkins.js.DeploymentUtils()

try {

  node {

    stage('Preparation') {
      //deleteDir()
      sh 'node --version'
      sh 'npm --version'
    }

    stage('Checkout') {
      checkoutProject {}
    }

    stage('Build') {
      withEnv(["APP_NAME=${utils.createDeploymentUnit().artifactId}", "NPM_CONFIG_PROGRESS=false", "NPM_CONFIG_SPIN=false"]) {
        sh '''
        npm install
        npm run build-ci --develop
        '''
      }
    }

    // Publish to snapshot repository
    if (utils.isSnapshot()) {
      stage('Publish artifact') {
        publishArtifact {}
      }
    }

    // Continuous integration for deployable branches only
    if (utils.isDeployableBranch()) {
      stage('Trigger DEV deployment') {
        triggerDeployment {
          environment = 'dev'
        }
      }
    }

  } // node

} catch (e) {
  currentBuild.result = 'FAILURE'
  throw e
} finally {
  sendBuildNotification {}
}
