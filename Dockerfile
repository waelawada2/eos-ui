FROM nginx:stable-alpine

RUN apk update \
 && apk add jq \
 && rm -rf /var/cache/apk/*

COPY dist/ /usr/share/nginx/html/eos-ui/

COPY docker/run.sh /run.sh
COPY docker/utils.sh /utils.sh
COPY docker/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf
COPY docker/nginx/conf.d/gzip.conf /etc/nginx/conf.d/gzip.conf

EXPOSE 80

CMD ["sh", "/run.sh"]
